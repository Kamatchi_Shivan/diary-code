drop  view IF exists defects_data;  


CREATE 
VIEW `v_defects_data` AS
    SELECT 
        `d`.`defectId` AS `defectId`,
        `d`.`defectKey` AS `defectKey`,
        `d`.`projectKey` AS `projectKey`,
        `d`.`projectName` AS `projectName`,
        `d`.`projectCategory` AS `projectCategory`,
        IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`,
        `pm`.`ProjectReference` AS `projectReference`,
        IFNULL(`d`.`resolution`, 'Un-Defined') AS `resolution`,
        IFNULL(`d`.`defectSevirity`, 'Un-Defined') AS `defectSevirity`,
        IFNULL(`d`.`product`, 'Un-Defined') AS `product`,
        (CASE
            WHEN (`d`.`testLevelDetected` LIKE '%system%') THEN 'System Test'
            WHEN (`d`.`testLevelDetected` LIKE '%sprint%') THEN 'Sprint Test'
            WHEN (`d`.`testLevelDetected` LIKE '%IT%') THEN 'Integration Test'
            WHEN (`d`.`testLevelDetected` LIKE '%UAT%') THEN 'UAT'
            WHEN (`d`.`testLevelDetected` LIKE '%unit%') THEN 'Unit Test'
            ELSE 'Un-Defined'
        END) AS `testLevelDetected`,
        (CASE
            WHEN (`d`.`testTypes` LIKE '%func%') THEN 'Functional'
            WHEN (`d`.`testTypes` LIKE '%aut%') THEN 'Automation'
            WHEN (`d`.`testTypes` LIKE '%per%') THEN 'Performance'
            WHEN (`d`.`testTypes` LIKE '%sec%') THEN 'Security Test'
            WHEN (`d`.`testTypes` LIKE '%uni%') THEN 'Unit Test'
            ELSE 'Un-Defined'
        END) AS `testTypes`,
        (CASE
            WHEN (`d`.`status` LIKE '%To Verify%') THEN 'In Test'
            WHEN (`d`.`status` LIKE '%To Groom%') THEN 'In Test'
            WHEN (`d`.`status` LIKE '%In Test%') THEN 'In Test'
            WHEN (`d`.`status` LIKE '%Ready%') THEN 'UAT Ready'
            WHEN (`d`.`status` LIKE '%open%') THEN 'Open'
            WHEN (`d`.`status` LIKE '%sprintable%') THEN 'IN SPRINTABLE'
            WHEN (`d`.`status` LIKE '%progress%') THEN 'In Progress'
            WHEN (`d`.`status` LIKE '%review%') THEN 'In Review'
            WHEN (`d`.`status` LIKE '%promot%') THEN 'To Promote'
            WHEN (`d`.`status` LIKE 'open') THEN 'Open'
            WHEN (`d`.`status` LIKE '%resol%') THEN 'Resolved'
            WHEN (`d`.`status` LIKE '%reopened%') THEN 'Reopened'
            WHEN (`d`.`status` LIKE '%closed%') THEN 'Closed'
        END) AS `status`,
        IFNULL(`d`.`createdUserName`, 'UnAssigned') AS `createdUserName`,
        IFNULL(`d`.`priority`, 'Un-Defined') AS `priority`,
        IFNULL(`d`.`assigneeName`, 'UnAssigned') AS `assigneeName`,
        IFNULL(`d`.`severity`, 'Un-Defined') AS `severity`,
        IFNULL(`d`.`component`, 'No Components') AS `component`,
        IFNULL(`d`.`sprintId`, 'Un-Defined') AS `sprintId`,
        IFNULL(`d`.`reporter`, 'UnAssigned') AS `reporter`,
        IFNULL(`d`.`progress`, 'Un-Defined') AS `progress`,
        IFNULL(`d`.`stereoType`, 'Un-Defined') AS `stereoType`,
        IFNULL(`d`.`epicLink`, 'Un-Defined') AS `epicLink`,
        IFNULL(`d`.`fixVersions`, 'Un-Defined') AS `fixVersions`,
        `pm`.`ReleaseName` AS `ReleaseName`,
        IFNULL(`d`.`issueType`, 'Un-Defined') AS `issueType`,
        IFNULL(`d`.`parentKey`, 'Un-Defined') AS `parentKey`,
        IFNULL(`d`.`storyDefectIdFixVersion`,
                'Un-Defined') AS `storyDefectIdFixVersion`
    FROM
        (`defect_details` `d`
        JOIN `project_milestones_status` `pm` ON (((CONVERT( `d`.`projectKey` USING UTF8) = `pm`.`Jira_defect_projects`)
            AND (CONVERT( `d`.`fixVersions` USING UTF8) = `pm`.`ReleaseName`))))
    WHERE
        (`d`.`status` <> 'Closed')