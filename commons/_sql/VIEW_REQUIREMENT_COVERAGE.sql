CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER 
VIEW `v_requirement_coverage` AS
 with `_temp_requirement_coverage` as 
	(select *,row_number() 
			OVER (PARTITION BY `temp_requirement_coverage`.`RequirementID` 
			ORDER BY field(`temp_requirement_coverage`.`testStatus`,'Fail','Pass','InComplete','Blocked','N/A','Not Executed','Not Covered') desc ) 
		AS `reqCount_num` from 
		(select trim(substring_index(`rq`.`ProjectName`,'-',1)) AS `ProjectReference`,
			trim(substr(`rq`.`ProjectName`,(locate('-',`rq`.`ProjectName`) + 1))) AS `ProjectName`,
			`rq`.`ProjectName` AS `project`,
			`rq`.`ReleaseName` AS `ReleaseName`,
			`rq`.`RequirementTreePAth` AS `RequirementTreePath`,
			`rq`.`RequirementTreeNode` AS `RequirementTreeNode`,
			`rq`.`RequirementTreeSubNode` AS `RequirementTreeSubNode`,
			`rq`.`RequirementTreeSubNode1` AS `RequirementTreeSubNode1`,
			`rq`.`RequirementID` AS `RequirementID`,`rq`.`actualTestCaseIds` AS `actualTestCaseIDs`,
			`rq`.`reqReleaseTcCountMapTCCount` AS `MappedTCCount`,
			`rq`.`actualTestIDs` AS `actualTestIds`,
			`tr`.`testCaseId` AS `testcaseid`,
			ifnull(`tr`.`testStatus`,'Not Covered') AS `testStatus`,
			ifnull(`tr`.`testStatus`,'Not Covered') AS `reqCoverageStatus`,
			`tr`.`releaseName` AS `fixVersions`,
			`tr`.`cycle` AS `cycle`,
			`tr`.`defects` AS `defectId`,
			`tr`.`executedOn` AS `executedOn`,
			(case when ((`rq`.`actualTestCaseIds` is null) or (`rq`.`actualTestCaseIds` = '')) then 'Not Covered' 
				else 'Covered' end) AS `ReQCoverage`
			from 
			((with recursive `t` as (select * from `requirement_details`), `n` as 
				(select 1 AS `n` union select (`n`.`n` + 1) AS `n + 1` from (`n` join `t`) 
					where (`n`.`n` <= (length(`t`.`actualTestcaseIds`) - length(replace(`t`.`actualTestcaseIds`,',','')))))
			select distinct substring_index(substring_index(`t`.`actualTestcaseIds`,',',`n`.`n`),',',-(1)) AS `actualTestIDs`,`t`.* 
			from (`n` join `t`)) `rq` left join 
		(with `_test_execution_results` as 
			(select *,row_number() 
				OVER (PARTITION BY `test_execution_results`.`testCaseId`
				ORDER BY `test_execution_results`.`executedOn` desc )  AS `row_num` from `test_execution_results`)
 select `_test_execution_results`.* from `_test_execution_results` where (`_test_execution_results`.`row_num` = 1))
	`tr` on((`rq`.`actualTestIDs` = `tr`.`testCaseId`)))) as `temp_requirement_coverage`) 
select * from `_temp_requirement_coverage` where (`_temp_requirement_coverage`.`reqCount_num` = 1)