CREATE 
VIEW `zr_test_results` AS
    SELECT 
        `zr_test_execution_results`.`id` AS `id`,
        TRIM(SUBSTRING_INDEX(`zr_test_execution_results`.`projectName`,
                    '-',
                    1)) AS `ProjectReference`,
        TRIM(SUBSTR(`zr_test_execution_results`.`projectName`,
                (LOCATE('-',
                        `zr_test_execution_results`.`projectName`) + 1))) AS `ProjectName`,
        `project_milestones_status`.`Domain` AS `DomainName`,
        `zr_test_execution_results`.`projectName` AS `Project`,
        `zr_test_execution_results`.`releaseName` AS `ReleaseName`,
        `zr_test_execution_results`.`hierarchy` AS `Hierarchy`,
        NULLIF(`zr_test_execution_results`.`folder`, '') AS `Folder`,
        NULLIF(`zr_test_execution_results`.`subFolder`,
                '') AS `subFolder`,
        NULLIF(`zr_test_execution_results`.`subFolder1`,
                '') AS `subFolder1`,
        NULLIF(`zr_test_execution_results`.`subFolder2`,
                '') AS `subFolder2`,
        `zr_test_execution_results`.`createdOn` AS `CreatedOn`,
        `zr_test_execution_results`.`requirementId` AS `RequirementId`,
        `zr_test_execution_results`.`reqAltId` AS `ReqAltId`,
        `zr_test_execution_results`.`requirementName` AS `RequirementName`,
        `zr_test_execution_results`.`environment` AS `Environment`,
        `zr_test_execution_results`.`testCaseAltId` AS `RestCaseAltId`,
        `zr_test_execution_results`.`testCaseName` AS `RestCaseName`,
        `zr_test_execution_results`.`testCaseId` AS `TestCaseId`,
        `zr_test_execution_results`.`automation` AS `Automation`,
        `zr_test_execution_results`.`cycle` AS `Cycle`,
        `zr_test_execution_results`.`testStatus` AS `tStatus`,
        (CASE
            WHEN (`zr_test_execution_results`.`testStatus` LIKE '%fail%') THEN 'Fail'
            WHEN (`zr_test_execution_results`.`testStatus` LIKE '%pass%') THEN 'Pass'
            WHEN (`zr_test_execution_results`.`testStatus` LIKE '%incomp%') THEN 'Incomplete'
            WHEN (`zr_test_execution_results`.`testStatus` LIKE '%blocked%') THEN 'Blocked'
            WHEN (`zr_test_execution_results`.`testStatus` LIKE '%Not exec%') THEN 'Not Executed'
            WHEN (`zr_test_execution_results`.`testStatus` LIKE '%N/A%') THEN 'N/A'
            ELSE 'Not Executed'
        END) AS `testStatus`,
        `zr_test_execution_results`.`executedOn` AS `ExecutedOn`,
        `zr_test_execution_results`.`tags` AS `Tags`,
        `zr_test_execution_results`.`defects` AS `Defects`,
        `zr_test_execution_results`.`executedBy` AS `ExecutedBy`,
        `zr_test_execution_results`.`createdBy` AS `CreatedBy`,
        `zr_test_execution_results`.`insertedOn` AS `insertedOn`,
        `zr_test_execution_results`.`subFolderLevel` AS `subFolderLevel`,
        `zr_test_execution_results`.`updatedOn` AS `updatedOn`
    FROM
        (`zr_test_execution_results`
        LEFT JOIN `project_milestones_status` ON (((TRIM(CONVERT( SUBSTRING_INDEX(`zr_test_execution_results`.`projectName`, '-', 1) USING UTF8)) = `project_milestones_status`.`ProjectReference`)
            AND (CONVERT( TRIM(`zr_test_execution_results`.`releaseName`) USING UTF8) = `project_milestones_status`.`ReleaseName`))))