drop  view IF exists v_project_ccb_details;  


CREATE 

VIEW `v_project_ccb_details` AS
    SELECT 
        `rm`.`ReleaseName` AS `ReleaseName`,
        `rm`.`ReleaseDate` AS `ReleaseDate`,
        `pm`.`ProjectReference` AS `ProjectReference`,
        `pm`.`ProjectName` AS `ProjectName`,
        `pm`.`Test_Status` AS `test_status`,
        `pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`,
        `pm`.`Domain` AS `Domain`,
        `pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`,
        `pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link`,
        `pm`.`JIRA_Link` AS `JIRA_Link`,
        `pm`.`ZEPHYR_Link` AS `ZEPHYR_Link`,
        `pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`,
        `pm`.`Test_Coordinator` AS `Test_Coordinator`,
        `pm`.`Scrum_Master` AS `Scrum_Master`,
        `pm`.`CCB_STARTUP_Date` AS `CCB_STARTUP_Date`,
        `pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`,
        CONVERT( `pm`.`CCB_STARTUP_Remarks` USING UTF8) AS `CCB_STARTUP_Remarks`,
        CONVERT( `pm`.`Ready2StartTestRemarks` USING UTF8) AS `Ready2StartTestRemarks`,
        `pm`.`CCB_Ready4SPRINT_Date` AS `CCB_Ready4SPRINT_Date`,
        `pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`,
        CONVERT( `pm`.`CCBReady4SPRINT_Remarks` USING UTF8) AS `CCBReady4SPRINT_Remarks`,
        CONVERT( `pm`.`Ready4SprintTestRemarks` USING UTF8) AS `Ready4SprintTestRemarks`,
        `pm`.`CCB_Ready4UAT_Date` AS `CCB_Ready4UAT_Date`,
        `pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`,
        CONVERT( `pm`.`CCBReady4UAT_Remarks` USING UTF8) AS `CCBReady4UAT_Remarks`,
        CONVERT( `pm`.`Ready4UATTestRemarks` USING UTF8) AS `Ready4UATTestRemarks`,
        `rm`.`LastRegistration` AS `LastRegistrationDate`,
        `rm`.`UAT_StartDate` AS `UAT_START_DATE`,
        `rm`.`End_UAT` AS `UAT_END_DATE`,
        `rm`.`Freeze` AS `FREEZE_DATE`,
        `pm`.`CCB_Ready4PROD_Date` AS `CCB_Ready4PROD_Date`,
        `pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`,
        CONVERT( `pm`.`CCBReady4PROD_Remarks` USING UTF8) AS `CCBReady4PROD_Remarks`,
        CONVERT( `pm`.`Ready4ProdTestRemarks` USING UTF8) AS `Ready4ProdTestRemarks`,
        `pm`.`CCB_Closure_Date` AS `CCB_Closure_Date`,
        `pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`,
        CONVERT( `pm`.`CCB_CLOSURE_Remarks` USING UTF8) AS `CCB_CLOSURE_Remarks`,
        `pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`,
        `pm`.`Project_Manager` AS `Project_Manager`,
        `pm`.`unitTestReportPath` AS `unitTestReportPath`,
        `pm`.`automationReportPath` AS `automationReportPath`,
        `pm`.`performanceReportPath` AS `performanceReportPath`,
        `pm`.`securityReportPath` AS `securityReportPath`,
         `pm`.`Jira_defect_projects` AS `Jira_defect_projects`
		
		
    FROM
        (`release_milestones_status` `rm`
        LEFT JOIN `project_milestones_status` `pm` ON ((CONVERT( `rm`.`ReleaseName` USING UTF8) = `pm`.`ReleaseName`)))