module.exports = (app) => {
  const project = require("../controllers/project.controller");

  app.get("/projects", project.findAll);

  app.get("/projects/:releaseName", project.getProjectByRelease);

  app.get("/projects/summary/:releaseName", project.getSummaryByRelease);

  app.get("/projects/summary/:releaseName/:projectReference", project.getSpecificSummaryByRelease);

  app.get("/projects/singlefetch/load", project.pageloadingapi);

  app.get("/projects/ccb/:releaseName/:projectReference", project.getProjectDetailsAndCCB);

  app.get("/projects/ccb/:releaseName/", project.getCCBProjectDetailsForRelease);

  app.get("/projects/releaseHistory/:projectReference", project.getProjectReleaseHistory);

  app.get("/testreports/ccb/:releaseName/:projectReference", project.getTestReports);


};
