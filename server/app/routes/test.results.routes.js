module.exports = (app) => {
  const testResults = require("../controllers/test.results.controller");

  app.get("/testResults/", testResults.getAll);

  app.get("/testResults/:releaseName", testResults.getByRelease);

  app.get("/testResults/byProject/:releaseName/:projectReference", testResults.getByProjects);

  app.get("/results/all/", testResults.getAllTestResults);

  app.get("/results/:releaseName", testResults.getResultsByRelease);

  app.get("/results/:releaseName/:projectReference", testResults.getResultsByReleaseAndproject);

  // app.get("/testResults/byProjectFolders/:releaseName/:projectReference", testResults.getByProjectsAndFolders);

  //app.get("/testResults/byRelease/:releaseName", testResults.getAllResultsByProjectsAndFolders);


};
