

module.exports = (app) => {
    const story = require("../controllers/story.controller");

    app.get("/story/:releaseName", story.getByRelease);
    app.get("/story/:releaseName/:projectReference", story.getByProjects);
    app.get("/userstory/all", story.getAll);



};
