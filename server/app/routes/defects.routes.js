module.exports = (app) => {
    const defects = require("../controllers/defects.controller");

    app.get("/defects/all", defects.allDefects);
    app.get("/defectsByRelease/:releaseName", defects.defectsByRelease);
    app.get("/defects/status", defects.byStatus);
    app.get("/defects/component", defects.byComponent);
    app.get("/defects/assignee", defects.byAssignee);
    app.get("/defects/domain", defects.byDomain);
    app.get("/defects/EpicLink/:releaseName/:projectReference",defects.EpicByReleaseAndProject)


};
