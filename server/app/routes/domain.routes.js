module.exports = (app) => {
  const domain = require("../controllers/domain.controller");

  /* Create a new Domain  */
  app.post("/domains/", domain.create);

  /** Retrieve all Customers  */
  app.get("/domains", domain.findAll);
};
