module.exports = (app) => {

  const coverage = require("../controllers/coverage-controller");

  app.get("/coverage/all/", coverage.getAll);

  app.get("/coverage/:releaseName", coverage.getByRelease);

  app.get("/coverage/:releaseName/:projectReference", coverage.getByProjects);

  app.get("/coverageStatus/all", coverage.getStatus);

};
