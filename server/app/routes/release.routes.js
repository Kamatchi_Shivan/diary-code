module.exports = (app) => {
  const release = require("../controllers/release.controller");

  app.get("/releases", release.findAll);

  app.get("/releases/mileStones", release.allMileStones);

  app.get("/releases/mileStones/:releaseName", release.mileStonesByReleaseName);

};
