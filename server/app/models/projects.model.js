const sql = require("./db.js");

const Project = function (project) {
  this.id = project.id;
  this.projectReference = project.projectReference;
  this.projectName = project.projectName;
};

const TestReports = function (project) {
};

Project.getAll = (result) => {

  var qry = "select Id,ReleaseName,ProjectReference,ProjectName,Domain from project_milestones_status";

  sql.getConnection(function (err, connection) {
    connection.query('START TRANSACTION', function (err, rows) {
      var getRecords = connection.query(qry, (error, res) => {
        if (error) {
          console.log("error: ", error);
          result(null, error);
          return;
        }
        console.log("PROJECTS: ", res.length);
        result(null, res);
      });
      connection.query('COMMIT', function (err, rows) {
        connection.release();
      });
    });

  });
};


Project.getByRelease = (releaseName, result) => {
  var qry = `select Id,ReleaseName,ProjectReference,ProjectName,Domain from project_milestones_status where ReleaseName='${releaseName}';`;

  sql.getConnection(function (err, connection) {
    connection.query('START TRANSACTION', function (err, rows) {
      var getRecords = connection.query(qry, (error, res) => {
        if (error) {
          console.log("error: ", error);
          result(null, error);
          return;
        }
        console.log(`PROJECTS for Release : ${releaseName} `, res.length);
        result(null, res);
      });
      connection.query('COMMIT', function (err, rows) {
        connection.release();
      });
    });

  });

};



Project.getSummaryByRelease = (releaseName, result) => {
  let qry = `select ID, ProjectReference,ReleaseName,ProjectName, Test_Status , KeyRemarksTesting, KeyRemarksTesting_Plain, CONCAT('http://weconnect.pvgroup.intranet/project/general/release/Lists/CCB%20Dashboard/EditForm.aspx?ID=',
            SP_ID) AS SP_ProjectEditLink, HistoryTestKeyMessage  from project_milestones_status where ReleaseName='${releaseName}';`

  sql.getConnection(function (err, connection) {
    connection.query('START TRANSACTION', function (err, rows) {
      var getRecords = connection.query(qry, (error, res) => {
        if (error) {
          console.log("error: ", error);
          result(null, error);
          return;
        }
        console.log("SUMMARY BY RELEASE: ", res.length);
        result(null, res);
      });
      connection.query('COMMIT', function (err, rows) {
        connection.release();
      });
    });

  });
};


Project.getSummaryByReleaseAndProject = (releaseName, projectReference, result) => {
  var qry = `select ID, ReleaseName,ProjectReference,ProjectName, "Status_Go" , Test_Status ,KeyRemarksTesting, KeyRemarksTesting_Plain ,CONCAT('http://weconnect.pvgroup.intranet/project/general/release/Lists/CCB%20Dashboard/EditForm.aspx?ID=',
  SP_ID) AS SP_ProjectEditLink ,HistoryTestKeyMessage  from project_milestones_status where ReleaseName='${releaseName}'  and ProjectReference = '${projectReference}';`

  sql.getConnection(function (err, connection) {
    connection.query('START TRANSACTION', function (err, rows) {
      var getRecords = connection.query(qry, (error, res) => {
        if (error) {
          console.log("error: ", error);
          result(null, error);
          return;
        }
        console.log(`PROJECTS Release History for Project : ${projectReference} `, res.length);
        result(null, res);
      });
      connection.query('COMMIT', function (err, rows) {
        connection.release();
      });
    });

  });

};


Project.getProjectReleaseHistory = (projectReference, result) => {
  var qry = "SELECT Id,ComponentId,ComponentName,ProjectReference from v_components";

  sql.query(
    ` SELECT ReleaseName,ProjectReference, CCB_STARTUP_Date ,Ready2StartTestRemarks, CCB_STARTUP_Decision, CCB_Ready4SPRINT_Date,Ready4SprintTestRemarks, CCB_Ready4SPRINT_Decision, CCB_Ready4UAT_Date,Ready4UATTestRemarks,
              CCB_Ready4UAT_Decision, CCB_Ready4PROD_Date,Ready4ProdTestRemarks, CCB_Ready4PROD_Decision, CCB_Closure_Date,CCB_CLOSURE_Remarks, CCB_CLOSURE_Decision ,HistoryTestKeyMessage
      FROM project_milestones_status 
      WHERE  
      projectReference='${projectReference}' order by ReleaseName desc;`,
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      console.log(`PROJECTS Release History for Release : ${projectReference} `, res.length);
      result(null, res);
    }
  );
};


Project.getProjectDetailsAndCCB = (releaseName, projectReference, result) => {
  var qry = `select ReleaseName,ReleaseDate,ProjectReference,ProjectName1 as ProjectName,test_status,KeyRemarksTesting,Domain,ProjectSize_MDs,
  PROJECTREPOSITORY_Link,JIRA_Link,Story_JIRA_Link,ZEPHYR_Link,Delivery_Master_Lead,Test_Coordinator,Scrum_Master,CCB_STARTUP_Date,
  CCB_STARTUP_Decision,CCB_STARTUP_Remarks ,Ready2StartTestRemarks,CCB_Ready4SPRINT_Date,CCB_Ready4SPRINT_Decision,CCBReady4SPRINT_Remarks,Ready4SprintTestRemarks,
  CCB_Ready4UAT_Date,CCB_Ready4UAT_Decision,CCBReady4UAT_Remarks,Ready4UATTestRemarks,LastRegistrationDate,UAT_START_DATE,UAT_END_DATE,FREEZE_DATE,
  CCB_Ready4PROD_Date,CCB_Ready4PROD_Decision,CCBReady4PROD_Remarks,Ready4ProdTestRemarks,CCB_Closure_Date,CCB_CLOSURE_Decision,CCB_CLOSURE_Remarks,
  Open_Risks_Issues,Project_Manager, unitTestReportPath,automationReportPath , performanceReportPath,securityReportPath ,
  IT_TestReportPath,UAT_TestReportPath ,CCB_GATING_PATH,MASTER_TEST_PLAN_PATH, DetailedTestStatus, Test_Coordinator_email, 
  Project_Manager_email, Scrum_Master_email, Delivery_Master_Lead_email, JIRA_KEY, UserStory_Link, UserStory_Link1, KeyRemarksTesting_Plain , SP_ProjectEditLink,BUSSINESS_GO_PATH , HistoryTestKeyMessage
  from v_project_ccb_details where projectReference ='${projectReference}' and ReleaseName='${releaseName}';`

  sql.getConnection(function (err, connection) {
    connection.query('START TRANSACTION', function (err, rows) {
      var getRecords = connection.query(qry, (error, res) => {
        if (error) {
          console.log("error: ", error);
          result(null, error);
          return;
        }
        console.log(`PROJECTS CCB details for Release : ${releaseName} & Project : ${projectReference} `, res.length);
        result(null, res);
      });
      connection.query('COMMIT', function (err, rows) {
        connection.release();
      });
    });

  });
};



Project.getCCBProjectDetailsForRelease = (releaseName, result) => {

  var qry = `select ReleaseName,ReleaseDate,ProjectReference,ProjectName,test_status,KeyRemarksTesting,Domain,ProjectSize_MDs,
  PROJECTREPOSITORY_Link,JIRA_Link,Story_JIRA_Link,ZEPHYR_Link,Delivery_Master_Lead,Test_Coordinator,Scrum_Master,CCB_STARTUP_Date,
  CCB_STARTUP_Decision,CCB_STARTUP_Remarks ,Ready2StartTestRemarks,CCB_Ready4SPRINT_Date,CCB_Ready4SPRINT_Decision,CCBReady4SPRINT_Remarks,Ready4SprintTestRemarks,
  CCB_Ready4UAT_Date,CCB_Ready4UAT_Decision,CCBReady4UAT_Remarks,Ready4UATTestRemarks,LastRegistrationDate,UAT_START_DATE,UAT_END_DATE,FREEZE_DATE,
  CCB_Ready4PROD_Date,CCB_Ready4PROD_Decision,CCBReady4PROD_Remarks,Ready4ProdTestRemarks,CCB_Closure_Date,CCB_CLOSURE_Decision,CCB_CLOSURE_Remarks,
  Open_Risks_Issues,Project_Manager, unitTestReportPath,automationReportPath , performanceReportPath,securityReportPath ,
  IT_TestReportPath,UAT_TestReportPath , CCB_GATING_PATH,MASTER_TEST_PLAN_PATH,DetailedTestStatus, Test_Coordinator_email, 
  Project_Manager_email, Scrum_Master_email, Delivery_Master_Lead_email,JIRA_KEY, UserStory_Link,UserStory_Link1  , KeyRemarksTesting_Plain, SP_ProjectEditLink,BUSSINESS_GO_PATH ,HistoryTestKeyMessage
   from v_project_ccb_details where ReleaseName='${releaseName}';`
  sql.getConnection(function (err, connection) {
    connection.query('START TRANSACTION', function (err, rows) {
      var getRecords = connection.query(qry, (error, res) => {
        if (error) {
          console.log("error: ", error);
          result(null, error);
          return;
        }
        console.log(`PROJECTS CCB details for Release : ${releaseName}  `, res.length);
        result(null, res);
      });
      connection.query('COMMIT', function (err, rows) {
        connection.release();
      });
    });

  });

};

Project.getTestReportForProject = (releaseName, projectReference, result) => {
  var qry = `select * from test_reports where ProjectReference ='${projectReference}' and ReleaseName='${releaseName}' and TypeOfReport is not null`;
  sql.getConnection(function (err, connection) {
    connection.query('START TRANSACTION', function (err, rows) {
      var getRecords = connection.query(qry, (error, res) => {
        if (error) {
          console.log("error: ", error);
          result(null, error);
          return;
        }
        console.log(`Test Reports : ${releaseName}  `, res.length);
        result(null, res);
      });
      connection.query('COMMIT', function (err, rows) {
        connection.release();
      });
    });

  });

}





module.exports = Project;

;
