const sql = require("./db.js");

const Coverage = function (coverage) {

};

Coverage.getAll = (result) => {

  var selectQry = `SELECT ProjectReference,ProjectName, COUNT(DISTINCT(requirementid)) as count, ReqCoverage as ReqCoverage, RequirementTreeNode ,
  count(reqCoverageStatus) as TestStatusCount ,
            reqCoverageStatus, epicLink, ReleaseName,RequirementTreeSubNode,RequirementTreeSubNode1,RequirementTreeSubNode2,
            RequirementTreeSubNode3,RequirementTreeSubNode4,RequirementTreeSubNode5,RequirementTreeSubNode6,RequirementTreeSubNode7,
            RequirementTreeSubNode8,RequirementTreeSubNode9,RequirementTreeSubNode10,requirementid,defectids as DefectId
              FROM v_req_details_wise_tests_phase
              GROUP by ReleaseName,ProjectReference,ProjectName,requirementid
              ORDER By ReleaseName ,ProjectReference,ProjectName,ReqCoverage,RequirementTreeNode,
                       field(ReqCoverageStatus,"Pass","Fail","Blocked","InComplete","N/A","Not Executed","Not Assigned","Not Covered");`

  sql.getConnection(function (err, connection) {
    connection.query('START TRANSACTION', function (err, rows) {
      var getRecords = connection.query(selectQry, (error, res) => {
        if (error) {
          console.log("error: ", error);
          result(null, error);
          return;
        }
        console.log("Coverage all Results: ", res.length);
        result(null, res);
      });
      connection.query('COMMIT', function (err, rows) {
        connection.release();
      });
    });

  });

};

//  where releaseName='${releaseName}'

Coverage.getByRelease = (releaseName, result) => {
  var selectQry = `SELECT ProjectReference,ProjectName, COUNT(DISTINCT(requirementid)) as count, ReqCoverage as ReqCoverage, RequirementTreeNode ,
  count(reqCoverageStatus) as TestStatusCount ,epicLink,
            reqCoverageStatus,  ReleaseName,RequirementTreeSubNode,RequirementTreeSubNode1,RequirementTreeSubNode2,
            RequirementTreeSubNode3,RequirementTreeSubNode4,RequirementTreeSubNode5,RequirementTreeSubNode6,RequirementTreeSubNode7,
            RequirementTreeSubNode8,RequirementTreeSubNode9,RequirementTreeSubNode10,requirementid,defectids as DefectId
              FROM v_req_details_wise_tests_phase
              WHERE releaseName='${releaseName}'
              GROUP by ReleaseName,ProjectReference,ProjectName,requirementid
              ORDER By ReleaseName ,ProjectReference,ProjectName,ReqCoverage,RequirementTreeNode,
                       field(ReqCoverageStatus,"Pass","Fail","Blocked","InComplete","N/A","Not Executed","Not Assigned","Not Covered");`;

  sql.getConnection(function (err, connection) {
    connection.query('START TRANSACTION', function (err, rows) {
      var getRecords = connection.query(selectQry, (error, res) => {
        if (error) {
          console.log("error: ", error);
          result(null, error);
          return;
        }
        console.log(`Coverage for Release : ${releaseName} `, res.length);
        result(null, res);
      });
      connection.query('COMMIT', function (err, rows) {
        connection.release();
      });
    });

  });
};


Coverage.getByProjects = (releaseName, projectReference, result) => {
  var selectQry = `SELECT ProjectReference,ProjectName, COUNT(DISTINCT(requirementid)) as count, ReqCoverage as ReqCoverage, RequirementTreeNode ,
  count(reqCoverageStatus) as TestStatusCount ,epicLink,
            reqCoverageStatus,  ReleaseName,RequirementTreeSubNode,requirementid,defectids as DefectId
              FROM v_req_details_wise_tests_phase
             WHERE  releasename='${releaseName}' and  projectReference='${projectReference}' 
             GROUP by ReleaseName,ProjectReference,ProjectName,requirementid
             ORDER By ReleaseName ,ProjectReference,ProjectName,ReqCoverage,RequirementTreeNode,
                      field(ReqCoverageStatus,"Pass","Fail","Blocked","InComplete","N/A","Not Executed", "Not Assigned","Not Covered");`;

  sql.getConnection(function (err, connection) {
    connection.query('START TRANSACTION', function (err, rows) {
      var getRecords = connection.query(selectQry, (error, res) => {
        if (error) {
          console.log("error: ", error);
          result(null, error);
          return;
        }
        console.log(`Coverage Group By Project : ${projectReference} `, res.length);
        result(null, res);
      });
      connection.query('COMMIT', function (err, rows) {
        connection.release();
      });
    });
  });
};


module.exports = Coverage;