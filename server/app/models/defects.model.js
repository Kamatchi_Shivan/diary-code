const sql = require('./db')

const Defects = function () {

};

Defects.allDefects = (result) => {

    let qry = `SELECT  count(*) as count ,projectKey, projectReference,ReleaseName, projectName,projectCategory,priority,createdOn,resolution,defectSevirity,summary,product,testTypes,
                       testLevelDetected,createdUserName,priority,assigneeName,assigneeID,status,severity,component ,  sprintId,defectKey,issueType,epicLink,epicName
               FROM v_defects_data  
               GROUP BY projectName,projectKey, priority,projectCategory,priority,resolution,defectSevirity,product,testTypes,
                        testLevelDetected,createdUserName,priority,assigneeName,assigneeID,status,severity,component , sprintId,defectKey,issueType,epicLink,epicName
               Order by field (status,'Open','Development','In Test','Analysis','To Validate','Closed','BackLog') ;`;

    sql.getConnection(function (err, connection) {
        connection.query('START TRANSACTION', function (err, rows) {
            var getRecords = connection.query(qry, (error, res) => {
                if (error) {
                    console.log("error: ", error);
                    result(null, error);
                    return;
                }
                console.log("Group By Project: ", res.length);
                result(null, res);
            });
            connection.query('COMMIT', function (err, rows) {
                connection.release();
            });
        });

    });
};


Defects.defectsByRelease = (releaseName, result) => {

    let selectQry = `SELECT  count(*) as count ,projectKey, projectReference,ReleaseName, projectName,projectCategory,priority,createdOn,resolution,defectSevirity,summary,product,testTypes,
                       testLevelDetected,createdUserName,priority,assigneeName,assigneeID,status,severity,component ,  sprintId,defectKey,issueType,epicLink,epicName
               FROM v_defects_data  
               WHERE ReleaseName='${releaseName}'
               GROUP BY ReleaseName,projectName,projectKey, priority,projectCategory,priority,resolution,defectSevirity,product,testTypes,
                        testLevelDetected,createdUserName,priority,assigneeName,assigneeID,status,severity,component , sprintId,defectKey,issueType,epicLink,epicName
               Order by field (status,'Open','Development','In Test','Analysis','To Validate','Closed','BackLog') ;`;


    sql.getConnection(function (err, connection) {
        connection.query('START TRANSACTION', function (err, rows) {
            var getRecords = connection.query(selectQry, (error, res) => {
                if (error) {
                    console.log("error: ", error);
                    result(null, error);
                    return;
                }
                console.log("Defects By Release: ", res.length);
                result(null, res);
            });
            connection.query('COMMIT', function (err, rows) {
                connection.release();
            });
        });

    });
};



Defects.byStatus = (result) => {
    let selectQry = `SELECT projectName, projectReference,ReleaseName,status,count(status) as count ,priority, count(priority),
                      assigneeName,assigneeID, sprintId ,component,product ,testTypes, testLevelDetected,defectKey,issueType,epicLink,epicName
               FROM v_defects_data 
               GROUP BY status,priority,projectName,  projectReference, assigneeName,assigneeID, sprintId, component,product ,testTypes, testLevelDetected,defectKey,issueType,epicLink,epicName 
               ORDER by projectName ,field (status,'Open','Development','In Test','Analysis','To Validate','Closed','BackLog')  ,priority;`;


    sql.getConnection(function (err, connection) {
        connection.query('START TRANSACTION', function (err, rows) {
            var getRecords = connection.query(selectQry, (error, res) => {
                if (error) {
                    console.log("error: ", error);
                    result(null, error);
                    return;
                }
                console.log("Group By Project: ", res.length);
                result(null, res);
            });
            connection.query('COMMIT', function (err, rows) {
                connection.release();
            });
        });

    });

};

Defects.byComponent = (result) => {
    let qry = `SELECT projectName, projectReference,ReleaseName, component ,count(epicName) as count,priority ,count(priority), projectName ,testTypes, testLevelDetected ,status,defectKey,issueType,epicLink,epicName
               FROM v_defects_data
               GROUP BY epicLink,epicName , priority ,  projectName, projectReference, testTypes, testLevelDetected ,status,defectKey,issueType,epicLink,epicName
               ORDER BY projectName ,epicLink,epicName,priority ; `;
    sql.getConnection(function (err, connection) {
        connection.query('START TRANSACTION', function (err, rows) {
            var getRecords = connection.query(qry, (error, res) => {
                if (error) {
                    console.log("error: ", error);
                    result(null, error);
                    return;
                }
                console.log("Group By Component: ", res.length);
                result(null, res);
            });
            connection.query('COMMIT', function (err, rows) {
                connection.release();
            });
        });

    });

};

Defects.byAssignee = (result) => {
    let qry = `SELECT  projectName, projectReference,assigneeName,assigneeID,ReleaseName ,count(assigneeName) as count ,Priority,count(priority), sprintId ,component,product ,testTypes, testLevelDetected,status,defectKey,issueType,epicLink,epicName
               FROM v_defects_data 
               GROUP BY assigneeName,priority, projectName, projectReference, sprintId ,component,product ,testTypes, testLevelDetected,status,defectKey,issueType,epicLink,epicName
               ORDER BY projectName,assigneeName,priority;`;

    sql.getConnection(function (err, connection) {
        connection.query('START TRANSACTION', function (err, rows) {
            var getRecords = connection.query(qry, (error, res) => {
                if (error) {
                    console.log("error: ", error);
                    result(null, error);
                    return;
                }
                console.log("Group By Assignee: ", res.length);
                result(null, res);
            });
            connection.query('COMMIT', function (err, rows) {
                connection.release();
            });
        });

    });
};

Defects.byDomain = (result) => {
    // let qry = `SELECT projectName, projectReference,domainName,ReleaseName,count(domainName) as count ,priority,count(priority),status,testTypes, testLevelDetected ,issueType,epicLink,epicName
    //            FROM v_defects_data 
    //            GROUP BY domainName , priority , projectName, projectReference, testTypes, testLevelDetected,issueType
    //            ORDER BY  projectName,domainName,priority;`;
    let qry = `SELECT  projectName, projectReference,domainName,assigneeID,ReleaseName ,count(domainName) as count ,Priority,count(priority), sprintId ,component,product ,testTypes, testLevelDetected,status,defectKey,issueType,epicLink,epicName
    FROM v_defects_data 
    GROUP BY domainName,priority, projectName, projectReference, sprintId ,component,product ,testTypes, testLevelDetected,status,defectKey,issueType,epicLink,epicName
    ORDER BY projectName,domainName,priority;`;



    sql.getConnection(function (err, connection) {
        connection.query('START TRANSACTION', function (err, rows) {
            var getRecords = connection.query(qry, (error, res) => {
                if (error) {
                    console.log("error: ", error);
                    result(null, error);
                    return;
                }
                console.log("Group by Domain: ", res.length);
                result(null, res);
            });
            connection.query('COMMIT', function (err, rows) {
                connection.release();
            });
        });

    });
};

Defects.getByProjects = (releaseName,projectReference, result) => {

    var selectQry = `SELECT distinct epicLink,epicName,epicStatus from v_defects_data where projectReference=${projectReference} and ReleaseName = "${releaseName}" and epicLink != "Un-Defined"`;
  
    sql.getConnection(function (err, connection) {
      connection.query('START TRANSACTION', function (err, rows) {
        var getRecords = connection.query(selectQry, (error, res) => {
          if (error) {
            console.log("error: ", error);
            result(null, error);
            return;
          }
          console.log("Epic Links for project: ", res.length);
          result(null, res);
        });
        connection.query('COMMIT', function (err, rows) {
          connection.release();
        });
      });
  
    });
  
  };


module.exports = Defects;