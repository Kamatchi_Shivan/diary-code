const sql = require("./db.js");

const Story = function (story) {
};



Story.getAll = (result) => {
  sql.query(`select  fixVersions, ProjectKey,ProjectName, count(ProjectName) as ProjectCount,ProjectReference,storystatus,
              count(storystatus) as statusCount  ,userstoryID  ,storyKey,JIRA_KEY,assigneeName,
              count(userstoryID) as storyCount ,resolution,priority,component,EpicLink,epicName,epicStatus,issueType ,summary
              from v_user_story
              group by fixversions,projectReference,storyStatus,component,userstoryID; `,
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      console.log("All Test Execution Results: ", res.length);
      result(null, res);
    }
  );
};


Story.getByRelease = (releaseName, result) => {
  sql.query(`select  fixVersions, ProjectKey,ProjectName, count(ProjectName) as ProjectCount,ProjectReference,storystatus,
              count(storystatus) as statusCount  ,userstoryID  ,storyKey,JIRA_KEY,assigneeName,
              count(userstoryID) as storyCount ,resolution,priority,component,EpicLink,epicName,epicStatus,issueType, summary
              from v_user_story
              where fixVersions  = '${releaseName}'
              group by fixversions,projectReference,storyStatus,component,userstoryID; `,
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      console.log(`User Story for Release : ${releaseName} `, res.length);
      result(null, res);
    }
  );
};


Story.getByProjects = (releaseName, projectReference, result) => {
  sql.query(`select  fixVersions, ProjectKey,ProjectName, count(ProjectName) as ProjectCount,ProjectReference,storystatus,
              count(storystatus) as statusCount  ,userstoryID  ,JIRA_KEY,
              count(userstoryID) as storyCount ,resolution,priority,component,EpicLink,epicName,epicStatus,issueType , summary
              from v_user_story
              where fixVersions  = '${releaseName}' and  projectReference='${projectReference}'
              group by fixversions,projectReference,storyStatus,component,userstoryID; `,
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      console.log(`User Story Group By Project : ${projectReference} `, res.length);
      result(null, res);
    }
  );
};

Story.getEpicsByProjects = (releaseName,projectReference, result) => {

  var selectQry = `SELECT distinct epicLink,epicName,epicStatus from v_user_story where ProjectReference=${projectReference} and ReleaseName = "${releaseName}" and epicLink != "Un-Defined"`;

  sql.getConnection(function (err, connection) {
    connection.query('START TRANSACTION', function (err, rows) {
      var getRecords = connection.query(selectQry, (error, res) => {
        if (error) {
          console.log("error: ", error);
          result(null, error);
          return;
        }
        console.log("Epic Links for project: ", res.length);
        result(null, res);
      });
      connection.query('COMMIT', function (err, rows) {
        connection.release();
      });
    });

  });

};


module.exports = { UserStory: Story, };


