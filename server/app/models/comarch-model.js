const sql = require('./db')

const ComarchJira = function () {


};

ComarchJira.allDefects = (result) => {

    let qry = `SELECT  count(*) as count ,projectKey, projectReference,ReleaseName, projectName,projectCategory,priority,createdOn,resolution,summary,
                       createdUserName,assigneeName,assigneeID,status,severity,component ,defectKey,issueType,epicLink
               FROM v_comarchjira_defects  
               GROUP BY projectName,projectKey, priority,projectCategory,
                        createdUserName,assigneeName,assigneeID,status,severity,component ,defectKey,issueType,epicLink
               Order by field (status,'Open','Development','In Test','Analysis','To Validate','Closed','BackLog') ;`;

    sql.getConnection(function (err, connection) {
        connection.query('START TRANSACTION', function (err, rows) {
            var getRecords = connection.query(qry, (error, res) => {
                if (error) {
                    console.log("error: ", error);
                    result(null, error);
                    return;
                }
                console.log("Group By Project: ", res.length);
                result(null, res);
            });
            connection.query('COMMIT', function (err, rows) {
                connection.release();
            });
        });

    });
};


ComarchJira.defectsByRelease = (releaseName, result) => {

    let selectQry = `SELECT  count(*) as count ,projectKey, projectReference,fixVersions as ReleaseName, projectName,projectCategory,priority,createdOn,resolution,summary,
    createdUserName,assigneeName,assigneeID,status,severity,component ,defectKey,issueType,epicLink
FROM v_comarchjira_defects 
               WHERE ReleaseName='${releaseName}'
               GROUP BY ReleaseName,projectName,projectKey, priority,projectCategory,
                    createdUserName,assigneeName,assigneeID,status,severity,component ,defectKey,issueType,epicLink
               Order by field (status,'Open','Development','In Test','Analysis','To Validate','Closed','BackLog') ;`;


    sql.getConnection(function (err, connection) {
        connection.query('START TRANSACTION', function (err, rows) {
            var getRecords = connection.query(selectQry, (error, res) => {
                if (error) {
                    console.log("error: ", error);
                    result(null, error);
                    return;
                }
                console.log("Defects By Release: ", res.length);
                result(null, res);
            });
            connection.query('COMMIT', function (err, rows) {
                connection.release();
            });
        });

    });
};



ComarchJira.byStatus = (result) => {
    let selectQry = `SELECT projectName, projectReference,ReleaseName,status,count(status) as count ,priority, count(priority),
                      assigneeName,assigneeID ,component ,defectKey,issueType,epicLink
               FROM v_comarchjira_defects 
               GROUP BY status,priority,projectName,  projectReference, assigneeName,assigneeID, component ,defectKey,issueType,epicLink 
               ORDER by projectName ,field (status,'Open','Development','In Test','Analysis','To Validate','Closed','BackLog')  ,priority;`;


    sql.getConnection(function (err, connection) {
        connection.query('START TRANSACTION', function (err, rows) {
            var getRecords = connection.query(selectQry, (error, res) => {
                if (error) {
                    console.log("error: ", error);
                    result(null, error);
                    return;
                }
                console.log("Group By Project: ", res.length);
                result(null, res);
            });
            connection.query('COMMIT', function (err, rows) {
                connection.release();
            });
        });

    });

};

ComarchJira.byComponent = (result) => {
    let qry = `SELECT projectName, projectReference,ReleaseName, component ,count(epicName) as count,priority ,count(priority), projectName,status,defectKey,issueType,epicLink,epicName
               FROM v_comarchjira_defects
               GROUP BY epicLink ,epicName, priority ,  projectName, projectReference ,status,defectKey,issueType,epicLink
               ORDER BY projectName ,epicLink,epicName,priority ; `;
    sql.getConnection(function (err, connection) {
        connection.query('START TRANSACTION', function (err, rows) {
            var getRecords = connection.query(qry, (error, res) => {
                if (error) {
                    console.log("error: ", error);
                    result(null, error);
                    return;
                }
                console.log("Group By Component: ", res.length);
                result(null, res);
            });
            connection.query('COMMIT', function (err, rows) {
                connection.release();
            });
        });

    });

};

ComarchJira.byAssignee = (result) => {
    let qry = `SELECT  projectName, projectReference,assigneeName,assigneeID,ReleaseName ,count(assigneeName) as count ,Priority,priority,count(priority) ,component ,status,defectKey,issueType,epicLink
               FROM v_comarchjira_defects 
               GROUP BY assigneeName,priority, projectName, projectReference ,component ,status,defectKey,issueType,epicLink
               ORDER BY projectName,assigneeName,priority;`;

    sql.getConnection(function (err, connection) {
        connection.query('START TRANSACTION', function (err, rows) {
            var getRecords = connection.query(qry, (error, res) => {
                if (error) {
                    console.log("error: ", error);
                    result(null, error);
                    return;
                }
                console.log("Group By Assignee: ", res.length);
                result(null, res);
            });
            connection.query('COMMIT', function (err, rows) {
                connection.release();
            });
        });

    });
};

ComarchJira.byDomain = (result) => {
    let qry = `SELECT projectName, projectReference,domainName,ReleaseName,count(domainName) as count ,priority,count(priority),status ,issueType,epicLink
               FROM v_comarchjira_defects 
               GROUP BY domainName , priority , projectName, projectReference,status ,issueType,epicLink
               ORDER BY  projectName,domainName,priority;`;


    sql.getConnection(function (err, connection) {
        connection.query('START TRANSACTION', function (err, rows) {
            var getRecords = connection.query(qry, (error, res) => {
                if (error) {
                    console.log("error: ", error);
                    result(null, error);
                    return;
                }
                console.log("Group by Domain: ", res.length);
                result(null, res);
            });
            connection.query('COMMIT', function (err, rows) {
                connection.release();
            });
        });

    });
};


ComarchJira.getTestResultsByRelease = (releaseName, result) => {
    sql.query(`SELECT count(testStatus) as count,testStatus,ReleaseName,projectName1 as ProjectName,ProjectReference,epicLink,Folder,subFolder,
    subFolder1,subFolder2,subFolder3,subFolder4,subFolder5,subFolder6,subFolder7,subFolder8,subFolder9,subFolder10,Defects,priority ,component
               FROM v_comarchjira_tests  
               WHERE ReleaseName='${releaseName}'
               GROUP BY ReleaseName,teststatus,ProjectName,projectReference,epicLink,Folder,subFolder,
               subFolder1,subFolder2,subFolder3,subFolder4,subFolder5,subFolder6,subFolder7,subFolder8,subFolder9,subFolder10,priority ,component
               ORDER BY  ReleaseName ,ProjectName,epicLink, Folder,subFolder,
               subFolder1,subFolder2,subFolder3,subFolder4,subFolder5,subFolder6,subFolder7,subFolder8,subFolder9,subFolder10,testStatus`,
      (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(null, err);
          return;
        }
        console.log(`PROJECTS for Release : ${releaseName} `, res.length);
        result(null, res);
      }
    );
  };

  ComarchJira.getAllResults = (result) => {
    sql.query(`SELECT TR.ReleaseName,TR.ProjectName,TR.ProjectReference,COUNT(TR.testStatus) as count,TR.testStatus ,TR.domainName ,   
                round(COUNT(TR.testStatus)/TT.totals*100,0) AS resultsInPercentage 
                FROM v_comarchjira_tests TR
                LEFT JOIN ( SELECT SUM(t.totals) as totals , t.releaseName,t.projectName , t.ProjectReference FROM  ( 
                                SELECT releaseName, projectName, COUNT(testStatus) as totals ,ProjectReference 
                                FROM  v_comarchjira_tests TR 
                                WHERE releasename is not null 
                                GROUP BY projectName,releaseName ) as t
                                GROUP by t.releaseName,t.projectName , t.ProjectReference ) TT
                ON TT.releaseName = TR.releaseName and  TT.ProjectReference = TR.ProjectReference
                WHERE TR.releasename is not null 
                GROUP BY TR.ReleaseName,TR.ProjectName,TR.ProjectReference,TR.teststatus ;`,
      (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(null, err);
          return;
        }
        console.log(`All Release Test Results `, res.length);
        result(null, res);
      }
    );
  };


  ComarchJira.getUnquieReleaseandProject = (result) => {
    sql.query(`SELECT DISTINCT fixVersions,projectName,mappedProjectName,ReleaseName,projectReference  from comarch_defects ;`,
      (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(null, err);
          return;
        }
        console.log(`All Release Test Results `, res.length);
        result(null, res);
      }
    );
  };



module.exports = ComarchJira;