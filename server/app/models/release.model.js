const sql = require("./db.js");

const Release = function (release) {
  this.id = release.id;
  this.releaseName = release.releaseName;
  this.releaseDate = release.releaseDate;
  this.releaseType = release.releaseType;
};

const ReleaseMileStones = function (releaseMileStones) {
  this.id = releaseMileStones.id;
  this.releaseName = releaseMileStones.releaseName;
  this.releaseDate = releaseMileStones.releaseDate;
  this.releaseTestStatus = releaseMileStones.releaseTestStatus;
  this.testKeyMessage = releaseMileStones.testKeyMessage;
  this.CCB_Ready4Sprint = releaseMileStones.CCB_Ready4Sprint;
  this.CCB_Ready4UAT = releaseMileStones.CCB_Ready4UAT;
  this.UAT_StartDate = releaseMileStones.UAT_StartDate;
  this.End_UAT = releaseMileStones.End_UAT;
  this.Freeze = releaseMileStones.Freeze;
  this.CCB_Ready4PROD = releaseMileStones.CCB_Ready4PROD;
  this.CCB_Closure = releaseMileStones.CCB_Closure;
};

Release.getAll = (result) => {

  var qry = `SELECT ReleaseName as ReleaseName,ReleaseDate , SP_ReleaseEditLink FROM v_release_moments where ReleaseName != "No Release Mapped"
  order by YEAR(ReleaseName) desc,MONTH(ReleaseName) ASC,Date(ReleaseName) ASC`;

  sql.getConnection(function (err, connection) {
    connection.query('START TRANSACTION', function (err, rows) {
      var getRecords = connection.query(qry, (error, res) => {
        if (error) {
          console.log("error: ", error);
          result(null, error);
          return;
        }
        console.log("RELEASES : ", res.length);
        result(null, res);
      });
      connection.query('COMMIT', function (err, rows) {
        connection.release();
      });
    });

  });
};

Release.getAllForSorting = (result) => {

  var qry = `SELECT ReleaseName as ReleaseName,ReleaseDate , SP_ReleaseEditLink FROM v_release_moments order by YEAR(ReleaseDate) ASC,MONTH(ReleaseDate) ASC,Date(ReleaseDate) ASC`;

  sql.getConnection(function (err, connection) {
    connection.query('START TRANSACTION', function (err, rows) {
      var getRecords = connection.query(qry, (error, res) => {
        if (error) {
          console.log("error: ", error);
          result(null, error);
          return;
        }
        console.log("RELEASES : ", res.length);
        result(null, res);
      });
      connection.query('COMMIT', function (err, rows) {
        connection.release();
      });
    });

  });
};

ReleaseMileStones.allMileStones = (result) => {
  var qry = `SELECT ReleaseName as ReleaseName,ReleaseDate , SP_ReleaseEditLink FROM v_release_moments order by YEAR(ReleaseName) desc,MONTH(ReleaseName) ASC`;

  sql.getConnection(function (err, connection) {
    connection.query('START TRANSACTION', function (err, rows) {
      var getRecords = connection.query(qry, (error, res) => {
        if (error) {
          console.log("error: ", error);
          result(null, error);
          return;
        }
        console.log("RELEASES MILESTONES: ", res.length);
        result(null, res);
      });
      connection.query('COMMIT', function (err, rows) {
        connection.release();
      });
    });

  });
};


ReleaseMileStones.mileStonesByReleaseName = (releaseName, result) => {
  var qry = `SELECT   ReleaseName,  ReleaseDate, ReleaseTestStatus, TestKeyMessage,LastRegistration, Risks_Issues,CCB_Ready4SPRINT, CCB_Ready4UAT
  , UAT_StartDate,  End_UAT, Freeze,  CCB_Ready4PROD ,  CCB_Closure , SP_ReleaseEditLink , HistoryTestKeyMessage FROM v_release_moments where ReleaseName = '${releaseName}'`;

  sql.getConnection(function (err, connection) {
    connection.query('START TRANSACTION', function (err, rows) {
      var getRecords = connection.query(qry, (error, res) => {
        if (error) {
          console.log("error: ", error);
          result(null, error);
          return;
        }
        console.log("RELEASES MILESTONES: ", res.length);
        result(null, res);
      });
      connection.query('COMMIT', function (err, rows) {
        connection.release();
      });
    });

  });
};


module.exports = { Release: Release, ReleaseMileStones: ReleaseMileStones };
