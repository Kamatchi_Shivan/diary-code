const sql = require('./db');

const Datauploads = function (Datauploads) {
    this.jobName = Datauploads.jobName;
    this.jobDescription = Datauploads.jobDescription;
    this.jobUrl = Datauploads.jobUrl;
    this.ManualRuntime = Datauploads.ManualRuntime;
    this.LastRunTime = Datauploads.LastRunTime;

};

Datauploads.getAll = (result) => {
    let selectQry = `select  CONCAT(UCASE(MID(jobName,1,1)),MID(jobName,2)) as jobName,jobDescription,jobUrl,ManualRuntime,LastRunTime from uploadjobs 
    order by field(jobName,"Sharepoint - Release moments", "Sharepoint - project and test status information", "Sharepoint - External Reports","Jira - Defects","Jira - User Stories","Zephyr - Test Requirements mapping","Zephyr - Test Execution results","Upload comarch Defectcs, Story Defects and Test Results");`;
   
    sql.getConnection(function (err, connection) {
        connection.query('START TRANSACTION', function (err, rows) {
          var getRecords = connection.query(selectQry, (error, res) => {
            if (error) {
              console.log("error: ", error);
              result(null, error);
              return;
            }
            console.log("DATA UPLOADS : ", res.length);
            result(null, res);
          });
          connection.query('COMMIT', function (err, rows) {
            connection.release();
          });
        });
    
      });
    
};

module.exports = Datauploads;