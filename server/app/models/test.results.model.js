const sql = require("./db.js");

const TestResults = function (testResults) {
};

const TestReleaseResults = function (testReleaseResults) {
};

TestResults.getAll = (result) => {
  sql.query(`SELECT  ReleaseName,Hierarchy,usergroup,businessDomain, ProjectName,testStatus,count(testStatus) as count ,projectReference,epicLink,Folder,subFolder,
             subFolder1,subFolder2,subFolder3,subFolder4,subFolder5,subFolder6,subFolder7,subFolder8,subFolder9,subFolder10,defects,priority ,component
             FROM v_test_results
             GROUP BY teststatus,ReleaseName,ProjectName,projectReference,epicLink, Folder,subFolder,
             subFolder1,subFolder2,subFolder3,subFolder4,subFolder5,subFolder6,subFolder7,subFolder8,subFolder9,subFolder10,defects ,priority,component
             ORDER BY  ReleaseName ,ProjectName,epicLink, Folder,subFolder,
             subFolder1,subFolder2,subFolder3,subFolder4,subFolder5,subFolder6,subFolder7,subFolder8,subFolder9,subFolder10,testStatus`,
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      console.log("All Test Execution Results: ", res.length);
      result(null, res);
    }
  );
};


TestResults.getByRelease = (releaseName, result) => {
  sql.query(`SELECT count(testStatus) as count,testStatus,ReleaseName,Hierarchy,usergroup,businessDomain,ProjectName,projectReference,epicLink,Folder,subFolder,
  subFolder1,subFolder2,subFolder3,subFolder4,subFolder5,subFolder6,subFolder7,subFolder8,subFolder9,subFolder10,defects,priority ,component
             FROM v_test_results  
             WHERE ReleaseName='${releaseName}'
             GROUP BY ReleaseName,teststatus,ProjectName,projectReference,epicLink,Folder,subFolder,
             subFolder1,subFolder2,subFolder3,subFolder4,subFolder5,subFolder6,subFolder7,subFolder8,subFolder9,subFolder10,defects,priority ,component
             ORDER BY  ReleaseName ,ProjectName,epicLink, Folder,subFolder,
             subFolder1,subFolder2,subFolder3,subFolder4,subFolder5,subFolder6,subFolder7,subFolder8,subFolder9,subFolder10,testStatus`,
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      console.log(`PROJECTS for Release : ${releaseName} `, res.length);
      result(null, res);
    }
  );
};


TestResults.getByProjects = (releaseName, projectReference, result) => {
  sql.query(`SELECT count(testStatus) as count,testStatus,ReleaseName,Hierarchy,usergroup,businessDomain,ProjectName,projectReference,Folder,subFolder,SubFolder1,SubFolder2,SubFolder3,SubFolder4,SubFolder5,SubFolder6,SubFolder7,SubFolder8,SubFolder9,SubFolder10 
            FROM v_test_results 
            WHERE projectReference='${projectReference}' and releasename='${releaseName}'
            GROUP BY teststatus,ReleaseName,ProjectName, projectReference,Folder,subFolder,SubFolder1,SubFolder2,SubFolder3,SubFolder4,SubFolder5,SubFolder6,SubFolder7,SubFolder8,SubFolder9,SubFolder10 
            ORDER BY folder,subfolder,SubFolder1,SubFolder2; `,
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      console.log(`Results Group By Project : ${projectReference} `, res.length);
      result(null, res);
    }
  );
};


TestResults.getByProjectsAndFolders = (releaseName, projectReference, result) => {
  sql.query(`SELECT count(testStatus) as count,testStatus,ReleaseName,Hierarchy,usergroup,businessDomain,ProjectName,projectReference,Folder,subFolder,SubFolder1,SubFolder2 
             FROM v_test_results 
             WHERE projectReference='${projectReference}' and releasename='${releaseName}'
             GROUP BY teststatus,ReleaseName,ProjectName,projectReference,Folder,subFolder,SubFolder1,SubFolder2
             ORDER BY folder,subFolder,SubFolder1,SubFolder2; `,
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      console.log(`Results Group By Project Folders : ${projectReference} `, res.length);
      result(null, res);
    }
  );
};



TestResults.getAllResultsByProjectsAndFolders = (releaseName, result) => {
  sql.query(`SELECT ReleaseName,Hierarchy, ProjectName ,projectReference,count(testStatus) as count,testStatus,Folder,subFolder,SubFolder1,SubFolder2
             FROM v_test_results 
             WHERE releasename='${releaseName}'
             GROUP BY teststatus,ReleaseName,ProjectName,projectReference, folder,subFolder,SubFolder1,SubFolder2 
             ORDER BY ReleaseName ,ProjectName, folder , subFolder,SubFolder1,SubFolder2; `,
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      console.log(`Results Group By Release & Project By Folders : ${releaseName} `, res.length);
      result(null, res);
    }
  );
};


TestReleaseResults.getAllResults = (result) => {
  sql.query(`SELECT TR.ReleaseName,TR.ProjectName,TR.ProjectReference,COUNT(TR.testStatus) as count,TR.testStatus ,TR.domainName ,   
              round(COUNT(TR.testStatus)/TT.totals*100,0) AS resultsInPercentage 
              FROM v_test_results TR
              LEFT JOIN ( SELECT SUM(t.totals) as totals , t.releaseName,t.projectName , t.ProjectReference FROM  ( 
                              SELECT releaseName, projectName, COUNT(testStatus) as totals ,ProjectReference 
                              FROM  v_test_results TR 
                              WHERE releasename is not null 
                              GROUP BY projectName,releaseName ) as t
                              GROUP by t.releaseName,t.projectName , t.ProjectReference ) TT
              ON TT.releaseName = TR.releaseName and  TT.ProjectReference = TR.ProjectReference
              WHERE TR.releasename is not null 
              GROUP BY TR.ReleaseName,TR.ProjectName,TR.ProjectReference,TR.teststatus ;`,
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      console.log(`All Release Test Results `, res.length);
      result(null, res);
    }
  );
};


TestReleaseResults.getResultsByRelease = (releaseName, result) => {
  /**   All Option for Test Execution Results */
  /* let allQry = `SELECT  RT.ReleaseName AS releaseName, 'All' AS projectName, '000000' AS ProjectReference, COUNT(0) AS count, RT.testStatus AS teststatus
   FROM test_results RT
   WHERE RT.ReleaseName ='${releaseName}'
   GROUP BY  RT.ReleaseName,RT.testStatus
 UNION
   SELECT TR.ReleaseName,TR.ProjectName,TR.ProjectReference,count(TR.testStatus) as count,TR.testStatus  
   FROM test_results TR  
   where TR.releasename ='${releaseName}' 
   GROUP BY TR.ReleaseName,TR.ProjectName,TR.ProjectReference,TR.teststatus;` ;   */

  sql.query(`SELECT TR.ReleaseName,TR.ProjectName,TR.ProjectReference,COUNT(TR.testStatus) as count,TR.testStatus ,TR.domainName , 
              ROUND(COUNT(TR.testStatus)/TT.totals*100,0) AS resultsInPercentage 
              FROM v_test_results TR LEFT JOIN ( SELECT SUM(t.totals) as totals , t.releaseName,t.projectName FROM  ( 
                      SELECT releaseName, projectName, COUNT(testStatus) as totals FROM  v_test_results TR 
                      WHERE releasename is not null  AND releaseName='${releaseName}' 
                      GROUP BY projectName,releaseName ) as t) TT
              on TT.releaseName = TR.releaseName 
              WHERE TR.releasename is not null   AND TR.releaseName='${releaseName}'
              GROUP BY TR.ReleaseName,TR.ProjectName,TR.ProjectReference,TR.teststatus ;`,
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      console.log(`Test Results By Release ${releaseName} `, res.length);
      result(null, res);
    }
  );
};


TestReleaseResults.getResultsByReleaseAndProject = (releaseName, projectReference, result) => {
  sql.query(`SELECT TR.ReleaseName,TR.ProjectName,TR.ProjectReference,COUNT(TR.testStatus) as count,TR.testStatus ,TR.domainName , 
             ROUND(COUNT(TR.testStatus)/TT.totals*100,0) AS resultsInPercentage 
             FROM v_test_results TR LEFT JOIN ( SELECT SUM(t.totals) as totals , t.releaseName,t.projectName FROM  ( 
                    SELECT releaseName, projectName, COUNT(testStatus) as totals FROM  v_test_results TR 
                    WHERE releasename is not null  AND releaseName='${releaseName}' AND projectReference ='${projectReference}'
                    GROUP BY projectName,releaseName ) as t) TT
             ON TT.releaseName = TR.releaseName 
             WHERE TR.releasename is not null   AND TR.releaseName='${releaseName}' AND projectReference ='${projectReference}'
             GROUP BY TR.ReleaseName,TR.ProjectName,TR.ProjectReference,TR.teststatus ;`,
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      console.log(`Results By Release ${releaseName} and ProjectReference ${projectReference} `, res.length);
      result(null, res);
    }
  );
};


module.exports = { TestResults: TestResults, TestReleaseResults: TestReleaseResults };