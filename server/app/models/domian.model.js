const sql = require("./db.js");

const Domain = function (domain) {
  this.id = domain.id;
  this.domainName = domain.domainName;
  this.description = domain.description;
};

Domain.create = (newDomain, result) => {
  
  
  sql.query("INSERT INTO domains SET ?", newDomain, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("created domain: ", { id: res.insertId, ...newDomain });
    result(null, { id: res.insertId, ...newDomain });
  });
};

Domain.getAll = (result) => {
  var qry = "SELECT Id, DomainName, Description FROM domains";

  sql.getConnection(function (err, connection) {
    connection.query('START TRANSACTION', function (err, rows) {
      var getRecords = connection.query(qry, (error, res) => {
        if (error) {
          console.log("error: ", error);
          result(null, error);
          return;
        }
        console.log("DOMAINS: ", res.length);
        result(null, res);
      });
      connection.query('COMMIT', function (err, rows) {
        connection.release();
      });
    });

  });
};

module.exports = Domain;
