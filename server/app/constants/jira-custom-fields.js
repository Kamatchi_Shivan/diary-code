export const fields = {
  id: "id",
  key: "key",
  issuetype: "issuetype",
  issuekey: "issuekey",
  project: "project",
  resolution: "resolution",
  status: "status",
  resolutionDate: "resolutiondate",

  priority: "priority",
  labels: "labels",
  assignee: "assignee",

  reference: "customfield_10500",

  product: "customfield_10700",
  severity: "customfield_11101",

  epicLink: "customfield_10006",
  epicName: "customfield_10007",
  epicStatus: "customfield_10008",

  account: "customfield_10200",

  linkedIssues: "issuelinks",
  userStoryId: "customfield_11021",
  bzUserStoryId: "customfield_10945",
  bzModule: "customfield_11013",

  components: "components",

  fixVersions: "fixVersions",
  releaseVersionHistory: "customfield_10101",
  aggregatteOriginalEstimate: "aggregatetimeoriginalestimate",

  remainingEstimate: "timeestimate",

  createdBy: "creator",
  created: "created",
  updated: "updated",

  origin: "customfield_10113",
  stream: "customfield_11159",
  capability: "customfield_11029",
  technicalComplexity: "customfield_11026",
  scrumTeam: "customfield_10600",

  unitTestingExecution: "customfield_10931",
  detectedEnvironment: "customfield_10415",
  defectSevirity: "customfield_10402",

  testType: "customfield_11005",
  testTypes: "customfield_10800",
  testLevelDetected: "customfield_10407",
};
