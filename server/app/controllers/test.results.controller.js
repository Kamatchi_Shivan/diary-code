const Results = require("../models/test.results.model");
const ComarchJira = require("../models/comarch-model")
const log4js = require("log4js");

log4js.configure({
  appenders: { server: { type: "file", filename: "server.log" } },
  categories: { default: { appenders: ["server"], level: "all" } },
});

const logger = log4js.getLogger("server");

/* Retrieve all Test Results from the database */
exports.getAll = (req, res) => {
  Results.TestResults.getAll((err, data) => {
    if (err)
      res.status(500).send({
        status: "Failure",
        messageCode: "MSG500",
        message: "Some error occurred while retrieving Test Results",
        userMessage: "Please check your credentials",
      }),
        logger.error("Some error occurred while retrieving the Test Results");
    else
      res.send({
        status: "SUCCESS",
        messageCode: "MSG200",
        message: "Test Results Data Retrieved",
        userMessage: "Test Results Retrieved Successfully !!!.",
        data,
      });
  });
};

/* Get Test Results for selected release  */
exports.getByRelease = (req, res) => {
  Results.TestResults.getByRelease(req.params.releaseName, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          status: "Failure",
          messageCode: "MSG404",
          message: "No Projects found",
          userMessage: `Projects not found for releaseName : ${req.params.releaseName}`,

        });
        logger.error(`Test Results  not found for releaseName : ${req.params.releaseName}`);
      } else {
        res.status(500).send({
          status: "Failure",
          messageCode: "MSG500",
          message: "Some error occurred while retrieving Test Results list",
          userMessage: "Please check the parsed data",
        });
        logger.error("Some error occurred while retrieving Test Resutls list");
      }
    } else {
      ComarchJira.getTestResultsByRelease(req.params.releaseName, (err, Comarchdata) => {
        if(err){
          res.send({
            status: "SUCCESS",
            messageCode: "MSG200",
            message: "Test Results List Retrieved",
            userMessage: "Test Results List Retrieved Successfully !!!.",
            data,
          });

        }else{
          data = data.concat(Comarchdata);
          res.send({
            status: "SUCCESS",
            messageCode: "MSG200",
            message: "Test Results List Retrieved",
            userMessage: "Test Results List Retrieved Successfully !!!.",
            data,
          });

        }
      });
    
      logger.info("Retrieved Data", data);
    }
  });
};


/* Get Test Results for selected release and project */
exports.getByProjects = (req, res) => {
  Results.TestResults.getByProjects(req.params.releaseName, req.params.projectReference, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          status: "Failure",
          messageCode: "MSG404",
          message: "No Projects found",
          userMessage: `Projects not found for releaseName : ${req.params.releaseName}`,

        });
        logger.error(`Test Results  not found for project : ${req.params.projectReference} releaseName : ${req.params.releaseName}`);
      } else {
        res.status(500).send({
          status: "Failure",
          messageCode: "MSG500",
          message: "Some error occurred while retrieving Test Results list",
          userMessage: "Please check the parsed data",
        });
        logger.error("Some error occurred while retrieving Test Resutls list");
      }
    } else {
      res.send({
        status: "SUCCESS",
        messageCode: "MSG200",
        message: "Test Results List Retrieved",
        userMessage: "Test Results List Retrieved Successfully !!!.",
        data,
      });
      logger.info("Retrieved Data", data);
    }
  });
};


/* Get Test Results for selected release and project */
exports.getByProjectsAndFolders = (req, res) => {
  Results.TestResults.getByProjectsAndFolders(req.params.releaseName, req.params.projectReference, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          status: "Failure",
          messageCode: "MSG404",
          message: "No Projects found",
          userMessage: `Projects not found for releaseName : ${req.params.releaseName}`,

        });
        logger.error(`Test Results  not found for project : ${req.params.projectReference} releaseName : ${req.params.releaseName}`);
      } else {
        res.status(500).send({
          status: "Failure",
          messageCode: "MSG500",
          message: "Some error occurred while retrieving Test Results list",
          userMessage: "Please check the parsed data",
        });
        logger.error("Some error occurred while retrieving Test Resutls list");
      }
    } else {
      res.send({
        status: "SUCCESS",
        messageCode: "MSG200",
        message: "Test Results List Retrieved",
        userMessage: "Test Results List Retrieved Successfully !!!.",
        data,
      });
      logger.info("Retrieved Data", data);
    }
  });
};
/* Get Test Results for selected release Group by Projects  */
exports.getAllResultsByProjectsAndFolders = (req, res) => {
  Results.TestResults.getAllResultsByProjectsAndFolders(req.params.releaseName, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          status: "Failure",
          messageCode: "MSG404",
          message: "No Projects found",
          userMessage: `Projects not found for releaseName : ${req.params.releaseName}`,

        });
        logger.error(`Test Results  not found for releaseName : ${req.params.releaseName}`);
      } else {
        res.status(500).send({
          status: "Failure",
          messageCode: "MSG500",
          message: "Some error occurred while retrieving Test Results list",
          userMessage: "Please check the parsed data",
        });
        logger.error("Some error occurred while retrieving Test Resutls list");
      }
    } else {
      res.send({
        status: "SUCCESS",
        messageCode: "MSG200",
        message: "Test Results List Retrieved",
        userMessage: "Test Results List Retrieved Successfully !!!.",
        data,
      });
      logger.info("Retrieved Data", data);
    }
  });
};

/* Get Test Results for selected release Group by Projects  */
exports.getAllTestResults = (req, res) => {
  Results.TestReleaseResults.getAllResults((err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          status: "Failure",
          messageCode: "MSG404",
          message: "No Projects found",
          userMessage: `Projects not found `,

        });
        logger.error(`Test Results  not found`);
      } else {
        res.status(500).send({
          status: "Failure",
          messageCode: "MSG500",
          message: "Some error occurred while retrieving Test Results list",
          userMessage: "Please check the parsed data",
        });
        logger.error("Some error occurred while retrieving Test Resutls list");
      }
    } else {
      ComarchJira.getAllResults((err, Comarchdata) => {
        if(err){
          res.send({
            status: "SUCCESS",
            messageCode: "MSG200",
            message: "Test Results List Retrieved",
            userMessage: "Test Results List Retrieved Successfully !!!.",
            data,
          });
        }else{
          data = data.concat(Comarchdata);
          res.send({
            status: "SUCCESS",
            messageCode: "MSG200",
            message: "Test Results List Retrieved",
            userMessage: "Test Results List Retrieved Successfully !!!.",
            data,
          });
        }

      });
    
      logger.info("Retrieved Data", data);
    }
  });
};

exports.getResultsByRelease = (req, res) => {
  Results.TestReleaseResults.getResultsByRelease(req.params.releaseName, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          status: "Failure",
          messageCode: "MSG404",
          message: "No Projects found",
          userMessage: `Projects not found `,

        });
        logger.error(`Test Results  not found`);
      } else {
        res.status(500).send({
          status: "Failure",
          messageCode: "MSG500",
          message: "Some error occurred while retrieving Test Results list",
          userMessage: "Please check the parsed data",
        });
        logger.error("Some error occurred while retrieving Test Resutls list");
      }
    } else {
      res.send({
        status: "SUCCESS",
        messageCode: "MSG200",
        message: "Test Results List Retrieved for given release",
        userMessage: "Test Results List Retrieved Successfully !!!.",
        data,
      });
      logger.info("Retrieved Data", data);
    }
  });
};

exports.getResultsByReleaseAndproject = (req, res) => {
  Results.TestReleaseResults.getResultsByReleaseAndProject(req.params.releaseName, req.params.projectReference, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          status: "Failure",
          messageCode: "MSG404",
          message: "No Projects found",
          userMessage: `Projects not found `,

        });
        logger.error(`Test Results  not found`);
      } else {
        res.status(500).send({
          status: "Failure",
          messageCode: "MSG500",
          message: "Some error occurred while retrieving Test Results list",
          userMessage: "Please check the parsed data",
        });
        logger.error("Some error occurred while retrieving Test Resutls list");
      }
    } else {
      res.send({
        status: "SUCCESS",
        messageCode: "MSG200",
        message: "Test Results List Retrieved",
        userMessage: "Test Results List Retrieved Successfully for project and Release !!!.",
        data,
      });
      logger.info("Retrieved Data", data);
    }
  });
};


