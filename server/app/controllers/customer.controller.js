const Customer = require("../models/customer.model.js");

//var logger = require('log4js').getLogger("server");

const log4js = require('log4js');

log4js.configure({
    appenders: { server: { type: 'file', filename: 'server.log' } },
    categories: { default: { appenders: ['server'], level: 'all' } }
});

const logger = log4js.getLogger('server');
// Create and Save a new Customer
exports.create = (req, res) => {
    // Validate request
    if (!req.body) {
        res.status(400).send({
            status: "Failure",
            messageCode: "MSG400",
            message: "No Content found",
            userMessage: "Content cannot be empty for creating a customer"           
        });

        logger.warn('Content cannot be empty for creating a customer');
    }

    // Create a Customer
    const customer = new Customer({
        email: req.body.email,
        name: req.body.name,
        active: req.body.active,
        password: req.body.password
    });

    // Save Customer in the database
    Customer.create(customer, (err, data) => {
        if (err) {
            res.status(500).send({
                status: "Failure",
                messageCode: "MSG500",
                message: "Error in Customer save",
                userMessage: "Please check your credentials"
                }),
                logger.error('Some error occurred while creating the Customer');
        } else {
            res.send({
            status: "SUCCESS",
            messageCode: "MSG200",
            message: "Customer created",
            userMessage: "Customer Added Successfully !!!.",
            data
         });
            logger.info('Customer created successfully', data);
        }


    });
};

// Retrieve all Customers from the database.
exports.findAll = (req, res) => {
    Customer.getAll((err, data) => {
        if (err)
            res.status(500).send({
                status: "Failure",
                messageCode: "MSG500",
                message: "Some error occurred while retrieving customers",
                userMessage: "Please check your credentials"
            }),
            logger.error('Some error occurred while retrieving the Customers');
        else res.send({
            status: "SUCCESS",
            messageCode: "MSG200",
            message: "Customer Data Retrieved",
            userMessage: "Customer Retrieved Successfully !!!.",
            data
         });
    });
};

// Find a single Customer with a customerId
exports.findOne = (req, res) => {
    Customer.findById(req.params.customerId, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                status: "Failure",
                messageCode: "MSG404",
                message: "No Customer found",
                userMessage: "Customer ID not found"
                   // message: `Not found Customer with id ${req.params.customerId}.`
                });
                logger.error('Customer ID not found');
            } else {
                res.status(500).send({
                status: "Failure",
                messageCode: "MSG500",
                message: "Some error occurred while retrieving customers",
                userMessage: "Please check your credentials"
                    //message: "Error retrieving Customer with id " + req.params.customerId
                });
                logger.error('Some error occurred while creating the Customer');
            }
        } else {
            res.send({
            status: "SUCCESS",
            messageCode: "MSG200",
            message: "Customer Data Retrieved",
            userMessage: "Customer Retrieved Successfully !!!.",
            data
            });
            logger.info("Retrieved Data", data);
        }
    });
};

// Update a Customer identified by the customerId in the request
exports.update = (req, res) => {
    // Validate Request
    if (!req.body) {
        res.status(400).send({
                status: "Failure",
                messageCode: "MSG400",
                message: "No Content found",
                userMessage: "Content cannot be empty for updating a customer"
        });
        logger.warn('Content cannot be empty for updating a customer');
    }

    console.log(req.body);

    Customer.updateById(
        req.params.customerId,
        new Customer(req.body),
        (err, data) => {
            if (err) {
                if (err.kind === "not_found") {
                    res.status(404).send({
                        status: "Failure",
                        messageCode: "MSG404",
                        message: "No Customer found",
                        userMessage: "Customer ID not found"
                    });
                    logger.error('Customer Id for updation Not found');
                } else {
                    res.status(500).send({
                        status: "Failure",
                        messageCode: "MSG500",
                        message: "Error in updating",
                        userMessage: "Error in updating Customer ID"
                    });
                    logger.error('Error in updating Customer ID');
                }
            } else {
                res.send({
                    status: "SUCCESS",
                    messageCode: "MSG200",
                    message: "Customer Data Updated",
                    userMessage: "Customer Updated successfully !!!.",
                    data});
                logger.info("Updated successfully");
            }
        }
    );
};

// Delete a Customer with the specified customerId in the request
exports.delete = (req, res) => {
    Customer.remove(req.params.customerId, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    status: "Failure",
                    messageCode: "MSG404",
                    message: "No Customer found",
                    userMessage: "Customer ID not found"                   
                });
                logger.error('Customer Id Not found to Delete');
            } else {
                res.status(500).send({
                    status: "Failure",
                    messageCode: "MSG500",
                    message: "Error in deleting",
                    userMessage: "Could not delete customer"                    
                });
                logger.error('Could not delete customer');
            }
        } else {
            res.send({ 
                status: "SUCCESS",
                messageCode: "MSG200",
                message: "Customer Data deleted",
                userMessage: "Customer was deleted successfully!!!."
                });
            logger.info("Customer Deleted Successfully");
        }
    });
};

// Delete all Customers from the database.
exports.deleteAll = (req, res) => {
    Customer.removeAll((err, data) => {
        if (err)
            res.status(500).send({
                status: "Failure",
                messageCode: "MSG500",
                message: "Error in deleting",
                userMessage: "Some error occurred while removing all customers"               
            }),
            logger.error('Error in deleting multiple customer');

        else res.send({ 
            status: "SUCCESS",
            messageCode: "MSG200",
            message: "Customer Data deleted",
            userMessage: "All Customers were deleted successfully!!!."
            }),
            logger.info("Customers deleted successfully");
    });
};