const Datauploads = require("../models/datauploads.model");
const log4js = require("log4js");

log4js.configure({
  appenders: { server: { type: "file", filename: "server.log" } },
  categories: { default: { appenders: ["server"], level: "all" } },
});

const logger = log4js.getLogger("server");

/* Retrieve all Test Results from the database */
exports.getAlljobs = (req, res) => {
    Datauploads.getAll((err, data) => {
    if (err)
      res.status(500).send({
        status: "Failure",
        messageCode: "MSG500",
        message: "Some error occurred while retrieving Test Results",
        userMessage: "Please check your credentials",
      }),
        logger.error("Some error occurred while retrieving the Test Results");
    else
      res.send({
        status: "SUCCESS",
        messageCode: "MSG200",
        message: "Test Coverage Data Retrieved",
        userMessage: "Test Coverage Retrieved Successfully !!!.",
        data,
      });
  });
};


