const ReleaseService = require("../models/release.model");

const log4js = require("log4js");

log4js.configure({
  appenders: { server: { type: "file", filename: "server.log" } },
  categories: { default: { appenders: ["server"], level: "all" } },
});

const logger = log4js.getLogger("server");


//Converting String to Date Format

DateFormatter = (value) => {
  value = value.substring(0, 10);
  var DateObj = new Date(value);
  var months = DateObj.getMonth() + 1;
  var Date1 = DateObj.getDate();
  var year = DateObj.getFullYear();
  return year + "." + months + "." + Date1;
};

//Returning Year 

YearConverter = (value) => {
  value = value.substring(0, 10);
  var DateObj = new Date(value);
  var year = DateObj.getFullYear();
  return year;
};

//Ordering upcomming release

async function FormattingArray(value) {
  //Current Date
let ts = Date.now();
let date_ob = new Date(ts),
  date = date_ob.getDate(),
  month = date_ob.getMonth() + 1,
  year = date_ob.getFullYear();
let dateTime = year + "." + month + "." + date;
  return new Promise((resolve, reject) => {
    let firstRelease = value[0];
    value.shift();
    value.push(firstRelease);
    if (
      new Date(dateTime).getTime() <=
      new Date(DateFormatter(value[0].ReleaseName)).getTime()
    ) {
      if(YearConverter(value[0].ReleaseName),date_ob.getFullYear()){
        return value;
      } else {
        FormattingArray(value);
      }
    } else {
      FormattingArray(value);
    }
  });
}

//Sorting the arry to filter out NaN issue for Date values
async function SortArrayForNaN(value) {
  return new Promise((resolve, reject) => {
    let firstData = value[0];
    value.shift();
    value.push(firstData);
    if (
      isNaN(new Date(DateFormatter(value[0].ReleaseName)).getTime()) !== true
    ) {
      return resolve(value);
    } else {
      SortArrayForNaN(value);
    }
  });
}

// Retrieve all Customers from the database.

exports.findAll = (req, res) => {
  //Current Date
let ts = Date.now();
let date_ob = new Date(ts),
  date = date_ob.getDate(),
  month = date_ob.getMonth() + 1,
  year = date_ob.getFullYear();
let dateTime = year + "." + month + "." + date;
  ReleaseService.Release.getAll((err, data) => {
    if (err)
      res.status(500).send({
        status: "Failure",
        messageCode: "MSG500",
        message: "Some error occurred while retrieving domains",
        userMessage: "Please check your credentials",
      }),
        logger.error("Some error occurred while retrieving the Release");
    else {
      ReleaseService.Release.getAllForSorting((err, SortData) => {
        if(!err){
          let InitialData = JSON.parse(JSON.stringify(data));
          if (
            isNaN(new Date(DateFormatter(SortData[0].ReleaseName)).getTime()) === true
          ) {
            SortArrayForNaN(SortData);
          }
          console.log(
            "new Date(DateFormatter(SortData[0].ReleaseName))  ",
            new Date(DateFormatter(SortData[0].ReleaseName))
          );
            FormattingArray(SortData);
          let FinalData = {
            releaseData : InitialData,
            currentRelease : SortData[0]
          }
          console.log("Final  ", SortData[0].ReleaseName);
          res.send({
            status: "SUCCESS",
            messageCode: "MSG200",
            message: "Release List Retrieved",
            userMessage: "Release List Retrieved Successfully !!!.",
            FinalData,
          });
        }else{
          res.status(500).send({
            status: "Failure",
            messageCode: "MSG500",
            message: "Some error occurred while retrieving domains",
            userMessage: "Please check your credentials",
          }),
            logger.error("Some error occurred while retrieving the Release");
        }
      });
    }
  });
};

exports.allMileStones = (req, res) => {
  ReleaseService.ReleaseMileStones.allMileStones((err, data) => {
    if (err)
      res.status(500).send({
        status: "Failure",
        messageCode: "MSG500",
        message: "Some error occurred while retrieving domains",
        userMessage: "Please check your credentials",
      }),
        logger.error("Some error occurred while retrieving the Release");
    else
      res.send({
        status: "SUCCESS",
        messageCode: "MSG200",
        message: "Release MileStones Retrieved",
        userMessage: "Release MileStones Retrieved Successfully !!!.",
        data,
      });
  });
};

/* Get milestone details for specific release  */
exports.mileStonesByReleaseName = (req, res) => {
  ReleaseService.ReleaseMileStones.mileStonesByReleaseName(
    req.params.releaseName,
    (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            status: "Failure",
            messageCode: "MSG404",
            message: `Release Milestone Not found for ${req.params.releaseName}.`,
            userMessage: "Release milestones not found",
          });
          logger.error(
            `Release Milestone Not found for ${req.params.releaseName}.`
          );
        } else {
          res.status(500).send({
            status: "Failure",
            messageCode: "MSG500",
            message: "Some error occurred while retrieving milestones",
            userMessage: "Please check the data",
          });
          logger.error("Some error occurred while creating the Customer");
        }
      } else {
        res.send({
          status: "SUCCESS",
          messageCode: "MSG200",
          message: "MileStones Data Retrieved",
          userMessage: "Release MileStones retrived Successfully !!!.",
          data,
        });
        logger.info("Retrieved Data", data);
      }
    }
  );
};
