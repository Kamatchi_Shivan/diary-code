const Customer = require("../models/customer.model.js");
const ReleaseService = require("../models/release.model");
const Project = require("../models/projects.model");
const sql = require("../models/db.js");
const log4js = require("log4js");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

log4js.configure({
  appenders: { server: { type: "file", filename: "server.log" } },
  categories: { default: { appenders: ["server"], level: "all" } },
});

const logger = log4js.getLogger("server");

exports.authentication = (req, res) => {
  if (!req.body) {
    res.json({
      status: "FAILED",
      messageCode: "ERR400",
      message: "No Data given",
      userMessage: "Invalid Credentials",
    });
    logger.warn("Content cannot be empty for authenticating a customer");
  }
  let customer = new Customer({
    email: req.body.email,
    password: req.body.password,
  });

  if (!req.body.email) {
    res.json({
      status: "FAILED",
      messageCode: "ERR411",
      message: "No Email was Provided",
      userMessage:
        "Invalid Credentials , Please check your Username and Password",
    });
    logger.warn("No Email was Provided");
  } else {
    sql.query(
      "select * from customers where email = ?",
      [req.body.email],
      function (error, results) {
        let result = results[0];
        if (error) {
          res.json({ message: error });
          logger.error("Some error occurred while authenticating the Customer");
        } else {
          if (results[0] === undefined) {
            res.json({
              status: "FAILED",
              messageCode: "ERR412",
              message: "Email was not found",
              userMessage:
                "Invalid Credentials , Please check your Username and Password",
            });
            logger.warn("Email was not found");
          } else {
            if (bcrypt.compareSync(customer.password, results[0].password)) {
            // if (customer.password === results[0].password) {
              jwt.sign(
                { customer },
                "3x6le09r0^p",
                { expiresIn: "1d" },
                (err, token) => {
                  let data = {
                    status: "SUCCESS",
                    messageCode: "200",
                    message: "Customer Authenticated Successfully",
                    userMessage: "Successfully Logged In",
                    token: token,
                    email: result.email,
                    name: result.name,
                    // password: result.password,
                    active: result.active,
                    //send response of changepassword
                  };
                  res.json(data);
                }
              );
             // logger.info("Customer Authenticated successfully", result);
            } else {
              res.json({
                status: "FAILED",
                messageCode: "ERR412",
                message: "Password was not correct",
                userMessage:
                  "Invalid Credentials , Please check your Username and Password",
              });
              logger.warn("Password was not correct");
            }
          }
        }
        // res.send(JSON.stringify(results));
      }
    );
  }
};
