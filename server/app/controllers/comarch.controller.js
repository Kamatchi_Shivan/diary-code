const ComarchJira = require("../models/comarch-model");


const log4js = require("log4js");

log4js.configure({
    appenders: { server: { type: "file", filename: "server.log" } },
    categories: { default: { appenders: ["server"], level: "all" } },
});

const logger = log4js.getLogger("server");

