const Coverage = require("../models/coverage-model");
const log4js = require("log4js");

log4js.configure({
  appenders: { server: { type: "file", filename: "server.log" } },
  categories: { default: { appenders: ["server"], level: "all" } },
});

const logger = log4js.getLogger("server");

/* Retrieve all Test Results from the database */
exports.getAll = (req, res) => {
  Coverage.getAll((err, data) => {
    if (err)
      res.status(500).send({
        status: "Failure",
        messageCode: "MSG500",
        message: "Some error occurred while retrieving Test Results",
        userMessage: "Please check your credentials",
      }),
        logger.error("Some error occurred while retrieving the Test Results");
    else
      res.send({
        status: "SUCCESS",
        messageCode: "MSG200",
        message: "Test Coverage Data Retrieved",
        userMessage: "Test Coverage Retrieved Successfully !!!.",
        data,
      });
  });
};

/* Get Test Results for selected release  */
exports.getByRelease = (req, res) => {
  Coverage.getByRelease(req.params.releaseName, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          status: "Failure",
          messageCode: "MSG404",
          message: "No Projects found",
          userMessage: `Test Coverage not found for releaseName : ${req.params.releaseName}`,

        });
        logger.error(`Test Coverage  not found for releaseName : ${req.params.releaseName}`);
      } else {
        res.status(500).send({
          status: "Failure",
          messageCode: "MSG500",
          message: "Some error occurred while retrieving Test Results list",
          userMessage: "Please check the parsed data",
        });
        logger.error("Some error occurred while retrieving Test Resutls list");
      }
    } else {
      res.send({
        status: "SUCCESS",
        messageCode: "MSG200",
        message: "Test Coverage List Retrieved",
        userMessage: "Test Coverage List Retrieved Successfully !!!.",
        data,
      });
      logger.info("Retrieved Data", data);
    }
  });
};


/* Get Test Results for selected release and project */
exports.getByProjects = (req, res) => {
  Coverage.getByProjects(req.params.releaseName, req.params.projectReference, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          status: "Failure",
          messageCode: "MSG404",
          message: "No Projects found",
          userMessage: `Projects not found for releaseName : ${req.params.releaseName}`,

        });
        logger.error(`Test Coverage  not found for project : ${req.params.projectReference} releaseName : ${req.params.releaseName}`);
      } else {
        res.status(500).send({
          status: "Failure",
          messageCode: "MSG500",
          message: "Some error occurred while retrieving Test Results list",
          userMessage: "Please check the parsed data",
        });
        logger.error("Some error occurred while retrieving Test Resutls list");
      }
    } else {
      res.send({
        status: "SUCCESS",
        messageCode: "MSG200",
        message: "Test Coverage List Retrieved",
        userMessage: "Test Coverage List Retrieved Successfully !!!.",
        data,
      });
      logger.info("Retrieved Data", data);
    }
  });
};




/* Retrieve Requirement Coveage STatus for All Release  */
exports.getStatus = (req, res) => {
  Coverage.getStatus((err, data) => {
    if (err)
      res.status(500).send({
        status: "Failure",
        messageCode: "MSG500",
        message: "Some error occurred while retrieving Test Results",
        userMessage: "Please check your credentials",
      }),
        logger.error("Some error occurred while retrieving the Test Results");
    else
      res.send({
        status: "SUCCESS",
        messageCode: "MSG200",
        message: "Requirement Coverage Status Data Retrieved",
        userMessage: "Requirement Coverage Status Retrieved Successfully !!!.",
        data,
      });
  });
};
