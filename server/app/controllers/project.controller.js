const Project = require("../models/projects.model");
const ReleaseService = require("../models/release.model");
const log4js = require("log4js");

log4js.configure({
  appenders: { server: { type: "file", filename: "server.log" } },
  categories: { default: { appenders: ["server"], level: "all" } },
});

const logger = log4js.getLogger("server");

//Converting String to Date Format

DateFormatter = (value) => {
  value = value.substring(0, 10)
  var DateObj = new Date(value);
  var months = DateObj.getMonth() + 1;
  var Date1 = DateObj.getDate();
  var year = DateObj.getFullYear();
  return year + "." + months + "." + Date1;
}

//Returning Year 

YearConverter = (value) => {
  value = value.substring(0, 10);
  var DateObj = new Date(value);
  var year = DateObj.getFullYear();
  return year;
};


//Ordering upcomming release

async function FormattingArray(value) {
  //Current Date
let ts = Date.now();
let date_ob = new Date(ts),
  date = date_ob.getDate(),
  month = date_ob.getMonth() + 1,
  year = date_ob.getFullYear();
let dateTime = year + "." + month + "." + date;
  return new Promise((resolve, reject) => {
    let firstRelease = value[0];
    value.shift();
    value.push(firstRelease);
    if (
      new Date(dateTime).getTime() <=
      new Date(DateFormatter(value[0].ReleaseName)).getTime()
    ) {
      if(YearConverter(value[0].ReleaseName),date_ob.getFullYear()){
        return value;
      } else {
        FormattingArray(value);
      }
    } else {
      FormattingArray(value);
    }
  });
}

//Sorting the arry to filter out NaN issue for Date values
async function SortArrayForNaN(value) {
  return new Promise((resolve, reject) => {
    let firstData = value[0];
    value.shift();
    value.push(firstData);
    if (isNaN(new Date(DateFormatter(value[0].ReleaseName)).getTime()) !== true) {
      return resolve(value);
    } else {
      SortArrayForNaN(value);
    }
  });
}

/* Retrieve all Projects from the database */
exports.findAll = (req, res) => {
  Project.getAll((err, data) => {
    if (err)
      res.status(500).send({
        status: "Failure",
        messageCode: "MSG500",
        message: "Some error occurred while retrieving customers",
        userMessage: "Please check your credentials",
      }),
        logger.error("Some error occurred while retrieving the Customers");
    else
      res.send({
        status: "SUCCESS",
        messageCode: "MSG200",
        message: "Projects Data Retrieved",
        userMessage: "Projects Retrieved Successfully !!!.",
       data,
      });
  });
};

/* Get Projects for selected release */
exports.getProjectByRelease = (req, res) => {
  Project.getByRelease(req.params.releaseName, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          status: "Failure",
          messageCode: "MSG404",
          message: "No Projects found",
          userMessage: `Projects not found for releaseName : ${req.params.releaseName}`,

        });
        logger.error(`Projects not found for releaseName : ${req.params.releaseName}`);
      } else {
        res.status(500).send({
          status: "Failure",
          messageCode: "MSG500",
          message: "Some error occurred while retrieving projects list",
          userMessage: "Please check the parsed data",
        });
        logger.error("Some error occurred while retrieving projects list");
      }
    } else {
      res.send({
        status: "SUCCESS",
        messageCode: "MSG200",
        message: "Projects List Retrieved",
        userMessage: "Projects List Retrieved Successfully !!!.",
        data,
      });
      logger.info("Retrieved Data", data);
    }
  });
};


/* Get Projects for selected release */
exports.getSummaryByRelease = (req, res) => {
  Project.getSummaryByRelease(req.params.releaseName, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          status: "Failure",
          messageCode: "MSG404",
          message: "No Projects found",
          userMessage: `Projects not found for releaseName : ${req.params.releaseName}`,

        });
        logger.error(`Projects not found for releaseName : ${req.params.releaseName}`);
      } else {
        res.status(500).send({
          status: "Failure",
          messageCode: "MSG500",
          message: "Some error occurred while retrieving projects list",
          userMessage: "Please check the parsed data",
        });
        logger.error("Some error occurred while retrieving projects list");
      }
    } else {
      res.send({
        status: "SUCCESS",
        messageCode: "MSG200",
        message: "Projects Summary Retrieved",
        userMessage: "Projects Summary Retrieved Successfully !!!.",
        data,
      });
      logger.info("Retrieved Data", data);
    }
  });
};



/* Get Summary for selected project & release */
exports.getSpecificSummaryByRelease = (req, res) => {
  Project.getSummaryByReleaseAndProject(req.params.releaseName, req.params.projectReference, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          status: "Failure",
          messageCode: "MSG404",
          message: "No Projects found",
          userMessage: `Projects not found for releaseName : ${req.params.releaseName}`,

        });
        logger.error(`Projects not found for releaseName : ${req.params.releaseName}`);
      } else {
        res.status(500).send({
          status: "Failure",
          messageCode: "MSG500",
          message: "Some error occurred while retrieving projects list",
          userMessage: "Please check the parsed data",
        });
        logger.error("Some error occurred while retrieving projects list");
      }
    } else {
      res.send({
        status: "SUCCESS",
        messageCode: "MSG200",
        message: "Projects Summary Retrieved",
        userMessage: "Projects Summary Retrieved Successfully !!!.",
        data,
      });
      logger.info("Retrieved Data", data);
    }
  });
};

exports.pageloadingapi = (req, res) => {
  //Current Date
let ts = Date.now();
let date_ob = new Date(ts),
  date = date_ob.getDate(),
  month = date_ob.getMonth() + 1,
  year = date_ob.getFullYear();
let dateTime = year + "." + month + "." + date;
  ReleaseService.Release.getAllForSorting((err, Responsedata) => {
    if (!err) {
      let ReleaseItem= JSON.parse(JSON.stringify(Responsedata));
      if (isNaN(new Date(DateFormatter(Responsedata[0].ReleaseName)).getTime()) === true) {
        SortArrayForNaN(Responsedata)
      }
      // if (new Date(dateTime).getTime() >= new Date(DateFormatter(Responsedata[0].ReleaseName)).getTime()) {
        FormattingArray(Responsedata);
      // }
      if (Responsedata.length !== 0) {
        ReleaseService.ReleaseMileStones.mileStonesByReleaseName(Responsedata[0].ReleaseName, (err, Milestonedatas) => {
          let Milestonedata = Milestonedatas[0];
          if (!err) {
            
            Project.getByRelease(Responsedata[0].ReleaseName, (err, Projectdata) => {
              if (!err) {
                if (Projectdata.length !== 0) {
                  Project.getSummaryByRelease(Responsedata[0].ReleaseName, (err, Summarydata) => {
                    if (!err) {
                      let FinalData = {
                        ReleaseData:ReleaseItem,
                        currentRelease:Responsedata[0],
                        Projectdata:Projectdata,
                        Summarydata:Summarydata,
                        Milestonedata:Milestonedata
                      };
                      // console.log("FinalData>>>>", FinalData);
                      res.send({
                        status: "SUCCESS",
                        messageCode: "MSG200",
                        message: "Projects Summary Retrieved",
                        userMessage: "Projects Summary Retrieved Successfully !!!.",
                        FinalData,
                      });

                    }
                  });

                }
              }
            }); //project End
          }
        });
      }
      else {
        res.json({
          status: "FAILED",
          messageCode: "ERR412",
          message: "Internal Server Error",
          userMessage:
            "Please load the page again",
        });
        logger.warn("Internal Server Error");

      }
    }
  });
}


exports.getProjectDetailsAndCCB = (req, res) => {
  Project.getProjectDetailsAndCCB(req.params.releaseName, req.params.projectReference, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          status: "Failure",
          messageCode: "MSG404",
          message: "No Projects found",
          userMessage: `Projects not found for releaseName : ${req.params.releaseName}`,

        });
        logger.error(`Projects not found for releaseName : ${req.params.releaseName}`);
      } else {
        res.status(500).send({
          status: "Failure",
          messageCode: "MSG500",
          message: "Some error occurred while retrieving projects ccb list",
          userMessage: "Please check the parsed data",
        });
        logger.error("Some error occurred while retrieving projects list");
      }
    } else {
      res.send({
        status: "SUCCESS",
        messageCode: "MSG200",
        message: "Projects CCB  Retrieved",
        userMessage: "Projects CCB Retrieved Successfully !!!.",
        data,
      });
      logger.info("Retrieved Data", data);
    }
  });
};




/* Get Projects for selected release */
exports.getProjectReleaseHistory = (req, res) => {
  Project.getProjectReleaseHistory(req.params.projectReference, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          status: "Failure",
          messageCode: "MSG404",
          message: "No ReleaseHistory found",
          userMessage: `Release history not found for releaseName : ${req.params.projectReference}`,

        });
        logger.error(`Release history not found for releaseName : ${req.params.projectReference}`);
      } else {
        res.status(500).send({
          status: "Failure",
          messageCode: "MSG500",
          message: "Some error occurred while retrieving projects list",
          userMessage: "Please check the parsed data",
        });
        logger.error("Some error occurred while retrieving Release history list");
      }
    } else {
      res.send({
        status: "SUCCESS",
        messageCode: "MSG200",
        message: "Projects Release history Retrieved",
        userMessage: "Projects Release History Retrieved Successfully !!!.",
        data,
      });
      logger.info("Retrieved Data", data);
    }
  });
};


exports.getCCBProjectDetailsForRelease = (req, res) => {
  Project.getCCBProjectDetailsForRelease(req.params.releaseName, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          status: "Failure",
          messageCode: "MSG404",
          message: "No Projects found",
          userMessage: `Projects not found for releaseName : ${req.params.releaseName}`,

        });
        logger.error(`Projects not found for releaseName : ${req.params.releaseName}`);
      } else {
        res.status(500).send({
          status: "Failure",
          messageCode: "MSG500",
          message: "Some error occurred while retrieving projects ccb list",
          userMessage: "Please check the parsed data",
        });
        logger.error("Some error occurred while retrieving projects list");
      }
    } else {
      res.send({
        status: "SUCCESS",
        messageCode: "MSG200",
        message: "Projects CCB  Retrieved",
        userMessage: "Projects CCB Retrieved Successfully !!!.",
        data,
      });
      logger.info("Retrieved Data", data);
    }
  });
};



exports.getTestReports = (req, res) => {
  Project.getTestReportForProject(req.params.releaseName, req.params.projectReference, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          status: "Failure",
          messageCode: "MSG404",
          message: "No Reports found",
          userMessage: `Reports not found for releaseName : ${req.params.releaseName}`,

        });
        logger.error(`Reports not found for releaseName : ${req.params.releaseName}`);
      } else {
        res.status(500).send({
          status: "Failure",
          messageCode: "MSG500",
          message: "Some error occurred while retrieving Reports ccb list",
          userMessage: "Please check the parsed data",
        });
        logger.error("Some error occurred while retrieving projects list");
      }
    } else {
      res.send({
        status: "SUCCESS",
        messageCode: "MSG200",
        message: "Reports Retrieved",
        userMessage: "Reports CCB Retrieved Successfully !!!.",
        data,
      });
      logger.info("Retrieved Data", data);
    }
  });
};