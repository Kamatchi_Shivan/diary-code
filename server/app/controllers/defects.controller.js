const Defects = require("../models/defects.model");
const Story = require('../models/story.model');
const ComarchJira = require('../models/comarch-model');

const log4js = require("log4js");

log4js.configure({
    appenders: { server: { type: "file", filename: "server.log" } },
    categories: { default: { appenders: ["server"], level: "all" } },
});

const logger = log4js.getLogger("server");


/* Retrieve all Defects Count from the database.  */
exports.allDefects = (req, res) => {
    Defects.allDefects((err, data) => {
        if (err)
            res.status(500).send({
                status: "Failure",
                messageCode: "MSG500",
                message: "Some error occurred while retrieving all Defects",
                userMessage: "Some error occurred while retrieving all Defects",
            }),
                logger.error("Some error occurred while retrieving the Domain");
        else {
            ComarchJira.allDefects((err, Comarchdata) => {
                if (err)
                    res.send({
                        status: "SUCCESS",
                        messageCode: "MSG200",
                        message: "Defects List Retrieved",
                        userMessage: "Defects List Retrieved Successfully !!!.",
                        data,
                    });
                else {
                    data = data.concat(Comarchdata)
                    res.send({
                        status: "SUCCESS",
                        messageCode: "MSG200",
                        message: "Defects List Retrieved",
                        userMessage: "Defects List Retrieved Successfully !!!.",
                        data,
                    });
                }

            });
        }
    });
};


/* Retrieve defects Count by release from the database.  */
exports.defectsByRelease = (req, res) => {
    Defects.defectsByRelease(req.params.releaseName, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    status: "Failure",
                    messageCode: "MSG404",
                    message: "No Projects found",
                    userMessage: `Defects not found for releaseName : ${req.params.releaseName}`,
                });
                logger.error(`Defects not found for releaseName : ${req.params.releaseName}`);
            } else {
                res.status(500).send({
                    status: "Failure",
                    messageCode: "MSG500",
                    message: "Some error occurred while retrieving all Defects",
                    userMessage: "Some error occurred while retrieving all Defects",
                }),
                    logger.error("Some error occurred while retrieving the Domain");

            }
        } else {
            ComarchJira.defectsByRelease(req.params.releaseName, (err, Comarchdata) => {
                if (err)
                    res.send({
                        status: "SUCCESS",
                        messageCode: "MSG200",
                        message: "Defects List Retrieved",
                        userMessage: "Defects List Retrieved Successfully !!!.",
                        data,
                    });
                else {
                    data = data.concat(Comarchdata)
                    res.send({
                        status: "SUCCESS",
                        messageCode: "MSG200",
                        message: "Defects List Retrieved",
                        userMessage: "Defects List Retrieved Successfully !!!.",
                        data,
                    });
                }

            });
        }
    });
};

/* Retrieve Defects - Group By Status Count from the database.  */
exports.byStatus = (req, res) => {
    Defects.byStatus((err, data) => {
        if (err)
            res.status(500).send({
                status: "Failure",
                messageCode: "MSG500",
                message: "Some error occurred while retrieving domains",
                userMessage: "Please check your credentials",
            }),
                logger.error("Some error occurred while retrieving the Domain");
        else {
            ComarchJira.byStatus((err, Comarchdata) => {
                if (err)
                    res.send({
                        status: "SUCCESS",
                        messageCode: "MSG200",
                        message: "Defects ByStatus List Retrieved",
                        userMessage: "Defects By Status Retrieved Successfully !!!.",
                        data,
                    });
                else {
                    data = data.concat(Comarchdata)
                    res.send({
                        status: "SUCCESS",
                        messageCode: "MSG200",
                        message: "Defects ByStatus List Retrieved",
                        userMessage: "Defects By Status Retrieved Successfully !!!.",
                        data,
                    });
                }

            });
        }

    });
};


/* Retrieve Defects By Components.  */
exports.byComponent = (req, res) => {
    Defects.byComponent((err, data) => {
        if (err)
            res.status(500).send({
                status: "Failure",
                messageCode: "MSG500",
                message: "Some error occurred while retrieving domains",
                userMessage: "Please check your credentials",
            }),
                logger.error("Some error occurred while retrieving the Domain");
        else {
            ComarchJira.byComponent((err, Comarchdata) => {
                if (err)
                    res.send({
                        status: "SUCCESS",
                        messageCode: "MSG200",
                        message: "Defects Group By Component List Retrieved",
                        userMessage: "Defects List Retrieved Successfully !!!.",
                        data,
                    });
                else {
                    data = data.concat(Comarchdata)
                    res.send({
                        status: "SUCCESS",
                        messageCode: "MSG200",
                        message: "Defects Group By Component List Retrieved",
                        userMessage: "Defects List Retrieved Successfully !!!.",
                        data,
                    });
                }

            });
        }
    });
};

/* Retrieve defects By assigneee Count from the database.  */
exports.byAssignee = (req, res) => {
    Defects.byAssignee((err, data) => {
        if (err)
            res.status(500).send({
                status: "Failure",
                messageCode: "MSG500",
                message: "Some error occurred while retrieving domains",
                userMessage: "Please check your credentials",
            }),
                logger.error("Some error occurred while retrieving the Domain");
        else {
            ComarchJira.byAssignee((err, Comarchdata) => {
                if (err)
                    res.send({
                        status: "SUCCESS",
                        messageCode: "MSG200",
                        message: "Defects List Group By Assignee Retrieved",
                        userMessage: "Defects List Group By Assignee Retrieved Successfully !!!.",
                        data,
                    });
                else {
                    data = data.concat(Comarchdata)
                    res.send({
                        status: "SUCCESS",
                        messageCode: "MSG200",
                        message: "Defects List Group By Assignee Retrieved",
                        userMessage: "Defects List Group By Assignee Retrieved Successfully !!!.",
                        data,
                    });
                }

            });
        }
    });
};

/* Retrieve defects By Domain Count from the database.  */
exports.byDomain = (req, res) => {
    Defects.byDomain((err, data) => {
        if (err)
            res.status(500).send({
                status: "Failure",
                messageCode: "MSG500",
                message: "Some error occurred while retrieving domains",
                userMessage: "Please check your credentials",
            }),
                logger.error("Some error occurred while retrieving the Domain");
        else {
             ComarchJira.byDomain((err, Comarchdata) => {
                if (err)
                    res.send({
                        status: "SUCCESS",
                        messageCode: "MSG200",
                        message: "Defects List ByDomain Retrieved",
                        userMessage: "Defects List By Domain Retrieved Successfully !!!.",
                        data,
                    });
                else {
                    data = data.concat(Comarchdata)
                    res.send({
                        status: "SUCCESS",
                        messageCode: "MSG200",
                        message: "Defects List ByDomain Retrieved",
                        userMessage: "Defects List By Domain Retrieved Successfully !!!.",
                        data,
                    });
                }

            });
           
        }
    });
};

/*Get Epic Link by project and releasewise */

exports.EpicByReleaseAndProject = (req, res) => {
    Defects.getByProjects(req.params.releaseName, req.params.projectReference, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    status: "Failure",
                    messageCode: "MSG404",
                    message: "No Projects found",
                    userMessage: `Defects not found for releaseName : ${req.params.releaseName}`,
                });
                logger.error(`Defects not found for releaseName : ${req.params.releaseName}`);
            } else {
                res.status(500).send({
                    status: "Failure",
                    messageCode: "MSG500",
                    message: "Some error occurred while retrieving Epic Link",
                    userMessage: "Some error occurred while retrieving Epic Link",
                }),
                    logger.error("Some error occurred while retrieving the Epic Link");

            }
        } else
            Story.UserStory.getEpicsByProjects(req.params.releaseName, req.params.projectReference, (err, EpicDatadata) => {
                if (err) {
                    if (err.kind === "not_found") {
                        res.status(404).send({
                            status: "Failure",
                            messageCode: "MSG404",
                            message: "No Projects found",
                            userMessage: `Defects not found for releaseName : ${req.params.releaseName}`,
                        });
                        logger.error(`Defects not found for releaseName : ${req.params.releaseName}`);
                    } else {
                        res.status(500).send({
                            status: "Failure",
                            messageCode: "MSG500",
                            message: "Some error occurred while retrieving Epic Link",
                            userMessage: "Some error occurred while retrieving Epic Link",
                        }),
                            logger.error("Some error occurred while retrieving the Epic Link");

                    }
                } else {
                    let FinalData = data.concat(EpicDatadata);
                    const ids = FinalData.map(o => o.epicLink)
                    const filtered = FinalData.filter(({ epicLink }, index) => !ids.includes(epicLink, index + 1))
                    res.send({
                        status: "SUCCESS",
                        messageCode: "MSG200",
                        message: "Epic Link List Retrieved",
                        userMessage: "Epic Link List Retrieved Successfully !!!.",
                        data: filtered,
                    })

                }

            });

    });
};


exports.defectsByProject = (req, res) => {

}
