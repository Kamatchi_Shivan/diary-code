const jwt = require("jsonwebtoken");

function tokenValidator(req, res, next) {
    const token = req.headers.token;
    if (req.originalUrl !== '/authenticate') {
        jwt.verify(token, '3x6le09r0^p', function(err, decoded) {
            if (err) {
                res.status(401).send({ error: 'Unauthorized access' })
            } else {
                next();
            }
        });
    } else {
        next();
    }
}

module.exports = tokenValidator;