const Domain = require("../models/domian.model.js");

const log4js = require("log4js");

log4js.configure({
  appenders: { server: { type: "file", filename: "server.log" } },
  categories: { default: { appenders: ["server"], level: "all" } },
});

const logger = log4js.getLogger("server");

exports.create = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      status: "Failure",
      messageCode: "MSG400",
      message: "No Content found",
      userMessage: "Content cannot be empty for creating a domain",
    });

    logger.warn("Content cannot be empty for creating a domain");
  }

  /** Create a Domain */

  const domain = new Domain({
    id: req.body.id,
    domainName: req.body.domainName,
    description: req.body.description,
  });

  /* Save Domain in the database  */
  Domain.create(domain, (err, data) => {
    if (err) {
      res.status(500).send({
        status: "Failure",
        messageCode: "MSG500",
        message: "Error in Domain save",
        userMessage: "Please check your credentials",
      }),
        logger.error("Some error occurred while creating the Domain");
    } else {
      res.send({
        status: "SUCCESS",
        messageCode: "MSG200",
        message: "Domain created",
        userMessage: "Domain Added Successfully !!!.",
        data,
      });
      logger.info("Domain created successfully", data);
    }
  });
};

// Retrieve all Customers from the database.
exports.findAll = (req, res) => {
  Domain.getAll((err, data) => {
    if (err)
      res.status(500).send({
        status: "Failure",
        messageCode: "MSG500",
        message: "Some error occurred while retrieving domains",
        userMessage: "Please check your credentials",
      }),
        logger.error("Some error occurred while retrieving the Domain");
    else
      res.send({
        status: "SUCCESS",
        messageCode: "MSG200",
        message: "Domains List Retrieved",
        userMessage: "Domains List Retrieved Successfully !!!.",
        data,
      });
  });
};
