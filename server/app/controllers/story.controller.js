const Story = require("../models/story.model");
const log4js = require("log4js");

log4js.configure({
  appenders: { server: { type: "file", filename: "server.log" } },
  categories: { default: { appenders: ["server"], level: "all" } },
});

const logger = log4js.getLogger("server");

/* Retrieve all Test Results from the database */
exports.getAll = (req, res) => {
  Story.UserStory.getAll((err, data) => {
    if (err)
      res.status(500).send({
        status: "Failure",
        messageCode: "MSG500",
        message: "Some error occurred while retrieving Test Results",
        userMessage: "Please check your credentials",
      }),
        logger.error("Some error occurred while retrieving the Test Results");
    else
      res.send({
        status: "SUCCESS",
        messageCode: "MSG200",
        message: "User Story Data Retrieved",
        userMessage: "User Story Retrieved Successfully !!!.",
        data,
      });
  });
};


exports.getByRelease = (req, res) => {
  Story.UserStory.getByRelease(req.params.releaseName, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          status: "Failure",
          messageCode: "MSG404",
          message: "No User Story found",
          userMessage: `USer Story not found for releaseName : ${req.params.releaseName}`,

        });
        logger.error(`User Story  not found for releaseName : ${req.params.releaseName}`);
      } else {
        res.status(500).send({
          status: "Failure",
          messageCode: "MSG500",
          message: "Some error occurred while retrieving Test Results list",
          userMessage: "Please check the parsed data",
        });
        logger.error("Some error occurred while retrieving Test Resutls list");
      }
    } else {
      res.send({
        status: "SUCCESS",
        messageCode: "MSG200",
        message: "User Story List Retrieved",
        userMessage: "User Story  List Retrieved Successfully !!!.",
        data,
      });
      logger.info("Retrieved Data", data);
    }
  });
};



exports.getByProjects = (req, res) => {
  Story.UserStory.getByProjects(req.params.releaseName, req.params.projectReference, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          status: "Failure",
          messageCode: "MSG404",
          message: "No User Story  found",
          userMessage: `User Story  not found for releaseName : ${req.params.releaseName}`,

        });
        logger.error(`Test Results  not found for project : ${req.params.projectReference} releaseName : ${req.params.releaseName}`);
      } else {
        res.status(500).send({
          status: "Failure",
          messageCode: "MSG500",
          message: "Some error occurred while retrieving Test Results list",
          userMessage: "Please check the parsed data",
        });
        logger.error("Some error occurred while retrieving Test Resutls list");
      }
    } else {
      res.send({
        status: "SUCCESS",
        messageCode: "MSG200",
        message: "User Story  Results List Retrieved",
        userMessage: "User Story  Results List Retrieved Successfully !!!.",
        data,
      });
      logger.info("Retrieved Data", data);
    }
  });
};
