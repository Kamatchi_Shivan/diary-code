var nodemailer = require("nodemailer");
const { PDFDocument, rgb, PDFName, PDFString, StandardFonts } = require("pdf-lib");
const fs = require("fs");
var spauth = require('node-sp-auth');
var requestprom = require('request-promise');
var sprequest = require('sp-request');
var config = require("../config/sharepointconig");
const credentialOptions = { clientId: config.clientId, clientSecret: config.clientSecret };

let spr = sprequest.create(credentialOptions);

exports.pdfMerger = (req, res) => {
  try {
    (async () => {
      const initialPage = await PDFDocument.load(req.body.object);
      const cover = await PDFDocument.load(req.body.object1);
      const content = await PDFDocument.load(req.body.object2);
      const doc = await PDFDocument.create();
      const contentPages = await doc.copyPages(
        initialPage,
        initialPage.getPageIndices()
      );
      for (const page of contentPages) {
        doc.addPage(page);
      }

      const contentPages1 = await doc.copyPages(cover, cover.getPageIndices());
      for (const page of contentPages1) {
        doc.addPage(page);
      }
      const contentPages2 = await doc.copyPages(
        content,
        content.getPageIndices()
      );
      for (const page of contentPages2) {
        doc.addPage(page);
      }
      if (req.body.externalURL.length !== 0) {
        returingExternalFile(req.body.externalURL).then(async response => {
          if (response.length !== 0) {
            for (let a = 0; a < response.length; a++) {
              if (response[a].file.includes('pdf')) {
                const aux = await PDFDocument.load(response[a].data);
                if (a === 0) {
                  const firstPage = aux.getPage(0);
                  const { width, height } = firstPage.getSize();
                  // firstPage.moveTo(72, 800);
                  firstPage.drawText(`Annex: ${response[a].type}`, {
                    x: width / 2 - (response[a].type.length * 10),
                    y: height -50,
                    size: 12,
                  });
                }
                const auxpages = await doc.copyPages(
                  aux,
                  aux.getPageIndices()
                );
                for (const page of auxpages) {
                  doc.addPage(page);
                }
              }
              else if (response[a].file.includes('JPG') || response[a].file.includes('PNG')) {
                const page = doc.addPage();
                let img;
                if (response[a].file.includes('PNG')) {
                  img = await doc.embedPng(response[a].data);
                } else {
                  img = await doc.embedJpg(response[a].data);
                }
                if (a === 0) {
                  page.moveTo(72, 800);
                  page.drawText(`Annex: ${response[a].type}`, {
                    size: 12,
                  });
                }
                const jpgDims = img.scale(0.5);
                page.drawImage(img, {
                  x: page.getWidth() / 2 - jpgDims.width / 2,
                  y: page.getHeight() / 2 - jpgDims.height / 2,
                  width: jpgDims.width,
                  height: jpgDims.height,
                });
              } else {
                const helveticaFont = await doc.embedFont(StandardFonts.Helvetica);
                /* (5) Create a page and it to the PDF document */
                const page = doc.addPage();
               /* (4) Create the link annotation object and ref */
                const linkAnnotation = doc.context.obj({
                  Type: 'Annot',
                  Subtype: 'Link',
                  Rect: [145, page.getHeight() / 2 - 5, 358, page.getHeight() / 2 + 15],
                  Border: [0, 0, 2],
                  C: [0, 0, 1],
                  A: {
                    Type: 'Action',
                    S: 'URI',
                    URI: PDFString.of(response[a].url),
                  },
                });
                const linkAnnotationRef = doc.context.register(linkAnnotation);
                /* (6) Draw some text for the link to be placed over */
                page.drawText('Click here to view the External Report', {
                  x: 150,
                  y: page.getHeight() / 2,
                  font: helveticaFont,
                  size: 15,
                  color: rgb(0, 0, 1),
                });
                /* (7) Add the link to the page */
                page.node.set(PDFName.of('Annots'), doc.context.obj([linkAnnotationRef]));
                if (a === 0) {
                  page.moveTo(72, 800);
                  page.drawText(`Annex: ${response[a].type}`, {
                    size: 12,
                  });
                }

              }
            }
          }
          try {
            const base64DataUri = await doc.saveAsBase64({ dataUri: true });
            res.send({
              status: "SUCCESS",
              data: base64DataUri,
              messageCode: "MSG200",
              message: "Pdf Merged successfully",
              userMessage: "Pdf Merged successfully !!!.",
            });
          } catch {
            res.status(500).send({
              status: "Failure",
              messageCode: "MSG500",
              message: "Some error occurred while merging the pdf",
              userMessage: "Please check the parsed data",
            });
          }
        })
      } else {
        try {
          const base64DataUri = await doc.saveAsBase64({ dataUri: true });
          res.send({
            status: "SUCCESS",
            data: base64DataUri,
            messageCode: "MSG200",
            message: "Pdf Merged successfully",
            userMessage: "Pdf Merged successfully !!!.",
          });
        } catch {
          res.status(500).send({
            status: "Failure",
            messageCode: "MSG500",
            message: "Some error occurred while merging the pdf",
            userMessage: "Please check the parsed data",
          });
        }
      }
    })();
  } catch {
    res.status(500).send({
      status: "Failure",
      messageCode: "MSG500",
      message: "Some error occurred while merging the pdf",
      userMessage: "Please check the parsed data",
    });
  }
};

async function returingExternalFile(url) {
  return new Promise((resolve, reject) => {
    if (url.length !== 0) {
      spauth.getAuth(config.authURL, {
        clientId: config.clientId,
        clientSecret: config.clientSecret
      }).then(function (headeroptions) {
        // Access Token will be available on the options.headers variable
        let finalArray = [];
        var headers = headeroptions.headers;
        headers['content-type'] = 'application/json;odata=verbose';
        headers['Accept'] = 'application/json;odata=verbose';
          url.map(async (element) => {
          const urlArr = element.url.split("https://pvgroupbe.sharepoint.com");
          const URLConstruction = config.externalReport + urlArr[1] + "')" + config.endurl;
          let options = {
            method: "GET",
            uri: encodeURI(URLConstruction), //  OpenBinaryStream also works
            headers: headers,
            encoding: null
          };
          await requestprom(options).then(function (dataRetrived) {
            let obj = {
              file: urlArr[1],
              data: Buffer.from(dataRetrived).toString('base64'),
              url: element.url,
              type: element.type
            }
            finalArray = finalArray.concat(obj);
            if (url.length === finalArray.length) {
              return resolve(finalArray);
            }
          });
        });  
      });

    } else {
      return [];
    }

  }).catch(error => {
    return null;
  });
}