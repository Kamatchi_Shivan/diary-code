let env = require('dotenv')
const express = require("express");
const bodyParser = require("body-parser");
cors = require("cors");
const app = express();

// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({limit: '50mb', extended: true}));        
app.use(bodyParser.text({limit: '50mb', extended: true}));        
app.use(bodyParser.urlencoded({limit:'50mb', extended: true }));    

app.use(cors());


app.get("/", (req, res) => {
  res.json({ message: "Welcome to DIARY API Service Tool application." });
});

require("./app/routes/domain.routes")(app);

require("./app/routes/customer.routes")(app);

require("./app/routes/datauploads.routes")(app);

require("./app/routes/release.routes")(app);

require("./app/routes/project.routes")(app);

require("./app/routes/defects.routes")(app);

require("./app/routes/test.results.routes")(app);

require("./app/routes/coverage.routes")(app);

require("./app/routes/story.routes")(app);

require("./app/routes/mailer.routes")(app);

//initialize .env file
env.config()

const PORT = process.env.APP_PORT_NO || 9000;

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
  //const ScheduledMaintanance = require('./app/api-scheduling/scheduling');
});
