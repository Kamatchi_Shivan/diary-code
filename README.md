## REACT Dashboard Application
* Server - Database 
* Client - UI Build
* Data Uploads - Database server
* Client - Data Uploads - UI Build
* Commons - Requirements gathers and related dcocuments


### Installation and Setup Instructions

Clone down this repository. You will need `node` and `npm` installed globally on your machine.  

Installation:

`npm install`  

To Run Test Suite:  

`npm test`  

To Start Server:

`npm start`  

To Visit App:

`localhost:3000/`

