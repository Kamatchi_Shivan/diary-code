import {
  ALL_JOBS,
  USER_MESSAGE,
  COMPONENT_MESSAGE,
  DEFECTS_MESSAGE,
  PROJECT_MESSAGE,
  RELEASE_MESSAGE,
  REQUIREMENT_MESSAGE,
  TESTREPORT_MESSAGE,
  TESTRESULTS_MESSAGE,
  USERSTORY_MESSAGE,
  SUCCESS_COMPONENT_MESSAGE,
  SUCCESS_DEFECTS_MESSAGE,
  SUCCESS_PROJECT_MESSAGE,
  SUCCESS_RELEASE_MESSAGE,
  SUCCESS_REQUIREMENT_MESSAGE,
  SUCCESS_TESTREPORT_MESSAGE,
  SUCCESS_TESTRESULTS_MESSAGE,
  SUCCESS_USERSTORY_MESSAGE,
  COMARCH_DATA,
  COMARCH_MESSAGE
} from "../types";
import fetch from "../../../utils/leoFetch";
import { config } from "../../../config/configs";

export const getAllJobs = () => (dispatch: any, option: any) => {
  fetch(config.serverUrl + config.jobsurl, option)
    .then((res) => res.json())
    .then((jobs) => {
      dispatch({
        payload: jobs.data,
        type: ALL_JOBS,
      });
    })
    .catch((err) => {
      dispatch({
        payload: [],
        type: ALL_JOBS,
      });
    });
};

export const RetriveComarchData = ( job: any) => (dispatch: any, option: any) => {
  fetch(config.datauploads+config.comarchJiraUrl,{
    body: JSON.stringify(job),
    headers: {
      "content-type": "application/json"
    },
    method: "POST"
  })
    .then((res) => res.json())
    .then((jobs) => {
      if (jobs.status === "SUCCESS") {
        dispatch({
          payload: jobs.data,
          type: ALL_JOBS,
        });
      }
      dispatch({
        payload: jobs.message,
        type: COMARCH_DATA,
      });
    })
    .catch((err) => {
      let data = "Internal Server Error, please check your server connection";
      dispatch({
        payload: data,
        type: COMARCH_DATA,
      });
    });
};

export const CallDataUploads = (url: any, job: any) => (
  dispatch: any,
  option: any
) => {
  fetch(url, option)
    .then((res) => res.json())
    .then((jobs) => {
      if (jobs.status === "SUCCESS") {
        dispatch({
          payload: jobs.data,
          type: ALL_JOBS,
        });
      }
      if (config.jobcomponents === job) {
        dispatch({
          payload: jobs.message,
          type: SUCCESS_COMPONENT_MESSAGE,
        });
      }
      if (config.jobdefects === job) {
        dispatch({
          payload: jobs.message,
          type: SUCCESS_DEFECTS_MESSAGE,
        });
      }
      if (config.jobprojects === job) {
        dispatch({
          payload: jobs.message,
          type: SUCCESS_PROJECT_MESSAGE,
        });
      }
      if (config.jobrelease === job) {
        dispatch({
          payload: jobs.message,
          type: SUCCESS_RELEASE_MESSAGE,
        });
      }
      if (config.jobrequirements === job) {
        dispatch({
          payload: jobs.message,
          type: SUCCESS_REQUIREMENT_MESSAGE,
        });
      }
      if (config.jobtestReports === job) {
        dispatch({
          payload: jobs.message,
          type: SUCCESS_TESTREPORT_MESSAGE,
        });
      }
      if (config.jobtestResults === job) {
        dispatch({
          payload: jobs.message,
          type: SUCCESS_TESTRESULTS_MESSAGE,
        });
      }
      if (config.jobuserStory === job) {
        dispatch({
          payload: jobs.message,
          type: SUCCESS_USERSTORY_MESSAGE,
        });
      }
    })
    .catch((err) => {
      let data = "Internal Server Error, please check your server connection";
      if (config.jobcomponents === job) {
        dispatch({
          payload: data,
          type: SUCCESS_COMPONENT_MESSAGE,
        });
      }
      if (config.jobdefects === job) {
        dispatch({
          payload: data,
          type: SUCCESS_DEFECTS_MESSAGE,
        });
      }
      if (config.jobprojects === job) {
        dispatch({
          payload: data,
          type: SUCCESS_PROJECT_MESSAGE,
        });
      }
      if (config.jobrelease === job) {
        dispatch({
          payload: data,
          type: SUCCESS_RELEASE_MESSAGE,
        });
      }
      if (config.jobrequirements === job) {
        dispatch({
          payload: data,
          type: SUCCESS_REQUIREMENT_MESSAGE,
        });
      }
      if (config.jobtestReports === job) {
        dispatch({
          payload: data,
          type: SUCCESS_TESTREPORT_MESSAGE,
        });
      }
      if (config.jobtestResults === job) {
        dispatch({
          payload: data,
          type: SUCCESS_TESTRESULTS_MESSAGE,
        });
      }
      if (config.jobuserStory === job) {
        dispatch({
          payload: data,
          type: SUCCESS_USERSTORY_MESSAGE,
        });
      }
    });
};

export const ClearingJobMessage = (job: any) => (dispatch: any, option: any) => {
  if (config.jobComarchJira === job) {
    dispatch({
      payload: null,
      type: COMARCH_MESSAGE,
    });
  }
  if (config.jobcomponents === job) {
    dispatch({
      payload: null,
      type: COMPONENT_MESSAGE,
    });
  }
  if (config.jobdefects === job) {
    dispatch({
      payload: null,
      type: DEFECTS_MESSAGE,
    });
  }
  if (config.jobprojects === job) {
    dispatch({
      payload: null,
      type: PROJECT_MESSAGE,
    });
  }
  if (config.jobrelease === job) {
    dispatch({
      payload: null,
      type: RELEASE_MESSAGE,
    });
  }
  if (config.jobrequirements === job) {
    dispatch({
      payload: null,
      type: REQUIREMENT_MESSAGE,
    });
  }
  if (config.jobtestReports === job) {
    dispatch({
      payload: null,
      type: TESTREPORT_MESSAGE,
    });
  }
  if (config.jobtestResults === job) {
    dispatch({
      payload: null,
      type: TESTRESULTS_MESSAGE,
    });
  }
  if (config.jobuserStory === job) {
    dispatch({
      payload: null,
      type: USERSTORY_MESSAGE,
    });
  }
}

export const ClearingSuccessJobMessage = (job: any) => (dispatch: any, option: any) => {
  if (config.jobComarchJira === job) {
    dispatch({
      payload: null,
      type: COMARCH_DATA,
    });
  }
  if (config.jobcomponents === job) {
    dispatch({
      payload: null,
      type: SUCCESS_COMPONENT_MESSAGE,
    });
  }
  if (config.jobdefects === job) {
    dispatch({
      payload: null,
      type: SUCCESS_DEFECTS_MESSAGE,
    });
  }
  if (config.jobprojects === job) {
    dispatch({
      payload: null,
      type: SUCCESS_PROJECT_MESSAGE,
    });
  }
  if (config.jobrelease === job) {
    dispatch({
      payload: null,
      type: SUCCESS_RELEASE_MESSAGE,
    });
  }
  if (config.jobrequirements === job) {
    dispatch({
      payload: null,
      type: SUCCESS_REQUIREMENT_MESSAGE,
    });
  }
  if (config.jobtestReports === job) {
    dispatch({
      payload: null,
      type: SUCCESS_TESTREPORT_MESSAGE,
    });
  }
  if (config.jobtestResults === job) {
    dispatch({
      payload: null,
      type: SUCCESS_TESTRESULTS_MESSAGE,
    });
  }
  if (config.jobuserStory === job) {
    dispatch({
      payload: null,
      type: SUCCESS_USERSTORY_MESSAGE,
    });
  }
}

export const ClearingMessage = () => (dispatch: any, option: any) => {
  dispatch({
    payload: "",
    type: USER_MESSAGE,
  });
};

export const SettingComponentMessage = (message: any) => (
  dispatch: any,
  option: any
) => {
  dispatch({
    payload: message,
    type: COMPONENT_MESSAGE,
  });
};

export const SettingDefectsMessage = (message: any) => (
  dispatch: any,
  option: any
) => {
  dispatch({
    payload: message,
    type: DEFECTS_MESSAGE,
  });
};

export const SettingProjectMessage = (message: any) => (
  dispatch: any,
  option: any
) => {
  dispatch({
    payload: message,
    type: PROJECT_MESSAGE,
  });
};

export const SettingReleaseMessage = (message: any) => (
  dispatch: any,
  option: any
) => {
  dispatch({
    payload: message,
    type: RELEASE_MESSAGE,
  });
};

export const SettingComarchMessage = (message: any) => (
  dispatch: any,
  option: any
) => {
  dispatch({
    payload: message,
    type: COMARCH_MESSAGE,
  });
};

export const SettingRequirementsMessage = (message: any) => (
  dispatch: any,
  option: any
) => {
  dispatch({
    payload: message,
    type: REQUIREMENT_MESSAGE,
  });
};

export const SettingTestReportsMessage = (message: any) => (
  dispatch: any,
  option: any
) => {
  dispatch({
    payload: message,
    type: TESTREPORT_MESSAGE,
  });
};

export const SettingTestResultsMessage = (message: any) => (
  dispatch: any,
  option: any
) => {
  dispatch({
    payload: message,
    type: TESTRESULTS_MESSAGE,
  });
};

export const SettinguserStoryMessage = (message: any) => (
  dispatch: any,
  option: any
) => {
  dispatch({
    payload: message,
    type: USERSTORY_MESSAGE,
  });
};
