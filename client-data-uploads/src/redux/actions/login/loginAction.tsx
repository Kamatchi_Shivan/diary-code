import {
  LOGIN,
  LOGOUT,
} from "../types";
import axios from "../../../utils/leoAxios";
import Global from "../../../components/layout/Global/Global";
import message from "../../../components/messages/message.json";
import { config } from "../../../config/configs";


export const logoutAction = () => (dispatch: any) => {
  let loginData = {
    isLoggedIn: false,
    redirect: false,
    token: null
  };
  //localStorage.clear();
  dispatch({
    payload: loginData,
    type: LOGOUT
  })
  Global.history.push("/login");

}

export const loginAction = (values: any) => (dispatch: any) => {
  let payload = {
    email: values.userName,
    password: values.password,
  };
  let loginData = {
    Name: null,
    userName: null,
    isLoggedIn: false,
    password: null, // for reference
    redirect: false,
    token: null,
  };

  axios
    .post(config.authenticationURL, payload, {
      headers: {
        "Content-Type": "application/json",
      },
    })
    .then((res) => {
      console.log(101, res.data);
        var token = res.data.token;
        if (token !== null && token !== undefined) {
          localStorage.setItem("token", token);
          localStorage.setItem("UserName", res.data.name);
          dispatch({
            payload: {
              email: res.data.email,
              name: res.data.name,
              isLoggedIn: true,
              redirect: true,
              ...res.data,
            },
            type: LOGIN,
          });
        }
      else {
        dispatch({
          payload: {
            ...loginData,
            message: message.loginErrorMessage,
          },
          type: LOGIN,
        });
      }
    });
};

