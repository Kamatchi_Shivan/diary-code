import {
  ALL_JOBS,
  USER_MESSAGE,
  COMPONENT_MESSAGE,
  DEFECTS_MESSAGE,
  PROJECT_MESSAGE,
  RELEASE_MESSAGE,
  REQUIREMENT_MESSAGE,
  TESTREPORT_MESSAGE,
  TESTRESULTS_MESSAGE,
  USERSTORY_MESSAGE,
  SUCCESS_COMPONENT_MESSAGE,
  SUCCESS_DEFECTS_MESSAGE,
  SUCCESS_PROJECT_MESSAGE,
  SUCCESS_RELEASE_MESSAGE,
  SUCCESS_REQUIREMENT_MESSAGE,
  SUCCESS_TESTREPORT_MESSAGE,
  SUCCESS_TESTRESULTS_MESSAGE,
  SUCCESS_USERSTORY_MESSAGE,
  COMARCH_DATA,
  COMARCH_MESSAGE
} from "../../actions/types";

const initialState = {
  alljobs: [],
  usermessage: "",
  componentmessage: null,
  defectmessage: null,
  projectmessage: null,
  releasemessage: null,
  requirementsmessage: null,
  testReportsmessage: null,
  testResultsmessage: null,
  userStorymessage: null,
  comarchmessage:null,
  successcomponentmessage: null,
  successdefectmessage: null,
  successprojectmessage: null,
  successreleasemessage: null,
  successrequirementsmessage: null,
  successtestReportsmessage: null,
  successtestResultsmessage: null,
  successuserStorymessage: null,
  comarchResponse: null
};

const datauploadsReducer = (state = initialState, action: any) => {
  //console.log("in reducer");
  switch (action.type) {
    case ALL_JOBS:
      return {
        ...state,
        alljobs: action.payload,
      };
      case COMARCH_DATA:
        return {
          ...state,
          comarchResponse: action.payload,
        };
    case USER_MESSAGE:
      return {
        ...state,
        usermessage: action.payload,
      };
    case COMPONENT_MESSAGE:
      return {
        ...state,
        componentmessage: action.payload,
      };
    case DEFECTS_MESSAGE:
      return {
        ...state,
        defectmessage: action.payload,
      };
    case PROJECT_MESSAGE:
      return {
        ...state,
        projectmessage: action.payload,
      };
      case COMARCH_MESSAGE:
      return {
        ...state,
        comarchmessage: action.payload,
      };
    case RELEASE_MESSAGE:
      return {
        ...state,
        releasemessage: action.payload,
      };
    case REQUIREMENT_MESSAGE:
      return {
        ...state,
        requirementsmessage: action.payload,
      };
    case TESTREPORT_MESSAGE:
      return {
        ...state,
        testReportsmessage: action.payload,
      };
    case TESTRESULTS_MESSAGE:
      return {
        ...state,
        testResultsmessage: action.payload,
      };
    case USERSTORY_MESSAGE:
      return {
        ...state,
        userStorymessage: action.payload,
      };
   case SUCCESS_COMPONENT_MESSAGE:
      return {
        ...state,
        successcomponentmessage: action.payload,
      };
    case SUCCESS_DEFECTS_MESSAGE:
      return {
        ...state,
        successdefectmessage: action.payload,
      };
    case SUCCESS_PROJECT_MESSAGE:
      return {
        ...state,
        successprojectmessage: action.payload,
      };
    case SUCCESS_RELEASE_MESSAGE:
      return {
        ...state,
        successreleasemessage: action.payload,
      };
    case SUCCESS_REQUIREMENT_MESSAGE:
      return {
        ...state,
        successrequirementsmessage: action.payload,
      };
    case SUCCESS_TESTREPORT_MESSAGE:
      return {
        ...state,
        successtestReportsmessage: action.payload,
      };
    case SUCCESS_TESTRESULTS_MESSAGE:
      return {
        ...state,
        successtestResultsmessage: action.payload,
      };
    case SUCCESS_USERSTORY_MESSAGE:
      return {
        ...state,
        successuserStorymessage: action.payload,
      };
    default:
      return state;
  }
}

export default datauploadsReducer;