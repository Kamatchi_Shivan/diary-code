import {combineReducers} from 'redux';
import loginReducer from '../reducers/login/loginReducer';
import datauploadsReducer from "./datauploads/datauploadsReducer"

export default combineReducers({
    loginData: loginReducer,
    datauploadsReducer: datauploadsReducer
});
