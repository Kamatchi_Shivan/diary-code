import React from "react";
import "./css/login.css";
import "./css/font-awesome-4.7.0/css/font-awesome.css";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import {
  loginAction,
  logoutAction,
} from "../../redux/actions/login/loginAction";
import { Row, Input, Form, Button } from "antd";
import { config } from "../../config/configs";
import {ArrowLeftOutlined,LoginOutlined } from "@ant-design/icons";
import banner from './images/banner-home.svg';

type MyProps = {
  logoutAction: any;
  loginAction: any;
  loginData: any;
  form: any;
};
type MyState = {
  loading: boolean;
  refreshset: boolean;
};

class Login extends React.Component<MyProps, MyState> {
  componentWillMount() {
    this.props.logoutAction();
    this.render();
  }

  constructor(props: any) {
    super(props);
    this.state = {
      loading: false,
      refreshset: true,
    };
  }

  handleSubmit = (e: any) => {
    e.preventDefault();
    this.setState({ refreshset: true });
    this.props.form.validateFieldsAndScroll((err: any, values: any) => {
      if (!err) {
        this.setState({ loading: true });
        this.props.loginAction(values);
      }
    });
  };

  renderRedirect = () => {
    if (this.props.loginData.redirect) {
      this.setState({ loading: false });
      return <Redirect to="/home" />;
    }
  };

  render() {
    if (this.state.refreshset && this.props.loginData.message !== undefined) {
      this.setState({ loading: false, refreshset: false });
    }
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="login-body login-layout">
        <div className="login">
          {this.renderRedirect()}
          <div className="login-content">
            <div className="login-top-content">
              <h1 className="login-title">
                Welcome to <span>DIARY</span>
              </h1>
              <div className="login-text">
                Dynamic Integrated Automated Reporting for You
              </div>
              <div className="login-btn">
                <a href={config.releaseView} rel="noopener noreferrer">
                  <span><ArrowLeftOutlined className="datauploads-icons"/> Back to Release View</span>
                </a>
              </div>
            </div>
            <div>
              <img
                src={banner}
                alt="banner"
                className="img-responsive"
              />
            </div>
          </div>
          <div className="login-form">
            <div className="login-form-section">
              <h1 className="login-content-title">Interim-Data uploads</h1>
              <p>Please login with your information username and password</p>
              <Form onSubmit={this.handleSubmit} className="form-signin">
                <div className="form-group mail-box">
                  <i className="fa fa-user" aria-hidden="true"></i>
                  <div className="form-info">
                    <label htmlFor="email">Username</label>
                    <Form.Item>
                      {getFieldDecorator("userName", {
                        rules: [
                          {
                            message: "Please enter username",
                            required: true,
                          },
                          {
                            max: 100,
                            message:
                              "Email should be accept maximum 35 characters",
                          },
                          {
                            message: "Email should be minimum 4 characters",
                            min: 4,
                          },
                        ],
                      })(
                        <Input
                          className="form-control"
                          id="email"
                          name="email"
                          placeholder="Enter email address"
                          allowClear
                        />
                      )}
                    </Form.Item>
                  </div>
                </div>

                <div className="form-group password-box">
                  <i className="fa fa-lock" aria-hidden="true"></i>
                  <div className="form-info">
                    <label htmlFor="password">Password</label>
                    <Form.Item>
                      {getFieldDecorator("password", {
                        rules: [
                          {
                            message: "Please enter Password",
                            required: true,
                          },
                          {
                            max: 25,
                            message:
                              "Password should be accept maximum 25 characters",
                          },
                          {
                            message: "Password should be minimum 5 characters",
                            min: 5,
                          },
                        ],
                      })(
                        <Row type="flex" justify="space-between">
                          <Input.Password
                            className="form-control"
                            id="password"
                            name="password"
                            placeholder="Enter password"
                          />
                        </Row>
                      )}
                    </Form.Item>
                  </div>
                </div>
                <div className="loginerror">{this.props.loginData.message}</div>
                <div className="btn-action">
                  <Button
                    className="login-form-btn"
                    htmlType="submit"
                    // loading={this.state.loading}
                  >
                    Login <LoginOutlined className="datauploads-icons"/>
                  </Button>
                </div>
              </Form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => ({
  loginData: state.loginData,
});

const LoginuserForm = Form.create({ name: "Login" })(Login);

export default connect(mapStateToProps, {
  loginAction,
  logoutAction,
})(LoginuserForm);
