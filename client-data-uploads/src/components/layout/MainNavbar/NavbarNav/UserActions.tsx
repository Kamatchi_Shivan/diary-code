import React from "react";
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Collapse,
  NavItem,
  NavLink,
  Button
} from "shards-react";
import {connect} from 'react-redux';
import {logoutAction} from "../../../../redux/actions/login/loginAction";
import { LogoutOutlined } from '@ant-design/icons';

type MyProps = { logoutAction: any, loginData: {
  loginData: any,
  name: any,

}};
type MyState = { visible: boolean };
class UserActions extends React.Component<MyProps,MyState> {
  constructor(props:any) {
    super(props);

    this.state = {
      visible: false
    };

    this.toggleUserActions = this.toggleUserActions.bind(this);
  }

  toggleUserActions() {
    this.setState({
      visible: !this.state.visible
    });
  }
  handleClose = () => {
    this.props.logoutAction();
  }


  render() {
    return (
      <Button type="primary"  onClick = {this.handleClose}> <div className="icon-style"><LogoutOutlined /></div></Button>
    )
  }
}
const mapStateToProps = (state:any) => ({
  loginData: state.loginData
});
export default connect(mapStateToProps,{logoutAction})(UserActions);
