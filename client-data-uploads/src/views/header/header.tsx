import React, { Component } from "react";
import { connect } from "react-redux";
import { Form, Button } from "antd";
import {logoutAction} from "../../redux/actions/login/loginAction";
import {
  getAllJobs
} from "../../redux/actions/datauploads/datauploadsAction";
import {LogoutOutlined  } from "@ant-design/icons";
import Logo from "../../assets/images/pandv-v3.svg";

/*Declaring Props Variable*/
type MyProps = {
  logoutAction : any;
  getAllJobs: any;
};

/*Declaring State Variable*/
type MyState = {};

class Header extends Component<MyProps, MyState> {
  constructor(props: any) {
    super(props);
    this.state = {};
  }

  handleClose = () => {
    this.props.logoutAction();
  }


  render() {
    return (
      <Form className="header header-main-menu">
        <div className="form-group-info header-logo">
          <img
            src={Logo}
            alt="banner"
            className="left-sec pv_logo"
            onClick = {this.props.getAllJobs}
          />
        </div>
        <div style={{ float: "right", marginLeft: "auto" }}>
          <Button
            className="header-btn header-btn-cancel"
            onClick={this.handleClose}
          >
            Logout <LogoutOutlined className="datauploads-icons"/>
            {/* <LogoutOutlined /> */}
          </Button>
        </div>
      </Form>
    );
  }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, {logoutAction,getAllJobs})(Header);
