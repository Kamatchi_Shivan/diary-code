import React, {Component} from "react";
import {connect} from 'react-redux';
import  Datauploads  from "../datauploads/datauploads";
import Global from "../../components/layout/Global/Global";

type MyProps = {
    loginData:any,
};

class Dashboard extends Component<MyProps> {

    constructor(props:any) {
        super(props);
        this.state = {
            consversionPanel: true
        }
    }

    componentWillMount() {
      if (this.props.loginData.token === null) {
        window.localStorage.removeItem("token");
        Global.history.push("/login");
    }
      }

    render() {
        return (
            <Datauploads></Datauploads>
        );
    }
}
const mapStateToProps = (state:any) => ({
    loginData:state.loginData,
});
export default connect(mapStateToProps,{
})(Dashboard);
