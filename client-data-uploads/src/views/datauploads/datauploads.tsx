import React, { Component } from "react";
import { Button, Form, Typography, message,Modal,Input } from "antd";
import { connect } from "react-redux";
import Global from "../../components/layout/Global/Global";
import {
  getAllJobs,
  CallDataUploads,
  ClearingMessage,
  SettingComponentMessage,
  SettingDefectsMessage,
  SettingProjectMessage,
  SettingReleaseMessage,
  SettingComarchMessage,
  SettingRequirementsMessage,
  SettingTestReportsMessage,
  SettingTestResultsMessage,
  SettinguserStoryMessage,
  ClearingJobMessage,
  ClearingSuccessJobMessage,
  RetriveComarchData
} from "../../redux/actions/datauploads/datauploadsAction";
import { config } from "../../config/configs";
import { SmileTwoTone } from '@ant-design/icons';
import { minsGen } from "./minsGenerating";
const { Text } = Typography;
/* Date Formatting Function */
const ValidateDate = (data) => {
  if (data < 10) {
    data = "0" + data;
  }
  return data;
};
const key = "updatable";

type MyProps = {
  loginData: any;
  alljobs: any;
  getAllJobs: any;
  CallDataUploads: any;
  usermessage: any;
  ClearingMessage: any;
  SettingComponentMessage: any;
  componentmessage: any;
  defectmessage: any;
  projectmessage: any;
  releasemessage: any;
  requirementsmessage: any;
  testReportsmessage: any;
  testResultsmessage: any;
  userStorymessage: any;
  comarchmessage:any;
  SettingDefectsMessage: any;
  SettingProjectMessage: any;
  SettingReleaseMessage: any;
  SettingComarchMessage:any;
  SettingRequirementsMessage: any;
  SettingTestReportsMessage: any;
  SettingTestResultsMessage: any;
  SettinguserStoryMessage: any;
  ClearingJobMessage: any;
  successcomponentmessage: any;
  successComarchmessage: any;
  successdefectmessage: any;
  successprojectmessage: any;
  successreleasemessage: any;
  successrequirementsmessage: any;
  successtestReportsmessage: any;
  successtestResultsmessage: any;
  successuserStorymessage: any;
  ClearingSuccessJobMessage: any;
  form: any;
  RetriveComarchData: any;
};
type MyState = {
  columns: any;
  componentButton: boolean;
  comarchButton: boolean;
  defectButton: boolean;
  projectButton: boolean;
  releaseButton: boolean;
  requirementButton: boolean;
  testReportButton: boolean;
  testResultsButton: boolean;
  userStoryButton: boolean;
  automaticcomponentButton: boolean;
  automaticdefectButton: boolean;
  automaticprojectButton: boolean;
  automaticreleaseButton: boolean;
  automaticrequirementButton: boolean;
  automatictestReportButton: boolean;
  automatictestResultsButton: boolean;
  automaticuserStoryButton: boolean;
  componentInprogress: any;
  comarchupdateInprogress: any;
  defectInprogress: any;
  projectInprogress: any;
  releaseInprogress: any;
  requirementInprogress: any;
  testReportInprogress: any;
  testResultsInprogress: any;
  userStoryInprogress: any;
  modalVisible: boolean;
};

class Datauploads extends Component<MyProps, MyState> {
  constructor(props: any) {
    super(props);
    this.DisablingFeatureBasedOnAutomaticRun = this.DisablingFeatureBasedOnAutomaticRun.bind(this);
    this.state = {
      componentButton: false,
      comarchButton: false,
      defectButton: false,
      projectButton: false,
      releaseButton: false,
      requirementButton: false,
      testReportButton: false,
      testResultsButton: false,
      userStoryButton: false,
      automaticcomponentButton: false,
      automaticdefectButton: false,
      automaticprojectButton: false,
      automaticreleaseButton: false,
      automaticrequirementButton: false,
      automatictestReportButton: false,
      automatictestResultsButton: false,
      automaticuserStoryButton: false,
      componentInprogress: null,
      comarchupdateInprogress: null,
      defectInprogress: null,
      projectInprogress: null,
      releaseInprogress: null,
      requirementInprogress: null,
      testReportInprogress: null,
      testResultsInprogress: null,
      userStoryInprogress: null,
      modalVisible:false,
      columns: [
        {
          dataIndex: "rowIndex",
          key: "rowIndex",
          render: (text, record, index) => {
            return <div>{++index}</div>;
          },
          title: "S.No ",
          width: "4%",
        },
        {
          title: "Job Name",
          dataIndex: "jobName",
          key: "jobName",
          // width: "15%"
        },
        {
          title: "Description",
          dataIndex: "jobDescription",
          key: "jobDescription",
          // width: "15%"
        },
        {
          title: "Manual Run Time",
          dataIndex: "ManualRuntime",
          key: "ManualRuntime",
          // width: "15%"
        },
        {
          title: "Automatic Last Run Time",
          dataIndex: "LastRunTime",
          key: "LastRunTime",
          // width: "15%"
        },
        {
          title: "Run Job",
          dataIndex: "jobUrl",
          key: "jobUrl",
          width: "10%",
        },
      ],
    };
  }
  componentWillMount() {
    if (this.props.loginData.token === null) {
      window.localStorage.removeItem("token");
      Global.history.push("/login");
    } else {
      this.props.getAllJobs();
      this.props.ClearingMessage();
    }
  }

  dataUploads = (data: any, event: any) => {
    if (config.jobComarchJira === event) {
      this.props.form.resetFields();
        this.setState({ modalVisible: true });
    }
    if (config.jobcomponents === event) {
      this.props.SettingComponentMessage(config.updatecomponents);
      this.setState({
        componentButton: true,
        componentInprogress: config.jobStatus,
      });
    }
    if (config.jobdefects === event) {
      this.props.SettingDefectsMessage(config.updatedefects);
      this.setState({ defectButton: true, defectInprogress: config.jobStatus });
    }
    if (config.jobprojects === event) {
      this.props.SettingProjectMessage(config.updateprojects);
      this.setState({
        projectButton: true,
        projectInprogress: config.jobStatus,
      });
    }
    if (config.jobrelease === event) {
      this.props.SettingReleaseMessage(config.updaterelease);
      this.setState({
        releaseButton: true,
        releaseInprogress: config.jobStatus,
      });
    }
    if (config.jobrequirements === event) {
      this.props.SettingRequirementsMessage(config.updaterequirement);
      this.setState({
        requirementButton: true,
        requirementInprogress: config.jobStatus,
      });
    }
    if (config.jobtestReports === event) {
      this.props.SettingTestReportsMessage(config.updatetestReports);
      this.setState({
        testReportButton: true,
        testReportInprogress: config.jobStatus,
      });
    }
    if (config.jobtestResults === event) {
      this.props.SettingTestResultsMessage(config.updatetestResults);
      this.setState({
        testResultsButton: true,
        testResultsInprogress: config.jobStatus,
      });
    }
    if (config.jobuserStory === event) {
      this.props.SettinguserStoryMessage(config.updateuserStory);
      this.setState({
        userStoryButton: true,
        userStoryInprogress: config.jobStatus,
      });
    }
    this.props.CallDataUploads(data, event);
  };

  /* Merging Message */
  MergingMessage = () => {
    let usermsg = this.props.usermessage;
    if (this.props.usermessage !== "") {
      message.success(usermsg);
      this.props.ClearingMessage();
    }
  };

  dateFormatting = (data: Date) => {
    if (data !== null && data !== undefined) {
      let date_ob: any = new Date(data);
      date_ob = date_ob.toUTCString();
      let finalDate = new Date(date_ob);
      let date = ("0" + finalDate.getDate()).slice(-2),
        month = ("0" + (finalDate.getMonth() + 1)).slice(-2),
        year = finalDate.getFullYear(),
        hour = ValidateDate(finalDate.getHours()),
        min = ValidateDate(finalDate.getMinutes()),
        sec = ValidateDate(finalDate.getSeconds()),
        dateTime =
          year + "." + month + "." + date + " " + hour + ":" + min + ":" + sec;
      return dateTime;
    } else {
      return "";
    }
  };

  /*Component Message */
  ComponentMessage = () => {
    if (this.props.componentmessage !== null) {
      message.loading({ content: this.props.componentmessage, key });
      this.props.ClearingJobMessage(config.jobcomponents);
    }
  };

    /*Component Message */
    ComarchMessage = () => {
      if (this.props.comarchmessage !== null) {
        message.loading({ content: this.props.comarchmessage, key });
        this.props.ClearingJobMessage(config.jobComarchJira);
      }
    };

  /*Defect Message */
  DefectMessage = () => {
    if (this.props.defectmessage !== null) {
      message.loading({ content: this.props.defectmessage, key });
      this.props.ClearingJobMessage(config.jobdefects);
    }
  };

  /*Project Message */
  ProjectMessage = () => {
    if (this.props.projectmessage !== null) {
      message.loading({ content: this.props.projectmessage, key });
      this.props.ClearingJobMessage(config.jobprojects);
    }
  };

  /*Release Message */
  ReleaseMessage = () => {
    if (this.props.releasemessage !== null) {
      message.loading({ content: this.props.releasemessage, key });
      this.props.ClearingJobMessage(config.jobrelease);
    }
  };

  /*Requirement Message */
  RequirementMessage = () => {
    if (this.props.requirementsmessage !== null) {
      message.loading({ content: this.props.requirementsmessage, key });
      this.props.ClearingJobMessage(config.jobrequirements);
    }
  };

  /*TestReport Message */
  TestReportMessage = () => {
    if (this.props.testReportsmessage !== null) {
      message.loading({ content: this.props.testReportsmessage, key });
      this.props.ClearingJobMessage(config.jobtestReports);
    }
  };

  /*TestResults Message */
  TestResultMessage = () => {
    if (this.props.testResultsmessage !== null) {
      message.loading({ content: this.props.testResultsmessage, key });
      this.props.ClearingJobMessage(config.jobtestResults);
    }
  };

  /*UserStory Message */
  UserStoryMessage = () => {
    if (this.props.userStorymessage !== null) {
      message.loading({ content: this.props.userStorymessage, key });
      this.props.ClearingJobMessage(config.jobuserStory);
    }
  };

  /*Component Message */
  ComponentSuccessMessage = () => {
    if (this.props.successcomponentmessage !== null && this.props.successcomponentmessage !== undefined) {
      this.setState({ componentButton: false, componentInprogress: null });
      if (!this.props.successcomponentmessage.includes("Error") && !this.props.successcomponentmessage.includes("background")) {
        message.success(this.props.successcomponentmessage);
      } else if (this.props.successcomponentmessage.includes("background")) {
        message.loading(this.props.successcomponentmessage, 10);
      } else {
        message.error(this.props.successcomponentmessage);
      }
    } else {
      message.error(config.errorStatus);
    }
    this.props.ClearingSuccessJobMessage(config.jobcomponents);
  };

  /*Component Message */
  ComarchSuccessMessage = () => {
    if (this.props.successComarchmessage !== null && this.props.successComarchmessage !== undefined) {
      this.setState({ comarchButton: false, comarchupdateInprogress: null });
      if (!this.props.successComarchmessage.includes("Error") && !this.props.successComarchmessage.includes("background")) {
        message.success(this.props.successComarchmessage);
      } else if (this.props.successComarchmessage.includes("background")) {
        message.loading(this.props.successComarchmessage, 10);
      } else {
        message.error(this.props.successComarchmessage);
      }
    } else {
      message.error(config.errorStatus);
    }
    this.props.ClearingSuccessJobMessage(config.jobComarchJira);
  };

  /*Defect Message */
  DefectSuccessMessage = () => {
    if (this.props.successdefectmessage !== null && this.props.successdefectmessage !== undefined) {
      this.setState({ defectInprogress: null, defectButton: false });
      if (!this.props.successdefectmessage.includes("Error") && !this.props.successdefectmessage.includes("background")) {
        message.success(this.props.successdefectmessage);
      } else if (this.props.successdefectmessage.includes("background")) {
        message.loading(this.props.successdefectmessage, 10);
      } else {
        message.error(this.props.successdefectmessage);
      }
    } else {
      message.error(config.errorStatus);
    }
    this.props.ClearingSuccessJobMessage(config.jobdefects);
  };

  /*Project Message */
  ProjectSuccessMessage = () => {
    if (this.props.successprojectmessage !== null && this.props.successprojectmessage !== undefined) {
      this.setState({ projectInprogress: null, projectButton: false });
      if (!this.props.successprojectmessage.includes("Error") && !this.props.successprojectmessage.includes("background")) {
        message.success(this.props.successprojectmessage);
      } else if (this.props.successprojectmessage.includes("background")) {
        message.loading(this.props.successprojectmessage, 10);
      }
      else {
        message.error(this.props.successprojectmessage);
      }
    } else {
      message.error(config.errorStatus);
    }
    this.props.ClearingSuccessJobMessage(config.jobprojects);
  };

  /*Release Message */
  ReleaseSuccessMessage = () => {
    if (this.props.successreleasemessage !== null && this.props.successreleasemessage !== undefined) {
      this.setState({ releaseInprogress: null, releaseButton: false });
      if (!this.props.successreleasemessage.includes("Error") && !this.props.successreleasemessage.includes("background")) {
        message.success(this.props.successreleasemessage);
      } else if (this.props.successreleasemessage.includes("background")) {
        message.loading(this.props.successreleasemessage, 10);
      } else {
        message.error(this.props.successreleasemessage);
      }
    } else {
      message.error(config.errorStatus);
    }
    this.props.ClearingSuccessJobMessage(config.jobrelease);
  };

  /*Requirement Message */
  RequirementSuccessMessage = () => {
    if (this.props.successrequirementsmessage !== null && this.props.successrequirementsmessage !== undefined) {
      this.setState({ requirementInprogress: null, requirementButton: false });
      if (!this.props.successrequirementsmessage.includes("Error") && !this.props.successrequirementsmessage.includes("background")) {
        message.success(this.props.successrequirementsmessage);
      } else if (this.props.successrequirementsmessage.includes("background")) {
        message.loading(this.props.successrequirementsmessage, 10);
      } else {
        message.error(this.props.successrequirementsmessage);
      }
    } else {
      message.error(config.errorStatus);
    }
    this.props.ClearingSuccessJobMessage(config.jobrequirements);
  };

  /*TestReport Message */
  TestReportSuccessMessage = () => {
    if (this.props.successtestReportsmessage !== null && this.props.successtestReportsmessage !== undefined) {
      this.setState({ testReportInprogress: null, testReportButton: false });
      if (!this.props.successtestReportsmessage.includes("Error") && !this.props.successtestReportsmessage.includes("background")) {
        message.success(this.props.successtestReportsmessage);
      } else if (this.props.successtestReportsmessage.includes("background")) {
        message.loading(this.props.successtestReportsmessage, 10);
      } else {
        message.error(this.props.successtestReportsmessage);
      }
    } else {
      message.error(config.errorStatus);
    }
    this.props.ClearingSuccessJobMessage(config.jobtestReports);
  };

  /*TestResults Message */
  TestResultSuccessMessage = () => {
    if (this.props.successtestResultsmessage !== null && this.props.successtestResultsmessage !== undefined) {
      this.setState({ testResultsInprogress: null, testResultsButton: false });
      if (!this.props.successtestResultsmessage.includes("Error") && !this.props.successtestResultsmessage.includes("background")) {
        message.success(this.props.successtestResultsmessage);
      } else if (this.props.successtestResultsmessage.includes("background")) {
        message.loading(this.props.successtestResultsmessage, 10);
      } else {
        message.error(this.props.successtestResultsmessage);
      }
    } else {
      message.error(config.errorStatus);
    }
    this.props.ClearingSuccessJobMessage(config.jobtestResults);
  };

  /*UserStory Message */
  UserStorySuccessMessage = () => {
    if (this.props.successuserStorymessage !== null && this.props.successuserStorymessage !== undefined) {
      this.setState({ userStoryInprogress: null, userStoryButton: false });
      if (!this.props.successuserStorymessage.includes("Error") && !this.props.successuserStorymessage.includes("background")) {
        message.success(this.props.successuserStorymessage);
      } else if (this.props.successuserStorymessage.includes("background")) {
        message.loading(this.props.successuserStorymessage, 10);
      } else {
        message.error(this.props.successuserStorymessage);
      }
    } else {
      message.error(config.errorStatus);
    }
    this.props.ClearingSuccessJobMessage(config.jobuserStory);
  };

  /*Disable at Particular time*/
  DisablingFeatureBasedOnAutomaticRun = (mins: any) => {
    if(mins !== undefined){
      if (mins === config.ReleaseTime) {
        this.setState({ automaticreleaseButton: true });
      } else if (mins === config.ProjectsTime) {
        this.setState({ automaticprojectButton: true, automaticreleaseButton: false });
      } else if (mins === config.TestReportsTime) {
        this.setState({ automatictestReportButton: true, automaticprojectButton: false });
      } else if (mins === config.ComponentsTime) {
        this.setState({ automaticcomponentButton: true, automatictestReportButton: false });
      }else if (mins > config.ComponentsTime && mins <= config.ComponentsTime+1) {
        this.setState({ automaticcomponentButton: false });
      }else if (mins === config.DefectsTime) {
        this.setState({ automaticdefectButton: true, automaticcomponentButton: false });
      } else if (mins > config.DefectsTime && mins <= config.DefectsTime+1) {
        this.setState({ automaticdefectButton: false });
      }else if (mins === config.UserStoryTime) {
        this.setState({ automaticuserStoryButton: true, automaticdefectButton: false });
      }else if (mins > config.UserStoryTime && mins <= config.UserStoryTime+1) {
        this.setState({ automaticuserStoryButton: false });
      }else if (mins === config.RequirementsTime) {
        this.setState({ automaticrequirementButton: true, automaticuserStoryButton: false });
      }else if (mins > config.RequirementsTime && mins <= config.RequirementsTime+1) {
        this.setState({ automaticrequirementButton: false });
      }else if (mins === config.TestResultsTime) {
        this.setState({ automatictestResultsButton: true, automaticrequirementButton: false });
      }
      else {
        if (mins === config.TestResultsTime + 1) {
          this.setState({ automatictestResultsButton: false, });
        }
      }
    }
  }

  /*Modal Pop up */

  ModalView = () => {
    this.setState({ modalVisible: false,comarchupdateInprogress: null,comarchButton: false });
    this.props.form.resetFields();
  };
  CancelModal = () => {
    this.setState({ modalVisible: false,comarchupdateInprogress: null,comarchButton: false });
    this.props.form.resetFields()
  };

  handleSubmit = (e: any) => {
    e.preventDefault();
    this.props.SettingComarchMessage(config.updateComarchJira)
    this.setState({ modalVisible: false,comarchButton:true,comarchupdateInprogress:config.jobStatus});
    this.props.form.validateFieldsAndScroll((err: any, values: any) => {
      if (!err) {
     this.props.RetriveComarchData(values);
      }
    });
    this.props.form.resetFields()
  };

  render() {
    let minLoop = undefined;
    var that = this;
    const { getFieldDecorator } = this.props.form;
    setInterval(function () {
      minLoop = minsGen();
      that.DisablingFeatureBasedOnAutomaticRun(minLoop);
    }, 1000);
    if (this.props.componentmessage !== null) {
      this.ComponentMessage();
    }
    if (this.props.comarchmessage !== null) {
      this.ComarchMessage();
    }
    if (this.props.defectmessage !== null) {
      this.DefectMessage();
    }
    if (this.props.projectmessage !== null) {
      this.ProjectMessage();
    }
    if (this.props.releasemessage !== null) {
      this.ReleaseMessage();
    }
    if (this.props.requirementsmessage !== null) {
      this.RequirementMessage();
    }
    if (this.props.testReportsmessage !== null) {
      this.TestReportMessage();
    }
    if (this.props.testResultsmessage !== null) {
      this.TestResultMessage();
    }
    if (this.props.userStorymessage !== null) {
      this.UserStoryMessage();
    }
    /*Calling Success Message*/
    if (this.props.successcomponentmessage !== null) {
      this.ComponentSuccessMessage();
    }
    if (this.props.successComarchmessage !== null) {
      this.ComarchSuccessMessage();
    }
    if (this.props.successdefectmessage !== null) {
      this.DefectSuccessMessage();
    }
    if (this.props.successprojectmessage !== null) {
      this.ProjectSuccessMessage();
    }
    if (this.props.successreleasemessage !== null) {
      this.ReleaseSuccessMessage();
    }
    if (this.props.successrequirementsmessage !== null) {
      this.RequirementSuccessMessage();
    }
    if (this.props.successtestReportsmessage !== null) {
      this.TestReportSuccessMessage();
    }
    if (this.props.successtestResultsmessage !== null) {
      this.TestResultSuccessMessage();
    }
    if (this.props.successuserStorymessage !== null) {
      this.UserStorySuccessMessage();
    }
    /*Table data construction*/
    // let mycolumns = this.state.columns.map((col, index) => ({
    //   ...col,
    //   onHeaderCell: (column) => ({
    //     width: column.width,
    //   }),
    // }));
    const dataItems = [];
    if (this.props.alljobs) {
      let sno = 1;
      this.props.alljobs.forEach((element) => {
        dataItems.push({
          serialNumber: sno,
          jobName: element.jobName,
          jobDescription: element.jobDescription,
          jobUrl: element.jobUrl,
          ManualRuntime: this.dateFormatting(element.ManualRuntime),
          LastRunTime: this.dateFormatting(element.LastRunTime),
          // ManualRuntime: element.ManualRuntime.toISOString(),
          // LastRunTime: element.LastRunTime,
        });
        sno++;
      });
    }
    return (
      <div className="container-flex page-wrapper p-3">
        <div className="content-title pt-0">
          <Text>{config.welcometext}<SmileTwoTone className="datauploads-icons" twoToneColor="#b30838" /> {config.welcome}</Text>
        </div>
        <div className="warning-txt">
          <Text className="warning-msg blink"><mark>{config.warningtext}</mark></Text>
        </div>
        <div className="summary_table table_Wid_full">
          <div className="row content-align header-box">
            <div className="col-md-1">
              <Text className="exp-text">S.No</Text>
            </div>
            <div className="col-md-2">
              <Text className="exp-text">JobName</Text>
            </div>
            <div className="col-md-2">
              <Text className="exp-text">Manual Runtime</Text>
            </div>
            <div className="col-md-2">
              <Text className="exp-text">Last Runtime(Automatic)</Text>
            </div>
            <div className="col-md-2">
              <Text className="exp-text">Run Job</Text>
            </div>
            <div className="col-md-2">
              <Text className="exp-text">Remarks</Text>
            </div>
          </div>
          <div className="content-border">
            {dataItems.map((data: any, index: any) => {
              return (
                <div className="row content-align">
                  <div className="col-md-1">
                    <Text>{data.serialNumber}</Text>
                  </div>
                  <div className="col-md-2">
                    <Text>{data.jobName}</Text>
                  </div>
                  <div className="col-md-2">
                    <Text>{data.ManualRuntime}</Text>
                  </div>
                  <div className="col-md-2">
                    <Text>{data.LastRunTime}</Text>
                  </div>
                  <div className="col-md-2">
                  {config.jobComarchJira === data.jobName ? (
                      <Button
                        type="primary"
                        disabled={this.state.comarchButton}
                        onClick={(event: any) =>
                          this.dataUploads(data.jobUrl, data.jobName)
                        }
                      >
                        Click Here
                      </Button>
                    ) : null}
                    {config.jobcomponents === data.jobName ? (
                      <Button
                        type="primary"
                        // disabled={this.state.automaticcomponentButton ?( this.state.automaticcomponentButton):(this.state.componentButton)}
                        onClick={(event: any) =>
                          this.dataUploads(data.jobUrl, data.jobName)
                        }
                      >
                        Click Here
                      </Button>
                    ) : null}
                    {config.jobdefects === data.jobName ? (
                      <Button
                        type="primary"
                        disabled={this.state.automaticdefectButton ?( this.state.automaticdefectButton):(this.state.defectButton)}
                        onClick={(event: any) =>
                          this.dataUploads(data.jobUrl, data.jobName)
                        }
                      >
                        Click Here
                      </Button>
                    ) : null}
                    {config.jobprojects === data.jobName ? (
                      <Button
                        type="primary"
                        disabled={this.state.automaticprojectButton ?( this.state.automaticprojectButton):(this.state.projectButton)}
                        onClick={(event: any) =>
                          this.dataUploads(data.jobUrl, data.jobName)
                        }
                      >
                        Click Here
                      </Button>
                    ) : null}
                    {config.jobrelease === data.jobName ? (
                      <Button
                        type="primary"
                        disabled={this.state.automaticreleaseButton ?( this.state.automaticreleaseButton):(this.state.releaseButton)}
                        onClick={(event: any) =>
                          this.dataUploads(data.jobUrl, data.jobName)
                        }
                      >
                        Click Here
                      </Button>
                    ) : null}
                    {config.jobrequirements === data.jobName ? (
                      <Button
                        type="primary"
                        disabled={this.state.automaticrequirementButton ?( this.state.automaticrequirementButton):(this.state.requirementButton)}
                        onClick={(event: any) =>
                          this.dataUploads(data.jobUrl, data.jobName)
                        }
                      >
                        Click Here
                      </Button>
                    ) : null}
                    {config.jobtestReports === data.jobName ? (
                      <Button
                        type="primary"
                        disabled={this.state.automatictestReportButton ?( this.state.automatictestReportButton):(this.state.testReportButton)}
                        onClick={(event: any) =>
                          this.dataUploads(data.jobUrl, data.jobName)
                        }
                      >
                        Click Here
                      </Button>
                    ) : null}
                    {config.jobtestResults === data.jobName ? (
                      <Button
                        type="primary"
                        disabled={this.state.automatictestResultsButton ?( this.state.automatictestResultsButton):(this.state.testResultsButton)}
                        onClick={(event: any) =>
                          this.dataUploads(data.jobUrl, data.jobName)
                        }
                      >
                        Click Here
                      </Button>
                    ) : null}
                    {config.jobuserStory === data.jobName ? (
                      <Button
                        type="primary"
                        disabled={this.state.automaticuserStoryButton ?( this.state.automaticuserStoryButton):(this.state.userStoryButton)}
                        onClick={(event: any) =>
                          this.dataUploads(data.jobUrl, data.jobName)
                        }
                      >
                        Click Here
                      </Button>
                    ) : null}
                  </div>
                  <div className="col-md-2">
                  {config.jobComarchJira === data.jobName && this.state.comarchupdateInprogress !== null ? (
                      <Text className="blink blink-msg">{config.jobStatus}</Text>
                    ) : null}
                    {config.jobcomponents === data.jobName && this.state.componentInprogress !== null ? (
                      <Text className="blink blink-msg">{config.jobStatus}</Text>
                    ) : null}
                    {config.jobdefects === data.jobName && this.state.defectInprogress !== null ? (
                      <Text className="blink blink-msg">{config.jobStatus}</Text>
                    ) : null}
                    {config.jobprojects === data.jobName && this.state.projectInprogress !== null ? (
                      <Text className="blink blink-msg">{config.jobStatus}</Text>
                    ) : null}
                    {config.jobrelease === data.jobName && this.state.releaseInprogress !== null ? (
                      <Text className="blink blink-msg">{config.jobStatus}</Text>
                    ) : null}
                    {config.jobrequirements === data.jobName && this.state.requirementInprogress !== null ? (
                      <Text className="blink blink-msg">{config.jobStatus}</Text>
                    ) : null}
                    {config.jobtestReports === data.jobName && this.state.testReportInprogress !== null ? (
                      <Text className="blink blink-msg">{config.jobStatus}</Text>
                    ) : null}
                    {config.jobtestResults === data.jobName && this.state.testResultsInprogress !== null ? (
                      <Text className="blink blink-msg">{config.jobStatus}</Text>
                    ) : null}
                    {config.jobuserStory === data.jobName && this.state.userStoryInprogress !== null ? (
                      <Text className="blink blink-msg">{config.jobStatus}</Text>
                    ) : null}
                     {config.jobcomponents === data.jobName && this.state.automaticcomponentButton ? (
                      <Text className="blink blink-msg">{config.AutojobStatus}</Text>
                    ) : null}
                    {config.jobdefects === data.jobName && this.state.automaticdefectButton ? (
                      <Text className="blink blink-msg">{config.AutojobStatus}</Text>
                    ) : null}
                    {config.jobprojects === data.jobName && this.state.automaticprojectButton ? (
                      <Text className="blink blink-msg">{config.AutojobStatus}</Text>
                    ) : null}
                    {config.jobrelease === data.jobName && this.state.automaticreleaseButton ? (
                      <Text className="blink blink-msg">{config.AutojobStatus}</Text>
                    ) : null}
                    {config.jobrequirements === data.jobName && this.state.automaticrequirementButton ? (
                      <Text className="blink blink-msg">{config.AutojobStatus}</Text>
                    ) : null}
                    {config.jobtestReports === data.jobName && this.state.automatictestReportButton ? (
                      <Text className="blink blink-msg">{config.AutojobStatus}</Text>
                    ) : null}
                    {config.jobtestResults === data.jobName && this.state.automatictestResultsButton ? (
                      <Text className="blink blink-msg">{config.AutojobStatus}</Text>
                    ) : null}
                    {config.jobuserStory === data.jobName && this.state.automaticuserStoryButton ? (
                      <Text className="blink blink-msg">{config.AutojobStatus}</Text>
                    ) : null}
                  </div>
                </div>
              );
            })}
          </div>
          <Modal
            title="Comarch Jira Data Uploading"
            centered
            visible={this.state.modalVisible}
            onOk={() => this.ModalView()}
            onCancel={() => this.CancelModal()}
          >
            {" "}
            <p className="sm-report-terms-condition">
              &nbsp; Enter your Comarch Jira Credentials to start the update
            </p>
            <div className="sm-modal-content">
            <Form onSubmit={this.handleSubmit} className="comarch_Jira">
                <div className="form-group mail-box">
                  <div className="form-info">
                    <label htmlFor="email">Username</label>
                    <Form.Item>
                      {getFieldDecorator("ComarchuserName",
                       {
                        rules: [
                          {
                            message: "Please enter username",
                            required: true,
                          },
                          {
                            max: 100,
                            message:
                              "Email should be accept maximum 35 characters",
                          },
                          {
                            message: "Email should be minimum 4 characters",
                            min: 4,
                          },
                        ],
                      })(
                        <Input
                          className="form-control"
                          id="Comarch_email"
                          name="Comarc_hemail"
                          placeholder="Enter email address"
                          allowClear
                        />
                      )}
                    </Form.Item>
                  </div>
                </div>

                <div className="form-group password-box">
                  <div className="form-info">
                    <label htmlFor="input">Password</label>
                    <Form.Item>
                      {getFieldDecorator("Comarchpassword", {
                        rules: [
                          {
                            message: "Please enter Password",
                            required: true
                          },
                          {
                            max: 25,
                            message:
                              "Password should be accept maximum 25 characters",
                          },
                          {
                            message: "Password should be minimum 5 characters",
                            min: 5,
                          },
                        ],
                      })(
                          <Input.Password
                            className="form-control"
                            id="comarch_password"
                            name="comarch_password"
                            placeholder="Enter password"
                          />
                      )}
                    </Form.Item>
                  </div>
                </div>
                <div className="btn-action">
                  <Button
                    className="modal-btn"
                    htmlType="submit"
                    // loading={this.state.loading}
                  >
                    Submit
                  </Button>
                </div>
              </Form>
            </div>
          </Modal>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  loginData: state.loginData,
  alljobs: state.datauploadsReducer.alljobs,
  usermessage: state.datauploadsReducer.usermessage,
  componentmessage: state.datauploadsReducer.componentmessage,
  defectmessage: state.datauploadsReducer.defectmessage,
  projectmessage: state.datauploadsReducer.projectmessage,
  releasemessage: state.datauploadsReducer.releasemessage,
  requirementsmessage: state.datauploadsReducer.requirementsmessage,
  testReportsmessage: state.datauploadsReducer.testReportsmessage,
  testResultsmessage: state.datauploadsReducer.testResultsmessage,
  comarchmessage: state.datauploadsReducer.comarchmessage,
  userStorymessage: state.datauploadsReducer.userStorymessage,
  successcomponentmessage: state.datauploadsReducer.successcomponentmessage,
  successComarchmessage: state.datauploadsReducer.comarchResponse,
  successdefectmessage: state.datauploadsReducer.successdefectmessage,
  successprojectmessage: state.datauploadsReducer.successprojectmessage,
  successreleasemessage: state.datauploadsReducer.successreleasemessage,
  successrequirementsmessage: state.datauploadsReducer.successrequirementsmessage,
  successtestReportsmessage: state.datauploadsReducer.successtestReportsmessage,
  successtestResultsmessage: state.datauploadsReducer.successtestResultsmessage,
  successuserStorymessage: state.datauploadsReducer.successuserStorymessage,
});

const WrappedCreateDatauploads = Form.create({ name: "Datauploads" })(
  Datauploads
);

export default connect(mapStateToProps, {
  getAllJobs,
  CallDataUploads,
  ClearingMessage,
  SettingComponentMessage,
  SettingDefectsMessage,
  SettingProjectMessage,
  SettingReleaseMessage,
  SettingRequirementsMessage,
  SettingTestReportsMessage,
  SettingTestResultsMessage,
  SettinguserStoryMessage,
  ClearingJobMessage,
  ClearingSuccessJobMessage,
  RetriveComarchData,
  SettingComarchMessage
})(WrappedCreateDatauploads);
