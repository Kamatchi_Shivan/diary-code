import React from "react";
import { Redirect } from "react-router-dom";

// Layout Types
import { DefaultLayout, LoginLayout } from "./layouts";

// Route Views
import Dashboard from "./views/dashboard/Dashboard";
import LoginApp from "./components/login/login";
import Errors from "./views/errors/Errors";

 const routes =  [
  {
    path: "/",
    exact: true,
    layout: DefaultLayout,
    component: () => <Redirect to="/login" />
  },
  {
    path: "/login",
    layout: LoginLayout,
    component: LoginApp
  },
  {
    path: "/home",
    layout: DefaultLayout,
    component: Dashboard
  },
  {
    path: "/errors",
    layout: DefaultLayout,
    component: Errors
  }
];

export default routes;