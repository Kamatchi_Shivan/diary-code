import React, { Component } from 'react'

export default class Browsersupport extends Component {
  render() {
    return (
      <div>
        <h1>DIARY is not supported in IE Browser / IE Edge (Legacy Version) </h1>
        <h3>Please use one of the following browsers Google Chrome or Firefox or MS Edge (Chromium Version). </h3>
      </div>
    )
  }
}
