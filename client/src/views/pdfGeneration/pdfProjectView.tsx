import React, { Component } from "react";
import {
  Table,
  Form,
  Button,
  Typography,
  message,
  BackTop,
  Tooltip,
  Select
} from "antd";
import { connect } from "react-redux";
import { Bar } from "react-chartjs-2";
import {
  DefectByStatusCharts,
  DefectByAssigneeCharts,
  DefectByComponentCharts,
  DefectByDomainsCharts,
  InitialDefectByAssigneeCharts,
  StatusChartsByProjectLevel,
  InitialStatusChartsByProjectLevel,
} from "../chartFunctions/defectChartFunctions";
import { TestExecutionStatus } from "../chartFunctions/testExecutionFunction";
import Global from "../../components/layout/Global/Global";
import {
  getChartData,
  DynamicTestExecutionStatusForthLevel,
  DynamicTestExecutionStatus,
  FilterValidation, FilterValidationForBussinessDomain
} from "../chartFunctions/testExecutionFunction";
import { PdfComponentStore } from "../../redux/actions/testExecution/testExecutionAction";
import Highcharts from "highcharts";
import drilldown from "highcharts/modules/drilldown.js";
import HighchartsReact from "highcharts-react-official";
import { RequirementCoverageMainFunction } from "../chartFunctions/RequirementCoverageFunctions";
import { UserStoriesFunction } from "../chartFunctions/UserStoriesFunction";
import SmileyComponent from "../../components/layout/Global/SmileyComponent";
import { config } from "../../config/configs";
import { NavigatorSet } from "../dashboard/Dashboard";
import NoDataToDisplay from "highcharts/modules/no-data-to-display";
import { pdfPreviewSelection } from "../../redux/actions/headers/headersAction";
import {
  storeDocType,
  storeGateVersion
} from "../../redux/actions/projectview/projectViewAction";
import JsPDF from "jspdf";
import html2canvas from "html2canvas";
import {
  MailingPDF,
  MergingPdf,
  ClearingPdfMessage,
  StoringInNetworkPath,
  ClearingNetworkPdfMessage,
  DefectsTogglePDF,
  PdfDefectsCategories,
} from "../../redux/actions/pdf/pdfAction";
import { drawDOM, exportPDF } from "@progress/kendo-drawing";
import { BussinessDomain, usergroup } from '../projectview/staticFilters'
import { MailOutlined, CloseCircleOutlined, DownloadOutlined } from "@ant-design/icons";
import * as folderset from "../chartFunctions/TestExecutionFolderSet";
import { clearTestLevels} from "../projectview/clearChartLevels";
drilldown(Highcharts);
NoDataToDisplay(Highcharts);

/*Global Variable Declaration section*/
const { Text } = Typography;
const { Option } = Select;
const key = "updatable";
let releaseURl = "";
let projectRelease = "";
let TestDefectCollections = [];
let captureDataset = "";
const initialDataset = "projects";
let ReqDataset = "";
let count = 1;
let bcount = 1;
let reqcount = 1;
let storycount = 1;
let TempComponentData;
let changeComponentData;
let TempReleaseData;
let riskIssues = "";
let DetailedTestStatus = "";
let DateValue;
let SharepointPDF;
let base64data: any;
let basedata: any;
let reportbasedata: any;
let independantLoad = true;
let independantLoadTestExecution = true;
let independantLoadRequirment = true;
let independantLoadStory = true;
let projectDatavalidation = true;
let openDefects = "";
let allDefects = "active";
let storyDefects = "";
let captureDatasetlevel3 = "";
let captureDatasetlevel4 = "";
let captureDatasetlevel5 = "";
let captureDatasetlevel6 = "";
let captureDatasetlevel7 = "";
let captureDatasetlevel8 = "";
let captureDatasetlevel9 = "";
let captureDatasetlevel10 = "";
let captureDatasetlevel11 = "";
// let Components = {
//   epicLink:"All Epic's",
//   epicName: "All Epic's"
// };
let metaData: any;

/* Date Formatting Function */
const ValidateDate = (data) => {
  if (data < 10) {
    data = "0" + data;
  }
  return data;
};

/*Declaring Props Variable*/
type MyProps = {
  DefectByStatusCharts: any;
  DefectByComponentCharts: any;
  DefectByDomainsCharts: any;
  DefectByAssigneeCharts: any;
  defectByAssignee: any;
  defectByDomain: any;
  defectByComponent: any;
  defectByStatus: any;
  milestoneDatas: any;
  summaryDatas: any;
  summaryProjectDatas: any;
  projectViewData: any;
  InitialDefectByAssigneeCharts: any;
  InitialStatusChartsByProjectLevel: any;
  projectDatas: any;
  allDefectData: any;
  releaseHistory: any;
  StatusChartsByProjectLevel: any;
  TestExecutionStatus: any;
  testResults: any;
  getChartData: any;
  componentData: any;
  requirementData: any;
  userStoriesData: any;
  PdfComponentStore: any;
  detailView: any;
  MailingPDF: any;
  MergingPdf: any;
  ClearingPdfMessage: any;
  mergemsg: any;
  StoringInNetworkPath: any;
  networkmsg: any;
  ClearingNetworkPdfMessage: any;
  gateversion: any;
  doctype: any;
  DefectsTogglePDF: any;
  pdfToggle: any;
  pdfPreviewSelection: any;
  PdfDefectsCategories: any;
  defectToggle: any;
  DynamicTestExecutionStatus: any;
  storeDocType: any;
  storeGateVersion: any;
  ExternaltestReport: any;
};

/*Declaring State Variable*/
type MyState = {
  columns: any;
  DefectData: any;
  TestExecutionData: Object;
  curTime: any;
  curdatetime: any;
  previousPage: any;
  currentPage: any;
  series: any;
  secondseries: any;
  thirdseries: any;
  forthseries: any;
  fifthseries: any;
  sixthseries: any;
  seventhseries: any;
  eigthseries: any;
  ninthseries: any;
  requirementSeries: any;
  UserstoriesSeries: any;
  AssigneeButton: any;
  StatusButton: any;
  ComponentButton: any;
  RequirementButton: any;
  TestCaseButton: any;
  TestExectionButton: any;
  UserStoriesButton: any;
  RequirementCoverageBack: any;
  UserStoriesBack: any;
  isTestCase: any;
  isUserStories: any;
  Inpercent: any;
  InpercentButton: any;
  InNumbermbers: any;
  InNumbermbersButton: any;
  HideInPercentNumbers: any;
  RequirementInpercent: boolean;
  UserStoriesInpercent: boolean;
  RequirementInNumbermbersButton: any;
  RequirementInpercentButton: any;
  UserstoriesInNumbermbersButton: any;
  UserstoriesInpercentButton: any;
  RequirementHideInPercentNumbers: any;
  UserStoryHideInPercentNumbers: any;
  testreport: any;
  storyType: any;
  testType: any;
  confirmPdf: boolean;
  userStorybtn: any;
  staticFilter: any;
  staticData: any;
  businessDomain: any;
};

class ProjectViewPdf extends Component<MyProps, MyState> {
  pdfExportComponent;
  table;
  _element = null;
  _report = null;

  componentWillMount() {
    NavigatorSet();
    this.props.pdfPreviewSelection(false);
    this.props.DefectsTogglePDF(config.allDefects);
    this.props.PdfDefectsCategories(config.allDefects);
    this.props.ClearingPdfMessage();
    this.props.ClearingNetworkPdfMessage();
    basedata = undefined;
    base64data = undefined;
    reportbasedata = undefined;
    let myLoc = JSON.stringify(window.location.pathname);
    let finalstr = myLoc.replace(/"/g, "");
    releaseURl = finalstr.substring(finalstr.lastIndexOf("/") + 1);
    let split = myLoc.split("/");
    projectRelease = split[2];
    TempComponentData = this.props.componentData;
    changeComponentData = this.props.componentData;
    TempReleaseData = this.props.projectViewData;
    if (this.props.doctype === "") {
      this.props.storeDocType("TSR");
      this.props.storeGateVersion("G1");
    }
    if (window.location.href.includes("pdfview")) {
      window.addEventListener("popstate", (event) => {
        Global.history.push(
          "/projectview/" + projectRelease + "/" + releaseURl
        );
      });
    }
  }

  constructor(props: any) {
    super(props);
    this.handleBack = this.handleBack.bind(this);
    this.requirementBack = this.requirementBack.bind(this);
    this.storiesBack = this.storiesBack.bind(this);
    const today = new Date(),
      date =
        today.getFullYear() +
        "-" +
        ValidateDate(today.getMonth() + 1) +
        "-" +
        ValidateDate(today.getDate()),
      datetime =
        date +
        " " +
        ValidateDate(today.getHours()) +
        ":" +
        ValidateDate(today.getMinutes());
    this.state = {
      Inpercent: false,
      RequirementInpercent: false,
      userStorybtn: config.stories,
      UserStoriesInpercent: false,
      InpercentButton: "",
      InNumbermbersButton: "active",
      InNumbermbers: false,
      HideInPercentNumbers: true,
      RequirementHideInPercentNumbers: true,
      UserStoryHideInPercentNumbers: true,
      AssigneeButton: "",
      StatusButton: "",
      ComponentButton: "",
      previousPage: "",
      RequirementButton: "",
      UserStoriesButton: "",
      TestCaseButton: "active",
      TestExectionButton: false,
      RequirementCoverageBack: false,
      UserStoriesBack: false,
      isTestCase: true,
      isUserStories: false,
      currentPage: initialDataset,
      series: [],
      secondseries: [],
      requirementSeries: [],
      UserstoriesSeries: [],
      thirdseries: [],
      forthseries: [],
      fifthseries: [],
      sixthseries: [],
      seventhseries: [],
      eigthseries: [],
      ninthseries: [],
      RequirementInNumbermbersButton: "active",
      RequirementInpercentButton: "",
      UserstoriesInNumbermbersButton: "active",
      UserstoriesInpercentButton: "",
      storyType: config.allstories,
      testType: config.alltests,
      confirmPdf: false,
      staticFilter: false,
      businessDomain: false,
      staticData: config.all,
      columns: [
        {
          title: "Project Name",
          dataIndex: "ProjectName",
          key: "ProjectName",
          render: (text, record) => {
            return <Text className="tabletd-pdf ">{text}</Text>;
          },
          width: "20%",
        },
        {
          title: "Release Name",
          dataIndex: "releaseName",
          key: "releaseName",
          render: (text, record) => {
            return <Text className="tabletd-pdf ">{text}</Text>;
          },
          width: "10%",
        },
        {
          title: "Status",
          dataIndex: "Status_Go",
          key: "Status_Go",
          render: (text, record) => {
            return <Text className="pdf-tablesmile">{text}</Text>;
          },
          width: "5%",
        },
        {
          title: "Test Key Message",
          dataIndex: "Test_Status",
          key: "Test_Status",
          render: (text, record) => {
            return (
              <Text>
                <div dangerouslySetInnerHTML={{ __html: text }}></div>
              </Text>
            );
          },
          // width: "40%",
        },
      ],
      DefectData: {},
      TestExecutionData: {},
      curTime: date,
      curdatetime: datetime,
      testreport: "Test Status Report - " + datetime,
    };
  }

  /*Redirecting to project view*/
  redirectProjectView = () => {
    Global.history.push("/projectview/" + projectRelease + "/" + releaseURl);
  };

  /*Open Defects - By Status Function*/
  DefectByStatus = (type, defectType) => {
    this.props.DefectsTogglePDF(type);
    this.props.PdfDefectsCategories(defectType);
    let DefectItem = {};
    DefectItem = this.props.StatusChartsByProjectLevel(
      this.props.defectByStatus,
      this.props.projectViewData,
      this.props.componentData,
      TestDefectCollections,
      type,
      defectType
    );
    if (defectType === config.allDefects) {
      openDefects = "";
      storyDefects = "";
      allDefects = "active";
    } else if (defectType === config.storyDefects) {
      openDefects = "";
      storyDefects = "active";
      allDefects = "";
    } else {
      openDefects = "active";
      allDefects = "";
      storyDefects = "";
    }
    this.setState({
      DefectData: DefectItem,
      AssigneeButton: "",
      StatusButton: "active",
      ComponentButton: "",
    });
  };

  /*Open Defects - By Assignee Function*/
  DefectByAssignee = (type, defectType) => {
    this.props.DefectsTogglePDF(type);
    this.props.PdfDefectsCategories(defectType);
    let DefectItem = {};
    DefectItem = this.props.DefectByAssigneeCharts(
      this.props.defectByAssignee,
      this.props.projectViewData,
      this.props.componentData,
      TestDefectCollections,
      type,
      defectType
    );
    if (defectType === config.allDefects) {
      openDefects = "";
      storyDefects = "";
      allDefects = "active";
    } else if (defectType === config.storyDefects) {
      openDefects = "";
      storyDefects = "active";
      allDefects = "";
    } else {
      openDefects = "active";
      allDefects = "";
      storyDefects = "";
    }
    this.setState({
      DefectData: DefectItem,
      AssigneeButton: "active",
      StatusButton: "",
      ComponentButton: "",
    });
  };

  /*Open Defects - Intial Defects Loading*/
  InitialDefectByStatus = () => {
    if (this.props.projectDatas.length !== 0) {
      TestDefectCollections = [];
      let DefectItem = {};
      let projectobj = this.props.projectDatas.filter((project) => {
        return (
          releaseURl === project.ProjectReference &&
          projectRelease === project.ReleaseName
        );
      });
      if (projectobj.length !== 0) {
        DefectItem = this.props.InitialStatusChartsByProjectLevel(
          this.props.defectByStatus,
          projectobj[0],
          TestDefectCollections,
          this.props.componentData
        );
        this.setState({
          DefectData: DefectItem,
          AssigneeButton: "",
          StatusButton: "active",
          ComponentButton: "",
          isUserStories: false,
          isTestCase: true,
        });
      }
    }
  };

  /*Open Defects - By Component Function*/
  DefectByComponent = (type, defectType) => {
    let DefectItem = {};
    this.props.DefectsTogglePDF(type);
    this.props.PdfDefectsCategories(defectType);
    DefectItem = this.props.DefectByComponentCharts(
      this.props.defectByComponent,
      this.props.projectViewData,
      this.props.componentData,
      TestDefectCollections,
      type,
      defectType
    );
    if (defectType === config.allDefects) {
      openDefects = "";
      storyDefects = "";
      allDefects = "active";
    } else if (defectType === config.storyDefects) {
      openDefects = "";
      storyDefects = "active";
      allDefects = "";
    } else {
      openDefects = "active";
      allDefects = "";
      storyDefects = "";
    }
    this.setState({
      DefectData: DefectItem,
      AssigneeButton: "",
      StatusButton: "",
      ComponentButton: "active",
    });
  };

  /*Defect Chart options */
  options: Object = {
    maintainAspectRatio: false,
    plugins: {
      datalabels: {
        color: "black",
        display: function (context) {
          return context.dataset.data[context.dataIndex] !== 0; // or >= 1 or ...
        },
      },
    },
    legend: {
      position: "bottom",
    },
    scales: {
      xAxes: [
        {
          stacked: true,
          gridLines: {
            display: false,
          },
          ticks: {
            callback: function (val) {
              return val !== null && val !== undefined && val.length > 30
                ? val.substring(0, 30) + "..."
                : val;
            },
          },
        },
      ],
      yAxes: [
        {
          stacked: true,
          gridLines: {
            display: false,
          },
          ticks: {
            beginAtZero: true,
            callback: function (value) {
              if (value % 1 === 0) {
                return value;
              }
            },
          },
        },
      ],
    },
  };

  /*Validating Project Data for Empty Handling*/
  validatingProjectData = () => {
    if (
      this.props.projectViewData.length === 0 ||
      this.props.projectViewData[0] === undefined ||
      this.props.projectViewData[0] === null ||
      Object.keys(this.props.projectViewData[0]).length === 0
    ) {
      return false;
    } else {
      return true;
    }
  };

  validatingProjectData_KeyRemarksTesting = () => {
    if (
      this.props.projectViewData[0].KeyRemarksTesting === undefined ||
      this.props.projectViewData[0].KeyRemarksTesting === null ||
      Object.keys(this.props.projectViewData[0].KeyRemarksTesting).length === 0
    ) {
      return false;
    } else {
      return true;
    }
  };

  validatingProjectData_OpenRiskAndIssues = () => {
    if (this.props.projectViewData.length !== 0) {
      if (
        this.props.projectViewData[0].Open_Risks_Issues === undefined ||
        this.props.projectViewData[0].Open_Risks_Issues === null ||
        Object.keys(this.props.projectViewData[0].Open_Risks_Issues).length === 0 ||
        this.props.projectViewData[0].Open_Risks_Issues === ""
      ) {
        riskIssues = "";
        return false;
      } else {
        riskIssues = this.props.projectViewData[0].Open_Risks_Issues;
        return true;
      }
    } else {
      riskIssues = "";
      return false;
    }
  };

  validatingProjectData_DetailedTestStatus = () => {
    if (
      this.props.projectViewData.length === 0 ||
      this.props.projectViewData[0].DetailedTestStatus === undefined ||
      this.props.projectViewData[0].DetailedTestStatus === null ||
      Object.keys(this.props.projectViewData[0].DetailedTestStatus).length === 0 ||
      this.props.projectViewData[0].DetailedTestStatus === ""
    ) {
      DetailedTestStatus = "";
      return false;
    } else {
      DetailedTestStatus = this.props.projectViewData[0].DetailedTestStatus;
      DetailedTestStatus = DetailedTestStatus.replace(/<img[^>]*>/g, "");
      return true;
    }
  };

  /*Matching Project Reference for component rendering*/
  matchingUrl = () => {
    let myLoc = JSON.stringify(window.location.pathname);
    let finalstr = myLoc.replace(/"/g, "");
    let newUrl = finalstr.substring(finalstr.lastIndexOf("/") + 1);
    if (releaseURl !== newUrl) {
      releaseURl = newUrl;
      return true;
    } else {
      return false;
    }
  };

  /*Test Execution - Test Case Function*/
  TestExecutionStatus = (type, staticFilters) => {
    count = 1;
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (projectobj.length !== 0) {
      let res = projectobj[0].ReleaseName.split(".");
      let releaseInfo = "";
      res.forEach((data) => {
        releaseInfo = releaseInfo + data + "-";
      });
      metaData = {
        Releasemoment: projectobj[0].ReleaseName,
        Projectname: projectobj[0].ProjectName,
        PlanviewID: projectobj[0].ProjectReference,
      }
      let projectName = projectobj[0].ProjectName.replace(".", " ");
      if (this.props.projectViewData.length !== 0) {
        DateValue =
          releaseInfo +
          projectobj[0].ProjectReference +
          "-" +
          projectName +
          "-" +
          this.props.projectViewData[0].JIRA_KEY +
          "-" +
          this.props.doctype +
          "-" +
          this.props.gateversion +
          "-" +
          this.state.curdatetime;
        SharepointPDF = releaseInfo +
          projectobj[0].ProjectReference +
          "-" +
          projectName +
          "-" +
          this.props.projectViewData[0].JIRA_KEY +
          "-" +
          this.props.doctype +
          "-" +
          this.props.gateversion
          + "-" +
          this.state.curTime;
      } else {
        DateValue =
          releaseInfo +
          projectobj[0].ProjectReference +
          "-" +
          projectobj[0].ProjectName +
          "-" +
          this.props.doctype +
          "-" +
          this.props.gateversion +
          "-" +
          this.state.curdatetime;
        SharepointPDF = releaseInfo +
          projectobj[0].ProjectReference +
          "-" +
          projectobj[0].ProjectName +
          "-" +
          this.props.doctype +
          "-" +
          this.props.gateversion +
          "-" +
          this.state.curTime;;
      }
      let regExpr = /[^a-zA-Z0-9-. ]/g;
      DateValue = DateValue.replace(regExpr, " ");
      SharepointPDF = SharepointPDF.replace(regExpr, " ");
      let TestItem = this.props.getChartData(
        initialDataset,
        this.props.testResults,
        projectobj[0],
        count,
        this.props.allDefectData,
        captureDataset,
        type,
        this.props.componentData,
        staticFilters
      );
      this.setState({
        staticFilter: (FilterValidation(this.props.testResults,
          projectobj[0])),
        businessDomain: (FilterValidationForBussinessDomain(this.props.testResults,
          projectobj[0]))
      })
      if (TestItem.TestData.length > 0) {
        this.setState({
          HideInPercentNumbers: true,
        });
      } else {
        if (type !== config.alltests) {
          this.setState({
            HideInPercentNumbers: true,
          });
        } else {
          this.setState({
            HideInPercentNumbers: false,
          });
        }
      }
      this.setState({
        series: TestItem.TestData,
        RequirementButton: "",
        TestCaseButton: "active",
        UserStoriesButton: "",
        isUserStories: false,
        isTestCase: true,
        TestExectionButton: false,
        UserStoriesBack: false,
        InpercentButton: "",
        InNumbermbersButton: "active",
        Inpercent: false,
        InNumbermbers: false,
      });
    }
  };

  /*Dynamic Test Execution - Test Case Function*/
  DynamicTestExecutionStatus = (count, type, staticData) => {
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (projectobj.length !== 0) {
      if (count === 2) {
        let TestItem = this.props.getChartData(
          config.functionaltesting,
          this.props.testResults,
          projectobj[0],
          count,
          this.props.allDefectData,
          captureDataset,
          type,
          this.props.componentData,
          staticData
        );
        if (TestItem.TestData !== 0) {
          this.setState({
            thirdseries: TestItem.TestData,
          });
        } else {
          this.setState({
            thirdseries: [],
          });
        }
      } else {
        let TestItem = this.props.getChartData(
          projectobj[0].ProjectName,
          this.props.testResults,
          projectobj[0],
          count,
          this.props.allDefectData,
          captureDataset,
          type,
          this.props.componentData,
          staticData
        );
        this.setState({
          secondseries: TestItem.TestData,
        });
      }
    }
  };

  /*Dynamic Test Execution  Levels - Test Case Function*/
  DynamicExecutionStatus = (type, staticData) => {
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (projectobj.length !== 0) {
      let TestItemLevel4 = DynamicTestExecutionStatusForthLevel(
        this.props.testResults,
        projectobj[0],
        config.functionaltesting,
        config.storytesting,
        type,
        this.props.componentData,
        staticData
      );
      let TestItemLevel5 = DynamicTestExecutionStatus(
        this.props.testResults,
        projectobj[0],
        config.performancetesting,
        //config.performancetesting,
        type,
        this.props.componentData,
        staticData
      );
      let TestItemLevel6 = DynamicTestExecutionStatusForthLevel(
        this.props.testResults,
        projectobj[0],
        config.functionaltesting,
        config.ITE2ETesting,
        type,
        this.props.componentData,
        staticData
      );
      let TestItemLevel7 = DynamicTestExecutionStatus(
        this.props.testResults,
        projectobj[0],
        config.securitytesting,
        // config.securitytesting,
        type,
        this.props.componentData,
        staticData
      );
      let TestItemLevel8 = DynamicTestExecutionStatusForthLevel(
        this.props.testResults,
        projectobj[0],
        config.functionaltesting,
        config.useracceptancetesting,
        type,
        this.props.componentData,
        staticData
      );
      let TestItemLevel9 = DynamicTestExecutionStatus(
        this.props.testResults,
        projectobj[0],
        config.automationtesting,
        //config.ITE2ETesting,
        type,
        this.props.componentData,
        staticData
      );
      this.setState({
        forthseries: TestItemLevel4,
        fifthseries: TestItemLevel5,
        sixthseries: TestItemLevel6,
        seventhseries: TestItemLevel7,
        eigthseries: TestItemLevel8,
        ninthseries: TestItemLevel9,
      });
    }
  };

  /* Test Execution - Requirement Coverage chart options */
  RequirementCoverage = () => {
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    reqcount = 1;
    if (projectobj.length !== 0) {
      let RequirementCoverageData = RequirementCoverageMainFunction(
        this.props.requirementData,
        projectobj[0],
        reqcount,
        ReqDataset,
        ReqDataset,
        this.props.componentData,
      );
      let RequirementFirstLevel = RequirementCoverageMainFunction(
        this.props.requirementData,
        projectobj[0],
        reqcount,
        initialDataset,
        ReqDataset,
        this.props.componentData,
      );
      if (
        Object.keys(RequirementCoverageData.RequirementCoverage).length !== 0 &&
        Object.keys(RequirementFirstLevel.RequirementCoverage).length !== 0
      ) {
        this.setState({
          RequirementCoverageBack: true,
          RequirementHideInPercentNumbers: true,
        });
      } else {
        this.setState({
          RequirementCoverageBack: false,
          RequirementHideInPercentNumbers: false,
        });
      }
      this.setState({
        requirementSeries: RequirementCoverageData.RequirementCoverage,
        isTestCase: false,
        isUserStories: false,
        RequirementButton: "active",
        TestCaseButton: "",
        UserStoriesButton: "",
        TestExectionButton: false,
        UserStoriesBack: false,
        RequirementInpercentButton: "",
        // RequirementHideInPercentNumbers: false,
        RequirementInNumbermbersButton: "active",
        RequirementInpercent: false,
        InNumbermbers: false,
      });
      if (RequirementCoverageData.RequirementCoverage === 0) {
        this.setState({
          requirementSeries: [],
        });
      }
      reqcount++;
    }
  };

  /* Test Execution - User Stories chart options */
  UserStories = (type, epicType) => {
    storycount = 1;
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (projectobj.length !== 0) {
      let UserStoriesData: any = UserStoriesFunction(
        this.props.userStoriesData,
        projectobj[0],
        storycount,
        initialDataset,
        type,
        this.props.componentData,
        epicType
      );
      if (Object.keys(UserStoriesData.UserStories.Finalseries).length !== 0) {
        this.setState({
          UserStoryHideInPercentNumbers: true,
        });
      } else {
        if (type !== config.allstories) {
          this.setState({
            UserStoryHideInPercentNumbers: true,
          });
        } else {
          this.setState({
            UserStoryHideInPercentNumbers: false,
          });
        }
      }
      this.setState({
        UserstoriesSeries: UserStoriesData.UserStories.Finalseries,
        isTestCase: false,
        isUserStories: true,
        RequirementButton: "",
        TestCaseButton: "",
        UserStoriesButton: "active",
        TestExectionButton: false,
        UserStoriesBack: false,
        UserstoriesInpercentButton: "",
        UserstoriesInNumbermbersButton: "active",
        UserStoriesInpercent: false,
        InNumbermbers: false,
      });
    }
  };

  /* Handling Startup Date*/
  handleStartUp = () => {
    if (
      this.props.projectViewData.length !== 0 &&
      this.props.projectViewData !== undefined
    ) {
      if (
        this.props.projectViewData[0].CCB_STARTUP_Date === null ||
        this.props.projectViewData[0].CCB_STARTUP_Date === undefined
      ) {
        return false;
      } else {
        return true;
      }
    } else {
      return false;
    }
  };

  handleBack() {
    const pp = this.state.previousPage;
    let Temp;
    if (pp === "") {
      return;
    }
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (projectobj.length !== 0) {
      if (count > 3 && count <= 13) {
        Temp = captureDataset;
        bcount = 2;
        if (count === 5) {
          bcount = 3;
          Temp = captureDatasetlevel3;
        } else if (count === 6) {
          bcount = 4;
          Temp = captureDatasetlevel4;
        } else if (count === 7) {
          bcount = 5;
          Temp = captureDatasetlevel5;
        } else if (count === 8) {
          bcount = 6;
          Temp = captureDatasetlevel6;
        } else if (count === 9) {
          bcount = 7;
          Temp = captureDatasetlevel7;
        } else if (count === 10) {
          bcount = 8;
          Temp = captureDatasetlevel8;
        } else if (count === 11) {
          bcount = 9;
          Temp = captureDatasetlevel9;
        } else if (count === 12) {
          bcount = 10;
          Temp = captureDatasetlevel10;
        } else if (count === 13) {
          bcount = 11;
          Temp = captureDatasetlevel11;
        }
        count = count - 1;
        let FinalData = this.props.getChartData(
          Temp,
          this.props.testResults,
          projectobj[0],
          bcount,
          this.props.allDefectData,
          captureDataset,
          this.state.testType,
          this.props.componentData,
          this.state.staticData
        );
        //TestDefectCollections = FinalData.DefectData;
        this.setState({
          series: FinalData.TestData,
        });
      } else if (count === 3) {
        Temp = "tests";
        bcount = 1;
        count = count - 1;
        let FinalData = this.props.getChartData(
          Temp,
          this.props.testResults,
          projectobj[0],
          bcount,
          this.props.allDefectData,
          captureDataset,
          this.state.testType,
          this.props.componentData,
          this.state.staticData
        );
        //TestDefectCollections = FinalData.DefectData;
        this.setState({
          series: FinalData.TestData,
          previousPage: this.state.currentPage,
        });
      } else {
        Temp = "projects";
        count = 1;
        bcount = 1;
        let FinalData = this.props.getChartData(
          Temp,
          this.props.testResults,
          projectobj[0],
          bcount,
          this.props.allDefectData,
          captureDataset,
          this.state.testType,
          this.props.componentData,
          this.state.staticData
        );
        //TestDefectCollections = FinalData.DefectData;
        this.setState({
          series: FinalData.TestData,
          previousPage: this.state.currentPage,
          TestExectionButton: false,
          HideInPercentNumbers: true,
        });
        clearTestLevels();
        captureDataset = "";
        captureDatasetlevel3 = "";
        captureDatasetlevel4 = "";
        captureDatasetlevel5 = "";
        captureDatasetlevel6 = "";
        captureDatasetlevel7 = "";
        captureDatasetlevel8 = "";
        captureDatasetlevel9 = "";
        captureDatasetlevel10 = "";
        captureDatasetlevel11 = "";
      }
    }
  }


  /* Test Exection Chart - Upadate option for Testcase*/
  updateChart(dataset) {
    let projectobj = this.props.projectDatas.filter((project) => {
       return (
         releaseURl === project.ProjectReference &&
         projectRelease === project.ReleaseName
       );
     });
     if (count === 1) {
       folderset.MainSubFolderSetter(dataset);
     }
     if (count === 2) {
       captureDataset = dataset;
     }
     if (projectobj.length !== 0) {
       if (count >= 4) {
         if (count === 4) {
           captureDatasetlevel4 = dataset;
           folderset.SubFolder2Setter(captureDatasetlevel4);
         } else if (count === 5) {
           captureDatasetlevel5 = dataset;
           folderset.SubFolderSetterForDefects(captureDatasetlevel5);
         } else if (count === 6) {
           folderset.SubFolder3Setter(dataset);
           captureDatasetlevel6 = dataset;
         } else if (count === 7) {
           folderset.SubFolder5Setter(dataset);
           captureDatasetlevel7 = dataset;
         } else if (count === 8) {
           captureDatasetlevel8 = dataset;
         } else if (count === 9) {
           folderset.SubFolder6Setter(dataset);
           captureDatasetlevel9 = dataset;
         } else if (count === 10) {
           captureDatasetlevel10 = dataset;
         } else if (count === 11) {
           folderset.SubFolder8Setter(dataset);
           captureDatasetlevel11 = dataset;
         }
         let FinalData = this.props.getChartData(
           dataset,
           this.props.testResults,
           projectobj[0],
           count,
           this.props.allDefectData,
           captureDataset,
           this.state.testType,
           this.props.componentData,
           this.state.staticData
         );
         //TestDefectCollections = FinalData.DefectData;
         this.setState({
           series: FinalData.TestData,
           TestExectionButton: true,
         });
         if (count < 13 && FinalData.TestData !== 0) {
           count++;
         }
       }
       if (count === 2 || count === 3) {
         if (count === 3) {
           captureDatasetlevel3 = dataset;
           folderset.SubFolderSetter(captureDatasetlevel3);
         }
         let FinalData = this.props.getChartData(
           dataset,
           this.props.testResults,
           projectobj[0],
           count,
           this.props.allDefectData,
           captureDataset,
           this.state.testType,
           this.props.componentData,
           this.state.staticData
         );
        // TestDefectCollections = FinalData.DefectData;
         this.setState({
           series: FinalData.TestData,
           TestExectionButton: true,
         });
         if (count < 13 && FinalData.TestData !== 0) {
           count++;
         }
       } else {
         let FinalData = this.props.getChartData(
           dataset,
           this.props.testResults,
           projectobj[0],
           count,
           this.props.allDefectData,
           captureDataset,
           this.state.testType,
           this.props.componentData,
           this.state.staticData
         );
        // TestDefectCollections = FinalData.DefectData;
         this.setState({
           series: FinalData.TestData,
           previousPage: this.state.currentPage,
           TestExectionButton: true,
           HideInPercentNumbers: false,
         });
         if (count < 13 && FinalData.TestData !== 0) {
           count++;
         }
       }

     }
   }

  /* Test Exection Chart - Update option for UserStories*/
  storiesupdate(dataset) {
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (projectobj.length !== 0) {
      if (storycount === 2) {
        this.setState({
          UserStoriesBack: true,
        });
      } else {
        let UserStoriesData: any = UserStoriesFunction(
          this.props.userStoriesData,
          projectobj[0],
          storycount,
          dataset,
          this.state.storyType,
          this.props.componentData,
          this.state.userStorybtn
        );
        this.setState({
          UserStoriesBack: true,
          UserstoriesSeries: UserStoriesData.UserStories.Finalseries,
          UserStoryHideInPercentNumbers: false,
        });
      }
      if (storycount < 2) {
        storycount++;
      }
    }
  }

  /* Test Exection Chart - Back option for User Stories*/
  storiesBack() {
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (projectobj.length !== 0) {
      storycount = storycount - 1;
      let UserStoriesData: any = UserStoriesFunction(
        this.props.userStoriesData,
        projectobj[0],
        storycount,
        initialDataset,
        this.state.storyType,
        this.props.componentData,
        this.state.userStorybtn
      );
      this.setState({
        UserstoriesSeries: UserStoriesData.UserStories.Finalseries,
        UserStoriesBack: false,
        UserStoryHideInPercentNumbers: true,
      });
    }
  }

  /* Test Exection Chart - Update option for Requirement Coverage*/
  requirementupdate(dataset) {
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (projectobj.length !== 0) {
      if (reqcount === 3) {
        this.setState({
          RequirementCoverageBack: true,
        });
      } else if (reqcount === 2) {
        let RequirementData = RequirementCoverageMainFunction(
          this.props.requirementData,
          projectobj[0],
          reqcount,
          dataset,
          ReqDataset,
          this.props.componentData,
        );
        this.setState({
          RequirementCoverageBack: true,
          requirementSeries: RequirementData.RequirementCoverage,
        });
        if (reqcount < 3 && RequirementData.RequirementCoverage !== 0) {
          reqcount++;
        }
      } else {
        let RequirementData = RequirementCoverageMainFunction(
          this.props.requirementData,
          projectobj[0],
          reqcount,
          dataset,
          ReqDataset,
          this.props.componentData,
        );
        this.setState({
          RequirementCoverageBack: true,
          requirementSeries: RequirementData.RequirementCoverage,
          RequirementHideInPercentNumbers: false,
        });
        if (reqcount < 3 && RequirementData.RequirementCoverage !== 0) {
          reqcount++;
        }
      }
    }
  }

  /* Test Exection Chart - Back option for Requirement Coverage*/
  requirementBack() {
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (projectobj.length !== 0) {
      if (reqcount === 3) {
        reqcount = 1;
        let RequirementData = RequirementCoverageMainFunction(
          this.props.requirementData,
          projectobj[0],
          reqcount,
          "tests",
          ReqDataset,
          this.props.componentData,
        );
        this.setState({
          requirementSeries: RequirementData.RequirementCoverage,
        });
      } else {
        reqcount = 1;
        let RequirementData = RequirementCoverageMainFunction(
          this.props.requirementData,
          projectobj[0],
          reqcount,
          initialDataset,
          ReqDataset,
          this.props.componentData,
        );
        this.setState({
          requirementSeries: RequirementData.RequirementCoverage,
          RequirementCoverageBack: false,
          RequirementHideInPercentNumbers: true,
        });
      }
    }
  }

  /* Test Exection Chart - option for Testcase*/
  chartOptions() {
    return {
      series: this.state.series,
      chart: {
        type: "bar",
        style: {
          fontFamily: "serif",
        },
      },
      exporting: {
        enabled: false, // hide button
      },
      title: null,
      xAxis: { type: "category" },
      legend: { enabled: true },
      tooltip: {
        formatter: function () {
          return this.series.name + " " + this.y;
        },
      },
      plotOptions: {
        bar: { stacking: "normal" },
        series: {
          cursor: "pointer",
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            formatter: function () {
              return this.y !== 0 ? this.y : "";
            },
            style: {
              textShadow: false,
              fontSize: "10px",
              color: "#000000",
              textOutline: false,
            },
          },
          events: {
            click: (function (self) {
              return function (event) {
                self.updateChart(event.point.options.name);
              };
            })(this),
          },
        },
      },
      yAxis: {
        reversedStacks: false,
        min: 0,
        max: null,
        gridLineWidth: 0,
        title: {
          text: null,
        },
      },
      credits: {
        enabled: false,
      },
    };
  }

  /*Test Execution In percent */
  chartOptionsInPercent() {
    return {
      series: this.state.series,
      chart: {
        type: "bar",
        style: {
          fontFamily: "serif",
        },
      },
      exporting: {
        enabled: false, // hide button
      },
      title: null,
      xAxis: { type: "category" },
      legend: { enabled: true },
      tooltip: {
        formatter: function () {
          return (
            this.series.name + " " + Math.round(this.point.percentage) + "%"
          );
        },
      },
      plotOptions: {
        bar: { stacking: "percent" },
        series: {
          cursor: "pointer",
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            formatter: function () {
              return this.y !== 0
                ? Math.round(this.point.percentage) + "%"
                : "";
            },
            style: {
              textShadow: false,
              fontSize: "10px",
              color: "#000000",
              textOutline: false,
            },
          },
          events: {
            click: (function (self) {
              return function (event) {
                self.updateChart(event.point.options.name);
              };
            })(this),
          },
        },
      },
      yAxis: {
        reversedStacks: false,
        min: 0,
        max: 100,
        labels: {
          formatter: function () {
            return this.value; //+ "%";
          },
        },
        gridLineWidth: 0,
        title: {
          text: null,
        },
      },
      credits: {
        enabled: false,
      },
    };
  }

  /* Dynamic Test Exection Chart - option for Testcase*/
  DynamicchartOptions(data) {
    return {
      series: data,
      chart: {
        type: "bar",
        style: {
          fontFamily: "serif",
        },
      },
      exporting: {
        enabled: false, // hide button
      },
      title: null,
      xAxis: { type: "category" },
      legend: { enabled: true },
      tooltip: {
        formatter: function () {
          return this.series.name + " " + this.y;
        },
      },
      plotOptions: {
        bar: { stacking: "normal" },
        series: {
          cursor: "pointer",
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            formatter: function () {
              return this.y !== 0 ? this.y : "";
            },
            style: {
              textShadow: false,
              fontSize: "10px",
              color: "#000000",
              textOutline: false,
            },
          },
          events: {
            click: (function (self) {
              return function (event) {
                // self.updateChart(event.point.options.name);
              };
            })(this),
          },
        },
      },
      yAxis: {
        reversedStacks: false,
        min: 0,
        max: null,
        gridLineWidth: 0,
        title: {
          text: null,
        },
      },
      credits: {
        enabled: false,
      },
    };
  }

  /*Test Execution In percent */
  DynamicchartOptionsInPercent(data) {
    return {
      series: data,
      chart: {
        type: "bar",
        style: {
          fontFamily: "serif",
        },
      },
      exporting: {
        enabled: false, // hide button
      },
      title: null,
      xAxis: { type: "category" },
      legend: { enabled: true },
      tooltip: {
        formatter: function () {
          return (
            this.series.name + " " + Math.round(this.point.percentage) + "%"
          );
        },
      },
      plotOptions: {
        bar: { stacking: "percent" },
        series: {
          cursor: "pointer",
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            formatter: function () {
              return this.y !== 0
                ? Math.round(this.point.percentage) + "%"
                : "";
            },
            style: {
              textShadow: false,
              fontSize: "10px",
              color: "#000000",
              textOutline: false,
            },
          },
          events: {
            click: (function (self) {
              return function (event) {
                // self.updateChart(event.point.options.name);
              };
            })(this),
          },
        },
      },
      yAxis: {
        reversedStacks: false,
        min: 0,
        max: 100,
        labels: {
          formatter: function () {
            return this.value; //+ "%";
          },
        },
        gridLineWidth: 0,
        title: {
          text: null,
        },
      },
      credits: {
        enabled: false,
      },
    };
  }

  /* Test Exection Chart -  option for Requirement*/
  RequirementchartOptions() {
    return {
      series: this.state.requirementSeries,
      chart: {
        type: "bar",
        style: {
          fontFamily: "serif",
        },
      },
      exporting: {
        enabled: false, // hide button
      },
      title: null,
      xAxis: { type: "category" },
      legend: { enabled: true },
      tooltip: {
        formatter: function () {
          return this.series.name + " " + this.y;
        },
      },
      plotOptions: {
        bar: { stacking: "normal" },
        series: {
          cursor: "pointer",
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            formatter: function () {
              return this.y !== 0 ? this.y : "";
            },
            style: {
              textShadow: false,
              fontSize: "10px",
              color: "#000000",
              textOutline: false,
            },
          },
          events: {
            click: (function (self) {
              return function (event) {
                self.requirementupdate(event.point.options.name);
              };
            })(this),
          },
        },
      },
      yAxis: {
        reversedStacks: false,
        min: 0,
        max: null,
        gridLineWidth: 0,
        title: {
          text: null,
        },
      },
      credits: {
        enabled: false,
      },
    };
  }

  /* Test Exection Chart -  option for Requirement*/
  RequirementchartOptionsInPercent() {
    return {
      series: this.state.requirementSeries,
      chart: {
        type: "bar",
        style: {
          fontFamily: "serif",
        },
      },
      exporting: {
        enabled: false, // hide button
      },
      title: null,
      xAxis: { type: "category" },
      legend: { enabled: true },
      tooltip: {
        formatter: function () {
          return (
            this.series.name + " " + Math.round(this.point.percentage) + "%"
          );
        },
      },
      plotOptions: {
        bar: { stacking: "percent" },
        series: {
          cursor: "pointer",
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            formatter: function () {
              return this.y !== 0
                ? Math.round(this.point.percentage) + "%"
                : "";
            },
            style: {
              textShadow: false,
              fontSize: "10px",
              color: "#000000",
              textOutline: false,
            },
          },
          events: {
            click: (function (self) {
              return function (event) {
                self.requirementupdate(event.point.options.name);
              };
            })(this),
          },
        },
      },
      yAxis: {
        reversedStacks: false,
        min: 0,
        max: 100,
        labels: {
          formatter: function () {
            return this.value; //+ "%";
          },
        },
        gridLineWidth: 0,
        title: {
          text: null,
        },
      },
      credits: {
        enabled: false,
      },
    };
  }

  /* Test Exection Chart -  option for UserStories*/
  StorieschartOptions() {
    return {
      series: this.state.UserstoriesSeries,
      chart: {
        type: "bar",
        style: {
          fontFamily: "serif",
        },
      },
      exporting: {
        enabled: false, // hide button
      },
      title: null,
      xAxis: { type: "category" },
      legend: { enabled: true },
      tooltip: {
        formatter: function () {
          return this.series.name + " " + this.y;
        },
      },
      plotOptions: {
        bar: { stacking: "normal" },
        series: {
          cursor: "pointer",
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            formatter: function () {
              return this.y !== 0 ? this.y : "";
            },
            style: {
              textShadow: false,
              fontSize: "10px",
              color: "#000000",
              textOutline: false,
            },
          },
          events: {
            click: (function (self) {
              return function (event) {
                self.storiesupdate(event.point.options.name);
              };
            })(this),
          },
        },
      },
      yAxis: {
        reversedStacks: false,
        min: 0,
        max: null,
        gridLineWidth: 0,
        title: {
          text: null,
        },
      },
      credits: {
        enabled: false,
      },
    };
  }

  /* Test Exection Chart -  option for UserStories*/
  StorieschartOptionsInPercent() {
    return {
      series: this.state.UserstoriesSeries,
      chart: {
        type: "bar",
        style: {
          fontFamily: "serif",
        },
      },
      exporting: {
        enabled: false, // hide button
      },
      title: null,
      xAxis: { type: "category" },
      legend: { enabled: true },
      tooltip: {
        formatter: function () {
          return (
            this.series.name + " " + Math.round(this.point.percentage) + "%"
          );
        },
      },
      plotOptions: {
        bar: { stacking: "percent" },
        series: {
          cursor: "pointer",
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            formatter: function () {
              return this.y !== 0
                ? Math.round(this.point.percentage) + "%"
                : "";
            },
            style: {
              textShadow: false,
              fontSize: "10px",
              color: "#000000",
              textOutline: false,
            },
          },
          events: {
            click: (function (self) {
              return function (event) {
                self.storiesupdate(event.point.options.name);
              };
            })(this),
          },
        },
      },
      yAxis: {
        reversedStacks: false,
        min: 0,
        max: 100,
        labels: {
          formatter: function () {
            return this.value; //+ "%";
          },
        },
        gridLineWidth: 0,
        title: {
          text: null,
        },
      },
      credits: {
        enabled: false,
      },
    };
  }

  /* Validating Component Data */
  ValidatingComponentData() {
    if (TempComponentData.ComponentName === changeComponentData.ComponentName) {
      return false;
    } else {
      TempComponentData = changeComponentData;
      return true;
    }
  }
  /* Validating Release Data */
  ValidatingReleaseData(projectviewdata) {
    if (TempReleaseData[0].ReleaseName === projectviewdata[0].ReleaseName) {
      return false;
    } else {
      TempReleaseData = projectviewdata;
      return true;
    }
  }

  /*Test Execution - In Numbers*/
  InNumbers = () => {
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (projectobj.length !== 0) {
      let TestItem = this.props.getChartData(
        initialDataset,
        this.props.testResults,
        projectobj[0],
        count,
        this.props.allDefectData,
        captureDataset,
        this.state.testType,
        this.props.componentData,
        config.all
      );
      this.setState({
        series: TestItem.TestData,
        Inpercent: false,
        InNumbermbers: true,
        InpercentButton: "",
        InNumbermbersButton: "active",
      });
    }
  };

  /*User Stories - In Numbers*/
  UserStoriesInNumbers = () => {
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (projectobj.length !== 0) {
      let UserStoriesData: any = UserStoriesFunction(
        this.props.userStoriesData,
        projectobj[0],
        storycount,
        initialDataset,
        this.state.storyType,
        this.props.componentData,
        this.state.userStorybtn
      );
      this.setState({
        UserstoriesSeries: UserStoriesData.UserStories.Finalseries,
        UserStoriesInpercent: false,
        InNumbermbers: true,
        UserstoriesInpercentButton: "",
        UserstoriesInNumbermbersButton: "active",
      });
    }
  };

  /*Requirement - In Numbers*/
  RequirementInNumbers = () => {
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (projectobj.length !== 0) {
      let RequirementCoverageData = RequirementCoverageMainFunction(
        this.props.requirementData,
        projectobj[0],
        reqcount,
        initialDataset,
        ReqDataset,
        this.props.componentData,
      );
      this.setState({
        requirementSeries: RequirementCoverageData.RequirementCoverage,
        RequirementInpercent: false,
        InNumbermbers: true,
        RequirementInpercentButton: "",
        RequirementInNumbermbersButton: "active",
      });
    }
  };

  /*Test Execution - In Percent*/
  InPercent = () => {
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (projectobj.length !== 0) {
      let TestItem = this.props.getChartData(
        initialDataset,
        this.props.testResults,
        projectobj[0],
        count,
        this.props.allDefectData,
        captureDataset,
        this.state.testType,
        this.props.componentData,
        config.all
      );
      this.setState({
        series: TestItem.TestData,
        Inpercent: true,
        InNumbermbers: false,
        InpercentButton: "active",
        InNumbermbersButton: "",
      });
    }
  };

  /*UserStories - In Percent*/
  UserStoriesInPercent = () => {
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (projectobj.length !== 0) {
      let UserStoriesData: any = UserStoriesFunction(
        this.props.userStoriesData,
        projectobj[0],
        storycount,
        initialDataset,
        this.state.storyType,
        this.props.componentData,
        this.state.userStorybtn
      );
      this.setState({
        UserstoriesSeries: UserStoriesData.UserStories.Finalseries,
        UserStoriesInpercent: true,
        InNumbermbers: false,
        UserstoriesInpercentButton: "active",
        UserstoriesInNumbermbersButton: "",
      });
    }
  };

  /*Requirement - In Percent*/
  RequirementInPercent = () => {
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (projectobj.length !== 0) {
      let RequirementCoverageData = RequirementCoverageMainFunction(
        this.props.requirementData,
        projectobj[0],
        reqcount,
        initialDataset,
        ReqDataset,
        this.props.componentData,
      );
      this.setState({
        requirementSeries: RequirementCoverageData.RequirementCoverage,
        RequirementInpercent: true,
        InNumbermbers: false,
        RequirementInpercentButton: "active",
        RequirementInNumbermbersButton: "",
      });
    }
  };

  /* Date Substring */
  DataSubstring = (data: any) => {
    if (data !== null) {
      return data.substr(0, 10);
    } else {
      return data;
    }
  };

  /* Remarks Validation */
  ValidateRemarks = (data: any) => {
    if (data !== null && data !== undefined) {
      return data;
    } else {
      return config.nodata;
    }
  };

  /* Loading Message for pdf*/
  openMessage = () => {
    message.loading({ content: config.loadingpdf, key });
  };

  /* Loading Message for outlook*/
  openMailMessage = () => {
    message.loading({ content: config.launchoutlook, key, duration: 2 });
  };

  /* Merging Message */
  MergingMessage = () => {
    this.setState({ confirmPdf: false });
    let msgcode = this.props.mergemsg.messageCode;
    let pdfData = this.props.mergemsg.data;
    if (msgcode === "MSG200") {
      this.downloadPDF(pdfData);
      message.success(config.successpdf);
      this.props.ClearingPdfMessage();
    } else {
      message.error(config.errpfd);
      this.props.ClearingPdfMessage();
    }
  };

  /*Converting Base64 to pdf */
  downloadPDF = (pdfData) => {
    var element = document.createElement("a");
    element.setAttribute("href", pdfData);
    element.setAttribute("download", DateValue);
    element.style.display = "none";
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  };

  GeneratingExternalURL = () => {
    let ExternalURLArray = [];
    if (this.props.ExternaltestReport.length !== 0) {
      this.props.ExternaltestReport.forEach((e) => {
        if (e.FileServerRelativeURL !== null && e.FileServerRelativeURL !== undefined) {
          let obj = {
            url: e.FileServerRelativeURL,
            type: e.TypeOfReport
          }
          ExternalURLArray.push(obj);
        }
      });

    }
    return ExternalURLArray;

  };

  /* Genereate Pdf*/
  getPDF = (data: any) => {
    this.setState({ confirmPdf: true });
    if (config.generatepdf === data) {
      this.openMessage();
    } else {
      this.openMailMessage();
    }
    this.exportPDF();
    this.GenerateReportPDF();
    let top_left_margin = 15;
    const element = document.getElementById("canvas_div_pdf");
    return html2canvas(element, {
      backgroundColor: "#FFFFFF",
      scrollY: -window.scrollY,
      useCORS: true,
    }).then((canvas) => {
      canvas.getContext("2d");
      let imgData = canvas.toDataURL("image/jpeg", 1.0);
      let pdf = new JsPDF("p", "pt", "a4");
      let height = pdf.internal.pageSize.getHeight();
      let width = pdf.internal.pageSize.getWidth();
      pdf.addImage(
        imgData,
        "JPEG",
        top_left_margin,
        top_left_margin,
        width - 60,
        height - 300
      );
      if (this.props.detailView) {
        const element = document.getElementById("canvas_div_pdf1");
        return html2canvas(element, {
          backgroundColor: "#FFFFFF",
          scrollY: -window.scrollY,
          useCORS: true,
          scale: window.devicePixelRatio,
        }).then((canvas) => {
          canvas.getContext("2d");
          let imgData = canvas.toDataURL("image/jpeg", 1.0);
          pdf.addPage();
          pdf.addImage(
            imgData,
            "JPEG",
            top_left_margin,
            top_left_margin,
            width - 60,
            height - 300
          );
          base64data = pdf.output("datauristring").split(";base64,")[1];
          let baseObject = {
            object: reportbasedata,
            object1: base64data,
            object2: basedata,
            name: SharepointPDF,
            metaData: metaData,
            externalURL: this.GeneratingExternalURL()
          };
          if (config.generatepdf === data) {
            this.props.MergingPdf(baseObject);
          } else {
            this.props.StoringInNetworkPath(baseObject);
          }
        });
      } else {
        base64data = pdf.output("datauristring").split(";base64,")[1];
        let baseObject = {
          object: reportbasedata,
          object1: base64data,
          object2: basedata,
          name: SharepointPDF,
          metaData: metaData,
          externalURL: this.GeneratingExternalURL()
        };
        if (config.generatepdf === data) {
          this.props.MergingPdf(baseObject);
        } else {
          this.props.StoringInNetworkPath(baseObject);
        }
      }
    });
  };

  constructingMail = () => {
    let mail: any = [];
    if (this.props.projectViewData[0].Test_Coordinator_email !== null) {
      mail.push(this.props.projectViewData[0].Test_Coordinator_email);
    }
    if (this.props.projectViewData[0].Project_Manager_email !== null) {
      mail.push(this.props.projectViewData[0].Project_Manager_email);
    }
    if (this.props.projectViewData[0].Scrum_Master_email !== null) {
      mail.push(this.props.projectViewData[0].Scrum_Master_email);
    }
    if (this.props.projectViewData[0].Delivery_Master_Lead_email !== null) {

      mail.push(this.props.projectViewData[0].Delivery_Master_Lead_email);
    }
    mail.push(config.defaultmailID);
    let FinalmailObj: string = "";
    mail.forEach((e) => {
      let temp = e + ";"
      FinalmailObj = FinalmailObj + temp;
    });
    return FinalmailObj;

  }

  /* Mail the pdf */
  onClickMailOpener = () => {
    let msgcode = this.props.networkmsg.messageCode;
    this.setState({ confirmPdf: false });
    if (msgcode === "MSG200") {
      this.props.ClearingNetworkPdfMessage();
      if (this.props.projectViewData.length !== 0) {
        let email = this.constructingMail();
        let encodedlink = decodeURI(encodeURI(config.networkpath1 + SharepointPDF + ".pdf" + config.networkpath2));
        try {
          window.location.href = `mailto:${email}?subject=Detailed Test Report of ${SharepointPDF}&body=Hi,%0D%0DClick the below link to download PDF Report.%0D%0D<${encodedlink}>%0D%0DThanks and Regards`;
        } catch {
          alert(config.errormailer);
        }
      }
    } else {
      message.error(config.errnetworkpath);
      this.props.ClearingNetworkPdfMessage();
    }
  };

  /*Exporting PDF Options*/
  exportPDF = () => {
    this.exportElement(this._element, {
      paperSize: "A4",
      repeatHeaders: true,
      scale: 0.5,
      margin: "0.5cm",
    });
  };

  exportElement(element, options) {
    drawDOM(element, options)
      .then((group) => {
        return exportPDF(group);
      })
      .then((dataUri) => {
        basedata = dataUri.split(";base64,")[1];
      });
  }

  GenerateReportPDF = () => {
    this.GenerateElement(this._report, {
      paperSize: "A4",
      scale: 0.5,
      margin: "0.5cm",
    });
  };

  GenerateElement(element, options) {
    drawDOM(element, options)
      .then((group) => {
        return exportPDF(group);
      })
      .then((dataUri) => {
        reportbasedata = dataUri.split(";base64,")[1];
      });
  }

  /* Generating Dynamic rows - Kendo table */
  generateRows() {
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (projectobj.length !== 0) {
      let filteredDefects = this.props.allDefectData.filter((defect) => {
        return (
          projectobj[0].ReleaseName === defect.ReleaseName &&
          projectobj[0].ProjectReference === defect.projectReference
        );
      });
      if (this.props.componentData.epicLink !== config.allEpic) {
        filteredDefects = filteredDefects.filter((defect) => {
          return (
            defect.epicLink === this.props.componentData.epicLink
          );
        });

      }
      if (filteredDefects.length !== 0) {
        return filteredDefects.map((element) => {
          return (
            <tr>
              <td className="defects-row">{element.defectKey}</td>
              <td>{this.ValidateRemarks(element.summary)}</td>
              <td>{element.priority}</td>
              <td>{element.status}</td>
              <td>{element.assigneeName}</td>
            </tr>
          );
        });
      } else {
        return (
          <tr>
            <td colSpan={5}>{config.nodataavailable}</td>
          </tr>
        );
      }
    }
  }

  /* Generating Dynamic rows for User Stories - Kendo table */
  generateRowsUserstories() {
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (projectobj.length !== 0) {
      let filteredStories = this.props.userStoriesData.filter((story) => {
        return (
          projectobj[0].ReleaseName === story.fixVersions &&
          projectobj[0].ProjectReference === story.ProjectReference
        );
      });
      if (this.props.componentData.epicLink !== config.allEpic) {
        filteredStories = filteredStories.filter((defect) => {
          return (
            defect.epicLink === this.props.componentData.epicLink
          );
        });
      }
      if (filteredStories.length !== 0) {
        return filteredStories.map((element) => {
          return (
            <tr key={element.storyKey}>
              <td className="defects-row">{element.storyKey}</td>
              <td>{this.ValidateRemarks(element.summary)}</td>
              <td>{element.priority}</td>
              <td>{element.storyStatus}</td>
              <td>{element.assigneeName}</td>
            </tr>
          );
        });
      } else {
        return (
          <tr>
            <td colSpan={5}>{config.nodataavailable}</td>
          </tr>
        );
      }
    }
  }

  /* Defects MVP and not MVP*/
  DefectsTypes = (type) => {
    this.props.DefectsTogglePDF(type);
    if (this.state.AssigneeButton === "active") {
      this.DefectByAssignee(type, this.props.defectToggle);
    } else if (this.state.StatusButton === "active") {
      this.DefectByStatus(type, this.props.defectToggle);
    } else {
      this.DefectByComponent(type, this.props.defectToggle);
    }
  };

  /* Defects Closed and Open */
  DefectsCategories = (type) => {
    if (type === config.allDefects) {
      openDefects = "";
      storyDefects = "";
      allDefects = "active";
    } else if (type === config.storyDefects) {
      openDefects = "";
      storyDefects = "active";
      allDefects = "";
    } else {
      openDefects = "active";
      allDefects = "";
      storyDefects = "";
    }
    this.props.PdfDefectsCategories(type);
    if (this.state.AssigneeButton === "active") {
      this.DefectByAssignee(this.props.pdfToggle, type);
    } else if (this.state.StatusButton === "active") {
      this.DefectByStatus(this.props.pdfToggle, type);
    } else {
      this.DefectByComponent(this.props.pdfToggle, type);
    }
  };

  /*Defects Naming Function */

  DefectsName = () => {
    if (
      this.props.pdfToggle !== "" &&
      this.props.defectToggle !== "" &&
      this.props.defectToggle !== undefined &&
      this.props.pdfToggle !== undefined
    ) {
      if (this.props.pdfToggle === config.allDefects) {
        return this.props.defectToggle.toUpperCase();
      } else {
        let data =
          this.props.defectToggle.toUpperCase() +
          "-" +
          this.props.pdfToggle.toUpperCase();
        return data;
      }
    } else {
      return config.allDefects.toUpperCase();
    }
  };

  /*User Stories MVP and not MVP */
  userStoriesType = (type) => {
    this.setState({ storyType: type });
    this.UserStories(type, this.state.userStorybtn);
  };

  /*Test Case MVP and not MVP */
  TestType = (type) => {
    this.setState({ testType: type });
    this.TestExecutionStatus(type, this.state.staticData);
    this.DynamicTestExecutionStatus(config.secondseries, type, this.state.staticData);
    this.DynamicTestExecutionStatus(config.thirdseries, type, this.state.staticData);
    this.DynamicExecutionStatus(type, this.state.staticData);
  };

  AgentBrokerFiltert = (value) => {
    this.setState({ staticData: value });
    this.TestExecutionStatus(this.state.testType, value);
    this.DynamicTestExecutionStatus(config.secondseries, this.state.testType, value);
    this.DynamicTestExecutionStatus(config.thirdseries, this.state.testType, value);
    this.DynamicExecutionStatus(this.state.testType, value);
  }

  /*EpicType*/
  EpicType = (epicType) => {
    this.setState({ userStorybtn: epicType });
    this.UserStories(this.state.storyType, epicType);

  }

  /*Validating Key Remark Testing*/

  validatingKeyRemarkTesting = (Data: any) => {
    if (Data === undefined || Data === null || Data === "") {
      Data = "No Test Status provided";
      return Data;
    } else {
      Data = Data.replace(/<img[^>]*>/g, "");
      Data = Data.replace(/([*+^$|{}[\]])/mg, "");
      return Data;
    }
  };

  render() {
    changeComponentData = this.props.componentData;
    if (Object.keys(this.props.mergemsg).length !== 0) {
      this.MergingMessage();
    }
    if (Object.keys(this.props.networkmsg).length !== 0) {
      this.onClickMailOpener();
    }
    if (Object.keys(this.state.DefectData).length === 0) {
      count = 1;
      reqcount = 1;
      storycount = 1;
      this.InitialDefectByStatus();
      this.TestExecutionStatus(config.alltests, config.all);
      this.RequirementCoverage();
      this.UserStories(config.allstories, config.stories);
      this.DynamicTestExecutionStatus(config.secondseries, this.state.testType, this.state.staticData);
      this.DynamicTestExecutionStatus(config.thirdseries, this.state.testType, this.state.staticData);
      this.DynamicExecutionStatus(this.state.testType, this.state.staticData);
    }
    if (
      this.props.testResults.length !== 0 &&
      this.props.allDefectData.length !== 0 &&
      independantLoadTestExecution
    ) {
      independantLoadTestExecution = false;
      count = 1;
      reqcount = 1;
      storycount = 1;
      this.TestExecutionStatus(config.alltests, config.all);
      this.DynamicTestExecutionStatus(config.secondseries, this.state.testType, this.state.staticData);
      this.DynamicTestExecutionStatus(config.thirdseries, this.state.testType, this.state.staticData);
      this.DynamicExecutionStatus(this.state.testType, this.state.staticData);
    }
    if (this.props.defectByStatus.length !== 0 && independantLoad === true) {
      independantLoad = false;
      this.InitialDefectByStatus();
    }
    if (this.props.requirementData.length !== 0 && independantLoadRequirment) {
      independantLoadRequirment = false;
      this.RequirementCoverage();
    }
    if (this.props.userStoriesData.length !== 0 && independantLoadStory) {
      independantLoadStory = false;
      this.UserStories(config.allstories, config.stories);
    }
    if (this.props.projectDatas.length !== 0) {
      if (
        this.props.projectDatas[1].ReleaseName === projectRelease &&
        projectDatavalidation
      ) {
        count = 1;
        reqcount = 1;
        storycount = 1;
        this.InitialDefectByStatus();
        this.TestExecutionStatus(config.alltests, config.all);
        this.RequirementCoverage();
        this.UserStories(config.allstories, config.stories);
        this.DynamicTestExecutionStatus(
          config.secondseries,
          this.state.testType,
          this.state.staticData
        );
        this.DynamicTestExecutionStatus(
          config.thirdseries,
          this.state.testType,
          this.state.staticData
        );
        this.DynamicExecutionStatus(this.state.testType, this.state.staticData);
        projectDatavalidation = false;
      }
    }

    /*Table data construction*/
    let mycolumns = this.state.columns.map((col, index) => ({
      ...col,
      onHeaderCell: (column) => ({
        width: column.width,
      }),
    }));
    const dataItems = [];
    if (this.props.projectViewData) {
      this.props.projectViewData.forEach((element) => {
        dataItems.push({
          ProjectName: element.ProjectReference + "-" + element.ProjectName,
          key: element.ProjectReference,
          Status_Go: (
            <div>
              {element.test_status === config.onTrack &&
                element.test_status !== null &&
                element.test_status !== undefined ? (
                <SmileyComponent
                  img={require("../../assets/images/thumbnail_norisks.png")}
                  desc="No Risks"
                ></SmileyComponent>
              ) : null}
              {element.test_status === config.notOnTrack &&
                element.test_status !== null &&
                element.test_status !== undefined ? (
                <SmileyComponent
                  img={require("../../assets/images/thumbnail_withissues.png")}
                  desc="With issues"
                ></SmileyComponent>
              ) : null}
              {element.test_status === config.atRisk &&
                element.test_status !== null &&
                element.test_status !== undefined ? (
                <SmileyComponent
                  img={require("../../assets/images/thumbnail_nodecision.png")}
                  desc="No Decision"
                ></SmileyComponent>
              ) : null}
              {element.test_status === config.notestStatus &&
                element.test_status !== null &&
                element.test_status !== undefined ? (
                <SmileyComponent
                  img={require("../../assets/images/noteststatus.png")}
                  desc="No Test Status"
                ></SmileyComponent>
              ) : null}
              {element.test_status === config.managementDecision &&
                element.test_status !== null &&
                element.test_status !== undefined ? (
                <SmileyComponent
                  img={require("../../assets/images/thumbnail_blocked.png")}
                  desc="Blocked"
                ></SmileyComponent>
              ) : null}{" "}
            </div>
          ),
          Test_Status: this.validatingKeyRemarkTesting(
            element.KeyRemarksTesting
          ),
          releaseName: projectRelease,
        });
      });
    }

    return (
      <div className="background-div pdf-generation">
        <div className="pdf-action-btn">
          <div className="row">
            <div className="col-md-12">
              <div style={{ float: "right", marginLeft: "auto" }}>
                <Button
                  className="header-btn header-btn-cancel"
                  onClick={this.redirectProjectView}
                >
                  <CloseCircleOutlined className="pdfpage-icons" />Cancel
                </Button>
              </div>
              <div style={{ float: "right", marginLeft: "auto" }}>
                <Button
                  className="header-btn"
                  onClick={(event: any) => {
                    this.getPDF(config.generatepdf);
                  }}
                  disabled={this.state.confirmPdf}
                >
                  <DownloadOutlined className="pdfpage-icons" />Confirm PDF Generation
                </Button>
              </div>
              <div style={{ float: "right", marginLeft: "auto" }}>
                <Button
                  className="header-btn"
                  onClick={(event: any) => {
                    this.getPDF(config.mailpdf);
                  }}
                >
                  <MailOutlined className="pdfpage-icons" />Mail PDF
                </Button>
              </div>
            </div>
          </div>
        </div>
        <div
          ref={(div) => {
            this._report = div;
          }}
        >
          <div className="container-flex page-wrapper">
            <div>
              <div className="pdf-view">
                <h5 className="pdf-headline-text">{this.state.testreport}</h5>
                <div className="col-md-12 summary_table pdf-view-table">
                  <Table
                    bordered
                    columns={mycolumns}
                    dataSource={dataItems}
                    pagination={false}
                  // scroll={{ y: 100 }}
                  />
                </div>
              </div>
              <br></br>
              <div className="pdf-view">
                <h5 className="pdf-headline-text"> Summary Test Status</h5>
                <div className="content-info-section col-md-12 summary-status">
                  <div className="row bg-sec">
                    <div className="col-md-6">
                      <h5 className="float-left">Open Risks & Issues</h5>
                      <div className="col-md-12 float-left pl-0">
                        <p>
                          {this.validatingProjectData_OpenRiskAndIssues() ? (
                            <div
                              dangerouslySetInnerHTML={{ __html: riskIssues }}
                            ></div>
                          ) : (
                            "No Data Available"
                          )}
                        </p>
                      </div>
                    </div>
                    <div className="col-md-6 remarks-style text-size">
                      <div className="row">
                        <div className="col-md-4 text-color">
                          <br></br>
                          <span>Startup Remarks</span>
                        </div>
                        <div className="col-md-8">
                          <br></br>
                          <p>
                            {this.validatingProjectData() &&
                              this.props.projectViewData[0]
                                .Ready2StartTestRemarks !== null &&
                              this.props.projectViewData[0]
                                .Ready2StartTestRemarks !== undefined
                              ? this.props.projectViewData[0]
                                .Ready2StartTestRemarks
                              : "No Data"}
                          </p>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-4 text-color">
                          <span>Ready For Sprint Remarks</span>
                        </div>
                        <div className="col-md-8">
                          <p>
                            {this.validatingProjectData() &&
                              this.props.projectViewData[0]
                                .Ready4SprintTestRemarks !== null &&
                              this.props.projectViewData[0]
                                .Ready4SprintTestRemarks !== undefined
                              ? this.props.projectViewData[0]
                                .Ready4SprintTestRemarks
                              : "No Data"}
                          </p>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-4 text-color">
                          <span>Ready For UAT Remarks</span>
                        </div>
                        <div className="col-md-8">
                          <p>
                            {this.validatingProjectData() &&
                              this.props.projectViewData[0]
                                .Ready4UATTestRemarks !== null &&
                              this.props.projectViewData[0]
                                .Ready4UATTestRemarks !== undefined
                              ? this.props.projectViewData[0]
                                .Ready4UATTestRemarks
                              : "No Data"}
                          </p>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-4 text-color">
                          <span>Ready For PROD Remarks</span>
                        </div>
                        <div className="col-md-8">
                          <p>
                            {this.validatingProjectData() &&
                              this.props.projectViewData[0]
                                .Ready4ProdTestRemarks !== null &&
                              this.props.projectViewData[0]
                                .Ready4ProdTestRemarks !== undefined
                              ? (<div dangerouslySetInnerHTML={{
                                __html: this.props.projectViewData[0]
                                  .Ready4ProdTestRemarks
                              }} />)
                              : "No Data"}
                          </p>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-4 text-color">
                          <span>Closure Remarks</span>
                        </div>
                        <div className="col-md-8">
                          <p>
                            {this.validatingProjectData() &&
                              this.props.projectViewData[0]
                                .CCB_CLOSURE_Remarks !== null &&
                              this.props.projectViewData[0]
                                .CCB_CLOSURE_Remarks !== undefined
                              ? this.props.projectViewData[0]
                                .CCB_CLOSURE_Remarks
                              : "No Data"}
                          </p>
                        </div>
                      </div>
                      <br></br>
                    </div>
                  </div>
                </div>
              </div>
              <div className="content-info-section col-md-12 summary-status detailed-report">
                <div className="row bg-sec">
                  <div className="col-md-12">
                    <h5 className="float-left">Detailed Test Status</h5>
                  </div>
                  <div className="col-md-12">
                    <p className="float-left">
                      {this.validatingProjectData_DetailedTestStatus() ? (
                        <div
                          className="detail-status-alignment"
                          dangerouslySetInnerHTML={{
                            __html: DetailedTestStatus,
                          }}
                        ></div>
                      ) : (
                        <div className="detail-status-alignment ml-0">
                          No Data Available
                        </div>
                      )}
                    </p>
                  </div>
                </div>
              </div>
              <div className="content-info-section summary-view summary-status">
                <div className="row bg-sec summary-view-color">
                  <div className="col-md-6">
                    <div className="row">
                      <div className="col-md-12 text-size">
                        <div className="row">
                          <div className="col-md-5 text-color">
                            <br></br>
                            <label>Domain Name</label>
                          </div>
                          <div className="col-md-7">
                            <br></br>
                            <p>
                              {this.validatingProjectData()
                                ? this.props.projectViewData[0].Domain
                                : "No Data"}
                            </p>
                          </div>
                          <div className="col-md-5 text-color">
                            <label>Project Size</label>
                          </div>
                          <div className="col-md-7">
                            <p>
                              {this.validatingProjectData()
                                ? this.props.projectViewData[0].ProjectSize_MDs
                                : "No Data"}
                            </p>
                          </div>
                          <div className="col-md-5 text-color">
                            <label>Delivery Master Lead</label>
                          </div>
                          <div className="col-md-7">
                            <p>
                              {this.validatingProjectData()
                                ? this.props.projectViewData[0]
                                  .Delivery_Master_Lead
                                : "No Data"}
                            </p>
                          </div>
                          <div className="col-md-5 text-color">
                            <label>Project Manager</label>
                          </div>
                          <div className="col-md-7">
                            <p>
                              {this.validatingProjectData()
                                ? this.props.projectViewData[0].Project_Manager
                                : "No Data"}
                            </p>
                          </div>
                          <div className="col-md-5 text-color">
                            <label>Scrum Master</label>
                          </div>
                          <div className="col-md-7">
                            <p>
                              {this.validatingProjectData()
                                ? this.props.projectViewData[0].Scrum_Master
                                : "No Data"}
                            </p>
                          </div>
                          <div className="col-md-5 text-color">
                            <label>Test Coordinator</label>
                          </div>
                          <div className="col-md-7">
                            <p>
                              {this.validatingProjectData()
                                ? this.props.projectViewData[0].Test_Coordinator
                                : "No Data"}
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <br></br>
                  <div className="col-md-6 ">
                    <div className="row">
                      <div className="col-md-12">
                        <div className="row">
                          <div className="col-md-5 text-color">
                            <br></br>
                            <label>Startup</label>
                          </div>
                          <div className="col-md-7">
                            <br></br>
                            {this.handleStartUp() ? (
                              <p>
                                {this.validatingProjectData() &&
                                  this.props.projectViewData[0]
                                    .CCB_STARTUP_Date !== null &&
                                  this.props.projectViewData[0]
                                    .CCB_STARTUP_Date !== undefined
                                  ? this.props.projectViewData[0].CCB_STARTUP_Date.substr(
                                    0,
                                    10
                                  )
                                  : "No Data"}
                                <span className="project_status text-color">
                                  {this.validatingProjectData() &&
                                    this.props.projectViewData[0]
                                      .CCB_STARTUP_Decision !== null &&
                                    this.props.projectViewData[0]
                                      .CCB_STARTUP_Decision !== undefined
                                    ? this.props.projectViewData[0]
                                      .CCB_STARTUP_Decision
                                    : "No Data"}
                                </span>
                              </p>
                            ) : (
                              <p>
                                {this.validatingProjectData() &&
                                  this.props.projectViewData[0]
                                    .LastRegistrationDate !== null &&
                                  this.props.projectViewData[0]
                                    .LastRegistrationDate !== undefined
                                  ? this.props.projectViewData[0].LastRegistrationDate.substr(
                                    0,
                                    10
                                  )
                                  : "No Data"}
                                <span className="project_status text-color">
                                  {this.validatingProjectData() &&
                                    this.props.projectViewData[0]
                                      .CCB_STARTUP_Decision !== null &&
                                    this.props.projectViewData[0]
                                      .CCB_STARTUP_Decision !== undefined
                                    ? this.props.projectViewData[0]
                                      .CCB_STARTUP_Decision
                                    : "No Data"}
                                </span>
                              </p>
                            )}
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-5 text-color">
                            <label>Ready For Sprint</label>
                          </div>
                          <div className="col-md-7">
                            <p>
                              {this.validatingProjectData() &&
                                this.props.projectViewData[0]
                                  .CCB_Ready4SPRINT_Date !== null &&
                                this.props.projectViewData[0]
                                  .CCB_Ready4SPRINT_Date !== undefined
                                ? this.props.projectViewData[0].CCB_Ready4SPRINT_Date.substr(
                                  0,
                                  10
                                )
                                : "No Data"}
                              <span className="project_status">
                                {this.validatingProjectData() &&
                                  this.props.projectViewData[0]
                                    .CCB_Ready4SPRINT_Decision !== null &&
                                  this.props.projectViewData[0]
                                    .CCB_Ready4SPRINT_Decision !== undefined
                                  ? this.props.projectViewData[0]
                                    .CCB_Ready4SPRINT_Decision
                                  : "No Data"}
                              </span>
                            </p>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-5 text-color">
                            <label>Ready For UAT</label>
                          </div>
                          <div className="col-md-7">
                            <p>
                              {this.validatingProjectData() &&
                                this.props.projectViewData[0]
                                  .CCB_Ready4UAT_Date !== null &&
                                this.props.projectViewData[0]
                                  .CCB_Ready4UAT_Date !== undefined
                                ? this.props.projectViewData[0].CCB_Ready4UAT_Date.substr(
                                  0,
                                  10
                                )
                                : "No Data"}
                              <span className="project_status text-color">
                                {this.validatingProjectData() &&
                                  this.props.projectViewData[0]
                                    .CCB_Ready4UAT_Decision !== null &&
                                  this.props.projectViewData[0]
                                    .CCB_Ready4UAT_Decision !== undefined
                                  ? this.props.projectViewData[0]
                                    .CCB_Ready4UAT_Decision
                                  : "No Data"}
                              </span>
                            </p>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-5 text-color">
                            <label>UAT Start</label>
                          </div>
                          <div className="col-md-7">
                            <p>
                              {this.validatingProjectData() &&
                                this.props.projectViewData[0].UAT_START_DATE !==
                                null &&
                                this.props.projectViewData[0].UAT_START_DATE !==
                                undefined
                                ? this.props.projectViewData[0].UAT_START_DATE.substr(
                                  0,
                                  10
                                )
                                : "No Data"}
                            </p>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-5 text-color">
                            <label>UAT End</label>
                          </div>
                          <div className="col-md-7">
                            <p>
                              {this.validatingProjectData() &&
                                this.props.projectViewData[0].UAT_END_DATE !==
                                null &&
                                this.props.projectViewData[0].UAT_END_DATE !==
                                undefined
                                ? this.props.projectViewData[0].UAT_END_DATE.substr(
                                  0,
                                  10
                                )
                                : "No Data"}
                            </p>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-5 text-color">
                            <label>Freeze</label>
                          </div>
                          <div className="col-md-7">
                            <p>
                              {this.validatingProjectData() &&
                                this.props.projectViewData[0].FREEZE_DATE !==
                                null &&
                                this.props.projectViewData[0].FREEZE_DATE !==
                                undefined
                                ? this.props.projectViewData[0].FREEZE_DATE.substr(
                                  0,
                                  10
                                )
                                : "No Data"}
                            </p>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-5 text-color">
                            <label>Ready For PROD</label>
                          </div>
                          <div className="col-md-7">
                            <p>
                              {this.validatingProjectData() &&
                                this.props.projectViewData[0]
                                  .CCB_Ready4PROD_Date !== null &&
                                this.props.projectViewData[0]
                                  .CCB_Ready4PROD_Date !== undefined
                                ? this.props.projectViewData[0].CCB_Ready4PROD_Date.substr(
                                  0,
                                  10
                                )
                                : "No Data"}
                              <span className="project_status">
                                {this.validatingProjectData() &&
                                  this.props.projectViewData[0]
                                    .CCB_Ready4PROD_Decision !== null &&
                                  this.props.projectViewData[0]
                                    .CCB_Ready4PROD_Decision !== undefined
                                  ? this.props.projectViewData[0]
                                    .CCB_Ready4PROD_Decision
                                  : "No Data"}
                              </span>
                            </p>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-5 text-color">
                            <label>Closure</label>
                          </div>
                          <div className="col-md-7">
                            <p>
                              {this.validatingProjectData() &&
                                this.props.projectViewData[0].CCB_Closure_Date !==
                                null &&
                                this.props.projectViewData[0].CCB_Closure_Date !==
                                undefined
                                ? this.props.projectViewData[0].CCB_Closure_Date.substr(
                                  0,
                                  10
                                )
                                : "No Data"}
                              <span className="project_status">
                                {this.validatingProjectData() &&
                                  this.props.projectViewData[0]
                                    .CCB_CLOSURE_Decision !== null &&
                                  this.props.projectViewData[0]
                                    .CCB_CLOSURE_Decision !== undefined
                                  ? this.props.projectViewData[0]
                                    .CCB_CLOSURE_Decision
                                  : "No Data"}
                              </span>
                            </p>
                          </div>
                        </div>
                        <br></br>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="canvas_div_pdf" id="canvas_div_pdf">
          <div>
            <div className="row cta-section border-change pro-view mt-2">
              <div className="col-md-6 cs-btn-group">
                <div className="test-execution test-execcution-chart">
                  <h5>USER STORIES -{this.state.storyType.toUpperCase()}</h5>
                  <div className="row">
                    <div className="col-md-12">
                      {this.state.storyType !== config.mvp ? (
                        <div
                          style={{
                            float: "left",
                            marginLeft: "5px",
                            marginRight: "5px",
                          }}
                        >
                          <Button
                            onClick={(event) => this.userStoriesType(config.mvp)}
                            style={{
                              display: this.state.UserStoryHideInPercentNumbers
                                ? "block"
                                : "none",
                            }}
                          >
                            MVP
                          </Button>
                        </div>
                      ) : null}
                      {this.state.storyType !== config.notmvp ? (
                        <div
                          style={{
                            float: "left",
                            marginLeft: "5px",
                            marginRight: "5px",
                          }}
                        >
                          <Button
                            onClick={(event) =>
                              this.userStoriesType(config.notmvp)
                            }
                            style={{
                              display: this.state.UserStoryHideInPercentNumbers
                                ? "block"
                                : "none",
                            }}
                          >
                            NOT MVP
                          </Button>
                        </div>
                      ) : null}
                      {this.state.storyType !== config.allstories ? (
                        <div
                          style={{
                            float: "left",
                            marginLeft: "5px",
                            marginRight: "5px",
                          }}
                        >
                          <Button
                            onClick={(event) =>
                              this.userStoriesType(config.allstories)
                            }
                            style={{
                              display: this.state.UserStoryHideInPercentNumbers
                                ? "block"
                                : "none",
                            }}
                          >
                            ALL STORIES
                          </Button>
                        </div>
                      ) : null}
                      <div style={{ float: "left", marginLeft: "5px", marginRight: "5px" }}>
                        <Button
                          onClick={(event) => this.EpicType(config.epics)}
                          style={{
                            display:
                              this.state.UserStoryHideInPercentNumbers && this.state.userStorybtn === config.stories
                                ? "block"
                                : "none",
                          }}
                        >
                          Epic
                        </Button>
                      </div>
                      <div style={{ float: "left", marginLeft: "5px", marginRight: "5px" }}>
                        <Button
                          onClick={(event) => this.EpicType(config.stories)}
                          style={{
                            display:
                              this.state.UserStoryHideInPercentNumbers && this.state.userStorybtn === config.epics
                                ? "block"
                                : "none",
                          }}
                        >
                          Stories
                        </Button>
                      </div>
                      <div
                        style={{
                          float: "right",
                          marginLeft: "5px",
                          marginRight: "5px",
                        }}
                      >
                        <Button
                          className={this.state.UserstoriesInNumbermbersButton}
                          onClick={this.UserStoriesInNumbers}
                          style={{
                            //marginLeft: "auto",
                            display: this.state.UserStoryHideInPercentNumbers
                              ? "block"
                              : "none",
                          }}
                        >
                          #
                        </Button>
                      </div>
                      <div
                        style={{
                          float: "right",
                          marginLeft: "5px",
                          marginRight: "5px",
                        }}
                      >
                        <Button
                          className={this.state.UserstoriesInpercentButton}
                          onClick={this.UserStoriesInPercent}
                          style={{
                            //marginLeft: "auto",
                            display: this.state.UserStoryHideInPercentNumbers
                              ? "block"
                              : "none",
                          }}
                        >
                          %
                        </Button>
                      </div>
                      <div
                        style={{
                          float: "right",
                          marginLeft: "auto",
                          marginRight: "5px",
                        }}
                      >
                        <Button
                          onClick={this.storiesBack}
                          className="active"
                          style={{
                            //marginLeft: "auto",
                            display: this.state.UserStoriesBack
                              ? "block"
                              : "none",
                          }}
                        >
                          Back
                        </Button>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-12">
                      {this.state.UserStoriesInpercent ? (
                        <HighchartsReact
                          highcharts={Highcharts}
                          options={this.StorieschartOptionsInPercent()}
                        />
                      ) : (
                        <HighchartsReact
                          highcharts={Highcharts}
                          options={this.StorieschartOptions()}
                        />
                      )}
                    </div>
                  </div>
                </div>
                <br></br>
                <div className="test-execution test-execcution-chart cs-btn-group">
                  <h5>REQUIREMENT COVERAGE</h5>
                  <div className="row">
                    <div className="col-md-12">
                      <div
                        style={{
                          float: "right",
                          marginLeft: "5px",
                          marginRight: "5px",
                        }}
                      >
                        <Button
                          className={this.state.RequirementInNumbermbersButton}
                          onClick={this.RequirementInNumbers}
                          style={{
                            //marginLeft: "auto",
                            display:
                              this.state.RequirementHideInPercentNumbers &&
                                !this.state.RequirementCoverageBack
                                ? "block"
                                : "none",
                          }}
                        >
                          #
                        </Button>
                      </div>
                      <div
                        style={{
                          float: "right",
                          marginLeft: "5px",
                          marginRight: "5px",
                        }}
                      >
                        <Button
                          className={this.state.RequirementInpercentButton}
                          onClick={this.RequirementInPercent}
                          style={{
                            //marginLeft: "auto",
                            display:
                              this.state.RequirementHideInPercentNumbers &&
                                !this.state.RequirementCoverageBack
                                ? "block"
                                : "none",
                          }}
                        >
                          %
                        </Button>
                      </div>
                      <div
                        style={{
                          float: "right",
                          marginLeft: "auto",
                          marginRight: "5px",
                        }}
                      >
                        <Button
                          onClick={this.requirementBack}
                          className="active"
                          style={{
                            //marginLeft: "auto",
                            display: this.state.RequirementCoverageBack
                              ? "block"
                              : "none",
                          }}
                        >
                          Back
                        </Button>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-12">
                      {this.state.RequirementInpercent ? (
                        <HighchartsReact
                          highcharts={Highcharts}
                          options={this.RequirementchartOptionsInPercent()}
                        />
                      ) : (
                        <HighchartsReact
                          highcharts={Highcharts}
                          options={this.RequirementchartOptions()}
                        />
                      )}
                    </div>
                  </div>
                </div>
                <br></br>
                <div className="test-execution test-execcution-chart cs-btn-group">
                  <h5>TEST TYPES - TEST EXECUTION</h5>
                  <div className="row">
                    <div className="col-md-12">
                      {this.state.Inpercent ? (
                        <HighchartsReact
                          highcharts={Highcharts}
                          options={this.DynamicchartOptionsInPercent(
                            this.state.secondseries
                          )}
                        />
                      ) : (
                        <HighchartsReact
                          highcharts={Highcharts}
                          options={this.DynamicchartOptions(
                            this.state.secondseries
                          )}
                        />
                      )}
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-6 pr-0">
                <div className="cs-btn-group open-defects-btn">
                  <div className="open-defects-action-btn">
                    <Button
                      type="primary"
                      onClick={(event) =>
                        this.DefectByStatus(config.allDefects, config.allDefects)
                      }
                      className={this.state.StatusButton}
                    >
                      By Status
                    </Button>
                  </div>
                  <div className="open-defects-action-btn">
                    <Button
                      type="primary"
                      className={this.state.AssigneeButton}
                      onClick={(event) =>
                        this.DefectByAssignee(
                          config.allDefects,
                          config.allDefects
                        )
                      }
                    >
                      By Assignee
                    </Button>
                  </div>
                  <div className="open-defects-action-btn">
                    <Button
                      type="primary"
                      onClick={(event) =>
                        this.DefectByComponent(
                          config.allDefects,
                          config.allDefects
                        )
                      }
                      className={this.state.ComponentButton}
                    >
                      By Epic
                    </Button>
                  </div>
                </div>

                <div className="open_defeccts open-defects-chart open-defects-chart-new">
                  <h5>
                    {/* DEFECTS -{" "} */}
                    {this.props.pdfToggle !== undefined
                      ? this.DefectsName()
                      : config.allDefects.toUpperCase()}
                  </h5>
                  <div className="row cs-btn-group">
                    <div className="col-md-12 mvp-btn">
                      <div
                        style={{
                          float: "left",
                          marginLeft: "auto",
                          marginRight: "10px",
                        }}
                      >
                        <Button
                          className={allDefects}
                          onClick={(event: any) =>
                            this.DefectsCategories(config.allDefects)
                          }
                        >
                          All
                        </Button>
                      </div>
                      <div
                        style={{
                          float: "left",
                          marginLeft: "auto",
                          marginRight: "10px",
                        }}
                      >
                        <Button
                          className={openDefects}
                          onClick={(event: any) =>
                            this.DefectsCategories(config.openDefects)
                          }
                        >
                          Open
                        </Button>
                      </div>

                      <div
                        style={{
                          float: "left",
                          marginLeft: "auto",
                          marginRight: "10px",
                        }}
                      >
                        <Button
                          className={storyDefects}
                          onClick={(event: any) =>
                            this.DefectsCategories(config.storyDefects)
                          }
                        >
                          Story Defects
                        </Button>
                      </div>
                      {this.props.pdfToggle !== config.notmvp ? (
                        <div
                          style={{
                            float: "right",
                            marginLeft: "auto",
                            marginRight: "10px",
                          }}
                        >
                          <Button
                            onClick={(event: any) =>
                              this.DefectsTypes(config.notmvp)
                            }
                          >
                            NOT MVP
                          </Button>
                        </div>
                      ) : null}
                      {this.props.pdfToggle !== config.mvp ? (
                        <div
                          style={{
                            float: "right",
                            marginLeft: "auto",
                            marginRight: "10px",
                          }}
                        >
                          <Button
                            onClick={(event: any) =>
                              this.DefectsTypes(config.mvp)
                            }
                          >
                            MVP
                          </Button>
                        </div>
                      ) : null}
                      {this.props.pdfToggle !== config.allDefects ? (
                        <div
                          style={{
                            float: "right",
                            marginLeft: "auto",
                            marginRight: "10px",
                          }}
                        >
                          <Button
                            onClick={(event: any) =>
                              this.DefectsTypes(config.allDefects)
                            }
                          >
                            TOTAL DEFECTS
                          </Button>
                        </div>
                      ) : null}
                    </div>
                  </div>
                  <Bar data={this.state.DefectData} options={this.options} />
                </div>

                <br></br>
                <div className="row test-execution test-execcution-chart cs-btn-group pdf-test-exec">
                  <div className="row heading-pdf">
                    <h5>TEST EXECUTION - {this.state.testType.toUpperCase()}</h5>
                  </div>
                  <div className="col-md-12">
                    {this.state.testType !== config.mvp ? (
                      <div
                        style={{
                          float: "left",
                          marginLeft: "5px",
                          marginRight: "5px",
                        }}
                      >
                        <Button
                          onClick={(event) => this.TestType(config.mvp)}
                          style={{
                            display: this.state.HideInPercentNumbers
                              ? "block"
                              : "none",
                          }}
                        >
                          MVP
                        </Button>
                      </div>
                    ) : null}
                    {this.state.testType !== config.notmvp ? (
                      <div
                        style={{
                          float: "left",
                          marginLeft: "5px",
                          marginRight: "5px",
                        }}
                      >
                        <Button
                          onClick={(event) => this.TestType(config.notmvp)}
                          style={{
                            display: this.state.HideInPercentNumbers
                              ? "block"
                              : "none",
                          }}
                        >
                          NOT MVP
                        </Button>
                      </div>
                    ) : null}
                    {this.state.testType !== config.alltests ? (
                      <div
                        style={{
                          float: "left",
                          marginLeft: "5px",
                          marginRight: "5px",
                        }}
                      >
                        <Button
                          onClick={(event) => this.TestType(config.alltests)}
                          style={{
                            display: this.state.HideInPercentNumbers
                              ? "block"
                              : "none",
                          }}
                        >
                          ALL TESTS
                        </Button>
                      </div>
                    ) : null}
                    <div style={{ float: "left", marginLeft: "5px", marginRight: "5px" }}>
                      <Select
                        defaultValue={config.all}
                        value={this.state.staticData}
                        style={{
                          display:
                            // // !this.state.TestExectionButton &&
                            // //   // !this.state.isUserStories &&
                            // this.state.HideInPercentNumbers &&
                            // this.state.staticFilter
                            !this.state.TestExectionButton &&
                              // !this.state.isUserStories &&
                              this.state.staticFilter
                              ? "block"
                              : "none",
                        }}
                        onChange={this.AgentBrokerFiltert}
                      >
                        {usergroup.map((obj) => (
                          <Option key={obj.Key} value={obj.Value}>
                            {obj.Value}
                          </Option>
                        ))}
                      </Select>
                      <Select
                        defaultValue={config.all}
                        value={this.state.staticData}
                        style={{
                          display:
                            !this.state.TestExectionButton &&
                              // !this.state.isUserStories &&
                              this.state.businessDomain
                              ? "block"
                              : "none",
                        }}
                        onChange={this.AgentBrokerFiltert}
                      >
                        {BussinessDomain.map((obj) => (
                          <Option key={obj.Key} value={obj.Value}>
                            {obj.Value}
                          </Option>
                        ))}
                      </Select>

                    </div>
                    <div
                      style={{
                        float: "right",
                        marginLeft: "5px",
                        marginRight: "5px",
                      }}
                    >
                      <Button
                        className={this.state.InNumbermbersButton}
                        onClick={this.InNumbers}
                        style={{
                          //marginLeft: "auto",
                          display: this.state.HideInPercentNumbers
                            ? "block"
                            : "none",
                        }}
                      >
                        #
                      </Button>
                    </div>
                    <div
                      style={{
                        float: "right",
                        marginLeft: "5px",
                        marginRight: "5px",
                      }}
                    >
                      <Button
                        className={this.state.InpercentButton}
                        onClick={this.InPercent}
                        style={{
                          //marginLeft: "auto",
                          display: this.state.HideInPercentNumbers
                            ? "block"
                            : "none",
                        }}
                      >
                        %
                      </Button>
                    </div>
                    <div
                      style={{
                        float: "right",
                        marginLeft: "auto",
                        marginRight: "5px",
                      }}
                    >
                      <Button
                        onClick={this.handleBack}
                        className="active"
                        style={{
                          //marginLeft: "auto",
                          display: this.state.TestExectionButton
                            ? "block"
                            : "none",
                        }}
                      >
                        Back
                      </Button>
                    </div>
                  </div>
                  <div className="col-md-12">
                    {this.state.Inpercent ? (
                      <HighchartsReact
                        highcharts={Highcharts}
                        options={this.chartOptionsInPercent()}
                      />
                    ) : (
                      <HighchartsReact
                        highcharts={Highcharts}
                        options={this.chartOptions()}
                      />
                    )}
                  </div>

                </div>
                <br></br>
                <div className="test-execution test-execcution-chart cs-btn-group">
                  <h5>TEST LEVELS - TEST EXECUTION</h5>

                  <div className="col-md-12">
                    {this.state.Inpercent ? (
                      <HighchartsReact
                        highcharts={Highcharts}
                        options={this.DynamicchartOptionsInPercent(
                          this.state.thirdseries
                        )}
                      />
                    ) : (
                      <HighchartsReact
                        highcharts={Highcharts}
                        options={this.DynamicchartOptions(
                          this.state.thirdseries
                        )}
                      />
                    )}
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
        {this.props.detailView ? (
          <div className="canvas_div_pdf1" id="canvas_div_pdf1">
            <div className="row cta-section border-change pdf-view-chart">
              <div className="content-info-section">
                <div className="row">
                  <div className="col-md-6">
                    <div className="test-execution test-execcution-chart cs-btn-group">
                      <h5>STORY TESTING - TEST EXECUTION</h5>
                      <div className="row">
                        <div className="col-md-12">
                          {this.state.Inpercent ? (
                            <HighchartsReact
                              highcharts={Highcharts}
                              options={this.DynamicchartOptionsInPercent(
                                this.state.forthseries
                              )}
                            />
                          ) : (
                            <HighchartsReact
                              highcharts={Highcharts}
                              options={this.DynamicchartOptions(
                                this.state.forthseries
                              )}
                            />
                          )}
                        </div>
                      </div>
                    </div>
                    <br></br>
                    <div className="test-execution test-execcution-chart cs-btn-group">
                      <h5>IT E2E TESTING - TEST EXECUTION</h5>
                      <div className="row">
                        <div className="col-md-12">
                          {this.state.Inpercent ? (
                            <HighchartsReact
                              highcharts={Highcharts}
                              options={this.DynamicchartOptionsInPercent(
                                this.state.sixthseries
                              )}
                            />
                          ) : (
                            <HighchartsReact
                              highcharts={Highcharts}
                              options={this.DynamicchartOptions(
                                this.state.sixthseries
                              )}
                            />
                          )}
                        </div>
                      </div>
                    </div>
                    <br></br>
                    <div className="test-execution test-execcution-chart cs-btn-group">
                      <h5>USER ACCEPTANCE - TEST EXECUTION</h5>
                      <div className="row">
                        <div className="col-md-12">
                          {this.state.Inpercent ? (
                            <HighchartsReact
                              highcharts={Highcharts}
                              options={this.DynamicchartOptionsInPercent(
                                this.state.eigthseries
                              )}
                            />
                          ) : (
                            <HighchartsReact
                              highcharts={Highcharts}
                              options={this.DynamicchartOptions(
                                this.state.eigthseries
                              )}
                            />
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="test-execution test-execcution-chart cs-btn-group">
                      <h5>PERFORMANCE TESTING - TEST EXECUTION</h5>
                      <div className="row">
                        <div className="col-md-12">
                          {this.state.Inpercent ? (
                            <HighchartsReact
                              highcharts={Highcharts}
                              options={this.DynamicchartOptionsInPercent(
                                this.state.fifthseries
                              )}
                            />
                          ) : (
                            <HighchartsReact
                              highcharts={Highcharts}
                              options={this.DynamicchartOptions(
                                this.state.fifthseries
                              )}
                            />
                          )}
                        </div>
                      </div>
                    </div>
                    <br></br>
                    <div className="test-execution test-execcution-chart cs-btn-group">
                      <h5>SECURITY TESTING - TEST EXECUTION</h5>
                      <div className="row">
                        <div className="col-md-12">
                          {this.state.Inpercent ? (
                            <HighchartsReact
                              highcharts={Highcharts}
                              options={this.DynamicchartOptionsInPercent(
                                this.state.seventhseries
                              )}
                            />
                          ) : (
                            <HighchartsReact
                              highcharts={Highcharts}
                              options={this.DynamicchartOptions(
                                this.state.seventhseries
                              )}
                            />
                          )}
                        </div>
                      </div>
                    </div>
                    <br></br>
                    <div className="test-execution test-execcution-chart cs-btn-group">
                      <h5>AUTOMATION TESTING - TEST EXECUTION</h5>
                      <div className="row">
                        <div className="col-md-12">
                          {this.state.Inpercent ? (
                            <HighchartsReact
                              highcharts={Highcharts}
                              options={this.DynamicchartOptionsInPercent(
                                this.state.ninthseries
                              )}
                            />
                          ) : (
                            <HighchartsReact
                              highcharts={Highcharts}
                              options={this.DynamicchartOptions(
                                this.state.ninthseries
                              )}
                            />
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : null}
        <div
          className="content-info-section defects-details-info"
          ref={(div) => {
            this._element = div;
          }}
          id="k-grid"
        >
          <div className="row">
            <div className="col-md-12">
              <h5 className="float-left">Defect Details</h5>
              <div className="open-defects-table-wid">
                <table
                  className="table defects-table"
                  ref={(table) => {
                    this.table = table;
                  }}
                >
                  <colgroup>
                    <col style={{ width: "10%" }} />
                    <col style={{ width: "40%" }} />
                    <col style={{ width: "10%" }} />
                    <col style={{ width: "10%" }} />
                    <col style={{ width: "15%" }} />
                  </colgroup>
                  <thead>
                    <tr>
                      <th>Defect Key</th>
                      <th>Summary</th>
                      <th>Priority</th>
                      <th>Status</th>
                      <th>Assignee</th>
                    </tr>
                  </thead>
                  <tbody>{this.generateRows()}</tbody>
                </table>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <h5 className="float-left">User Stories Details</h5>
              <div className="open-defects-table-wid">
                <table
                  className="table defects-table"
                  ref={(table) => {
                    this.table = table;
                  }}
                >
                  <colgroup>
                    <col style={{ width: "10%" }} />
                    <col style={{ width: "40%" }} />
                    <col style={{ width: "10%" }} />
                    <col style={{ width: "10%" }} />
                    <col style={{ width: "15%" }} />
                  </colgroup>
                  <thead>
                    <tr>
                      <th>Story Key</th>
                      <th>Summary</th>
                      <th>Priority</th>
                      <th>Status</th>
                      <th>Assignee</th>
                    </tr>
                  </thead>
                  <tbody>{this.generateRowsUserstories()}</tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <Tooltip title="Back to Top">
          <BackTop />
        </Tooltip>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  milestoneDatas: state.summaryData.milestoneDatas,
  summaryDatas: state.summaryData.summaryDatas,
  summaryProjectDatas: state.summaryData.summaryProjectDatas,
  defectByStatus: state.defectData.bystatus,
  defectByAssignee: state.defectData.byassignee,
  defectByDomain: state.defectData.bydomain,
  defectByComponent: state.defectData.bycomponent,
  projectViewData: state.projectView.projectDatas,
  releaseHistory: state.projectView.releaseDatas,
  projectDatas: state.summaryData.projectDatas,
  allDefectData: state.defectData.allDefects,
  testResults: state.testExecution.testResults,
  componentData: state.projectView.componentData,
  requirementData: state.testExecution.testRequirement,
  userStoriesData: state.testExecution.userStories,
  detailView: state.projectView.detailedView,
  mergemsg: state.mailer.Merger,
  networkmsg: state.mailer.networkstore,
  doctype: state.projectView.doctype,
  ExternaltestReport: state.projectView.testReports,
  gateversion: state.projectView.gateversion,
  pdfToggle: state.mailer.pdfToggle,
  defectToggle: state.mailer.defectToggle,
});

const WrappedCreateProjectViewPdf = Form.create({ name: "ProjectViewPdf" })(
  ProjectViewPdf
);

export default connect(mapStateToProps, {
  DefectByStatusCharts,
  DefectByAssigneeCharts,
  DefectByComponentCharts,
  DefectByDomainsCharts,
  InitialDefectByAssigneeCharts,
  InitialStatusChartsByProjectLevel,
  StatusChartsByProjectLevel,
  TestExecutionStatus,
  getChartData,
  PdfComponentStore,
  MailingPDF,
  MergingPdf,
  ClearingPdfMessage,
  StoringInNetworkPath,
  ClearingNetworkPdfMessage,
  DefectsTogglePDF,
  pdfPreviewSelection,
  PdfDefectsCategories,
  DynamicTestExecutionStatus,
  storeDocType,
  storeGateVersion
})(WrappedCreateProjectViewPdf);
