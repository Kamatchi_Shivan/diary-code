export const Defects_colors = [
  {
    label: "Blocker",
    color: "rgba(255, 0, 0, 0.8)",

  },
  {
    label: "Critical",
    color: "rgba(255, 0, 0, 0.6)",
  },
  {
    label: "Major",
    color: "rgb(255,140,0)",
  },
  {
    label: "High",
    color: "rgb(255,165,0)",
  },
  {
    label: "Medium",
    // color: "rgb(255,182,200)",
    color: "rgb(255,200,0)",
  },
  {
    label: "Minor",
    color: "hsla(0, 100%, 70%, 0.3)",
  },
  {
    label: "Trivial",
    color: "rgb(245, 250, 172)",
  },
  {
    label: "Un Assigned",
    color: "rgb(255,0,255)",
  },
];

export const Defects_priority_order = [
  "Blocker",
  "Critical",
  "Major",
  "Medium",
  "Minor",
];

export const Defects_MVP_Priority = [
  "Blocker",
  "Critical",
  "Major",
];

export const Defects_NOTMVP_Priority = [
  "Minor",
  "Medium",
];

export const TestCase_MVP_Priority = [
  "1",
  "2",
  "empty",
];

export const TestCase_NOTMVP_Priority = [
  "3",
  "4",
  "5"
];

export const TestResults_colors = [
  { name: "Pass", color: "rgb(117, 176, 0)" },
  { name: "Fail", color: "rgb(204, 51, 0)" },
  { name: "Blocked", color: "rgb(139,0,0)" },
  { name: "Incomplete", color: "rgb(242, 176, 0)" },
  { name: "Not Executed", color: "rgb(158, 152, 125)" },
  { name: "N/A", color: "rgb(51, 102, 255)" },
  { name: "Fail: test data", color: "rgb(158, 152, 125)" },
  { name: "Not Covered", color: "#ff704d" },
];

export const TestResults_priority_order = [
  "Pass",
  "Fail",
  "Blocked",
  "Incomplete",
  "Not Executed",
  "N/A",
  "Not Covered"
];

export const RequirementResults_priority_order = [
  "Pass",
  "Fail",
  "Blocked",
  "Incomplete",
  "N/A",
  "Not Executed",
  "Not Assigned",
  "Not Covered"
];

export const RequirementData_colors = [
  { name: "Pass", color: "rgb(117, 176, 0)" },
  { name: "Fail", color: "rgb(204, 51, 0)" },
  { name: "Executed", color: "rgb(117, 176, 0)" },
  { name: "Blocked", color: "rgb(102, 147, 176)" },
  { name: "Incomplete", color: "rgb(242, 176, 0)" },
  { name: "N/A", color: "rgb(51, 102, 255)" },
  { name: "Not Assigned", color: "rgb(144, 12, 63)" },
  { name: "Not Executed", color: "rgb(158, 152, 125)" },
  { name: "Not Covered", color: "#ff704d" },
];

export const RequirementData_priority_order = [
  "Executed",
  "Blocked",
  "N/A",
  "Not Assigned",
  "Not Executed",
  "Not Covered"
];

export const RequirementData_Main_priority_order = [
  "Covered",
  "Not Covered"
];

export const RequirementData_Main_colors = [
  { name: "Covered", color: "#004080" },
  { name: "Not Covered", color: "#ff704d" },
];

export const UserStories_priority_order = [
  "Backlog",
  "Analysis",
  "Development",
  "In Test",
  "To Validate",
  "Open",
  "Closed",
  "No Status Mapped"
];

export const UserStories_Main_priority_order = [
  "Backlog",
  "Analysis",
  "Development",
  "In Test",
  "To Validate",
  "Open",
  "Closed",
  "No Status Mapped"
];

export const UserStories_Main_colors = [
  { name: "Backlog", color: "#2874A6 " },
  { name: "Analysis", color: "#B03A2E " },
  { name: "Development", color: "#117A65" },
  { name: "In Test", color: "#B7950B" },
  { name: "Closed", color: "#95A5A6" },
  { name: "To Validate", color: "#f9ac1b" },
  { name: "Open", color: "#CCCC00" },
  { name: "No Status Mapped", color: "#808080" },
];

export const Defect_status_order = [
  'Open','Development','In Test','Analysis','To Validate','Closed','BackLog'
];
