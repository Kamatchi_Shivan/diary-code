import {TestResults_colors,TestResults_priority_order,RequirementData_Main_priority_order,RequirementData_Main_colors} from "../common/Chartcolors";

/*Adding Series Values*/

export const addSeriesMultilevel = (data:any,sortingOrder:any) => {
  let FinalData = [];
  if (data.length !== 0) {
    data.forEach((d) => {
      let sum = 0;
      if (FinalData.length === 0) {
        let FilterElement = data.filter((subdata) => {
          return d.name === subdata.name;
        });
        FilterElement.forEach((datasum) => {
          sum = sum + datasum.y;
        });
        let finalobj = {
          name: d.name,
          y: sum,
        };
        FinalData.push(finalobj);
      } else {
        let m = FinalData.filter((matchdata) => {
          return d.name === matchdata.name;
        });
        if (m.length === 0) {
          let FilterElement = data.filter((subdata) => {
            return d.name === subdata.name;
          });
          FilterElement.forEach((datasum) => {
            sum = sum + datasum.y;
          });
          let finalobj = {
            name: d.name,
            y: sum,
          };
          FinalData.push(finalobj);
        }
      }
    });
  }
  return Datastoring(FinalData,sortingOrder);
}

/*Constructing the order of the Data*/

export const SeriesOrder = (data:any) => {
  let priorityData = [];
  if (data.length !== 0) {
    TestResults_priority_order.forEach((order) => {
      let TempItem = data.filter((col) => {
        return col.name === order;
      });
      if (TempItem.length !== 0) {
        let Findcolor = TestResults_colors.filter((col) => {
          return col.name === TempItem[0].name;
        });
        if (Findcolor.length !== 0) {
          TempItem[0].color = Findcolor[0].color;
        } else {
          TempItem[0].color = "#DC143C";
        }
        priorityData.push(TempItem[0]);
      }
    });
  }
  return priorityData;
}

/*Constructing the order of the Data*/

export const RequirementSeriesOrder = (data:any) => {
  let priorityData = [];
  if (data.length !== 0) {
    RequirementData_Main_priority_order.forEach((order) => {
      let TempItem = data.filter((col) => {
        return col.name === order;
      });
      if (TempItem.length !== 0) {
        let Findcolor = RequirementData_Main_colors.filter((col) => {
          return col.name === TempItem[0].name;
        });
        if (Findcolor.length !== 0) {
          TempItem[0].color = Findcolor[0].color;
        } else {
          TempItem[0].color = "#DC143C";
        }
        priorityData.push(TempItem[0]);
      }
    });
  }
  return priorityData;
}

/*Sorting Function*/

export function Datastoring(data,sortOrder){
  let FinalArray:any = [];
  for(let b = 0;b<sortOrder.length;b++){
    let TempItem = data.filter((col) => {
      return col.name === sortOrder[b];
    });
   if(TempItem.length === 0){
     let tempObj = {
       name:sortOrder[b],
       y:0
     }
     FinalArray.splice(b,0,tempObj);
     data.splice(b,0,tempObj);
    }
    if(TempItem.length !== 0){
      FinalArray.push(TempItem[0]);
    }
  }
  return FinalArray;
}