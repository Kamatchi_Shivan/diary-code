export const DataFormation = (count:any) => {
  if(count === 2){
    let folders = {
     folder1:"folder",
     folder2:"subFolder"
   }
   return folders;
  }
  if(count === 3){
    let folders = {
     folder1:"subFolder",
     folder2:"subFolder1"
   }
   return folders;
  }
    if(count === 4){
       let folders = {
        folder1:"subFolder1",
        folder2:"subFolder2"
      }
      return folders;
    }else if(count === 5){
      let folders = {
        folder1:"subFolder2",
        folder2:"subFolder3"
      }
      return folders;
    }else if(count === 6){
      let folders = {
        folder1:"subFolder3",
        folder2:"subFolder4"
      }
      return folders;
    }else if(count === 7){
      let folders = {
        folder1:"subFolder4",
        folder2:"subFolder5"
      }
      return folders;
    }else if(count === 8){
      let folders = {
        folder1:"subFolder5",
        folder2:"subFolder6"
      }
      return folders;
    }else if(count === 9){
      let folders = {
        folder1:"subFolder6",
        folder2:"subFolder7"
      }
      return folders;
    }else if(count === 10){
      let folders = {
        folder1:"subFolder7",
        folder2:"subFolder8"
      }
      return folders;
    }else if(count === 11){
      let folders = {
        folder1:"subFolder8",
        folder2:"subFolder9"
      }
      return folders;
    }else if(count === 12){
      let folders = {
        folder1:"subFolder9",
        folder2:"subFolder10"
      }
      return folders;
    }else if(count === 13){
      let folders = {
        folder1:"subFolder10",
        folder2:"subFolder10"
      }
      return folders;
    }
}

export const RequirementDataFormation = (count:any) => {
  if(count === 2){
    let folders = {
     folder1:"RequirementTreeNode",
     folder2:"RequirementTreeSubNode"
   }
   return folders;
  }
  if(count === 3){
    let folders = {
     folder1:"RequirementTreeSubNode",
     folder2:"RequirementTreeSubNode1"
   }
   return folders;
  }
    if(count === 4){
       let folders = {
        folder1:"RequirementTreeSubNode1",
        folder2:"RequirementTreeSubNode2"
      }
      return folders;
    }else if(count === 5){
      let folders = {
        folder1:"RequirementTreeSubNode2",
        folder2:"RequirementTreeSubNode3"
      }
      return folders;
    }else if(count === 6){
      let folders = {
        folder1:"RequirementTreeSubNode3",
        folder2:"RequirementTreeSubNode4"
      }
      return folders;
    }else if(count === 7){
      let folders = {
        folder1:"RequirementTreeSubNode4",
        folder2:"RequirementTreeSubNode5"
      }
      return folders;
    }else if(count === 8){
      let folders = {
        folder1:"RequirementTreeSubNode5",
        folder2:"RequirementTreeSubNode6"
      }
      return folders;
    }else if(count === 9){
      let folders = {
        folder1:"RequirementTreeSubNode6",
        folder2:"RequirementTreeSubNode7"
      }
      return folders;
    }else if(count === 10){
      let folders = {
        folder1:"RequirementTreeSubNode7",
        folder2:"RequirementTreeSubNode8"
      }
      return folders;
    }else if(count === 11){
      let folders = {
        folder1:"RequirementTreeSubNode8",
        folder2:"RequirementTreeSubNode9"
      }
      return folders;
    }else if(count === 12){
      let folders = {
        folder1:"RequirementTreeSubNode9",
        folder2:"RequirementTreeSubNode10"
      }
      return folders;
    }else if(count === 13){
      let folders = {
        folder1:"RequirementTreeSubNode10",
        folder2:"RequirementTreeSubNode10"
      }
      return folders;
    }
}

