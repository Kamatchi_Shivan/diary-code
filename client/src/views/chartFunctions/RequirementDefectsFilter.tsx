import {
  RequirementsubFolderCapture,
  RequirementsubFolder2Capture,
  RequirementsubFolder3Capture,
  RequirementsubFolder5Capture,
  RequirementsubFolder6Capture,
  RequirementsubFolder8Capture,
} from "./TestExecutionFolderSet";
import { RequirementDataFormation } from "../common/TestExecutionFolder";

/* Requirement Tree Node*/

export const RequirementCoverageDefectFilters = (
  ProjectData: any,
  RequirementLevel: any,
  RequirementResults: any
) => {
  let RequirementDefects = [];
  let MatchingDefects = [];
  MatchingDefects = RequirementResults.filter((col) => {
    return (
      col.ProjectReference === ProjectData.ProjectReference &&
      col.ReleaseName === ProjectData.ReleaseName &&
      col.RequirementTreeNode === RequirementLevel
    );
  });

  if (MatchingDefects.length !== 0) {
    MatchingDefects.forEach((Md) => {
      if (Md.DefectId !== null && Md.DefectId !== undefined) {
        if (Md.DefectId.includes(",")) {
          let Dset = Md.DefectId.split(",");
          RequirementDefects = RequirementDefects.concat(Dset);
        } else {
          RequirementDefects.push(Md.DefectId);
        }
      }
    });
    RequirementDefects = RequirementDefects.filter(
      (value, index) => RequirementDefects.indexOf(value) === index
    );
  }
  if (RequirementDefects.length > 0) {
    return RequirementDefects;
  } else {
    return [""];
  }
};

/* Requirement Sub Tree Node*/

export const RequirementCoverageDefectFiltersSubTreeNode = (
  ProjectData: any,
  RequirementLevel: any,
  RequirementResults: any
) => {
  let RequirementDefects = [];
  let MatchingDefects = [];
  MatchingDefects = RequirementResults.filter((col) => {
    return (
      col.ProjectReference === ProjectData.ProjectReference &&
      col.ReleaseName === ProjectData.ReleaseName &&
      col.RequirementTreeSubNode === RequirementLevel
    );
  });
  if (MatchingDefects.length !== 0) {
    MatchingDefects.forEach((Md) => {
      if (Md.DefectId !== null && Md.DefectId !== undefined) {
        if (Md.DefectId.includes(",")) {
          let Dset = Md.DefectId.split(",");
          RequirementDefects = RequirementDefects.concat(Dset);
        } else {
          RequirementDefects.push(Md.DefectId);
        }
      }
    });
    RequirementDefects = RequirementDefects.filter(
      (value, index) => RequirementDefects.indexOf(value) === index
    );
  }
  if (RequirementDefects.length > 0) {
    return RequirementDefects;
  } else {
    return [""];
  }
};

/* Dynamic Sub Folder 1*/

export const RequirementsByDynamicSubFolderDefectLevel = (
  ProjectData: any,
  RequirementLevel: any,
  RequirementResults: any,
  RequirementcaptureDataset: any,
  count: any
) => {
  let RequirementDefects = [];
  let MatchingDefects = [];
  let folderdata = RequirementDataFormation(count);
  MatchingDefects = RequirementResults.filter((col) => {
    return (
      col.ProjectReference === ProjectData.ProjectReference &&
      col.ReleaseName === ProjectData.ReleaseName &&
      col.RequirementTreeNode === RequirementcaptureDataset &&
      col[folderdata.folder1] === RequirementLevel &&
      col.DefectId !== "" &&
      col.DefectId !== null
    );
  });
  MatchingDefects = constructingDynamicData(count, MatchingDefects);
  if (MatchingDefects.length !== 0) {
    MatchingDefects.forEach((Md) => {
      if (Md.DefectId !== null && Md.DefectId !== undefined) {
        if (Md.DefectId.includes(",")) {
          let Dset = Md.DefectId.split(",");
          RequirementDefects = RequirementDefects.concat(Dset);
        } else {
          RequirementDefects.push(Md.DefectId);
        }
      }
    });
    RequirementDefects = RequirementDefects.filter(
      (value, index) => RequirementDefects.indexOf(value) === index
    );
  }
  if (RequirementDefects.length > 0) {
    return RequirementDefects;
  } else {
    return [""];
  }
};

/* Dynamic Data filter */

function constructingDynamicData(count, TestResults) {
  let requirementData = TestResults;
  if (count === 4) {
    requirementData = TestResults.filter((col) => {
      return col.RequirementTreeSubNode === RequirementsubFolderCapture;
    });
  } else if (count > 4 && count <= 6) {
    requirementData = TestResults.filter((col) => {
      return (
        col.RequirementTreeSubNode === RequirementsubFolderCapture &&
        col.RequirementTreeSubNode1 === RequirementsubFolder2Capture
      );
    });
  } else if (count > 6 && count <= 7) {
    requirementData = TestResults.filter((col) => {
      return (
        col.RequirementTreeSubNode === RequirementsubFolderCapture &&
        col.RequirementTreeSubNode1 === RequirementsubFolder2Capture &&
        col.RequirementTreeSubNode3 === RequirementsubFolder3Capture
      );
    });
  } else if (count > 7 && count >= 8) {
    requirementData = TestResults.filter((col) => {
      return (
        col.RequirementTreeSubNode === RequirementsubFolderCapture &&
        col.RequirementTreeSubNode1 === RequirementsubFolder2Capture &&
        col.RequirementTreeSubNode3 === RequirementsubFolder3Capture &&
        col.RequirementTreeSubNode4 === RequirementsubFolder5Capture
      );
    });
  } else if (count >= 9 && count <= 10) {
    requirementData = TestResults.filter((col) => {
      return (
        col.RequirementTreeSubNode === RequirementsubFolderCapture &&
        col.RequirementTreeSubNode1 === RequirementsubFolder2Capture &&
        col.RequirementTreeSubNode3 === RequirementsubFolder3Capture &&
        col.RequirementTreeSubNode4 === RequirementsubFolder5Capture &&
        col.RequirementTreeSubNode6 === RequirementsubFolder6Capture
      );
    });
  } else if (count >= 11) {
    requirementData = TestResults.filter((col) => {
      return (
        col.RequirementTreeSubNode === RequirementsubFolderCapture &&
        col.RequirementTreeSubNode1 === RequirementsubFolder2Capture &&
        col.RequirementTreeSubNode3 === RequirementsubFolder3Capture &&
        col.RequirementTreeSubNode4 === RequirementsubFolder5Capture &&
        col.RequirementTreeSubNode6 === RequirementsubFolder6Capture &&
        col.RequirementTreeSubNode8 === RequirementsubFolder8Capture
      );
    });
  }
  return requirementData;
}
