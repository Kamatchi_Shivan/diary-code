import { addSeriesMultilevel } from "../common/TestExecutionCommonFunctions";
import {
  UserStories_Main_priority_order,
  UserStories_Main_colors,
  Defects_MVP_Priority,
  Defects_NOTMVP_Priority,
} from "../common/Chartcolors";
import { config } from "../../config/configs";
import { EpicFirstLevelMainLevel,EpicSecondLevelMainLevel } from "./EpicLinkChartFunctions";
import { ConstructingUserStoryTableView,ConstructingFirstLevelUserStoryTableView } from "./userStoryTableView";

/*Main Function*/

export const UserStoriesFunction = (
  UserStoriesData: any,
  Project: any,
  count: any,
  dataset: any,
  priorityType: any,
  component: any,
  btnValue: any
) => {
  // let btnValue = "epic";
  console.log(btnValue)
  if (count === 1 && dataset === "projects") {
    let DefectData = [];
    let UserStories = UserStoriesMainLevel(
      UserStoriesData,
      Project,
      priorityType,
      component,
      btnValue
    );
    let FinalObj = {
      DefectData: DefectData,
      UserStories: UserStories,
    };
    console.log(FinalObj,109)
    return FinalObj;
  }
  if (count === 1 && dataset !== "projects") {
    let DefectData = [];
    // let UserStories = UserStoriesSecondLevel(
    //   UserStoriesData,
    //   Project,
    //   dataset,
    //   priorityType,
    //   component,
    //   btnValue
    // );
    let UserStories = UserStoriesThirdLevel(
      UserStoriesData,
      Project,
      dataset,
      priorityType,
      component,
      btnValue
    );
    let FinalObj = {
      DefectData: DefectData,
      UserStories: UserStories,
    };

    return FinalObj;
  }
  if (count === 2) {
    let DefectData = [];
    let UserStories = UserStoriesThirdLevel(
      UserStoriesData,
      Project,
      dataset,
      priorityType,
      component,
      btnValue
    );
    let FinalObj = {
      DefectData: DefectData,
      UserStories: UserStories,
    };
    return FinalObj;
  }
  if (count === 3) {
    let DefectData = [];
    let UserStories = [];
    let FinalObj = {
      DefectData: DefectData,
      UserStories: UserStories,
    };
    return FinalObj;
  }
};

/*Level 1 Data */

export const UserStoriesMainLevel = (
  UserStoriesData: any,
  Project: any,
  priorityType: any,
  component: any,
  btnValue: any
) => {
  if (
    UserStoriesData !== undefined &&
    UserStoriesData !== null &&
    Project !== null &&
    Project !== undefined
  ) {
    let sortingOrder = [];
    let UserStoriesResults = UserStoriesData.filter((col) => {
      return (
        col.ProjectReference === Project.ProjectReference &&
        col.fixVersions === Project.ReleaseName
      );
    });
    UserStoriesResults = ConstructingDataBasedOnType(
      UserStoriesResults,
      priorityType
    );
    UserStoriesResults = ConstructingDataBasedOnComponent(
      UserStoriesResults,
      component
    );
    UserStoriesResults.forEach((sort) => {
      sortingOrder.push(sort.projectName);
    });
    if (sortingOrder.length !== 0) {
      sortingOrder = sortingOrder.filter(
        (value, index) => sortingOrder.indexOf(value) === index
      );
    }
    if (UserStoriesResults !== undefined && UserStoriesResults.length !== 0) {
      let series = [];
      let storyStatus = [];
      for (let i = 0; i < UserStoriesResults.length; i++) {
        let testExecutionData = UserStoriesResults[i];
        if (
          testExecutionData.storyStatus !== null &&
          testExecutionData.storyStatus !== undefined
        ) {
          if (storyStatus.length === 0) {
            storyStatus.push(testExecutionData.storyStatus);
            let data = [];
            let drillobject = {
              name: testExecutionData.projectName,
              y: testExecutionData.ProjectCount,
            };
            data.push(drillobject);
            let seriesObj = {
              name: testExecutionData.storyStatus,
              data: data,
            };
            series.push(seriesObj);
          } else {
            let storyStatusFind = storyStatus.filter((col) => {
              return col === testExecutionData.storyStatus;
            });
            if (storyStatusFind.length === 0) {
              storyStatus.push(testExecutionData.storyStatus);
              let data = [];
              let drillobject = {
                name: testExecutionData.projectName,
                y: testExecutionData.statusCount,
              };
              data.push(drillobject);
              let seriesObj = {
                name: testExecutionData.storyStatus,
                data: data,
              };
              series.push(seriesObj);
            } else {
              let EditSerisObj = series.filter((col) => {
                return col.name === testExecutionData.storyStatus;
              });
              let drillobject = {
                name: testExecutionData.projectName,
                y: testExecutionData.statusCount,
              };
              EditSerisObj[0].data.push(drillobject);
            }
          }
        }
      }
      let Finalseries = [];
      series.forEach((s) => {
        s.data = addSeriesMultilevel(s.data, sortingOrder);
        Finalseries.push(s);
      });
      Finalseries = SeriesOrderForMainFunction(Finalseries);
      let tableViewData:any;
      if(btnValue === config.epics){
        let EpicData = EpicFirstLevelMainLevel(
          UserStoriesData,
          Project,
          priorityType,
          component,
          btnValue
        );
        console.log(EpicData)
        tableViewData = EpicData.tableViewData;

      }else{
        tableViewData = ConstructingFirstLevelUserStoryTableView(sortingOrder,Finalseries,btnValue);
      }
      return {
        Finalseries: Finalseries,
        tableViewData: tableViewData
      };
    } else {
      return {
        Finalseries: [],
        tableViewData: []
      };
    }
  } else {
    return {
      Finalseries: [],
      tableViewData: []
    };
  }
};

/* Level 2 Data*/

export const UserStoriesSecondLevel = (
  UserStoriesData: any,
  Project: any,
  UserStoryStatus: any,
  priorityType: any,
  component: any,
  btnValue: any
) => {
  if (
    UserStoriesData !== undefined &&
    UserStoriesData !== null &&
    Project !== null &&
    Project !== undefined
  ) {
    if(btnValue === config.epics){
      return EpicFirstLevelMainLevel(
        UserStoriesData,
        Project,
        priorityType,
        component,
        btnValue
      );
    }else{
      let sortingOrder = [];
      let storiesData = UserStoriesData.filter((col) => {
        return (
          col.ProjectReference === Project.ProjectReference &&
          col.fixVersions === Project.ReleaseName
          // &&
          // col.projectName === UserStoryStatus
        );
      });
      storiesData = ConstructingDataBasedOnType(storiesData, priorityType);
      storiesData = ConstructingDataBasedOnComponent(
        storiesData,
        component
      );
      storiesData.forEach((sort) => {
        if (
          sort.storyStatus !== null &&
          sort.storyStatus !== undefined &&
          sort.storyStatus !== ""
        ) {
        sortingOrder.push(sort.storyStatus);
        }
      });
      if (sortingOrder.length !== 0) {
        sortingOrder = sortingOrder.filter(
          (value, index) => sortingOrder.indexOf(value) === index
        );
      }

      if (storiesData !== undefined && storiesData.length !== 0) {
        let series = [];
        let storyStatus = [];
        for (let i = 0; i < storiesData.length; i++) {
          let testExecutionData = storiesData[i];
          if (
            testExecutionData.storyStatus !== null &&
            testExecutionData.storyStatus !== undefined
          ) {
            if (storyStatus.length === 0) {
              storyStatus.push(testExecutionData.storyStatus);
              let data = [];
              let drillobject = {
                name: testExecutionData.storyStatus,
                y: testExecutionData.statusCount,
              };
              data.push(drillobject);
              let seriesObj = {
                name: testExecutionData.storyStatus,
                data: data,
              };
              series.push(seriesObj);
            } else {
              let storyStatusFind = storyStatus.filter((col) => {
                return col === testExecutionData.storyStatus;
              });
              if (storyStatusFind.length === 0) {
                storyStatus.push(testExecutionData.storyStatus);
                let data = [];
                let drillobject = {
                  name: testExecutionData.storyStatus,
                  y: testExecutionData.statusCount,
                };
                data.push(drillobject);
                let seriesObj = {
                  name: testExecutionData.storyStatus,
                  data: data,
                };
                series.push(seriesObj);
              } else {
                let EditSerisObj = series.filter((col) => {
                  return col.name === testExecutionData.storyStatus;
                });
                let drillobject = {
                  name: testExecutionData.storyStatus,
                  y: testExecutionData.statusCount,
                };
                EditSerisObj[0].data.push(drillobject);
              }
            }
          }
        }
        let Finalseries = [];
        series.forEach((s) => {
          s.data = addSeriesUserStories(s.data, sortingOrder);
          Finalseries.push(s);
        });
        Finalseries = SeriesOrderForSecondLevelFunction(Finalseries);
        let tableViewData = ConstructingUserStoryTableView(Finalseries);
        if (Finalseries.length !== 0) {
          return {
            Finalseries: Finalseries,
            tableViewData: tableViewData
          };
        } else {
          return 0;
        }
      } else {
        return 0;
      }

    }

  } else {
    return 0;
  }
};

/* Level 2 Data*/

export const UserStoriesThirdLevel = (
  UserStoriesData: any,
  Project: any,
  UserStoryStatus: any,
  priorityType: any,
  component: any,
  btnValue: any
) => {
  if (
    UserStoriesData !== undefined &&
    UserStoriesData !== null &&
    Project !== null &&
    Project !== undefined
  ) {
    console.log(btnValue)
    if(btnValue === config.epics){
      return (
        EpicSecondLevelMainLevel(
          UserStoriesData,
          Project,
          priorityType,
          component,
          btnValue
        )
      );
    }else{
      let sortingOrder = [];
      let storiesData = UserStoriesData.filter((col) => {
        return (
          col.ProjectReference === Project.ProjectReference &&
          col.fixVersions === Project.ReleaseName
          // &&
          // col.storyStatus === UserStoryStatus
        );
      });
      storiesData = ConstructingDataBasedOnType(storiesData, priorityType);
      storiesData = ConstructingDataBasedOnComponent(
        storiesData,
        component
      );
      storiesData.forEach((sort) => {
        if (
          sort.storyStatus !== null &&
          sort.storyStatus !== undefined &&
          sort.storyStatus !== ""
        ) {
        sortingOrder.push(sort.storyKey+"_"+sort.summary);
        }
      });
      if (sortingOrder.length !== 0) {
        sortingOrder = sortingOrder.filter(
          (value, index) => sortingOrder.indexOf(value) === index
        );
      }
      if (storiesData !== undefined && storiesData.length !== 0) {
        let series = [];
        let storyStatus = [];
        for (let i = 0; i < storiesData.length; i++) {
          let testExecutionData = storiesData[i];
          if (
            testExecutionData.storyStatus !== null &&
            testExecutionData.storyStatus !== undefined
          ) {
            if (storyStatus.length === 0) {
              let data = [];
              if (
                testExecutionData.storyKey !== null &&
                testExecutionData.storyKey !== ""
              ) {
                storyStatus.push(testExecutionData.storyStatus);
                let drillobject = {
                  name: testExecutionData.storyKey+"_"+testExecutionData.summary,
                  y: testExecutionData.statusCount,
                };
                data.push(drillobject);
                let seriesObj = {
                  name: testExecutionData.storyStatus,
                  // pointWidth: 20,
                  data: data,
                };
                series.push(seriesObj);
              }
            } else {
              let storyStatusFind = storyStatus.filter((col) => {
                return col === testExecutionData.storyStatus;
              });
              if (storyStatusFind.length === 0) {
                let data = [];
                if (
                  testExecutionData.storyKey !== null &&
                  testExecutionData.storyKey !== ""
                ) {
                  storyStatus.push(testExecutionData.storyStatus);
                  let drillobject = {
                    name: testExecutionData.storyKey+"_"+testExecutionData.summary,
                    y: testExecutionData.statusCount,
                  };
                  data.push(drillobject);
                  let seriesObj = {
                    name: testExecutionData.storyStatus,
                    // pointWidth: 20,
                    data: data,
                  };
                  series.push(seriesObj);
                }
              } else {
                let EditSerisObj = series.filter((col) => {
                  return col.name === testExecutionData.storyStatus;
                });
                if (EditSerisObj.length !== 0) {
                  if (
                    testExecutionData.storyKey !== null &&
                    testExecutionData.storyKey !== ""
                  ) {
                    let drillobject = {
                      name: testExecutionData.storyKey+"_"+testExecutionData.summary,
                      y: testExecutionData.statusCount,
                    };
                    EditSerisObj[0].data.push(drillobject);
                  }
                }
              }
            }
          } else {
            return [];
          }
        }
        let Finalseries = [];
        series.forEach((s) => {
          s.data = addSeriesMultilevel(s.data, sortingOrder);
          Finalseries.push(s);
        });
        Finalseries = SeriesOrderForMainFunction(Finalseries);
        let tableViewData = ConstructingUserStoryTableView(Finalseries);
        if (Finalseries.length !== 0) {
          return {
            Finalseries: Finalseries,
            tableViewData: tableViewData
          };
        } else {
          return 0;
        }
      } else {
        return 0;
      }
    }
  } else {
    return 0;
  }
};



/*Constructing the order of the Data*/

function SeriesOrderForMainFunction(data) {
  let priorityData = [];
  if (data.length !== 0) {
    UserStories_Main_priority_order.forEach((order) => {
      let TempItem = data.filter((col) => {
        return col.name === order;
      });
      if (TempItem.length !== 0) {
        let Findcolor = UserStories_Main_colors.filter((col) => {
          return col.name === TempItem[0].name;
        });
        if (Findcolor.length !== 0) {
          TempItem[0].color = Findcolor[0].color;
        } else {
          TempItem[0].color = "#DC143C";
        }
        priorityData.push(TempItem[0]);
      }
    });
  }
  return priorityData;
}

/*Constructing the order of the Data*/

function SubSeriesOrderForSecondLevelFunction(data) {
  let priorityData = [];
  if (data.length !== 0) {
    UserStories_Main_priority_order.forEach((order) => {
      let TempItem = data.filter((col) => {
        return col.name === order;
      });
      if (TempItem.length !== 0) {
          priorityData.push(TempItem[0]);
      }
    });
  }
  return priorityData;
}

/*Constructing the order of the Data*/

function SeriesOrderForSecondLevelFunction(data) {
  let priorityData = [];
  if (data.length !== 0) {
    UserStories_Main_priority_order.forEach((order) => {
      let TempItem = data.filter((col) => {
        return col.name === order;
      });
      if (TempItem.length !== 0) {
        let Findcolor = UserStories_Main_colors.filter((col) => {
          return col.name === TempItem[0].name;
        });
        if (Findcolor.length !== 0) {
          TempItem[0].color = Findcolor[0].color;
        } else {
          TempItem[0].color = "#DC143C";
        }
        TempItem[0].data = SubSeriesOrderForSecondLevelFunction(TempItem[0].data);
        priorityData.push(TempItem[0]);
      }
    });
  }
  return priorityData;
}


/*Adding Series Values for User Stories*/

export const addSeriesUserStories = (data: any, sortingOrder: any) => {
  let FinalData = [];
  if (data.length !== 0) {
    data.forEach((d) => {
      let sum = 0;
      if (FinalData.length === 0) {
        let FilterElement = data.filter((subdata) => {
          return d.name === subdata.name;
        });
        FilterElement.forEach((datasum) => {
          sum = sum + datasum.y;
        });
        let finalobj = {
          name: d.name,
          y: sum,
        };
        FinalData.push(finalobj);
      } else {
        let m = FinalData.filter((matchdata) => {
          return d.name === matchdata.name;
        });
        if (m.length === 0) {
          let FilterElement = data.filter((subdata) => {
            return d.name === subdata.name;
          });
          FilterElement.forEach((datasum) => {
            sum = sum + datasum.y;
          });
          let finalobj = {
            name: d.name,
            y: sum,
          };
          FinalData.push(finalobj);
        }
      }
    });
  }
  return Datastoring(FinalData, sortingOrder);
};

/*Sorting Function*/

export function Datastoring(data, sortOrder) {
  for (let b = 0; b < sortOrder.length; b++) {
    let TempItem = data.filter((col) => {
      return col.name === sortOrder[b];
    });
    if (TempItem.length === 0) {
      let tempObj = {
        name: sortOrder[b],
        y: 0,
      };
      data.splice(b, 0, tempObj);
    }
  }
  return data;
}

/**Point Width Calculation */

export function PointwidthCal(Finalseries) {
  if (Finalseries.length !== 0) {
    if (Finalseries[0].data.length > 1) {
      for (let obj = 0; obj < Finalseries.length; obj++) {
        Finalseries[obj].pointWidth = 10;
      }
    }
  }
  return Finalseries;
}

/* Based on Type */

function ConstructingDataBasedOnType(data, type) {
  let priorityData = [];
  if (data.length !== 0) {
    if (type === config.allstories) {
      priorityData = data;
    } else if (type === config.mvp) {
      Defects_MVP_Priority.forEach((order) => {
        let TempItem = data.filter((col) => {
          return col.priority === order;
        });
        if (TempItem.length !== 0) {
          priorityData = priorityData.concat(TempItem);
        }
      });
    } else {
      Defects_NOTMVP_Priority.forEach((order) => {
        let TempItem = data.filter((col) => {
          return col.priority === order;
        });
        if (TempItem.length !== 0) {
          priorityData = priorityData.concat(TempItem);
        }
      });
    }
  }
  return priorityData;
}


/* Based on Component */

function ConstructingDataBasedOnComponent(data, Component) {
  let priorityData = [];
  if (data.length !== 0) {
    if (Component.epicLink === config.allEpic ){
      priorityData = data;
    } else {
      let ComponentData = data.filter((col) => {
        return col.epicLink !== null;
      });
      if(ComponentData.length !== 0){
        priorityData = ComponentData.filter((col) => {
          return col.epicLink === Component.epicLink;
        });
      }
    }
  }
  return priorityData;
}
