export const ConstructingDefectView = (data: any, label: any) => {
  let columns = [];
  let rows = [];
  if (data.length !== 0 && label.length !== 0) {
    data.forEach((column: any) => {
      if (columns.length === 0) {
        columns.push("");
      }
      columns.push(column.label);
    });
    for (let i = 0; i < label.length; i++) {
      let dataObj = {};
      let dataArray = [];
      if (dataArray.length === 0) {
        dataArray.push(label[i]);
      }
      for (let j = 0; j < data.length; j++) {
        let tempData = data[j].data;
        for (let k = 0; k < tempData.length; k++) {
          if (i === k) {
            dataArray.push(tempData[k]);
          }
        }
        if (j === data.length - 1) {
          if (dataArray.length !== columns.length) {
            EqualizingArrayData(dataArray, columns.length);
          }
          dataObj = {
            dataArray,
          };
          rows.push(dataObj);
        }
      }
    }
    return {
      columns: columns,
      rows: rows,
    };
  } else {
    return {
      columns: [],
      rows: [],
    };
  }
};

function EqualizingArrayData(data, length) {
  if (data.length !== length) {
    for (let i = 0; i < length; i++) {
      if (i >= data.length) {
        data.push(0);
      }
    }
  }
  return data;
}
