import { addSeriesMultilevel } from "../common/TestExecutionCommonFunctions";
import {
  UserStories_Main_priority_order,
  UserStories_Main_colors,
} from "../common/Chartcolors";

export const UserStoriesChartFunction = (
  UserStoriesData: any,
  Project: any
) => {
  if (
    UserStoriesData !== undefined &&
    UserStoriesData !== null &&
    Project !== null &&
    Project !== undefined
  ) {
    let sortingOrder = [];
    let UserStoriesResults = [];

    Project.forEach((project) => {
      UserStoriesData.forEach((tests) => {
        if (
          project.ProjectReference === tests.ProjectReference &&
          project.ReleaseName === tests.fixVersions
        ) {
          UserStoriesResults.push(tests);
        }
      });
    });
    UserStoriesResults.forEach((sort) => {
      sortingOrder.push(sort.projectName);
    });
    if (sortingOrder.length !== 0) {
      sortingOrder = sortingOrder.filter(
        (value, index) => sortingOrder.indexOf(value) === index
      );
    }
    if (UserStoriesResults !== undefined && UserStoriesResults.length !== 0) {
      let series = [];
      let storyStatus = [];
      for (let i = 0; i < UserStoriesResults.length; i++) {
        let testExecutionData = UserStoriesResults[i];
        if (
          testExecutionData.storyStatus !== null &&
          testExecutionData.storyStatus !== undefined
        ) {
          if (storyStatus.length === 0) {
            storyStatus.push(testExecutionData.storyStatus);
            let data = [];
            let drillobject = {
              name: testExecutionData.projectName,
              y: testExecutionData.ProjectCount,
            };
            data.push(drillobject);
            let seriesObj = {
              name: testExecutionData.storyStatus,
              // pointWidth: 100,
              data: data,
            };
            series.push(seriesObj);
          } else {
            let storyStatusFind = storyStatus.filter((col) => {
              return col === testExecutionData.storyStatus;
            });
            if (storyStatusFind.length === 0) {
              storyStatus.push(testExecutionData.storyStatus);
              let data = [];
              let drillobject = {
                name: testExecutionData.projectName,
                y: testExecutionData.statusCount,
              };
              data.push(drillobject);
              let seriesObj = {
                name: testExecutionData.storyStatus,
                // pointWidth: 100,
                data: data,
              };
              series.push(seriesObj);
            } else {
              let EditSerisObj = series.filter((col) => {
                return col.name === testExecutionData.storyStatus;
              });
              let drillobject = {
                name: testExecutionData.projectName,
                y: testExecutionData.statusCount,
              };
              EditSerisObj[0].data.push(drillobject);
            }
          }
        }
      }
      let Finalseries = [];
      series.forEach((s) => {
        s.data = addSeriesMultilevel(s.data, sortingOrder);
        Finalseries.push(s);
      });
      Finalseries = SeriesOrderForMainFunction(Finalseries);
      return Finalseries;
    } else {
      return [];
    }
  } else {
    return [];
  }
};

/*Sorting Function*/

export function Datastoring(data, sortOrder) {
  for (let b = 0; b < sortOrder.length; b++) {
    let TempItem = data.filter((col) => {
      return col.name === sortOrder[b];
    });
    if (TempItem.length === 0) {
      let tempObj = {
        name: sortOrder[b],
        y: 0,
      };
      data.splice(b, 0, tempObj);
    }
  }
  return data;
}

/*Constructing the order of the Data*/

function SeriesOrderForMainFunction(data) {
  let priorityData = [];
  if (data.length !== 0) {
    UserStories_Main_priority_order.forEach((order) => {
      let TempItem = data.filter((col) => {
        return col.name === order;
      });
      if (TempItem.length !== 0) {
        let Findcolor = UserStories_Main_colors.filter((col) => {
          return col.name === TempItem[0].name;
        });
        if (Findcolor.length !== 0) {
          TempItem[0].color = Findcolor[0].color;
        } else {
          TempItem[0].color = "#DC143C";
        }
        priorityData.push(TempItem[0]);
      }
    });
  }
  return priorityData;
}
