export let mainsubFolderCapture = "";
export let subFolderCapture = "";
export let subFolder2Capture = "";
export let subFolder3Capture = "";
export let subFolder4Capture = "";
export let subFolder5Capture = "";
export let subFolder6Capture = "";
export let subFolder8Capture = "";
export let subFolderCaptureForDefects = "";
export function MainSubFolderSetter(data) {
  mainsubFolderCapture = data;
}
export function SubFolderSetter(data) {
  subFolderCapture = data;
}
export function SubFolder2Setter(data) {
  subFolder2Capture = data;
}
export function SubFolder3Setter(data) {
  subFolder3Capture = data;
}
export function SubFolder4Setter(data) {
  subFolder4Capture = data;
}
export function SubFolder5Setter(data) {
  subFolder5Capture = data;
}
export function SubFolder6Setter(data) {
  subFolder6Capture = data;
}
export function SubFolder8Setter(data) {
  subFolder8Capture = data;
}

export function SubFolderSetterForDefects(data) {
  subFolderCaptureForDefects = data;
}

export let RequirementsubFolderCapture = "";
export let RequirementsubFolder2Capture = "";
export let RequirementsubFolder3Capture = "";
export let RequirementsubFolder4Capture = "";
export let RequirementsubFolder5Capture = "";
export let RequirementsubFolder6Capture = "";
export let RequirementsubFolder8Capture = "";
export function RequirementSubFolderSetter(data) {
  RequirementsubFolderCapture = data;
}
export function RequirementSubFolder2Setter(data) {
  RequirementsubFolder2Capture = data;
}
export function RequirementSubFolder3Setter(data) {
  RequirementsubFolder3Capture = data;
}
export function RequirementSubFolder4Setter(data) {
  RequirementsubFolder4Capture = data;
}
export function RequirementSubFolder5Setter(data) {
  RequirementsubFolder5Capture = data;
}
export function RequirementSubFolder6Setter(data) {
  RequirementsubFolder6Capture = data;
}
export function RequirementSubFolder8Setter(data) {
  RequirementsubFolder8Capture = data;
}
