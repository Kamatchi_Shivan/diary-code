import { DataFormation } from "../common/TestExecutionFolder";
import {
  subFolderCapture,
  subFolder2Capture,
  subFolder3Capture,
  subFolder5Capture,
  subFolder6Capture,
  subFolder8Capture,
  subFolderCaptureForDefects
} from "./TestExecutionFolderSet";
/* Test Defect Filters */
export const MainTestFiltersByDefectLevel = (
  Defects: any,
  summaryDatas: any,
  TestResults: any,
  dataset: any
) => {
  let TestDefects = [];
  if (Defects !== undefined) {
    let MatchingDefects = [];

    MatchingDefects = TestResults.filter((col) => {
      return (
        col.ProjectReference === summaryDatas.ProjectReference &&
        col.ReleaseName === summaryDatas.ReleaseName && 
        col.ProjectName === dataset
      );
    });

    if (MatchingDefects.length !== 0) {
      MatchingDefects.forEach((Md) => {
        if (Md.Defects !== null && Md.Defects !== undefined) {
          if (Md.Defects.includes(",")) {
            let Dset = Md.Defects.split(",");
            TestDefects = TestDefects.concat(Dset);
          } else {
            TestDefects.push(Md.Defects);
          }
        }
      });
       TestDefects = TestDefects.filter(
        (value, index) => TestDefects.indexOf(value) === index
      );
    }
  }
  if (TestDefects.length > 0) {
    return TestDefects;
  } else {
    return [""];
  }
};

/* Folder Defect Filters */

export const TestFiltersByDefectLevel = (
  Defects: any,
  summaryDatas: any,
  TestLevel: any,
  TestResults: any
) => {
  let TestDefects = [];
  if (Defects !== undefined) {
    let MatchingDefects = [];
    MatchingDefects = TestResults.filter((col) => {
      return (
        col.ProjectReference === summaryDatas.ProjectReference &&
        col.ReleaseName === summaryDatas.ReleaseName &&
        // col.Folder.includes(TestLevel)
        col.Folder === TestLevel
      );
    });

    if (MatchingDefects.length !== 0) {
      MatchingDefects.forEach((Md) => {
        if (Md.Defects !== null && Md.Defects !== undefined) {
          if (Md.Defects.includes(",")) {
            let Dset = Md.Defects.split(",");
            TestDefects = TestDefects.concat(Dset);
          } else {
            TestDefects.push(Md.Defects);
          }
        }
      });
       TestDefects = TestDefects.filter(
        (value, index) => TestDefects.indexOf(value) === index
      );
    }
  }
  if (TestDefects.length > 0) {
    return TestDefects;
  } else {
    return [""];
  }
};

/* SubFolder Defect Filters */

export const TestFiltersBySubFolderDefectLevel = (
  Defects: any,
  summaryDatas: any,
  TestLevel: any,
  TestResults: any,
  FolderData: any
) => {
  let TestDefects = [];
  if (Defects !== undefined) {
    let MatchingDefects = [];

    MatchingDefects = TestResults.filter((col) => {
      return (
        col.ProjectReference === summaryDatas.ProjectReference &&
        col.ReleaseName === summaryDatas.ReleaseName &&
        col.Folder === FolderData &&
        col.subFolder === TestLevel
      );
    });

    if (MatchingDefects.length !== 0) {
      MatchingDefects.forEach((Md) => {
        if (Md.Defects !== null && Md.Defects !== undefined) {
          if (Md.Defects.includes(",")) {
            let Dset = Md.Defects.split(",");
            TestDefects = TestDefects.concat(Dset);
          } else {
            TestDefects.push(Md.Defects);
          }
        }
      });
        TestDefects = TestDefects.filter(
        (value, index) => TestDefects.indexOf(value) === index
      );
    }
  }
  if (TestDefects.length > 0) {
    return TestDefects;
  } else {
    return [""];
  }
};

/* Sub Folder 1*/

export const TestFiltersBySubFolder1DefectLevel = (
  Defects: any,
  summaryDatas: any,
  TestLevel: any,
  TestResults: any,
  FolderData: any
) => {
  let TestDefects = [];
  if (Defects !== undefined) {
    let MatchingDefects = [];

    MatchingDefects = TestResults.filter((col) => {
      return (
        col.ProjectReference === summaryDatas.ProjectReference &&
        col.ReleaseName === summaryDatas.ReleaseName &&
        col.Folder === FolderData &&
        col.subFolder1 === TestLevel
      );
    });

    if (MatchingDefects.length !== 0) {
      MatchingDefects.forEach((Md) => {
        if (Md.Defects !== null && Md.Defects !== undefined) {
          if (Md.Defects.includes(",")) {
            let Dset = Md.Defects.split(",");
            TestDefects = TestDefects.concat(Dset);
          } else {
            TestDefects.push(Md.Defects);
          }
        }
      });
       TestDefects = TestDefects.filter(
        (value, index) => TestDefects.indexOf(value) === index
      );
    }
  }
  if (TestDefects.length > 0) {
    return TestDefects;
  } else {
    return [""];
  }
};

/* Dynamic Sub Folder 1*/

export const TestFiltersByDynamicSubFolderDefectLevel = (
  Defects: any,
  summaryDatas: any,
  TestLevel: any,
  TestResults: any,
  FolderData: any,
  count: any,
) => {
  let TestDefects = [];
  if (Defects !== undefined) {
    let MatchingDefects = [];
    let folderdata = DataFormation(count);
    MatchingDefects = TestResults.filter((col) => {
      return (
        col.ProjectReference === summaryDatas.ProjectReference &&
        col.ReleaseName === summaryDatas.ReleaseName &&
        col.Folder === FolderData &&
        col[folderdata.folder1] === TestLevel &&
        // col[folderdata.folder2] !== null &&
        // col[folderdata.folder2] !== "" &&
        col.Defects !== "" &&
        col.Defects !== null
      );
    });
    MatchingDefects = constructingDynamicData(count, MatchingDefects);
    if (MatchingDefects.length !== 0) {
      MatchingDefects.forEach((Md) => {
        if (Md.Defects !== null && Md.Defects !== undefined) {
          if (Md.Defects.includes(",")) {
            let Dset = Md.Defects.split(",");
            TestDefects = TestDefects.concat(Dset);
          } else {
            TestDefects.push(Md.Defects);
          }
        }
      });
       TestDefects = TestDefects.filter(
        (value, index) => TestDefects.indexOf(value) === index
      );
    }
  }
  if (TestDefects.length > 0) {
    return TestDefects;
  } else {
    return [""];
  }
};

/* Dynamic Data filter */

function constructingDynamicData(count, TestResults) {
  let testData = TestResults;
  if (count === 4) {
    testData = TestResults.filter((col) => {
      return col.subFolder === subFolderCapture;
    });
  } else if (count > 4 && count <= 6) {
    testData = TestResults.filter((col) => {
      return (
        col.subFolder === subFolderCapture &&
        col.subFolder1 === subFolder2Capture &&
        col.subFolder2 === subFolderCaptureForDefects
      );
    });
  } else if (count > 6 && count <= 7) {
    testData = TestResults.filter((col) => {
      return (
        col.subFolder === subFolderCapture &&
        col.subFolder1 === subFolder2Capture &&
        col.subFolder3 === subFolder3Capture
      );
    });
  } else if (count > 7 && count >= 8) {
    testData = TestResults.filter((col) => {
      return (
        col.subFolder === subFolderCapture &&
        col.subFolder1 === subFolder2Capture &&
        col.subFolder3 === subFolder3Capture &&
        col.subFolder4 === subFolder5Capture
      );
    });
  } else if (count >= 9 && count <= 10) {
    testData = TestResults.filter((col) => {
      return (
        col.subFolder === subFolderCapture &&
        col.subFolder1 === subFolder2Capture &&
        col.subFolder3 === subFolder3Capture &&
        col.subFolder4 === subFolder5Capture &&
        col.subFolder6 === subFolder6Capture
      );
    });
  } else if (count >= 11) {
    testData = TestResults.filter((col) => {
      return (
        col.subFolder === subFolderCapture &&
        col.subFolder1 === subFolder2Capture &&
        col.subFolder3 === subFolder3Capture &&
        col.subFolder4 === subFolder5Capture &&
        col.subFolder6 === subFolder6Capture &&
        col.subFolder8 === subFolder8Capture
      );
    });
  }
  return testData;
}
