import { config } from "../../config/configs";

export const ConstructingUserStoryTableView = (data: any) => {
  let columns = [];
  let rows = [];  
  if(data.length !== 0){
      data.forEach(story => {
        story.data.forEach(tab =>{
            if(tab.y !== 0){
                columns.push(tab.name);
                rows.push(tab.y)
            }
        });
      });
  }
  return {
      columns : columns,
      rows : rows
  };
}

export const ConstructingFirstLevelUserStoryTableView = (projects:any,data: any,type:any) => {
  console.log(data)
  let columns = [];
  let rows = [];  
  if(data.length !== 0){
    if(type !== config.epics){
      data.forEach(story => {
        if(columns.length === 0){
          columns.push('Project_Name')
        }
        columns.push(story.name);
      });
      projects.forEach(project => {
        let dataArray = [];
        let dataObj ={};
        dataArray.push(project);
        data.forEach(story => {
          story.data.forEach(tab =>{
            if(tab.name === project){
              dataArray.push(tab.y)
            }
        });
        });
        dataObj = {
          dataArray
        }
        rows.push(dataObj);

      })

    }else{
      data.forEach(story => {
        if(columns.length === 0){
          columns.push('Epic_Name')
        }
        columns.push(story.name);
      });
      projects.forEach(project => {
        let dataArray = [];
        let dataObj ={};
        dataArray.push(project);
        data.forEach(story => {
          story.data.forEach(tab =>{
            if(tab.name === project){
              dataArray.push(tab.y)
            }
        });
        });
        dataObj = {
          dataArray
        }
        rows.push(dataObj);

      })

    }

  }
  return {
      columns : columns,
      rows : rows
  };
}