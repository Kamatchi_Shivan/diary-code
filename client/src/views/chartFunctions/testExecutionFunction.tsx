import {
  TestFiltersByDefectLevel,
  TestFiltersByDynamicSubFolderDefectLevel,
  MainTestFiltersByDefectLevel
} from "./testTypeDefectFilters";
import {
  addSeriesMultilevel,
  SeriesOrder,
} from "../common/TestExecutionCommonFunctions";
import {
  TestCase_MVP_Priority,
  TestCase_NOTMVP_Priority,
} from "../common/Chartcolors";
import { DataFormation } from "../common/TestExecutionFolder";
import { config } from "../../config/configs";
import {
  mainsubFolderCapture,
  subFolderCapture,
  subFolder2Capture,
  subFolder3Capture,
  subFolder5Capture,
  subFolder6Capture,
  subFolder8Capture,
} from "./TestExecutionFolderSet";

/*Static Filter Validation*/

export const FilterValidation = (
  TestResults: any,
  Project: any
) => {

  let testData = TestResults.filter((col) => {
    return (
      col.ProjectReference === Project.ProjectReference &&
      col.ReleaseName === Project.ReleaseName
    );
  });
 if(testData.length !== 0){
  let StaticFilter = testData.filter((col) => {
    return (
      col.hasOwnProperty("usergroup") &&
      col['usergroup'] !== null
    );
  });
  if(StaticFilter.length !== 0){
    return true;
  }else{
    return false;
  }
 }else{
  return false;
 }
}

/*Static Filter Validation*/

export const FilterValidationForBussinessDomain = (
  TestResults: any,
  Project: any
) => {

  let testData = TestResults.filter((col) => {
    return (
      col.ProjectReference === Project.ProjectReference &&
      col.ReleaseName === Project.ReleaseName
    );
  });
 if(testData.length !== 0){
  let StaticFilter = testData.filter((col) => {
    return (
      col.hasOwnProperty("businessDomain") &&
      col['businessDomain'] !== null
    );
  });
  if(StaticFilter.length !== 0){
    return true;
  }else{
    return false;
  }
 }else{
  return false;
 }
}

/* Main Function Calling all the sub functions */

export const getChartData = (
  dataset: any,
  testResults: any,
  project: any,
  count: any,
  Defects: any,
  FolderData: any,
  priorityType: any,
  component: any,
  staticFilter: any,
) => (dispatch: any, option: any) => {
  if (dataset === "projects" && count === 1) {
    let TestData = TestExecutionStatus(
      testResults,
      project,
      priorityType,
      component,
      staticFilter
    );
    let DefectData = [];
    let FinalObj = {
      DefectData: DefectData,
      TestData: TestData,
    };
    return FinalObj;
  } else if (count === 1 && dataset !== "projects") {
    let DefectData = MainTestFiltersByDefectLevel(Defects,
      project, testResults, mainsubFolderCapture,);;
    let TestData = TestExecutionStatussecondLevel(
      testResults,
      project,
      priorityType,
      component,
      staticFilter
      // dataset
    );
    if (dataset.includes("Comarch")) {
      let FinalObj = {
        DefectData: DefectData,
        TestData: 0,
      };
      return FinalObj;
    } else {
      let FinalObj = {
        DefectData: DefectData,
        TestData: TestData,
      };
      return FinalObj;
    }

  } else if (count === 2) {
    let DefectData = TestFiltersByDefectLevel(
      Defects,
      project,
      dataset,
      testResults
    );
    let TestData = TestExecutionStatusThirdLevel(
      testResults,
      project,
      dataset,
      priorityType,
      component,
      staticFilter
    );
    let FinalObj = {
      DefectData: DefectData,
      TestData: TestData,
    };
    return FinalObj;
  } else if (count >= 3 && count <= 12) {
    let DefectData = TestFiltersByDynamicSubFolderDefectLevel(
      Defects,
      project,
      dataset,
      testResults,
      FolderData,
      count
    );
    let TestData = TestExecutionStatusDynamicLevels(
      testResults,
      project,
      dataset,
      FolderData,
      priorityType,
      component,
      count,
      staticFilter

    );
    let FinalObj = {
      DefectData: DefectData,
      TestData: TestData,
    };
    return FinalObj;
  } else if (count > 12) {
    let FinalObj = {
      DefectData: TestFiltersByDynamicSubFolderDefectLevel(
        Defects,
        project,
        dataset,
        testResults,
        FolderData,
        count,
      ),
      TestData: 0,
    };
    return FinalObj;
  }
};

/*Level 1 Data */

export const TestExecutionStatus = (
  TestResults: any,
  Project: any,
  priorityType: any,
  component: any,
  staticFilter: any
) => {
  if (
    TestResults !== undefined &&
    TestResults !== null &&
    Project !== null &&
    Project !== undefined
  ) {
    let sortingOrder = [];
    let testData = TestResults.filter((col) => {
      return (
        col.ProjectReference === Project.ProjectReference &&
        col.ReleaseName === Project.ReleaseName
      );
    });
    if(staticFilter !== config.all){
      testData = StaticFilterValidation(staticFilter,testData);
    }
    testData = ConstructingDataBasedOnType(testData, priorityType);
    // testData = ConstructingDataBasedOnComponent(testData, component);
    testData.forEach((sort) => {
      sortingOrder.push(sort.ProjectName);
    });
    if (sortingOrder.length !== 0) {
      sortingOrder = sortingOrder.filter(
        (value, index) => sortingOrder.indexOf(value) === index
      );
    }
    if (testData !== undefined && testData.length !== 0) {
      let series = [];
      let testStatus = [];
      for (let i = 0; i < testData.length; i++) {
        let testExecutionData = testData[i];
        if (
          testExecutionData.testStatus !== null &&
          testExecutionData.testStatus !== undefined
        ) {
          if (testStatus.length === 0) {
            testStatus.push(testExecutionData.testStatus);
            let data = [];
            let drillobject = {
              name: testExecutionData.ProjectName,
              y: testExecutionData.count,
            };
            data.push(drillobject);
            let seriesObj = {
              name: testExecutionData.testStatus,
              // pointWidth: 100,
              data: data,
            };

            series.push(seriesObj);
          } else {
            let testStatusFind = testStatus.filter((col) => {
              return col === testExecutionData.testStatus;
            });
            if (testStatusFind.length === 0) {
              testStatus.push(testExecutionData.testStatus);
              let data = [];
              let drillobject = {
                name: testExecutionData.ProjectName,
                y: testExecutionData.count,
              };
              data.push(drillobject);
              let seriesObj = {
                name: testExecutionData.testStatus,
                // pointWidth: 100,
                data: data,
              };

              series.push(seriesObj);
            } else {
              let EditSerisObj = series.filter((col) => {
                return col.name === testExecutionData.testStatus;
              });
              if (EditSerisObj.length !== 0) {
                let drillobject = {
                  name: testExecutionData.ProjectName,
                  y: testExecutionData.count,
                };
                EditSerisObj[0].data.push(drillobject);
              }
            }
          }
        }
      }
      let Finalseries = [];
      series.forEach((s) => {
        s.data = addSeriesMultilevel(s.data, sortingOrder);
        Finalseries.push(s);
      });
      Finalseries = SeriesOrder(Finalseries);
      return Finalseries;
    } else {
      return [];
    }
  } else {
    return [];
  }
};

/*Level 2 Data */

export const TestExecutionStatussecondLevel = (
  TestResults: any,
  Project: any,
  priorityType: any,
  component: any,
  // dataset:any
  staticFilter: any
) => {
  if (
    TestResults !== undefined &&
    TestResults !== null &&
    Project !== null &&
    Project !== undefined
  ) {
    let sortingOrder = [];
    let testData = TestResults.filter((col) => {
      return (
        col.ProjectReference === Project.ProjectReference &&
        col.ReleaseName === Project.ReleaseName
        // &&
        // col.ProjectName === dataset
      );
    });
    if(staticFilter !== config.all){
      testData = StaticFilterValidation(staticFilter,testData);
    }
    testData = ConstructingDataBasedOnType(testData, priorityType);
    // testData = ConstructingDataBasedOnComponent(testData, component);
    testData.forEach((sort) => {
      if (
        sort.Folder !== null &&
        sort.Folder !== undefined &&
        sort.Folder !== ""
      ) {
        sortingOrder.push(sort.Folder);
      }
    });
    if (sortingOrder.length !== 0) {
      sortingOrder = sortingOrder.filter(
        (value, index) => sortingOrder.indexOf(value) === index
      );
    }
    if (testData !== undefined && testData.length !== 0) {
      let series = [];
      let testStatus = [];
      for (let i = 0; i < testData.length; i++) {
        let testExecutionData = testData[i];
        if (
          testExecutionData.testStatus !== null &&
          testExecutionData.testStatus !== undefined
        ) {
          if (testStatus.length === 0) {
            let data = [];
            if (
              testExecutionData.Folder !== null &&
              testExecutionData.Folder !== ""
            ) {
              testStatus.push(testExecutionData.testStatus);
              let drillobject = {
                name: testExecutionData.Folder,
                y: testExecutionData.count,
              };
              data.push(drillobject);
              let seriesObj = {
                name: testExecutionData.testStatus,
                // pointWidth: 20,
                data: data,
              };
              series.push(seriesObj);
            }
          } else {
            let testStatusFind = testStatus.filter((col) => {
              return col === testExecutionData.testStatus;
            });
            if (testStatusFind.length === 0) {
              let data = [];
              if (
                testExecutionData.Folder !== null &&
                testExecutionData.Folder !== ""
              ) {
                testStatus.push(testExecutionData.testStatus);
                let drillobject = {
                  name: testExecutionData.Folder,
                  y: testExecutionData.count,
                };
                data.push(drillobject);
                let seriesObj = {
                  name: testExecutionData.testStatus,
                  // pointWidth: 20,
                  data: data,
                };
                series.push(seriesObj);
              }
            } else {
              let EditSerisObj = series.filter((col) => {
                return col.name === testExecutionData.testStatus;
              });
              if (EditSerisObj.length !== 0) {
                if (
                  testExecutionData.Folder !== null &&
                  testExecutionData.Folder !== ""
                ) {
                  let drillobject = {
                    name: testExecutionData.Folder,
                    y: testExecutionData.count,
                  };
                  EditSerisObj[0].data.push(drillobject);
                }
              }
            }
          }
        }
      }
      let Finalseries = [];
      series.forEach((s) => {
        s.data = addSeriesMultilevel(s.data, sortingOrder);
        Finalseries.push(s);
      });
      Finalseries = SeriesOrder(Finalseries);
      if (Finalseries.length !== 0) {
        return Finalseries;
      } else {
        return [];
      }
    } else {
      return [];
    }
  } else {
    return [];
  }
};

/*Level 3 Data */

export const TestExecutionStatusThirdLevel = (
  TestResults: any,
  Project: any,
  Folder: any,
  priorityType: any,
  component: any,
  staticFilter: any
) => {
  if (
    TestResults !== undefined &&
    TestResults !== null &&
    Project !== null &&
    Project !== undefined
  ) {
    let sortingOrder = [];
    let testData = TestResults.filter((col) => {
      return (
        col.ProjectReference === Project.ProjectReference &&
        col.ReleaseName === Project.ReleaseName &&
        col.Folder !== null
        // &&
        // col.Folder.includes(Folder)
        // col.Folder === Folder

      );
    });
     if(staticFilter !== config.all){
      testData = StaticFilterValidation(staticFilter,testData);
    }
     testData = testData.filter((col) => {
      return (
        col.Folder.includes(Folder) ||
        col.Folder === Folder

      );
    });
    testData = ConstructingDataBasedOnType(testData, priorityType);
    // testData = ConstructingDataBasedOnComponent(testData, component);
    testData.forEach((sort) => {
      if (
        sort.subFolder !== null &&
        sort.subFolder !== undefined &&
        sort.subFolder !== ""
      ) {
        sortingOrder.push(sort.subFolder);
      }
    });
    if (sortingOrder.length !== 0) {
      sortingOrder = sortingOrder.filter(
        (value, index) => sortingOrder.indexOf(value) === index
      );
    }
    if (testData !== undefined && testData.length !== 0) {
      let series = [];
      let testStatus = [];
      for (let i = 0; i < testData.length; i++) {
        let testExecutionData = testData[i];
        if (
          testExecutionData.testStatus !== null &&
          testExecutionData.testStatus !== undefined
        ) {
          if (testStatus.length === 0) {
            let data = [];
            if (
              testExecutionData.subFolder !== null &&
              testExecutionData.subFolder !== ""
            ) {
              testStatus.push(testExecutionData.testStatus);
              let drillobject = {
                name: testExecutionData.subFolder,
                y: testExecutionData.count,
              };
              data.push(drillobject);
              let seriesObj = {
                name: testExecutionData.testStatus,
                // pointWidth: 20,
                data: data,
              };
              series.push(seriesObj);
            }
          } else {
            let testStatusFind = testStatus.filter((col) => {
              return col === testExecutionData.testStatus;
            });
            if (testStatusFind.length === 0) {
              let data = [];
              if (
                testExecutionData.subFolder !== null &&
                testExecutionData.subFolder !== ""
              ) {
                testStatus.push(testExecutionData.testStatus);
                let drillobject = {
                  name: testExecutionData.subFolder,
                  y: testExecutionData.count,
                };
                data.push(drillobject);
                let seriesObj = {
                  name: testExecutionData.testStatus,
                  // pointWidth: 20,
                  data: data,
                };
                series.push(seriesObj);
              }
            } else {
              let EditSerisObj = series.filter((col) => {
                return col.name === testExecutionData.testStatus;
              });
              if (EditSerisObj.length !== 0) {
                if (
                  testExecutionData.subFolder !== null &&
                  testExecutionData.subFolder !== ""
                ) {
                  let drillobject = {
                    name: testExecutionData.subFolder,
                    y: testExecutionData.count,
                  };
                  EditSerisObj[0].data.push(drillobject);
                }
              }
            }
          }
        } else {
          return [];
        }
      }
      let Finalseries = [];
      series.forEach((s) => {
        s.data = addSeriesMultilevel(s.data, sortingOrder);
        Finalseries.push(s);
      });
      Finalseries = SeriesOrder(Finalseries);
      if (Finalseries.length !== 0) {
        return Finalseries;
      } else {
        return 0;
      }
    } else {
      return 0;
    }
  } else {
    return 0;
  }
};

/* Level 4 */

export const TestExecutionStatusForthLevel = (
  TestResults: any,
  Project: any,
  Folder: any,
  ManiFolderData: any,
  priorityType: any,
  component: any
) => {
  if (
    TestResults !== undefined &&
    TestResults !== null &&
    Project !== null &&
    Project !== undefined
  ) {
    let sortingOrder = [];
    let testData = TestResults.filter((col) => {
      return (
        col.ProjectReference === Project.ProjectReference &&
        col.ReleaseName === Project.ReleaseName &&
        col.Folder === ManiFolderData &&
        col.subFolder === Folder
      );
    });
    testData = ConstructingDataBasedOnType(testData, priorityType);
    // testData = ConstructingDataBasedOnComponent(testData, component);
    testData.forEach((sort) => {
      if (
        sort.subFolder1 !== null &&
        sort.subFolder1 !== undefined &&
        sort.subFolder1 !== ""
      ) {
        sortingOrder.push(sort.subFolder1);
      }
    });

    if (sortingOrder.length !== 0) {
      sortingOrder = sortingOrder.filter(
        (value, index) => sortingOrder.indexOf(value) === index
      );
    }
    if (testData !== undefined && testData.length !== 0) {
      let series = [];
      let testStatus = [];
      for (let i = 0; i < testData.length; i++) {
        let testExecutionData = testData[i];
        if (
          testExecutionData.testStatus !== null &&
          testExecutionData.testStatus !== undefined
        ) {
          if (testStatus.length === 0) {
            let data = [];
            if (
              testExecutionData.subFolder1 !== null &&
              testExecutionData.subFolder1 !== ""
            ) {
              testStatus.push(testExecutionData.testStatus);
              let drillobject = {
                name: testExecutionData.subFolder1,
                y: testExecutionData.count,
              };
              data.push(drillobject);
              let seriesObj = {
                name: testExecutionData.testStatus,
                // pointWidth: 10,
                data: data,
              };
              series.push(seriesObj);
            }
          } else {
            let testStatusFind = testStatus.filter((col) => {
              return col === testExecutionData.testStatus;
            });
            if (testStatusFind.length === 0) {
              let data = [];
              if (
                testExecutionData.subFolder1 !== null &&
                testExecutionData.subFolder1 !== ""
              ) {
                testStatus.push(testExecutionData.testStatus);
                let drillobject = {
                  name: testExecutionData.subFolder1,
                  y: testExecutionData.count,
                };
                data.push(drillobject);
                let seriesObj = {
                  name: testExecutionData.testStatus,
                  // pointWidth: 10,
                  data: data,
                };
                series.push(seriesObj);
              }
            } else {
              let EditSerisObj = series.filter((col) => {
                return col.name === testExecutionData.testStatus;
              });
              if (EditSerisObj.length !== 0) {
                if (
                  testExecutionData.subFolder1 !== null &&
                  testExecutionData.subFolder1 !== ""
                ) {
                  let drillobject = {
                    name: testExecutionData.subFolder1,
                    y: testExecutionData.count,
                  };
                  EditSerisObj[0].data.push(drillobject);
                }
              }
            }
          }
        } else {
          return [];
        }
      }
      let Finalseries = [];
      series.forEach((s) => {
        s.data = addSeriesMultilevel(s.data, sortingOrder);
        Finalseries.push(s);
      });
      Finalseries = SeriesOrder(Finalseries);
      if (Finalseries.length !== 0) {
        return Finalseries;
      } else {
        return 0;
      }
    } else {
      return 0;
    }
  } else {
    return 0;
  }
};

/* Test Execution Status above 4th level */

export const TestExecutionStatusDynamicLevels = (
  TestResults: any,
  Project: any,
  Folder: any,
  ManiFolderData: any,
  priorityType: any,
  component: any,
  count: any,
  staticFilter: any
) => {
  if (
    TestResults !== undefined &&
    TestResults !== null &&
    Project !== null &&
    Project !== undefined
  ) {
    let sortingOrder = [];
    let folderdata = DataFormation(count);
    let testData = TestResults.filter((col) => {
      return (
        col.ProjectReference === Project.ProjectReference &&
        col.ReleaseName === Project.ReleaseName &&
        col.Folder === ManiFolderData &&
        col[folderdata.folder1] === Folder &&
        col[folderdata.folder2] !== null &&
        col[folderdata.folder2] !== ""
      );
    });
    if(staticFilter !== config.all){
      testData = StaticFilterValidation(staticFilter,testData);
    }
    testData = constructingDynamicData(count, testData);
    testData = ConstructingDataBasedOnType(testData, priorityType);
    // testData = ConstructingDataBasedOnComponent(testData, component);
    testData.forEach((sort) => {
      if (
        sort[folderdata.folder2] !== null &&
        sort[folderdata.folder2] !== undefined &&
        sort[folderdata.folder2] !== ""
      ) {
        sortingOrder.push(sort[folderdata.folder2]);
      }
    });
    if (sortingOrder.length !== 0) {
      sortingOrder = sortingOrder.filter(
        (value, index) => sortingOrder.indexOf(value) === index
      );
    }
    if (testData !== undefined && testData.length !== 0) {
      let series = [];
      let testStatus = [];
      for (let i = 0; i < testData.length; i++) {
        let testExecutionData = testData[i];
        if (
          testExecutionData.testStatus !== null &&
          testExecutionData.testStatus !== undefined
        ) {
          if (testStatus.length === 0) {
            let data = [];
            if (
              testExecutionData[folderdata.folder2] !== null &&
              testExecutionData[folderdata.folder2] !== ""
            ) {
              testStatus.push(testExecutionData.testStatus);
              let drillobject = {
                name: testExecutionData[folderdata.folder2],
                y: testExecutionData.count,
              };
              data.push(drillobject);
              let seriesObj = {
                name: testExecutionData.testStatus,
                // pointWidth: 10,
                data: data,
              };
              series.push(seriesObj);
            }
          } else {
            let testStatusFind = testStatus.filter((col) => {
              return col === testExecutionData.testStatus;
            });
            if (testStatusFind.length === 0) {
              let data = [];
              if (
                testExecutionData[folderdata.folder2] !== null &&
                testExecutionData[folderdata.folder2] !== ""
              ) {
                testStatus.push(testExecutionData.testStatus);
                let drillobject = {
                  name: testExecutionData[folderdata.folder2],
                  y: testExecutionData.count,
                };
                data.push(drillobject);
                let seriesObj = {
                  name: testExecutionData.testStatus,
                  // pointWidth: 10,
                  data: data,
                };
                series.push(seriesObj);
              }
            } else {
              let EditSerisObj = series.filter((col) => {
                return col.name === testExecutionData.testStatus;
              });
              if (EditSerisObj.length !== 0) {
                if (
                  testExecutionData[folderdata.folder2] !== null &&
                  testExecutionData[folderdata.folder2] !== ""
                ) {
                  let drillobject = {
                    name: testExecutionData[folderdata.folder2],
                    y: testExecutionData.count,
                  };
                  EditSerisObj[0].data.push(drillobject);
                }
              }
            }
          }
        } else {
          return [];
        }
      }
      let Finalseries = [];
      series.forEach((s) => {
        s.data = addSeriesMultilevel(s.data, sortingOrder);
        Finalseries.push(s);
      });
      Finalseries = SeriesOrder(Finalseries);
      if (Finalseries.length !== 0) {
        return Finalseries;
      } else {
        return 0;
      }
    } else {
      return 0;
    }
  } else {
    return 0;
  }
};

/*Dynamic Level 4 - Data Fetch */

export const DynamicTestExecutionStatusForthLevel = (
  TestResults: any,
  Project: any,
  Folder: any,
  subFolder: any,
  priorityType: any,
  epicData: any,
  staticFilter: any
) => {
  if (
    TestResults !== undefined &&
    TestResults !== null &&
    Project !== null &&
    Project !== undefined
  ) {
    let sortingOrder = [];
    let testData = [];
    TestResults.forEach((col) => {
      if (col.Folder !== null && col.subFolder !== null) {
        if (
          col.ProjectReference === Project.ProjectReference &&
          col.ReleaseName === Project.ReleaseName &&
          col.Folder.toLowerCase().includes(Folder.toLowerCase()) &&
          col.subFolder.toLowerCase().includes(subFolder.toLowerCase())
        ) {
          testData.push(col);
        }
      }
    });
    console.log(testData)
    if(staticFilter !== config.all){
      testData = StaticFilterValidation(staticFilter,testData);
    }
    testData = ConstructingDataBasedOnType(testData, priorityType);
    testData.forEach((sort) => {
      if (
        sort.subFolder1 !== null &&
        sort.subFolder1 !== undefined &&
        sort.subFolder1 !== ""
      ) {
        sortingOrder.push(sort.subFolder1);
      }
    });

    if (sortingOrder.length !== 0) {
      sortingOrder = sortingOrder.filter(
        (value, index) => sortingOrder.indexOf(value) === index
      );
    }
    if (testData !== undefined && testData.length !== 0) {
      let series = [];
      let testStatus = [];
      for (let i = 0; i < testData.length; i++) {
        let testExecutionData = testData[i];
        if (
          testExecutionData.testStatus !== null &&
          testExecutionData.testStatus !== undefined
        ) {
          if (testStatus.length === 0) {
            testStatus.push(testExecutionData.testStatus);
            let data = [];
            if (
              testExecutionData.subFolder1 !== null &&
              testExecutionData.subFolder1 !== ""
            ) {
              let drillobject = {
                name: testExecutionData.subFolder1,
                y: testExecutionData.count,
              };
              data.push(drillobject);
              let seriesObj = {
                name: testExecutionData.testStatus,
                // pointWidth: 20,
                data: data,
              };
              series.push(seriesObj);
            }
          } else {
            let testStatusFind = testStatus.filter((col) => {
              return col === testExecutionData.testStatus;
            });
            if (testStatusFind.length === 0) {
              testStatus.push(testExecutionData.testStatus);
              let data = [];
              if (
                testExecutionData.subFolder1 !== null &&
                testExecutionData.subFolder1 !== ""
              ) {
                let drillobject = {
                  name: testExecutionData.subFolder1,
                  y: testExecutionData.count,
                };
                data.push(drillobject);
                let seriesObj = {
                  name: testExecutionData.testStatus,
                  // pointWidth: 20,
                  data: data,
                };
                series.push(seriesObj);
              }
            } else {
              let EditSerisObj = series.filter((col) => {
                return col.name === testExecutionData.testStatus;
              });
              if (EditSerisObj.length !== 0) {
                if (
                  testExecutionData.subFolder1 !== null &&
                  testExecutionData.subFolder1 !== ""
                ) {
                  let drillobject = {
                    name: testExecutionData.subFolder1,
                    y: testExecutionData.count,
                  };
                  EditSerisObj[0].data.push(drillobject);
                }
              }
            }
          }
        } else {
          return [];
        }
      }
      let Finalseries = [];
      series.forEach((s) => {
        s.data = addSeriesMultilevel(s.data, sortingOrder);
        Finalseries.push(s);
      });
      Finalseries = SeriesOrder(Finalseries);
      if (Finalseries.length !== 0) {
        return Finalseries;
      } else {
        return [];
      }
    } else {
      return [];
    }
  } else {
    return [];
  }
};

/*Dynamic Level 4 - Data Fetch */

export const DynamicTestExecutionStatus = (
  TestResults: any,
  Project: any,
  Folder: any,
  //subFolder: any,
  priorityType: any,
  epicData: any,
  staticFilter: any
) => {
  if (
    TestResults !== undefined &&
    TestResults !== null &&
    Project !== null &&
    Project !== undefined
  ) {
    let sortingOrder = [];
    let testData = [];
    TestResults.forEach((col) => {
      if (col.Folder !== null && col.subFolder !== null) {
        if (
          col.ProjectReference === Project.ProjectReference &&
          col.ReleaseName === Project.ReleaseName &&
          col.Folder.toLowerCase().includes(Folder)
          //col.subFolder.includes(subFolder)
        ) {
          testData.push(col);
        }
      }
    });
    if(staticFilter !== config.all){
      testData = StaticFilterValidation(staticFilter,testData);
    }
    testData = ConstructingDataBasedOnType(testData, priorityType);
    // testData = ConstructingDataBasedOnComponent(testData, epicData);
    testData.forEach((sort) => {
      if (
        sort.Folder !== null &&
        sort.Folder !== undefined &&
        sort.Folder !== ""
      ) {
        sortingOrder.push(sort.subFolder);
      }
    });

    if (sortingOrder.length !== 0) {
      sortingOrder = sortingOrder.filter(
        (value, index) => sortingOrder.indexOf(value) === index
      );
    }
    if (testData !== undefined && testData.length !== 0) {
      let series = [];
      let testStatus = [];
      for (let i = 0; i < testData.length; i++) {
        let testExecutionData = testData[i];
        if (
          testExecutionData.testStatus !== null &&
          testExecutionData.testStatus !== undefined
        ) {
          if (testStatus.length === 0) {
            testStatus.push(testExecutionData.testStatus);
            let data = [];
            if (
              testExecutionData.subFolder !== null &&
              testExecutionData.subFolder !== ""
            ) {
              let drillobject = {
                name: testExecutionData.subFolder,
                y: testExecutionData.count,
              };
              data.push(drillobject);
              let seriesObj = {
                name: testExecutionData.testStatus,
                // pointWidth: 20,
                data: data,
              };
              series.push(seriesObj);
            }
          } else {
            let testStatusFind = testStatus.filter((col) => {
              return col === testExecutionData.testStatus;
            });
            if (testStatusFind.length === 0) {
              testStatus.push(testExecutionData.testStatus);
              let data = [];
              if (
                testExecutionData.subFolder !== null &&
                testExecutionData.subFolder !== ""
              ) {
                let drillobject = {
                  name: testExecutionData.subFolder,
                  y: testExecutionData.count,
                };
                data.push(drillobject);
                let seriesObj = {
                  name: testExecutionData.testStatus,
                  // pointWidth: 20,
                  data: data,
                };
                series.push(seriesObj);
              }
            } else {
              let EditSerisObj = series.filter((col) => {
                return col.name === testExecutionData.testStatus;
              });
              if (EditSerisObj.length !== 0) {
                if (
                  testExecutionData.subFolder !== null &&
                  testExecutionData.subFolder !== ""
                ) {
                  let drillobject = {
                    name: testExecutionData.subFolder,
                    y: testExecutionData.count,
                  };
                  EditSerisObj[0].data.push(drillobject);
                }
              }
            }
          }
        } else {
          return [];
        }
      }
      let Finalseries = [];
      series.forEach((s) => {
        s.data = addSeriesMultilevel(s.data, sortingOrder);
        Finalseries.push(s);
      });
      Finalseries = SeriesOrder(Finalseries);
      if (Finalseries.length !== 0) {
        return Finalseries;
      } else {
        return [];
      }
    } else {
      return [];
    }
  } else {
    return [];
  }
};


/* Based on Type */

function ConstructingDataBasedOnType(data, type) {
  let priorityData = [];
  if (data.length !== 0) {
    if (type === config.alltests) {
      priorityData = data;
    } else if (type === config.mvp) {
      TestCase_MVP_Priority.forEach((order) => {
        let TempItem = data.filter((col) => {
          return col.priority === order;
        });
        if (TempItem.length !== 0) {
          priorityData = priorityData.concat(TempItem);
        }
      });
    } else {
      TestCase_NOTMVP_Priority.forEach((order) => {
        let TempItem = data.filter((col) => {
          return col.priority === order;
        });
        if (TempItem.length !== 0) {
          priorityData = priorityData.concat(TempItem);
        }
      });
    }
  }
  return priorityData;
}

// /* Based on Component */

// function ConstructingDataBasedOnComponent(data, Component) {
//   let priorityData = [];
//   if (data.length !== 0) {
//     if (
//       Component.epicLink === config.allEpic
//     ) {
//       priorityData = data;
//     } else {
//       let ComponentData = data.filter((col) => {
//         return col.epicLink !== null;
//       });
//       if (ComponentData.length !== 0) {
//         priorityData = ComponentData.filter((col) => {
//           return col.epicLink === Component.epicLink;
//         });
//       }
//     }
//   }
//   return priorityData;
// }

/* Dynamic Data filter */

function constructingDynamicData(count, TestResults) {
  let testData = TestResults;
  if (count === 4) {
    testData = TestResults.filter((col) => {
      return col.subFolder === subFolderCapture;
    });
  } else if (count > 4 && count <= 6) {
    testData = TestResults.filter((col) => {
      return (
        col.subFolder === subFolderCapture &&
        col.subFolder1 === subFolder2Capture
      );
    });
  } else if (count > 6 && count <= 7) {
    testData = TestResults.filter((col) => {
      return (
        col.subFolder === subFolderCapture &&
        col.subFolder1 === subFolder2Capture &&
        col.subFolder3 === subFolder3Capture
      );
    });
  } else if (count > 7 && count >= 8) {
    testData = TestResults.filter((col) => {
      return (
        col.subFolder === subFolderCapture &&
        col.subFolder1 === subFolder2Capture &&
        col.subFolder3 === subFolder3Capture &&
        col.subFolder4 === subFolder5Capture
      );
    });
  } else if (count >= 9 && count <= 10) {
    testData = TestResults.filter((col) => {
      return (
        col.subFolder === subFolderCapture &&
        col.subFolder1 === subFolder2Capture &&
        col.subFolder3 === subFolder3Capture &&
        col.subFolder4 === subFolder5Capture &&
        col.subFolder6 === subFolder6Capture
      );
    });
  } else if (count >= 11) {
    testData = TestResults.filter((col) => {
      return (
        col.subFolder === subFolderCapture &&
        col.subFolder1 === subFolder2Capture &&
        col.subFolder3 === subFolder3Capture &&
        col.subFolder4 === subFolder5Capture &&
        col.subFolder6 === subFolder6Capture &&
        col.subFolder8 === subFolder8Capture
      );
    });
  }
  return testData;
}


const StaticFilterValidation = (staticFilter,testData) => {
  if(staticFilter === config.agent || staticFilter === config.broker){
    testData = testData.filter((col) => {
      return (
        col.Hierarchy.includes(staticFilter) &&
        col['usergroup'] !== null
      );
    });
    return testData;
  }else{
    testData = testData.filter((col) => {
      return (
        col.Hierarchy.includes(staticFilter) &&
        col['businessDomain'] !== null
      );
    });
    return testData;
  }

}
