import {
  addSeriesMultilevel
} from "../common/TestExecutionCommonFunctions";
import {
  RequirementData_Main_priority_order,
  RequirementData_Main_colors,
  RequirementResults_priority_order,
  RequirementData_colors
} from "../common/Chartcolors";
import {
  RequirementCoverageDefectFilters,
  // RequirementCoverageDefectFiltersSubTreeNode,
  RequirementsByDynamicSubFolderDefectLevel,
} from "./RequirementDefectsFilter";
import { RequirementDataFormation } from "../common/TestExecutionFolder";
import {
  RequirementsubFolderCapture,
  RequirementsubFolder2Capture,
  RequirementsubFolder3Capture,
  RequirementsubFolder5Capture,
  RequirementsubFolder6Capture,
  RequirementsubFolder8Capture,
} from "./TestExecutionFolderSet";
// import { config } from "../../config/configs";

/*Main Function*/

export const RequirementCoverageMainFunction = (
  RequirementData: any,
  Project: any,
  count: any,
  dataset: any,
  RequirementcaptureDataset: any,
  epicfilter: any
) => {
  if (count === 1 && dataset === "projects") {
    let DefectData = [];
    let RequirementCoverage = RequirementCoverageMainLevel(
      RequirementData,
      Project,
      epicfilter
    );
    let FinalObj = {
      DefectData: DefectData,
      RequirementCoverage: RequirementCoverage,
    };
    return FinalObj;
  } else if (count === 1 && dataset !== "projects") {
    let DefectData = [];
    let RequirementCoverage = RequirementCoverageSecondLevel(
      RequirementData,
      Project,
      dataset,
      epicfilter
    );
    let FinalObj = {
      DefectData: DefectData,
      RequirementCoverage: RequirementCoverage,
    };
    return FinalObj;
  } else if (count === 2) {
    let DefectData = RequirementCoverageDefectFilters(
      Project,
      dataset,
      RequirementData
    );
    let RequirementCoverage = RequirementCoverageThirdLevel(
      RequirementData,
      Project,
      dataset,
      epicfilter
    );
    let FinalObj = {
      DefectData: DefectData,
      RequirementCoverage: RequirementCoverage,
    };
    return FinalObj;
  } else if (count >= 3 && count <= 12) {
    let DefectData = RequirementsByDynamicSubFolderDefectLevel(
      Project,
      dataset,
      RequirementData,
      RequirementcaptureDataset,
      count,
    );
    let RequirementCoverage = RequirementCoverageDynamicLevels(
      RequirementData,
      Project,
      dataset,
      RequirementcaptureDataset,
      count,
      epicfilter
    );
    let FinalObj = {
      DefectData: DefectData,
      RequirementCoverage: RequirementCoverage,
    };
    return FinalObj;
  } else if (count > 12) {
    let FinalObj = {
      DefectData: RequirementsByDynamicSubFolderDefectLevel(
        Project,
        dataset,
        RequirementData,
        RequirementcaptureDataset,
        count
      ),
      RequirementCoverage: 0,
    };
    return FinalObj;
  }
};

/*Level 1 Data */

export const RequirementCoverageMainLevel = (
  RequirementData: any,
  Project: any,
  epicfilter: any
) => {
  if (
    RequirementData !== undefined &&
    RequirementData !== null &&
    Project !== null &&
    Project !== undefined
  ) {
    let sortingOrder = [];
    let RequirementResults = RequirementData.filter((col) => {
      return (
        col.ProjectReference === Project.ProjectReference &&
        col.ReleaseName === Project.ReleaseName
      );
    });
    // RequirementResults = ConstructingDataBasedOnComponent(RequirementResults, epicfilter);
    RequirementResults.forEach((sort) => {
      sortingOrder.push(sort.ProjectName);
    });
    if (sortingOrder.length !== 0) {
      sortingOrder = sortingOrder.filter(
        (value, index) => sortingOrder.indexOf(value) === index
      );
    }
    if (RequirementResults !== undefined && RequirementResults.length !== 0) {
      let series = [];
      let ReqCoverage = [];
      for (let i = 0; i < RequirementResults.length; i++) {
        let testExecutionData = RequirementResults[i];
        if (
          testExecutionData.ReqCoverage !== null &&
          testExecutionData.ReqCoverage !== undefined
        ) {
          if (ReqCoverage.length === 0) {
            ReqCoverage.push(testExecutionData.ReqCoverage);
            let data = [];
            let drillobject = {
              name: testExecutionData.ProjectName,
              y: testExecutionData.count,
            };
            data.push(drillobject);
            let seriesObj = {
              name: testExecutionData.ReqCoverage,
              // pointWidth: 100,
              data: data,
            };
            series.push(seriesObj);
          } else {
            let ReqCoverageFind = ReqCoverage.filter((col) => {
              return col === testExecutionData.ReqCoverage;
            });
            if (ReqCoverageFind.length === 0) {
              ReqCoverage.push(testExecutionData.ReqCoverage);
              let data = [];
              let drillobject = {
                name: testExecutionData.ProjectName,
                y: testExecutionData.count,
              };
              data.push(drillobject);
              let seriesObj = {
                name: testExecutionData.ReqCoverage,
                // pointWidth: 100,
                data: data,
              };
              series.push(seriesObj);
            } else {
              // let index = ReqCoverage.findIndex(
              //   (img) => img === testExecutionData.ReqCoverage
              // );
              let EditSerisObj = series.filter((col) => {
                return col.name === testExecutionData.ReqCoverage;
              });
              let drillobject = {
                name: testExecutionData.ProjectName,
                y: testExecutionData.count,
              };
              EditSerisObj[0].data.push(drillobject);
            }
          }
        }
      }
      let Finalseries = [];
      series.forEach((s) => {
        s.data = addSeriesMultilevel(s.data, sortingOrder);
        Finalseries.push(s);
      });
      Finalseries = SeriesOrderForMainFunction(Finalseries);
      return Finalseries;
    } else {
      return [];
    }
  } else {
    return [];
  }
};

/* Level 2 Data*/

export const RequirementCoverageSecondLevel = (
  RequirementData: any,
  Project: any,
  RequirementStatus: any,
  epicfilter:any
) => {
  if (
    RequirementData !== undefined &&
    RequirementData !== null &&
    Project !== null &&
    Project !== undefined
  ) {
    let sortingOrder = [];
    let requirementcoverageData = RequirementData.filter((col) => {
      return (
        col.ProjectReference === Project.ProjectReference &&
        col.ReleaseName === Project.ReleaseName
        //col.ReqCoverage === RequirementStatus
      );
    });
    // requirementcoverageData = ConstructingDataBasedOnComponent(requirementcoverageData, epicfilter);
    requirementcoverageData.forEach((sort) => {
      sortingOrder.push(sort.RequirementTreeNode);
    });
    if (sortingOrder.length !== 0) {
      sortingOrder = sortingOrder.filter(
        (value, index) => sortingOrder.indexOf(value) === index
      );
      sortingOrder = sortingOrder.sort();
    }

    if (
      requirementcoverageData !== undefined &&
      requirementcoverageData.length !== 0
    ) {
      let series = [];
      let reqCoverageStatus = [];
      for (let i = 0; i < requirementcoverageData.length; i++) {
        let testExecutionData = requirementcoverageData[i];
        if (
          testExecutionData.reqCoverageStatus !== null &&
          testExecutionData.reqCoverageStatus !== undefined
        ) {
          if (reqCoverageStatus.length === 0) {
            reqCoverageStatus.push(testExecutionData.reqCoverageStatus);
            let data = [];
            let drillobject = {
              name: testExecutionData.RequirementTreeNode,
              y: testExecutionData.count,
            };
            data.push(drillobject);
            let seriesObj = {
              name: testExecutionData.reqCoverageStatus,
              // pointWidth: 20,
              data: data,
            };
            series.push(seriesObj);
          } else {
            let reqCoverageStatusFind = reqCoverageStatus.filter((col) => {
              return col === testExecutionData.reqCoverageStatus;
            });
            if (reqCoverageStatusFind.length === 0) {
              reqCoverageStatus.push(testExecutionData.reqCoverageStatus);
              let data = [];
              let drillobject = {
                name: testExecutionData.RequirementTreeNode,
                y: testExecutionData.count,
              };
              data.push(drillobject);
              let seriesObj = {
                name: testExecutionData.reqCoverageStatus,
                // pointWidth: 20,
                data: data,
              };
              series.push(seriesObj);
            } else {
              // let index = reqCoverageStatus.findIndex(
              //   (img) => img === testExecutionData.reqCoverageStatus
              // );
              let EditSerisObj = series.filter((col) => {
                return col.name === testExecutionData.reqCoverageStatus;
              });
              let drillobject = {
                name: testExecutionData.RequirementTreeNode,
                y: testExecutionData.count,
              };
              EditSerisObj[0].data.push(drillobject);
            }
          }
        }
      }
      let Finalseries = [];
      series.forEach((s) => {
        s.data = addSeriesMultilevel(s.data, sortingOrder);
        Finalseries.push(s);
      });
      Finalseries = SeriesOrder(Finalseries);
      // let updatedseries=[];
      // updatedseries = PointwidthCal(Finalseries);
      if (Finalseries.length !== 0) {
        return Finalseries;
      } else {
        return 0;
      }
    } else {
      return 0;
    }
  } else {
    return 0;
  }
};

/* Level 3 Data*/

export const RequirementCoverageThirdLevel = (
  RequirementData: any,
  Project: any,
  Folder: any,
  epicfilter:any
) => {
  if (
    RequirementData !== undefined &&
    RequirementData !== null &&
    Project !== null &&
    Project !== undefined
  ) {
    let sortingOrder = [];
    let requirementcoverageData = RequirementData.filter((col) => {
      return (
        col.ProjectReference === Project.ProjectReference &&
        col.ReleaseName === Project.ReleaseName &&
        col.RequirementTreeNode === Folder
        //col.ReqCoverage === RequirementStatus
      );
    });
    // requirementcoverageData = ConstructingDataBasedOnComponent(requirementcoverageData, epicfilter);
    requirementcoverageData.forEach((sort) => {
      if (sort.RequirementTreeSubNode !== "") {
        sortingOrder.push(sort.RequirementTreeSubNode);
      }
    });

    if (sortingOrder.length !== 0) {
      sortingOrder = sortingOrder.filter(
        (value, index) => sortingOrder.indexOf(value) === index
      );
      sortingOrder = sortingOrder.sort();
    }

    if (
      requirementcoverageData !== undefined &&
      requirementcoverageData.length !== 0
    ) {
      let series = [];
      let reqCoverageStatus = [];
      for (let i = 0; i < requirementcoverageData.length; i++) {
        let testExecutionData = requirementcoverageData[i];
        if (
          testExecutionData.reqCoverageStatus !== null &&
          testExecutionData.reqCoverageStatus !== undefined &&
          testExecutionData.RequirementTreeSubNode !== ""
        ) {
          if (reqCoverageStatus.length === 0) {
            reqCoverageStatus.push(testExecutionData.reqCoverageStatus);
            let data = [];
            let drillobject = {
              name: testExecutionData.RequirementTreeSubNode,
              y: testExecutionData.count,
            };
            data.push(drillobject);
            let seriesObj = {
              name: testExecutionData.reqCoverageStatus,
              // pointWidth: 20,
              data: data,
            };
            series.push(seriesObj);
          } else {
            let reqCoverageStatusFind = reqCoverageStatus.filter((col) => {
              return col === testExecutionData.reqCoverageStatus;
            });
            if (reqCoverageStatusFind.length === 0) {
              reqCoverageStatus.push(testExecutionData.reqCoverageStatus);
              let data = [];
              let drillobject = {
                name: testExecutionData.RequirementTreeSubNode,
                y: testExecutionData.count,
              };
              data.push(drillobject);
              let seriesObj = {
                name: testExecutionData.reqCoverageStatus,
                // pointWidth: 20,
                data: data,
              };
              series.push(seriesObj);
            } else {
              // let index = reqCoverageStatus.findIndex(
              //   (img) => img === testExecutionData.reqCoverageStatus
              // );
              let EditSerisObj = series.filter((col) => {
                return col.name === testExecutionData.reqCoverageStatus;
              });
              let drillobject = {
                name: testExecutionData.RequirementTreeSubNode,
                y: testExecutionData.count,
              };
              EditSerisObj[0].data.push(drillobject);
            }
          }
        }
      }
      let Finalseries = [];
      series.forEach((s) => {
        s.data = addSeriesMultilevel(s.data, sortingOrder);
        Finalseries.push(s);
      });
      Finalseries = SeriesOrder(Finalseries);
      if (Finalseries.length !== 0) {
        return Finalseries;
      } else {
        return 0;
      }
    } else {
      return 0;
    }
  } else {
    return 0;
  }
};

/* Test Execution Status above 4th level */

export const RequirementCoverageDynamicLevels = (
  RequirementData: any,
  Project: any,
  Folder: any,
  ManiFolderData: any,
  count: any,
  epicfilter: any
) => {
  if (
    RequirementData !== undefined &&
    RequirementData !== null &&
    Project !== null &&
    Project !== undefined
  ) {
    let sortingOrder = [];
    let folderdata = RequirementDataFormation(count);
    let requirementcoverageData = RequirementData.filter((col) => {
      return (
        col.ProjectReference === Project.ProjectReference &&
        col.ReleaseName === Project.ReleaseName &&
        col.RequirementTreeNode === ManiFolderData &&
        col[folderdata.folder1] === Folder &&
        col[folderdata.folder2] !== null &&
        col[folderdata.folder2] !== ""
      );
    });
    // requirementcoverageData = ConstructingDataBasedOnComponent(requirementcoverageData, epicfilter);
    requirementcoverageData = constructingDynamicData(
      count,
      requirementcoverageData
    );
    requirementcoverageData.forEach((sort) => {
      if (
        sort[folderdata.folder2] !== null &&
        sort[folderdata.folder2] !== undefined &&
        sort[folderdata.folder2] !== ""
      ) {
        sortingOrder.push(sort[folderdata.folder2]);
      }
    });
    if (sortingOrder.length !== 0) {
      sortingOrder = sortingOrder.filter(
        (value, index) => sortingOrder.indexOf(value) === index
      );
      sortingOrder = sortingOrder.sort();
    }
    if (
      requirementcoverageData !== undefined &&
      requirementcoverageData.length !== 0
    ) {
      let series = [];
      let reqCoverageStatus = [];
      for (let i = 0; i < requirementcoverageData.length; i++) {
        let testExecutionData = requirementcoverageData[i];
        if (
          testExecutionData.reqCoverageStatus !== null &&
          testExecutionData.reqCoverageStatus !== undefined &&
          testExecutionData.reqCoverageStatus !== ""
        ) {
          if (reqCoverageStatus.length === 0) {
            let data = [];
            if (
              testExecutionData[folderdata.folder2] !== null &&
              testExecutionData[folderdata.folder2] !== ""
            ) {
              reqCoverageStatus.push(testExecutionData.reqCoverageStatus);
              let drillobject = {
                name: testExecutionData[folderdata.folder2],
                y: testExecutionData.count,
              };
              data.push(drillobject);
              let seriesObj = {
                name: testExecutionData.reqCoverageStatus,
                // pointWidth: 10,
                data: data,
              };
              series.push(seriesObj);
            }
          } else {
            let reqCoverageStatusFind = reqCoverageStatus.filter((col) => {
              return col === testExecutionData.reqCoverageStatus;
            });
            if (reqCoverageStatusFind.length === 0) {
              let data = [];
              if (
                testExecutionData[folderdata.folder2] !== null &&
                testExecutionData[folderdata.folder2] !== ""
              ) {
                reqCoverageStatus.push(testExecutionData.reqCoverageStatus);
                let drillobject = {
                  name: testExecutionData[folderdata.folder2],
                  y: testExecutionData.count,
                };
                data.push(drillobject);
                let seriesObj = {
                  name: testExecutionData.reqCoverageStatus,
                  // pointWidth: 10,
                  data: data,
                };
                series.push(seriesObj);
              }
            } else {
              let EditSerisObj = series.filter((col) => {
                return col.name === testExecutionData.reqCoverageStatus;
              });
              if (EditSerisObj.length !== 0) {
                if (
                  testExecutionData[folderdata.folder2] !== null &&
                  testExecutionData[folderdata.folder2] !== ""
                ) {
                  let drillobject = {
                    name: testExecutionData[folderdata.folder2],
                    y: testExecutionData.count,
                  };
                  EditSerisObj[0].data.push(drillobject);
                }
              }
            }
          }
        } else {
          return [];
        }
      }
      let Finalseries = [];
      series.forEach((s) => {
        s.data = addSeriesMultilevel(s.data, sortingOrder);
        Finalseries.push(s);
      });
      Finalseries = SeriesOrder(Finalseries);
      if (Finalseries.length !== 0) {
        return Finalseries;
      } else {
        return 0;
      }
    } else {
      return 0;
    }
  } else {
    return 0;
  }
};

/*Constructing the order of the Data*/

function SeriesOrderForMainFunction(data) {
  let priorityData = [];
  if (data.length !== 0) {
    RequirementData_Main_priority_order.forEach((order) => {
      let TempItem = data.filter((col) => {
        return col.name === order;
      });
      if (TempItem.length !== 0) {
        let Findcolor = RequirementData_Main_colors.filter((col) => {
          return col.name === TempItem[0].name;
        });
        if (Findcolor.length !== 0) {
          TempItem[0].color = Findcolor[0].color;
        } else {
          TempItem[0].color = "#DC143C";
        }
        priorityData.push(TempItem[0]);
      }
    });
  }
  return priorityData;
}

/**Point Width Calculation */

export function PointwidthCal(Finalseries) {
  if (Finalseries.length !== 0) {
    if (Finalseries[0].data.length > 10) {
      for (let obj = 0; obj < Finalseries.length; obj++) {
        Finalseries[obj].pointWidth = 10;
      }
    }
  }
  return Finalseries;
}

/* Dynamic Data filter */

function constructingDynamicData(count, TestResults) {
  let requirementData = TestResults;
  if (count === 4) {
    requirementData = TestResults.filter((col) => {
      return col.RequirementTreeSubNode === RequirementsubFolderCapture;
    });
  } else if (count > 4 && count <= 6) {
    requirementData = TestResults.filter((col) => {
      return (
        col.RequirementTreeSubNode === RequirementsubFolderCapture &&
        col.RequirementTreeSubNode1 === RequirementsubFolder2Capture
      );
    });
  } else if (count > 6 && count <= 7) {
    requirementData = TestResults.filter((col) => {
      return (
        col.RequirementTreeSubNode === RequirementsubFolderCapture &&
        col.RequirementTreeSubNode1 === RequirementsubFolder2Capture &&
        col.RequirementTreeSubNode3 === RequirementsubFolder3Capture
      );
    });
  } else if (count > 7 && count >= 8) {
    requirementData = TestResults.filter((col) => {
      return (
        col.RequirementTreeSubNode === RequirementsubFolderCapture &&
        col.RequirementTreeSubNode1 === RequirementsubFolder2Capture &&
        col.RequirementTreeSubNode3 === RequirementsubFolder3Capture &&
        col.RequirementTreeSubNode4 === RequirementsubFolder5Capture
      );
    });
  } else if (count >= 9 && count <= 10) {
    requirementData = TestResults.filter((col) => {
      return (
        col.RequirementTreeSubNode === RequirementsubFolderCapture &&
        col.RequirementTreeSubNode1 === RequirementsubFolder2Capture &&
        col.RequirementTreeSubNode3 === RequirementsubFolder3Capture &&
        col.RequirementTreeSubNode4 === RequirementsubFolder5Capture &&
        col.RequirementTreeSubNode6 === RequirementsubFolder6Capture
      );
    });
  } else if (count >= 11) {
    requirementData = TestResults.filter((col) => {
      return (
        col.RequirementTreeSubNode === RequirementsubFolderCapture &&
        col.RequirementTreeSubNode1 === RequirementsubFolder2Capture &&
        col.RequirementTreeSubNode3 === RequirementsubFolder3Capture &&
        col.RequirementTreeSubNode4 === RequirementsubFolder5Capture &&
        col.RequirementTreeSubNode6 === RequirementsubFolder6Capture &&
        col.RequirementTreeSubNode8 === RequirementsubFolder8Capture
      );
    });
  }
  return requirementData;
}


/*Constructing the order of the Data*/

function SeriesOrder (data:any) {
  let priorityData = [];
  if (data.length !== 0) {
    RequirementResults_priority_order.forEach((order) => {
      let TempItem = data.filter((col) => {
        return col.name === order;
      });
      if (TempItem.length !== 0) {
        let Findcolor = RequirementData_colors.filter((col) => {
          return col.name === TempItem[0].name;
        });
        if (Findcolor.length !== 0) {
          TempItem[0].color = Findcolor[0].color;
        } else {
          TempItem[0].color = "#DC143C";
        }
        priorityData.push(TempItem[0]);
      }
    });
  }
  return priorityData;
}


// /* Based on Component */

// function ConstructingDataBasedOnComponent(data, Component) {
//   let priorityData = [];
//   if (data.length !== 0) {
//     if (
//       Component.epicLink === config.allEpic
//     ) {
//       priorityData = data;
//     } else {
//       let ComponentData = data.filter((col) => {
//         return col.epicLink !== null;
//       });
//       if (ComponentData.length !== 0) {
//         priorityData = ComponentData.filter((col) => {
//           return col.epicLink === Component.epicLink;
//         });
//       }
//     }
//   }
//   return priorityData;
// }
