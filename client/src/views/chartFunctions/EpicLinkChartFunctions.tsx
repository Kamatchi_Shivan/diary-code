import { addSeriesMultilevel } from "../common/TestExecutionCommonFunctions";
import {
  UserStories_Main_priority_order,
  UserStories_Main_colors,
  Defects_MVP_Priority,
  Defects_NOTMVP_Priority,
} from "../common/Chartcolors";
import { config } from "../../config/configs";
import { ConstructingFirstLevelUserStoryTableView } from "./userStoryTableView";


/*Level 1 Data */

export const EpicFirstLevelMainLevel = (
  UserStoriesData: any,
  Project: any,
  priorityType: any,
  component: any,
  btnValue: any
) => {
  if (
    UserStoriesData !== undefined &&
    UserStoriesData !== null &&
    Project !== null &&
    Project !== undefined
  ) {
    let sortingOrder = [];
    let UserStoriesResults = UserStoriesData.filter((col) => {
      return (
        col.ProjectReference === Project.ProjectReference &&
        col.fixVersions === Project.ReleaseName
      );
    });
    UserStoriesResults = ConstructingDataBasedOnType(
      UserStoriesResults,
      priorityType
    );
    UserStoriesResults = ConstructingDataBasedOnComponent(
      UserStoriesResults,
      component
    );
    UserStoriesResults.forEach((sort) => {
      sortingOrder.push(sort.epicName);
    });
    if (sortingOrder.length !== 0) {
      sortingOrder = sortingOrder.filter(
        (value, index) => sortingOrder.indexOf(value) === index
      );
    }
    if (UserStoriesResults !== undefined && UserStoriesResults.length !== 0) {
      let series = [];
      let storyStatus = [];
      for (let i = 0; i < UserStoriesResults.length; i++) {
        let testExecutionData = UserStoriesResults[i];
        if (
          testExecutionData.storyStatus !== null &&
          testExecutionData.storyStatus !== undefined
        ) {
          if (storyStatus.length === 0) {
            storyStatus.push(testExecutionData.storyStatus);
            let data = [];
            let drillobject = {
              name: testExecutionData.epicName,
              y: testExecutionData.ProjectCount,
            };
            data.push(drillobject);
            let seriesObj = {
              name: testExecutionData.storyStatus,
              data: data,
            };
            series.push(seriesObj);
          } else {
            let storyStatusFind = storyStatus.filter((col) => {
              return col === testExecutionData.storyStatus;
            });
            if (storyStatusFind.length === 0) {
              storyStatus.push(testExecutionData.storyStatus);
              let data = [];
              let drillobject = {
                name: testExecutionData.epicName,
                y: testExecutionData.statusCount,
              };
              data.push(drillobject);
              let seriesObj = {
                name: testExecutionData.storyStatus,
                data: data,
              };
              series.push(seriesObj);
            } else {
              let EditSerisObj = series.filter((col) => {
                return col.name === testExecutionData.storyStatus;
              });
              let drillobject = {
                name: testExecutionData.epicName,
                y: testExecutionData.statusCount,
              };
              EditSerisObj[0].data.push(drillobject);
            }
          }
        }
      }
      let Finalseries = [];
      series.forEach((s) => {
        s.data = addSeriesMultilevel(s.data, sortingOrder);
        Finalseries.push(s);
      });
      Finalseries = SeriesOrderForMainFunction(Finalseries);
      let DetailedEpics = EpicSecondLevelMainLevel(
        UserStoriesData,
        Project,
        priorityType,
        component,
        btnValue
      );
      let tableViewData = ConstructingFirstLevelUserStoryTableView(sortingOrder, DetailedEpics.Finalseries, btnValue);
      return {
        Finalseries: Finalseries,
        tableViewData: tableViewData
      };
    } else {
      return {
        Finalseries: [],
        tableViewData: []
      };
    }
  } else {
    return {
      Finalseries: [],
      tableViewData: []
    };
  }
};

/*Epic 2nd level */

export const EpicSecondLevelMainLevel = (
  UserStoriesData: any,
  Project: any,
  priorityType: any,
  component: any,
  btnValue: any

) => {
  if (
    UserStoriesData !== undefined &&
    UserStoriesData !== null &&
    Project !== null &&
    Project !== undefined
  ) {
    let sortingOrder = [];
    let UserStoriesResults = UserStoriesData.filter((col) => {
      return (
        col.ProjectReference === Project.ProjectReference &&
        col.fixVersions === Project.ReleaseName &&
        col.epicLink !== "Un-Defined"
        // &&
        // col.epicName === dataset
      );
    });
    UserStoriesResults = ConstructingDataBasedOnType(
      UserStoriesResults,
      priorityType
    );
    UserStoriesResults = ConstructingDataBasedOnComponent(
      UserStoriesResults,
      component
    );
    UserStoriesResults.forEach((sort) => {
      sortingOrder.push(sort.epicName);
    });
    if (sortingOrder.length !== 0) {
      sortingOrder = sortingOrder.filter(
        (value, index) => sortingOrder.indexOf(value) === index
      );
    }
    if (UserStoriesResults !== undefined && UserStoriesResults.length !== 0) {
      let series = [];
      let storyStatus = [];
      for (let i = 0; i < UserStoriesResults.length; i++) {
        let testExecutionData = UserStoriesResults[i];
        if (
          testExecutionData.storyStatus !== null &&
          testExecutionData.storyStatus !== undefined
        ) {
          if (storyStatus.length === 0) {
            storyStatus.push(testExecutionData.storyStatus);
            let data = [];
            let drillobject = {
              name: testExecutionData.epicName,
              y: testExecutionData.ProjectCount,
            };
            data.push(drillobject);
            let seriesObj = {
              name: testExecutionData.storyStatus,
              data: data,
            };
            series.push(seriesObj);
          } else {
            let storyStatusFind = storyStatus.filter((col) => {
              return col === testExecutionData.storyStatus;
            });
            if (storyStatusFind.length === 0) {
              storyStatus.push(testExecutionData.storyStatus);
              let data = [];
              let drillobject = {
                name: testExecutionData.epicName,
                y: testExecutionData.statusCount,
              };
              data.push(drillobject);
              let seriesObj = {
                name: testExecutionData.storyStatus,
                data: data,
              };
              series.push(seriesObj);
            } else {
              let EditSerisObj = series.filter((col) => {
                return col.name === testExecutionData.storyStatus;
              });
              let drillobject = {
                name: testExecutionData.epicName,
                y: testExecutionData.statusCount,
              };
              EditSerisObj[0].data.push(drillobject);
            }
          }
        }
      }
      let Finalseries = [];
      series.forEach((s) => {
        s.data = addSeriesMultilevel(s.data, sortingOrder);
        Finalseries.push(s);
      });
      Finalseries = SeriesOrderForMainFunction(Finalseries);
      let tableViewData = ConstructingFirstLevelUserStoryTableView(sortingOrder, Finalseries, btnValue);
      return {
        Finalseries: Finalseries,
        tableViewData: tableViewData
      };
    } else {
      return {
        Finalseries: [],
        tableViewData: []
      };
    }
  } else {
    return {
      Finalseries: [],
      tableViewData: []
    };
  }
};

/*Epic 2nd level */

export const AllEpics = (
  UserStoriesData: any,
  component: any,
  btnValue: any

) => {
  if (
    UserStoriesData !== undefined &&
    UserStoriesData !== null
  ) {
	let statusCount:Number = 1;
    let sortingOrder = [];
    let UserStoriesResults = UserStoriesData;
    UserStoriesResults = ConstructingDataBasedOnComponent(
      UserStoriesResults,
      component
    );
    UserStoriesResults.forEach((sort) => {
      if(sort.epicName !== config.allEpic){
        sortingOrder.push(sort.epicName);
      }
    });
    if (sortingOrder.length !== 0) {
      sortingOrder = sortingOrder.filter(
        (value, index) => sortingOrder.indexOf(value) === index
      );
    }
    if (UserStoriesResults !== undefined && UserStoriesResults.length !== 0) {
      let series = [];
      let epicStatus = [];
      for (let i = 0; i < UserStoriesResults.length; i++) {
        let testExecutionData = UserStoriesResults[i];
        if (
          testExecutionData.epicStatus !== null &&
          testExecutionData.epicStatus !== undefined
        ) {
          if (epicStatus.length === 0) {
            epicStatus.push(testExecutionData.epicStatus);
            let data = [];
            let drillobject = {
              name: testExecutionData.epicName,
              y: statusCount
            };
            data.push(drillobject);
            let seriesObj = {
              name: testExecutionData.epicStatus,
              data: data,
            };
            series.push(seriesObj);
          } else {
            let storyStatusFind = epicStatus.filter((col) => {
              return col === testExecutionData.epicStatus;
            });
            if (storyStatusFind.length === 0) {
              epicStatus.push(testExecutionData.epicStatus);
              let data = [];
              let drillobject = {
                name: testExecutionData.epicName,
                y: statusCount,
              };
              data.push(drillobject);
              let seriesObj = {
                name: testExecutionData.epicStatus,
                data: data,
              };
              series.push(seriesObj);
            } else {
              let EditSerisObj = series.filter((col) => {
                return col.name === testExecutionData.epicStatus;
              });
              let drillobject = {
                name: testExecutionData.epicName,
                y: statusCount,
              };
              EditSerisObj[0].data.push(drillobject);
            }
          }
        }
      }
      let Finalseries = [];
      series.forEach((s) => {
        s.data = addSeriesMultilevel(s.data, sortingOrder);
        Finalseries.push(s);
      });
      Finalseries = SeriesOrderForMainFunction(Finalseries);
      let tableViewData = ConstructingFirstLevelUserStoryTableView(sortingOrder, Finalseries, btnValue);
      return {
        Finalseries: Finalseries,
        tableViewData: tableViewData
      };
    } else {
      return {
        Finalseries: [],
        tableViewData: []
      };
    }
  } else {
    return {
      Finalseries: [],
      tableViewData: []
    };
  }
};



/*Constructing the order of the Data*/

function SeriesOrderForMainFunction(data) {
  let priorityData = [];
  if (data.length !== 0) {
    UserStories_Main_priority_order.forEach((order) => {
      let TempItem = data.filter((col) => {
        return col.name === order;
      });
      if (TempItem.length !== 0) {
        let Findcolor = UserStories_Main_colors.filter((col) => {
          return col.name === TempItem[0].name;
        });
        if (Findcolor.length !== 0) {
          TempItem[0].color = Findcolor[0].color;
        } else {
          TempItem[0].color = "#DC143C";
        }
        priorityData.push(TempItem[0]);
      }
    });
  }
  return priorityData;
}



/*Adding Series Values for User Stories*/

export const addSeriesUserStories = (data: any, sortingOrder: any) => {
  let FinalData = [];
  if (data.length !== 0) {
    data.forEach((d) => {
      let sum = 0;
      if (FinalData.length === 0) {
        let FilterElement = data.filter((subdata) => {
          return d.name === subdata.name;
        });
        FilterElement.forEach((datasum) => {
          sum = sum + datasum.y;
        });
        let finalobj = {
          name: d.name,
          y: sum,
        };
        FinalData.push(finalobj);
      } else {
        let m = FinalData.filter((matchdata) => {
          return d.name === matchdata.name;
        });
        if (m.length === 0) {
          let FilterElement = data.filter((subdata) => {
            return d.name === subdata.name;
          });
          FilterElement.forEach((datasum) => {
            sum = sum + datasum.y;
          });
          let finalobj = {
            name: d.name,
            y: sum,
          };
          FinalData.push(finalobj);
        }
      }
    });
  }
  return Datastoring(FinalData, sortingOrder);
};

/*Sorting Function*/

export function Datastoring(data, sortOrder) {
  for (let b = 0; b < sortOrder.length; b++) {
    let TempItem = data.filter((col) => {
      return col.name === sortOrder[b];
    });
    if (TempItem.length === 0) {
      let tempObj = {
        name: sortOrder[b],
        y: 0,
      };
      data.splice(b, 0, tempObj);
    }
  }
  return data;
}

/**Point Width Calculation */

export function PointwidthCal(Finalseries) {
  if (Finalseries.length !== 0) {
    if (Finalseries[0].data.length > 1) {
      for (let obj = 0; obj < Finalseries.length; obj++) {
        Finalseries[obj].pointWidth = 10;
      }
    }
  }
  return Finalseries;
}

/* Based on Type */

function ConstructingDataBasedOnType(data, type) {
  let priorityData = [];
  if (data.length !== 0) {
    if (type === config.allstories) {
      priorityData = data;
    } else if (type === config.mvp) {
      Defects_MVP_Priority.forEach((order) => {
        let TempItem = data.filter((col) => {
          return col.priority === order;
        });
        if (TempItem.length !== 0) {
          priorityData = priorityData.concat(TempItem);
        }
      });
    } else {
      Defects_NOTMVP_Priority.forEach((order) => {
        let TempItem = data.filter((col) => {
          return col.priority === order;
        });
        if (TempItem.length !== 0) {
          priorityData = priorityData.concat(TempItem);
        }
      });
    }
  }
  return priorityData;
}


/* Based on Component */

function ConstructingDataBasedOnComponent(data, Component) {
  let priorityData = [];
  if (data.length !== 0) {
    if (Component.epicName === config.allEpic) {
      priorityData = data;
    } else {
      let ComponentData = data.filter((col) => {
        return col.epicName !== null;
      });
      if (ComponentData.length !== 0) {
        priorityData = ComponentData.filter((col) => {
          return col.epicName === Component.epicName;
        });
      }
    }
  }
  return priorityData;
}
