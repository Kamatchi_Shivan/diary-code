import {
  addSeriesMultilevel,
  SeriesOrder,
} from "../common/TestExecutionCommonFunctions";

/*High Charts*/

export const TestExecutionChartData = (TestResults: any, Project: any) => {
  if (
    TestResults.length !== 0 &&
    TestResults !== undefined &&
    TestResults !== null &&
    Project.length !== 0 &&
    Project !== null &&
    Project !== undefined
  ) {
    let sortingOrder = [];
    let testData = [];
    Project.forEach((project) => {
      TestResults.forEach((tests) => {
        if (
          project.ProjectReference === tests.ProjectReference &&
          project.ReleaseName === tests.ReleaseName
        ) {
          testData.push(tests);
        }
      });
    });
    testData.forEach((sort) => {
      sortingOrder.push(sort.ProjectName);
    });
    if (sortingOrder.length !== 0) {
      sortingOrder = sortingOrder.filter(
        (value, index) => sortingOrder.indexOf(value) === index
      );
    }

    if (testData !== undefined && testData.length !== 0) {
      let series = [];
      let testStatus = [];

      for (let i = 0; i < testData.length; i++) {
        let testExecutionData = testData[i];
        if (
          testExecutionData.testStatus !== null &&
          testExecutionData.testStatus !== undefined
        ) {
          if (testStatus.length === 0) {
            testStatus.push(testExecutionData.testStatus);
            let data = [];
            let drillobject = {
              name: testExecutionData.ProjectName,
              y: testExecutionData.resultsInPercentage,
            };
            data.push(drillobject);
            let seriesObj = {
              name: testExecutionData.testStatus,
              //pointWidth: 50,
              data: data,
            };

            series.push(seriesObj);
          } else {
            let testStatusFind = testStatus.filter((col) => {
              return col === testExecutionData.testStatus;
            });
            if (testStatusFind.length === 0) {
              testStatus.push(testExecutionData.testStatus);
              let data = [];
              let drillobject = {
                name: testExecutionData.ProjectName,
                y: testExecutionData.resultsInPercentage,
              };
              data.push(drillobject);
              let seriesObj = {
                name: testExecutionData.testStatus,
                //pointWidth: 50,
                data: data,
              };

              series.push(seriesObj);
            } else {
              let EditSerisObj = series.filter((col) => {
                return col.name === testExecutionData.testStatus;
              });

              let drillobject = {
                name: testExecutionData.ProjectName,
                y: testExecutionData.resultsInPercentage,
              };
              EditSerisObj[0].data.push(drillobject);
            }
          }
        }
      }
      let Finalseries = [];
      series.forEach((s) => {
        s.data = addSeriesMultilevel(s.data, sortingOrder);
        Finalseries.push(s);
      });
      Finalseries = SeriesOrder(Finalseries);
      return Finalseries;
    } else {
      return [];
    }
  } else {
    return [];
  }
};
