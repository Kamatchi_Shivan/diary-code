import {
  Defects_colors,
  Defects_priority_order,
  Defect_status_order,
  Defects_MVP_Priority,
  Defects_NOTMVP_Priority,
} from "../common/Chartcolors";
import { config } from "../../config/configs";
import {ConstructingDefectView} from './defectChartView';

export const DefectByStatusCharts = (
  Defects: any,
  summaryDatas: any,
  componentData: any,
  priorityType: any,
  defectType: any,
) => (dispatch: any, option: any) => {
  let Bypriority = {};
  if (Defects !== undefined) {
    let Finalpriority = [];
    let defectProjectName = [];
    let priorityName = [];
    let data = [];
    summaryDatas.forEach((project) => {
      Defects.forEach((defect) => {
        if (
          project.ProjectReference === defect.projectReference &&
          project.ReleaseName === defect.ReleaseName
        ) {
          Finalpriority.push(defect);
        }
      });
    });
    Finalpriority = ConstructingDataBasedOnType(Finalpriority, priorityType);
    Finalpriority = ConstructingDataBasedOnCategory(Finalpriority, defectType);
    Finalpriority = DefectStatusOrder(Finalpriority);
    if (Finalpriority.length !== 0) {
      for (let i = 0; i < Finalpriority.length; i++) {
        let countData = [];
        let defectData = Finalpriority[i];
        if (defectProjectName.length === 0) {
          defectProjectName.push(defectData.projectName);
          priorityName.push(defectData.priority);
          countData.push(defectData.count);
          let dataObj = {
            label: defectData.priority,
            data: countData,
          };
          data.push(dataObj);
        } else {
          let ProjectNameFind = defectProjectName.filter((col) => {
            return col === defectData.projectName;
          });
          if (ProjectNameFind.length === 0) {
            defectProjectName.push(defectData.projectName);
            let index = defectProjectName.length - 1;
            let priorityFind = priorityName.filter((col) => {
              return col === defectData.priority;
            });
            if (priorityFind.length === 0) {
              priorityName.push(defectData.priority);
              for (let j = 0; j <= index; j++) {
                if (j === index) {
                  countData.splice(j, 1, defectData.count);
                } else {
                  countData.splice(j, 1, 0);
                }
              }
              let dataObj = {
                label: defectData.priority,
                data: countData,
              };
              data.push(dataObj);
            } else {
              for (let s = 0; s < data.length; s++) {
                let d = data[s];
                if (d.label === defectData.priority) {
                  for (let k = 0; k <= index; k++) {
                    if (k === index) {
                      d.data.splice(index, 1, defectData.count);
                    } else {
                      if (
                        d.data[k] === 0 ||
                        d.data[k] === null ||
                        d.data[k] === undefined
                      ) {
                        d.data.splice(k, 1, 0);
                      }
                    }
                  }
                }
              }
            }
          } else {
            let index = defectProjectName.findIndex(
              (img) => img === defectData.projectName
            );
            let priorityFind = priorityName.filter((col) => {
              return col === defectData.priority;
            });
            if (priorityFind.length === 0) {
              priorityName.push(defectData.priority);
              for (let j = 0; j <= index; j++) {
                if (j === index) {
                  countData.splice(j, 1, defectData.count);
                } else {
                  countData.splice(j, 1, 0);
                }
              }
              let dataObj = {
                label: defectData.priority,
                data: countData,
              };
              data.push(dataObj);
            } else {
              for (let s = 0; s < data.length; s++) {
                let d = data[s];
                if (d.label === defectData.priority) {
                  for (let k = 0; k <= index; k++) {
                    if (k === index) {
                      let addCount = d.data[k] + defectData.count;
                      if (isNaN(addCount)) {
                        addCount = defectData.count;
                      }
                      d.data.splice(index, 1, addCount);
                    } else {
                      if (
                        d.data[k] === 0 ||
                        d.data[k] === null ||
                        d.data[k] === undefined
                      ) {
                        d.data.splice(k, 1, 0);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      let priorityData = AddingColorsAndSeries(data);
      Bypriority = {
        labels: defectProjectName,
        datasets: priorityData,
      };
      return Bypriority;
    } else {
      let emppriority = { labels: [config.nodataavailable] };
      return emppriority;
    }
  } else {
    let emppriority = { labels: [config.nodataavailable] };
    return emppriority;
  }
};

/* Defect By Assignee */

export const DefectByAssigneeCharts = (
  AssigneeDefects: any,
  summaryDatas: any,
  componentData: any,
  TestDefectCollections: any,
  priorityType: any,
  defectType: any,
) => (dispatch: any, option: any) => {
  let Bypriority = {};
  if (AssigneeDefects !== undefined) {
    let Finalpriority = [];
    let defectassigneeName = [];
    let priorityName = [];
    let data = [];
    if (TestDefectCollections.length !== 0) {
      TestDefectCollections.forEach((testResults) => {
        summaryDatas.forEach((project) => {
          AssigneeDefects.forEach((defect) => {
            if (componentData.epicName === "All Epic's") {
              if (
                project.ProjectReference === defect.projectReference &&
                defect.ReleaseName === project.ReleaseName &&
                testResults === defect.defectKey
              ) {
                Finalpriority.push(defect);
              }
            } else {
              if (
                project.ProjectReference === defect.projectReference &&
                defect.ReleaseName === project.ReleaseName &&
                testResults === defect.defectKey &&
                componentData.epicName === defect.epicName
              ) {
                Finalpriority.push(defect);
              }
            }
          });
        });
      });
    } else {
      summaryDatas.forEach((project) => {
        AssigneeDefects.forEach((defect) => {
          if (componentData.epicName === "All Epic's") {
            if (
              project.ProjectReference === defect.projectReference &&
              defect.ReleaseName === project.ReleaseName
            ) {
              Finalpriority.push(defect);
            }
          } else {
            if (
              project.ProjectReference === defect.projectReference &&
              componentData.epicName === defect.epicName &&
              defect.ReleaseName === project.ReleaseName
            ) {
              Finalpriority.push(defect);
            }
          }
        });
      });
    }
    Finalpriority = ConstructingDataBasedOnType(Finalpriority, priorityType);
    Finalpriority = ConstructingDataBasedOnCategory(Finalpriority, defectType);
    if (Finalpriority.length !== 0) {
      for (let i = 0; i < Finalpriority.length; i++) {
        let countData = [];
        let defectData = Finalpriority[i];
        if (
          defectData.assigneeName !== null &&
          defectData.assigneeName !== undefined
        ) {
          if (defectassigneeName.length === 0) {
            defectassigneeName.push(defectData.assigneeName);
            priorityName.push(defectData.priority);
            countData.push(defectData.count);
            let dataObj = {
              label: defectData.priority,
              data: countData,
            };
            data.push(dataObj);
          } else {
            let assigneeNameFind = defectassigneeName.filter((col) => {
              return col === defectData.assigneeName;
            });
            if (assigneeNameFind.length === 0) {
              defectassigneeName.push(defectData.assigneeName);
              let index = defectassigneeName.length - 1;
              let priorityFind = priorityName.filter((col) => {
                return col === defectData.priority;
              });
              if (priorityFind.length === 0) {
                priorityName.push(defectData.priority);
                for (let j = 0; j <= index; j++) {
                  if (j === index) {
                    countData.push(defectData.count);
                  } else {
                    countData.splice(j, 1, 0);
                  }
                }
                let dataObj = {
                  label: defectData.priority,
                  data: countData,
                };
                data.push(dataObj);
              } else {
                for (let s = 0; s < data.length; s++) {
                  let d = data[s];
                  if (d.label === defectData.priority) {
                    for (let k = 0; k <= index; k++) {
                      if (k === index) {
                        d.data.splice(index, 1, defectData.count);
                      } else {
                        if (
                          d.data[k] === 0 ||
                          d.data[k] === null ||
                          d.data[k] === undefined
                        ) {
                          d.data.splice(k, 1, 0);
                        }
                      }
                    }
                  }
                }
              }
            } else {
              let index = defectassigneeName.findIndex(
                (img) => img === defectData.assigneeName
              );
              let priorityFind = priorityName.filter((col) => {
                return col === defectData.priority;
              });
              if (priorityFind.length === 0) {
                priorityName.push(defectData.priority);
                for (let j = 0; j <= index; j++) {
                  if (j === index) {
                    countData.push(defectData.count);
                  } else {
                    countData.splice(j, 1, 0);
                  }
                }
                let dataObj = {
                  label: defectData.priority,
                  data: countData,
                };
                data.push(dataObj);
              } else {
                for (let s = 0; s < data.length; s++) {
                  let d = data[s];
                  if (d.label === defectData.priority) {
                    for (let k = 0; k <= index; k++) {
                      if (k === index) {
                        let addCount = d.data[k] + defectData.count;
                        if (isNaN(addCount)) {
                          addCount = defectData.count;
                        }
                        d.data.splice(index, 1, addCount);
                      } else {
                        if (
                          d.data[k] === 0 ||
                          d.data[k] === null ||
                          d.data[k] === undefined
                        ) {
                          d.data.splice(k, 1, 0);
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      let priorityData = AddingColorsAndSeries(data);
      let TableViewData = ConstructingDefectView(priorityData,defectassigneeName);
      Bypriority = {
        labels: defectassigneeName,
        datasets: priorityData,
        tableView: TableViewData
      };
      return Bypriority;
    } else {
      let emppriority = { labels: [config.nodataavailable] ,tableView: {}};
      return emppriority;
    }
  } else {
    let emppriority = { labels: [config.nodataavailable],tableView: {} };
    return emppriority;
  }
};

/* Defect By Components */

export const DefectByComponentCharts = (
  AssigneeDefects: any,
  summaryDatas: any,
  componentData: any,
  TestDefectCollections: any,
  priorityType: any,
  defectType: any,
) => (dispatch: any, option: any) => {
  let Bypriority = {};
  if (AssigneeDefects !== undefined) {
    let Finalpriority = [];
    let defectcomponent = [];
    let priorityName = [];
    let data = [];
    if (TestDefectCollections.length !== 0) {
      TestDefectCollections.forEach((testResults) => {
        summaryDatas.forEach((project) => {
          AssigneeDefects.forEach((defect) => {
            if (componentData.epicName === "All Epic's") {
              if (
                project.ProjectReference === defect.projectReference &&
                defect.ReleaseName === project.ReleaseName &&
                testResults === defect.defectKey
              ) {
                Finalpriority.push(defect);
              }
            } else {
              if (
                project.ProjectReference === defect.projectReference &&
                defect.ReleaseName === project.ReleaseName &&
                testResults === defect.defectKey &&
                componentData.epicName === defect.epicName
              ) {
                Finalpriority.push(defect);
              }
            }
          });
        });
      });
    } else {
      summaryDatas.forEach((project) => {
        AssigneeDefects.forEach((defect) => {
          if (componentData.epicName === "All Epic's") {
            if (
              project.ProjectReference === defect.projectReference &&
              defect.ReleaseName === project.ReleaseName
            ) {
              Finalpriority.push(defect);
            }
          } else {
            if (
              project.ProjectReference === defect.projectReference &&
              componentData.epicName === defect.epicName &&
              defect.ReleaseName === project.ReleaseName
            ) {
              Finalpriority.push(defect);
            }
          }
        });
      });
    }
    Finalpriority = ConstructingDataBasedOnType(Finalpriority, priorityType);
    Finalpriority = ConstructingDataBasedOnCategory(Finalpriority, defectType);
    if (Finalpriority.length !== 0) {
      for (let i = 0; i < Finalpriority.length; i++) {
        let countData = [];
        let defectData = Finalpriority[i];
        if (
          defectData.epicName !== null &&
          defectData.epicName !== undefined
          // &&
          // defectData.epicName !== "Un-Defined"
        ) {
          if (defectcomponent.length === 0) {
            defectcomponent.push(defectData.epicName);
            priorityName.push(defectData.priority);
            countData.push(defectData.count);
            let dataObj = {
              label: defectData.priority,
              data: countData,
            };
            data.push(dataObj);
          } else {
            let componentFind = defectcomponent.filter((col) => {
              return col === defectData.epicName;
            });
            if (componentFind.length === 0) {
              defectcomponent.push(defectData.epicName);
              let index = defectcomponent.length - 1;
              let priorityFind = priorityName.filter((col) => {
                return col === defectData.priority;
              });
              if (priorityFind.length === 0) {
                priorityName.push(defectData.priority);
                for (let j = 0; j <= index; j++) {
                  if (j === index) {
                    countData.push(defectData.count);
                  } else {
                    countData.splice(j, 1, 0);
                  }
                }
                let dataObj = {
                  label: defectData.priority,
                  data: countData,
                };
                data.push(dataObj);
              } else {
                for (let s = 0; s < data.length; s++) {
                  let d = data[s];
                  if (d.label === defectData.priority) {
                    for (let k = 0; k <= index; k++) {
                      if (k === index) {
                        d.data.splice(index, 1, defectData.count);
                      } else {
                        if (
                          d.data[k] === 0 ||
                          d.data[k] === null ||
                          d.data[k] === undefined
                        ) {
                          d.data.splice(k, 1, 0);
                        }
                      }
                    }
                  }
                }
              }
            } else {
              let index = defectcomponent.findIndex(
                (img) => img === defectData.epicName
              );
              let priorityFind = priorityName.filter((col) => {
                return col === defectData.priority;
              });
              if (priorityFind.length === 0) {
                priorityName.push(defectData.priority);
                for (let j = 0; j <= index; j++) {
                  if (j === index) {
                    countData.push(defectData.count);
                  } else {
                    countData.splice(j, 1, 0);
                  }
                }
                let dataObj = {
                  label: defectData.priority,
                  data: countData,
                };
                data.push(dataObj);
              } else {
                for (let s = 0; s < data.length; s++) {
                  let d = data[s];
                  if (d.label === defectData.priority) {
                    for (let k = 0; k <= index; k++) {
                      if (k === index) {
                        let addCount = d.data[k] + defectData.count;
                        if (isNaN(addCount)) {
                          addCount = defectData.count;
                        }
                        d.data.splice(index, 1, addCount);
                      } else {
                        if (
                          d.data[k] === 0 ||
                          d.data[k] === null ||
                          d.data[k] === undefined
                        ) {
                          d.data.splice(k, 1, 0);
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      let priorityData = AddingColorsAndSeries(data);
      let TableViewData = ConstructingDefectView(priorityData,defectcomponent);
      if (defectcomponent.length !== 0 && priorityData.length !== 0) {
        Bypriority = {
          labels: defectcomponent,
          datasets: priorityData,
          tableView: TableViewData
        };
      } else {
        Bypriority = { labels: [config.nodataavailable] ,tableView: TableViewData};
      }
      return Bypriority;
    } else {
      let emppriority = { labels: [config.nodataavailable],tableView: {}  };
      return emppriority;
    }
  } else {
    let emppriority = { labels: [config.nodataavailable],tableView: {} };
    return emppriority;
  }
};

/* Defect By Domains */

export const DefectByDomainsCharts = (
  AssigneeDefects: any,
  summaryDatas: any,
  priorityType: any,
  defectType: any,
) => (dispatch: any, option: any) => {
  let Bypriority = {};
  if (AssigneeDefects !== undefined) {
    let Finalpriority = [];
    let defectdomainName = [];
    let priorityName = [];
    let data = [];
    summaryDatas.forEach((project) => {
      AssigneeDefects.forEach((defect) => {
        if (
          project.ProjectReference === defect.projectReference &&
          defect.ReleaseName === project.ReleaseName
        ) {
          ///dont change here
          Finalpriority.push(defect);
        }
      });
    });
    Finalpriority = ConstructingDataBasedOnType(Finalpriority, priorityType);
    Finalpriority = ConstructingDataBasedOnCategory(Finalpriority, defectType);
    if (Finalpriority.length !== 0) {
      for (let i = 0; i < Finalpriority.length; i++) {
        let countData = [];
        let defectData = Finalpriority[i];
        if (
          defectData.domainName !== null &&
          defectData.domainName !== undefined
        ) {
          if (defectdomainName.length === 0) {
            defectdomainName.push(defectData.domainName);
            priorityName.push(defectData.priority);
            countData.push(defectData.count);
            let dataObj = {
              label: defectData.priority,
              data: countData,
            };
            data.push(dataObj);
          } else {
            let domainNameFind = defectdomainName.filter((col) => {
              return col === defectData.domainName;
            });
            if (domainNameFind.length === 0) {
              defectdomainName.push(defectData.domainName);
              let index = defectdomainName.length - 1;
              let priorityFind = priorityName.filter((col) => {
                return col === defectData.priority;
              });
              if (priorityFind.length === 0) {
                priorityName.push(defectData.priority);
                for (let j = 0; j <= index; j++) {
                  if (j === index) {
                    countData.push(defectData.count);
                  } else {
                    countData.splice(j, 1, 0);
                  }
                }
                let dataObj = {
                  label: defectData.priority,
                  data: countData,
                };
                data.push(dataObj);
              } else {
                for (let s = 0; s < data.length; s++) {
                  let d = data[s];
                  if (d.label === defectData.priority) {
                    for (let k = 0; k <= index; k++) {
                      if (k === index) {
                        d.data.splice(index, 1, defectData.count);
                      } else {
                        if (
                          d.data[k] === 0 ||
                          d.data[k] === null ||
                          d.data[k] === undefined
                        ) {
                          d.data.splice(k, 1, 0);
                        }
                      }
                    }
                  }
                }
              }
            } else {
              let index = defectdomainName.findIndex(
                (img) => img === defectData.domainName
              );
              let priorityFind = priorityName.filter((col) => {
                return col === defectData.priority;
              });
              if (priorityFind.length === 0) {
                priorityName.push(defectData.priority);
                for (let j = 0; j <= index; j++) {
                  if (j === index) {
                    countData.push(defectData.count);
                  } else {
                    countData.splice(j, 1, 0);
                  }
                }
                let dataObj = {
                  label: defectData.priority,
                  data: countData,
                };
                data.push(dataObj);
              } else {
                for (let s = 0; s < data.length; s++) {
                  let d = data[s];
                  if (d.label === defectData.priority) {
                    for (let k = 0; k <= index; k++) {
                      if (k === index) {
                        let addCount = d.data[k] + defectData.count;
                        if (isNaN(addCount)) {
                          addCount = defectData.count;
                        }
                        d.data.splice(index, 1, addCount);
                      } else {
                        if (
                          d.data[k] === 0 ||
                          d.data[k] === null ||
                          d.data[k] === undefined
                        ) {
                          d.data.splice(k, 1, 0);
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      let priorityData = AddingColorsAndSeries(data);
      Bypriority = {
        labels: defectdomainName,
        datasets: priorityData,
      };
      return Bypriority;
    } else {
      let emppriority = { labels: [config.nodataavailable] };
      return emppriority;
    }
  } else {
    let emppriority = { labels: [config.nodataavailable] };
    return emppriority;
  }
};

/* Defect By Assignee */

export const InitialDefectByAssigneeCharts = (
  AssigneeDefects: any,
  summaryDatas: any,
  TestDefects: any,
  componentData: any
) => (dispatch: any, option: any) => {
  let Bypriority = {};
  if (AssigneeDefects !== undefined) {
    let Finalpriority = [];
    let defectassigneeName = [];
    let priorityName = [];
    let data = [];
    if (TestDefects.length !== 0) {
      TestDefects.forEach((testResults) => {
        AssigneeDefects.forEach((defect) => {
          if (componentData.epicName === "All Epic's") {
            if (
              testResults === defect.defectKey &&
              defect.projectReference === summaryDatas.ProjectReference &&
              defect.ReleaseName === summaryDatas.ReleaseName
            ) {
              Finalpriority.push(defect);
            }
          } else {
            if (
              testResults === defect.defectKey &&
              defect.projectReference === summaryDatas.ProjectReference &&
              defect.ReleaseName === summaryDatas.ReleaseName &&
              componentData.epicName === defect.epicName
            ) {
              Finalpriority.push(defect);
            }
          }
        });
      });
    } else {
      AssigneeDefects.forEach((defect) => {
        if (componentData.epicName === "All Epic's") {
          if (
            summaryDatas.ProjectReference === defect.projectReference &&
            defect.ReleaseName === summaryDatas.ReleaseName
          ) {
            ///dont change here
            Finalpriority.push(defect);
          }
        } else {
          if (
            summaryDatas.ProjectReference === defect.projectReference &&
            defect.ReleaseName === summaryDatas.ReleaseName &&
            componentData.epicName === defect.epicName
          ) {
            ///dont change here
            Finalpriority.push(defect);
          }
        }
      });
    }
    if (Finalpriority.length !== 0) {
      for (let i = 0; i < Finalpriority.length; i++) {
        let countData = [];
        let defectData = Finalpriority[i];
        if (
          defectData.assigneeName !== null &&
          defectData.assigneeName !== undefined
        ) {
          if (defectassigneeName.length === 0) {
            defectassigneeName.push(defectData.assigneeName);
            priorityName.push(defectData.priority);
            countData.push(defectData.count);
            let dataObj = {
              label: defectData.priority,
              data: countData,
            };
            data.push(dataObj);
          } else {
            let assigneeNameFind = defectassigneeName.filter((col) => {
              return col === defectData.assigneeName;
            });
            if (assigneeNameFind.length === 0) {
              defectassigneeName.push(defectData.assigneeName);
              let index = defectassigneeName.length - 1;
              let priorityFind = priorityName.filter((col) => {
                return col === defectData.priority;
              });
              if (priorityFind.length === 0) {
                priorityName.push(defectData.priority);
                for (let j = 0; j <= index; j++) {
                  if (j === index) {
                    countData.push(defectData.count);
                  } else {
                    countData.splice(j, 1, 0);
                  }
                }
                let dataObj = {
                  label: defectData.priority,
                  data: countData,
                };
                data.push(dataObj);
              } else {
                for (let s = 0; s < data.length; s++) {
                  let d = data[s];
                  if (d.label === defectData.priority) {
                    for (let k = 0; k <= index; k++) {
                      if (k === index) {
                        d.data.splice(index, 1, defectData.count);
                      } else {
                        if (
                          d.data[k] === 0 ||
                          d.data[k] === null ||
                          d.data[k] === undefined
                        ) {
                          d.data.splice(k, 1, 0);
                        }
                      }
                    }
                  }
                }
              }
            } else {
              let index = defectassigneeName.findIndex(
                (img) => img === defectData.assigneeName
              );
              let priorityFind = priorityName.filter((col) => {
                return col === defectData.priority;
              });
              if (priorityFind.length === 0) {
                priorityName.push(defectData.priority);
                for (let j = 0; j <= index; j++) {
                  if (j === index) {
                    countData.push(defectData.count);
                  } else {
                    countData.splice(j, 1, 0);
                  }
                }
                let dataObj = {
                  label: defectData.priority,
                  data: countData,
                };
                data.push(dataObj);
              } else {
                for (let s = 0; s < data.length; s++) {
                  let d = data[s];
                  if (d.label === defectData.priority) {
                    for (let k = 0; k <= index; k++) {
                      if (k === index) {
                        let addCount = d.data[k] + defectData.count;
                        if (isNaN(addCount)) {
                          addCount = defectData.count;
                        }
                        d.data.splice(index, 1, addCount);
                      } else {
                        if (
                          d.data[k] === 0 ||
                          d.data[k] === null ||
                          d.data[k] === undefined
                        ) {
                          d.data.splice(k, 1, 0);
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      let priorityData = AddingColorsAndSeries(data);
      let TableViewData = ConstructingDefectView(priorityData,defectassigneeName);
      Bypriority = {
        labels: defectassigneeName,
        datasets: priorityData,
        tableView:TableViewData
      };
      return Bypriority;
    } else {
      let emppriority = { labels: [config.nodataavailable],tableView:{} };
      return emppriority;
    }
  } else {
    let emppriority = { labels: [config.nodataavailable],tableView:{} };
    return emppriority;
  }
};

/*Defects by status by project level*/

export const StatusChartsByProjectLevel = (
  Defects: any,
  summaryDatas: any,
  componentData: any,
  TestDefectCollections: any,
  priorityType: any,
  defectType: any,
) => (dispatch: any, option: any) => {
  let Bypriority = {};
  if (Defects !== undefined) {
    let Finalpriority = [];
    let defectstatus = [];
    let priorityName = [];
    let data = [];
    if (TestDefectCollections.length !== 0) {
      TestDefectCollections.forEach((testResults) => {
        summaryDatas.forEach((project) => {
          Defects.forEach((defect) => {
            if (componentData.epicName === "All Epic's") {
              if (
                project.ProjectReference === defect.projectReference &&
                defect.ReleaseName === project.ReleaseName &&
                testResults === defect.defectKey
              ) {
                Finalpriority.push(defect);
              }
            } else {
              if (
                project.ProjectReference === defect.projectReference &&
                defect.ReleaseName === project.ReleaseName &&
                testResults === defect.defectKey &&
                componentData.epicName === defect.epicName
              ) {
                Finalpriority.push(defect);
              }
            }
          });
        });
      });
    } else {
      summaryDatas.forEach((project) => {
        Defects.forEach((defect) => {
          if (componentData.epicName === "All Epic's") {
            if (
              project.ProjectReference === defect.projectReference &&
              defect.ReleaseName === project.ReleaseName
            ) {
              Finalpriority.push(defect);
            }
          } else {
            if (
              project.ProjectReference === defect.projectReference &&
              componentData.epicName === defect.epicName &&
              defect.ReleaseName === project.ReleaseName
            ) {
              Finalpriority.push(defect);
            }
          }
        });
      });
    }
    Finalpriority = ConstructingDataBasedOnType(Finalpriority, priorityType);
    Finalpriority = ConstructingDataBasedOnCategory(Finalpriority, defectType);
    Finalpriority = DefectStatusOrder(Finalpriority);
    if (Finalpriority.length !== 0) {
      for (let i = 0; i < Finalpriority.length; i++) {
        let countData = [];
        let defectData = Finalpriority[i];
        if (defectstatus.length === 0) {
          defectstatus.push(defectData.status);
          priorityName.push(defectData.priority);
          countData.push(defectData.count);
          let dataObj = {
            label: defectData.priority,
            data: countData,
          };
          data.push(dataObj);
        } else {
          let statusFind = defectstatus.filter((col) => {
            return col === defectData.status;
          });
          if (statusFind.length === 0) {
            defectstatus.push(defectData.status);
            let index = defectstatus.length - 1;
            let priorityFind = priorityName.filter((col) => {
              return col === defectData.priority;
            });
            if (priorityFind.length === 0) {
              priorityName.push(defectData.priority);
              for (let j = 0; j <= index; j++) {
                if (j === index) {
                  countData.splice(j, 1, defectData.count);
                } else {
                  countData.splice(j, 1, 0);
                }
              }
              let dataObj = {
                label: defectData.priority,
                data: countData,
              };
              data.push(dataObj);
            } else {
              for (let s = 0; s < data.length; s++) {
                let d = data[s];
                if (d.label === defectData.priority) {
                  for (let k = 0; k <= index; k++) {
                    if (k === index) {
                      d.data.splice(index, 1, defectData.count);
                    } else {
                      if (
                        d.data[k] === 0 ||
                        d.data[k] === null ||
                        d.data[k] === undefined
                      ) {
                        d.data.splice(k, 1, 0);
                      }
                    }
                  }
                }
              }
            }
          } else {
            let index = defectstatus.findIndex(
              (img) => img === defectData.status
            );
            let priorityFind = priorityName.filter((col) => {
              return col === defectData.priority;
            });
            if (priorityFind.length === 0) {
              priorityName.push(defectData.priority);
              for (let j = 0; j <= index; j++) {
                if (j === index) {
                  countData.splice(j, 1, defectData.count);
                } else {
                  countData.splice(j, 1, 0);
                }
              }
              let dataObj = {
                label: defectData.priority,
                data: countData,
              };
              data.push(dataObj);
            } else {
              for (let s = 0; s < data.length; s++) {
                let d = data[s];
                if (d.label === defectData.priority) {
                  for (let k = 0; k <= index; k++) {
                    if (k === index) {
                      let addCount = d.data[k] + defectData.count;
                      if (isNaN(addCount)) {
                        addCount = defectData.count;
                      }
                      d.data.splice(index, 1, addCount);
                    } else {
                      if (
                        d.data[k] === 0 ||
                        d.data[k] === null ||
                        d.data[k] === undefined
                      ) {
                        d.data.splice(k, 1, 0);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      let priorityData = AddingColorsAndSeries(data);
      let TableViewData = ConstructingDefectView(priorityData,defectstatus);
      Bypriority = {
        labels: defectstatus,
        datasets: priorityData,
        tableView: TableViewData
      };
      return Bypriority;
    } else {
      let emppriority = { labels: [config.nodataavailable],tableView: {} };
      return emppriority;
    }
  } else {
    let emppriority = { labels: [config.nodataavailable],tableView: {} };
    return emppriority;
  }
};

/*Initial Defects By status*/

export const InitialStatusChartsByProjectLevel = (
  Defects: any,
  summaryDatas: any,
  TestDefectCollections: any,
  componentData: any
) => (dispatch: any, option: any) => {
  let Bypriority = {};
  if (Defects !== undefined) {
    let Finalpriority = [];
    let defectstatus = [];
    let priorityName = [];
    let data = [];
    if (TestDefectCollections.length !== 0) {
      TestDefectCollections.forEach((testResults) => {
        Defects.forEach((defect) => {
          if (componentData.epicName === "All Epic's") {
            if (
              summaryDatas.ProjectReference === defect.projectReference &&
              defect.ReleaseName === summaryDatas.ReleaseName &&
              testResults === defect.defectKey
            ) {
              Finalpriority.push(defect);
            }
          } else {
            if (
              summaryDatas.ProjectReference === defect.projectReference &&
              defect.ReleaseName === summaryDatas.ReleaseName &&
              testResults === defect.defectKey &&
              componentData.epicName === defect.epicName
            ) {
              Finalpriority.push(defect);
            }
          }
        });
      });
    } else {
      Defects.forEach((defect) => {
        if (componentData.epicName === "All Epic's") {
          if (
            summaryDatas.ProjectReference === defect.projectReference &&
            defect.ReleaseName === summaryDatas.ReleaseName
          ) {
            Finalpriority.push(defect);
          }
        } else {
          if (
            summaryDatas.ProjectReference === defect.projectReference &&
            componentData.epicName === defect.epicName &&
            defect.ReleaseName === summaryDatas.ReleaseName
          ) {
            Finalpriority.push(defect);
          }
        }
      });
    }
    Finalpriority = DefectStatusOrder(Finalpriority);
    if (Finalpriority.length !== 0) {
      for (let i = 0; i < Finalpriority.length; i++) {
        let countData = [];
        let defectData = Finalpriority[i];
        if (defectstatus.length === 0) {
          defectstatus.push(defectData.status);
          priorityName.push(defectData.priority);
          countData.push(defectData.count);
          let dataObj = {
            label: defectData.priority,
            data: countData,
          };
          data.push(dataObj);
        } else {
          let statusFind = defectstatus.filter((col) => {
            return col === defectData.status;
          });
          if (statusFind.length === 0) {
            defectstatus.push(defectData.status);
            let index = defectstatus.length - 1;
            let priorityFind = priorityName.filter((col) => {
              return col === defectData.priority;
            });
            if (priorityFind.length === 0) {
              priorityName.push(defectData.priority);
              for (let j = 0; j <= index; j++) {
                if (j === index) {
                  countData.splice(j, 1, defectData.count);
                } else {
                  countData.splice(j, 1, 0);
                }
              }
              let dataObj = {
                label: defectData.priority,
                data: countData,
              };
              data.push(dataObj);
            } else {
              for (let s = 0; s < data.length; s++) {
                let d = data[s];
                if (d.label === defectData.priority) {
                  for (let k = 0; k <= index; k++) {
                    if (k === index) {
                      d.data.splice(index, 1, defectData.count);
                    } else {
                      if (
                        d.data[k] === 0 ||
                        d.data[k] === null ||
                        d.data[k] === undefined
                      ) {
                        d.data.splice(k, 1, 0);
                      }
                    }
                  }
                }
              }
            }
          } else {
            let index = defectstatus.findIndex(
              (img) => img === defectData.status
            );
            let priorityFind = priorityName.filter((col) => {
              return col === defectData.priority;
            });
            if (priorityFind.length === 0) {
              priorityName.push(defectData.priority);
              for (let j = 0; j <= index; j++) {
                if (j === index) {
                  countData.splice(j, 1, defectData.count);
                } else {
                  countData.splice(j, 1, 0);
                }
              }
              let dataObj = {
                label: defectData.priority,
                data: countData,
              };
              data.push(dataObj);
            } else {
              for (let s = 0; s < data.length; s++) {
                let d = data[s];
                if (d.label === defectData.priority) {
                  for (let k = 0; k <= index; k++) {
                    if (k === index) {
                      let addCount = d.data[k] + defectData.count;
                      if (isNaN(addCount)) {
                        addCount = defectData.count;
                      }
                      d.data.splice(index, 1, addCount);
                    } else {
                      if (
                        d.data[k] === 0 ||
                        d.data[k] === null ||
                        d.data[k] === undefined
                      ) {
                        d.data.splice(k, 1, 0);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      let priorityData = AddingColorsAndSeries(data);
      let TableViewData = ConstructingDefectView(priorityData,defectstatus);
      Bypriority = {
        labels: defectstatus,
        datasets: priorityData,
        tableView: TableViewData
      };
      return Bypriority;
    } else {
      let emppriority = { labels: [config.nodataavailable] ,tableView: {}};
      return emppriority;
    }
  } else {
    let emppriority = { labels: [config.nodataavailable],tableView: {} };
    return emppriority;
  }
};

/* Adding Colors and Ordering Data */

function AddingColorsAndSeries(data) {
  let priorityData = [];
  if (data.length !== 0) {
    Defects_priority_order.forEach((order) => {
      let TempItem = data.filter((col) => {
        return col.label === order;
      });
      if (TempItem.length !== 0) {
        let color = Defects_colors.filter((col) => {
          return col.label === TempItem[0].label;
        });
        TempItem[0].backgroundColor = color[0].color;
        priorityData.push(TempItem[0]);
      }
    });
    return priorityData;
  }
}

/* Based on Type */

function ConstructingDataBasedOnType(data, type) {
  let priorityData = [];
  if (data.length !== 0) {
    if (type === config.allDefects) {
      priorityData = data;
    } else if (type === config.mvp) {
      Defects_MVP_Priority.forEach((order) => {
        let TempItem = data.filter((col) => {
          return col.priority === order;
        });
        if (TempItem.length !== 0) {
          priorityData = priorityData.concat(TempItem);
        }
      });
    } else {
      Defects_NOTMVP_Priority.forEach((order) => {
        let TempItem = data.filter((col) => {
          return col.priority === order;
        });
        if (TempItem.length !== 0) {
          priorityData = priorityData.concat(TempItem);
        }
      });
    }
  }
  return priorityData;
}

/* Based on Type */

function ConstructingDataBasedOnCategory(data, type) {
  let priorityData = [];
  if (data.length !== 0) {
    if (type === config.allDefects) {
      priorityData = data;
    }else if(type === config.storyDefects){
      priorityData = data.filter((col) => {
        return col.issueType === config.storyDefectsLabel;
      });
    }else {
      priorityData = data.filter((col) => {
        return col.status !== config.closeddefects;
      });
    }
  }
  return priorityData;
}

/*Status Order*/

function DefectStatusOrder(data) {
  let ordered_data = [];
  if (data.length !== 0) {
    Defect_status_order.forEach((order) => {
      data.forEach((defect) => {
        if (defect.status === order) {
          ordered_data.push(defect);
        }
      });
    });
  }
  return ordered_data;
}
