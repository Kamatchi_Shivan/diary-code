import {
    addSeriesMultilevel,
    RequirementSeriesOrder,
  } from "../common/TestExecutionCommonFunctions";
  
  /*High Charts*/
  
  export const RequirememtChart = (Requirements: any, Project: any) => {
    console.log("Requirement Data>>>>",Requirements)
    if (
      Requirements.length !== 0 &&
      Requirements !== undefined &&
      Requirements !== null &&
      Project.length !== 0 &&
      Project !== null &&
      Project !== undefined
    ) {
      let sortingOrder = [];
      let requirementData = [];
      Project.forEach((project) => {
        Requirements.forEach((requirements) => {
          if (
            project.ProjectReference === requirements.ProjectReference &&
            project.ReleaseName === requirements.ReleaseName
          ) {
            requirementData.push(requirements);
          }
        });
      });
      console.log(requirementData,1011)
      requirementData.forEach((sort) => {
        sortingOrder.push(sort.ProjectName);
      });
      if (sortingOrder.length !== 0) {
        sortingOrder = sortingOrder.filter(
          (value, index) => sortingOrder.indexOf(value) === index
        );
      }
  
      if (requirementData !== undefined && requirementData.length !== 0) {
        let series = [];
        let ReqCoverage = [];
  
        for (let i = 0; i < requirementData.length; i++) {
          let testExecutionData = requirementData[i];
          if (
            testExecutionData.ReqCoverage !== null &&
            testExecutionData.ReqCoverage !== undefined
          ) {
            if (ReqCoverage.length === 0) {
              ReqCoverage.push(testExecutionData.ReqCoverage);
              let data = [];
              let drillobject = {
                name: testExecutionData.ProjectName,
                y: testExecutionData.count,
              };
              data.push(drillobject);
              let seriesObj = {
                name: testExecutionData.ReqCoverage,
                //pointWidth: 50,
                data: data,
              };
  
              series.push(seriesObj);
            } else {
              let ReqCoverageFind = ReqCoverage.filter((col) => {
                return col === testExecutionData.ReqCoverage;
              });
              if (ReqCoverageFind.length === 0) {
                ReqCoverage.push(testExecutionData.ReqCoverage);
                let data = [];
                let drillobject = {
                  name: testExecutionData.ProjectName,
                  y: testExecutionData.count,
                };
                data.push(drillobject);
                let seriesObj = {
                  name: testExecutionData.ReqCoverage,
                  //pointWidth: 50,
                  data: data,
                };
  
                series.push(seriesObj);
              } else {
                let EditSerisObj = series.filter((col) => {
                  return col.name === testExecutionData.ReqCoverage;
                });
  
                let drillobject = {
                  name: testExecutionData.ProjectName,
                  y: testExecutionData.count,
                };
                EditSerisObj[0].data.push(drillobject);
              }
            }
          }
        }
        let Finalseries = [];
        series.forEach((s) => {
          s.data = addSeriesMultilevel(s.data, sortingOrder);
          Finalseries.push(s);
        });
        Finalseries = RequirementSeriesOrder(Finalseries);
        return Finalseries;
      } else {
        return [];
      }
    } else {
      return [];
    }
  };
  