import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import Splashscreen from "./splashscreen";
import {
  fetchLocalData,
} from "../../redux/actions/summary/summaryAction";

type MyProps = {
  fetchLocalData: any;
};
type MyState = {
  done: any;
};
class Loading extends Component<MyProps,MyState> {
  constructor(props){
     super(props)
     this.state = {
        done: undefined
     }
  }

  componentDidMount() {
    setTimeout(() => {
    this.setState({ done: true });
    }, 6000);
    }

  render() {
    return (
       <div>
          {!this.state.done ? (
             <Splashscreen/>
          ) : (
            <Redirect to="/home"/>
          )}
       </div>
    )
 }
}

const mapStateToProps = (state: any) => ({
});

export default connect(mapStateToProps, {
  fetchLocalData
})(Loading);
