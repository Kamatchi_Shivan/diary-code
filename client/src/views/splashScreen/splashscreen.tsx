import React, { Component } from "react";
import { Typography } from "antd";
import FadeIn from "react-fade-in";
import "./css/splash.css";
const { Text } = Typography;

export default class Splashscreen extends Component {
  render() {
    return (
      <div className="splash-backgroud">
        <div className = "splash-div">
          <FadeIn>
          <img
            src={require("../../assets/images/diary-logo-vector.png")}
            alt="banner"
            width="400"
            height="70"
            className="splash-img"
          />
          </FadeIn>
        </div>
        <div className="splash-text">
          <Text className="splash-text-col">
           Please use one of the latest Versions of MS Edge/Chrome/Firefox for the better experience
          </Text>
        </div>
        <img
            src={require("../../assets/images/loading.gif")}
            alt="banner"
            width="70"
            height="40"
            className="splash-img"
          />
      </div>
    );
  }
}
