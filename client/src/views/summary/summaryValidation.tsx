import { config } from '../../config/configs';

export const TestKeyMessageValidation = (data:any) => {
  if(data === null || data === undefined || data === ""){
    return config.nodatadisplay;
  }else{
    if(data.length <= 40){
      return data;
    }else{
      let result = data.replace(/([.*+^#$|{}[\]])/mg, "");
      let trimdata =  stripHtml(result);
      if(trimdata.length <= 40){
        return trimdata;
      }else{
        return trimdata.substring(0,40) +"...";
      }
    }
  }

}

export const TestKeyMessageValidationTooltip = (data:any) => {
  if(data === null || data === undefined || data === ""){
    return config.nodatadisplay;
  }else{
      return data.replace(/([.*+^#$|{}[\]])/mg, "");
  }
}

function stripHtml(html)
{
   let tmp = document.createElement("DIV");
   tmp.innerHTML = html;
   return tmp.textContent || tmp.innerText || "";
}
