import React, { Component } from "react";
import { Table, Button, Typography, message, Tooltip,BackTop } from "antd";
import { connect } from "react-redux";
import { Bar } from "react-chartjs-2";
import "chartjs-plugin-datalabels";
import {
  fetchDefectsByStatus,
  fetchDefectsByAssignee,
  fetchDefectsByComponent,
  fetchDefectsByDomain,
  fetchAllDefects,
} from "../../redux/actions/defects/defectsAction";
import {
  DefectByStatusCharts,
  DefectByAssigneeCharts,
  DefectByComponentCharts,
  DefectByDomainsCharts,
  StatusChartsByProjectLevel,
  InitialStatusChartsByProjectLevel,
} from "../chartFunctions/defectChartFunctions";
import { ReleasePageView } from "../../redux/actions/headers/headersAction";
import Global from "../../components/layout/Global/Global";
import {
  SummaryDefectsToggle,
  SummaryDefectsCategories,
  fetchEpicLinks
} from "../../redux/actions/summary/summaryAction";
import {
  fetchProjectDetails,
  fetchReleaseHistory,
  EmptyProjectDetails,
} from "../../redux/actions/projectview/projectViewAction";
import SmileyComponent from "../../components/layout/Global/SmileyComponent";
import Highcharts from "highcharts";
import drilldown from "highcharts/modules/drilldown.js";
import HighchartsReact from "highcharts-react-official";
import { TestExecutionChartData } from "../chartFunctions/stackedChartFunctions";
import { UserStoriesChartFunction } from "../chartFunctions/releaseviewUserStories";
import { config } from "../../config/configs";
import { Navigation } from "../dashboard/Dashboard";
import { EnableFilterSet } from "../header/header";
import Exporting from "highcharts/modules/exporting";
import Clock from "react-live-clock";
import {
  TestKeyMessageValidation,
  TestKeyMessageValidationTooltip,
} from "./summaryValidation";
import {RequirememtChart} from '../chartFunctions/stackedRequirementFunctions';
import {EditTwoTone } from "@ant-design/icons";
drilldown(Highcharts);
Exporting(Highcharts);

/* Global Variable Declaration Section */
const { Text } = Typography;
let projectTempData = [];
let EmptyHandler = [];
let AssigneeButton = "";
let StatusButton = "active";
let ComponentButton = "";
let ProjectButton = "";
let DomainButton = "";
let htmlFromDatabases = "";
let htmlFromDatabase = "";
let HistoryTestKeyMessage = "";
let testExecutionbtn = "active";
let openDefects = "";
let allDefects = "active";
let requirementbtn = "";
let independantLoad = true;
let independantLoadTestExecution = true;
let independantLoadStories = true;
let independantLoadRequirement = true;
let testKeycount = 0;
let riskkeycount = 0;
let storyDefects = "";
let component = {
  epicLink:"All Epic's",
  epicName: "All Epic's",
};

/* Date Formatting Function */
const ValidateDate = (data) => {
  if (data < 10) {
    data = "0" + data;
  }
  return data;
};

/* Declaring Props variable */
type MyProps = {
  DefectByStatusCharts: any;
  DefectByComponentCharts: any;
  DefectByDomainsCharts: any;
  DefectByAssigneeCharts: any;
  milestoneDatas: any;
  summaryDatas: any;
  summaryProjectDatas: any;
  fetchDefectsByStatus: any;
  fetchDefectsByAssignee: any;
  fetchDefectsByComponent: any;
  fetchDefectsByDomain: any;
  defectByStatus: any;
  projectDatas: any;
  defectByAssignee: any;
  defectByDomain: any;
  defectByComponent: any;
  fetchProjectDetails: any;
  fetchReleaseHistory: any;
  EmptyProjectDetails: any;
  allProjectsInitialRelease: any;
  fetchAllDefects: any;
  componentData: any;
  StatusChartsByProjectLevel: any;
  testexecutionbyrelease: any;
  userStoriesData: any;
  ReleasePageView: any;
  SummaryDefectsToggle: any;
  summaryToggle: any;
  SummaryDefectsCategories: any;
  defectToggle: any;
  InitialStatusChartsByProjectLevel: any;
  fetchEpicLinks:any;
  RequirememtChart: any;
  requirementsbyRelease: any;
};

/* Declaring State variable */
type MyState = {
  disabled: boolean;
  columns: any;
  DefectData: any;
  TestExecutionData: Object;
  series: any;
  userstoriesseries: any;
  requirementSeries: any;
  curTime: any;
  testExecution: boolean;
  defectCategory: any;
};

class Summary extends Component<MyProps, MyState> {
  componentWillMount() {
    independantLoad = true;
    independantLoadTestExecution = true;
    independantLoadStories = true;
    independantLoadRequirement = true;
    EnableFilterSet(false);
    this.props.fetchDefectsByStatus();
    this.props.fetchDefectsByAssignee();
    this.props.fetchDefectsByComponent();
    this.props.fetchDefectsByDomain();
    // this.props.fetchAllDefects();
    this.props.SummaryDefectsToggle(config.allDefects);
    this.props.SummaryDefectsCategories(config.allDefects);
    projectTempData = this.props.summaryDatas;
  }

  constructor(props: any) {
    super(props);
    const today = new Date(),
      date =
        today.getFullYear() +
        "-" +
        ValidateDate(today.getMonth() + 1) +
        "-" +
        ValidateDate(today.getDate());
    this.state = {
      disabled: true,
      series: [],
      userstoriesseries: [],
      testExecution: true,
      defectCategory: config.allDefects,
      requirementSeries:[],
      columns: [
        {
          title: "Project",
          dataIndex: "ProjectName",
          key: "ProjectName",
          render: (text, record) => {
            return (
              // eslint-disable-next-line jsx-a11y/anchor-is-valid
              <a
                className="tabletd"
                onClick={() => {
                  this.handleClick(record);
                }}
              >
                {text}
              </a>
            );
          },
          width: "45%",
        },
        {
          title: "Status",
          dataIndex: "Status_Go",
          key: "Status_Go",
          render: (text, record) => {
            return (
              <Tooltip title={record.status}>
                <Text className="tablesmile">{text}</Text>
              </Tooltip>
            );
          },
          width: "8%",
        },
        {
          title: "Test Key Message",
          dataIndex: "Test_Status",
          key: "Test_Status",
          render: (text, record) => {
            return (
              <Tooltip
                title={
                  <div
                    dangerouslySetInnerHTML={{
                      __html: record.KeyMessageTooltip,
                    }}
                  ></div>
                }
                placement="rightTop"
              >
                <Text>
                  <div dangerouslySetInnerHTML={{ __html: text }}></div>
                </Text>
              </Tooltip>
            );
          },
          width: "40%",
        },
      ],
      DefectData: {},
      TestExecutionData: {},
      curTime: date,
    };
  }

  /* Project Table - Project Name Routing */

  handleClick(e) {
    if (this.props.projectDatas.length !== 0) {
      let projectobj = this.props.projectDatas.filter((project) => {
        return e.projectReference === project.ProjectReference;
      });
      if (projectobj.length !== 0) {
        if (isNaN(e.projectReference)) {
          message.error("Invalid Project Reference", 1);
        } else {
          this.props.fetchProjectDetails(
            e.projectReference,
            projectobj[0].ReleaseName
          );
          this.props.fetchReleaseHistory(e.projectReference);
          this.props.fetchEpicLinks(projectobj[0].ReleaseName,e.projectReference)
          this.props.ReleasePageView(true);
          Global.history.push(
            "/projectview/" +
              projectobj[0].ReleaseName +
              "/" +
              e.projectReference
          );
        }
      }
    }
  }

  /* Open Defects - By Project Function */

  DefectByProject = (type, defectType) => {
    let DefectItem = {};
    this.props.SummaryDefectsToggle(type);
    this.props.SummaryDefectsCategories(defectType);
    DefectItem = this.props.DefectByStatusCharts(
      this.props.defectByStatus,
      this.props.summaryDatas,
      component,
      type,
      defectType
    );
    AssigneeButton = "";
    StatusButton = "";
    ComponentButton = "";
    ProjectButton = "active";
    DomainButton = "";
    if (defectType === config.allDefects) {
      openDefects = "";
      storyDefects = "";
      allDefects = "active";
    }else if(defectType === config.storyDefects){
      openDefects = "";
      storyDefects = "active";
      allDefects = "";
    }else{
      openDefects = "active";
      allDefects = "";
      storyDefects = "";
    }
    this.setState({
      DefectData: DefectItem,
    });
  };

  /* Open Defects - By Status Function */

  DefectByStatus = (type, defectType) => {
    let DefectItem = {};
    this.props.SummaryDefectsToggle(type);
    this.props.SummaryDefectsCategories(defectType);
    DefectItem = this.props.StatusChartsByProjectLevel(
      this.props.defectByStatus,
      this.props.summaryDatas,
      component,
      EmptyHandler,
      type,
      defectType
    );
    AssigneeButton = "";
    StatusButton = "active";
    ComponentButton = "";
    ProjectButton = "";
    DomainButton = "";
    if (defectType === config.allDefects) {
      openDefects = "";
      storyDefects = "";
      allDefects = "active";
    }else if(defectType === config.storyDefects){
      openDefects = "";
      storyDefects = "active";
      allDefects = "";
    }else{
      openDefects = "active";
      allDefects = "";
      storyDefects = "";
    }
    this.setState({
      DefectData: DefectItem,
    });
  };

  /* Open Defects - By Assignee Function */

  DefectByAssignee = (type, defectType) => {
    let DefectItem = {};
    this.props.SummaryDefectsToggle(type);
    this.props.SummaryDefectsCategories(defectType);
    DefectItem = this.props.DefectByAssigneeCharts(
      this.props.defectByAssignee,
      this.props.summaryDatas,
      component,
      EmptyHandler,
      type,
      defectType
    );
    AssigneeButton = "active";
    StatusButton = "";
    ComponentButton = "";
    ProjectButton = "";
    DomainButton = "";
    if (defectType === config.allDefects) {
      openDefects = "";
      storyDefects = "";
      allDefects = "active";
    }else if(defectType === config.storyDefects){
      openDefects = "";
      storyDefects = "active";
      allDefects = "";
    }else{
      openDefects = "active";
      allDefects = "";
      storyDefects = "";
    }
    this.setState({
      DefectData: DefectItem,
    });
  };

  /* Test Execution Chart - Data construction */
  TestExecutionChart = () => {
    testExecutionbtn = "active";
    requirementbtn = "";
    this.setState({
      series: TestExecutionChartData(
        this.props.testexecutionbyrelease,
        this.props.summaryDatas
      ),
      testExecution: true,
    });
  };

  /* User stories Chart - Data construction */
  UserStoriesChart = () => {
    this.setState({
      userstoriesseries: UserStoriesChartFunction(
        this.props.userStoriesData,
        this.props.summaryDatas
      ),

    });
  };

  /*Requirement Chart*/

  RequirementChartData = () => {
    testExecutionbtn = "";
    requirementbtn = "active";
    this.setState({
      requirementSeries: RequirememtChart(
        this.props.requirementsbyRelease,
        this.props.summaryDatas
      ),
      testExecution: false,
    });
  }

  /*Initial TestExecution*/
  InitialTestExecutionChart = () => {
    if (Navigation) {
      testExecutionbtn = "active";
      requirementbtn = "";
      this.setState({
        series: TestExecutionChartData(
          this.props.testexecutionbyrelease,
          // this.props.allProjectsInitialRelease
          this.props.summaryDatas
        ),
        testExecution: true,
      });
    } else {
      if (testExecutionbtn === "active") {
        this.setState({
          series: TestExecutionChartData(
            this.props.testexecutionbyrelease,
            this.props.summaryDatas
          ),
          testExecution: true,
        });
      } else {
        this.setState({
          requirementSeries: RequirememtChart(
            this.props.requirementsbyRelease,
            this.props.summaryDatas
          ),
          testExecution: false,
        });
      }
    }
  };

  /* Open Defects - Initial Loading Function*/
  InitialDefectByAssignee = () => {
    let DefectItem = {};
    if (Navigation) {
      // DefectItem = this.props.DefectByAssigneeCharts(
      //   this.props.defectByAssignee,
      //   // this.props.allProjectsInitialRelease,
      //   this.props.summaryDatas,
      //   component,
      //   EmptyHandler,
      //   config.allDefects,
      //   config.allDefects
      // );
      DefectItem = this.props.StatusChartsByProjectLevel(
        this.props.defectByStatus,
        this.props.summaryDatas,
        component,
        EmptyHandler,
        config.allDefects,
        config.allDefects
      );
      AssigneeButton = "";
      StatusButton = "active";
      ComponentButton = "";
      ProjectButton = "";
      DomainButton = "";
      this.setState({
        DefectData: DefectItem,
      });
    } else {
      openDefects = "";
      storyDefects = "";
      allDefects = "active";
      if (AssigneeButton === "active") {
        this.DefectByAssignee(config.allDefects, config.allDefects);
      } else if (StatusButton === "active") {
        this.DefectByStatus(config.allDefects, config.allDefects);
      } else if (ComponentButton === "active") {
        this.DefectByComponent(config.allDefects, config.allDefects);
      } else if (ProjectButton === "active") {
        this.DefectByProject(config.allDefects, config.allDefects);
      } else if (DomainButton === "active") {
        this.DefectByDomain(config.allDefects, config.allDefects);
      }
    }
  };

  /* Open Defects - By Domain Function */
  DefectByDomain = (type, defectType) => {
    let DefectItem = {};
    this.props.SummaryDefectsToggle(type);
    this.props.SummaryDefectsCategories(defectType);
    DefectItem = this.props.DefectByDomainsCharts(
      this.props.defectByDomain,
      this.props.summaryDatas,
      type,
      defectType
    );
    AssigneeButton = "";
    StatusButton = "";
    ComponentButton = "";
    ProjectButton = "";
    DomainButton = "active";
    if (defectType === config.allDefects) {
      openDefects = "";
      storyDefects = "";
      allDefects = "active";
    }else if(defectType === config.storyDefects){
      openDefects = "";
      storyDefects = "active";
      allDefects = "";
    }else{
      openDefects = "active";
      allDefects = "";
      storyDefects = "";
    }
    this.setState({
      DefectData: DefectItem,
    });
  };

  /* Open Defects - By Component Function */
  DefectByComponent = (type, defectType) => {
    let DefectItem = {};
    this.props.SummaryDefectsToggle(type);
    this.props.SummaryDefectsCategories(defectType);
    DefectItem = this.props.DefectByComponentCharts(
      this.props.defectByComponent,
      this.props.summaryDatas,
      component,
      EmptyHandler,
      type,
      defectType
    );
    AssigneeButton = "";
    StatusButton = "";
    ComponentButton = "active";
    ProjectButton = "";
    DomainButton = "";
    if (defectType === config.allDefects) {
      openDefects = "";
      storyDefects = "";
      allDefects = "active";
    }else if(defectType === config.storyDefects){
      openDefects = "";
      storyDefects = "active";
      allDefects = "";
    }else{
      openDefects = "active";
      allDefects = "";
      storyDefects = "";
    }
    this.setState({
      DefectData: DefectItem,
    });
  };

  /* Open Defects - Chart Options */
  options: Object = {
    plugins: {
      datalabels: {
        font: {
          size: 10,
        },
        color: "black",
        display: function (context) {
          return context.dataset.data[context.dataIndex] !== 0; // or >= 1 or ...
        },
      },
    },
    legend: {
      position: "bottom",
    },
    scales: {
      xAxes: [
        {
          stacked: true,
          gridLines: {
            display: false,
          },
          ticks: {
            callback: function (val) {
              return val !== null && val !== undefined && val.length > 30
                ? val.substring(0, 30) + "..."
                : val;
            },
          },
        },
      ],
      yAxes: [
        {
          stacked: true,
          gridLines: {
            display: false,
          },
          ticks: {
            beginAtZero: true,
            callback: function (value) {
              if (value % 1 === 0) {
                return value;
              }
            },
          },
        },
      ],
    },
  };

  /* Validating Milestone Data - Empty Handling */
  validatingMilestoneData = () => {
    if (
      this.props.milestoneDatas === undefined ||
      this.props.milestoneDatas === null ||
      Object.keys(this.props.milestoneDatas).length === 0
    ) {
      return false;
    } else {
      return true;
    }
  };

  validatingTestKeyMessage = () => {
    if (
      this.props.milestoneDatas.TestKeyMessage === undefined ||
      this.props.milestoneDatas.TestKeyMessage === null ||
      this.props.milestoneDatas.TestKeyMessage === "" ||
      Object.keys(this.props.milestoneDatas).length === 0
    ) {
      testKeycount = 0;
      htmlFromDatabases = "";
      return false;
    } else {
      htmlFromDatabases = this.props.milestoneDatas.TestKeyMessage;
      htmlFromDatabases = this.props.milestoneDatas.TestKeyMessage.replace(/([*+?^$|(){}[\]])/mg, "");
      testKeycount = htmlFromDatabases.length;
      return true;
    }
  };

  validatingTestKeyMessageHistory = () => {
    if (
      this.props.milestoneDatas.HistoryTestKeyMessage === undefined ||
      this.props.milestoneDatas.HistoryTestKeyMessage === null ||
      this.props.milestoneDatas.HistoryTestKeyMessage === "" ||
      Object.keys(this.props.milestoneDatas).length === 0
    ) {
      HistoryTestKeyMessage = "";
      return false;
    } else {
      HistoryTestKeyMessage = this.props.milestoneDatas.HistoryTestKeyMessage;
      HistoryTestKeyMessage = HistoryTestKeyMessage.replace(/([*+?^$@|(){}[\]])/mg, "");
      return true;
    }
  };

  validatingRiskAndIssues = () => {
    if (
      this.props.milestoneDatas.Risks_Issues === undefined ||
      this.props.milestoneDatas.Risks_Issues === null ||
      Object.keys(this.props.milestoneDatas).length === 0
    ) {
      riskkeycount = 0;
      htmlFromDatabase = "";
      return false;
    } else {
      htmlFromDatabase = this.props.milestoneDatas.Risks_Issues;
      riskkeycount = htmlFromDatabase.length;
      return true;
    }
  };

  /* Validating Summary Data - For Each time component rendering */
  validatingSummaryData = () => {
    let is_same =
      projectTempData.length === this.props.summaryDatas.length &&
      this.props.summaryDatas.every(function (element, index) {
        return element === projectTempData[index];
      });
    if (is_same) {
      return false;
    } else {
      projectTempData = this.props.summaryDatas;
      return true;
    }
  };

  /* Test Execution - Chart Options*/
  chartOptions() {
    return {
      series: this.state.series,
      chart: {
        type: "bar",
        style: {
          fontFamily: "serif",
        },
      },
      exporting: {
        enabled: false, // hide button
      },
      title: null,
      xAxis: { type: "category" },
      legend: { enabled: true },
      tooltip: {
        pointFormat:
          '<span style="color:{series.color}">{series.name}</span>: ({point.percentage:.0f}%)<br/>',
        shared: true,
      },
      plotOptions: {
        bar: { stacking: "percent" },
        series: {
          cursor: "pointer",
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            formatter: function () {
              return this.y !== 0
                ? Math.round(this.point.percentage) + "%"
                : "";
            },
            style: {
              textShadow: false,
              fontSize: "10px",
              color: "#000000",
              textOutline: false,
            },
          },
        },
      },
      yAxis: {
        reversedStacks: false,
        min: 0,
        labels: {
          style: {
            fontSize: "0.2em",
          },
          formatter: function () {
            return this.value; //+ "%";
          },
        },
        gridLineWidth: 0,
        title: {
          text: null,
        },
      },
      credits: {
        enabled: false,
      },
    };
  }

  /* User Stories - Chart Options*/
  userStories() {
    return {
      series: this.state.userstoriesseries,
      chart: {
        type: "bar",
        style: {
          fontFamily: "serif",
        },
      },
      exporting: {
        enabled: false, // hide button
      },
      title: null,
      xAxis: { type: "category" },
      legend: { enabled: true },
      tooltip: {
        pointFormat:
          '<span style="color:{series.color}">{series.name}</span>: ({point.percentage:.0f}%)<br/>',
        shared: true,
      },
      plotOptions: {
        bar: { stacking: "percent" },
        series: {
          cursor: "pointer",
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            formatter: function () {
              return this.y !== 0
                ? Math.round(this.point.percentage) + "%"
                : "";
            },
            style: {
              textShadow: false,
              fontSize: "10px",
              color: "#000000",
              textOutline: false,
            },
          },
        },
      },
      yAxis: {
        reversedStacks: false,
        min: 0,
        labels: {
          style: {
            fontSize: "0.2em",
          },
          formatter: function () {
            return this.value; //+ "%";
          },
        },
        gridLineWidth: 0,
        title: {
          text: null,
        },
      },
      credits: {
        enabled: false,
      },
    };
  }

  /*Requirement Chart Options */
  RequirementChartOptions() {
    return {
      series: this.state.requirementSeries,
      chart: {
        type: "bar",
        style: {
          fontFamily: "serif",
        },
      },
      exporting: {
        enabled: false, // hide button
      },
      title: null,
      xAxis: { type: "category" },
      legend: { enabled: true },
      tooltip: {
        pointFormat:
          '<span style="color:{series.color}">{series.name}</span>: ({point.percentage:.0f}%)<br/>',
        shared: true,
      },
      plotOptions: {
        bar: { stacking: "percent" },
        series: {
          cursor: "pointer",
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            formatter: function () {
              return this.y !== 0
                ? Math.round(this.point.percentage) + "%"
                : "";
            },
            style: {
              textShadow: false,
              fontSize: "10px",
              color: "#000000",
              textOutline: false,
            },
          },
        },
      },
      yAxis: {
        reversedStacks: false,
        min: 0,
        labels: {
          style: {
            fontSize: "0.2em",
          },
          formatter: function () {
            return this.value; //+ "%";
          },
        },
        gridLineWidth: 0,
        title: {
          text: null,
        },
      },
      credits: {
        enabled: false,
      },
    };
  }

  /* Defects MVP and not MVP*/
  DefectsTypes = (type) => {
    this.props.SummaryDefectsToggle(type);
    if (AssigneeButton === "active") {
      this.DefectByAssignee(type, this.props.defectToggle);
    } else if (StatusButton === "active") {
      this.DefectByStatus(type, this.props.defectToggle);
    } else if (ComponentButton === "active") {
      this.DefectByComponent(type, this.props.defectToggle);
    } else if (ProjectButton === "active") {
      this.DefectByProject(type, this.props.defectToggle);
    } else if (DomainButton === "active") {
      this.DefectByDomain(type, this.props.defectToggle);
    }
  };

  /* Defects Closed and Open */
  DefectsCategories = (type) => {
    if (type === config.allDefects) {
      openDefects = "";
      storyDefects = "";
      allDefects = "active";
    }else if(type === config.storyDefects){
      openDefects = "";
      storyDefects = "active";
      allDefects = "";
    }else{
      openDefects = "active";
      allDefects = "";
      storyDefects = "";
    }
    this.props.SummaryDefectsCategories(type);
    if (AssigneeButton === "active") {
      this.DefectByAssignee(this.props.summaryToggle, type);
    } else if (StatusButton === "active") {
      this.DefectByStatus(this.props.summaryToggle, type);
    } else if (ComponentButton === "active") {
      this.DefectByComponent(this.props.summaryToggle, type);
    } else if (ProjectButton === "active") {
      this.DefectByProject(this.props.summaryToggle, type);
    } else if (DomainButton === "active") {
      this.DefectByDomain(this.props.summaryToggle, type);
    }
  };

  /*Defects Naming Function */

  DefectsName = () => {
    if (this.props.summaryToggle !== "" && this.props.defectToggle !== "") {
      if (this.props.summaryToggle === config.allDefects) {
        return this.props.defectToggle.toUpperCase();
      } else {
        let data =
          this.props.defectToggle.toUpperCase() +
          "-" +
          this.props.summaryToggle.toUpperCase();
        return data;
      }
    } else {
      return config.allDefects.toUpperCase();
    }
  };

  render() {
    if (
      Object.keys(this.state.DefectData).length === 0
    ) {
      this.InitialDefectByAssignee();
      this.InitialTestExecutionChart();
      this.RequirementChartData();
    }
    if (this.validatingSummaryData()) {
      this.UserStoriesChart();
      if (testExecutionbtn === "active") {
        this.TestExecutionChart();
      } else {
        this.RequirementChartData();
      }
      if (AssigneeButton === "active") {
        this.DefectByAssignee(
          this.props.summaryToggle,
          this.props.defectToggle
        );
      } else if (StatusButton === "active") {
        this.DefectByStatus(this.props.summaryToggle, this.props.defectToggle);
      } else if (ComponentButton === "active") {
        this.DefectByComponent(
          this.props.summaryToggle,
          this.props.defectToggle
        );
      } else if (ProjectButton === "active") {
        this.DefectByProject(this.props.summaryToggle, this.props.defectToggle);
      } else if (DomainButton === "active") {
        this.DefectByDomain(this.props.summaryToggle, this.props.defectToggle);
      }
    }

    if (
      this.props.defectByStatus.length !== 0 &&
      independantLoad === true &&
      this.props.summaryDatas.length !== 0
    ) {
      independantLoad = false;
      this.InitialDefectByAssignee();
      this.InitialTestExecutionChart();
    }

    if (
      this.props.testexecutionbyrelease.length !== 0 &&
      independantLoadTestExecution === true
    ) {
      independantLoadTestExecution = false;
      this.InitialTestExecutionChart();
    }

    if (
      this.props.userStoriesData.length !== 0 &&
      independantLoadStories === true
    ) {
      independantLoadStories = false;
      this.UserStoriesChart();
    }

    if (
      this.props.requirementsbyRelease.length !== 0 &&
      independantLoadRequirement === true && requirementbtn === "active"
    ) {
      independantLoadRequirement = false;
      this.RequirementChartData();
    }


    /*Table data construction*/
    let mycolumns = this.state.columns.map((col, index) => ({
      ...col,
      onHeaderCell: (column) => ({
        width: column.width,
      }),
    }));
    const dataItems = [];
    if (this.props.summaryDatas) {
      let SerialNumber = 1;
      this.props.summaryDatas.forEach((element) => {
        let projectref = element.ProjectReference + " - " + element.ProjectName;
        dataItems.push({
          serialNumber: SerialNumber,
          ProjectName: projectref,
          Status_Go: (
            <div>
              {element.Test_Status === config.onTrack &&
              element.Test_Status !== null &&
              element.Test_Status !== undefined ? (
                <SmileyComponent
                  img={require("../../assets/images/thumbnail_norisks.png")}
                  desc="No Risks"
                ></SmileyComponent>
              ) : null}
              {element.Test_Status === config.notOnTrack &&
              element.Test_Status !== null &&
              element.Test_Status !== undefined ? (
                <SmileyComponent
                  img={require("../../assets/images/thumbnail_withissues.png")}
                  desc="With issues"
                ></SmileyComponent>
              ) : null}
              {element.Test_Status === config.atRisk &&
              element.Test_Status !== null &&
              element.Test_Status !== undefined ? (
                <SmileyComponent
                  img={require("../../assets/images/thumbnail_nodecision.png")}
                  desc="No Decision"
                ></SmileyComponent>
              ) : null}
              {element.Test_Status === config.notestStatus &&
              element.Test_Status !== null &&
              element.Test_Status !== undefined ? (
                <SmileyComponent
                  img={require("../../assets/images/noteststatus.png")}
                  desc="No Test Status"
                ></SmileyComponent>
              ) : null}
              {element.Test_Status === config.managementDecision &&
              element.Test_Status !== null &&
              element.Test_Status !== undefined ? (
                <SmileyComponent
                  img={require("../../assets/images/thumbnail_blocked.png")}
                  desc="Blocked"
                ></SmileyComponent>
              ) : null}{" "}
            </div>
          ),
          Test_Status: TestKeyMessageValidation(
            element.KeyRemarksTesting_Plain
          ),
          project_Name: projectref, //for routing
          status: element.Test_Status,
          projectReference: element.ProjectReference,
          // key: element.ProjectReference,
          KeyMessageTooltip: TestKeyMessageValidationTooltip(
            element.KeyRemarksTesting
          ), //for routing
        });
        SerialNumber++;
      });
    }
    return (
      <div className="container-flex page-wrapper">
        <div className="row content-row summary-scroll ">
          <div className="col-md-2 summary d-flex align-items-top remove-border">
            <div className="form-group">
              <div className="pro-info">
                <h5>
                  {this.state.curTime}{" "}
                  <Clock format="HH:mm:ss" interval={1000} ticking={true} />
                </h5>
              </div>
              <div className="pro-info pt-1">

                <div className="projct-status-info">
                <Text className="summary-text">
                  {this.validatingMilestoneData() ? (
                    <div>
                      {this.props.milestoneDatas.ReleaseTestStatus !== null &&
                      this.props.milestoneDatas.ReleaseTestStatus !== undefined
                        ? (this.props.milestoneDatas.ReleaseTestStatus =
                            this.props.milestoneDatas.ReleaseTestStatus.replace(
                              /(<([^>]+)>)/gi,
                              ""
                            ))
                        : "No Data Available"}
                    </div>
                  ) : (
                    "No Data Available"
                  )}
                </Text>
                {this.validatingMilestoneData() ? (
                  <div>
                    {this.props.milestoneDatas.ReleaseTestStatus ===
                      config.onTrack &&
                    this.props.milestoneDatas.ReleaseTestStatus !== null &&
                    this.props.milestoneDatas.ReleaseTestStatus !==
                      undefined ? (
                      <SmileyComponent
                        img={require("../../assets/images/thumbnail_norisks.png")}
                        desc="No Risks"
                      ></SmileyComponent>
                    ) : this.props.milestoneDatas.ReleaseTestStatus ===
                        config.notOnTrack &&
                      this.props.milestoneDatas.ReleaseTestStatus !== null &&
                      this.props.milestoneDatas.ReleaseTestStatus !==
                        undefined ? (
                      <SmileyComponent
                        img={require("../../assets/images/thumbnail_withissues.png")}
                        desc="With issues"
                      ></SmileyComponent>
                    ) : this.props.milestoneDatas.ReleaseTestStatus ===
                        config.atRiskRelease &&
                      this.props.milestoneDatas.ReleaseTestStatus !== null &&
                      this.props.milestoneDatas.ReleaseTestStatus !==
                        undefined ? (
                      <SmileyComponent
                        img={require("../../assets/images/thumbnail_nodecision.png")}
                        desc="No Decision"
                      ></SmileyComponent>)
                      : this.props.milestoneDatas.ReleaseTestStatus ===
                        config.notestStatus &&
                      this.props.milestoneDatas.ReleaseTestStatus !== null &&
                      this.props.milestoneDatas.ReleaseTestStatus !==
                        undefined ? (
                      <SmileyComponent
                      img={require("../../assets/images/noteststatus.png")}
                      desc="No Test Status"
                      ></SmileyComponent>
                    )
                     : null}
                  </div>
                ) : null}
                </div>
                <div className="login-btn">
                  <a
                    rel="noopener noreferrer"
                    href={config.adminPosibiltiy}
                    target="_blank"
                  >
                    <span>{config.datauploadsbtn}</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-5 summary d-flex align-items-top remove-border">
            <div className="form-group">
              <div className="pro-info">
                <div className="row">
                  <div className="col-md-8">
                    <h5>Test Key Message</h5>
                  </div>
                  <div className="col-md-4  project-data">
                    {this.validatingMilestoneData() &&
                    this.props.milestoneDatas.SP_ReleaseEditLink !== null &&
                    this.props.milestoneDatas.SP_ReleaseEditLink !==
                      undefined ? (
                      <a
                        rel="noopener noreferrer"
                        href={this.props.milestoneDatas.SP_ReleaseEditLink}
                        target="_blank"
                      >
                        <span>Edit <EditTwoTone className="edit-icon" /></span>
                      </a>
                    ) : (
                      <Text className="project-data" disabled={true}>
                        Edit
                      </Text>
                    )}
                  </div>
                </div>
                <Text className="summary-text">
                  {this.validatingTestKeyMessage() ? (
                    <div
                      dangerouslySetInnerHTML={{ __html: htmlFromDatabases }}
                    ></div>
                  ) : (
                    config.nodataavailable
                  )}
                </Text>
              </div>
            </div>
          </div>
          <div className="col-md-5 summary d-flex align-items-top">
            <div className="form-group">
              <div className="pro-info">
                <h5>Risks Issues</h5>
                <Text className="summary-text">
                  {this.validatingRiskAndIssues() ? (
                    <div
                      dangerouslySetInnerHTML={{ __html: htmlFromDatabase }}
                    ></div>
                  ) : (
                    "No Data Available"
                  )}
                </Text>
              </div>
            </div>
          </div>
        </div>
        {testKeycount > 700 || riskkeycount > 700 ? <br></br> : null}
        <div className="row pro-history">
          <div className="col-md-6 summary_table">
            <Table
              bordered
              columns={mycolumns}
              dataSource={dataItems}
              pagination={dataItems.length > 50 ? { pageSize: 50 } : false}
            />
          </div>
          <div className="col-md-6 cta-section pr-0">
            <div className="test-execution">
              <div className="col-md-12">
                <div className="Test_execuation">
                  {this.state.testExecution ? (
                    <h5>TEST EXECUTION</h5>
                  ) : (
                    <h5>REQUIREMENTS</h5>
                  )}
                  <div className="row cs-btn-group release-page-btn">
                    <div className="col-md-6 p-0">
                      <Button
                        type="primary"
                        className={testExecutionbtn}
                        onClick={this.TestExecutionChart}
                      >
                        Test Execution
                      </Button>
                    </div>
                    <div className="col-md-6 pl-1">
                      <Button
                        type="primary"
                        className={requirementbtn}
                        onClick={this.RequirementChartData}
                      >
                        Requirements
                      </Button>
                    </div>
                  </div>
                  {this.state.testExecution ? (
                    <HighchartsReact
                      highcharts={Highcharts}
                      options={this.chartOptions()}
                    />
                  ) : (
                    <HighchartsReact
                      highcharts={Highcharts}
                      options={this.RequirementChartOptions()}
                    />
                  )}
                </div>
              </div>
            </div>
                 <br></br>
                 <div className=" pr-0">
              <div className="col-md-12">
                <div className="Test_execuation">
                    <h5>USER STORIES</h5>
                    <HighchartsReact
                      highcharts={Highcharts}
                      options={this.userStories()}
                    />
                </div>
              </div>
            </div>


            <div className="open-defects absolute-center mt-3 open-defects">
              <div
                className="row cs-btn-group open-defects-btn-summary"
                id="btngroup"
              >
                <div className="col-md-2">
                  <Button
                    type="primary"
                    onClick={(event) =>
                      this.DefectByProject(config.allDefects, config.allDefects)
                    }
                    className={ProjectButton}
                  >
                    By Project
                  </Button>
                </div>
                <div className="col-md-2">
                  <Button
                    type="primary"
                    onClick={(event) =>
                      this.DefectByStatus(config.allDefects, config.allDefects)
                    }
                    className={StatusButton}
                  >
                    By Status
                  </Button>
                </div>
                <div className="col-md-2">
                  <Button
                    type="primary"
                    onClick={(event) =>
                      this.DefectByDomain(config.allDefects, config.allDefects)
                    }
                    className={DomainButton}
                  >
                    By Domain
                  </Button>
                </div>
                <div className="col-md-3">
                  <Button
                    type="primary"
                    onClick={(event) =>
                      this.DefectByAssignee(
                        config.allDefects,
                        config.allDefects
                      )
                    }
                    className={AssigneeButton}
                  >
                    By Assignee
                  </Button>
                </div>
                <div className="col-md-3">
                  <Button
                    type="primary"
                    onClick={(event) =>
                      this.DefectByComponent(
                        config.allDefects,
                        config.allDefects
                      )
                    }
                    className={ComponentButton}
                  >
                    By Epic
                  </Button>
                </div>
              </div>

              <div className="col-md-12">
                <div className="open_defeccts mt-1">
                  <h5>
                    {/* DEFECTS -{" "} */}
                    {this.props.summaryToggle !== undefined &&
                    this.props.defectToggle !== undefined
                      ? this.DefectsName()
                      : config.allDefects.toUpperCase()}
                  </h5>
                  <div className="row cs-btn-group">
                    <div className="col-md-12">
                    <div
                        style={{
                          float: "left",
                          marginLeft: "auto",
                          marginRight: "10px",
                        }}
                      >
                        <Button
                          className={allDefects}
                          onClick={(event: any) =>
                            this.DefectsCategories(config.allDefects)
                          }
                        >
                          All
                        </Button>
                      </div>
                      <div
                        style={{
                          float: "left",
                          marginLeft: "auto",
                          marginRight: "10px",
                        }}
                      >
                        <Button
                          className={openDefects}
                          onClick={(event: any) =>
                            this.DefectsCategories(config.openDefects)
                          }
                        >
                          Open
                        </Button>
                      </div>
                      <div style={{ float: "left", marginLeft: "auto", marginRight: "10px" }}>
                      <Button
                        className={storyDefects}
                        onClick={(event: any) =>
                          this.DefectsCategories(config.storyDefects)
                        }
                      >
                        Story Defects
                      </Button>
                    </div>
                      {this.props.summaryToggle !== config.notmvp ? (
                        <div
                          style={{
                            float: "right",
                            marginLeft: "auto",
                            marginRight: "10px",
                          }}
                        >
                          <Button
                            // className="active"
                            onClick={(event: any) =>
                              this.DefectsTypes(config.notmvp)
                            }
                          >
                            NOT MVP
                          </Button>
                        </div>
                      ) : null}
                      {this.props.summaryToggle !== config.mvp ? (
                        <div
                          style={{
                            float: "right",
                            marginLeft: "auto",
                            marginRight: "10px",
                          }}
                        >
                          <Button
                            // className="active"
                            onClick={(event: any) =>
                              this.DefectsTypes(config.mvp)
                            }
                          >
                            MVP
                          </Button>
                        </div>
                      ) : null}
                      {this.props.summaryToggle !== config.allDefects ? (
                        <div
                          style={{
                            float: "right",
                            marginLeft: "auto",
                            marginRight: "10px",
                          }}
                        >
                          <Button
                            // className="active"
                            onClick={(event: any) =>
                              this.DefectsTypes(config.allDefects)
                            }
                          >
                            TOTAL DEFECTS
                          </Button>
                        </div>
                      ) : null}
                    </div>
                  </div>
                  <Bar data={this.state.DefectData} options={this.options} />
                </div>
              </div>
            </div>

          </div>
          <div className="content-info-section col-md-12  testkeyHistory">
            <div className="row bg-sec">
              <div className="col-md-12">
                <h5 className="float-left">Test Key Message History</h5>
              </div>
              <div className="col-md-12">
                <p className="float-left">
                  {this.validatingTestKeyMessageHistory() ? (
                    <div
                      className="detail-status-alignment"
                      dangerouslySetInnerHTML={{
                        __html: HistoryTestKeyMessage,
                      }}
                    ></div>
                  ) : (
                    <div className="detail-status-alignment ml-0">
                      No Data Available
                    </div>
                  )}
                </p>
              </div>
            </div>
          </div>
        </div>
        <Tooltip title="Back to Top">
          <BackTop />
        </Tooltip>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  milestoneDatas: state.summaryData.milestoneDatas,
  summaryDatas: state.summaryData.summaryDatas,
  summaryProjectDatas: state.summaryData.summaryProjectDatas,
  defectByStatus: state.defectData.bystatus,
  defectByAssignee: state.defectData.byassignee,
  defectByDomain: state.defectData.bydomain,
  defectByComponent: state.defectData.bycomponent,
  projectDatas: state.summaryData.projectDatas,
  allProjectsInitialRelease: state.summaryData.allProjectsInitialRelease,
  componentData: state.projectView.componentData,
  testexecutionbyrelease: state.testExecution.testExecution,
  requirementsbyRelease: state.testExecution.testRequirement,
  userStoriesData: state.testExecution.userStories,
  summaryToggle: state.summaryData.summaryToggle,
  defectToggle: state.summaryData.defectToggle,
});

export default connect(mapStateToProps, {
  DefectByStatusCharts,
  DefectByComponentCharts,
  DefectByDomainsCharts,
  DefectByAssigneeCharts,
  StatusChartsByProjectLevel,
  fetchDefectsByAssignee,
  fetchDefectsByStatus,
  fetchDefectsByComponent,
  fetchDefectsByDomain,
  fetchProjectDetails,
  fetchReleaseHistory,
  EmptyProjectDetails,
  fetchAllDefects,
  ReleasePageView,
  SummaryDefectsToggle,
  SummaryDefectsCategories,
  InitialStatusChartsByProjectLevel,
  fetchEpicLinks,
  RequirememtChart
})(Summary);
