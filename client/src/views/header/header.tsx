import React, { Component } from "react";
import {
  fetchDomain,
  fetchRelease,
  fetchMilestones,
  fetchProject,
  fetchSummary,
  fetchSummaryProject,
  fetchAllProjectsInitialRelease,
  inmemoryfilteredsummarydata,
  inmemorydomainclear,
  fetchEpicLinks,
  fetchLocalData,
  SummaryDefectsToggle,
  ProjectClear
} from "../../redux/actions/summary/summaryAction";
import {
  fetchProjectDetails,
  fetchTestReports,
  fetchReleaseHistory,
  DefectsToggle,
  inmemoryComponent
} from "../../redux/actions/projectview/projectViewAction";
import { fetchCCB } from "../../redux/actions/detailview/detailviewAction";
import {
  fetchAllDefects,
  fetchDefectsByStatus,
  fetchDefectsByComponent,
  fetchDefectsByAssignee,
  fetchDefectsByRelease,
} from "../../redux/actions/defects/defectsAction";
import {
  fetchTestResults,
  fetchTestExecutionByReleaseName,
  fetchTestRequirement,
  fetchTestRequirementByRelease,
  fetchUserStories,
  fetchUserStoriesByRelease,
  fetchTestResultsByRelease
} from "../../redux/actions/testExecution/testExecutionAction";
import { connect } from "react-redux";
import { Form, Select, message } from "antd";
import Global from "../../components/layout/Global/Global";
import { Navigation, NavigatorSet } from "../dashboard/Dashboard";
import { RefreshProjectSelect } from "../../redux/actions/headers/headersAction";
import {
  pdfPreviewSelection,
  ReleasePageView,
} from "../../redux/actions/headers/headersAction";
import { config } from "../../config/configs";
export let EnableFilter = false;
export function EnableFilterSet(data) {
  EnableFilter = data;
}
/*Global Variable Declaration*/
let releasevariable = "";
let domainvariable = "";
let GlobalProjectVariable = "All Projects";
let releaseURl;
let statefix = 0;
const { Option } = Select;
let component = {
  epicLink:"All Epic's",
  epicName: "All Epic's"
};

/*Declaring Props Variable*/
type MyProps = {
  ccbdetails: any;
  fetchDefectsByRelease: any;
  domainDatas: any;
  fetchDomain: any;
  fetchRelease: any;
  releaseDatas: any;
  fetchMilestones: any;
  fetchProject: any;
  projectDatas: any;
  fetchSummary: any;
  fetchSummaryProject: any;
  fetchAllProjectsInitialRelease: any;
  fetchProjectDetails: any;
  fetchTestReports:any;
  fetchReleaseHistory: any;
  fetchTestResults: any;
  fetchCCB: any;
  summaryDatas: any;
  inmemoryfilteredsummarydata: any;
  milestoneDatas: any;
  inmemoryComponent: any;
  componentData: any;
  fetchTestExecutionByReleaseName: any;
  selecteddomain: any;
  inmemorydomainclear: any;
  filteredsummarydatas: any;
  fetchTestRequirement: any;
  RefreshProjectSelect: any;
  globalProjectName: any;
  fetchUserStories: any;
  fetchUserStoriesByRelease: any;
  projectURL: any;
  pdfPreviewSelection: any;
  notpdfView: any;
  pdfbase64: any;
  projectViewData: any;
  fetchAllDefects: any;
  fetchDefectsByStatus: any;
  fetchDefectsByComponent: any;
  fetchDefectsByAssignee: any;
  fromReleasePage: any;
  fetchLocalData: any;
  ReleasePageView: any;
  DefectsToggle: any;
  SummaryDefectsToggle: any;
  fetchTestResultsByRelease: any;
  currentRelease: any;
  ProjectClear: any;
  allprojects: any;
  fetchTestRequirementByRelease: any;
  fetchEpicLinks:any;
  epicLinks:any;
};

/*Declaring State Variable*/
type MyState = {
  projectvariable: string;
  componentvariable: string;
  disable: boolean;
  visible: boolean;
  componentdisable: boolean;
  domaindisable: boolean;
};

class Header extends Component<MyProps, MyState> {
  constructor(props: any) {
    super(props);
    this.state = {
      projectvariable: "",
      componentvariable: "",
      disable: false,
      visible: true,
      componentdisable: false,
      domaindisable: false,
    };
  }
  componentWillMount() {
    let myLoc = JSON.stringify(window.location.pathname);
    let finalstr = myLoc.replace(/"/g, "");
    releaseURl = finalstr.substring(finalstr.lastIndexOf("/") + 1);
    statefix = 0;
    this.props.fetchAllDefects();
    if (Navigation === true) {
      this.props.fetchDomain();
      if (releaseURl === "home") {
        if (this.props.releaseDatas[0] !== undefined) {
          this.props.fetchProject(this.props.currentRelease.ReleaseName);
          this.props.fetchCCB(this.props.currentRelease.ReleaseName);
        }
      } else if (myLoc.includes("projectview") || myLoc.includes("pdfview")) {
        let split = myLoc.split("/");
        let projectRelease = split[2];
        this.props.fetchProjectDetails(releaseURl, projectRelease);
        this.props.fetchTestReports(releaseURl, projectRelease);
        this.props.fetchReleaseHistory(releaseURl);
        this.props.fetchProject(projectRelease);
        console.log("releaseURl, projectRelease",releaseURl, projectRelease)
        this.props.fetchEpicLinks(projectRelease,releaseURl);
        this.props.fetchCCB(projectRelease);
        this.props.fetchDefectsByStatus();
        this.props.fetchDefectsByComponent();
        this.props.fetchDefectsByAssignee();
        this.props.fetchAllDefects();
        this.props.fetchTestResultsByRelease(projectRelease);
        this.props.fetchTestRequirementByRelease(projectRelease);
        // this.props.fetchUserStoriesByRelease(projectRelease);
        this.props.fetchDefectsByRelease(projectRelease);
      }
      this.props.fetchRelease();
      //this.props.fetchTestRequirement();
      this.props.fetchUserStories();
      this.props.fetchTestExecutionByReleaseName();
      // this.props.fetchTestResults();
      this.props.fetchAllProjectsInitialRelease();
    }
  }

  /*Updating Project URL - Project View Page*/
  updateURL = (projectRef) => {
    let projectobj = this.props.projectDatas.filter((project) => {
      return projectRef === project.ProjectReference;
    });
    if (window.history.pushState) {
      var newurl =
        window.location.protocol +
        "//" +
        window.location.host +
        "/projectview/" +
        projectobj[0].ReleaseName +
        "/" +
        projectRef;
      window.history.pushState({ path: newurl }, "", newurl);
    }
  };

  /*Setting header component Values dynamically*/
  setVariable = () => {
    let myLoc = JSON.stringify(window.location.pathname);
    if (!isNaN(releaseURl) && !(releaseURl === "")) {
      if (this.props.projectDatas.length !== 0) {
        let split = myLoc.split("/");
        let projectRelease = split[2];
        let projectobj = this.props.allprojects.filter((project) => {
          return (
            releaseURl === project.ProjectReference &&
            projectRelease === project.ReleaseName
          );
        });
        releasevariable = projectRelease;
        if (
          projectobj.length !== 0 &&
          statefix === 0 &&
          this.props.domainDatas.length !== 0 &&
          this.props.epicLinks.length !== 0
        ) {
          // if(this.state.componentvariable === ""){
          //   this.props.inmemoryComponent(this.props.epicLinks[0]);
          // }
          // this.props.inmemorydomainclear(this.props.domainDatas[0]);
          if (Array.isArray(this.props.selecteddomain)) {
            domainvariable = this.props.selecteddomain[0].DomainName;
          } else {
            domainvariable = this.props.domainDatas[0].DomainName;
          }
          this.props.fetchProject(projectRelease);
          this.setState({
            projectvariable:
              projectobj[0].ProjectReference + "_" + projectobj[0].ProjectName,
            disable: true,
            visible: false,
            domaindisable: true,
          });
          if (Object.keys(this.props.componentData).length === 0) {
            this.setState({
              componentvariable: this.props.epicLinks[0].epicLink,
              componentdisable: false,
            });
          } else {
            this.setState({
              componentvariable: this.props.componentData.epicName,
              componentdisable: false,
            });
          }
          this.props.fetchTestResultsByRelease(projectRelease);
          statefix++;
        }
      }
    } else {
      if (Navigation) {
        if (
          this.props.releaseDatas[0] !== undefined &&
          statefix === 0 &&
          this.props.domainDatas.length !== 0
        ) {
          releasevariable = this.props.currentRelease.ReleaseName;
          domainvariable = this.props.domainDatas[0].DomainName;
          this.props.inmemorydomainclear(this.props.domainDatas[0]);
          if (this.props.epicLinks.length !== 0) {
            this.props.inmemoryComponent(this.props.epicLinks[0]);
          } else {
            this.props.inmemoryComponent(component);
          }
          this.setState({
            projectvariable: "All Projects",
            disable: false,
            visible: true,
            componentvariable: "All Epic's",
            componentdisable: true,
            domaindisable: false,
          });
          statefix++;
        }
      } else if (this.props.fromReleasePage && Navigation === false) {
        if (
          this.props.releaseDatas[0] !== undefined &&
          statefix === 0 &&
          this.props.domainDatas.length !== 0
        ) {
          if (this.props.epicLinks.length !== 0) {
            this.props.inmemoryComponent(this.props.epicLinks[0]);
          } else {
            this.props.inmemoryComponent(component);
          }
          this.setState({
            projectvariable: GlobalProjectVariable,
            disable: false,
            visible: true,
            componentvariable: "All Epic's",
            componentdisable: true,
            domaindisable: false,
          });
          statefix++;
        }
      } else if (this.props.fromReleasePage && Navigation) {
        if (
          this.props.releaseDatas[0] !== undefined &&
          statefix === 0 &&
          this.props.domainDatas.length !== 0
        ) {
          if (this.props.epicLinks.length !== 0) {
            this.props.inmemoryComponent(this.props.epicLinks[0]);
          } else {
            this.props.inmemoryComponent(component);
          }
          this.props.fetchLocalData();
          releasevariable = this.props.currentRelease.ReleaseName;
          this.setState({
            projectvariable: GlobalProjectVariable,
            disable: false,
            visible: true,
            componentvariable: "All Epic's",
            componentdisable: true,
            domaindisable: false,
          });
          statefix++;
        }
      } else {
        if (
          this.props.releaseDatas[0] !== undefined &&
          statefix === 0 &&
          this.props.domainDatas.length !== 0
        ) {
          if (this.props.epicLinks.length !== 0) {
            this.props.inmemoryComponent(this.props.epicLinks[0]);
          } else {
            this.props.inmemoryComponent(component);
          }
          if (!EnableFilter) {
            this.props.fetchLocalData();
            releasevariable = this.props.currentRelease.ReleaseName;
          }
          this.setState({
            projectvariable: GlobalProjectVariable,
            disable: false,
            visible: true,
            componentvariable: "All Epic's",
            componentdisable: true,
            domaindisable: false,
          });
          statefix++;
        }
      }
    }
  };

  /*Navigation to defalut component */
  setdefaultComponent = () => {
    if (releaseURl === "homsetdefaultComponente") {
      if (this.props.epicLinks.length !== 0) {
        this.props.inmemoryComponent(this.props.epicLinks[0]);
      } else {
        this.props.inmemoryComponent(component);
      }
    }
  };

  /*Setting release dropdown - onclick */
  releaseSelect = (params: any) => {
    releasevariable = params;
    this.props.fetchTestResultsByRelease(params);
    this.props.fetchDefectsByRelease(params);
    this.props.fetchTestRequirementByRelease(params);
    this.props.ProjectClear();
    this.props.RefreshProjectSelect("All Projects");
    this.props.SummaryDefectsToggle(config.allDefects);
    if (!isNaN(releaseURl)) {
      Global.history.push("/home");
      EnableFilterSet(true);
      GlobalProjectVariable = "All Projects";
    }
    if (this.props.projectDatas.length !== 0) {
      this.setState({
        projectvariable: this.props.projectDatas[0].ProjectName,
      });
    } else {
      this.setState({ projectvariable: "All Projects" });
    }
    this.props.fetchMilestones(params);
    this.props.fetchProject(params);
    this.props.fetchSummary(params);
    this.props.fetchCCB(params);
    if (this.props.domainDatas.length !== 0) {
      this.props.inmemorydomainclear(this.props.domainDatas[0]);
      domainvariable = this.props.domainDatas[0].DomainName;
    }
  };

  /*Setting project dropdown - onclick */
  projectSelect = (params: any) => {
    let splits = params.split("-");
    params = splits[0];
    this.props.DefectsToggle(config.allDefects);
    if (isNaN(params)) {
      message.error("Invalid Project Reference", 1);
    } else {
      if (this.props.projectDatas.length !== 0) {
        if (params !== "0") {
          let projectobj = this.props.projectDatas.filter((project) => {
            return params === project.ProjectReference;
          });
          this.props.fetchEpicLinks(releasevariable,projectobj[0].ProjectReference);
          if (!isNaN(releaseURl)) {
            this.props.fetchProjectDetails(
              projectobj[0].ProjectReference,
              projectobj[0].ReleaseName
            );
            this.props.fetchTestReports(
              projectobj[0].ProjectReference,
              projectobj[0].ReleaseName
            );
            this.props.inmemoryComponent(this.props.epicLinks[0]);
            this.props.fetchReleaseHistory(projectobj[0].ProjectReference);
            GlobalProjectVariable =
              projectobj[0].ProjectReference +
              " - " +
              projectobj[0].ProjectName;
            this.setState({
              projectvariable:
                projectobj[0].ProjectReference +
                " - " +
                projectobj[0].ProjectName,
              componentvariable: "All Epic's",
            });
            this.updateURL(projectobj[0].ProjectReference);
          } else {
            this.props.RefreshProjectSelect(
              projectobj[0].ProjectReference + " - " + projectobj[0].ProjectName
            );
            this.props.inmemoryComponent(component);
            GlobalProjectVariable =
              projectobj[0].ProjectReference +
              " - " +
              projectobj[0].ProjectName;
            this.setState({
              projectvariable:
                projectobj[0].ProjectReference +
                " - " +
                projectobj[0].ProjectName,
              componentvariable: "All Epic's",
            });
            this.props.fetchSummaryProject(params, projectobj[0].ReleaseName);
          }
        } else {
          if (isNaN(releaseURl)) {
            this.props.RefreshProjectSelect("All Projects");
            GlobalProjectVariable = "All Projects";
            this.props.inmemoryComponent(this.props.epicLinks[0]);
            this.setState({
              projectvariable: "All Projects",
              componentvariable: "All Epic's",
            });
            this.props.fetchSummary(this.props.milestoneDatas.ReleaseName);
          }
        }
      }
    }
  };

  /*Setting domain dropdown - onclick */
  domainSelect = (params: any) => {
    let filteredproject = [];
    let selecteddomain = [];
    domainvariable = params;
    if (params === "All Domains") {
      filteredproject = this.props.filteredsummarydatas;
      selecteddomain.push(this.props.domainDatas[0]);
      this.props.inmemoryfilteredsummarydata(filteredproject, selecteddomain);
    } else {
      this.props.ccbdetails.forEach((element) => {
        if (params === element.Domain) {
          this.props.filteredsummarydatas.forEach((data) => {
            if (element.ProjectReference === data.ProjectReference) {
              filteredproject.push(data);
            }
          });
        }
      });
      selecteddomain = this.props.domainDatas.filter((domain) => {
        return params === domain.DomainName;
      });
      this.props.inmemoryfilteredsummarydata(filteredproject, selecteddomain);
    }
  };

  /*Setting component dropdown - onclick */
  componentSelect = (params: any) => {
    if (this.props.epicLinks.length !== 0) {
      this.props.epicLinks.forEach((data) => {
        if (data.epicName === params) {
          this.props.inmemoryComponent(data);
          this.setState({ componentvariable: data.epicName });
        }
      });
    }
  };

  /*Navigation to home */
  redirecthome = (e: any) => {
    e.preventDefault();
    NavigatorSet();
    this.props.pdfPreviewSelection(true);
    Global.history.push("/home");
  };

  /*Navigation to project view */
  redirectProjectView = () => {
    this.props.pdfPreviewSelection(true);
    Global.history.push("/projectview/" + this.props.projectURL);
  };

  render() {
    if (releasevariable === "" || releaseURl !== null) {
      this.setVariable();
      // this.setdefaultComponent();
      GlobalProjectVariable = this.props.globalProjectName;
    }
    if (releasevariable === undefined) {
      releasevariable = this.props.currentRelease.ReleaseName;
    }
    return (
      <Form className="header header-main-menu">
        <div className="form-group-info header-logo">
          <img
            src={require("../../assets/images/pandv-v3.svg")}
            alt="banner"
            className="left-sec pv_logo"
            onClick={this.redirecthome}
          />
          {/* <div className="header-text-div">
            <label className="right-sec header-txt">
              <b>
                <Text className="exp-text">® Expleo </Text>Dynamic Integrated
                Automated Reporting for You
              </b>P-and-V-logo-v2
            </label>
          </div> */}
        </div>
        <div className="header-dp-menu">
          <div className="form-group-info">
            <label
              className="left-sec"
              style={{
                display: this.props.notpdfView ? "block" : "none",
              }}
            >
              Domain
            </label>
            <div className="right-sec">
              <Select
                showSearch
                placeholder="All Domains"
                onChange={this.domainSelect}
                disabled={this.state.domaindisable}
                value={domainvariable}
                style={{
                  display: this.props.notpdfView ? "block" : "none"
                }}
              >
                {this.props.domainDatas.map((obj) => (
                  <Option key={obj.Id} value={obj.DomainName}>
                    {obj.DomainName}
                  </Option>
                ))}
              </Select>
            </div>
          </div>
          <div className="form-group-info">
            <label
              className="left-sec"
              style={{
                display: this.props.notpdfView ? "block" : "none",
              }}
            >
              Epic
            </label>
            <div className="right-sec">
              <Select
                showSearch
                placeholder="All Epic's"
                onChange={this.componentSelect}
                value={this.state.componentvariable}
                disabled={this.state.componentdisable}
                style={{
                  display: this.props.notpdfView ? "block" : "none",
                }}
              >
                {this.props.epicLinks.map((obj) => (
                  <Option key={obj.epicLink} value={obj.epicName}>
                    {obj.epicName}
                  </Option>
                ))}
              </Select>
            </div>
          </div>

          <div className="form-group-info">
            <label
              className="left-sec"
              style={{
                display: this.props.notpdfView ? "block" : "none",
              }}
            >
              Release
            </label>
            <div className="right-sec">
              <Select
                onChange={this.releaseSelect}
                showSearch
                // placeholder="Select Release"
                // disabled={this.state.disable}
                value={releasevariable}
                style={{
                  display: this.props.notpdfView ? "block" : "none",
                }}
              >
                {this.props.releaseDatas.map((obj) => (
                  <Option key={obj.ReleaseName} value={obj.ReleaseName}>
                    {obj.ReleaseName}
                  </Option>
                ))}
              </Select>
            </div>
          </div>
          <div className="form-group-info">
            <label
              className="left-sec"
              style={{
                display: this.props.notpdfView ? "block" : "none",
              }}
            >
              Project
            </label>
            <div className="right-sec">
              <Select
                onChange={this.projectSelect}
                showSearch
                value={this.state.projectvariable}
                style={{
                  display: this.props.notpdfView ? "block" : "none",
                }}
              //disabled = {this.state.disable}
              >
                {this.props.projectDatas.map((obj) => (
                  <Option
                    key={obj.ProjectReference}
                    value={obj.ProjectReference + "-" + obj.ProjectName}
                  >
                    {obj.ProjectName !== "All Projects"
                      ? obj.ProjectReference + " - " + obj.ProjectName
                      : obj.ProjectName}
                  </Option>
                ))}
              </Select>
            </div>
          </div>
        </div>
      </Form>
    );
  }
}

const mapStateToProps = (state) => ({
  domainDatas: state.summaryData.domainDatas,
  // releaseDatas: state.summaryData.releaseDatas,
  releaseDatas: state.summaryData.releaseDatasfilter,
  projectDatas: state.summaryData.projectDatas,
  ccbdetails: state.detailview.ccbdetails,
  summaryDatas: state.summaryData.summaryDatas,
  milestoneDatas: state.summaryData.milestoneDatas,
  selecteddomain: state.summaryData.selecteddomain,
  filteredsummarydatas: state.summaryData.filteredsummarydatas,
  globalProjectName: state.headerView.projectName,
  projectURL: state.projectView.projectURL,
  notpdfView: state.headerView.isnotpdfview,
  fromReleasePage: state.headerView.fromReleasePage,
  pdfbase64: state.testExecution.pdfData,
  projectViewData: state.projectView.projectDatas,
  currentRelease: state.summaryData.currentRelease,
  allprojects: state.summaryData.allProjectsInitialRelease,
  epicLinks: state.defectData.epicLink,
  componentData: state.projectView.componentData,
});

const WrappedCreateHeader = Form.create({ name: "header" })(Header);

export default connect(mapStateToProps, {
  fetchDomain,
  fetchRelease,
  fetchMilestones,
  fetchProject,
  fetchSummary,
  fetchSummaryProject,
  fetchAllProjectsInitialRelease,
  fetchProjectDetails,
  fetchTestReports,
  fetchReleaseHistory,
  fetchTestResults,
  fetchCCB,
  inmemoryfilteredsummarydata,
  fetchTestExecutionByReleaseName,
  fetchTestRequirement,
  fetchTestRequirementByRelease,
  inmemorydomainclear,
  RefreshProjectSelect,
  fetchUserStories,
  pdfPreviewSelection,
  fetchAllDefects,
  fetchDefectsByStatus,
  fetchDefectsByComponent,
  fetchDefectsByAssignee,
  fetchLocalData,
  ReleasePageView,
  DefectsToggle,
  SummaryDefectsToggle,
  fetchTestResultsByRelease,
  ProjectClear,
  fetchUserStoriesByRelease,
  fetchDefectsByRelease,
  fetchEpicLinks,
  inmemoryComponent
})(WrappedCreateHeader);
