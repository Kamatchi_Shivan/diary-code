import React, { Component } from "react";
import {
  Table,
  Form,
  Button,
  Menu,
  Dropdown,
  Typography,
  Modal,
  Checkbox,
  Select,
  BackTop,
  Tooltip,
} from "antd";
import { connect } from "react-redux";
import { Bar } from "react-chartjs-2";
import {
  DefectByStatusCharts,
  DefectByAssigneeCharts,
  DefectByComponentCharts,
  DefectByDomainsCharts,
  InitialDefectByAssigneeCharts,
  StatusChartsByProjectLevel,
  InitialStatusChartsByProjectLevel
} from "../chartFunctions/defectChartFunctions";
import { TestExecutionStatus } from "../chartFunctions/testExecutionFunction";
import { getChartData, FilterValidation, FilterValidationForBussinessDomain } from "../chartFunctions/testExecutionFunction";
import * as folderset from "../chartFunctions/TestExecutionFolderSet";
import {
  inmemoryProjectURLStore,
  inmemoryDetailedView,
  storeDocType,
  storeGateVersion,
  DefectsToggle,
  ProjectDefectsCategories
} from "../../redux/actions/projectview/projectViewAction";
import { pdfPreviewSelection } from "../../redux/actions/headers/headersAction";
import Highcharts from "highcharts";
import drilldown from "highcharts/modules/drilldown.js";
import HighchartsReact from "highcharts-react-official";
import { RequirementCoverageMainFunction } from "../chartFunctions/RequirementCoverageFunctions";
import { UserStoriesFunction } from "../chartFunctions/UserStoriesFunction";
import SmileyComponent from "../../components/layout/Global/SmileyComponent";
import { config } from "../../config/configs";
import { NavigatorSet } from "../dashboard/Dashboard";
import Clock from "react-live-clock";
import Global from "../../components/layout/Global/Global";
import { FilePdfOutlined, DownCircleOutlined, EditTwoTone } from "@ant-design/icons";
import { clearRequirementLevels, clearTestLevels, FormingLinks, FormingUserStoryLink } from "./clearChartLevels";
import Pdf from '../../assets/images/avatars/Pdf.gif';
import { UnorderedListOutlined, BarChartOutlined } from '@ant-design/icons';
import { AllEpics, EpicSecondLevelMainLevel } from '../chartFunctions/EpicLinkChartFunctions';
import { BussinessDomain, usergroup } from './staticFilters'
drilldown(Highcharts);

/*Global Variable Declaration section*/
const { Text } = Typography;
const { Option } = Select;
const initialDataset = "projects";
let releaseURl = "";
let projectRelease = "";
let TestDefectCollections = [];
let captureDataset = "";
let captureDatasetlevel3 = "";
let captureDatasetlevel4 = "";
let captureDatasetlevel5 = "";
let captureDatasetlevel6 = "";
let captureDatasetlevel7 = "";
let captureDatasetlevel8 = "";
let captureDatasetlevel9 = "";
let captureDatasetlevel10 = "";
let captureDatasetlevel11 = "";
let RequirementcaptureDataset = "";
let RequirementcaptureDatasetlevel3 = "";
let RequirementcaptureDatasetlevel4 = "";
let RequirementcaptureDatasetlevel5 = "";
let RequirementcaptureDatasetlevel6 = "";
let RequirementcaptureDatasetlevel7 = "";
let RequirementcaptureDatasetlevel8 = "";
let RequirementcaptureDatasetlevel9 = "";
let RequirementcaptureDatasetlevel10 = "";
let RequirementcaptureDatasetlevel11 = "";
let count = 1;
let bcount = 1;
let storycount = 1;
let TempComponentData;
let changeComponentData;
let TempReleaseData;
let report;
let htmlFromDatabase = "";
let riskIssues = "";
let DetailedTestStatus = "";
let HistoryTestKeyMessage = "";
let independantLoad = true;
let independantLoadTestExecution = true;
let independantLoadRequirment = true;
let projectDatavalidation = true;
let openDefects = "";
let allDefects = "active";
let storyDefects = "";
let independantLoadStory = true;

/* Date Formatting Function */
const ValidateDate = (data) => {
  if (data < 10) {
    data = "0" + data;
  }
  return data;
};

/*Declaring Props Variable*/
type MyProps = {
  DefectByStatusCharts: any;
  DefectByComponentCharts: any;
  DefectByDomainsCharts: any;
  DefectByAssigneeCharts: any;
  defectByAssignee: any;
  defectByDomain: any;
  defectByComponent: any;
  defectByStatus: any;
  milestoneDatas: any;
  summaryDatas: any;
  summaryProjectDatas: any;
  projectViewData: any;
  InitialDefectByAssigneeCharts: any;
  projectDatas: any;
  allDefectData: any;
  releaseHistory: any;
  StatusChartsByProjectLevel: any;
  TestExecutionStatus: any;
  testResults: any;
  getChartData: any;
  componentData: any;
  requirementData: any;
  userStoriesData: any;
  inmemoryProjectURLStore: any;
  pdfPreviewSelection: any;
  inmemoryDetailedView: any;
  detailView: any;
  storeDocType: any;
  storeGateVersion: any;
  DefectsToggle: any;
  projectToggle: any;
  ProjectDefectsCategories: any;
  projectdefectToggle: any;
  InitialStatusChartsByProjectLevel: any;
  epicLinks: any;
};

/*Declaring State Variable*/
type MyState = {
  columns: any;
  DefectData: any;
  TestExecutionData: Object;
  curTime: any;
  previousPage: any;
  currentPage: any;
  series: any;
  AssigneeButton: any;
  StatusButton: any;
  ComponentButton: any;
  RequirementButton: any;
  TestCaseButton: any;
  TestExectionButton: any;
  UserStoriesButton: any;
  RequirementCoverageBack: any;
  UserStoriesBack: any;
  isTestCase: any;
  isUserStories: any;
  Inpercent: any;
  InpercentButton: any;
  InNumbermbers: any;
  InNumbermbersButton: any;
  HideInPercentNumbers: any;
  modalVisible: boolean;
  storyType: any;
  testType: any;
  tableView: boolean;
  tableViewData: any;
  storyTableData: any;
  userStoryTable: boolean;
  chartcolor: boolean;
  tablecolor: boolean;
  UserStorychartcolor: boolean;
  UserStorytablecolor: boolean;
  userStorybtn: any;
  UserstoriesInNumbermbersButton: any;
  UserstoriesInpercentButton: any;
  UserStoryHideInPercentNumbers: any;
  UserStoriesInpercent: boolean;
  UserstoriesSeries: any;
  epicbtn: any;
  staticFilter: any;
  staticData: any;
  businessDomain: any;
  allEpicsbtn: any;
};

class ProjectView extends Component<MyProps, MyState> {
  table;
  componentWillMount() {
    NavigatorSet();
    independantLoad = true;
    independantLoadTestExecution = true;
    independantLoadRequirment = true;
    let myLoc = JSON.stringify(window.location.pathname);
    let finalstr = myLoc.replace(/"/g, "");
    releaseURl = finalstr.substring(finalstr.lastIndexOf("/") + 1);
    let split = myLoc.split("/");
    projectRelease = split[2];
    TempComponentData = this.props.componentData;
    changeComponentData = this.props.componentData;
    TempReleaseData = this.props.projectViewData;
    this.props.inmemoryProjectURLStore("");
    this.props.pdfPreviewSelection(true);
    this.props.inmemoryDetailedView(false);
    this.props.storeDocType("TSR");
    this.props.storeGateVersion("G1");
    this.props.DefectsToggle(config.allDefects);
    this.props.ProjectDefectsCategories(config.allDefects);
    if (window.location.href.includes("projectview")) {
      window.addEventListener("popstate", (event) => {
        Global.history.push("/home");
      });
    }
  }
  constructor(props: any) {
    super(props);
    this.handleBack = this.handleBack.bind(this);
    this.requirementBack = this.requirementBack.bind(this);
    this.storiesBack = this.storiesBack.bind(this);
    const today = new Date(),
      date =
        today.getFullYear() +
        "-" +
        ValidateDate(today.getMonth() + 1) +
        "-" +
        ValidateDate(today.getDate());
    this.state = {
      Inpercent: false,
      userStorybtn: config.stories,
      InpercentButton: "",
      InNumbermbersButton: "active",
      InNumbermbers: false,
      HideInPercentNumbers: true,
      AssigneeButton: "active",
      StatusButton: "",
      ComponentButton: "",
      previousPage: "",
      RequirementButton: "",
      UserStoriesButton: "",
      allEpicsbtn: "",
      TestCaseButton: "active",
      TestExectionButton: false,
      RequirementCoverageBack: false,
      UserStoriesBack: false,
      isTestCase: true,
      isUserStories: false,
      currentPage: initialDataset,
      series: [],
      UserstoriesSeries: [],
      tableView: false,
      userStoryTable: false,
      tableViewData: {},
      storyTableData: {},
      modalVisible: false,
      storyType: config.allstories,
      testType: config.alltests,
      chartcolor: true,
      tablecolor: false,
      UserStorychartcolor: true,
      UserStorytablecolor: false,
      UserstoriesInNumbermbersButton: "active",
      UserstoriesInpercentButton: "",
      epicbtn: "",
      UserStoryHideInPercentNumbers: true,
      UserStoriesInpercent: false,
      staticFilter: false,
      businessDomain: false,
      staticData: config.all,
      columns: [
        {
          title: "CCB",
          dataIndex: "CCB_Closure_Date",
          key: "CCB_Closure_Date",
          render: (text, record) => {
            return <Text className="tabledata">{text}</Text>;
          },
          width: "6%",
        },
        {
          title: "Startup",
          dataIndex: "CCB_STARTUP_Decision",
          key: "CCB_STARTUP_Decision",
          width: "10%",
        },
        {
          title: "Startup Remarks",
          dataIndex: "Ready2StartTestRemarks",
          key: "Ready2StartTestRemarks",
          width: "10%",
        },
        {
          title: "Ready For Sprint",
          dataIndex: "CCB_Ready4SPRINT_Decision",
          key: "CCB_Ready4SPRINT_Decision",
        },
        {
          title: "Ready For Sprint Remarks",
          dataIndex: "Ready4SprintTestRemarks",
          key: "Ready4SprintTestRemarks",
          width: "10%",
        },
        {
          title: "Ready For UAT",
          dataIndex: "CCB_Ready4UAT_Decision",
          key: "CCB_Ready4UAT_Decision",
        },
        {
          title: "Ready For UAT Remarks",
          dataIndex: "Ready4UATTestRemarks",
          key: "Ready4UATTestRemarks",
          width: "10%",
        },
        {
          title: "Ready For PROD",
          dataIndex: "CCB_Ready4PROD_Decision",
          key: "CCB_Ready4PROD_Decision",
        },
        {
          title: "Ready For PROD Remarks",
          dataIndex: "Ready4ProdTestRemarks",
          key: "Ready4ProdTestRemarks",
          width: "10%",
        },
        {
          title: "Closure",
          dataIndex: "CCB_CLOSURE_Decision",
          key: "CCB_CLOSURE_Decision",
        },
        {
          title: "Closure Remarks",
          dataIndex: "CCB_CLOSURE_Remarks",
          key: "CCB_CLOSURE_Remarks",
          width: "10%",
        },
      ],
      DefectData: {},
      TestExecutionData: {},
      curTime: date,
    };
  }

  /*Open Defects - By Status Function*/

  DefectByStatus = (type, defectType) => {
    let DefectItem: any;
    this.props.DefectsToggle(type);
    this.props.ProjectDefectsCategories(defectType);
    DefectItem = this.props.StatusChartsByProjectLevel(
      this.props.defectByStatus,
      this.props.projectViewData,
      this.props.componentData,
      TestDefectCollections,
      type,
      defectType
    );
    if (defectType === config.allDefects) {
      openDefects = "";
      storyDefects = "";
      allDefects = "active";
    } else if (defectType === config.storyDefects) {
      openDefects = "";
      storyDefects = "active";
      allDefects = "";
    } else {
      openDefects = "active";
      allDefects = "";
      storyDefects = "";
    }
    this.setState({
      DefectData: DefectItem,
      tableViewData: DefectItem.tableView,
      AssigneeButton: "",
      StatusButton: "active",
      ComponentButton: "",
    });
  };

  /*Open Defects - By Assignee Function*/

  DefectByAssignee = (type, defectType) => {
    let DefectItem: any;
    this.props.DefectsToggle(type);
    this.props.ProjectDefectsCategories(defectType);
    DefectItem = this.props.DefectByAssigneeCharts(
      this.props.defectByAssignee,
      this.props.projectViewData,
      this.props.componentData,
      TestDefectCollections,
      type,
      defectType
    );
    if (defectType === config.allDefects) {
      openDefects = "";
      storyDefects = "";
      allDefects = "active";
    } else if (defectType === config.storyDefects) {
      openDefects = "";
      storyDefects = "active";
      allDefects = "";
    } else {
      openDefects = "active";
      allDefects = "";
      storyDefects = "";
    }
    this.setState({
      DefectData: DefectItem,
      tableViewData: DefectItem.tableView,
      AssigneeButton: "active",
      StatusButton: "",
      ComponentButton: "",
    });
  };

  /*Open Defects - Intial Defects Loading*/

  InitialDefectByAssignee = () => {
    this.props.DefectsToggle(config.allDefects);
    if (this.props.projectDatas.length !== 0) {
      TestDefectCollections = [];
      let projectobj = this.props.projectDatas.filter((project) => {
        return (
          releaseURl === project.ProjectReference &&
          projectRelease === project.ReleaseName
        );
      });
      let DefectItem: any;
      if (projectobj.length !== 0) {
        DefectItem = this.props.InitialStatusChartsByProjectLevel(
          this.props.defectByStatus,
          projectobj[0],
          TestDefectCollections,
          this.props.componentData
        );
        this.setState({
          DefectData: DefectItem,
          tableViewData: DefectItem.tableView,
          AssigneeButton: "",
          StatusButton: "active",
          ComponentButton: "",
          isUserStories: false,
          isTestCase: true,
        });
      }
    }
  };

  /*Open Defects - By Component Function*/

  DefectByComponent = (type, defectType) => {
    let DefectItem: any;
    this.props.DefectsToggle(type);
    this.props.ProjectDefectsCategories(defectType);
    DefectItem = this.props.DefectByComponentCharts(
      this.props.defectByComponent,
      this.props.projectViewData,
      this.props.componentData,
      TestDefectCollections,
      type,
      defectType
    );
    if (defectType === config.allDefects) {
      openDefects = "";
      storyDefects = "";
      allDefects = "active";
    } else if (defectType === config.storyDefects) {
      openDefects = "";
      storyDefects = "active";
      allDefects = "";
    } else {
      openDefects = "active";
      allDefects = "";
      storyDefects = "";
    }
    this.setState({
      DefectData: DefectItem,
      tableViewData: DefectItem.tableView,
      AssigneeButton: "",
      StatusButton: "",
      ComponentButton: "active",
    });
  };

  /*Defect Chart options */

  options: Object = {
    maintainAspectRatio: false,
    responsive: true,
    plugins: {
      datalabels: {
        color: "black",
        display: function (context) {
          return context.dataset.data[context.dataIndex] !== 0; // or >= 1 or ...
        },
      },
    },
    legend: {
      position: "bottom",
    },
    scales: {
      xAxes: [
        {
          stacked: true,
          gridLines: {
            display: false,
          },
          ticks: {
            callback: function (val) {
              return val !== null && val !== undefined && val.length > 30
                ? val.substring(0, 30) + "..."
                : val;
            },
          },
        },
      ],
      yAxes: [
        {
          stacked: true,
          gridLines: {
            display: false,
          },
          ticks: {
            beginAtZero: true,
            callback: function (value) {
              if (value % 1 === 0) {
                return value;
              }
            },
          },
        },
      ],
    },
  };

  /*Validating Project Data for Empty Handling*/

  validatingProjectData = () => {
    if (
      this.props.projectViewData.length === 0 ||
      this.props.projectViewData[0] === undefined ||
      this.props.projectViewData[0] === null ||
      Object.keys(this.props.projectViewData[0]).length === 0
    ) {
      return false;
    } else {
      return true;
    }
  };

  validatingProjectData_KeyRemarksTesting = () => {
    if (this.props.projectViewData.length !== 0) {
      if (
        this.props.projectViewData[0].KeyRemarksTesting === undefined ||
        this.props.projectViewData[0].KeyRemarksTesting === null ||
        this.props.projectViewData[0].KeyRemarksTesting === "" ||
        Object.keys(this.props.projectViewData[0]).length === 0
      ) {
        htmlFromDatabase = "";
        return false;
      } else {
        htmlFromDatabase = this.props.projectViewData[0].KeyRemarksTesting;
        htmlFromDatabase = this.props.projectViewData[0].KeyRemarksTesting.replace(/([*+^$|{}[\]])/mg, "");
        return true;
      }
    } else {
      htmlFromDatabase = "";
      return false;
    }
  };

  validatingProjectData_TestKeyMessageHistory = () => {
    if (this.props.projectViewData.length !== 0) {
      if (
        this.props.projectViewData[0].HistoryTestKeyMessage === null ||
        this.props.projectViewData[0].HistoryTestKeyMessage === undefined ||
        this.props.projectViewData[0].HistoryTestKeyMessage === "" ||
        Object.keys(this.props.projectViewData[0]).length === 0
      ) {
        HistoryTestKeyMessage = "";
        return false;
      } else {
        HistoryTestKeyMessage = this.props.projectViewData[0].HistoryTestKeyMessage;
        HistoryTestKeyMessage = this.props.projectViewData[0].HistoryTestKeyMessage.replace(/([*+@^$|{}[\]])/mg, "");
        return true;
      }
    } else {
      HistoryTestKeyMessage = "";
      return false;
    }
  };

  validatingProjectData_OpenRiskAndIssues = () => {
    if (
      this.props.projectViewData.length === 0 ||
      this.props.projectViewData[0].Open_Risks_Issues === undefined ||
      this.props.projectViewData[0].Open_Risks_Issues === null ||
      Object.keys(this.props.projectViewData[0].Open_Risks_Issues).length === 0 ||
      this.props.projectViewData[0].Open_Risks_Issues === ""
    ) {
      riskIssues = "";
      return false;
    } else {
      riskIssues = this.props.projectViewData[0].Open_Risks_Issues;
      return true;
    }
  };

  validatingProjectData_DetailedTestStatus = () => {
    if (
      this.props.projectViewData.length === 0 ||
      this.props.projectViewData[0].DetailedTestStatus === undefined ||
      this.props.projectViewData[0].DetailedTestStatus === null ||
      Object.keys(this.props.projectViewData[0].DetailedTestStatus).length === 0 ||
      this.props.projectViewData[0].DetailedTestStatus === ""
    ) {
      DetailedTestStatus = "";
      return false;
    } else {
      DetailedTestStatus = this.props.projectViewData[0].DetailedTestStatus;
      return true;
    }
  };

  /*Matching Project Reference for component rendering*/

  matchingUrl = () => {
    let myLoc = JSON.stringify(window.location.pathname);
    let finalstr = myLoc.replace(/"/g, "");
    let newUrl = finalstr.substring(finalstr.lastIndexOf("/") + 1);
    if (releaseURl !== newUrl) {
      releaseURl = newUrl;
      return true;
    } else {
      return false;
    }
  };

  /*Test Execution - Test Case Function*/

  TestExecutionStatus = (type, staticFilters) => {
    count = 1;
    TestDefectCollections = [];
    if (this.state.AssigneeButton === "active") {
      this.DefectByAssignee(
        this.props.projectToggle,
        this.props.projectdefectToggle
      );
    } else if (this.state.StatusButton === "active") {
      this.DefectByStatus(
        this.props.projectToggle,
        this.props.projectdefectToggle
      );
    } else {
      this.DefectByComponent(
        this.props.projectToggle,
        this.props.projectdefectToggle
      );
    }
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (projectobj.length !== 0) {
      let TestItem = this.props.getChartData(
        initialDataset,
        this.props.testResults,
        projectobj[0],
        count,
        this.props.allDefectData,
        captureDataset,
        type,
        this.props.componentData,
        staticFilters
      );
      if (TestItem.TestData.length > 0) {
        this.setState({
          HideInPercentNumbers: true,
        });
      } else {
        if (type !== config.alltests) {
          this.setState({
            HideInPercentNumbers: true,
          });
        } else {
          this.setState({
            HideInPercentNumbers: false,
          });
        }
      }
      this.setState({
        series: TestItem.TestData,
        RequirementButton: "",
        TestCaseButton: "active",
        isTestCase: true,
        RequirementCoverageBack: false,
        TestExectionButton: false,
        InpercentButton: "",
        InNumbermbersButton: "active",
        Inpercent: false,
        InNumbermbers: false,
        // staticData: config.all
      });
    }
  };

  /*Initial Test Execution - Test Case Function*/

  InitialTestExecutionStatus = () => {
    count = 1;
    TestDefectCollections = [];
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (projectobj.length !== 0) {
      let TestItem = this.props.getChartData(
        initialDataset,
        this.props.testResults,
        projectobj[0],
        count,
        this.props.allDefectData,
        captureDataset,
        config.alltests,
        this.props.componentData,
        config.all
      );
      this.setState({
        staticFilter: (FilterValidation(this.props.testResults,
          projectobj[0])),
        businessDomain: (FilterValidationForBussinessDomain(this.props.testResults,
          projectobj[0]))
      })
      if (TestItem.TestData.length > 0) {
        this.setState({
          HideInPercentNumbers: true,
        });
      } else {
        this.setState({
          HideInPercentNumbers: false,
        });
      }
      this.setState({
        series: TestItem.TestData,
        RequirementButton: "",
        TestCaseButton: "active",
        // UserStoriesButton: "",
        // isUserStories: false,
        isTestCase: true,
        RequirementCoverageBack: false,
        TestExectionButton: false,
        // UserStoriesBack: false,
        InpercentButton: "",
        InNumbermbersButton: "active",
        Inpercent: false,
        InNumbermbers: false,
        // storyType: config.allstories,
        testType: config.alltests,
        staticData: config.all
        // userStoryTable: false,
        // UserStorychartcolor: true, UserStorytablecolor: false,
        // userStorybtn: config.stories
      });
    }
  };

  /* Test Execution - Requirement Coverage chart options */

  RequirementCoverage = () => {
    count = 1;
    TestDefectCollections = [];
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (this.state.AssigneeButton === "active") {
      this.DefectByAssignee(
        this.props.projectToggle,
        this.props.projectdefectToggle
      );
    } else if (this.state.StatusButton === "active") {
      this.DefectByStatus(
        this.props.projectToggle,
        this.props.projectdefectToggle
      );
    } else {
      this.DefectByComponent(
        this.props.projectToggle,
        this.props.projectdefectToggle
      );
    }
    if (projectobj.length !== 0) {
      let RequirementCoverageData = RequirementCoverageMainFunction(
        this.props.requirementData,
        projectobj[0],
        count,
        initialDataset,
        RequirementcaptureDataset,
        this.props.componentData
      );
      if (
        Object.keys(RequirementCoverageData.RequirementCoverage).length !== 0
      ) {
        this.setState({
          HideInPercentNumbers: true,
        });
      } else {
        this.setState({
          HideInPercentNumbers: false,
        });
      }
      this.setState({
        series: RequirementCoverageData.RequirementCoverage,
        isTestCase: false,
        // isUserStories: false,
        RequirementButton: "active",
        TestCaseButton: "",
        // UserStoriesButton: "",
        RequirementCoverageBack: false,
        TestExectionButton: false,
        // UserStoriesBack: false,
        InpercentButton: "",
        InNumbermbersButton: "active",
        Inpercent: false,
        InNumbermbers: false,
        // storyType: config.allstories,
        testType: config.alltests,
        staticData: config.all
        // userStoryTable: false,
        // UserStorychartcolor: true, UserStorytablecolor: false,
        // userStorybtn: config.stories
      });
    }
  };

  /* Test Execution - User Stories chart options */

  UserStories = (type, epicView) => {
    storycount = 1;
    let projectobj = this.props.projectDatas.filter((project) => {
      return releaseURl === project.ProjectReference;
    });
    if (projectobj.length !== 0) {
      let UserStoriesData: any = UserStoriesFunction(
        this.props.userStoriesData,
        projectobj[0],
        storycount,
        initialDataset,
        type,
        this.props.componentData,
        epicView
      );
      if (UserStoriesData.UserStories.length !== 0) {
        if (Object.keys(UserStoriesData.UserStories.Finalseries).length !== 0) {
          this.setState({
            UserStoryHideInPercentNumbers: true,
          });
        } else {
          if (type !== config.allstories) {
            this.setState({
              UserStoryHideInPercentNumbers: true,
            });
          } else {
            this.setState({
              UserStoryHideInPercentNumbers: false,
            });
          }
        }

      }
      this.setState({
        UserstoriesSeries: UserStoriesData.UserStories.Finalseries,
        storyTableData: UserStoriesData.UserStories.tableViewData,
        isUserStories: true,
        UserStoriesBack: false,
        InpercentButton: "",
        InNumbermbersButton: "active",
        Inpercent: false,
        InNumbermbers: false,
        testType: config.alltests,
        UserStorychartcolor: true,
        UserStorytablecolor: false,
      });
      if (epicView === config.epics) {
        this.setState({ epicbtn: "active", UserStoriesButton: "", allEpicsbtn: "" });
      } else {
        this.setState({ epicbtn: "", UserStoriesButton: "active", allEpicsbtn: "" });
      }

    }
  };

  /* Handling Startup Date*/

  handleStartUp = () => {
    if (
      this.props.projectViewData.length !== 0 &&
      this.props.projectViewData !== undefined
    ) {
      if (
        this.props.projectViewData[0].CCB_STARTUP_Date === null ||
        this.props.projectViewData[0].CCB_STARTUP_Date === undefined
      ) {
        return false;
      } else {
        return true;
      }
    } else {
      return false;
    }
  };

  /* Test Exection Chart - Back option for Testcase*/

  handleBack() {
    const pp = this.state.previousPage;
    let Temp;
    if (pp === "") {
      return;
    }
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (projectobj.length !== 0) {
      if (count > 3 && count <= 13) {
        Temp = captureDataset;
        bcount = 2;
        if (count === 5) {
          bcount = 3;
          Temp = captureDatasetlevel3;
        } else if (count === 6) {
          bcount = 4;
          Temp = captureDatasetlevel4;
        } else if (count === 7) {
          bcount = 5;
          Temp = captureDatasetlevel5;
        } else if (count === 8) {
          bcount = 6;
          Temp = captureDatasetlevel6;
        } else if (count === 9) {
          bcount = 7;
          Temp = captureDatasetlevel7;
        } else if (count === 10) {
          bcount = 8;
          Temp = captureDatasetlevel8;
        } else if (count === 11) {
          bcount = 9;
          Temp = captureDatasetlevel9;
        } else if (count === 12) {
          bcount = 10;
          Temp = captureDatasetlevel10;
        } else if (count === 13) {
          bcount = 11;
          Temp = captureDatasetlevel11;
        }
        count = count - 1;
        let FinalData = this.props.getChartData(
          Temp,
          this.props.testResults,
          projectobj[0],
          bcount,
          this.props.allDefectData,
          captureDataset,
          this.state.testType,
          this.props.componentData,
          this.state.staticData
        );
        TestDefectCollections = FinalData.DefectData;
        this.setState({
          series: FinalData.TestData,
        });
      } else if (count === 3) {
        Temp = "tests";
        bcount = 1;
        count = count - 1;
        let FinalData = this.props.getChartData(
          Temp,
          this.props.testResults,
          projectobj[0],
          bcount,
          this.props.allDefectData,
          captureDataset,
          this.state.testType,
          this.props.componentData,
          this.state.staticData
        );
        TestDefectCollections = FinalData.DefectData;
        this.setState({
          series: FinalData.TestData,
          previousPage: this.state.currentPage,
        });
      } else {
        Temp = "projects";
        count = 1;
        bcount = 1;
        let FinalData = this.props.getChartData(
          Temp,
          this.props.testResults,
          projectobj[0],
          bcount,
          this.props.allDefectData,
          captureDataset,
          this.state.testType,
          this.props.componentData,
          this.state.staticData
        );
        TestDefectCollections = FinalData.DefectData;
        this.setState({
          series: FinalData.TestData,
          previousPage: this.state.currentPage,
          TestExectionButton: false,
          HideInPercentNumbers: true,
        });
        clearTestLevels();
        captureDataset = "";
        captureDatasetlevel3 = "";
        captureDatasetlevel4 = "";
        captureDatasetlevel5 = "";
        captureDatasetlevel6 = "";
        captureDatasetlevel7 = "";
        captureDatasetlevel8 = "";
        captureDatasetlevel9 = "";
        captureDatasetlevel10 = "";
        captureDatasetlevel11 = "";
      }
      if (this.state.AssigneeButton === "active") {
        this.DefectByAssignee(
          this.props.projectToggle,
          this.props.projectdefectToggle
        );
      } else if (this.state.StatusButton === "active") {
        this.DefectByStatus(
          this.props.projectToggle,
          this.props.projectdefectToggle
        );
      } else {
        this.DefectByComponent(
          this.props.projectToggle,
          this.props.projectdefectToggle
        );
      }
    }
  }

  /* Test Exection Chart - Upadate option for Testcase*/

  updateChart(dataset) {
    clearRequirementLevels();
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (count === 1) {
      folderset.MainSubFolderSetter(dataset);
    }
    if (count === 2) {
      captureDataset = dataset;
    }
    if (projectobj.length !== 0) {
      if (count >= 4) {
        if (count === 4) {
          captureDatasetlevel4 = dataset;
          folderset.SubFolder2Setter(captureDatasetlevel4);
        } else if (count === 5) {
          captureDatasetlevel5 = dataset;
          folderset.SubFolderSetterForDefects(captureDatasetlevel5);
        } else if (count === 6) {
          folderset.SubFolder3Setter(dataset);
          captureDatasetlevel6 = dataset;
        } else if (count === 7) {
          folderset.SubFolder5Setter(dataset);
          captureDatasetlevel7 = dataset;
        } else if (count === 8) {
          captureDatasetlevel8 = dataset;
        } else if (count === 9) {
          folderset.SubFolder6Setter(dataset);
          captureDatasetlevel9 = dataset;
        } else if (count === 10) {
          captureDatasetlevel10 = dataset;
        } else if (count === 11) {
          folderset.SubFolder8Setter(dataset);
          captureDatasetlevel11 = dataset;
        }
        let FinalData = this.props.getChartData(
          dataset,
          this.props.testResults,
          projectobj[0],
          count,
          this.props.allDefectData,
          captureDataset,
          this.state.testType,
          this.props.componentData,
          this.state.staticData
        );
        TestDefectCollections = FinalData.DefectData;
        this.setState({
          series: FinalData.TestData,
          TestExectionButton: true,
        });
        if (count < 13 && FinalData.TestData !== 0) {
          count++;
        }
      }
      if (count === 2 || count === 3) {
        if (count === 3) {
          captureDatasetlevel3 = dataset;
          folderset.SubFolderSetter(captureDatasetlevel3);
        }
        let FinalData = this.props.getChartData(
          dataset,
          this.props.testResults,
          projectobj[0],
          count,
          this.props.allDefectData,
          captureDataset,
          this.state.testType,
          this.props.componentData,
          this.state.staticData
        );
        TestDefectCollections = FinalData.DefectData;
        this.setState({
          series: FinalData.TestData,
          TestExectionButton: true,
        });
        if (count < 13 && FinalData.TestData !== 0) {
          count++;
        }
      } else {
        let FinalData = this.props.getChartData(
          dataset,
          this.props.testResults,
          projectobj[0],
          count,
          this.props.allDefectData,
          captureDataset,
          this.state.testType,
          this.props.componentData,
          this.state.staticData
        );
        TestDefectCollections = FinalData.DefectData;
        this.setState({
          series: FinalData.TestData,
          previousPage: this.state.currentPage,
          TestExectionButton: true,
          HideInPercentNumbers: false,
        });
        if (count < 13 && FinalData.TestData !== 0) {
          count++;
        }
      }
      if (this.state.AssigneeButton === "active") {
        this.DefectByAssignee(
          this.props.projectToggle,
          this.props.projectdefectToggle
        );
      } else if (this.state.StatusButton === "active") {
        this.DefectByStatus(
          this.props.projectToggle,
          this.props.projectdefectToggle
        );
      } else {
        this.DefectByComponent(
          this.props.projectToggle,
          this.props.projectdefectToggle
        );
      }
    }
  }

  /* Test Exection Chart - Update option for UserStories*/

  storiesupdate(dataset) {
    if (this.state.epicbtn === "active" || this.state.allEpicsbtn === "active") {
      this.setState({
        UserStoriesBack: false
      });
    } else {
      let projectobj = this.props.projectDatas.filter((project) => {
        return (
          releaseURl === project.ProjectReference &&
          projectRelease === project.ReleaseName
        );
      });
      if (projectobj.length !== 0) {
        if (storycount === 2) {
          this.setState({
            UserStoriesBack: true,
          });
        } else {
          let UserStoriesData: any = UserStoriesFunction(
            this.props.userStoriesData,
            projectobj[0],
            storycount,
            dataset,
            this.state.storyType,
            this.props.componentData,
            this.state.userStorybtn
          );
          this.setState({
            UserStoriesBack: true,
            UserstoriesSeries: UserStoriesData.UserStories.Finalseries,
            storyTableData: UserStoriesData.UserStories.tableViewData,
            UserStoryHideInPercentNumbers: false,
          });
        }
        if (storycount < 2) {
          storycount++;
        }
      }
    }
  }

  /* Test Exection Chart - Back option for User Stories*/

  storiesBack() {
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (projectobj.length !== 0) {
      storycount = storycount - 1;
      let UserStoriesData: any = UserStoriesFunction(
        this.props.userStoriesData,
        projectobj[0],
        storycount,
        initialDataset,
        this.state.storyType,
        this.props.componentData,
        this.state.userStorybtn
      );
      this.setState({
        UserstoriesSeries: UserStoriesData.UserStories.Finalseries,
        UserStoriesBack: false,
        storyTableData: UserStoriesData.UserStories.tableViewData,
        UserStoryHideInPercentNumbers: true,
      });
    }
  }

  /* Test Exection Chart - Update option for Requirement Coverage*/

  requirementupdate(dataset) {
    clearTestLevels();
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (count === 2) {
      RequirementcaptureDataset = dataset;
    }
    if (projectobj.length !== 0) {
      if (count >= 4) {
        if (count === 4) {
          RequirementcaptureDatasetlevel4 = dataset;
          folderset.RequirementSubFolder2Setter(
            RequirementcaptureDatasetlevel4
          );
        } else if (count === 5) {
          RequirementcaptureDatasetlevel5 = dataset;
        } else if (count === 6) {
          folderset.RequirementSubFolder3Setter(dataset);
          RequirementcaptureDatasetlevel6 = dataset;
        } else if (count === 7) {
          folderset.RequirementSubFolder5Setter(dataset);
          RequirementcaptureDatasetlevel7 = dataset;
        } else if (count === 8) {
          RequirementcaptureDatasetlevel8 = dataset;
        } else if (count === 9) {
          folderset.RequirementSubFolder6Setter(dataset);
          RequirementcaptureDatasetlevel9 = dataset;
        } else if (count === 10) {
          RequirementcaptureDatasetlevel10 = dataset;
        } else if (count === 11) {
          folderset.RequirementSubFolder8Setter(dataset);
          RequirementcaptureDatasetlevel11 = dataset;
        }
        let RequirementData = RequirementCoverageMainFunction(
          this.props.requirementData,
          projectobj[0],
          count,
          dataset,
          RequirementcaptureDataset,
          this.props.componentData
        );
        TestDefectCollections = RequirementData.DefectData;
        this.setState({
          RequirementCoverageBack: true,
          series: RequirementData.RequirementCoverage,
        });
        if (count < 13 && RequirementData.RequirementCoverage !== 0) {
          count++;
        }
      }
      if (count === 2 || count === 3) {
        if (count === 3) {
          RequirementcaptureDatasetlevel3 = dataset;
          folderset.RequirementSubFolderSetter(RequirementcaptureDatasetlevel3);
        }
        let RequirementData = RequirementCoverageMainFunction(
          this.props.requirementData,
          projectobj[0],
          count,
          dataset,
          RequirementcaptureDataset,
          this.props.componentData
        );
        TestDefectCollections = RequirementData.DefectData;
        this.setState({
          RequirementCoverageBack: true,
          series: RequirementData.RequirementCoverage,
        });
        if (count < 13 && RequirementData.RequirementCoverage !== 0) {
          count++;
        }
      } else {
        let RequirementData = RequirementCoverageMainFunction(
          this.props.requirementData,
          projectobj[0],
          count,
          dataset,
          RequirementcaptureDataset,
          this.props.componentData
        );
        this.setState({
          RequirementCoverageBack: true,
          series: RequirementData.RequirementCoverage,
          HideInPercentNumbers: false,
        });
        if (count < 13 && RequirementData.RequirementCoverage !== 0) {
          count++;
        }
      }
      if (this.state.AssigneeButton === "active") {
        this.DefectByAssignee(
          this.props.projectToggle,
          this.props.projectdefectToggle
        );
      } else if (this.state.StatusButton === "active") {
        this.DefectByStatus(
          this.props.projectToggle,
          this.props.projectdefectToggle
        );
      } else {
        this.DefectByComponent(
          this.props.projectToggle,
          this.props.projectdefectToggle
        );
      }
    }
  }

  /* Test Exection Chart - Back option for Requirement Coverage*/

  requirementBack() {
    let Temp;
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (projectobj.length !== 0) {
      if (count > 3 && count <= 13) {
        Temp = RequirementcaptureDataset;
        bcount = 2;
        if (count === 5) {
          bcount = 3;
          Temp = RequirementcaptureDatasetlevel3;
        } else if (count === 6) {
          bcount = 4;
          Temp = RequirementcaptureDatasetlevel4;
        } else if (count === 7) {
          bcount = 5;
          Temp = RequirementcaptureDatasetlevel5;
        } else if (count === 8) {
          bcount = 6;
          Temp = RequirementcaptureDatasetlevel6;
        } else if (count === 9) {
          bcount = 7;
          Temp = RequirementcaptureDatasetlevel7;
        } else if (count === 10) {
          bcount = 8;
          Temp = RequirementcaptureDatasetlevel8;
        } else if (count === 11) {
          bcount = 9;
          Temp = RequirementcaptureDatasetlevel9;
        } else if (count === 12) {
          bcount = 10;
          Temp = RequirementcaptureDatasetlevel10;
        } else if (count === 13) {
          bcount = 11;
          Temp = RequirementcaptureDatasetlevel11;
        }
        count = count - 1;
        let RequirementData = RequirementCoverageMainFunction(
          this.props.requirementData,
          projectobj[0],
          bcount,
          Temp,
          RequirementcaptureDataset,
          this.props.componentData
        );
        TestDefectCollections = RequirementData.DefectData;
        this.setState({
          series: RequirementData.RequirementCoverage,
        });
      } else if (count === 3) {
        Temp = "tests";
        bcount = 1;
        count = count - 1;
        let RequirementData = RequirementCoverageMainFunction(
          this.props.requirementData,
          projectobj[0],
          bcount,
          Temp,
          RequirementcaptureDataset,
          this.props.componentData
        );
        TestDefectCollections = RequirementData.DefectData;
        this.setState({
          series: RequirementData.RequirementCoverage,
        });
      } else {
        Temp = initialDataset;
        count = 1;
        bcount = 1;
        let RequirementData = RequirementCoverageMainFunction(
          this.props.requirementData,
          projectobj[0],
          count,
          initialDataset,
          RequirementcaptureDataset,
          this.props.componentData
        );
        TestDefectCollections = [];
        this.setState({
          series: RequirementData.RequirementCoverage,
          RequirementCoverageBack: false,
          HideInPercentNumbers: true,
        });
        clearRequirementLevels();
        RequirementcaptureDataset = "";
        RequirementcaptureDatasetlevel3 = "";
        RequirementcaptureDatasetlevel4 = "";
        RequirementcaptureDatasetlevel5 = "";
        RequirementcaptureDatasetlevel6 = "";
        RequirementcaptureDatasetlevel7 = "";
        RequirementcaptureDatasetlevel8 = "";
        RequirementcaptureDatasetlevel9 = "";
        RequirementcaptureDatasetlevel10 = "";
        RequirementcaptureDatasetlevel11 = "";
      }
    }
    if (this.state.AssigneeButton === "active") {
      this.DefectByAssignee(
        this.props.projectToggle,
        this.props.projectdefectToggle
      );
    } else if (this.state.StatusButton === "active") {
      this.DefectByStatus(
        this.props.projectToggle,
        this.props.projectdefectToggle
      );
    } else {
      this.DefectByComponent(
        this.props.projectToggle,
        this.props.projectdefectToggle
      );
    }
  }

  /* Test Exection Chart - option for Testcase*/

  chartOptions() {
    return {
      series: this.state.series,
      chart: {
        type: "bar",
        style: {
          fontFamily: "serif",
        },
      },
      exporting: {
        enabled: false, // hide button
      },
      title: null,
      xAxis: { type: "category" },
      legend: { enabled: true },
      tooltip: {
        formatter: function () {
          return this.series.name + " " + this.y;
        },
      },
      plotOptions: {
        bar: { stacking: "normal" },
        series: {
          cursor: "pointer",
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            formatter: function () {
              return this.y !== 0 ? this.y : "";
            },
            style: {
              textShadow: false,
              fontSize: "10px",
              color: "#000000",
              textOutline: false,
            },
          },
          events: {
            click: (function (self) {
              return function (event) {
                self.updateChart(event.point.options.name);
              };
            })(this),
          },
        },
      },
      yAxis: {
        reversedStacks: false,
        min: 0,
        max: null,
        gridLineWidth: 0,
        title: {
          text: null,
        },
      },
      credits: {
        enabled: false,
      },
    };
  }

  /*Test Execution In percent */

  chartOptionsInPercent() {
    return {
      series: this.state.series,
      chart: {
        type: "bar",
        style: {
          fontFamily: "serif",
        },
      },
      exporting: {
        enabled: false, // hide button
      },
      title: null,
      xAxis: { type: "category" },
      legend: { enabled: true },
      tooltip: {
        formatter: function () {
          return (
            this.series.name + " " + Math.round(this.point.percentage) + "%"
          );
        },
      },
      plotOptions: {
        bar: { stacking: "percent" },
        series: {
          cursor: "pointer",
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            formatter: function () {
              return this.y !== 0
                ? Math.round(this.point.percentage) + "%"
                : "";
            },
            style: {
              textShadow: false,
              fontSize: "10px",
              color: "#000000",
              textOutline: false,
            },
          },
          events: {
            click: (function (self) {
              return function (event) {
                self.updateChart(event.point.options.name);
              };
            })(this),
          },
        },
      },
      yAxis: {
        reversedStacks: false,
        min: 0,
        max: 100,
        labels: {
          formatter: function () {
            return this.value; //+ "%";
          },
        },
        gridLineWidth: 0,
        title: {
          text: null,
        },
      },
      credits: {
        enabled: false,
      },
    };
  }

  /* Test Exection Chart -  option for Requirement*/

  RequirementchartOptions() {
    return {
      series: this.state.series,
      chart: {
        type: "bar",
        style: {
          fontFamily: "serif",
        },
      },
      exporting: {
        enabled: false, // hide button
      },
      title: null,
      xAxis: { type: "category" },
      legend: { enabled: true },
      tooltip: {
        formatter: function () {
          return this.series.name + " " + this.y;
        },
      },
      plotOptions: {
        bar: { stacking: "normal" },
        series: {
          cursor: "pointer",
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            formatter: function () {
              return this.y !== 0 ? this.y : "";
            },
            style: {
              textShadow: false,
              fontSize: "10px",
              color: "#000000",
              textOutline: false,
            },
          },
          events: {
            click: (function (self) {
              return function (event) {
                self.requirementupdate(event.point.options.name);
              };
            })(this),
          },
        },
      },
      yAxis: {
        reversedStacks: false,
        min: 0,
        max: null,
        gridLineWidth: 0,
        title: {
          text: null,
        },
      },
      credits: {
        enabled: false,
      },
    };
  }

  /* Test Exection Chart -  option for Requirement*/

  RequirementchartOptionsInPercent() {
    return {
      series: this.state.series,
      chart: {
        type: "bar",
        style: {
          fontFamily: "serif",
        },
      },
      exporting: {
        enabled: false, // hide button
      },
      title: null,
      xAxis: { type: "category" },
      legend: { enabled: true },
      tooltip: {
        formatter: function () {
          return (
            this.series.name + " " + Math.round(this.point.percentage) + "%"
          );
        },
      },
      plotOptions: {
        bar: { stacking: "percent" },
        series: {
          cursor: "pointer",
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            formatter: function () {
              return this.y !== 0
                ? Math.round(this.point.percentage) + "%"
                : "";
            },
            style: {
              textShadow: false,
              fontSize: "10px",
              color: "#000000",
              textOutline: false,
            },
          },
          events: {
            click: (function (self) {
              return function (event) {
                self.requirementupdate(event.point.options.name);
              };
            })(this),
          },
        },
      },
      yAxis: {
        reversedStacks: false,
        min: 0,
        max: 100,
        labels: {
          formatter: function () {
            return this.value; //+ "%";
          },
        },
        gridLineWidth: 0,
        title: {
          text: null,
        },
      },
      credits: {
        enabled: false,
      },
    };
  }

  /* Test Exection Chart -  option for UserStories*/

  StorieschartOptions() {
    return {
      series: this.state.UserstoriesSeries,
      chart: {
        type: "bar",
        style: {
          fontFamily: "serif",
        },
      },
      exporting: {
        enabled: false, // hide button
      },
      title: null,
      xAxis: { type: "category" },
      legend: { enabled: true },
      tooltip: {
        formatter: function () {
          return this.series.name + " " + this.y;
        },
      },
      plotOptions: {
        bar: { stacking: "normal" },
        series: {
          cursor: "pointer",
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            formatter: function () {
              return this.y !== 0 ? this.y : "";
            },
            style: {
              textShadow: false,
              fontSize: "10px",
              color: "#000000",
              textOutline: false,
            },
          },
          events: {
            click: (function (self) {
              return function (event) {
                self.storiesupdate(event.point.options.name);
              };
            })(this),
          },
        },
      },
      yAxis: {
        reversedStacks: false,
        min: 0,
        max: null,
        gridLineWidth: 0,
        title: {
          text: null,
        },
      },
      credits: {
        enabled: false,
      },
    };
  }

  /*Epic Stories*/

  StoriesEpicchartOptions() {
    return {
      series: this.state.UserstoriesSeries,
      chart: {
        type: "bar",
        style: {
          fontFamily: "serif",
        },
      },
      exporting: {
        enabled: false, // hide button
      },
      title: null,
      xAxis: { type: "category" },
      legend: { enabled: true },
      tooltip: {
        formatter: function () {
          return this.series.name + " " + this.y;
        },
      },
      plotOptions: {
        bar: { stacking: "normal" },
        series: {
          cursor: "pointer",
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            formatter: function () {
              return this.y !== 0 ? this.y : "";
            },
            style: {
              textShadow: false,
              fontSize: "10px",
              color: "#000000",
              textOutline: false,
            },
          },
          // events: {
          //   click: (function (self) {
          //     return function (event) {
          //       if(this.state.allEpicsbtn !== "active" && this.state.epicbtn !== "active")
          //       self.storiesupdate(event.point.options.name);
          //     };
          //   })(this),
          // },
        },
      },
      yAxis: {
        reversedStacks: false,
        min: 0,
        max: null,
        gridLineWidth: 0,
        title: {
          text: null,
        },
      },
      credits: {
        enabled: false,
      },
    };
  }

  /* Test Exection Chart -  option for UserStories*/

  StorieschartOptionsInPercent() {
    return {
      series: this.state.UserstoriesSeries,
      chart: {
        type: "bar",
        style: {
          fontFamily: "serif",
        },
      },
      exporting: {
        enabled: false, // hide button
      },
      title: null,
      xAxis: { type: "category" },
      legend: { enabled: true },
      tooltip: {
        formatter: function () {
          return (
            this.series.name + " " + Math.round(this.point.percentage) + "%"
          );
        },
      },
      plotOptions: {
        bar: { stacking: "percent" },
        series: {
          cursor: "pointer",
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            formatter: function () {
              return this.y !== 0
                ? Math.round(this.point.percentage) + "%"
                : "";
            },
            style: {
              textShadow: false,
              fontSize: "10px",
              color: "#000000",
              textOutline: false,
            },
          },
          events: {
            click: (function (self) {
              return function (event) {
                self.storiesupdate(event.point.options.name);
              };
            })(this),
          },
        },
      },
      yAxis: {
        reversedStacks: false,
        min: 0,
        max: 100,
        labels: {
          formatter: function () {
            return this.value; //+ "%";
          },
        },
        gridLineWidth: 0,
        title: {
          text: null,
        },
      },
      credits: {
        enabled: false,
      },
    };
  }

  /* Validating Component Data */

  ValidatingComponentData() {
    if (TempComponentData.epicLink === changeComponentData.epicLink) {
      return false;
    } else {
      TempComponentData = changeComponentData;
      return true;
    }
  }
  /* Validating Release Data */

  ValidatingReleaseData(projectviewdata) {
    if (TempReleaseData.length !== 0 && projectviewdata.length !== 0) {
      if (TempReleaseData[0].ReleaseName === projectviewdata[0].ReleaseName) {
        return false;
      } else {
        TempReleaseData = projectviewdata;
        return true;
      }
    }
  }

  /*HighChart in Numbers*/

  InNumbers = () => {
    this.setState({
      Inpercent: false,
      InNumbermbers: true,
      InpercentButton: "",
      InNumbermbersButton: "active",
    });
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (this.state.isTestCase === true) {
      let TestItem = this.props.getChartData(
        initialDataset,
        this.props.testResults,
        projectobj[0],
        count,
        this.props.allDefectData,
        captureDataset,
        this.state.testType,
        this.props.componentData,
        this.state.staticData
      );
      this.setState({
        series: TestItem.TestData,
      });
    }
    // else if (
    //   this.state.isTestCase !== true &&
    //   this.state.isUserStories === true
    // ) {
    //   if (projectobj.length !== 0) {
    //     let UserStoriesData: any = UserStoriesFunction(
    //       this.props.userStoriesData,
    //       projectobj[0],
    //       count,
    //       initialDataset,
    //       this.state.storyType,
    //       this.props.componentData,
    //       this.state.userStorybtn
    //     );
    //     this.setState({
    //       series: UserStoriesData.UserStories.Finalseries,
    //       storyTableData: UserStoriesData.UserStories.tableViewData,
    //     });
    //   }
    // }
    else {
      if (projectobj.length !== 0) {
        let RequirementCoverageData = RequirementCoverageMainFunction(
          this.props.requirementData,
          projectobj[0],
          count,
          initialDataset,
          RequirementcaptureDataset,
          this.props.componentData
        );
        this.setState({
          series: RequirementCoverageData.RequirementCoverage,
        });
      }
    }
  };

  /*HighChart in Percent*/

  InPercent = () => {
    this.setState({
      Inpercent: true,
      InNumbermbers: false,
      InpercentButton: "active",
      InNumbermbersButton: "",
    });
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (this.state.isTestCase === true) {
      let TestItem = this.props.getChartData(
        initialDataset,
        this.props.testResults,
        projectobj[0],
        count,
        this.props.allDefectData,
        captureDataset,
        this.state.testType,
        this.props.componentData,
        this.state.staticData
      );
      this.setState({
        series: TestItem.TestData,
      });
    }
    // else if (
    //   this.state.isTestCase !== true &&
    //   this.state.isUserStories === true
    // ) {
    //   if (projectobj.length !== 0) {
    //     let UserStoriesData: any = UserStoriesFunction(
    //       this.props.userStoriesData,
    //       projectobj[0],
    //       count,
    //       initialDataset,
    //       this.state.storyType,
    //       this.props.componentData,
    //       this.state.userStorybtn
    //     );
    //     this.setState({
    //       series: UserStoriesData.UserStories.Finalseries,
    //       storyTableData: UserStoriesData.UserStories.tableViewData,
    //     });
    //   }
    // }
    else {
      if (projectobj.length !== 0) {
        let RequirementCoverageData = RequirementCoverageMainFunction(
          this.props.requirementData,
          projectobj[0],
          count,
          initialDataset,
          RequirementcaptureDataset,
          this.props.componentData
        );
        this.setState({
          series: RequirementCoverageData.RequirementCoverage,
        });
      }
    }
  };

  /*PDF Preview Modal*/

  GeneratePdf = () => {
    this.setState({ modalVisible: false });
    this.props.inmemoryProjectURLStore(releaseURl);
    Global.history.push("/pdfview/" + projectRelease + "/" + releaseURl);
  };

  CancelModal = () => {
    this.setState({ modalVisible: false });
    this.props.inmemoryDetailedView(false);
  };

  ShowModal = () => {
    this.setState({ modalVisible: true });
  };

  DetailViewCheck = (event: any) => {
    this.props.inmemoryDetailedView(event.target.checked);
  };

  /* Remarks Validation */

  ValidateRemarks = (data: any) => {
    if (data !== null && data !== undefined) {
      return data;
    } else {
      return config.nodata;
    }
  };

  /* Project URL Validation*/

  ValidateProjectURL = (data: any) => {
    if (
      this.props.projectViewData.length === 0 ||
      this.props.projectViewData[0] === undefined ||
      this.props.projectViewData[0] === null ||
      Object.keys(this.props.projectViewData[0]).length === 0
    ) {
      return false;
    } else {
      if (data === config.projectUrl) {
        if (this.props.projectViewData[0].PROJECTREPOSITORY_Link === null) {
          return false;
        } else {
          return true;
        }
      } else if (data === config.jiraUrl) {
        if (this.props.projectViewData[0].JIRA_Link === null) {
          return false;
        } else {
          return true;
        }
      } else if (data === config.zephyrurl) {
        if (this.props.projectViewData[0].ZEPHYR_Link === null) {
          return false;
        } else {
          return true;
        }
      } else if (data === config.jirastories) {
        if (this.props.projectViewData[0].UserStory_Link1 === null) {
          return false;
        } else {
          return true;
        }
      } else {
        return false;
      }
    }
  };

  /*Handling Dropdown*/

  handleChangeDocType = (value) => {
    this.props.storeDocType(value);
  };

  handleChangeGateVersion = (value) => {
    this.props.storeGateVersion(value);
  };

  /* Defects MVP and not MVP*/
  DefectsTypes = (type) => {
    this.props.DefectsToggle(type);
    if (this.state.AssigneeButton === "active") {
      this.DefectByAssignee(type, this.props.projectdefectToggle);
    } else if (this.state.StatusButton === "active") {
      this.DefectByStatus(type, this.props.projectdefectToggle);
    } else {
      this.DefectByComponent(type, this.props.projectdefectToggle);
    }
  };

  /* Defects Closed and Open */
  DefectsCategories = (type) => {
    if (type === config.allDefects) {
      openDefects = "";
      storyDefects = "";
      allDefects = "active";
    } else if (type === config.storyDefects) {
      openDefects = "";
      storyDefects = "active";
      allDefects = "";
    } else {
      openDefects = "active";
      allDefects = "";
      storyDefects = "";
    }
    this.props.ProjectDefectsCategories(type);
    if (this.state.AssigneeButton === "active") {
      this.DefectByAssignee(this.props.projectToggle, type);
    } else if (this.state.StatusButton === "active") {
      this.DefectByStatus(this.props.projectToggle, type);
    } else {
      this.DefectByComponent(this.props.projectToggle, type);
    }
  };

  /*Defects Naming Function */

  DefectsName = () => {
    if (
      this.props.projectToggle !== "" &&
      this.props.projectdefectToggle !== "" &&
      this.props.projectToggle !== undefined &&
      this.props.projectdefectToggle !== undefined
    ) {
      if (this.props.projectToggle === config.allDefects) {
        return this.props.projectdefectToggle.toUpperCase();
      } else {
        let data =
          this.props.projectdefectToggle.toUpperCase() +
          "-" +
          this.props.projectToggle.toUpperCase();
        return data;
      }
    } else {
      return config.allDefects.toUpperCase();
    }
  };

  /*User Stories MVP and not MVP */
  userStoriesType = (type) => {
    this.setState({ storyType: type });
    this.UserStories(type, this.state.userStorybtn);
  };

  /*EpicType*/
  EpicType = (epicType) => {
    this.setState({ userStorybtn: epicType });
    this.UserStories(this.state.storyType, epicType);

  }

  /*Test Case MVP and not MVP */
  TestType = (type) => {
    this.setState({ testType: type });
    this.TestExecutionStatus(type, this.state.staticData);
  };

  /*Validating External Report ICON */

  ValidatingExternalReportIcon = () => {
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (projectobj.length !== 0) {
      let filteredDefects = this.props.allDefectData.filter((defect) => {
        return (
          projectobj[0].ReleaseName === defect.ReleaseName &&
          projectobj[0].ProjectReference === defect.projectReference
        );
      });
      let filteredStories = this.props.userStoriesData.filter((story) => {
        return (
          projectobj[0].ReleaseName === story.fixVersions &&
          projectobj[0].ProjectReference === story.ProjectReference
        );
      });
      let filteredRequirements = this.props.requirementData.filter((requirement) => {
        return (
          projectobj[0].ReleaseName === requirement.ReleaseName &&
          projectobj[0].ProjectReference === requirement.ProjectReference
        );
      });
      let filteredTestExecution = this.props.testResults.filter((testResults) => {
        return (
          projectobj[0].ReleaseName === testResults.ReleaseName &&
          projectobj[0].ProjectReference === testResults.ProjectReference
        );
      });
      if (filteredTestExecution.length === 0 && filteredRequirements.length === 0 &&
        filteredStories.length === 0 && filteredDefects.length === 0 && !this.validatingProjectData_KeyRemarksTesting() && !this.validatingProjectData_TestKeyMessageHistory() && !this.validatingProjectData_OpenRiskAndIssues() && !this.validatingProjectData_DetailedTestStatus()) {
        return false;
      } else {
        return true;
      }
    } else {
      return false;
    }
  }

  /*Table view*/

  DefectView = (event: any) => {
    if (event) {
      this.setState({ tableView: false, chartcolor: true, tablecolor: false });
      // this.DefectByAssignee(config.allDefects, config.allDefects);
      if (this.state.AssigneeButton === "active") {
        this.DefectByAssignee(config.allDefects, config.allDefects);
      } else if (this.state.StatusButton === "active") {
        this.DefectByStatus(config.allDefects, config.allDefects);
      } else {
        this.DefectByComponent(config.allDefects, config.allDefects);
      }
    } else {
      this.setState({ tableView: true, chartcolor: false, tablecolor: true });
      if (this.state.AssigneeButton === "active") {
        this.DefectByAssignee(config.allDefects, config.allDefects);
      } else {
        this.DefectByStatus(config.allDefects, config.allDefects);
      }
    }
  };

  /*Generating Dynamic Columns*/

  generateColumns = () => {
    if (Object.keys(this.state.tableViewData).length !== 0) {
      return this.state.tableViewData.columns.map((key, index) => {
        return <th key={key}>{key}</th>;
      });
    } else {
      return [config.nodataavailable];
    }
  };

  /*Generating Dynamic Columns*/

  generateRows = () => {
    if (Object.keys(this.state.tableViewData).length !== 0) {
      return this.state.tableViewData.rows.map((data, index) => {
        return <tr key={index}>{this.generateSubRows(data)}</tr>;
      });
    } else {
      return (
        <tr>
          <td colSpan={5}>{config.nodataavailable}</td>
        </tr>
      );
    }
  };

  /*Generating subrows*/

  generateSubRows = (data) => {
    let rowArray = [];
    data.dataArray.forEach((row, index) => {
      if (isNaN(row) || row === 0) {
        rowArray.push(<td>{row}</td>);
      } else {
        let JiraURL = this.props.projectViewData[0].JIRA_Link;
        let StoryJiraURL = this.props.projectViewData[0].Story_JIRA_Link;
        let ActiveStatus;
        if (this.state.AssigneeButton === "active") {
          ActiveStatus = config.assigneebtn;
        } else if (this.state.StatusButton === "active") {
          ActiveStatus = config.statusbtn;
        }
        if (this.props.projectdefectToggle === config.storyDefects) {
          JiraURL = FormingLinks(this.state.tableViewData.columns, data.dataArray, ActiveStatus, index, StoryJiraURL, this.props.defectByAssignee,
            this.props.projectViewData, this.props.projectdefectToggle);
        } else {
          JiraURL = FormingLinks(this.state.tableViewData.columns, data.dataArray, ActiveStatus, index, JiraURL, this.props.defectByAssignee,
            this.props.projectViewData, this.props.projectdefectToggle);
        }
        rowArray.push(<td> <a href={JiraURL} target="_blank" rel="noopener noreferrer">{row}</a></td>);
      }
    });
    return rowArray;
  };

  /*UserStoryTableView*/

  UserStoryView = (event: any) => {
    if (event) {
      this.setState({ userStoryTable: false, UserStorychartcolor: true, UserStorytablecolor: false })
    } else {
      this.setState({ userStoryTable: true, UserStorychartcolor: false, UserStorytablecolor: true })
    }
  }

  generateStoryColumns = () => {
    if (Object.keys(this.state.storyTableData).length !== 0) {
      return this.state.storyTableData.columns.map((key, index) => {
        return <th key={key}>{key}</th>;
      });
    } else {
      return [config.nodataavailable];
    }
  }

  generateStoryRows = () => {
    if (Object.keys(this.state.storyTableData).length !== 0) {
      return this.state.storyTableData.rows.map((data, index) => {
        return <tr key={index}>{this.generateSubRowsforStory(data)}</tr>;
      });
    } else {
      return (
        <tr>
          <td colSpan={5}>{config.nodataavailable}</td>
        </tr>
      );
    }
  };

  generateSubRowsforStory = (data: any) => {
    let rowArray = [];
    data.dataArray.forEach((row, index) => {
      if (isNaN(row) || row === 0) {
        rowArray.push(<td>{row}</td>);
      } else {
        if (this.props.projectViewData.length !== 0) {
          let storyURL = this.props.projectViewData[0].UserStory_Link;
          rowArray.push(<td key={index}><a href={FormingUserStoryLink(storyURL, this.state.storyTableData.columns[index], data.dataArray, this.props.userStoriesData, this.state.userStorybtn, releaseURl, projectRelease, this.props.epicLinks, this.props.componentData)} target="_blank" rel="noopener noreferrer">{row}</a></td>);
        }
      }
    });
    return rowArray;


  }

  /*User Stories - In Numbers*/
  UserStoriesInNumbers = () => {
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (projectobj.length !== 0) {
      let UserStoriesData: any = UserStoriesFunction(
        this.props.userStoriesData,
        projectobj[0],
        storycount,
        initialDataset,
        this.state.storyType,
        this.props.componentData,
        this.state.userStorybtn
      );
      this.setState({
        UserstoriesSeries: UserStoriesData.UserStories.Finalseries,
        UserStoriesInpercent: false,
        InNumbermbers: true,
        UserstoriesInpercentButton: "",
        UserstoriesInNumbermbersButton: "active",
      });
    }
  };

  /*UserStories - In Percent*/
  UserStoriesInPercent = () => {
    let projectobj = this.props.projectDatas.filter((project) => {
      return (
        releaseURl === project.ProjectReference &&
        projectRelease === project.ReleaseName
      );
    });
    if (projectobj.length !== 0) {
      let UserStoriesData: any = UserStoriesFunction(
        this.props.userStoriesData,
        projectobj[0],
        storycount,
        initialDataset,
        this.state.storyType,
        this.props.componentData,
        this.state.userStorybtn
      );
      this.setState({
        UserstoriesSeries: UserStoriesData.UserStories.Finalseries,
        UserStoriesInpercent: true,
        InNumbermbers: false,
        UserstoriesInpercentButton: "active",
        UserstoriesInNumbermbersButton: "",
      });
    }
  };

  AgentBrokerFiltert = (value) => {
    this.setState({ staticData: value });
    this.TestExecutionStatus(this.state.testType, value);
  }

  UserStoriesPerEpic = () => {
    let projectobj = this.props.projectDatas.filter((project) => {
      return releaseURl === project.ProjectReference;
    });

    if (projectobj.length !== 0) {
      let userStoriesPerEpics = EpicSecondLevelMainLevel(
        this.props.userStoriesData,
        projectobj[0],
        config.allstories,
        this.props.componentData,
        config.epics
      );
      this.setState({
        UserstoriesSeries: userStoriesPerEpics.Finalseries,
        storyTableData: userStoriesPerEpics.tableViewData,
        allEpicsbtn: "",
        epicbtn: "active", UserStoriesButton: "",
        userStorybtn: config.epicsperstory,
        UserStoriesBack: false,
        UserStoriesInpercent: false,
      });

    }


  }

  AllEpics = () => {
    let allEpics: any = AllEpics(this.props.epicLinks, this.props.componentData,
      this.state.userStorybtn);
    this.setState({
      UserstoriesSeries: allEpics.Finalseries,
      storyTableData: allEpics.tableViewData,
      allEpicsbtn: "active",
      epicbtn: "", UserStoriesButton: "",
      userStorybtn: config.epics,
      UserStoriesBack: false,
      UserStoriesInpercent: false,
    });
  }

  render() {
    changeComponentData = this.props.componentData;
    if (
      this.props.projectViewData.length !== 0 &&
      this.props.projectViewData !== undefined &&
      this.props.projectViewData !== null
    ) {
      /*Drop down menu value assigning*/
      report = (
        <Menu className="report-menu">
          {this.props.projectViewData[0].MASTER_TEST_PLAN_PATH !== null ? (
            <Menu.Item key="1">
              <a
                rel="noopener noreferrer"
                href={this.props.projectViewData[0].MASTER_TEST_PLAN_PATH}
                target="_blank"
              >
                Master Test Plan{" "}
              </a>
            </Menu.Item>
          ) : (
            <Menu.Item key="1" disabled={true}>
              <Text disabled={true}>Master Test Plan</Text>
            </Menu.Item>
          )}
          {this.props.projectViewData[0].unitTestReportPath !== null &&
            this.props.projectViewData[0].unitTestReportPath !== undefined ? (
            <Menu.Item key="2">
              <a
                rel="noopener noreferrer"
                href={this.props.projectViewData[0].unitTestReportPath}
                target="_blank"
              >
                Unit Test Report
              </a>
            </Menu.Item>
          ) : (
            <Menu.Item key="2" disabled={true}>
              <Text disabled={true}>Unit Test Report</Text>
            </Menu.Item>
          )}
          {this.props.projectViewData[0].performanceReportPath !== null &&
            this.props.projectViewData[0].performanceReportPath !== undefined ? (
            <Menu.Item key="3">
              <a
                rel="noopener noreferrer"
                href={this.props.projectViewData[0].performanceReportPath}
                target="_blank"
              >
                Performance Test Report
              </a>
            </Menu.Item>
          ) : (
            <Menu.Item key="3" disabled={true}>
              <Text disabled={true}>Performance Test Report</Text>
            </Menu.Item>
          )}

          {this.props.projectViewData[0].securityReportPath !== null &&
            this.props.projectViewData[0].securityReportPath !== undefined ? (
            <Menu.Item key="4">
              <a
                rel="noopener noreferrer"
                href={this.props.projectViewData[0].securityReportPath}
                target="_blank"
              >
                Security Test Report
              </a>
            </Menu.Item>
          ) : (
            <Menu.Item key="4" disabled={true}>
              <Text disabled={true}>Security Test Report</Text>
            </Menu.Item>
          )}
          {this.props.projectViewData[0].IT_TestReportPath !== null &&
            this.props.projectViewData[0].IT_TestReportPath !== undefined ? (
            <Menu.Item key="5">
              <a
                rel="noopener noreferrer"
                href={this.props.projectViewData[0].IT_TestReportPath}
                target="_blank"
              >
                Test Status Report
              </a>
            </Menu.Item>
          ) : (
            <Menu.Item key="5" disabled={true}>
              <Text disabled={true}>Test Status Report</Text>
            </Menu.Item>
          )}
          {this.props.projectViewData[0].CCB_GATING_PATH !== null ? (
            <Menu.Item key="6">
              <a
                rel="noopener noreferrer"
                href={this.props.projectViewData[0].CCB_GATING_PATH}
                target="_blank"
              >
                CCB Gating Test Report
              </a>
            </Menu.Item>
          ) : (
            <Menu.Item key="6" disabled={true}>
              <Text disabled={true}>CCB Gating Test Report</Text>
            </Menu.Item>
          )}
          {this.props.projectViewData[0].automationReportPath !== null &&
            this.props.projectViewData[0].automationReportPath !== undefined ? (
            <Menu.Item key="7">
              <a
                rel="noopener noreferrer"
                href={this.props.projectViewData[0].automationReportPath}
                target="_blank"
              >
                Test Automation Report
              </a>
            </Menu.Item>
          ) : (
            <Menu.Item key="7" disabled={true}>
              <Text disabled={true}>Test Automation Report</Text>
            </Menu.Item>
          )}
          {/* {this.props.projectViewData[0].UAT_TestReportPath !== null &&
            this.props.projectViewData[0].UAT_TestReportPath !== undefined ? (
            <Menu.Item key="6">
              <a
                rel="noopener noreferrer"
                href={this.props.projectViewData[0].UAT_TestReportPath}
                target="_blank"
              >
                UAT Test{" "}
              </a>
            </Menu.Item>
          ) : (
            <Menu.Item key="6" disabled={true}>
              <Text disabled={true}>UAT Test</Text>
            </Menu.Item>
          )} */}
          {this.props.projectViewData[0].BUSSINESS_GO_PATH !== null ? (
            <Menu.Item key="8">
              <a
                rel="noopener noreferrer"
                href={this.props.projectViewData[0].BUSSINESS_GO_PATH}
                target="_blank"
              >
                Business Go Report
              </a>
            </Menu.Item>
          ) : (
            <Menu.Item key="8" disabled={true}>
              <Text disabled={true}>Business Go Report</Text>
            </Menu.Item>
          )}

        </Menu>
      );
    }
    if (Object.keys(this.state.DefectData).length === 0) {
      count = 1;
      this.InitialDefectByAssignee();
      this.InitialTestExecutionStatus();
      this.UserStories(config.allstories, config.stories);
    }
    if (
      this.props.testResults.length !== 0 &&
      this.props.allDefectData.length !== 0 &&
      independantLoadTestExecution
    ) {
      independantLoadTestExecution = false;
      this.InitialTestExecutionStatus();
    }
    if (this.props.defectByStatus.length !== 0 && independantLoad === true) {
      independantLoad = false;
      this.InitialDefectByAssignee();
      this.InitialTestExecutionStatus();
    }
    if (this.props.userStoriesData.length !== 0 && independantLoadStory) {
      independantLoadStory = false;
      this.UserStories(config.allstories, config.stories);
    }
    if (
      this.props.requirementData.length !== 0 &&
      this.state.RequirementButton &&
      independantLoadRequirment
    ) {
      independantLoadRequirment = false;
      this.RequirementCoverage();
    }
    if (this.props.projectDatas.length !== 0) {
      if (
        this.props.projectDatas[1].ReleaseName === projectRelease &&
        projectDatavalidation
      ) {
        this.InitialDefectByAssignee();
        this.InitialTestExecutionStatus();
        projectDatavalidation = false;
      }
    }
    if (
      this.ValidatingComponentData() ||
      this.ValidatingReleaseData(this.props.projectViewData)
    ) {
      count = 1;
      if (this.state.AssigneeButton === "active") {
        this.DefectByAssignee(
          this.props.projectToggle,
          this.props.projectdefectToggle
        );
      } else if (this.state.StatusButton === "active") {
        this.DefectByStatus(
          this.props.projectToggle,
          this.props.projectdefectToggle
        );
      } else {
        this.DefectByComponent(
          this.props.projectToggle,
          this.props.projectdefectToggle
        );
      }
      if (this.state.RequirementButton === "active") {
        this.RequirementCoverage();
      }
      else {
        this.InitialTestExecutionStatus();
      }
      // this.InitialTestExecutionStatus();
      this.UserStories(config.allstories, config.stories)
      this.setState({ userStoryTable: false, });
    }
    if (this.matchingUrl()) {
      count = 1;
      TestDefectCollections = [];
      let projectobj = this.props.projectDatas.filter((project) => {
        return (
          releaseURl === project.ProjectReference &&
          projectRelease === project.ReleaseName
        );
      });
      this.props.ProjectDefectsCategories(config.allDefects);
      openDefects = "";
      allDefects = "active";
      storyDefects = "";
      if (this.state.AssigneeButton === "active") {
        this.setState({
          DefectData: this.props.DefectByAssigneeCharts(
            this.props.defectByAssignee,
            projectobj,
            this.props.componentData,
            TestDefectCollections,
            this.props.projectToggle,
            config.allDefects, config.allDefects,
          ),
          tableView: false,
          userStoryTable: false,
          UserStorychartcolor: true,
          UserStorytablecolor: false,
          chartcolor: true,
          tablecolor: false
        });
      } else if (this.state.StatusButton === "active") {
        this.setState({
          DefectData: this.props.StatusChartsByProjectLevel(
            this.props.defectByAssignee,
            projectobj,
            this.props.componentData,
            TestDefectCollections,
            this.props.projectToggle,
            config.allDefects, config.allDefects
          ),
          tableView: false,
          userStoryTable: false,
          UserStorychartcolor: true,
          UserStorytablecolor: false,
          chartcolor: true,
          tablecolor: false
        });
      } else {
        this.setState({
          DefectData: this.props.DefectByComponentCharts(
            this.props.defectByAssignee,
            projectobj,
            this.props.componentData,
            TestDefectCollections,
            this.props.projectToggle,
            config.allDefects, config.allDefects
          ),
          tableView: false,
          userStoryTable: false,
          UserStorychartcolor: true,
          UserStorytablecolor: false,
          chartcolor: true,
          tablecolor: false
        });
      }
      this.InitialTestExecutionStatus();
      this.UserStories(config.allstories, config.stories)
    }

    /*Table data construction*/
    let mycolumns = this.state.columns.map((col, index) => ({
      ...col,
      onHeaderCell: (column) => ({
        width: column.width,
      }),
    }));

    const dataItems = [];
    if (this.props.releaseHistory) {
      this.props.releaseHistory.forEach((element) => {
        dataItems.push({
          CCB_Closure_Date: element.ReleaseName,
          key: element.ReleaseName,
          CCB_STARTUP_Decision: (
            <div>
              {element.CCB_STARTUP_Date !== null &&
                element.CCB_STARTUP_Decision !== null ? (
                <p className="release-history-tble">
                  {element.CCB_STARTUP_Date.substr(0, 10) +
                    "   " +
                    element.CCB_STARTUP_Decision}
                </p>
              ) : (
                <div>
                  {element.CCB_STARTUP_Date === null &&
                    element.CCB_STARTUP_Decision !== null ? (
                    <p className="release-history-tble">
                      {"[No Data]  " + element.CCB_STARTUP_Decision}
                    </p>
                  ) : (
                    <div>
                      {element.CCB_STARTUP_Date !== null &&
                        element.CCB_STARTUP_Decision === null ? (
                        <p className="release-history-tble">
                          {element.CCB_STARTUP_Date.substr(0, 10) +
                            "  [No Data]"}
                        </p>
                      ) : (
                        <p className="release-history-tble">
                          {"[No Data]  [No Data]"}
                        </p>
                      )}
                    </div>
                  )}
                </div>
              )}
            </div>
          ),
          CCB_Ready4SPRINT_Decision: (
            <div>
              {element.CCB_Ready4SPRINT_Date !== null &&
                element.CCB_Ready4SPRINT_Decision !== null ? (
                <p className="release-history-tble">
                  {element.CCB_Ready4SPRINT_Date.substr(0, 10) +
                    "   " +
                    element.CCB_Ready4SPRINT_Decision}
                </p>
              ) : (
                <div>
                  {element.CCB_Ready4SPRINT_Date === null &&
                    element.CCB_Ready4SPRINT_Decision !== null ? (
                    <p className="release-history-tble">
                      {"[No Data]  " + element.CCB_Ready4SPRINT_Decision}
                    </p>
                  ) : (
                    <div>
                      {element.CCB_Ready4SPRINT_Date !== null &&
                        element.CCB_Ready4SPRINT_Decision === null ? (
                        <p className="release-history-tble">
                          {element.CCB_Ready4SPRINT_Date.substr(0, 10) +
                            "  [No Data]"}
                        </p>
                      ) : (
                        <p className="release-history-tble">
                          {"[No Data]  [No Data]"}
                        </p>
                      )}
                    </div>
                  )}
                </div>
              )}
            </div>
          ),
          CCB_Ready4UAT_Decision: (
            <div>
              {element.CCB_Ready4UAT_Date !== null &&
                element.CCB_Ready4UAT_Decision !== null ? (
                <p className="release-history-tble">
                  {element.CCB_Ready4UAT_Date.substr(0, 10) +
                    "   " +
                    element.CCB_Ready4UAT_Decision}
                </p>
              ) : (
                <div>
                  {element.CCB_Ready4UAT_Date === null &&
                    element.CCB_Ready4UAT_Decision !== null ? (
                    <p className="release-history-tble">
                      {"[No Data]  " + element.CCB_Ready4UAT_Decision}
                    </p>
                  ) : (
                    <div>
                      {element.CCB_Ready4UAT_Date !== null &&
                        element.CCB_Ready4UAT_Decision === null ? (
                        <p className="release-history-tble">
                          {element.CCB_Ready4UAT_Date.substr(0, 10) +
                            "  [No Data]"}
                        </p>
                      ) : (
                        <p className="release-history-tble">
                          {"[No Data]  [No Data]"}
                        </p>
                      )}
                    </div>
                  )}
                </div>
              )}
            </div>
          ),
          CCB_Ready4PROD_Decision: (
            <div>
              {element.CCB_Ready4PROD_Date !== null &&
                element.CCB_Ready4PROD_Decision !== null ? (
                <p className="release-history-tble">
                  {element.CCB_Ready4PROD_Date.substr(0, 10) +
                    "   " +
                    element.CCB_Ready4PROD_Decision}
                </p>
              ) : (
                <div>
                  {element.CCB_Ready4PROD_Date === null &&
                    element.CCB_Ready4PROD_Decision !== null ? (
                    <p className="release-history-tble">
                      {"[No Data]  " + element.CCB_Ready4PROD_Decision}
                    </p>
                  ) : (
                    <div>
                      {element.CCB_Ready4PROD_Date !== null &&
                        element.CCB_Ready4PROD_Decision === null ? (
                        <p className="release-history-tble">
                          {element.CCB_Ready4PROD_Date.substr(0, 10) +
                            "  [No Data]"}
                        </p>
                      ) : (
                        <p className="release-history-tble">
                          {"[No Data]  [No Data]"}
                        </p>
                      )}
                    </div>
                  )}
                </div>
              )}
            </div>
          ),
          CCB_CLOSURE_Decision: (
            <div>
              {element.CCB_Closure_Date !== null &&
                element.CCB_CLOSURE_Decision !== null ? (
                <p className="release-history-tble">
                  {element.CCB_Closure_Date.substr(0, 10) +
                    "   " +
                    element.CCB_CLOSURE_Decision}
                </p>
              ) : (
                <div>
                  {element.CCB_Closure_Date === null &&
                    element.CCB_CLOSURE_Decision !== null ? (
                    <p className="release-history-tble">
                      {"[No Data]  " + element.CCB_CLOSURE_Decision}
                    </p>
                  ) : (
                    <div>
                      {element.CCB_Closure_Date !== null &&
                        element.CCB_CLOSURE_Decision === null ? (
                        <p className="release-history-tble">
                          {element.CCB_Closure_Date.substr(0, 10) +
                            "  [No Data]"}
                        </p>
                      ) : (
                        <p className="release-history-tble">
                          {"[No Data]  [No Data]"}
                        </p>
                      )}
                    </div>
                  )}
                </div>
              )}
            </div>
          ),
          Ready2StartTestRemarks: this.ValidateRemarks(
            element.Ready2StartTestRemarks
          ),
          Ready4SprintTestRemarks: this.ValidateRemarks(
            element.Ready4SprintTestRemarks
          ),
          Ready4UATTestRemarks: this.ValidateRemarks(
            element.Ready4UATTestRemarks
          ),
          Ready4ProdTestRemarks: (<div dangerouslySetInnerHTML={{
            __html: this.ValidateRemarks(
              element.Ready4ProdTestRemarks
            )
          }} />),
          CCB_CLOSURE_Remarks: this.ValidateRemarks(
            element.CCB_CLOSURE_Remarks
          ),
        });
      });
    }
    return (
      <div className="container-flex page-wrapper">
        <div className="row content-row project_details">
          <div className="col-md-2 summary d-flex align-items-top remove-border">
            <div className="form-group">
              <div className="pro-info pt-0">
                <h5>
                  {this.state.curTime}{" "}
                  <Clock format="HH:mm:ss" interval={1000} ticking={true} />
                </h5>
                <div className="projct-status-info">
                  <p className="projct-status">
                    {this.validatingProjectData()
                      ? this.props.projectViewData[0].test_status !== null &&
                        this.props.projectViewData[0].test_status !== undefined
                        ? this.props.projectViewData[0].test_status
                        : "No Data"
                      : "No Data"}
                  </p>
                  {this.validatingProjectData() ? (
                    this.props.projectViewData[0].test_status ===
                      config.onTrack &&
                      this.props.projectViewData[0].test_status !== null &&
                      this.props.projectViewData[0].test_status !== undefined ? (
                      <SmileyComponent
                        img={require("../../assets/images/thumbnail_norisks.png")}
                        desc="No Risks"
                      />
                    ) : null
                  ) : (
                    "No Data"
                  )}

                  {this.validatingProjectData() ? (
                    this.props.projectViewData[0].test_status ===
                      config.notOnTrack &&
                      this.props.projectViewData[0].test_status !== null &&
                      this.props.projectViewData[0].test_status !== undefined ? (
                      <SmileyComponent
                        img={require("../../assets/images/thumbnail_withissues.png")}
                        desc="With issues"
                      />
                    ) : null
                  ) : (
                    "No Data"
                  )}

                  {this.validatingProjectData() ? (
                    this.props.projectViewData[0].test_status ===
                      config.notestStatus &&
                      this.props.projectViewData[0].test_status !== null &&
                      this.props.projectViewData[0].test_status !== undefined ? (
                      <SmileyComponent
                        img={require("../../assets/images/noteststatus.png")}
                        desc="No Test Status"
                      ></SmileyComponent>
                    ) : null
                  ) : (
                    "No Data"
                  )}

                  {this.validatingProjectData() ? (
                    this.props.projectViewData[0].test_status === config.atRisk &&
                      this.props.projectViewData[0].test_status !== null &&
                      this.props.projectViewData[0].test_status !== undefined ? (
                      <SmileyComponent
                        img={require("../../assets/images/thumbnail_nodecision.png")}
                        desc="No Decision"
                      />
                    ) : null
                  ) : (
                    "No Data"
                  )}
                  {this.validatingProjectData() ? (
                    this.props.projectViewData[0].test_status ===
                      config.managementDecision &&
                      this.props.projectViewData[0].test_status !== null &&
                      this.props.projectViewData[0].test_status !== undefined ? (
                      <SmileyComponent
                        img={require("../../assets/images/thumbnail_blocked.png")}
                        desc="Blocked"
                      />
                    ) : null
                  ) : (
                    "No Data"
                  )}
                </div>
              </div>

              <div className="pro-info sm-report-info">
                <div className="sm-report-action">
                  <Tooltip title="External Reports">
                    <Dropdown overlay={report}>
                      <DownCircleOutlined className="report" />
                    </Dropdown>
                  </Tooltip>
                </div>
                <div className="sm-report-action">
                  {this.ValidatingExternalReportIcon() ? (
                    <Tooltip title="PDF Preview">
                      <img src={Pdf} alt="PDF Preview" className="animated-pdf" onClick={this.ShowModal} />
                    </Tooltip>
                  ) : (
                    <Tooltip title="PDF preview is not available" >
                      <FilePdfOutlined
                        className="report"
                      />
                    </Tooltip>

                  )}
                </div>
              </div>
            </div>
          </div>

          <div className="col-md-3 summary d-flex align-items-top remove-border">
            <div className="form-group respon">
              <div className="row">
                <div className="col-md-8">
                  <h5>Test Key Message</h5>
                </div>
                <div className="col-md-4">
                  {this.validatingProjectData() &&
                    this.props.projectViewData[0].SP_ProjectEditLink !== null &&
                    this.props.projectViewData[0].SP_ProjectEditLink !==
                    undefined ? (
                    <a
                      rel="noopener noreferrer"
                      href={this.props.projectViewData[0].SP_ProjectEditLink}
                      target="_blank"
                    >
                      Edit <EditTwoTone className="edit-icon" />
                    </a>
                  ) : (
                    <Text className="project-data" disabled={true}>
                      Edit
                    </Text>
                  )}
                </div>
              </div>
              <Text className="summary-text">
                {this.validatingProjectData_KeyRemarksTesting() ? (
                  <div dangerouslySetInnerHTML={{ __html: htmlFromDatabase }} />
                ) : (
                  "No Test Status provided"
                )}
              </Text>
            </div>
          </div>
          <div className="col-md-7 summary">
            <div className="row">
              <div className="col-md-5">
                <div className="row">
                  <div className="col-md-6">
                    <label>Domain Name</label>
                  </div>
                  <div className="col-md-6">
                    <Text className="summary-text">
                      {this.validatingProjectData()
                        ? this.props.projectViewData[0].Domain
                        : "No Data"}
                    </Text>
                  </div>
                  <div className="col-md-6">
                    <label>Project Size</label>
                  </div>
                  <div className="col-md-6">
                    <Text className="summary-text">
                      {this.validatingProjectData()
                        ? this.props.projectViewData[0].ProjectSize_MDs
                        : "No Data"}
                    </Text>
                  </div>
                  <div className="col-md-6">
                    <label>Project Repository</label>
                  </div>
                  <div className="col-md-6">
                    {this.ValidateProjectURL(config.projectUrl) ? (
                      <a
                        rel="noopener noreferrer"
                        href={
                          this.props.projectViewData[0].PROJECTREPOSITORY_Link
                        }
                        target="_blank"
                      >
                        Project Details
                      </a>
                    ) : (
                      <Text className="project-data" disabled={true}>
                        Project Details
                      </Text>
                    )}
                  </div>
                  <div className="col-md-6">
                    <label>Jira </label>
                  </div>
                  <div className="col-md-6">
                    {this.ValidateProjectURL(config.jiraUrl) ? (
                      <a
                        rel="noopener noreferrer"
                        href={this.props.projectViewData[0].JIRA_Link}
                        target="_blank"
                      >
                        {" "}
                        Defect Details
                      </a>
                    ) : (
                      <Text className="project-data" disabled={true}>
                        Defect Details
                      </Text>
                    )}
                    <Text className="project-data"> / </Text>
                    {this.ValidateProjectURL(config.jirastories) ? (
                      <a
                        rel="noopener noreferrer"
                        href={this.props.projectViewData[0].UserStory_Link1}
                        target="_blank"
                      >
                        {" "}
                        Story Details
                      </a>
                    ) : (
                      <Text className="project-data" disabled={true}>
                        Story Details
                      </Text>
                    )}
                  </div>
                  <div className="col-md-6">
                    <label>Zephyr</label>
                  </div>
                  <div className="col-md-6">
                    {this.ValidateProjectURL(config.zephyrurl) ? (
                      <a
                        rel="noopener noreferrer"
                        href={this.props.projectViewData[0].ZEPHYR_Link}
                        target="_blank"
                      >
                        {" "}
                        Test Details
                      </a>
                    ) : (
                      <Text className="project-data" disabled={true}>
                        Test Details
                      </Text>
                    )}
                  </div>
                  <div className="col-md-6">
                    <label>Delivery Master Lead</label>
                  </div>
                  <div className="col-md-6">
                    <p>
                      {this.validatingProjectData()
                        ? this.props.projectViewData[0].Delivery_Master_Lead
                        : "No Data"}
                    </p>
                  </div>
                  <div className="col-md-6">
                    <label>Project Manager</label>
                  </div>
                  <div className="col-md-6">
                    <p>
                      {this.validatingProjectData()
                        ? this.props.projectViewData[0].Project_Manager
                        : "No Data"}
                    </p>
                  </div>
                  <div className="col-md-6">
                    <label>Scrum Master</label>
                  </div>
                  <div className="col-md-6">
                    <p>
                      {this.validatingProjectData()
                        ? this.props.projectViewData[0].Scrum_Master
                        : "No Data"}
                    </p>
                  </div>
                  <div className="col-md-6">
                    <label>Test Coordinator</label>
                  </div>
                  <div className="col-md-6">
                    <p>
                      {this.validatingProjectData()
                        ? this.props.projectViewData[0].Test_Coordinator
                        : "No Data"}
                    </p>
                  </div>
                </div>
              </div>

              <div className="col-md-7">
                <div className="row">
                  <div className="col-md-5">
                    <label>Startup</label>
                  </div>
                  <div className="col-md-7">
                    {this.handleStartUp() ? (
                      <p>
                        {this.validatingProjectData() &&
                          this.props.projectViewData[0].CCB_STARTUP_Date !==
                          null &&
                          this.props.projectViewData[0].CCB_STARTUP_Date !==
                          undefined
                          ? this.props.projectViewData[0].CCB_STARTUP_Date.substr(
                            0,
                            10
                          )
                          : "No Data"}
                        <span className="project_status">
                          {this.validatingProjectData() &&
                            this.props.projectViewData[0].CCB_STARTUP_Decision !==
                            null &&
                            this.props.projectViewData[0].CCB_STARTUP_Decision !==
                            undefined
                            ? this.props.projectViewData[0].CCB_STARTUP_Decision
                            : "No Data"}
                        </span>
                      </p>
                    ) : (
                      <p>
                        {this.validatingProjectData() &&
                          this.props.projectViewData[0].LastRegistrationDate !==
                          null &&
                          this.props.projectViewData[0].LastRegistrationDate !==
                          undefined
                          ? this.props.projectViewData[0].LastRegistrationDate.substr(
                            0,
                            10
                          )
                          : "No Data"}
                        <span className="project_status">
                          {this.validatingProjectData() &&
                            this.props.projectViewData[0].CCB_STARTUP_Decision !==
                            null &&
                            this.props.projectViewData[0].CCB_STARTUP_Decision !==
                            undefined
                            ? this.props.projectViewData[0].CCB_STARTUP_Decision
                            : "No Data"}
                        </span>
                      </p>
                    )}
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-5">
                    <label>Ready For Sprint</label>
                  </div>
                  <div className="col-md-7">
                    <p>
                      {this.validatingProjectData() &&
                        this.props.projectViewData[0].CCB_Ready4SPRINT_Date !==
                        null &&
                        this.props.projectViewData[0].CCB_Ready4SPRINT_Date !==
                        undefined
                        ? this.props.projectViewData[0].CCB_Ready4SPRINT_Date.substr(
                          0,
                          10
                        )
                        : "No Data"}
                      <span className="project_status">
                        {this.validatingProjectData() &&
                          this.props.projectViewData[0]
                            .CCB_Ready4SPRINT_Decision !== null &&
                          this.props.projectViewData[0]
                            .CCB_Ready4SPRINT_Decision !== undefined
                          ? this.props.projectViewData[0]
                            .CCB_Ready4SPRINT_Decision
                          : "No Data"}
                      </span>
                    </p>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-5">
                    <label>Ready For UAT</label>
                  </div>
                  <div className="col-md-7">
                    <p>
                      {this.validatingProjectData() &&
                        this.props.projectViewData[0].CCB_Ready4UAT_Date !==
                        null &&
                        this.props.projectViewData[0].CCB_Ready4UAT_Date !==
                        undefined
                        ? this.props.projectViewData[0].CCB_Ready4UAT_Date.substr(
                          0,
                          10
                        )
                        : "No Data"}
                      <span className="project_status">
                        {this.validatingProjectData() &&
                          this.props.projectViewData[0].CCB_Ready4UAT_Decision !==
                          null &&
                          this.props.projectViewData[0].CCB_Ready4UAT_Decision !==
                          undefined
                          ? this.props.projectViewData[0].CCB_Ready4UAT_Decision
                          : "No Data"}
                      </span>
                    </p>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-5">
                    <label>UAT Start</label>
                  </div>
                  <div className="col-md-7">
                    <p>
                      {this.validatingProjectData() &&
                        this.props.projectViewData[0].UAT_START_DATE !== null &&
                        this.props.projectViewData[0].UAT_START_DATE !== undefined
                        ? this.props.projectViewData[0].UAT_START_DATE.substr(
                          0,
                          10
                        )
                        : "No Data"}
                    </p>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-5">
                    <label>UAT End</label>
                  </div>
                  <div className="col-md-7">
                    <p>
                      {this.validatingProjectData() &&
                        this.props.projectViewData[0].UAT_END_DATE !== null &&
                        this.props.projectViewData[0].UAT_END_DATE !== undefined
                        ? this.props.projectViewData[0].UAT_END_DATE.substr(
                          0,
                          10
                        )
                        : "No Data"}
                    </p>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-5">
                    <label>Freeze</label>
                  </div>
                  <div className="col-md-7">
                    <p>
                      {this.validatingProjectData() &&
                        this.props.projectViewData[0].FREEZE_DATE !== null &&
                        this.props.projectViewData[0].FREEZE_DATE !== undefined
                        ? this.props.projectViewData[0].FREEZE_DATE.substr(
                          0,
                          10
                        )
                        : "No Data"}
                    </p>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-5">
                    <label>Ready For PROD</label>
                  </div>
                  <div className="col-md-7">
                    <p>
                      {this.validatingProjectData() &&
                        this.props.projectViewData[0].CCB_Ready4PROD_Date !==
                        null &&
                        this.props.projectViewData[0].CCB_Ready4PROD_Date !==
                        undefined
                        ? this.props.projectViewData[0].CCB_Ready4PROD_Date.substr(
                          0,
                          10
                        )
                        : "No Data"}
                      <span className="project_status">
                        {this.validatingProjectData() &&
                          this.props.projectViewData[0]
                            .CCB_Ready4PROD_Decision !== null &&
                          this.props.projectViewData[0]
                            .CCB_Ready4PROD_Decision !== undefined
                          ? this.props.projectViewData[0]
                            .CCB_Ready4PROD_Decision
                          : "No Data"}
                      </span>
                    </p>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-5">
                    <label>Closure</label>
                  </div>
                  <div className="col-md-7">
                    <p>
                      {this.validatingProjectData() &&
                        this.props.projectViewData[0].CCB_Closure_Date !== null &&
                        this.props.projectViewData[0].CCB_Closure_Date !==
                        undefined
                        ? this.props.projectViewData[0].CCB_Closure_Date.substr(
                          0,
                          10
                        )
                        : "No Data"}
                      <span className="project_status">
                        {this.validatingProjectData() &&
                          this.props.projectViewData[0].CCB_CLOSURE_Decision !==
                          null &&
                          this.props.projectViewData[0].CCB_CLOSURE_Decision !==
                          undefined
                          ? this.props.projectViewData[0].CCB_CLOSURE_Decision
                          : "No Data"}
                      </span>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row cta-section pro-view mt-2">
          <div className="col-md-4 cs-btn-group">
            <div className="row">
              <div className="col-md-6">
                <Button
                  type="primary"
                  className={this.state.RequirementButton}
                  onClick={this.RequirementCoverage}
                >
                  Requirement Coverage
                </Button>
              </div>
              <div className="col-md-6">
                <Button
                  type="primary"
                  className={this.state.TestCaseButton}
                  onClick={(event) => this.TestExecutionStatus(config.alltests, config.all)}
                >
                  Test Case Status
                </Button>
              </div>
            </div>
            <div className="test-execution test-execcution-chart">
              <h5>
                {this.state.isTestCase === true
                  ? (
                    `TEST EXECUTION - ${this.state.testType.toUpperCase()}`
                  ) : (
                    "REQUIREMENT COVERAGE"
                  )}
              </h5>
              <div className="row cs-btn-group">
                <div className="col-md-12">
                  {this.state.testType !== config.mvp ? (
                    <div style={{ float: "left", marginLeft: "5px", marginRight: "5px" }}>
                      <Button
                        onClick={(event) => this.TestType(config.mvp)}
                        style={{
                          display:
                            this.state.HideInPercentNumbers &&
                              // !this.state.isUserStories &&
                              this.state.isTestCase
                              ? "block"
                              : "none",
                        }}
                      >
                        MVP
                      </Button>
                    </div>
                  ) : null}
                  {this.state.testType !== config.notmvp ? (
                    <div style={{ float: "left", marginLeft: "5px", marginRight: "5px" }}>
                      <Button
                        onClick={(event) => this.TestType(config.notmvp)}
                        style={{
                          display:
                            this.state.HideInPercentNumbers &&
                              // !this.state.isUserStories &&
                              this.state.isTestCase
                              ? "block"
                              : "none",
                        }}
                      >
                        NOT MVP
                      </Button>
                    </div>
                  ) : null}
                  {this.state.testType !== config.alltests ? (
                    <div style={{ float: "left", marginLeft: "5px", marginRight: "5px" }}>
                      <Button
                        onClick={(event) => this.TestType(config.alltests)}
                        style={{
                          display:
                            this.state.HideInPercentNumbers &&
                              this.state.isTestCase
                              ? "block"
                              : "none",
                        }}
                      >
                        ALL TESTS
                      </Button>
                    </div>
                  ) : null}
                  {/* {this.state.staticData !== config.agent ? ( */}
                  <div style={{ float: "left", marginLeft: "5px", marginRight: "5px" }}>
                    <Select
                      defaultValue={config.all}
                      value={this.state.staticData}
                      style={{
                        display:
                          !this.state.TestExectionButton &&
                            // !this.state.isUserStories &&
                            this.state.isTestCase && this.state.staticFilter
                            ? "block"
                            : "none",
                      }}
                      onChange={this.AgentBrokerFiltert}
                    >
                      {usergroup.map((obj) => (
                        <Option key={obj.Key} value={obj.Value}>
                          {obj.Value}
                        </Option>
                      ))}
                    </Select>
                    <Select
                      defaultValue={config.all}
                      value={this.state.staticData}
                      style={{
                        display:
                          !this.state.TestExectionButton &&
                            // !this.state.isUserStories &&
                            this.state.isTestCase && this.state.businessDomain
                            ? "block"
                            : "none",
                      }}
                      onChange={this.AgentBrokerFiltert}
                    >
                      {BussinessDomain.map((obj) => (
                        <Option key={obj.Key} value={obj.Value}>
                          {obj.Value}
                        </Option>
                      ))}
                    </Select>

                  </div>
                  <div style={{ float: "right", marginLeft: "auto", marginRight: "5px" }}>
                    <Button
                      className={this.state.InNumbermbersButton}
                      onClick={this.InNumbers}
                      style={{
                        display: this.state.HideInPercentNumbers
                          // && !this.state.userStoryTable
                          ? "block"
                          : "none",
                      }}
                    >
                      #
                    </Button>
                  </div>
                  <div style={{ float: "right", marginLeft: "auto", marginRight: "5px" }}>
                    <Button
                      className={this.state.InpercentButton}
                      onClick={this.InPercent}
                      style={{
                        display: this.state.HideInPercentNumbers
                          // && !this.state.userStoryTable
                          ? "block"
                          : "none",
                      }}
                    >
                      %
                    </Button>
                  </div>

                  <div style={{ float: "right", marginLeft: "auto", marginRight: "5px" }}>
                    <Button
                      className="active"
                      onClick={this.handleBack}
                      style={{
                        display: this.state.TestExectionButton
                          ? "block"
                          : "none",
                      }}
                    >
                      Back
                    </Button>
                  </div>
                  <div style={{ float: "right", marginLeft: "auto", marginRight: "5px" }}>
                    <Button
                      className="active"
                      onClick={this.requirementBack}
                      style={{
                        display: this.state.RequirementCoverageBack
                          ? "block"
                          : "none",
                      }}
                    >
                      Back
                    </Button>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  {this.state.isTestCase === true
                    // &&
                    //   this.state.isUserStories === false
                    ? (
                      this.state.Inpercent ? (
                        <HighchartsReact
                          highcharts={Highcharts}
                          options={this.chartOptionsInPercent()}
                        />
                      ) : (
                        <HighchartsReact
                          highcharts={Highcharts}
                          options={this.chartOptions()}
                        />
                      )
                    ) : (
                      <div>
                        {this.state.isTestCase !== true
                          ? (
                            this.state.Inpercent ? (
                              <HighchartsReact
                                highcharts={Highcharts}
                                options={this.RequirementchartOptionsInPercent()}
                              />
                            ) : (
                              <HighchartsReact
                                highcharts={Highcharts}
                                options={this.RequirementchartOptions()}
                              />
                            )) : (null)}
                      </div>
                    )}
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-4">
            <div className="row cs-btn-group open-defects-btn">
              <div className={this.state.tableView ? "col-md-4" : "col-md-3"}>
                <Button
                  type="primary"
                  onClick={(event) =>
                    this.DefectByStatus(config.allDefects, config.allDefects)
                  }
                  className={this.state.StatusButton}
                >
                  By Status
                </Button>
              </div>
              <div className={this.state.tableView ? "col-md-4" : "col-md-3"}>
                <Button
                  type="primary"
                  className={this.state.AssigneeButton}
                  onClick={(event) =>
                    this.DefectByAssignee(config.allDefects, config.allDefects)
                  }
                >
                  By Assignee
                </Button>
              </div>
              <div className={this.state.tableView ? "col-md-4" : "col-md-3"} style={{
                display:
                  this.state.tableView
                    ? "none"
                    : "block",
              }}>
                <Button
                  // type="primary"
                  onClick={(event) =>
                    this.DefectByComponent(config.allDefects, config.allDefects)
                  }
                  className={this.state.ComponentButton}
                >
                  By Epic
                </Button>
              </div>
              <div className={this.state.tableView ? "col-md-4" : "col-md-3"} >
                <div className="row switch-icon-div">
                  <div className={this.state.chartcolor ? ("col-md-6 switch") : ("col-md-6 switch1")} onClick={(event) => this.DefectView(true)}>
                    <BarChartOutlined />
                  </div>
                  <div className={this.state.tablecolor ? ("col-md-6 switch") : ("col-md-6 switch1")} onClick={(event) => this.DefectView(false)}>
                    <UnorderedListOutlined />
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12 open_defeccts open-defects-chart">
                <h5>
                  {/* DEFECTS -{" "} */}
                  {this.props.projectToggle !== undefined
                    ? this.DefectsName()
                    : config.allDefects.toUpperCase()}
                </h5>
                <div className="row cs-btn-group">
                  <div className="col-md-12 mvp-btn">
                    <div style={{ float: "left", marginLeft: "auto", marginRight: "10px" }}>
                      <Button
                        className={allDefects}
                        onClick={(event: any) =>
                          this.DefectsCategories(config.allDefects)
                        }
                      >
                        All
                      </Button>
                    </div>
                    <div style={{ float: "left", marginLeft: "auto", marginRight: "10px" }}>
                      <Button
                        className={openDefects}
                        onClick={(event: any) =>
                          this.DefectsCategories(config.openDefects)
                        }
                      >
                        Open
                      </Button>
                    </div>
                    <div style={{ float: "left", marginLeft: "auto", marginRight: "10px" }}>
                      <Button
                        className={storyDefects}
                        onClick={(event: any) =>
                          this.DefectsCategories(config.storyDefects)
                        }
                      >
                        Story Defects
                      </Button>
                    </div>
                    {this.props.projectToggle !== config.notmvp ? (
                      <div style={{ float: "right", marginLeft: "auto", marginRight: "10px" }}>
                        <Button
                          onClick={(event: any) =>
                            this.DefectsTypes(config.notmvp)
                          }
                        >
                          NOT MVP
                        </Button>
                      </div>
                    ) : null}
                    {this.props.projectToggle !== config.mvp ? (
                      <div style={{ float: "right", marginLeft: "auto", marginRight: "10px" }}>
                        <Button
                          onClick={(event: any) =>
                            this.DefectsTypes(config.mvp)
                          }
                        >
                          MVP
                        </Button>
                      </div>
                    ) : null}
                    {this.props.projectToggle !== config.allDefects ? (
                      <div style={{ float: "right", marginLeft: "auto", marginRight: "10px" }}>
                        <Button
                          onClick={(event: any) =>
                            this.DefectsTypes(config.allDefects)
                          }
                        >
                          TOTAL DEFECTS
                        </Button>
                      </div>
                    ) : null}
                  </div>
                </div>
                {this.state.tableView ? (
                  <div className="defects-table-projectview">
                    <table
                      className="table"
                      ref={(table) => {
                        this.table = table;
                      }}
                    >
                      <thead>
                        <tr className="defect-header">{this.generateColumns()}</tr>
                      </thead>
                      <tbody className="defect-table-bdy">{this.generateRows()}</tbody>
                    </table>
                  </div>
                ) : (
                  <Bar data={this.state.DefectData} options={this.options} />
                )}
              </div>
            </div>
          </div>
          <br></br>
          <div className="col-md-4 cs-btn-group">
            <div className="row">
              {/* <div className="col-md-4">
                <Button
                  type="primary"
                  className={this.state.UserStoriesButton}
                  onClick={(event) => this.UserStories(config.allstories, config.stories)}
                >
                  User Stories
                </Button>
              </div> */}
              <div className="col-md-4">
                <Button
                  type="primary"
                  className={this.state.UserStoriesButton}
                  onClick={(event) => this.EpicType(config.stories)}
                >
                  User Stories
                </Button>
              </div>
              <div className="col-md-4">
                <Button
                  onClick={(event) => this.UserStoriesPerEpic()}
                  type="primary"
                  className={this.state.epicbtn}
                >
                  UserStories per Epic
                </Button>
              </div>
              <div className="col-md-4">
                <Button
                  onClick={(event) => this.AllEpics()}
                  type="primary"
                  className={this.state.allEpicsbtn}
                >
                  All Epics
                </Button>
              </div>
            </div>
            <div className="test-execution test-execcution-chart">
              <h5>
                {this.state.UserStoriesButton === "active"
                  ? (
                    `USER STORIES -${this.state.storyType.toUpperCase()}`
                  ) : (this.state.epicbtn === "active" ? ("User Stories per Epic") : ("All Epics")

                  )}
              </h5>
              <div className="row">
                <div className="col-md-12">
                  {this.state.storyType !== config.mvp ? (
                    <div
                      style={{
                        float: "left",
                        marginLeft: "5px",
                        marginRight: "5px",
                      }}
                    >
                      <Button
                        onClick={(event) => this.userStoriesType(config.mvp)}
                        style={{
                          display: this.state.UserStoryHideInPercentNumbers && !this.state.UserStoriesBack && this.state.allEpicsbtn !== "active" && this.state.epicbtn !== "active"
                            ? "block"
                            : "none",
                        }}
                      >
                        MVP
                      </Button>
                    </div>
                  ) : null}
                  {this.state.storyType !== config.notmvp ? (
                    <div
                      style={{
                        float: "left",
                        marginLeft: "5px",
                        marginRight: "5px",
                      }}
                    >
                      <Button
                        onClick={(event) =>
                          this.userStoriesType(config.notmvp)
                        }
                        style={{
                          display: this.state.UserStoryHideInPercentNumbers && !this.state.UserStoriesBack && this.state.allEpicsbtn !== "active" && this.state.allEpicsbtn !== "active" && this.state.epicbtn !== "active"
                            ? "block"
                            : "none",
                        }}
                      >
                        NOT MVP
                      </Button>
                    </div>
                  ) : null}
                  {this.state.storyType !== config.allstories ? (
                    <div
                      style={{
                        float: "left",
                        marginLeft: "5px",
                        marginRight: "5px",
                      }}
                    >
                      <Button
                        onClick={(event) =>
                          this.userStoriesType(config.allstories)
                        }
                        style={{
                          display: this.state.UserStoryHideInPercentNumbers && !this.state.UserStoriesBack && this.state.allEpicsbtn !== "active" && this.state.allEpicsbtn !== "active" && this.state.epicbtn !== "active"
                            ? "block"
                            : "none",
                        }}
                      >
                        ALL STORIES
                      </Button>
                    </div>
                  ) : null}
                  <div
                    style={{
                      float: "right",
                      marginLeft: "5px",
                      marginRight: "5px",
                    }}
                  >
                    <Button
                      className={this.state.UserstoriesInNumbermbersButton}
                      onClick={this.UserStoriesInNumbers}
                      style={{
                        //marginLeft: "auto",
                        display: this.state.UserStoryHideInPercentNumbers && !this.state.UserStoriesBack && this.state.allEpicsbtn !== "active" && this.state.epicbtn !== "active"
                          ? "block"
                          : "none",
                      }}
                    >
                      #
                    </Button>
                  </div>
                  <div
                    style={{
                      float: "right",
                      marginLeft: "5px",
                      marginRight: "5px",
                    }}
                  >
                    <Button
                      className={this.state.UserstoriesInpercentButton}
                      onClick={this.UserStoriesInPercent}
                      style={{
                        //marginLeft: "auto",
                        display: this.state.UserStoryHideInPercentNumbers && !this.state.UserStoriesBack && !this.state.UserStoriesBack && this.state.allEpicsbtn !== "active" && this.state.epicbtn !== "active"
                          ? "block"
                          : "none",
                      }}
                    >
                      %
                    </Button>
                  </div>
                  <div
                    style={{
                      float: "right",
                      marginLeft: "auto",
                      marginRight: "5px",
                    }}
                  >
                    <Button
                      onClick={this.storiesBack}
                      className="active"
                      style={{
                        //marginLeft: "auto",
                        display: this.state.UserStoriesBack
                          ? "block"
                          : "none",
                      }}
                    >
                      Back
                    </Button>
                  </div>
                  <div style={{ float: "right", marginLeft: "auto", marginRight: "5px" }}>
                    <div className="row switch-btn" style={{
                      //  display: this.state.UserStoryHideInPercentNumbers && !this.state.UserStoriesBack
                      display: !this.state.UserStoriesBack
                        ? "block"
                        : "none",
                      // display: this.state.isUserStories && !this.state.UserStoriesBack && this.state.HideInPercentNumbers ? "block" : "none",
                    }}>
                      <div className="row switch-icon-story">
                        <div className={this.state.UserStorychartcolor ? ("col-md-6 switch") : ("col-md-6 switch1")} onClick={(event) => this.UserStoryView(true)}>
                          <BarChartOutlined />
                        </div>
                        <div className={this.state.UserStorytablecolor ? ("col-md-6 switch") : ("col-md-6 switch1")} onClick={(event) => this.UserStoryView(false)}>
                          <UnorderedListOutlined />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  {this.state.userStoryTable ? (
                    <div className="story-table-projectview">
                      <table
                        className="table"
                        ref={(table) => {
                          this.table = table;
                        }}
                      >
                        <thead>
                          <tr className="defect-header">{this.generateStoryColumns()}</tr>
                        </thead>
                        <tbody className="defect-table-bdy">{this.generateStoryRows()}</tbody>
                      </table>
                    </div>
                  ) : (
                    this.state.UserStoriesInpercent ? (
                      <HighchartsReact
                        highcharts={Highcharts}
                        options={this.StorieschartOptionsInPercent()}
                      />
                    ) : (
                      <HighchartsReact
                        highcharts={Highcharts}
                        options={this.StorieschartOptions()}
                      />
                    )
                  )}
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-6 cs-btn-group"></div>
          <div className="content-info-section">
            <div className="row bg-sec mb-3">
              <div className="col-md-12">
                <h5 className="float-left">Open Risks & Issues</h5>
                <div className="col-md-12 float-left pl-0">
                  <p>
                    {this.validatingProjectData_OpenRiskAndIssues() ? (
                      <div dangerouslySetInnerHTML={{ __html: riskIssues }} />
                    ) : (
                      "No Data Available"
                    )}
                  </p>
                </div>
              </div>
            </div>
            <div className="row bg-sec mb-3">
              <div className="col-md-12">
                <h5 className="float-left">Detailed Test Status</h5>
              </div>
              <div className="col-md-12  pl-0">
                <p className="float-left">
                  {this.validatingProjectData_DetailedTestStatus() ? (
                    <div className="detail-status-alignment"
                      dangerouslySetInnerHTML={{ __html: DetailedTestStatus }}
                    />
                  ) : (
                    <div className="detail-status-alignment">No Data Available</div>

                  )}
                </p>
              </div>
            </div>
            <div className="row bg-sec mb-3">
              <div className="col-md-12">
                <h5 className="float-left">Test Key Message History</h5>
              </div>
              <div className="col-md-12">
                <p className="float-left">
                  {this.validatingProjectData_TestKeyMessageHistory() ? (
                    <div className="detail-status-alignment"
                      dangerouslySetInnerHTML={{ __html: HistoryTestKeyMessage }}
                    />
                  ) : (
                    <div className="detail-status-alignment ml-0">No Data Available</div>

                  )}
                </p>
              </div>
            </div>
            <div className="row mt-3">
              <div className="col-md-12 bg-sec">
                <h5 className="float-left">Release History</h5>
                <Table
                  bordered
                  columns={mycolumns}
                  dataSource={dataItems}
                  pagination={false}
                />
              </div>
            </div>
          </div>
          <Modal
            title="PDF Report-Preview"
            centered
            visible={this.state.modalVisible}
            onOk={() => this.GeneratePdf()}
            onCancel={() => this.CancelModal()}
          >
            {" "}
            <p className="sm-report-terms-condition">
              <Checkbox
                checked={this.props.detailView}
                onChange={this.DetailViewCheck}
              />
              &nbsp; Add detailed test execution report graph
            </p>
            <div className="sm-modal-content">
              <div className="sm-report-option">
                <div>
                  Doc Type
                </div>
                <div>
                  <Select
                    defaultValue="TSR"
                    style={{ width: 300 }}
                    onChange={this.handleChangeDocType}
                  >
                    <Option value="TSR">Test status Report</Option>
                    <Option value="TR4UAT">
                      Final CCB gating Test Report before UAT
                    </Option>
                    <Option value="TR4PROD">
                      Final CCB gating Test Report before PROD
                    </Option>
                  </Select>
                </div>
              </div>
              <div className="sm-report-option pb-0">
                <div>
                  Gate.Version
                </div>
                <div>
                  <Select
                    defaultValue="G1"
                    style={{ width: 300 }}
                    onChange={this.handleChangeGateVersion}
                  >
                    <Option value="G1">Gate 1: CCB Startup</Option>
                    <Option value="G2">Gate 2: R4SPRINT</Option>
                    <Option value="G3">Gate 3: R4UAT</Option>
                    <Option value="G4">Gate 4: R4PROD</Option>
                  </Select>
                </div>
              </div>
            </div>
          </Modal>
        </div>
        <Tooltip title="Back to Top">
          <BackTop />
        </Tooltip>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  milestoneDatas: state.summaryData.milestoneDatas,
  summaryDatas: state.summaryData.summaryDatas,
  summaryProjectDatas: state.summaryData.summaryProjectDatas,
  defectByStatus: state.defectData.bystatus,
  defectByAssignee: state.defectData.byassignee,
  defectByDomain: state.defectData.bydomain,
  defectByComponent: state.defectData.bycomponent,
  projectViewData: state.projectView.projectDatas,
  releaseHistory: state.projectView.releaseDatas,
  projectDatas: state.summaryData.projectDatas,
  allDefectData: state.defectData.allDefects,
  testResults: state.testExecution.testResults,
  componentData: state.projectView.componentData,
  requirementData: state.testExecution.testRequirement,
  userStoriesData: state.testExecution.userStories,
  detailView: state.projectView.detailedView,
  projectToggle: state.projectView.projectToggle,
  projectdefectToggle: state.projectView.defectToggle,
  epicbtn: state.projectView.epicview,
  epicLinks: state.defectData.epicLink,
});

const WrappedCreatePrrojectView = Form.create({ name: "summary" })(ProjectView);

export default connect(mapStateToProps, {
  DefectByStatusCharts,
  DefectByAssigneeCharts,
  DefectByComponentCharts,
  DefectByDomainsCharts,
  InitialDefectByAssigneeCharts,
  StatusChartsByProjectLevel,
  TestExecutionStatus,
  getChartData,
  inmemoryProjectURLStore,
  pdfPreviewSelection,
  inmemoryDetailedView,
  storeDocType,
  storeGateVersion,
  DefectsToggle,
  ProjectDefectsCategories,
  InitialStatusChartsByProjectLevel
})(WrappedCreatePrrojectView);
