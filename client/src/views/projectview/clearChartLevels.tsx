import {
  SubFolderSetter,
  SubFolder2Setter,
  SubFolder3Setter,
  SubFolder4Setter,
  SubFolder5Setter,
  SubFolder6Setter,
  SubFolder8Setter,
  MainSubFolderSetter,
  RequirementSubFolderSetter,
  RequirementSubFolder2Setter,
  RequirementSubFolder3Setter,
  RequirementSubFolder4Setter,
  RequirementSubFolder5Setter,
  RequirementSubFolder8Setter,
  RequirementSubFolder6Setter,
  SubFolderSetterForDefects
} from "../chartFunctions/TestExecutionFolderSet";
import { config } from "../../config/configs";

export const clearRequirementLevels = (
) => {
  RequirementSubFolderSetter("");
  RequirementSubFolder2Setter("");
  RequirementSubFolder3Setter("");
  RequirementSubFolder4Setter("");
  RequirementSubFolder5Setter("");
  RequirementSubFolder6Setter("");
  RequirementSubFolder8Setter("");
};

export const clearTestLevels = () => {
  MainSubFolderSetter("");
  SubFolderSetter("");
  SubFolder2Setter("");
  SubFolder3Setter("");
  SubFolder4Setter("");
  SubFolder5Setter("");
  SubFolder6Setter("");
  SubFolder8Setter("");
  SubFolderSetterForDefects("");

}

export const FormingLinks = (columns, row, statusbtn, index, jiraURL, defects, projectData, defectType) => {
  let status = row[0];
  let priority = columns[index];
  let statusConstruction;
  if (status === "Backlog") {
    status = config.Defect_Backlog;
  } else if (status === "Analysis") {
    status = config.Defect_Analysis;
  } else if (status === "Development") {
    status = config.Defect_Development;
  } else if (status === "In Test") {
    status = config.Defect_InTest;
  } else if (status === "Closed") {
    status = config.Defect_Closed;
  } else if (status === "To Validate") {
    status = config.Defect_ToValidate;
  } else if (status === "Open") {
    status = config.Defect_Open
  }
  if (statusbtn === config.assigneebtn) {
    let assigneeId = findingAssigneeID(defects, projectData, status, priority);
    if (assigneeId === "UnAssigned") {
      if (defectType === config.allDefects) {
        statusConstruction = `assignee is null and priority = ${priority}`;
      } else {
        statusConstruction = `status != Closed and assignee is null and priority = ${priority}`;
      }
    } else {
      if (defectType === config.allDefects) {
        statusConstruction = `assignee = ${assigneeId} and priority = ${priority}`;
      } else {
        statusConstruction = `status != Closed and assignee = ${assigneeId} and priority = ${priority}`;
      }
    }
  } else {
    statusConstruction = `status in (${status}) and priority = ${priority}`;
  }
  jiraURL = jiraURL.replace('status != Closed', statusConstruction);
  return jiraURL;
}

function findingAssigneeID(defects, projectData, status, priority) {
  if (defects.length !== 0 && projectData.length !== 0) {
    let DefectFilter = defects.filter((col) => {
      return col.ReleaseName === projectData[0].ReleaseName &&
        col.projectReference === projectData[0].ProjectReference &&
        col.assigneeName === status &&
        col.priority === priority
    });
    if (DefectFilter.length !== 0) {
      return DefectFilter[0].assigneeID;
    } else {
      return 0;
    }
  } else {
    return 0;
  }
}

export const FormingUserStoryLink = (userStoryURL, status, data, userStoryData, btnValue, projectRef, release, epicData,selectedEpic) => {
  if (btnValue !== config.epics && btnValue !== config.epicsperstory) {
    let projectKey = userStoryData.filter((project) => {
      return (
        project.projectName === data[0]
      );
    });
    projectKey = projectKey[0].projectKey;
    let TestStatus: any;
    if (status === "Backlog") {
      TestStatus = config.Story_Backlog;
    } else if (status === "Analysis") {
      TestStatus = config.Story_Analysis;
    } else if (status === "Development") {
      TestStatus = config.Story_Development;
    } else if (status === "In Test") {
      TestStatus = config.Story_InTest;
    } else if (status === "Closed") {
      TestStatus = config.Story_Closed;
    } else if (status === "To Validate") {
      TestStatus = config.Story_ToValidate;
    } else if (status === "Open") {
      TestStatus = config.Story_Open
    }
    let statusConstruction = `and project in (${projectKey.toString()}) and status in (${TestStatus.toString()}) ORDER BY created DESC`;
    if(selectedEpic.epicLink !== config.allEpic){
      let epicadd = `and "Epic Link" = ${selectedEpic.epicLink.toString()}`;
      statusConstruction = `and project in (${projectKey.toString()}) and status in (${TestStatus.toString()}) ${epicadd} ORDER BY created DESC`;
    }
    userStoryURL = userStoryURL.replace('ORDER BY created DESC', statusConstruction);
    return userStoryURL;
  } else if (btnValue !== config.epics && btnValue === config.epicsperstory) {
    let projectKey = userStoryData.filter((project) => {
      return (
        project.ProjectReference === projectRef
        &&
        project.fixVersions === release
        &&
        project.epicName === data[0]
      );
    });
    if (projectKey.length !== 0) {
      let Key = projectKey[0].projectKey;
      let EpicLink = projectKey.filter((project) => {
        return (
          project.epicName === data[0]
        );
      });
      if (EpicLink.length !== 0) {
        EpicLink = EpicLink[0].epicLink;
        let TestStatus: any;
        if (status === "Backlog") {
          TestStatus = config.Story_Backlog;
        } else if (status === "Analysis") {
          TestStatus = config.Story_Analysis;
        } else if (status === "Development") {
          TestStatus = config.Story_Development;
        } else if (status === "In Test") {
          TestStatus = config.Story_InTest;
        } else if (status === "Closed") {
          TestStatus = config.Story_Closed;
        } else if (status === "To Validate") {
          TestStatus = config.Story_ToValidate;
        } else if (status === "Open") {
          TestStatus = config.Story_Open
        }
        let epicadd = `and "Epic Link" = ${EpicLink.toString()}`;
        let statusConstruction = `and project in (${Key.toString()}) and status in (${TestStatus.toString()}) ${epicadd} ORDER BY created DESC`;
        userStoryURL = userStoryURL.replace('ORDER BY created DESC', statusConstruction);
        return userStoryURL;
      }
    }

  }
  else {
    let EpicURL = "https://jira.pvgroup.be/browse/";
    let projectKey = epicData.filter((epic) => {
      return (
        epic.epicName === data[0]
      );
    });
    let EpicId = projectKey[0].epicLink;
    return EpicURL + EpicId;
  }
}
