export const BussinessDomain = [
  { Key: 1, Value: "All" },
  { Key: 2, Value: "BDP" },
  { Key: 3, Value: "Claims" },
  { Key: 4, Value: "Compliance" },
  { Key: 5, Value: "DCC" },
  { Key: 6, Value: "EB" },
  { Key: 7, Value: "Facility" },
  { Key: 8, Value: "Finance & Admin" },
  { Key: 9, Value: "Front" },
  { Key: 10, Value: "HR" },
  { Key: 11, Value: "Infra" },
  { Key: 12, Value: "Life" },
  { Key: 13, Value: "Non Life" },
  { Key: 14, Value: "Other" },
  { Key: 15, Value: "Sales & Marketing" },
  { Key: 16, Value: "Channels" },
  { Key: 17, Value: "PIM" }
]

export const usergroup = [
  { Key: 0, Value: "All" },
  { Key: 1, Value: "Agent" },
  { Key: 2, Value: "Broker" },
]
