import React, { Component } from "react";
import { connect } from "react-redux";
import Summary from "../summary/summary";
import {
  fetchMilestones,
  fetchProject,
  fetchSummary,
  fetchSummaryProject,
  fetchLocalData,
} from "../../redux/actions/summary/summaryAction";
import {
  fetchTestResultsByRelease,
  fetchTestExecutionByReleaseName,
  fetchTestResults
} from "../../redux/actions/testExecution/testExecutionAction";
import {
  RefreshProjectSelect,
  pdfPreviewSelection,
  ReleasePageView,
} from "../../redux/actions/headers/headersAction";
export let Navigation = true;
export function NavigatorSet() {
  Navigation = false;
}

type MyProps = {
  releaseDatas: any;
  fetchMilestones: any;
  fetchProject: any;
  fetchSummary: any;
  fetchSummaryProject: any;
  fetchLocalData: any;
  fetchTestResultsByRelease: any;
  fetchTestExecutionByReleaseName: any;
  RefreshProjectSelect: any;
  pdfPreviewSelection: any;
  ReleasePageView: any;
  fetchTestResults: any;
  currentRelease: any;
};

class Dashboard extends Component<MyProps> {
  constructor(props: any) {
    super(props);
    this.state = {
      consversionPanel: true,
    };
  }

  componentWillMount() {
    this.props.ReleasePageView(false);
    if (Navigation === true ) {
        this.props.fetchLocalData();
        this.props.RefreshProjectSelect("All Projects");
        this.props.pdfPreviewSelection(true);
        if (this.props.releaseDatas.length !== 0) {
          this.props.fetchMilestones(this.props.currentRelease.ReleaseName);
          this.props.fetchProject(this.props.currentRelease.ReleaseName);
          this.props.fetchSummary(this.props.currentRelease.ReleaseName);
          this.props.fetchTestResultsByRelease(this.props.currentRelease.ReleaseName);
        }
    }
  }

  render() {
    return <Summary></Summary>;
  }
}
const mapStateToProps = (state: any) => ({
  // releaseDatas: state.summaryData.releaseDatas,
  releaseDatas: state.summaryData.releaseDatasfilter,
  currentRelease: state.summaryData.currentRelease
});
export default connect(mapStateToProps, {
  fetchMilestones,
  fetchProject,
  fetchSummary,
  fetchSummaryProject,
  fetchLocalData,
  fetchTestResultsByRelease,
  fetchTestExecutionByReleaseName,
  RefreshProjectSelect,
  pdfPreviewSelection,
  ReleasePageView,
  fetchTestResults
})(Dashboard);
