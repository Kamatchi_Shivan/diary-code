import React from "react";
import { Redirect } from "react-router-dom";

// Layout Types
import { DefaultLayout,LoginLayout } from "./layouts";

// Route Views
import Dashboard from "./views/dashboard/Dashboard";
import Errors from "./views/errors/Errors";
import ProjectView from "./views/projectview/projectview";
import ProjectViewPdf from "./views/pdfGeneration/pdfProjectView";
import Loading from "./views/splashScreen/loding";

export default [
  {
    path: "/",
    exact: true,
    layout: DefaultLayout,
    component: () => <Redirect to="/loading"/>
  },
  {
  path: "/loading",
  layout: LoginLayout,
  component: Loading
  },
  {
    path: "/home",
    layout: DefaultLayout,
    component: Dashboard
  },
  {
    path: "/errors",
    layout: DefaultLayout,
    component: Errors
  },

  {
    path: "/projectview",
    layout: DefaultLayout,
    component: ProjectView
  },
  {
    path: "/pdfview",
    layout: DefaultLayout,
    component: ProjectViewPdf
  }
];
