import React from "react";

const SmileyComponent = (props) => {
  return <img src={props.img} alt={props.desc} width="35"/>;
};

export default SmileyComponent;
