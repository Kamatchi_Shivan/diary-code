import { createBrowserHistory } from 'history';
export default class Global {
    static history = createBrowserHistory();
}
