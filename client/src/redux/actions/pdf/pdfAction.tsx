import {
  MAIL_PDF,
  MERGE_PDF,
  NETWORK_PDF_STORE,
  DEFECTS_TOGGLE_PDF,
  DEFECTS_CATEGORY_PDF
} from "../types";
import fetch from "../../../utils/leoFetch";
import { config } from "../../../config/configs";
import axios from "../../../utils/leoAxios";

export const MailingPDF = (value: any) => (dispatch: any) => {
  fetch(config.serviceUrl + config.pdfmailer, {
    body: JSON.stringify(value),
    headers: {
      "content-type": "application/json",
    },
    method: "POST",
  })
    .then((res) => res.json())
    .then((pdfMail) => {
      dispatch({
        payload: pdfMail,
        type: MAIL_PDF,
      });
    });
};

export const MergingPdf = (value: any) => (dispatch: any) => {
  fetch(config.serviceUrl + config.pdfmerger, {
    body: JSON.stringify(value),
    headers: {
      "content-type": "application/json",
    },
    method: "POST",
  })
    .then((res) => res.json())
    .then((Merger) => {
      dispatch({
        payload: Merger,
        type: MERGE_PDF,
      });
    });
};

export const StoringInNetworkPath = (value: any) => (dispatch: any) => {
  let body =  JSON.stringify(value);
  axios
  .post(config.dataUploadsURl + config.pdfnetworkStore, body, {
    headers: {
      "Content-Type": "application/json",
    },
  })
    // .then((res) => res.json())
    .then((Merger) => {
      console.log(Merger.data)
      dispatch({
        payload: Merger.data,
        type: NETWORK_PDF_STORE,
      });
    });
};

export const ClearingPdfMessage = () => (dispatch: any) => {
  dispatch({
    payload: {},
    type: MERGE_PDF,
  });
};

export const ClearingNetworkPdfMessage = () => (dispatch: any) => {
  dispatch({
    payload: {},
    type: NETWORK_PDF_STORE,
  });
};

export const DefectsTogglePDF = (data: any) => (dispatch: any, option: any) => {
  dispatch({
    payload: data,
    type: DEFECTS_TOGGLE_PDF,
  });
};

export const PdfDefectsCategories = (data: any) => (
  dispatch: any,
  option: any
) => {
  dispatch({
    payload: data,
    type: DEFECTS_CATEGORY_PDF,
  });
};
