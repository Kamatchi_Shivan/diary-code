import {
  FETCH_PROJECT_DETAIL,
  RELEASE_HISTORY,
  FILTER_COMPONENT,
  PROJECT_URL,
  DETAILED_VIEW,
  DOC_TYPE,
  GATE_VERSION,
  DEFECTS_TOGGLE_PROJECTVIEW,
  DEFECTS_CATEGORY_PROJECTVIEW,
  TEST_REPORTS,
} from "../types";
import fetch from "../../../utils/leoFetch";
import { config } from "../../../config/configs";

export const fetchProjectDetails = (
  projectReference: any,
  releaseName: any
) => (dispatch: any, option: any) => {
  fetch(
    config.serviceUrl +
      config.projectccb +
      releaseName +
      "/" +
      projectReference,
    option
  )
    .then((res) => res.json())
    .then((projectdetails) => {
      dispatch({
        payload: projectdetails.data,
        type: FETCH_PROJECT_DETAIL,
      });
    });
};

export const fetchReleaseHistory = (projectReference: any) => (
  dispatch: any,
  option: any
) => {
  fetch(
    config.serviceUrl + config.projectReleasehistory + projectReference,
    option
  )
    .then((res) => res.json())
    .then((releasehistory) => {
      dispatch({
        payload: releasehistory.data,
        type: RELEASE_HISTORY,
      });
    });
};

export const EmptyProjectDetails = () => (dispatch: any, option: any) => {
  dispatch({
    payload: [],
    type: FETCH_PROJECT_DETAIL,
  });
  dispatch({
    payload: [],
    type: RELEASE_HISTORY,
  });
};

export const inmemoryComponent = (ComponentData: any) => (
  dispatch: any,
  option: any
) => {
  dispatch({
    payload: ComponentData,
    type: FILTER_COMPONENT,
  });
};

export const inmemoryProjectURLStore = (URL: any) => (
  dispatch: any,
  option: any
) => {
  dispatch({
    payload: URL,
    type: PROJECT_URL,
  });
};

export const inmemoryDetailedView = (data: any) => (
  dispatch: any,
  option: any
) => {
  dispatch({
    payload: data,
    type: DETAILED_VIEW,
  });
};

export const storeDocType = (data: any) => (dispatch: any, option: any) => {
  dispatch({
    payload: data,
    type: DOC_TYPE,
  });
};

export const storeGateVersion = (data: any) => (dispatch: any, option: any) => {
  dispatch({
    payload: data,
    type: GATE_VERSION,
  });
};

export const DefectsToggle = (data: any) => (dispatch: any, option: any) => {
  dispatch({
    payload: data,
    type: DEFECTS_TOGGLE_PROJECTVIEW,
  });
};

export const ProjectDefectsCategories = (data: any) => (
  dispatch: any,
  option: any
) => {
  dispatch({
    payload: data,
    type: DEFECTS_CATEGORY_PROJECTVIEW,
  });
};


export const fetchTestReports = (
  projectReference: any,
  releaseName: any
) => (dispatch: any, option: any) => {
  fetch(
    config.serviceUrl +
      config.testreport +
      releaseName +
      "/" +
      projectReference,
    option
  )
    .then((res) => res.json())
    .then((testReports) => {
     console.log(testReports,10000)
      dispatch({
        payload: testReports.data,
        type: TEST_REPORTS,
      });
    });
};