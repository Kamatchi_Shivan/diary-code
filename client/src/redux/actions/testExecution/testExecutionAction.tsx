import {
  FETCH_TESTEXECUTION,
  FETCH_TEST_EXECUTION_BY_RELEASE,
  FETCH_REQUIREMENT,
  FETCH_USERSTORIES,
  PDF_DATA,
} from "../types";
import fetch from "../../../utils/leoFetch";
import { config } from "../../../config/configs";

export const fetchTestResultsByRelease = (releaseName: any) => (
  dispatch: any,
  option: any
) => {
  fetch(config.serviceUrl + config.testResults + releaseName, option)
    .then((res) => res.json())
    .then((tests) => {
      dispatch({
        payload: tests.data,
        type: FETCH_TESTEXECUTION,
      });
    });
};

export const fetchTestResults = () => (dispatch: any, option: any) => {
  console.log("Test Exection URL >>>>>>",config.serviceUrl + config.alltestResults)
  fetch(config.serviceUrl + config.alltestResults, option)
    .then((res) => res.json())
    .then((tests) => {
      console.log("Test Exection URL >>>>>>",tests)
      if(tests.messageCode === 'MSG200'){
        dispatch({
          payload: tests.data,
          type: FETCH_TESTEXECUTION,
        });
      }else{
        dispatch({
          payload: [],
          type: FETCH_TESTEXECUTION,
        });
      }
    })
};

/*Release Page Action */

export const fetchTestExecutionByReleaseName = () => (
  dispatch: any,
  option: any
) => {
  fetch(config.serviceUrl + config.allresults, option)
    .then((res) => res.json())
    .then((testexecution) => {
      dispatch({
        payload: testexecution.data,
        type: FETCH_TEST_EXECUTION_BY_RELEASE,
      });
    });
};

export const fetchTestResultsByProject = (
  releaseName: any,
  projectReference: any
) => (dispatch: any, option: any) => {
  fetch(
    config.serviceUrl +
      config.testresultsProject +
      releaseName +
      "/" +
      projectReference,
    option
  )
    .then((res) => res.json())
    .then((tests) => {
      dispatch({
        payload: tests.data,
        type: FETCH_TESTEXECUTION,
      });
    });
};

export const fetchTestExecutionByReleaseProject = (
  releaseName: any,
  projectReference: any
) => (dispatch: any, option: any) => {
  fetch(
    config.serviceUrl + config.results + releaseName + "/" + projectReference,
    option
  )
    .then((res) => res.json())
    .then((testexecution) => {
      dispatch({
        payload: testexecution.data,
        type: FETCH_TEST_EXECUTION_BY_RELEASE,
      });
    });
};

/* Requirement Coverage Action*/

export const fetchTestRequirement = () => (dispatch: any, option: any) => {
  fetch(config.serviceUrl + config.coverageAll, option)
    .then((res) => res.json())
    .then((tests) =>
      dispatch({
        payload: tests.data,
        type: FETCH_REQUIREMENT,
      })
    );
};

/* Requirement Coverage Action*/

export const fetchTestRequirementByRelease = (releaseName: any) => (dispatch: any, option: any) => {
  fetch(config.serviceUrl + config.coverage+releaseName, option)
    .then((res) => res.json())
    .then((tests) =>
      dispatch({
        payload: tests.data,
        type: FETCH_REQUIREMENT,
      })
    );
};

/* User Stories Action*/

export const fetchUserStories = () => (dispatch: any, option: any) => {
  fetch(config.serviceUrl + config.userStoriseall, option)
    .then((res) => res.json())
    .then((tests) =>
      dispatch({
        payload: tests.data,
        type: FETCH_USERSTORIES,
      })
    );
};

/* User Stories Action*/

export const fetchUserStoriesByRelease = (release:any) => (dispatch: any, option: any) => {
  fetch(config.serviceUrl + config.story+release, option)
    .then((res) => res.json())
    .then((tests) =>
      dispatch({
        payload: tests.data,
        type: FETCH_USERSTORIES,
      })
    );
};

/* User Stories Action*/

export const PdfComponentStore = (data: any) => (
  dispatch: any,
  option: any
) => {
  dispatch({
    payload: data,
    type: PDF_DATA,
  });
};
