import {
  DEFECTS_TOGGLE_SUMMARY,
  FETCH_DOMAIN,
  FETCH_ALLPROJECTS,
  FETCH_RELESE,
  FETCH_MILESTONE,
  FETCH_PROJECT,
  FETCH_SUMMARY,
  FETCH_FILTERED_SUMMARY,
  SELECTED_DOMAIN,
  ALL_DOMAIN,
  FILTER_FETCH_RELESE,
  FETCH_PROJECT_RELEASEFILTER,
  DEFECTS_CATEGORY_SUMMARY,
  FETCH_CURRENT_RELESE,
  FETCH_EPICLINK
} from "../types";
import fetch from "../../../utils/leoFetch";
import { config } from "../../../config/configs";

export const fetchDomain = () => (dispatch: any, option: any) => {
  fetch(config.serviceUrl + config.allDomains, option)
    .then((res) => res.json())
    .then((domains) => {
      dispatch({
        payload: domains.data,
        type: FETCH_DOMAIN,
      });
    });
};

export const fetchLocalData = () => (dispatch: any, option: any) => {
  fetch(config.serviceUrl + config.initialFetch, option)
    .then((res) => res.json())
    .then((res) => {
      dispatch({
        payload: res.FinalData.ReleaseData,
        type: FETCH_RELESE,
      });
      dispatch({
        payload: res.FinalData.currentRelease,
        type: FETCH_CURRENT_RELESE,
      });
      dispatch({
        payload: res.FinalData.Projectdata,
        type: FETCH_PROJECT,
      });
      dispatch({
        payload: res.FinalData.Summarydata,
        type: FETCH_SUMMARY,
      });
      dispatch({
        payload: res.FinalData.Milestonedata,
        type: FETCH_MILESTONE,
      });
    });
};


export const fetchEpicLinks = (ReleaseName:any,projectReference:any) => (dispatch: any, option: any) => {
  fetch(config.serviceUrl + config.epicLink+ReleaseName+"/"+projectReference, option)
    .then((res) => res.json())
    .then((epic) =>
      dispatch({
        payload: epic.data,
        type: FETCH_EPICLINK,
      })
    );
};


export const fetchRelease = () => (dispatch: any, option: any) => {
  fetch(config.serviceUrl + config.releases, option)
    .then((res) => res.json())
    .then((release) => {
      dispatch({
        payload: release.FinalData.releaseData,
        type: FETCH_RELESE,
      });
      dispatch({
        payload: release.FinalData.releaseData,
        type: FILTER_FETCH_RELESE,
      });
      dispatch({
        payload: release.FinalData.currentRelease,
        type: FETCH_CURRENT_RELESE,
      });
    });
};

export const inmemoryrelesedatareset = (releasedatas: any) => (
  dispatch: any,
  option: any
) => {
  dispatch({
    payload: releasedatas,
    type: FETCH_RELESE,
  });
};

export const fetchAllProjectsInitialRelease = () => (
  dispatch: any,
  option: any
) => {
  fetch(config.serviceUrl + config.projects, option)
    .then((res) => res.json())
    .then((release) =>
      dispatch({
        payload: release.data,
        type: FETCH_ALLPROJECTS,
      })
    );
};

export const fetchMilestones = (releaseName: any) => (
  dispatch: any,
  option: any
) => {
  fetch(config.serviceUrl + config.releasemilestones + releaseName, option)
    .then((res) => res.json())
    .then((milestones) => {
      if (milestones.data.length !== 0) {
        dispatch({
          payload: milestones.data[0],
          type: FETCH_MILESTONE,
        });
      } else {
        dispatch({
          payload: {},
          type: FETCH_MILESTONE,
        });
      }
    });
};

export const fetchProject = (releaseName: any) => (
  dispatch: any,
  option: any
) => {
  fetch(config.serviceUrl + config.projects + releaseName, option)
    .then((res) => res.json())
    .then((projects) => {
      dispatch({
        payload: projects.data,
        type: FETCH_PROJECT,
      });
      dispatch({
        payload: projects.data,
        type: FETCH_PROJECT_RELEASEFILTER,
      });
    });
};

export const fetchProjectreleasefilter = (releaseName: any) => (
  dispatch: any,
  option: any
) => {
  fetch(config.serviceUrl + config.projects + releaseName, option)
    .then((res) => res.json())
    .then((projects) => {
      dispatch({
        payload: projects.data,
        type: FETCH_PROJECT,
      });
    });
};

export const fetchSummary = (releaseName: any) => (
  dispatch: any,
  option: any
) => {
  fetch(config.serviceUrl + config.projectSummary + releaseName, option)
    .then((res) => res.json())
    .then((summary) => {
      dispatch({
        payload: summary.data,
        type: FETCH_SUMMARY,
      });
      dispatch({
        payload: summary.data,
        type: FETCH_FILTERED_SUMMARY,
      });
    });
};

export const fetchSummaryProject = (
  projectReference: any,
  releaseName: any
) => (dispatch: any, option: any) => {
  fetch(
    config.serviceUrl +
      config.projectSummary +
      releaseName +
      "/" +
      projectReference,
    option
  )
    .then((res) => res.json())
    .then((summaryproject) => {
      dispatch({
        payload: summaryproject.data,
        type: FETCH_SUMMARY,
      });
      dispatch({
        payload: summaryproject.data,
        type: FETCH_FILTERED_SUMMARY,
      });
    });
};

export const inmemoryfilteredsummarydata = (
  filteredproject: any,
  domain: any
) => (dispatch: any, option: any) => {
  dispatch({
    payload: filteredproject,
    type: FETCH_SUMMARY,
  });
  dispatch({
    payload: domain,
    type: SELECTED_DOMAIN,
  });
};

export const inmemorydomainclear = (domain: any) => (
  dispatch: any,
  option: any
) => {
  let data = [];
  data.push(domain);
  dispatch({
    payload: data,
    type: ALL_DOMAIN,
  });
};

export const SummaryDefectsToggle = (data: any) => (
  dispatch: any,
  option: any
) => {
  dispatch({
    payload: data,
    type: DEFECTS_TOGGLE_SUMMARY,
  });
};

export const SummaryDefectsCategories = (data: any) => (
  dispatch: any,
  option: any
) => {
  dispatch({
    payload: data,
    type: DEFECTS_CATEGORY_SUMMARY,
  });
};

export const ProjectClear = () => (
  dispatch: any,
  option: any
) => {
  dispatch({
    payload: [],
    type: FETCH_PROJECT,
  });
};
