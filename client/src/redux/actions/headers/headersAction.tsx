import { REFERESH_PROJECT, PDF_PREVIEW, FROM_RELEASE_VIEW } from "../types";

export const RefreshProjectSelect = (releaseName: any) => (
  dispatch: any,
  option: any
) => {
  dispatch({
    payload: releaseName,
    type: REFERESH_PROJECT,
  });
};

export const pdfPreviewSelection = (data: any) => (
  dispatch: any,
  option: any
) => {
  dispatch({
    payload: data,
    type: PDF_PREVIEW,
  });
};

export const ReleasePageView = (data: any) => (dispatch: any, option: any) => {
  dispatch({
    payload: data,
    type: FROM_RELEASE_VIEW,
  });
};
