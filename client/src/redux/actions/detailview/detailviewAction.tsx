import { FETCH_CCB } from "../types";
import fetch from "../../../utils/leoFetch";
import { config } from "../../../config/configs";

export const fetchCCB = (releaseName: any) => (dispatch: any, option: any) => {
  fetch(config.serviceUrl + config.projectccb + releaseName, option)
    .then((res) => res.json())
    .then((domains) => {
      dispatch({
        payload: domains.data,
        type: FETCH_CCB,
      });
    });
};
