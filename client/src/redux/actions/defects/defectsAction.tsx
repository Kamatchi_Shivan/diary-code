import {
  FETCH_BYSTATUS,
  FETCH_BYCOMPONENT,
  FETCH_BYASSINGEE,
  FETCH_BYDOMAIN,
  FETCH_BYPROJECT,
  FETCH_ALLDEFECTS,
} from "../types";
import fetch from "../../../utils/leoFetch";
import { config } from "../../../config/configs";

export const fetchAllDefects = () => (dispatch: any, option: any) => {
  fetch(config.serviceUrl + config.alldefectsurl, option)
    .then((res) => res.json())
    .then((defects) => {
      dispatch({
        payload: defects.data,
        type: FETCH_ALLDEFECTS,
      });
    });
};

export const fetchDefectsByRelease = (ReleaseName:any) => (dispatch: any, option: any) => {
  fetch(config.serviceUrl + config.defectsByRelease+ReleaseName, option)
    .then((res) => res.json())
    .then((defects) => {
      dispatch({
        payload: defects.data,
        type: FETCH_ALLDEFECTS,
      });
    });
};

export const fetchDefectsByStatus = () => (dispatch: any, option: any) => {
  fetch(config.serviceUrl + config.defectsStatus, option)
    .then((res) => res.json())
    .then((defects) => {
      dispatch({
        payload: defects.data,
        type: FETCH_BYSTATUS,
      });
    });
};

export const fetchDefectsByComponent = () => (dispatch: any, option: any) => {
  fetch(config.serviceUrl + config.defectsComponent, option)
    .then((res) => res.json())
    .then((defects) => {
      dispatch({
        payload: defects.data,
        type: FETCH_BYCOMPONENT,
      });
    });
};

export const fetchDefectsByAssignee = () => (dispatch: any, option: any) => {
  fetch(config.serviceUrl + config.defectsAssignee, option)
    .then((res) => res.json())
    .then((defects) => {
      dispatch({
        payload: defects.data,
        type: FETCH_BYASSINGEE,
      });
    });
};

export const fetchDefectsByDomain = () => (dispatch: any, option: any) => {
  fetch(config.serviceUrl + config.defectsDomain, option)
    .then((res) => res.json())
    .then((defects) => {
      dispatch({
        payload: defects.data,
        type: FETCH_BYDOMAIN,
      });
    });
};

export const fetchDefectsByProject = () => (dispatch: any, option: any) => {
  fetch(config.serviceUrl + config.defectsProject, option)
    .then((res) => res.json())
    .then((defects) => {
      dispatch({
        payload: defects.data,
        type: FETCH_BYPROJECT,
      });
    });
};
