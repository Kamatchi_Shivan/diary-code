import {
  REFERESH_PROJECT,
  PDF_PREVIEW,
  FROM_RELEASE_VIEW
 } from "../../actions/types";

 const initialState = {
  projectName:"All Projects",
  isnotpdfview: true,
  fromReleasePage: false,
 };

 export default function(state = initialState, action:any) {
   switch (action.type) {
    case PDF_PREVIEW:
      return {
        ...state,
        isnotpdfview: action.payload
      };
         case REFERESH_PROJECT:
          return {
            ...state,
            projectName: action.payload
          };
          case FROM_RELEASE_VIEW:
          return {
            ...state,
            fromReleasePage: action.payload
          };
     default:
         return state;
   }
 }
