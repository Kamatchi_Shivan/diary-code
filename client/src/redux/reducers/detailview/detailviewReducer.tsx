import {
  FETCH_BYSTATUS,FETCH_CCB   
 } from "../../actions/types";
 
 const initialState = {
   bystatus: [],
   ccbdetails:[]
 };
 
 export default function(state = initialState, action:any) {
   //console.log("in reducer");
   switch (action.type) {
       case FETCH_BYSTATUS:
         return {
           ...state,
           bystatus: action.payload
         };  
         case FETCH_CCB:
          return {
            ...state,
            ccbdetails: action.payload
          };         
     default:
         return state;
   }
 }
 