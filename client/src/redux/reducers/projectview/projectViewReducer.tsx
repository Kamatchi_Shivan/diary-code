import {
  FETCH_PROJECT_DETAIL,
  RELEASE_HISTORY,
  FILTER_COMPONENT,
  PROJECT_URL,
  DETAILED_VIEW,
  DOC_TYPE,
  GATE_VERSION,
  DEFECTS_TOGGLE_PROJECTVIEW,
  DEFECTS_CATEGORY_PROJECTVIEW,
  TEST_REPORTS
} from "../../actions/types";

const initialState = {
  projectDatas: [],
  releaseDatas: [],
  componentData: {  epicLink:"All Epic's",epicName: "All Epic's"},
  projectURL: "",
  detailedView: false,
  doctype: "",
  gateversion: "",
  projectToggle: "",
  defectToggle: "",
  testReports:[]
};

export default function (state = initialState, action: any) {
  //console.log("in reducer");
  switch (action.type) {
    case FETCH_PROJECT_DETAIL:
      return {
        ...state,
        projectDatas: action.payload,
      };
    case RELEASE_HISTORY:
      return {
        ...state,
        releaseDatas: action.payload,
      };
    case FILTER_COMPONENT:
      return {
        ...state,
        componentData: action.payload,
      };
    case PROJECT_URL:
      return {
        ...state,
        projectURL: action.payload,
      };
    case DETAILED_VIEW:
      return {
        ...state,
        detailedView: action.payload,
      };
    case DOC_TYPE:
      return {
        ...state,
        doctype: action.payload,
      };
    case GATE_VERSION:
      return {
        ...state,
        gateversion: action.payload,
      };
    case DEFECTS_TOGGLE_PROJECTVIEW:
      return {
        ...state,
        projectToggle: action.payload,
      };
    case DEFECTS_CATEGORY_PROJECTVIEW:
      return {
        ...state,
        defectToggle: action.payload,
      };
      case TEST_REPORTS:
        return {
          ...state,
          testReports: action.payload,
        };
    default:
      return state;
  }
}
