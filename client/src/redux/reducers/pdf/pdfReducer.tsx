import {
  MAIL_PDF,
  MERGE_PDF,
  NETWORK_PDF_STORE,
  DEFECTS_TOGGLE_PDF,
  DEFECTS_CATEGORY_PDF,
} from "../../actions/types";

const initialState = {
  mailpdf: {},
  Merger: {},
  networkstore: {},
  pdfToggle: "",
  defectToggle: "",
};

export default function (state = initialState, action: any) {
  switch (action.type) {
    case MAIL_PDF:
      return {
        ...state,
        mailpdf: action.payload,
      };
    case MERGE_PDF:
      return {
        ...state,
        Merger: action.payload,
      };
    case NETWORK_PDF_STORE:
      return {
        ...state,
        networkstore: action.payload,
      };
    case DEFECTS_TOGGLE_PDF:
      return {
        ...state,
        pdfToggle: action.payload,
      };
    case DEFECTS_CATEGORY_PDF:
      return {
        ...state,
        defectToggle: action.payload,
      };
    default:
      return state;
  }
}
