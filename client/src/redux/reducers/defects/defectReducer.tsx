import {
 FETCH_BYSTATUS ,
 FETCH_BYCOMPONENT ,
 FETCH_BYASSINGEE,
 FETCH_BYDOMAIN ,
 FETCH_BYPROJECT,
 FETCH_ALLDEFECTS,
 FETCH_EPICLINK
} from "../../actions/types";

const initialState = {
  allDefects: [],
  bystatus: [],
  bycomponent:[],
  byassignee:[],
  bydomain:[],
  byproject:[],
  epicLink:[]
};

export default function(state = initialState, action:any) {
  //console.log("in reducer");
  switch (action.type) {
      case FETCH_BYSTATUS:
        return {
          ...state,
          bystatus: action.payload
        };
      case FETCH_EPICLINK:
        let data = {epicLink:"All Epic's",epicName: "All Epic's" };
        return {
          ...state,
          epicLink: [data, ...action.payload]
        };  
        case FETCH_ALLDEFECTS:
        return {
          ...state,
          allDefects: action.payload
        };
      case FETCH_BYCOMPONENT:
        return {
          ...state,
          bycomponent: action.payload
        };
        case FETCH_BYASSINGEE:
          return {
            ...state,
            byassignee: action.payload
          };
        case FETCH_BYDOMAIN:
          return {
            ...state,
            bydomain: action.payload
          };
          case FETCH_BYPROJECT:
            return {
              ...state,
              byproject: action.payload
            };
    default:
        return state;
  }
}
