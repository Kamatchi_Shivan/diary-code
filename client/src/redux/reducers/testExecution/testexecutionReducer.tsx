import { FETCH_TESTEXECUTION,FETCH_TEST_EXECUTION_BY_RELEASE,FETCH_REQUIREMENT,FETCH_USERSTORIES,PDF_DATA} from "../../actions/types";

const initialState = {
  testResults: [],
  testExecution: [],
  testRequirement:[],
  userStories:[],
  pdfData: {}
};

export default function (state = initialState, action: any) {
  switch (action.type) {
    case FETCH_TESTEXECUTION:
      return {
        ...state,
        testResults: action.payload,
      };
      case FETCH_TEST_EXECUTION_BY_RELEASE:
      return {
        ...state,
        testExecution: action.payload,
      };
      case FETCH_REQUIREMENT:
      return {
        ...state,
        testRequirement: action.payload,
      };
      case FETCH_USERSTORIES:
        return {
          ...state,
          userStories: action.payload,
        };
        case PDF_DATA:
          return {
            ...state,
            pdfData: action.payload,
          };
    default:
      return state;
  }
}
