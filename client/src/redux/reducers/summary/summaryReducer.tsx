import {
  FETCH_ALLPROJECTS,
  FETCH_DOMAIN,
  FETCH_RELESE,
  FETCH_MILESTONE,
  FETCH_PROJECT,
  FETCH_SUMMARY,
  DEFECTS_TOGGLE_SUMMARY,
  FETCH_SUMMARY_PROJECT,
  FETCH_FILTERED_SUMMARY,
  SELECTED_DOMAIN,
  ALL_DOMAIN,
  FILTER_FETCH_RELESE,
  FETCH_PROJECT_RELEASEFILTER,
  DEFECTS_CATEGORY_SUMMARY,
  FETCH_CURRENT_RELESE
} from "../../actions/types";

const initialState = {
  allProjectsInitialRelease: [],
  domainDatas: [],
  filteredsummarydatas: [],
  milestoneDatas: {},
  releaseDatas: [],
  projectDatas: [],
  summaryDatas: [],
  summaryProjectDatas: [],
  selecteddomain: {},
  currentRelease: {},
  releaseDatasfilter: [],
  projectDatasfilter: [],
  summaryToggle: "",
  defectToggle: "",
};

export default function (state = initialState, action: any) {
  //console.log("in reducer");
  switch (action.type) {
    case FETCH_DOMAIN:
      let data = { Id: 0, DomainName: "All Domains" };
      action.payload = [data, ...action.payload];
      return {
        ...state,
        domainDatas: action.payload,
      };
    case FETCH_ALLPROJECTS:
      return {
        ...state,
        allProjectsInitialRelease: action.payload,
      };
    case FETCH_RELESE:
      return {
        ...state,
        releaseDatas: action.payload,
      };
      case FETCH_CURRENT_RELESE:
        return {
          ...state,
          currentRelease: action.payload,
        };
    case FILTER_FETCH_RELESE:
      return {
        ...state,
        releaseDatasfilter: action.payload,
      };
    case FETCH_MILESTONE:
      return {
        ...state,
        milestoneDatas: action.payload,
      };
    case FETCH_PROJECT:
      if (action.payload.length === 0) {
        return {
          ...state,
          projectDatas: action.payload,
        };
      } else {
        let data2 = { Id: 0, ProjectReference: 0, ProjectName: "All Projects" };
        action.payload = [data2, ...action.payload];
        return {
          ...state,
          projectDatas: action.payload,
        };
      }
    case FETCH_PROJECT_RELEASEFILTER:
      if (action.payload.length === 0) {
        return {
          ...state,
          projectDatasfilter: action.payload,
        };
      } else {
        let data2 = { Id: 0, ProjectReference: 0, ProjectName: "All Projects" };
        action.payload = [data2, ...action.payload];
        return {
          ...state,
          projectDatasfilter: action.payload,
        };
      }

    case FETCH_SUMMARY:
      return {
        ...state,
        summaryDatas: action.payload,
      };
    case FETCH_SUMMARY_PROJECT:
      return {
        ...state,
        summaryProjectDatas: action.payload,
      };
    case FETCH_FILTERED_SUMMARY:
      return {
        ...state,
        filteredsummarydatas: action.payload,
      };
    case SELECTED_DOMAIN:
      return {
        ...state,
        selecteddomain: action.payload,
      };
    case ALL_DOMAIN:
      return {
        ...state,
        selecteddomain: action.payload,
      };
    case DEFECTS_TOGGLE_SUMMARY:
      return {
        ...state,
        summaryToggle: action.payload,
      };
    case DEFECTS_CATEGORY_SUMMARY:
      return {
        ...state,
        defectToggle: action.payload,
      };
    default:
      return state;
  }
}
