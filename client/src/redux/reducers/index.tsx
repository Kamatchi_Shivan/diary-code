import {combineReducers} from 'redux';
import summaryReducer from "../reducers/summary/summaryReducer";
import defectReducer from "../reducers/defects/defectReducer";
import testexecutionReducer from "../reducers/testExecution/testexecutionReducer";
import projectViewReducer from "../reducers/projectview/projectViewReducer";
import detailviewReducer from "../reducers/detailview/detailviewReducer";
import headerReducer from "../reducers/header/headerReducer";
import mailer from "../reducers/pdf/pdfReducer"

export default combineReducers({
    summaryData: summaryReducer,
    defectData: defectReducer,
    projectView: projectViewReducer,
    testExecution: testexecutionReducer,
    detailview: detailviewReducer,
    headerView:headerReducer,
    mailer:mailer
});
