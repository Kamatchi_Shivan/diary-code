module.exports = {
    ReleaseName: 'Release Name',
    ReleaseDate: 'Date Release',
    ReleaseTestStatus: 'Release Test Status',
    ReleaseTestKeymessage: 'Release Test Key message',
    ReleasetestrisksIssues: 'Release test risks & Issues',
    LastRegistration: 'Last Registration',
    CCBReady4SPRINT: 'CCB Ready4SPRINT',
    CCBReady4UAT: 'CCB Ready4UAT',
    UATStartDate: 'UAT Start Date',
    EndUAT: 'End UAT',
    Freeze: 'Freeze',
    CCBReady4PROD: 'CCB Ready4PROD',
    CCBClosure: 'CCB Closure',
    ItemType: 'Item Type',
    Path: 'Path',


}