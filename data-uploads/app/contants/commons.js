module.exports = {
   testDeleteLog:'Test-Delete.log',
   requirementsDeleteLog:'Requirements-Delete.log',
   testResults:'TEST RESULTS ',
   requirements: 'REQUIREMENTS ',
   defects : 'DEFECTS ',
   userStory: 'USER STORIES ',
   testReports: 'TEST REPORTS ',
   compononets: 'COMPONENTS ',
   release : 'RELEASE ',
   projects : 'PROJECTS ',
   zephyr : 'ZEPHYR ',
   jira : 'JIRA ',
}