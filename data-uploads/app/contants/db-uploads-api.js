module.exports = {
    project: 'Sharepoint - project and test status information',
    release: 'Sharepoint - Release moments',
    requirements: 'Zephyr - Test Requirements mapping',
    testResults: 'Zephyr - Test Execution results',
    testReports: 'Sharepoint - External Reports',
    defects: 'Jira - Defects',
    comarchDefects:"Jira - Comarch",
    userStory: 'Jira - User Stories',
    components: 'Components',
    testResults: 'Zephyr - Test Execution results',
    manualRun:'manualRun',
    automatedRun:'automatedRun'
}
