module.exports = {
    /* Defult Fields */
    firstResult: "firstResult",
    resultSize: "resultSize",
    results: "results",
    id: "id",
    assignmentDate: "assignmentDate",
    actualTime: "actualTime",
    versionId: "versionId",
    status: "status",
    comment: "comment",
    testerId: "testerId",
    executedBy: "executedBy",
    tcrTreeTestcase: "tcrTreeTestcase",
    tcrCatalogTreeId: "tcrCatalogTreeId",
    revision: "revision",
    stateFlag: "stateFlag",
    lastModifiedOn: "lastModifiedOn",
    versionNumber: "versionNumber",
    customProperties: "customProperties",
    custom_environmentName: "zcf_1002",
    creatorName: "creatorName",

    automated: "automated",
    cyclePhases: "cyclePhases",
    cycleId: "cycleId",

    hierarchy: "hierarchy",
    treeId: "treeId",
    treeName: "treeName",
    lastModifierName: "lastModifierName",
    automatedDefault: "automatedDefault",



    createDatetime: "createDatetime",
    createdById: "createdById",
    modifiedById: "modifiedById",
    testcase: "testcase",
    name: "name",
    fullName: "fullName",
    categories: "categories",

    lastModifiedOn: "lastModifiedOn",
    creationDate: "creationDate",

    tcCreationDate: "tcCreationDate",
    environment: "environment",
    fieldId: "fieldId",
    // customer file d- fieldId  - Environment  1002
    displayName: "displayName",
    seqNumber: "seqNumber",
    testcaseSequence: "testcaseSequence",
    testcaseId: "testcaseId",
    projectId: "projectId",
    projectName: "projectName",
    requirementIds: "requirementIds",

    // Requirement ID
    releaseId: "releaseId",
    requirementIdsNew: "requirementIdsNew",
    requirementId: "requirementId",
    releaseId: "releaseId",
    projectIdParam: "projectIdParam",


    cyclePhaseId: "cyclePhaseId",
    lastTestResult: "lastTestResult",
    execDate: "execDate",
    executionDate: "executionDate",
    executionStatus: "executionStatus",

    // Defects 
    defects: "defects",
    bugId: "bugId",
    externalId: "externalId",
    description: "description",

    priority: "priority",
    state: "state",
    parentKey: "parentKey",
    createdOn: "createdOn",

    customFieldValues: "customFieldValues",
     displayName: "displayName",
     textValue:"textValue",
     preconditions:"Preconditions", 
     component:"Component",
     usergroup:"usergroup",
     businessDomain:"Business Domain",
     pickListValue:"pickListValue",


    // zcf_1009

    /*"testcase" : {
    "customProperties" : {
      "zcf_1009" : "hello"
    },
    */




    // * End of Test Cas 

    // Req Starting


    requirementType: "requirementType",
    testcaseIds: "testcaseIds",
    testcaseVersionIds: "testcaseVersionIds",
    requirementTreeIds: "requirementTreeIds",
    requirementReleaseTestcaseCountMapping: "requirementReleaseTestcaseCountMapping",
    testcaseCount: "testcaseCount",
    requirementTreePath: "requirementTreePath",
    actualTestcaseIds: "actualTestcaseIds",
    type: "type",
    reqCreationDate: "reqCreationDate",
    createdBy: "createdBy",
    attachmentCount: "attachmentCount",
    releaseIds: "releaseIds",
    releaseNames: "releaseNames",
    lastModifiedBy: "lastModifiedBy"

};
