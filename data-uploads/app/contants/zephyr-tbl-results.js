module.exports = {

    releaseName: "releaseName",
    projectName: "projectName",
    hierarchy: "hierarchy",
    folder: "folder",
    subFolder: "subFolder",
    subFolder1: "subFolder1",
    subFolder2: "subFolder2",
    createdOn: "",
    requirementId: "requirementId",
    reqAltid: "reqAltid",
    requirementName: "requirementName",
    environment: "environment",
    testCaseAltId: "testCaseAltId",
    testCaseName: "testCaseName",
    testCaseId: "testCaseId",
    automation: "automation",
    cyclePhaseName: "cyclePhaseName",
    testStatus: "testStatus",
    executedOn: "executedOn",
    tags: "tags",
    defects: "defects",
    executedBy: "executedBy",
    createdBy: "createdBy",
    subFolderLevel: "subFolderLevel",
    updatedOn: "updatedOn"

};
