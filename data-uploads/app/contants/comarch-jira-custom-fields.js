module.exports = {
    /* Defult Fields */
    id: "id",
    name: "name",
    value: "value",
    key: "key",
    issues: "issues",
    issuetype: "issuetype",
    fields: "fields",
    issuekey: "issuekey",
    project: "project",
    parent: "parent",
    resolution: "resolution",
     status: "status",
    resolutionDate: "resolutiondate",
    displayName: "displayName",
    priority: "priority",
    labels: "labels",
    assignee: "assignee",
    components: "components",
    fixVersions: "fixVersions",
    versions: "versions",
    aggregatteOriginalEstimate: "aggregatetimeoriginalestimate",
    remainingEstimate: "timeestimate",
    createdBy: "creator",
    created: "created",
    updated: "updated",
    projectCategory: "projectCategory",
    environment: "environment",
    reporter: "reporter",
    progress: "progress",
    duedate: "duedate",
    // scrumTeam: "scrumTeam",
    // state: "state",
    // startDate: "startDate",
    // endDate: "endDate",
    /* Custom Fields */
    // releaseVersionHistory: "customfield_10101",
    // bzModule: "customfield_11013",
    // stream: "customfield_11159",
    // defectSevirity: "customfield_10402",
    // scrumTeamNode: "customfield_10600",
    // product: "customfield_10700",
    // detectedEnvironment: "customfield_10415",
    // testTypes: "customfield_10800",
    // testLevelDetected: "customfield_10407",
    summary: "summary",
    // reference: "customfield_10500",
    // eaPhase: "customfield_10304",
    severity: "customfield_12500",
    epicLink: "customfield_10500",
    epicName: "customfield_10501",
    epicStatus: "customfield_10502",
    // account: "customfield_10200",
    linkedIssues: "issuelinks",
    // userStoryId: "customfield_11021",
    // bzUserStoryId: "customfield_10945",
    // eaStereoType: "customfield_10308",
    // originalTeam: "customfield_11300",
    // issueFunction: "customfield_10010",
    // iteration: "customfield_10202",
    // origin: "customfield_10113",
    // capability: "customfield_11029",
    // technicalComplexity: "customfield_11026",
    // unitTestingExecution: "customfield_10931",
    // testType: "customfield_11005",
    // logType: "customfield_11160",
    // focus: "customfield_11162",
    sprint: "customfield_10400",
    // testCaseLink: "customfield_10315",
    // summary:"summary"
};














