
function convert(str) {
    var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
}

module.exports.getDateTimeFrmNumber = function (str) {
    if (str === undefined || str ===null)
        return null;
    else {
        var date = new Date(str),
            mnth = ("0" + (date.getMonth() + 1)).slice(-2),
            day = ("0" + date.getDate()).slice(-2);
        var hr = date.getUTCHours(),
            min = date.getUTCMinutes(),
            sec = date.getUTCSeconds();
        var time = [hr, min, sec].join(":")
        var str1 = ([date.getFullYear(), mnth, day].join("-")) + " " + time;
        return str1;
    }
}

module.exports.getDateTime = function (data) {
    var convertedData = new Boolean(false);
    if (data !== null && data !== undefined) {
        if (typeof data === "object") {
            data = convert(data);
            convertedData = true;
        }
        if (typeof data === "string") {
            if (!data.includes("STR")) {
                if (data.includes("T")) {
                    str = data.substring(0, 19);
                }
                else if (data.length > 12) {
                    str = data.substring(0, 19);
                } else if (data.length = 10 && data.includes('/')) {
                    data = convert(data);
                    str = data;
                }
                else if (convertedData) {
                    str = data;
                }
                return str;
            }
        }
    } else
        return data = null

}

module.exports.getDate = function (data) {
    if (data !== null && data !== undefined) {
        console.log(typeof data)
        if (typeof data === "string") {
            str = { data }
            return str;
        }
    } else
        return data = null

}



module.exports.currentTime = function () {
    let ts = Date.now();

    let date_ob = new Date(ts),
        date = date_ob.getDate(),
        month = date_ob.getMonth() + 1,
        year = date_ob.getFullYear(),
        hr = date_ob.getHours(),
        min = date_ob.getMinutes(),
        sec = date_ob.getSeconds(),
        ms = date_ob.getMilliseconds();
    let dateTime = year + "-" + month + "-" + date + " " + hr + ":" + min + ":" + sec + ":" + ms;

    return dateTime
}


module.exports.convertUTCDateToLocalDate =  function convertUTCDateToLocalDate(date) 
{
     var newDate = new Date(date);
     newDate.setMinutes(date.getMinutes() - date.getTimezoneOffset());
     return newDate;
}


 //Format Date
 module.exports.formatDate =function formatDate(d)
{
     date = new Date(d)
     var dd = date.getDate(); 
     var mm = date.getMonth()+1;
     var yyyy = date.getFullYear(); 
     if(dd<10){dd='0'+dd} 
     if(mm<10){mm='0'+mm};
     return d =yyyy+'-'+ mm+'-'+dd
}
