var moment = require('moment');

function convert(str) {
    var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
}

module.exports.getDateTimeFrmNumber = function (str) {
    if (str === undefined)
        return null;
    else {
        var date = new Date(str),
            mnth = ("0" + (date.getMonth() + 1)).slice(-2),
            day = ("0" + date.getDate()).slice(-2);
        var hr = date.getUTCHours(),
            min = date.getUTCMinutes(),
            sec = date.getUTCSeconds();
        var time = [hr, min, sec].join(":")
        var str1 = ([date.getFullYear(), mnth, day].join("-")) + " " + time;
        return `STR_TO_DATE('${str1}', '%Y-%m-%d %H:%i:%s')`;
    }
}

module.exports.getDateTime = function (data) {
    var convertedData = new Boolean(false);
    if (data !== null && data !== undefined) {
        if (typeof data === "object") {
            data = convert(data);
            convertedData = true;
        }
        if (typeof data === "string") {
            if (!data.includes("STR")) {
                if (data.includes("T")) {
                    str = `STR_TO_DATE('${data.substring(0, 19)}', '%Y-%m-%dT%H:%i:%s')`;
                }
                else if (data.length > 12) {
                    str = `STR_TO_DATE('${data.substring(0, 19)}', '%Y-%m-%d %H:%i:%s')`;
                } else if (data.length = 10 && data.includes('/')) {
                    data = convert(data);
                    str = `STR_TO_DATE('${data}', '%Y-%m-%d')`;
                }
                else if (convertedData) {
                    str = `STR_TO_DATE('${data}', '%Y-%m-%d')`;
                }
                return str;
            }
        }
    } else
        return data = null

}

module.exports.getDate = function (data) {
    if (data !== null && data !== undefined) {
        console.log(data)
        if (typeof data === "string") {
            if (!data.includes("STR")) {
                str = `STR_TO_DATE('${data}', '%Y-%m-%d')`;
                return str;
            }
        }
    } else
        return data = null

}


module.exports.processQuery = function (query) {
    let res = query.replace(/'null'/g, "null").replace(/undefined/g, null).replace(/'undefined'/g, null);

    return res;
}


module.exports.process = function (data) {
    if (data === undefined) {
        return data = null
    }
    else {
        if (typeof data === "string") {
            data = data.replace(/'/g, "''")
            if (data.endsWith('\\')) {
                data = data + "\\";
            }
        }
        return data;
    }
}


module.exports.getColumnValues = function (value, index, row) {
    if (row[index] !== null) {
        value = row[index]
        value = this.process(value)
    }
    return value;
}



module.exports.totalTimeTaken = function totalTimeTaken(startedTime) {
    return new Promise((resolve, reject) => {
        var endDate = moment(new Date(), 'YYYY-M-DD HH:mm:ss')
        var msDiff = endDate.diff(startedTime, 'milliseconds')
        var secondsDiff = endDate.diff(startedTime, 'seconds')
        var minutesDiff = endDate.diff(startedTime, 'minutes')

        var tat = null;

        if (msDiff < 60) {
            tat = msDiff + '(ms)';
            return resolve(tat);
        } else if (secondsDiff > 120) {
            tat = minutesDiff + '(minutes)';
            return resolve(tat);
        } else {
            tat = secondsDiff + '(seconds)';
            return resolve(tat);

        }

    }).catch(error => {
        reject(totalTimeTaken, error)
    });
}


module.exports.formatString = function formatString(str) {
    let string = str;
    if (typeof string === "string") {
        i = 0;
        string = string.replace(/&amp;/g, "&")
        // string = string.replace(/<p>/g, "\n");
        string = string.replace(/<div>/g, "");
        string = string.replace(/<br>/g, "");
        // string = string.replace(/(<p([^>]+)>)/gi, "");
        string = string.replace(/(<div([^>]+)>)/gi, "");
        string = string.replace(/(<([^>]+)div>)/gi, "");
        // string = string.replace(/(<([^>]+)p>)/gi, "");     
        string = string.replace(/&amp;/g, "&")
        string = string.replace(/&amp;/g, "&")
        string = string.replace(/&#58;/g, ": ")
        string = string.replace(/&#160;/g, " ").replace(/&gt;/g, ">").replace(/&lt;/g, "<").replace(/&quot;/g, '"')
    }

    return this.process(string);
}

module.exports.projectformatString = function projectformatString(str) {
    let string = str;
    if (typeof string === "string") {
        i = 0;
        string = string.replace(/&amp;/g, "&")
        string = string.replace(/<p>/g, "\n");
        string = string.replace(/<div>/g, "");
        string = string.replace(/<br>/g, "");
        string = string.replace(/(<p([^>]+)>)/gi, "");
        string = string.replace(/(<div([^>]+)>)/gi, "");
        string = string.replace(/(<([^>]+)div>)/gi, "");
        string = string.replace(/(<([^>]+)p>)/gi, "");
        string = string.replace(/&amp;/g, "&")
        string = string.replace(/&amp;/g, "&")
        string = string.replace(/&#58;/g, ": ")
        string = string.replace(/&#160;/g, " ").replace(/&gt;/g, ">").replace(/&lt;/g, "<").replace(/&quot;/g, '"')
    }

    return this.process(string);
}

module.exports.replaceQuotes = function replaceQuotes(str) {
    let string = str;
    if(str!==null){
        if (typeof string === "string") {
            i = 0;
            string = string.replace(/&quot;/g, '"')
        }
    }
   

    return this.process(string);
}