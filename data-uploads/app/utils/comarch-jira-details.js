const { rejects } = require("assert");
const { promises } = require("fs");
const JIRA = require("../contants/comarch-jira-custom-fields");

const { getDateTime, getDateTimeFrmNumber } = require("./date-utils")

module.exports.getDefectDetailsByRow = function (list, key) {


        let component = null, scrumTeamId = null, scrumTeamName = null, assigneeName = null, assigneeId = null,
            defectSevirity = null, severity = null, priority = null, resolution = null, fixVersions = null,
            sprintId = null, eaStereoType = null, sprintEndDate = null, sprintStartDate = null, sprintName = null,
            sprintState = null, product = null, testLevelDetected = null, epicLink = null,
            parentKey = null, projectCategory = null, projectKey = null, projectName = null, origin = null, sprint = null;

        let defectId = processData(list[key][JIRA.id]);
        let defectKey = processData(list[key][JIRA.key]);
        let fields = processData(list[key][JIRA.fields]);
        let project = processData(fields[JIRA.project]);
        let issueType = processData(fields[JIRA.issuetype][JIRA.name]);
        let linkedIssues = processData(fields[JIRA.linkedIssues]);
        parentKey = processData(getParentKeyInfo(parentKey, fields));
        linkedIssues = processLinkedIssue(linkedIssues)
        component = processData(getComponentInfo(component, fields));
        projectKey = processData(project[JIRA.key]);
        projectName = processData(project[JIRA.name]);
        projectCategory = processData(getProjectCategoryInfo(projectCategory, project));
        fixVersions = processData(getFixVersionsInfo(fixVersions, fields));
        resolution = processData(getResolution(resolution, fields));
        // origin = processData(fields[JIRA.origin]);
        sprint = processData(fields[JIRA.sprint]);

        // product = processData(getProductInfo(product, fields));
        // let testTypes = processData(fields[JIRA.testTypes]);
        let summary = processData(fields[JIRA.summary]);
        // testLevelDetected = processData(getTestLevelDetectedInfo(testLevelDetected, fields));
        // let resolutiondate = getDateTimeValue(processData(fields[JIRA.resolutiondate]));
        let createdUserName = processData(fields[JIRA.createdBy][JIRA.displayName]);
        let createdUserID = processData(fields[JIRA.createdBy][JIRA.name]);
        let createdOn = getDateTimeValue(processData(fields[JIRA.created]));
        priority = processData(getPriority(priority, fields));
        let labels = processData(fields[JIRA.labels]);
        // let stream = processData(fields[JIRA.stream]);
        // let testCaseLink = processData(fields[JIRA.testCaseLink]);
        // let detectedEnvironment = processData(fields[JIRA.detectedEnvironment]);

        let updated = getDateTimeValue(processData(fields[JIRA.updated]));
        let status = processData(fields[JIRA.status][JIRA.name]);
        let environment = processData(fields[JIRA.environment]);
        severity = processData(fields[JIRA.severity]);

        eaStereoType = geteaStereoTypeInfo(eaStereoType, fields);
        epicLink = processData(fields[JIRA.epicLink])

        let reporter = null;
        reporter = processData(getReporterInfo(reporter, fields));

        let progress = null;
        progress = processData(getProgressInfo(progress, fields));

        ({ scrumTeamId, scrumTeamName } = getScrumInfo(fields, scrumTeamId, scrumTeamName, fields));

        ({ assigneeName, assigneeId } = getAssigneeInfo(assigneeName, assigneeId, fields));
        assigneeName = processData(assigneeName);

        ({ sprintName, sprintState, sprintId, sprintStartDate, sprintEndDate } = getSprintInfo(sprintName, sprintState, sprintId, sprintStartDate, sprintEndDate, fields))

        sprintStartDate = getDateTimeValue(processData(sprintStartDate));
        sprintEndDate = getDateTimeValue(processData(sprintEndDate));
        sprintName = processData(sprintName);
        sprintState = processData(sprintState);
        sprintId = processData(sprintId);

        // defectSevirity = processData(getDefectSevirity(defectSevirity, fields));

        let customProjKey = defectKey.split("-")[0];

        let labelsList = `"${labels}"`;
        let dataList = {
            component: component,
            projectKey: customProjKey,
            projectName: projectName,
            projectCategory, projectCategory,
            fixVersions: fixVersions,
            resolution: resolution,
            // origin: origin,
            summary: summary,
            // product: product,
            scrumTeamID: scrumTeamId,
            scrumTeamName: scrumTeamName,
            // testTypes: testTypes,
            // testLevelDetected: testLevelDetected,
            // resolutiondate: resolutiondate,
            createdUserName: createdUserName,
            createdUserID: createdUserID,
            createdOn: createdOn,
            priority: priority,
            labels: labelsList.toString,
            // stream: stream,
            // detectedEnvironment: detectedEnvironment,
            assigneeID: assigneeId,
            assigneeName: assigneeName,
            updated: updated,
            status: status,
            severity: severity,
            sprintName: sprintName,
            sprintState: sprintState,
            // sprintId: sprintId,
            sprintStartDate: sprintStartDate,
            sprintEndDate: sprintEndDate,
            reporter: reporter,
            progress: progress,
            stereoType: eaStereoType,
            epicLink: epicLink,
            parentKey: parentKey,
            issueType: issueType,
            linkedIssues:linkedIssues
        }

        let keyList = {
            defectId: defectId,
            defectKey: defectKey,

        }

        return ([dataList, keyList, defectKey]);
   
}
module.exports.getDefectDataById = function (list, key) {

   // return new Promise((resolve, reject) => {
        let defectId = processData(list[key][JIRA.id]);
        let defectKey = processData(list[key][JIRA.key]);
        let fields = processData(list[key][JIRA.fields]);
        let issueType = processData(fields[JIRA.issuetype][JIRA.name]);
        let toBeRemoved = false;
        if (issueType !== "Defect" && issueType !== "Story Defect") {
            toBeRemoved = true;
        }
        return ([defectKey, toBeRemoved]);
   // });
}

module.exports.getUserStoryDataById = function (list, key, userMap, storyKeyMap) {

    return new Promise((resolve, reject) => {
        let userStoryKey = processData(list[key][JIRA.key]);
        let userStoryId = processData(list[key][JIRA.id]);
        let fields = processData(list[key][JIRA.fields]);
        let issueType = processData(fields[JIRA.issuetype][JIRA.name]);
        let project = processData(fields[JIRA.project]);
        let projectKey = processData(project[JIRA.key]);
        let toBeRemoved = false;
        let projKey = userMap.get(userStoryId);
        let storyKey = storyKeyMap.get(userStoryId);
        /**  Validating if the User story is mapped to different project  */
        if (projectKey !== projKey) {
            toBeRemoved = true;
        }
        /** Validating if the issues type is changes form userstory */
        if (issueType !== "Story") {
            toBeRemoved = true;
        }
        return resolve([storyKey, toBeRemoved]);
    });
}



module.exports.getStoryDefectDetailsByRow = function (list, key) {
    let component = null, scrumTeamId = null, scrumTeamName = null, assigneeName = null, assigneeId = null,
        severity = null, priority = null, resolution = null, fixVersions = null, issueType = null, sprintId = null, eaStereoType = null, sprintEndDate = null, sprintStartDate = null, sprintName = null,
        sprintState = null, product = null, epicLink = null;

    let projectCategory = null;
    let storyId = processData(list[key][JIRA.id]);
    let storyKey = processData(list[key][JIRA.key]);
    let fields = processData(list[key][JIRA.fields]);
    let project = processData(fields[JIRA.project]);

    issueType = processData(fields[JIRA.issuetype][JIRA.name]);
    component = processData(getComponentInfo(component, fields));
    let projectKey = processData(project[JIRA.key]);
    let projectName = processData(project[JIRA.name]);
    fixVersions = processData(getFixVersionsInfo(fixVersions, fields));
    resolution = processData(getResolution(resolution, fields));
    let sprint = processData(fields[JIRA.sprint]);

    product = processData(getProductInfo(product, fields));

    let resolutiondate = getDateTimeValue(processData(fields[JIRA.resolutiondate]));
    let createdUserName = processData(fields[JIRA.createdBy][JIRA.displayName]);
    let createdUserID = processData(fields[JIRA.createdBy][JIRA.name]);
    let createdOn = getDateTimeValue(processData(fields[JIRA.created]));
    priority = processData(getPriority(priority, fields));
    let labels = processData(fields[JIRA.labels]);
    let testCaseLink = processData(fields[JIRA.testCaseLink]);

    let updated = getDateTimeValue(processData(fields[JIRA.updated]));
    let status = processData(fields[JIRA.status][JIRA.name]);
    severity = processData(fields[JIRA.severity]);

    epicLink = processData(fields[JIRA.epicLink])

    let reporter = null;
    reporter = processData(getReporterInfo(reporter, fields));

    ({ scrumTeamId, scrumTeamName } = getScrumInfo(fields, scrumTeamId, scrumTeamName, fields));

    ({ assigneeName, assigneeId } = getAssigneeInfo(assigneeName, assigneeId, fields));
    assigneeName = processData(assigneeName);

    ({ sprintName, sprintState, sprintId, sprintStartDate, sprintEndDate } = getSprintInfo(sprintName, sprintState, sprintId, sprintStartDate, sprintEndDate, fields))

    sprintStartDate = getDateTimeValue(processData(sprintStartDate));
    sprintEndDate = getDateTimeValue(processData(sprintEndDate));
    sprintName = processData(sprintName);
    sprintState = processData(sprintState);
    sprintId = processData(sprintId);

    module = processData(fields[JIRA.bzModule]);


    let customProjKey = storyKey.split("-")[0];

    let labelsList = `"${labels}"`;

    let dataList = {
        component: component,
        projectKey: customProjKey,
        projectName: projectName,
        fixVersions: fixVersions,
        resolution: resolution,
        product: product,
        scrumTeamID: scrumTeamId,
        scrumTeamName: scrumTeamName,
        resolutiondate: resolutiondate,
        createdUserName: createdUserName,
        createdUserID: createdUserID,
        createdOn: createdOn,
        priority: priority,
        labels: labelsList.toString,
        assigneeID: assigneeId,
        assigneeName: assigneeName,
        updated: updated,
        status: status,
        severity: severity,
        sprintId: sprintId,
        sprintState: sprintState,
        sprintName: sprintName,
        sprintStartDate: sprintStartDate,
        sprintEndDate: sprintEndDate,
        reporter: reporter,
        epicLink: epicLink,
        module: module,
        issueType: issueType

    }
    let keyList = {
        storyID: storyId,
        storyKey: storyKey,

    }

    return [dataList, keyList, storyKey];
}


module.exports.getUserStoryDetailsByRow = function (list, key) {
    let component = null, scrumTeamId = null, scrumTeamName = null, assigneeName = null, assigneeId = null,
        severity = null, priority = null, resolution = null, fixVersions = null, issueType = null, sprintId = null, eaStereoType = null, sprintEndDate = null, sprintStartDate = null, sprintName = null,
        sprintState = null, product = null, epicLink = null, summary = null;

    let projectCategory = null;

    let storyId = processData(list[key][JIRA.id]);
    let storyKey = processData(list[key][JIRA.key]);
    let fields = processData(list[key][JIRA.fields]);
    let project = processData(fields[JIRA.project]);
    summary = processData(fields[JIRA.summary]);
    //console.log('summary ' , summary)

    issueType = processData(fields[JIRA.issuetype][JIRA.name]);
    component = processData(getComponentInfo(component, fields));
    let projectKey = processData(project[JIRA.key]);
    let projectName = processData(project[JIRA.name]);
    fixVersions = processData(getFixVersionsInfo(fixVersions, fields));
    resolution = processData(getResolution(resolution, fields));
    let sprint = processData(fields[JIRA.sprint]);

    product = processData(getProductInfo(product, fields));

    let resolutiondate = getDateTime(processData(fields[JIRA.resolutiondate]));
    let createdUserName = processData(fields[JIRA.createdBy][JIRA.displayName]);
    let createdUserID = processData(fields[JIRA.createdBy][JIRA.name]);
    let createdOn = getDateTime(processData(fields[JIRA.created]));
    priority = processData(getPriority(priority, fields));
    let labels = processData(fields[JIRA.labels]);
    if (labels.length === 0) {
        labels = null;
    } else {
        labels = labels.toString();
    }


    let updated = (processData(fields[JIRA.updated]));
    let status = processData(fields[JIRA.status][JIRA.name]);
    severity = processData(fields[JIRA.severity]);

    epicLink = processData(fields[JIRA.epicLink])

    let reporter = null;
    reporter = processData(getReporterInfo(reporter, fields));

    ({ scrumTeamId, scrumTeamName } = getScrumInfo(fields, scrumTeamId, scrumTeamName, fields));

    ({ assigneeName, assigneeId } = getAssigneeInfo(assigneeName, assigneeId, fields));
    assigneeName = processData(assigneeName);

    ({ sprintName, sprintState, sprintId, sprintStartDate, sprintEndDate } = getSprintInfo(sprintName, sprintState, sprintId, sprintStartDate, sprintEndDate, fields))

    sprintStartDate = getDateTime(processData(sprintStartDate));
    sprintEndDate = getDateTime(processData(sprintEndDate));
    sprintName = processData(sprintName);
    sprintState = processData(sprintState);
    sprintId = processData(sprintId);

    module = processData(fields[JIRA.bzModule]);


    let customProjKey = storyKey.split("-")[0];


    var dataList = {
        component: component,
        projectKey: customProjKey,
        component: component,
        projectName: projectName,
        fixVersions: fixVersions,
        resolution: resolution,
        product: product,
        scrumTeamID: scrumTeamId,
        scrumTeamName: scrumTeamName,
        resolutiondate: resolutiondate,
        createdUserName: createdUserName,
        createdUserID: createdUserID,
        createdOn: createdOn,
        priority: priority,
        labels: labels,
        assigneeID: assigneeId,
        assigneeName: assigneeName,
        updated: updated,
        status: status,
        severity: severity,
        epicLink: epicLink,
        module: module,
        issueType: issueType,
        summary: summary
    }

    var keyList = {
        userStoryID: storyId,
        storyKey: storyKey,

    }
    return [dataList, keyList];
}



function getParentKeyInfo(parentKey, fields) {
    if (fields[JIRA.parent] !== null && fields[JIRA.parent] !== undefined) {
        return parentKey = fields[JIRA.parent][JIRA.key];
    } else {
        parentKey = null;
    }
}

function geteaStereoTypeInfo(eaStereoType, fields) {
    if (fields[JIRA.eaStereoType] !== null && fields[JIRA.eaStereoType] !== undefined) {
        return eaStereoType = fields[JIRA.eaStereoType][JIRA.value];
    }
}

function getProgressInfo(progress, fields) {
    if (fields[JIRA.progress] !== null && fields[JIRA.progress] !== undefined) {
        return progress = fields[JIRA.progress]["percent"];
    }
}


function getReporterInfo(reporter, fields) {
    if (fields[JIRA.reporter] !== null && fields[JIRA.reporter] !== undefined) {
        return reporter = fields[JIRA.reporter][JIRA.displayName];
    }
}

function formatSprintData(str) {
    //let str = `[id=209,rapidViewId=34,state=CLOSED,name=Sprint 11 = Portal Front S62,startDate=2015-11-23T13:00:34.057+01:00,endDate=2015-12-11T13:00:00.000+01:00,completeDate=2015-12-14T07:57:54.538+01:00,sequence=209"}`;


    str = str.replace("id=", "\"id\":\"");
    str = str.replace(",rapidViewId=", "\",\"rapidViewId\":\"");
    str = str.replace(",state=", "\",\"state\":\"");
    str = str.replace(",name=", "\",\"name\":\"");
    str = str.replace(",startDate=", "\",\"startDate\":\"");
    str = str.replace(",endDate=", "\",\"endDate\":\"");
    str = str.replace(",completeDate=", "\",\"completeDate\":\"");
    str = str.replace(",sequence=", "\",\"sequence\":\"");
}

//formatSprintData();

function getProjectCategoryInfo(projectCategory, project) {
    if (project[JIRA.projectCategory] !== null && project[JIRA.projectCategory] !== undefined) {
        return projectCategory = project[JIRA.projectCategory][JIRA.name];
    }
}

function getFixVersionsInfo(fixVersions, fields) {
    if (fields[JIRA.fixVersions] !== null && fields[JIRA.fixVersions] !== undefined) {
        if (fields[JIRA.fixVersions].length > 0) {
            fixVersions = fields[JIRA.fixVersions][0][JIRA.name]
        }
        return fixVersions;
    }
}

function getComponentInfo(component, fields) {
    if (fields[JIRA.components] !== null && fields[JIRA.components] !== undefined) {
        if (fields[JIRA.components].length > 0) {
            component = fields[JIRA.components][0][JIRA.name]
        }
    }
    return component;
}
function getResolution(resolution, fields) {
    if (fields[JIRA.resolution] !== null) {
        resolution = fields[JIRA.resolution][JIRA.name]
        resolution = resolution.replace(/'/g, "''")
    }
    return resolution
}

function getTestLevelDetectedInfo(testLevelDetected, fields) {
    if (fields[JIRA.testLevelDetected] !== null) {
        testLevelDetected = fields[JIRA.testLevelDetected][JIRA.value]
    }
    return testLevelDetected
}

function getDefectSevirity(defectSevirity, fields) {
    if (fields[JIRA.defectSevirity] !== null) {
        defectSevirity = fields[JIRA.defectSevirity][JIRA.value];
    }
    return defectSevirity;
}
function getPriority(priority, fields) {
    if (fields[JIRA.priority] !== null) {
        priority = fields[JIRA.priority][JIRA.name];
    }
    return priority;
}
function getAssigneeInfo(assigneeName, assigneeId, fields) {
    if (fields[JIRA.assignee] !== null) {
        assigneeName = fields[JIRA.assignee][JIRA.displayName];
        assigneeId = fields[JIRA.assignee][JIRA.name];
    }
    return { assigneeName, assigneeId };
}

function getProductInfo(product, fields) {
    if (fields[JIRA.product] !== null) {
        product = fields[JIRA.product][JIRA.value];
    }
    return product;
}

function getScrumInfo(fields, scrumTeamId, scrumTeamName) {
    if (fields[JIRA.scrumTeamNode] !== null && fields[JIRA.scrumTeamNode] !== undefined) {
        scrumTeamId = fields[JIRA.scrumTeamNode][JIRA.id];
        scrumTeamName = fields[JIRA.scrumTeamNode][JIRA.value];
    }
    return { scrumTeamId, scrumTeamName };
}

function getSprintInfo(sprintName, sprintState, sprintId, sprintStartDate, sprintEndDate, fields) {
    if (fields[JIRA.sprint] !== null) {
        let sprint = fields[JIRA.sprint];

        let val = sprint[sprint.length - 1]
        let str = val.substring(val.indexOf("["), val.indexOf("goal") - 1) + "\"}";

        let ab = str.replace("[", "{").replace("]", "\"}");
        ab = formatSprintData(ab);
        if (ab !== undefined) {
            let arr = JSON.parse(ab);
            sprintName = (arr[JIRA.name]);
            sprintState = (arr[JIRA.state]);
            sprintId = arr[JIRA.id];
            sprintStartDate = arr[JIRA.startDate];
            sprintEndDate = arr[JIRA.endDate];
            if (sprintName === undefined) {
                //  console.log(">>> ", sprintName, sprintState, sprintId, sprintStartDate, sprintEndDate)
            } else if (sprintName !== null) {
                sprintName = sprintName.replace(/'/g, "''")
            }
        }
    }
    return { sprintName, sprintState, sprintId, sprintStartDate, sprintEndDate };
}


function processData(data) {
    if (data === undefined)
        return null
    else {
        if (typeof data === "string") {
            data = data.replace(/'/g, "''")
        }
        return data;
    }
}

function getDateTimeValue(data) {
    if (data !== null) {
        if (!data.includes(null) && !data.includes("Un-Assigned"))

            return str = getDateTimeFrmNumber(data)
        //          return str = `STR_TO_DATE('${data.substring(0, 19)}', '%Y-%m-%dT%H:%i:%s')`;
    } else
        return data = null

}

function processQuery(query) {
    let res = query.replace(/'null'/g, "null").replace(/undefined/g, null).replace(/'undefined'/g, null);
    return res;
}

function processLinkedIssue(data){
    if(data.length !== 0){
        let linkedissues = [];
        data.forEach(issue => {
           if(issue.inwardIssue){
            linkedissues.push(issue.inwardIssue.key)
           }
        })
        return linkedissues.toString();

    }else{
        return null;
    }
}