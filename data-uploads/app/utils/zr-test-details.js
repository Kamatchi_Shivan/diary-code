const ZR = require("../contants/zephyr-fields");
const { process: processData, processQuery } = require("./data-utils");

const { getDateTime, getDateTimeFrmNumber, getDate } = require("./date-utils");

const DATA_MODEL = require("../models/dairy-data.model");

const fetch = require("node-fetch");

const config = require("../configs/zephyr-config");


const baseUrl = config.baseURL;

const base64 = require("base-64");

const { currentTime } = require("../utils/date-utils");
const { map } = require("async");
const { created } = require("../contants/jira-custom-fields");
async function getRequest(path) {
  return await fetch(path, {
    method: 'GET',
    credentials: 'same-origin',
    redirect: 'follow',
    headers: {
      'Authorization': 'Basic ' + base64.encode(config.username + ":" + config.password),
    },
    //timeout: 5000
  }).then(response => {
    if (response.ok) {
      return response.json();
    } else {
      return "error  " + response.status + response.statusText
    }
  }).catch(error => {
    return error
  })

}

async function getReleaseName(releaseid) {
  let path = baseUrl + `release/${releaseid}`
  let response = await getRequest(path)
  return response[ZR.name];

}

async function getProjectName(projectId) {
  let path = baseUrl + `project/${projectId}`
  let response = await getRequest(path)
  return response[ZR.name];
}

function getUserName(userId, users) {
  return new Promise((resolve, reject) => {
    for (var i in users) {
      let data = users[i]
      let id = data["id"];
      let fullName = data["fullName"]
      if (id === userId) {
        resolve(fullName);
        break;
      }
    }
  });
}

let folderData = [];
let id,phaseName;
function getTCFolderPath(tcrCatalogTreeId, cyclePhaseId, phasesJson, recordID, testCaseId) {
  return new Promise((resolve, reject) => {
    let json = phasesJson;
    let id = null;
    folderData = [];
    outterLoop:
    for (var key in json) {
      if (Number(key) === cyclePhaseId) {
        folderData = []    
        id = json[key]["id"];
        phaseName =json[key]["name"];
        innerLoop:
        for(let i=0; i<20; i++){  
            if(i===0 && folderData.length === 0){
                if(id === tcrCatalogTreeId){
                    let obj = {
                        name: phaseName,
                        parentId: 0
                    };
                    folderData.push(obj);
                    break innerLoop;
                }else{
                  TestretryTreeNode(json[key]["treeNodes"],tcrCatalogTreeId)
                }
            }else{
              if(folderData.length !== 0){
                if(folderData[folderData.length-1].parentId === 0){
                    break innerLoop;
                }
                else if(folderData[folderData.length-1].parentId === id){
                    let obj = {
                        name: phaseName,
                        parentId: 0
                    };
                    folderData.push(obj);
                }else{
                  TestretryTreeNode(json[key]["treeNodes"],folderData[folderData.length-1].parentId)
                }                                  
                }else{
                  break innerLoop;
                }
              }
        }
      }
      if(folderData.length > 0){
        return resolve(DataCostruction(folderData));
      }
    }

  function DataCostruction(folderData){
      let FinalData = []; 
      folderData.forEach(data =>{
          FinalData.push(data.name);
      })
      return FinalData.reverse();
  }


  async function TestretryTreeNode( treeNodesJson,tcrCatalogTreeId) {
    return new Promise((resolve, reject) => {
    let finalobj ={};
      for (var node in treeNodesJson) {
        let id = treeNodesJson[node]["id"];
        if (id !== undefined) {
          if (Number(id) === Number(tcrCatalogTreeId)) {
            let name = treeNodesJson[node]["name"];
            let parentId = treeNodesJson[node]["parentId"];
            finalobj = {
                name:name,
                parentId:parentId
            };
            folderData.push(finalobj);
            break;             
          } else {
            if (treeNodesJson[node]["treeNodes"]) {
              let tree = treeNodesJson[node]["treeNodes"]
              let numOfSubTrees = Object.keys(tree).length;
              for (let i = 0; i < numOfSubTrees; i++) {
                let sub = tree[i]
                let id = sub["id"]
                if (Number(id) === Number(tcrCatalogTreeId)) {
                    let name = sub["name"];
                    let parentId = sub["parentId"];
                    finalobj = {
                        name:name,
                        parentId:parentId
                    };
                    folderData.push(finalobj);
                  break;
                } else {
                  let subTreeNodes = sub["treeNodes"];
                  let numOfSubTreesNode = Object.keys(subTreeNodes).length;
                  if (numOfSubTreesNode > 0) {
                    TestretryTreeNode(subTreeNodes,tcrCatalogTreeId)
                  }
                }
              }
            }
          }
        } else {
          break;
        }
      }      
    });    
  }

  function retryTreeNode(levelCount, treeNodesJson) {
    return new Promise((resolve, reject) => {
      let name = "";
      for (var node in treeNodesJson) {
        if (tcrFound) {
          resolve(true)
          break;
        }
        let id = treeNodesJson[node]["id"];
        if (id !== undefined) {
          let level4 = treeNodesJson[node]["name"]
          path.set(levelCount, level4)
          if (Number(id) === Number(tcrCatalogTreeId)) {
            tcrFound = true
            pathSize = levelCount
            resolve(true)
            break;
          } else {
            if (treeNodesJson[node]["treeNodes"]) {
              let tree = treeNodesJson[node]["treeNodes"]
              let numOfSubTrees = Object.keys(tree).length;
              if (numOfSubTrees > 0)
                levelCount = levelCount + 1;
              for (let i = 0; i < numOfSubTrees; i++) {
                let sub = tree[i]
                name = sub["name"];
                path.set(levelCount, name)
                id = sub["id"]
                if (Number(id) === Number(tcrCatalogTreeId)) {
                  pathSize = levelCount
                  tcrFound = true
                  resolve(true)
                  break;
                } else {
                  let subTreeNodes = sub["treeNodes"];
                  let numOfSubTreesNode = Object.keys(subTreeNodes).length;
                  if (numOfSubTreesNode > 0) {
                    levelCount++
                    retryTreeNode(levelCount, subTreeNodes)
                  }
                }
              }
            }
          }
        } else {
          break;
        }
      }
    });
  }

});
}
    
function getTCCycleName(cyclePhaseId, testCycle) {
  return new Promise((resolve, reject) => {
    let arr = testCycle;
    let cycName = "", id = "", name = "", phases = null;
    for (let i in arr) {
      let json = arr[i];
      for (var key in json) {
        if (key === "id") id = json[key];
        if (key === "name") name = json[key];
        if (key === "cyclePhases") {
          phasesArr = json[key];
          for (let j in phasesArr) {
            phases = phasesArr[j];
            let idValue = phases["id"];
            if (Number(idValue) === Number(cyclePhaseId)) {
              if (Number(id) === Number(phases["cycleId"])) {
                cycName = name;
                resolve(cycName);
                return resolve(cycName);
              }
            } else {
              let treeNodesJson = json[key]["treeNodes"];
              for (var node in treeNodesJson) {
                let id = treeNodesJson[node][ZR.id];
                if (Number(id) === Number(tcrCatalogTreeId)) {
                  name = treeNodesJson[node][ZR.name];
                  pathArr.push(name)
                  resolve(pathArr)
                  return (pathArr);
                }
              }

            }
          }
        }
      }
    }
  });
}


let executedUserName = "";
module.exports.getTestExecutionDetailsByRow = function (data, releaseName, testCycle, phasesJson, recordID) {
  return new Promise((resolve, reject) => {
    // console.log("Printing Data for reference>>>>>",data)
    try {
      let requirementName = null,
        tcrTreeTestcase = processData(data[ZR.tcrTreeTestcase]),
        tcrCatalogTreeId = processData(tcrTreeTestcase[ZR.tcrCatalogTreeId]), //hierarchy
        testCase = processData(tcrTreeTestcase[ZR.testcase]),
        environment = processData(testCase[ZR.customProperties][ZR.custom_environmentName]),
        testCaseName = processData(testCase[ZR.name]),
        testCaseId = processData(testCase[ZR.testcaseId]),
        tcManualOrAutomated = processData(testCase[ZR.automated] ? "Automated" : "Manual"),
        tcCreationDate = getDateTimeFrmNumber(testCase[ZR.createDatetime]),
        projectName = processData(testCase[ZR.projectName]),
        requirementId = processData(testCase[ZR.requirementIds][0]),
        requirementIds = processData(testCase[ZR.requirementIds]),
        testCaseAltId = processData(testCase[ZR.externalId]),
        createdBy = processData(testCase[ZR.creatorName]),
        createdOn = getDateTimeFrmNumber(data[ZR.assignmentDate]),
        releaseId = processData(tcrTreeTestcase[ZR.releaseId]),
        cyclePhaseId = processData(data[ZR.cyclePhaseId]),
        lastTestResult = processData(data[ZR.lastTestResult]),
        defects = processData(data[ZR.defects]),
        projectId = processData(testCase[ZR.projectId]),
        reqAltId = null,
        priority = processData(testCase[ZR.priority]),
        defectIds = [];
      ;
      testCaseCustomFields = processData(testCase[ZR.customFieldValues]);
      let testWiseReqIds = [];
      if (requirementIds.length < 1) {
        requirementIds = null;
      } else {
        requirementIds = requirementIds.toString();
        testWiseReqIds = requirementIds.split(",");
        //  console.log("requirementIDS " , requirementIds.toString());
      }

      let preConditions = null;

      let preCondtionsFound = false;
      for (const key in testCaseCustomFields) {
        const element = testCaseCustomFields[key];
        if (element[ZR.displayName] === "Preconditions") {
          if (element[ZR.textValue] !== undefined) {
            preConditions = element[ZR.textValue];
            preCondtionsFound = true;
            break;
          }
        }
        if (preCondtionsFound === true) {
          break;
        }

      }

      let component = null;
      let componentFound = false;


      for (const key in testCaseCustomFields) {
        const element = testCaseCustomFields[key];
        if (element[ZR.displayName] === ZR.component) {
          if (element[ZR.textValue] !== undefined) {
            component = element[ZR.textValue];
            componentFound = true;
            break;
          }
        }
        if (componentFound === true) {
          break;
        }

      }

      let usergroup = null;
      let usergroupFound = false;

      for (const key in testCaseCustomFields) {
        const element = testCaseCustomFields[key];
        if (element[ZR.displayName] === ZR.usergroup) {
          if (element[ZR.pickListValue] !== undefined) {
            usergroup = element[ZR.pickListValue];
            usergroupFound = true;
            break;
          }
        }
        if (usergroupFound === true) {
          break;
        }

      }

      let businessDomain = null;
      let businessDomainFound = false;

      for (const key in testCaseCustomFields) {
        const element = testCaseCustomFields[key];
        if (element[ZR.displayName] === ZR.businessDomain) {
          if (element[ZR.pickListValue] !== undefined) {
            businessDomain = element[ZR.pickListValue];
            businessDomainFound = true;
            break;
          }
        }
        if (businessDomainFound === true) {
          break;
        }

      }

      let executedOn = null,
        testStatus = null,
        executedBy = processData(data[ZR.executedBy]);

      let folderData = null;
      let mappedCycle = "";


      getTCFolderPath(tcrCatalogTreeId, cyclePhaseId, phasesJson, recordID, testCaseId).then(
        (fpath) => {
          folderData = fpath;
          // if(cyclePhaseId === 5950 ){
            // console.log("Print folder Data>>>>>>",folderData)
          // }
          let hierarchy = [],
            path = null,
            folder = "",
            subfolder = "",
            subfolder1 = "",
            subfolder2 = "",
            subfolder3 = "",
            subfolder4 = "",
            subfolder5 = "",
            subfolder6 = "",
            subfolder7 = "",
            subfolder8 = "",
            subfolder9 = "",
            subfolder10 = "",
            subfolderLevel = 0;

          folder = folderData[0];

          if (folderData.length > 1) {
            subfolder = folderData[1];
          }
          if (folderData.length > 2) {
            subfolder1 = folderData[2]
          }
          if (folderData.length > 3) {
            subfolder2 = folderData[3]
          }
          if (folderData.length > 4) {
            subfolder3 = folderData[4]
          }
          if (folderData.length > 5) {
            subfolder4 = folderData[5]
          }
          if (folderData.length > 6) {
            subfolder5 = folderData[6]
          }
          if (folderData.length > 7) {
            subfolder6 = folderData[7]
          }
          if (folderData.length > 8) {
            subfolder7 = folderData[8]
          }
          if (folderData.length > 9) {
            subfolder8 = folderData[9]
          }
          hierarchy = folderData;

          //console.log(folderData)
          getTCCycleName(cyclePhaseId, testCycle).then((cycResponseName) => {
            mappedCycle = cycResponseName;
            if (lastTestResult !== null) {
              executedOn = getDateTimeFrmNumber(
                lastTestResult[ZR.executionDate]
              );
              testStatus = processData(lastTestResult[ZR.executionStatus]);
              if (testStatus == 2) {
                testStatus = "Fail";
              } else if (testStatus == 1) {
                testStatus = "Pass";
              } else if (testStatus == 10) {
                testStatus = "Not Executed";
              } else if (testStatus == 3) {
                testStatus = "Incomplete";
              } else if (testStatus == 4) {
                testStatus = "Blocked";
              } else if (testStatus == 11) {
                testStatus = "N/A";
              } else if (testStatus == 12) {
                testStatus = "Fail:test data";
              }
            } else {
              testStatus = "Not Executed";
            }

            for (let i in defects) {
              let defect = defects[i];
              reqAltId = processData(defect[ZR.parentKey]);
              let id = null;
              id = processData(defect[ZR.externalId]);
              if (id !== null) defectIds.push(id);
            }
            executedUserName = executedBy;

            path = hierarchy.toString().replace(/,/g, ">");

            let cycleName = mappedCycle;
            subfolderLevel = hierarchy.length;

            if (requirementId === null) requirementId = "";
            if (!environment) environment = "";

            if (defectIds.length < 0) defectIds = null;
            else {
              defectIds = defectIds.toString();
            }

            var keyList = {

              testCaseId: processData(testCaseId),
              projectId: projectId,
              cyclePhaseId: cyclePhaseId,
              tcrCatalogTreeId: tcrCatalogTreeId,
              releaseId: releaseId
            };

            var dataList = {
              projectName: processData(projectName),
              hierarchy: processData(path),
              folder: folder,
              releaseName: processData(releaseName),
              subFolder: subfolder,
              subFolder1: subfolder1,
              subFolder2: subfolder2,
              createdOn: tcCreationDate,
              reqAltId: reqAltId,
              cycle: processData(cycleName),
              requirementName: requirementName,
              testCaseAltId: testCaseAltId,
              testCaseName: testCaseName,
              automation: tcManualOrAutomated,
              testStatus: testStatus,
              executedOn: executedOn,
              executedBy: executedUserName,
              defects: defectIds,
              createdBy: createdBy,
              subFolderLevel: subfolderLevel,
              subfolder3: subfolder3,
              subfolder4: subfolder4,
              subfolder5: subfolder5,
              subfolder6: subfolder6,
              subfolder7: subfolder7,
              subfolder8: subfolder8,
              subfolder9: subfolder9,
              subfolder10: subfolder10,
              requirementId: processData(requirementId),
              requirementIds: requirementIds,
              environment: processData(environment),
              preConditions: processData(preConditions),

              priority: processData(priority),
              component: processData(component),
              usergroup: processData(usergroup),
              businessDomain: processData(businessDomain)

            };
            previousUserId = executedBy;
            /*resolve([dataList, keyList]);
            return [[dataList, keyList]];*/


            let testWiseReqIdList = [];
            for (rq in requirementIds) {
              if (requirementIds[rq].length > 0) {
                let tcRequirementId = requirementIds[rq];
                let reqObj = {
                  tcRequirementId: tcRequirementId,
                  requirementIds: requirementIds,

                };
                testWiseReqIdList.push(reqObj);
              }
            }



            previousUserId = executedBy;
            resolve([dataList, keyList, testWiseReqIdList]);
            return [[dataList, keyList, testWiseReqIdList]];
            /* }).catch((error) => {
               console.error(currentTime(), " >> ", "Err on >> TD.getUserName", error);
             });*/

          }
          );
        });


    } catch (error) {
      console.log(currentTime(), " >> ", error.message);
      return reject(error.message);
    }
  }).catch((error) => {
    console.log(currentTime(), " >> ", error);
    reject(error);
  });
};
let ReqfolderData = [];
function getTree(ids, pathJson) {
  return new Promise((resolve, reject) => {
    let json = pathJson;
    let id = null;
    ReqfolderData = [];
    for(var i in ids){
        let tcrCatalogTreeId = ids[i];
        for (var key in json) {
            ReqfolderData = []    
            id = json[key]["id"];
            phaseName =json[key]["name"];
            innerLoop:
            for(let i=0; i<20; i++){  
                if(i===0 && ReqfolderData.length === 0){
                    if(id === tcrCatalogTreeId){
                        let obj = {
                            name: phaseName,
                            parentId: 0
                        };
                        ReqfolderData.push(obj);
                        break innerLoop;
                    }else{
                      ReqretryTreeNode(json[key]["categories"],tcrCatalogTreeId)
                    }
                }else{
                  if(ReqfolderData.length !== 0){
                    if(ReqfolderData[ReqfolderData.length-1].parentId === 0){
                        break innerLoop;
                    }
                    else if(ReqfolderData[ReqfolderData.length-1].parentId === id){
                        let obj = {
                            name: phaseName,
                            parentId: 0
                        };
                        ReqfolderData.push(obj);
                    }else{
                      ReqretryTreeNode(json[key]["categories"],ReqfolderData[ReqfolderData.length-1].parentId)
                    }                                  
                    }else{
                      break innerLoop;
                    }
                  }
            }
        
          if(ReqfolderData.length > 0){
              // console.log("Reqfolder Data >>>>>",ReqfolderData);
            return resolve(DataCostruction(ReqfolderData));
          }
        }

    }

  function DataCostruction(ReqfolderData){
      let FinalData = []; 
      ReqfolderData.forEach(data =>{
          FinalData.push(data.name);
      })
      return FinalData.reverse();
  }


  async function ReqretryTreeNode( categoriesJson,tcrCatalogTreeId) {
    return new Promise((resolve, reject) => {
    let finalobj ={};
      for (var node in categoriesJson) {
        let id = categoriesJson[node]["id"];
        if (id !== undefined) {
          if (Number(id) === Number(tcrCatalogTreeId)) {
            let name = categoriesJson[node]["name"];
            let parentId = categoriesJson[node]["parentId"];
            finalobj = {
                name:name,
                parentId:parentId
            };
            ReqfolderData.push(finalobj);
            break;             
          } else {
            if (categoriesJson[node]["categories"]) {
              let tree = categoriesJson[node]["categories"]
              let numOfSubTrees = Object.keys(tree).length;
              for (let i = 0; i < numOfSubTrees; i++) {
                let sub = tree[i]
                let id = sub["id"]
                if (Number(id) === Number(tcrCatalogTreeId)) {
                    let name = sub["name"];
                    let parentId = sub["parentId"];
                    finalobj = {
                        name:name,
                        parentId:parentId
                    };
                    ReqfolderData.push(finalobj);
                  break;
                } else {
                  let subcategories = sub["categories"];
                  let numOfSubTreesNode = Object.keys(subcategories).length;
                  if (numOfSubTreesNode > 0) {
                    ReqretryTreeNode(subcategories,tcrCatalogTreeId)
                  }
                }
              }
            }
          }
        } else {
          break;
        }
      }      
    });    
  }

});
}




let previousProjectName = null, previousReleaseName = null;
module.exports.getRequirementsByRow = function (data, releaseName, projectName, requirementTreeList, usersList, releaseId) {
  return new Promise((resolve, reject) => {
    try {
      let requirementId = processData(data[ZR.id]),
        requirementName = processData(data[ZR.name]),
        priority = processData(data[ZR.priority]),
        createdOn = getDateTimeFrmNumber(data[ZR.createdOn]),
        reqCreationDate = getDateTime(data[ZR.reqCreationDate]),
        createdBy = processData(data[ZR.createdBy]),
        lastModifiedBy = processData(data[ZR.lastModifiedBy]),
        lastModifiedOn = getDateTimeFrmNumber(data[ZR.lastModifiedOn]),
        externalId = processData(data[ZR.externalId]),
        requirementType = processData(data[ZR.requirementType]),
        reqStatus = processData(data[ZR.status]),
        attachmentCount = processData(data[ZR.attachmentCount]),
        testcaseIds = processData(data[ZR.testcaseIds]),
        releaseIds = processData(data[ZR.releaseIds]),
        requirementTreeIds = processData(data[ZR.requirementTreeIds]),
        reqReleaseTestCCountMap = processData(
          data[ZR.requirementReleaseTestcaseCountMapping][0]
        );
      
      let reqReleaseTCCountMapId = null,
        reqReleaseTCCountMapReqId = null,
        reqReleaseTCCountMapReleaseId = null,
        reqReleaseTCCountMapTCCount = null;

      createdOn = getDateTimeFrmNumber(data[ZR.createdOn]);

      let customFields = processData(data[ZR.customFieldValues]);
      let component = null;
      let componentFond = false;
      for (const key in customFields) {
        const element = customFields[key];
        if (element[ZR.displayName] === ZR.component) {
          if (element[ZR.textValue] !== undefined) {
            component = element[ZR.textValue];
            componentFond = true;
            break;
          }
        }
        if (componentFond === true) {
          break;
        }

      }

      if (requirementType === 4) {
        requirementType = "Story";
      }

      if (reqReleaseTestCCountMap != null) {
        let map = reqReleaseTestCCountMap;
        reqReleaseTCCountMapId = processData(map[ZR.id]);
        reqReleaseTCCountMapReqId = processData(map[ZR.requirementId]);
        reqReleaseTCCountMapReleaseId = processData(map[ZR.releaseId]);
        reqReleaseTCCountMapTCCount = processData(map[ZR.testcaseCount]);
      }

      let projectId = processData(data[ZR.projectId]),
        reqTreePath = processData(data[ZR.requirementTreePath]),
        releaseNames = processData(data[ZR.releaseNames]),
        actualTestcaseIds = processData(data[ZR.actualTestcaseIds]);

      let requirementTreePath = null;
      requirementTreePath = getReqTreePathByRelease();
      function getReqTreePathByRelease() {
        let arr = reqTreePath.split(",");
        for (i in arr) {
          if (arr[i].includes(releaseName)) {
            return arr[i];
          }
        }
      }

      let requirementTreeNode = "",
        requirementTreeSubNode = "",
        requirementTreeSubNode1 = "";
      let requirementTreeSubNode2 = null, requirementTreeSubNode3 = null, requirementTreeSubNode4 = null,
        requirementTreeSubNode5 = null,
        requirementTreeSubNode6 = null, requirementTreeSubNode7 = null, requirementTreeSubNode8 = null,
        requirementTreeSubNode9 = null,
        requirementTreeSubNode10 = null, requirementTreeSubNode11 = null, requirementTreeSubNode12 = null;;

      getTree(requirementTreeIds, requirementTreeList).then(reqTreePathValue => {
        requirementTreeNode = reqTreePathValue[0];
        if (reqTreePathValue.length > 1)
          requirementTreeSubNode = reqTreePathValue[1];
        if (reqTreePathValue.length > 2)
          requirementTreeSubNode1 = reqTreePathValue[2];
        if (reqTreePathValue.length > 3)
          requirementTreeSubNode2 = reqTreePathValue[3]
        if (reqTreePathValue.length > 4)
          requirementTreeSubNode3 = reqTreePathValue[4]
        if (reqTreePathValue.length > 5)
          requirementTreeSubNode4 = reqTreePathValue[5]
        if (reqTreePathValue.length > 6)
          requirementTreeSubNode5 = reqTreePathValue[6]
        if (reqTreePathValue.length > 7)
          requirementTreeSubNode6 = reqTreePathValue[7]
        if (reqTreePathValue.length > 8)
          requirementTreeSubNode7 = reqTreePathValue[8]
        if (reqTreePathValue.length > 9)
          requirementTreeSubNode8 = reqTreePathValue[9]
        if (reqTreePathValue.length > 10)
          requirementTreeSubNode9 = reqTreePathValue[10]
        if (reqTreePathValue.length > 11)
          requirementTreeSubNode10 = reqTreePathValue[11]
        if (reqTreePathValue.length > 12)
          requirementTreeSubNode11 = reqTreePathValue[12]

        if (reqTreePathValue.length > 1)
          requirementTreeSubNode12 = reqTreePathValue[13]



        getUserName(createdBy, usersList).then((response) => {
          if (response !== null) {
            createdBy = response;
          }

          if (requirementTreeIds === undefined) {
            requirementTreeIds = "";
          } else {
            requirementTreeIds = requirementTreeIds.toString();
          }

          if (actualTestcaseIds.length < 0) {
            actualTestcaseIds = "NULL";
          } else {
            actualTestcaseIds = actualTestcaseIds.toString();
            reqWiseTestCaseIds = actualTestcaseIds.split(",");
          }



          var dataList = {
            releaseName: releaseName,
            projectName: projectName,
            requirementTreePath: requirementTreePath,
            RequirementTreeNode: requirementTreeNode,
            RequirementTreeSubNode: requirementTreeSubNode,
            RequirementTreeSubNode1: requirementTreeSubNode1,
            RequirementTreeSubNode2: requirementTreeSubNode2,
            RequirementTreeSubNode3: requirementTreeSubNode3,
            RequirementTreeSubNode4: requirementTreeSubNode4,
            RequirementTreeSubNode5: requirementTreeSubNode5,
            RequirementTreeSubNode6: requirementTreeSubNode6,
            RequirementTreeSubNode7: requirementTreeSubNode7,
            RequirementTreeSubNode8: requirementTreeSubNode8,
            RequirementTreeSubNode9: requirementTreeSubNode9,
            RequirementTreeSubNode10: requirementTreeSubNode10,
            RequirementTreeSubNode11: requirementTreeSubNode11,
            RequirementTreeSubNode12: requirementTreeSubNode12,

            requirementName: requirementName,
            priority: priority,
            createdOn: createdOn,
            reqCreationDate: reqCreationDate,
            createdBy: createdBy,
            lastModifiedBy: lastModifiedBy,
            lastModifiedOn: lastModifiedOn,
            externalId: externalId,
            requirementType: requirementType,
            ReqStatus: reqStatus,
            attachmentCount: attachmentCount,
            releaseIds: releaseIds.toString(),
            reqReleaseTCCountMapId: reqReleaseTCCountMapId,
            reqReleaseTCCountMapReqId: reqReleaseTCCountMapReqId,
            reqReleaseTCCountMapReleaseId: reqReleaseTCCountMapReleaseId,
            reqReleaseTCCountMapTCCount: reqReleaseTCCountMapTCCount,
            releaseNames: releaseNames.toString(),
            actualTestcaseIds: actualTestcaseIds,
            component: component
          };

          let keyList = {
            projectId: projectId,
            releaseId: releaseId,
            requirementID: requirementId,
            requirementTreeIds: requirementTreeIds,
          };

          let reqWiseTestCaseList = [];
          for (tc in reqWiseTestCaseIds) {
            if (reqWiseTestCaseIds[tc].length > 0) {
              let reqTestCaseId = reqWiseTestCaseIds[tc];
              var testCaseIDObj = {
                reqTestCaseId: reqTestCaseId,
                //       actualTestcaseIds:dataList["actualTestcaseIds"],
                reqReleaseTCCountMapTCCount: dataList["reqReleaseTCCountMapTCCount"],
              };
              //  let tempData = { ...testCaseIDObj, ...dataList["actualTestcaseIds"] };
              //  reqWiseTestCaseList.push(tempData);
              reqWiseTestCaseList.push(testCaseIDObj);
            }
          }
          if (reqWiseTestCaseList.length === 0) {
            var testCaseIDObj = {
              reqTestCaseId: null,
              //actualTestcaseIds:0,
              reqReleaseTCCountMapTCCount: dataList["reqReleaseTCCountMapTCCount"],
            };
            //  let tempData = { ...testCaseIDObj, ...dataList["actualTestcaseIds"] };
            //  reqWiseTestCaseList.push(tempData);
            //reqWiseTestCaseList.push(testCaseIDObj);
            //console.log("For NULL Entries " , reqWiseTestCaseList)
          }
          resolve([dataList, keyList, reqWiseTestCaseList]);
          return [[dataList, keyList, reqWiseTestCaseList]];
          // });
          // });
        }).catch((error) => {
          console.log(currentTime(), " >> ", "Error on REQ.getUserName CreateBy ", error);
          reject(error);
        });
      }).catch((error) => {
        console.log(currentTime(), " >> ", "Error on REQ.getTree  ", error);
        reject(error);
      });
    } catch (error) {
      console.log(currentTime(), " >> ", error.message);
      reject(error.message);
    }
  }).catch((error) => {
    console.log(currentTime(), " >> ", error);
    reject(error);
  });
};
