/*Defect table view 19/11/2021*/

CREATE OR REPLACE VIEW `v_defects_data` AS

select `d`.`defectId` AS `defectId`,`d`.`defectKey` AS `defectKey`,`d`.`projectKey` AS `projectKey`,`d`.`projectName` AS `projectName`,`d`.`projectCategory` AS `projectCategory`,ifnull(`pm`.`Domain`,'Un-Defined') AS `domainName`,`pm`.`ProjectReference` AS `projectReference`,ifnull(`d`.`resolution`,'Un-Defined') AS `resolution`,ifnull(`d`.`defectSevirity`,'Un-Defined') AS `defectSevirity`,ifnull(`d`.`product`,'Un-Defined') AS `product`,case when `d`.`testLevelDetected` like '%system%' then 'System Test' when `d`.`testLevelDetected` like '%sprint%' then 'Sprint Test' when `d`.`testLevelDetected` like '%IT%' then 'Integration Test' when `d`.`testLevelDetected` like '%UAT%' then 'UAT' when `d`.`testLevelDetected` like '%unit%' then 'Unit Test' else 'Un-Defined' end AS `testLevelDetected`,case when `d`.`testTypes` like '%func%' then 'Functional' when `d`.`testTypes` like '%aut%' then 'Automation' when `d`.`testTypes` like '%per%' then 'Performance' when `d`.`testTypes` like '%sec%' then 'Security Test' when `d`.`testTypes` like '%uni%' then 'Unit Test' else 'Un-Defined' end AS `testTypes`,case when `d`.`status` like '%To Validate%' then 'UAT Ready' when `d`.`status` like '%To Verify%' then 'In Test' when `d`.`status` like '%To Groom%' then 'In Test' when `d`.`status` like '%In Test%' then 'In Test' when `d`.`status` like '%UAT on-going%' then 'In Test' when `d`.`status` like '%Ready%' then 'UAT Ready' when `d`.`status` like '%open%' then 'Open' when `d`.`status` like '%sprintable%' then 'In Progress' when `d`.`status` like '%progress%' then 'In Progress' when `d`.`status` like '%Analysis%' then 'In Progress' when `d`.`status` like '%Business Validated%' then 'In Progress' when `d`.`status` like '%review%' then 'In Review' when `d`.`status` like '%incep%' then 'In Review' when `d`.`status` like '%promot%' then 'To Promote' when `d`.`status` like 'open' then 'Open' when `d`.`status` like 'Backlog' then 'Open' when `d`.`status` like 'Next' then 'Open' when `d`.`status` like '%resol%' then 'Resolved' when `d`.`status` like '%done%' then 'Resolved' when `d`.`status` like '%reopened%' then 'Reopened' when `d`.`status` like '%closed%' then 'Closed' when `d`.`status` like '%prd ready%' then 'Closed' else `d`.`status` end AS `status`,`d`.`createdOn` AS `createdOn`,`d`.`summary` AS `summary`,ifnull(`d`.`createdUserName`,'UnAssigned') AS `createdUserName`,ifnull(`d`.`priority`,'Un-Defined') AS `priority`,ifnull(`d`.`assigneeName`,'UnAssigned') AS `assigneeName`,ifnull(`d`.`assigneeID`,'UnAssigned') AS `assigneeID`,ifnull(`d`.`severity`,'Un-Defined') AS `severity`,ifnull(`d`.`component`,'No Components') AS `component`,ifnull(`d`.`sprintId`,'Un-Defined') AS `sprintId`,ifnull(`d`.`reporter`,'UnAssigned') AS `reporter`,ifnull(`d`.`progress`,'Un-Defined') AS `progress`,ifnull(`d`.`stereoType`,'Un-Defined') AS `stereoType`,ifnull(`d`.`epicLink`,'Un-Defined') AS `epicLink`,ifnull(`d`.`epicName`,'Un-Defined') AS `epicName`,ifnull(`d`.`fixVersions`,'Un-Defined') AS `fixVersions`,`pm`.`ReleaseName` AS `ReleaseName`,ifnull(`d`.`issueType`,'Un-Defined') AS `issueType`,ifnull(`d`.`parentKey`,'Un-Defined') AS `parentKey`,ifnull(`d`.`storyDefectIdFixVersion`,'Un-Defined') AS `storyDefectIdFixVersion`,replace(`pm`.`JIRA_KEY`,' ','') AS `JIRA_KEY` from (`iar_byld_1`.`defect_details` `d` join `iar_byld_1`.`v_project_wise_defect_keys` `pm` on(`d`.`projectKey` = replace(`pm`.`JIRA_KEY`,' ','') and `d`.`fixVersions` = `pm`.`ReleaseName`)) where `d`.`fixVersions` <> 'Un-Defined' 
/*Including Story Defects JIRA URL - 07/12/2021*/

CREATE OR REPLACE VIEW `v_project_ccb_details` AS

select `rm`.`ReleaseName` AS `ReleaseName`,`rm`.`ReleaseDate` AS `ReleaseDate`,`pm`.`ProjectReference` AS `ProjectReference`,`pm`.`ProjectName` AS `ProjectName1`,concat(`pm`.`ProjectReference`,' - ',`pm`.`ProjectName`) AS `ProjectName`,`pm`.`Test_Status` AS `test_status`,`pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`,`pm`.`KeyRemarksTesting_Plain` AS `KeyRemarksTesting_Plain`,`pm`.`Domain` AS `Domain`,`pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`,`pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link1`,case when `pm`.`PROJECTREPOSITORY_Link` like 'file://%' then `pm`.`PROJECTREPOSITORY_Link` when `pm`.`PROJECTREPOSITORY_Link` like 'http://%' then `pm`.`PROJECTREPOSITORY_Link` when `pm`.`PROJECTREPOSITORY_Link` like 'https://%' then `pm`.`PROJECTREPOSITORY_Link` else NULL end AS `PROJECTREPOSITORY_Link`,`pm`.`JIRA_Link` AS `JIRA_Link1`,concat('https://jira.pvgroup.be/issues/?jql=project in (',`jr`.`jira_key`,') AND ((issuetype in("Story Defect") and issueFunction in subtasksOf("fixversion in (',`rm`.`ReleaseName`,')")) OR (issuetype = Defect and fixVersion in (',`rm`.`ReleaseName`,') AND created > startOfMonth(-12))) and status != Closed ORDER BY created DESC') AS `JIRA_Link`,concat('https://jira.pvgroup.be/issues/?jql=project in (',`jr`.`jira_key`,') AND ((issuetype in("Story Defect") and issueFunction in subtasksOf("fixversion in (',`rm`.`ReleaseName`,')"))) and status != Closed ORDER BY created DESC') AS `Story_JIRA_Link`,ifnull(`pm`.`ZEPHYR_Link`,'http://zephyr.pvgroup.intranet/flex/html5/login') AS `ZEPHYR_Link`,concat('https://jira.pvgroup.be/issues/?jql=project in (',`jr`.`jira_key`,') AND issuetype in ("Story") AND fixVersion in (',`rm`.`ReleaseName`,')  ORDER BY created DESC') AS `UserStory_Link`,`pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`,`pm`.`Test_Coordinator` AS `Test_Coordinator`,`pm`.`Scrum_Master` AS `Scrum_Master`,ifnull(`pm`.`Domain`,'Un-Defined') AS `domainName`,ifnull(`pm`.`CCB_STARTUP_Date`,`rm`.`LastRegistration`) AS `CCB_STARTUP_Date`,ifnull(`pm`.`CCB_Ready4SPRINT_Date`,`rm`.`CCB_Ready4SPRINT`) AS `CCB_Ready4SPRINT_Date`,ifnull(`pm`.`CCB_Ready4UAT_Date`,`rm`.`CCB_Ready4UAT`) AS `CCB_Ready4UAT_Date`,ifnull(`pm`.`CCB_Ready4PROD_Date`,`rm`.`CCB_Ready4PROD`) AS `CCB_Ready4PROD_Date`,ifnull(`pm`.`CCB_Closure_Date`,`rm`.`CCB_Closure`) AS `CCB_Closure_Date`,`pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`,`pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`,convert(`pm`.`CCB_STARTUP_Remarks` using utf8) AS `CCB_STARTUP_Remarks`,convert(`pm`.`Ready2StartTestRemarks` using utf8) AS `Ready2StartTestRemarks`,`pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`,convert(`pm`.`CCBReady4SPRINT_Remarks` using utf8) AS `CCBReady4SPRINT_Remarks`,convert(`pm`.`Ready4SprintTestRemarks` using utf8) AS `Ready4SprintTestRemarks`,`pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`,convert(`pm`.`CCBReady4UAT_Remarks` using utf8) AS `CCBReady4UAT_Remarks`,convert(`pm`.`Ready4UATTestRemarks` using utf8) AS `Ready4UATTestRemarks`,`rm`.`LastRegistration` AS `LastRegistrationDate`,ifnull(`pm`.`UAT_Start_Date`,`rm`.`UAT_StartDate`) AS `UAT_START_DATE`,`rm`.`End_UAT` AS `UAT_END_DATE`,`rm`.`Freeze` AS `FREEZE_DATE`,`pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`,convert(`pm`.`CCBReady4PROD_Remarks` using utf8) AS `CCBReady4PROD_Remarks`,convert(`pm`.`Ready4ProdTestRemarks` using utf8) AS `Ready4ProdTestRemarks`,convert(`pm`.`CCB_CLOSURE_Remarks` using utf8) AS `CCB_CLOSURE_Remarks`,`pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`,`pm`.`Project_Manager` AS `Project_Manager`,`tr`.`UNIT_TEST_PATH` AS `unitTestReportPath`,`tr`.`UAT_TEST_PATH` AS `UAT_TestReportPath`,`tr`.`AUTOMATION_PATH` AS `automationReportPath`,`tr`.`PERFORMANCE_PATH` AS `performanceReportPath`,`tr`.`SECURITY_TEST_PATH` AS `securityReportPath`,`tr`.`IT_TEST_PATH` AS `IT_TestReportPath`,`tr`.`CCB_GATING_PATH` AS `CCB_GATING_PATH`,`tr`.`MASTER_TEST_PLAN_PATH` AS `MASTER_TEST_PLAN_PATH`,`tr`.`BUSSINESS_GO_PATH` AS `BUSSINESS_GO_PATH`,`pm`.`Jira_defect_projects` AS `Jira_defect_projects`,`pm`.`DetailedTestStatus` AS `DetailedTestStatus`,`pm`.`Test_Coordinator_email` AS `Test_Coordinator_email`,`pm`.`Project_Manager_email` AS `Project_Manager_email`,`pm`.`Scrum_Master_email` AS `Scrum_Master_email`,`pm`.`Delivery_Master_Lead_email` AS `Delivery_Master_Lead_email`,substring_index(`pm`.`Jira_defect_projects`,',',1) AS `JIRA_KEY`,concat('http://weconnect.pvgroup.intranet/project/general/release/Lists/CCB%20Dashboard/EditForm.aspx?ID=',`pm`.`SP_ID`) AS `SP_ProjectEditLink`,`pm`.`HistoryTestKeyMessage` AS `HistoryTestKeyMessage` from (((`v_release_moments` `rm` left join `project_milestones_status` `pm` on(convert(`rm`.`ReleaseName` using utf8) = replace(trim(substring_index(`pm`.`ReleaseName`,'-',1)),'+',''))) left join `v_test_reports` `tr` on(`pm`.`ReleaseName` = `tr`.`ReleaseName` and `pm`.`ProjectReference` = `tr`.`ProjectReference`)) left join `v_project_wise_defect_keys_grouping` `jr` on(`pm`.`ReleaseName` = `jr`.`ReleaseName` and `pm`.`ProjectReference` = `jr`.`ProjectReference`)) 



/*Include epickName in defect Table  06-01-2022*/

ALTER TABLE `defect_details`
	ADD COLUMN `epicName` LONGTEXT NULL DEFAULT NULL AFTER `epicLink`;
ALTER TABLE `defect_details`
	CHANGE COLUMN `epicName` `epicName` LONGTEXT NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `epicLink`;

/*Update Defect View 06-01-2022*/
CREATE OR REPLACE VIEW `v_defects_data` AS

select `d`.`defectId` AS `defectId`,`d`.`defectKey` AS `defectKey`,`d`.`projectKey` AS `projectKey`,`d`.`projectName` AS `projectName`,`d`.`projectCategory` AS `projectCategory`,ifnull(`pm`.`Domain`,'Un-Defined') AS `domainName`,`pm`.`ProjectReference` AS `projectReference`,ifnull(`d`.`resolution`,'Un-Defined') AS `resolution`,ifnull(`d`.`defectSevirity`,'Un-Defined') AS `defectSevirity`,ifnull(`d`.`product`,'Un-Defined') AS `product`,case when `d`.`testLevelDetected` like '%system%' then 'System Test' when `d`.`testLevelDetected` like '%sprint%' then 'Sprint Test' when `d`.`testLevelDetected` like '%IT%' then 'Integration Test' when `d`.`testLevelDetected` like '%UAT%' then 'UAT' when `d`.`testLevelDetected` like '%unit%' then 'Unit Test' else 'Un-Defined' end AS `testLevelDetected`,case when `d`.`testTypes` like '%func%' then 'Functional' when `d`.`testTypes` like '%aut%' then 'Automation' when `d`.`testTypes` like '%per%' then 'Performance' when `d`.`testTypes` like '%sec%' then 'Security Test' when `d`.`testTypes` like '%uni%' then 'Unit Test' else 'Un-Defined' end AS `testTypes`,case when `d`.`status` like '%To Validate%' then 'UAT Ready' when `d`.`status` like '%To Verify%' then 'In Test' when `d`.`status` like '%To Groom%' then 'In Test' when `d`.`status` like '%In Test%' then 'In Test' when `d`.`status` like '%UAT on-going%' then 'In Test' when `d`.`status` like '%Ready%' then 'UAT Ready' when `d`.`status` like '%open%' then 'Open' when `d`.`status` like '%sprintable%' then 'In Progress' when `d`.`status` like '%progress%' then 'In Progress' when `d`.`status` like '%Analysis%' then 'In Progress' when `d`.`status` like '%Business Validated%' then 'In Progress' when `d`.`status` like '%review%' then 'In Review' when `d`.`status` like '%incep%' then 'In Review' when `d`.`status` like '%promot%' then 'To Promote' when `d`.`status` like 'open' then 'Open' when `d`.`status` like 'Backlog' then 'Open' when `d`.`status` like 'Next' then 'Open' when `d`.`status` like '%resol%' then 'Resolved' when `d`.`status` like '%done%' then 'Resolved' when `d`.`status` like '%reopened%' then 'Reopened' when `d`.`status` like '%closed%' then 'Closed' when `d`.`status` like '%prd ready%' then 'Closed' else `d`.`status` end AS `status`,`d`.`createdOn` AS `createdOn`,`d`.`summary` AS `summary`,ifnull(`d`.`createdUserName`,'UnAssigned') AS `createdUserName`,ifnull(`d`.`priority`,'Un-Defined') AS `priority`,ifnull(`d`.`assigneeName`,'UnAssigned') AS `assigneeName`,ifnull(`d`.`assigneeID`,'UnAssigned') AS `assigneeID`,ifnull(`d`.`severity`,'Un-Defined') AS `severity`,ifnull(`d`.`component`,'No Components') AS `component`,ifnull(`d`.`sprintId`,'Un-Defined') AS `sprintId`,ifnull(`d`.`reporter`,'UnAssigned') AS `reporter`,ifnull(`d`.`progress`,'Un-Defined') AS `progress`,ifnull(`d`.`stereoType`,'Un-Defined') AS `stereoType`,ifnull(`d`.`epicLink`,'Un-Defined') AS `epicLink`,ifnull(`d`.`epicName`,'Un-Defined') AS `epicName`,ifnull(`d`.`fixVersions`,'Un-Defined') AS `fixVersions`,`pm`.`ReleaseName` AS `ReleaseName`,ifnull(`d`.`issueType`,'Un-Defined') AS `issueType`,ifnull(`d`.`parentKey`,'Un-Defined') AS `parentKey`,ifnull(`d`.`storyDefectIdFixVersion`,'Un-Defined') AS `storyDefectIdFixVersion`,replace(`pm`.`JIRA_KEY`,' ','') AS `JIRA_KEY` from (`iar_byld_1`.`defect_details` `d` join `iar_byld_1`.`v_project_wise_defect_keys` `pm` on(`d`.`projectKey` = replace(`pm`.`JIRA_KEY`,' ','') and `d`.`fixVersions` = `pm`.`ReleaseName`)) where `d`.`fixVersions` <> 'Un-Defined' 


/*Updated Defect View 14-02-2022 */
CREATE OR REPLACE VIEW `v_defects_data` AS

select `d`.`defectId` AS `defectId`,`d`.`defectKey` AS `defectKey`,`d`.`projectKey` AS `projectKey`,`d`.`projectName` AS `projectName`,`d`.`projectCategory` AS `projectCategory`,ifnull(`pm`.`Domain`,'Un-Defined') AS `domainName`,`pm`.`ProjectReference` AS `projectReference`,ifnull(`d`.`resolution`,'Un-Defined') AS `resolution`,ifnull(`d`.`defectSevirity`,'Un-Defined') AS `defectSevirity`,ifnull(`d`.`product`,'Un-Defined') AS `product`,case when `d`.`testLevelDetected` like '%system%' then 'System Test' when `d`.`testLevelDetected` like '%sprint%' then 'Sprint Test' when `d`.`testLevelDetected` like '%IT%' then 'Integration Test' when `d`.`testLevelDetected` like '%UAT%' then 'UAT' when `d`.`testLevelDetected` like '%unit%' then 'Unit Test' else 'Un-Defined' end AS `testLevelDetected`,case when `d`.`testTypes` like '%func%' then 'Functional' when `d`.`testTypes` like '%aut%' then 'Automation' when `d`.`testTypes` like '%per%' then 'Performance' when `d`.`testTypes` like '%sec%' then 'Security Test' when `d`.`testTypes` like '%uni%' then 'Unit Test' else 'Un-Defined' end AS `testTypes`,case when `d`.`status` like '%To Validate%' then 'UAT Ready' when `d`.`status` like '%To Verify%' then 'In Test' when `d`.`status` like '%To Groom%' then 'In Test' when `d`.`status` like '%In Test%' then 'In Test' when `d`.`status` like '%UAT on-going%' then 'In Test' when `d`.`status` like '%Ready%' then 'UAT Ready' when `d`.`status` like '%open%' then 'Open' when `d`.`status` like '%sprintable%' then 'In Progress' when `d`.`status` like '%progress%' then 'In Progress' when `d`.`status` like '%Analysis%' then 'In Progress' when `d`.`status` like '%Business Validated%' then 'In Progress' when `d`.`status` like '%review%' then 'In Review' when `d`.`status` like '%incep%' then 'In Review' when `d`.`status` like '%promot%' then 'To Promote' when `d`.`status` like 'open' then 'Open' when `d`.`status` like 'Backlog' then 'Open' when `d`.`status` like 'Next' then 'Open' when `d`.`status` like '%resol%' then 'Resolved' when `d`.`status` like '%done%' then 'Resolved' when `d`.`status` like '%reopened%' then 'Reopened' when `d`.`status` like '%closed%' then 'Closed' when `d`.`status` like '%prd ready%' then 'Closed' else `d`.`status` end AS `status`,`d`.`createdOn` AS `createdOn`,`d`.`summary` AS `summary`,ifnull(`d`.`createdUserName`,'UnAssigned') AS `createdUserName`,ifnull(`d`.`priority`,'Un-Defined') AS `priority`,ifnull(`d`.`assigneeName`,'UnAssigned') AS `assigneeName`,ifnull(`d`.`assigneeID`,'UnAssigned') AS `assigneeID`,ifnull(`d`.`severity`,'Un-Defined') AS `severity`,ifnull(`d`.`component`,'No Components') AS `component`,ifnull(`d`.`sprintId`,'Un-Defined') AS `sprintId`,ifnull(`d`.`reporter`,'UnAssigned') AS `reporter`,ifnull(`d`.`progress`,'Un-Defined') AS `progress`,ifnull(`d`.`stereoType`,'Un-Defined') AS `stereoType`,ifnull(`d`.`epicLink`,'Un-Defined') AS `epicLink`,ifnull(`d`.`epicName`,'No Epic Mapped') AS `epicName`,ifnull(`d`.`fixVersions`,'Un-Defined') AS `fixVersions`,`pm`.`ReleaseName` AS `ReleaseName`,ifnull(`d`.`issueType`,'Un-Defined') AS `issueType`,ifnull(`d`.`parentKey`,'Un-Defined') AS `parentKey`,ifnull(`d`.`storyDefectIdFixVersion`,'Un-Defined') AS `storyDefectIdFixVersion`,replace(`pm`.`JIRA_KEY`,' ','') AS `JIRA_KEY` from (`iar_byld_1`.`defect_details` `d` join `iar_byld_1`.`v_project_wise_defect_keys` `pm` on(`d`.`projectKey` = replace(`pm`.`JIRA_KEY`,' ','') and `d`.`fixVersions` = `pm`.`ReleaseName`)) where `d`.`fixVersions` <> 'Un-Defined' 

/*Projectmilestones table updated  14-02-2022*/

ALTER TABLE `project_milestones_status`
	CHANGE COLUMN `DEL_STA_PIDStage1` `DEL_STA_PIDStage1` MEDIUMTEXT NULL DEFAULT NULL COLLATE 'utf8mb3_general_ci' AFTER `DEL_SPR_SolutionOutlineArchitecture`;

/*Releasemilestones view updated 14-02-2022*/

CREATE OR REPLACE VIEW `v_release_milestones_status` AS

select replace(trim(substring_index(`pm`.`ReleaseName`,'-',1)),'+','') AS `ReleaseName`,`pm`.`ReleaseName` AS `ReleaseMoment`,`pm`.`ReleaseName` AS `RM_ReleaseName`,(str_to_date(substring_index(substring_index(`pm`.`ReleaseName`,'-',1),'.',3),'%Y.%m.%d')) AS `ReleaseDate`,`pm`.`ProjectReference` AS `ProjectReference`,`pm`.`ProjectName` AS `ProjectName1`,concat(`pm`.`ProjectReference`,' - ',`pm`.`ProjectName`) AS `ProjectName`,(null) AS `ReleaseTestStatus`,(null) AS `TestKeyMessage`,(null) AS `Risks_Issues`,(null) AS `LastRegistration`,(null) AS `CCB_Ready4SPRINT`,(null) AS `CCB_Ready4UAT`,(null) AS `UAT_StartDate`,(null) AS `End_UAT`,(null) AS `Freeze`,(null) AS `CCB_Ready4PROD`,(null) AS `CCB_Closure`,(null) AS `SP_ReleaseEditLink`,(null) AS `HistoryTestKeyMessage` from (`project_milestones_status` `pm`) group by `pm`.`ReleaseName` order by `pm`.`ReleaseName` 

/*Release moment view updated 14-02-2022*/

CREATE OR REPLACE VIEW `v_release_moments` AS

select `rm`.`ReleaseName` AS `ReleaseName`,`rm`.`ReleaseMoment` AS `releasemoment`,`rm`.`ReleaseDate` AS `RD`,ifnull(`rm`.`ReleaseDate`,(STR_TO_DATE(SUBSTRING_INDEX(`rm`.`ReleaseMoment`,' - ',-1), '%d/%m/%Y'))) AS `ReleaseDate`,`rm`.`LastRegistration` AS `LastRegistration`,`rm`.`CCB_Ready4SPRINT` AS `CCB_Ready4SPRINT`,`rm`.`CCB_Ready4UAT` AS `CCB_Ready4UAT`,`rm`.`CCB_Ready4PROD` AS `CCB_Ready4PROD`,`rm`.`CCB_Closure` AS `CCB_Closure`,`rm`.`LastRegistration` AS `LastRegistrationDate`,`rm`.`UAT_StartDate` AS `UAT_StartDate`,`rm`.`End_UAT` AS `End_UAT`,`rm`.`Freeze` AS `Freeze`,`rm`.`SP_ReleaseEditLink` AS `SP_ReleaseEditLink`,`rm`.`ReleaseTestStatus` AS `ReleaseTestStatus`,`rm`.`TestKeyMessage` AS `TestKeyMessage`,`rm`.`Risks_Issues` AS `Risks_Issues`,`rm`.`HistoryTestKeyMessage` AS `HistoryTestKeyMessage` from `v_release_milestones_status` `rm` 

/*Requeriemnt id too long issue fixec 23/2/22*/

ALTER TABLE `requirement_details`
	ADD COLUMN `requirementTreeIds` LONGTEXT NULL DEFAULT NULL AFTER `reqReleaseTCCountMapId`,
	DROP COLUMN `requirementTreeIds`;

/*Requeriemnt treeid too long issue fixed 23/2/22*/

ALTER TABLE `requirement_wise_test_details`
	ADD COLUMN `requirementTreeIds` LONGTEXT NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `reqReleaseTCCountMapReqId`,
	DROP COLUMN `requirementTreeIds`;

/*New share point link updated - 24/02/2022*/

CREATE OR REPLACE VIEW `v_project_ccb_details` AS

select `rm`.`ReleaseName` AS `ReleaseName`,`rm`.`ReleaseDate` AS `ReleaseDate`,`pm`.`ProjectReference` AS `ProjectReference`,`pm`.`ProjectName` AS `ProjectName1`,concat(`pm`.`ProjectReference`,' - ',`pm`.`ProjectName`) AS `ProjectName`,`pm`.`Test_Status` AS `test_status`,`pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`,`pm`.`KeyRemarksTesting_Plain` AS `KeyRemarksTesting_Plain`,`pm`.`Domain` AS `Domain`,`pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`,`pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link1`,case when `pm`.`PROJECTREPOSITORY_Link` like 'file://%' then `pm`.`PROJECTREPOSITORY_Link` when `pm`.`PROJECTREPOSITORY_Link` like 'http://%' then `pm`.`PROJECTREPOSITORY_Link` when `pm`.`PROJECTREPOSITORY_Link` like 'https://%' then `pm`.`PROJECTREPOSITORY_Link` else NULL end AS `PROJECTREPOSITORY_Link`,`pm`.`JIRA_Link` AS `JIRA_Link1`,concat('https://jira.pvgroup.be/issues/?jql=project in (',`jr`.`jira_key`,') AND ((issuetype in("Story Defect") and issueFunction in subtasksOf("fixversion in (',`rm`.`ReleaseName`,')")) OR (issuetype = Defect and fixVersion in (',`rm`.`ReleaseName`,') AND created > startOfMonth(-12))) and status != Closed ORDER BY created DESC') AS `JIRA_Link`,concat('https://jira.pvgroup.be/issues/?jql=project in (',`jr`.`jira_key`,') AND ((issuetype in("Story Defect") and issueFunction in subtasksOf("fixversion in (',`rm`.`ReleaseName`,')"))) and status != Closed ORDER BY created DESC') AS `Story_JIRA_Link`,ifnull(`pm`.`ZEPHYR_Link`,'http://zephyr.pvgroup.intranet/flex/html5/login') AS `ZEPHYR_Link`,concat('https://jira.pvgroup.be/issues/?jql=project in (',`jr`.`jira_key`,') AND issuetype in ("Story") AND fixVersion in (',`rm`.`ReleaseName`,')  ORDER BY created DESC') AS `UserStory_Link`,`pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`,`pm`.`Test_Coordinator` AS `Test_Coordinator`,`pm`.`Scrum_Master` AS `Scrum_Master`,ifnull(`pm`.`Domain`,'Un-Defined') AS `domainName`,ifnull(`pm`.`CCB_STARTUP_Date`,`rm`.`LastRegistration`) AS `CCB_STARTUP_Date`,ifnull(`pm`.`CCB_Ready4SPRINT_Date`,`rm`.`CCB_Ready4SPRINT`) AS `CCB_Ready4SPRINT_Date`,ifnull(`pm`.`CCB_Ready4UAT_Date`,`rm`.`CCB_Ready4UAT`) AS `CCB_Ready4UAT_Date`,ifnull(`pm`.`CCB_Ready4PROD_Date`,`rm`.`CCB_Ready4PROD`) AS `CCB_Ready4PROD_Date`,ifnull(`pm`.`CCB_Closure_Date`,`rm`.`CCB_Closure`) AS `CCB_Closure_Date`,`pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`,`pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`,convert(`pm`.`CCB_STARTUP_Remarks` using utf8) AS `CCB_STARTUP_Remarks`,convert(`pm`.`Ready2StartTestRemarks` using utf8) AS `Ready2StartTestRemarks`,`pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`,convert(`pm`.`CCBReady4SPRINT_Remarks` using utf8) AS `CCBReady4SPRINT_Remarks`,convert(`pm`.`Ready4SprintTestRemarks` using utf8) AS `Ready4SprintTestRemarks`,`pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`,convert(`pm`.`CCBReady4UAT_Remarks` using utf8) AS `CCBReady4UAT_Remarks`,convert(`pm`.`Ready4UATTestRemarks` using utf8) AS `Ready4UATTestRemarks`,`rm`.`LastRegistration` AS `LastRegistrationDate`,ifnull(`pm`.`UAT_Start_Date`,`rm`.`UAT_StartDate`) AS `UAT_START_DATE`,`rm`.`End_UAT` AS `UAT_END_DATE`,`rm`.`Freeze` AS `FREEZE_DATE`,`pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`,convert(`pm`.`CCBReady4PROD_Remarks` using utf8) AS `CCBReady4PROD_Remarks`,convert(`pm`.`Ready4ProdTestRemarks` using utf8) AS `Ready4ProdTestRemarks`,convert(`pm`.`CCB_CLOSURE_Remarks` using utf8) AS `CCB_CLOSURE_Remarks`,`pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`,`pm`.`Project_Manager` AS `Project_Manager`,`tr`.`UNIT_TEST_PATH` AS `unitTestReportPath`,`tr`.`UAT_TEST_PATH` AS `UAT_TestReportPath`,`tr`.`AUTOMATION_PATH` AS `automationReportPath`,`tr`.`PERFORMANCE_PATH` AS `performanceReportPath`,`tr`.`SECURITY_TEST_PATH` AS `securityReportPath`,`tr`.`IT_TEST_PATH` AS `IT_TestReportPath`,`tr`.`CCB_GATING_PATH` AS `CCB_GATING_PATH`,`tr`.`MASTER_TEST_PLAN_PATH` AS `MASTER_TEST_PLAN_PATH`,`tr`.`BUSSINESS_GO_PATH` AS `BUSSINESS_GO_PATH`,`pm`.`Jira_defect_projects` AS `Jira_defect_projects`,`pm`.`DetailedTestStatus` AS `DetailedTestStatus`,`pm`.`Test_Coordinator_email` AS `Test_Coordinator_email`,`pm`.`Project_Manager_email` AS `Project_Manager_email`,`pm`.`Scrum_Master_email` AS `Scrum_Master_email`,`pm`.`Delivery_Master_Lead_email` AS `Delivery_Master_Lead_email`,substring_index(`pm`.`Jira_defect_projects`,',',1) AS `JIRA_KEY`,concat('https://pvgroupbe.sharepoint.com/sites/ChangeGovernance/Lists/CCB%20Dashboard/CCBMeetingMinutes.aspx?ID=',`pm`.`SP_ID`) AS `SP_ProjectEditLink`,`pm`.`HistoryTestKeyMessage` AS `HistoryTestKeyMessage` from (((`v_release_moments` `rm` left join `project_milestones_status` `pm` on(convert(`rm`.`ReleaseName` using utf8) = replace(trim(substring_index(`pm`.`ReleaseName`,'-',1)),'+',''))) left join `v_test_reports` `tr` on(`pm`.`ReleaseName` = `tr`.`ReleaseName` and `pm`.`ProjectReference` = `tr`.`ProjectReference`)) left join `v_project_wise_defect_keys_grouping` `jr` on(`pm`.`ReleaseName` = `jr`.`ReleaseName` and `pm`.`ProjectReference` = `jr`.`ProjectReference`))

/* Adding Epic Link in Requirements and Test Execution - 09-03-2022*/

ALTER TABLE `test_execution_results`
	ADD COLUMN `epicLink` VARCHAR(500) NULL DEFAULT NULL AFTER `reqAltId`;

ALTER TABLE `requirement_details`
	ADD COLUMN `epicLink` VARCHAR(100) NULL DEFAULT NULL AFTER `externalId`;

/* Test Results View - 09-03-2022*/
CREATE OR REPLACE VIEW `v_test_results` AS

select `test_execution_results`.`id` AS `id`,trim(substring_index(replace(`test_execution_results`.`projectName`,'–','-'),'-',1)) AS `ProjectReference`,trim(substr(`test_execution_results`.`projectName`,locate('-',`test_execution_results`.`projectName`) + 1)) AS `ProjectName`,`project_milestones_status`.`Domain` AS `DomainName`,`test_execution_results`.`projectName` AS `Project`,`test_execution_results`.`releaseName` AS `ReleaseName`,`test_execution_results`.`hierarchy` AS `Hierarchy`,nullif(`test_execution_results`.`folder`,'') AS `Folder`,nullif(`test_execution_results`.`subFolder`,'') AS `subFolder`,nullif(`test_execution_results`.`subFolder1`,'') AS `subFolder1`,nullif(`test_execution_results`.`subFolder2`,'') AS `subFolder2`,nullif(`test_execution_results`.`subfolder3`,'') AS `subFolder3`,nullif(`test_execution_results`.`subfolder4`,'') AS `subFolder4`,nullif(`test_execution_results`.`subfolder5`,'') AS `subFolder5`,nullif(`test_execution_results`.`subfolder6`,'') AS `subFolder6`,nullif(`test_execution_results`.`subfolder7`,'') AS `subFolder7`,nullif(`test_execution_results`.`subfolder8`,'') AS `subFolder8`,nullif(`test_execution_results`.`subfolder9`,'') AS `subFolder9`,nullif(`test_execution_results`.`subfolder10`,'') AS `subFolder10`,`test_execution_results`.`createdOn` AS `CreatedOn`,`test_execution_results`.`requirementId` AS `RequirementId`,`test_execution_results`.`reqAltId` AS `ReqAltId`,`test_execution_results`.`epicLink` AS `epicLink`,`test_execution_results`.`requirementName` AS `RequirementName`,`test_execution_results`.`environment` AS `Environment`,`test_execution_results`.`testCaseAltId` AS `RestCaseAltId`,`test_execution_results`.`testCaseName` AS `RestCaseName`,`test_execution_results`.`testCaseId` AS `TestCaseId`,`test_execution_results`.`automation` AS `Automation`,`test_execution_results`.`cycle` AS `Cycle`,`test_execution_results`.`testStatus` AS `tStatus`,case when `test_execution_results`.`testStatus` like '%fail%' then 'Fail' when `test_execution_results`.`testStatus` like '%pass%' then 'Pass' when `test_execution_results`.`testStatus` like '%incomp%' then 'Incomplete' when `test_execution_results`.`testStatus` like '%blocked%' then 'Blocked' when `test_execution_results`.`testStatus` like '%Not exec%' then 'Not Executed' when `test_execution_results`.`testStatus` like '%N/A%' then 'N/A' else 'Not Executed' end AS `testStatus`,`test_execution_results`.`executedOn` AS `ExecutedOn`,`test_execution_results`.`tags` AS `Tags`,`test_execution_results`.`priority` AS `priority`,`test_execution_results`.`defects` AS `Defects`,`test_execution_results`.`executedBy` AS `ExecutedBy`,`test_execution_results`.`createdBy` AS `CreatedBy`,`test_execution_results`.`insertedOn` AS `insertedOn`,`test_execution_results`.`subFolderLevel` AS `subFolderLevel`,`test_execution_results`.`updatedOn` AS `updatedOn`,`test_execution_results`.`component` AS `component` from (`test_execution_results` left join `project_milestones_status` on(trim(convert(substring_index(replace(`test_execution_results`.`projectName`,'–','-'),'-',1) using utf8)) = `project_milestones_status`.`ProjectReference` and trim(substr(`test_execution_results`.`projectName`,locate('-',`test_execution_results`.`projectName`) + 1)) = `project_milestones_status`.`ProjectName` and convert(trim(`test_execution_results`.`releaseName`) using utf8) = `project_milestones_status`.`ReleaseName`)) ;

/* User Story View - 09-03-2022*/

CREATE OR REPLACE VIEW `v_user_story` AS

select `d`.`userStoryID` AS `userStoryID`,`d`.`storyKey` AS `storyKey`,`d`.`projectKey` AS `projectKey`,`d`.`projectName` AS `projectName`,case when `d`.`status` like '%Backlog%' then 'Backlog' when `d`.`status` like '%BA in progress%' then 'Backlog' when `d`.`status` like '%BUSINESS VALIDATED%' then 'Backlog' when `d`.`status` like '%Open%' then 'Backlog' when `d`.`status` like 'Reopened' then 'Backlog' when `d`.`status` like '%BA to validate%' then 'Analysis' when `d`.`status` like '%FA in progress%' then 'Analysis' when `d`.`status` like '%FA to validate%' then 'Analysis' when `d`.`status` like '%TA in progress%' then 'Analysis' when `d`.`status` like '%TA to validate%' then 'Analysis' when `d`.`status` like '%To groom%' then 'Analysis' when `d`.`status` like '%Sprintable%' then 'Analysis' when `d`.`status` like '%In Inception%' then 'Analysis' when `d`.`status` like '%Ready%' then 'Analysis' when `d`.`status` like '%Next%' then 'Analysis' when `d`.`status` like '%In progress%' then 'Development' when `d`.`status` like 'In review' then 'Development' when `d`.`status` like 'To promote' then 'Development' when `d`.`status` like 'In test' then 'Build/Test' when `d`.`status` like '%To Verify%' then 'Build/Test' when `d`.`status` like '%To review%' then 'Build/Test' when `d`.`status` like '%UAT ON-GOING%' then 'Build/Test' when `d`.`status` like 'Ready for uat' then 'Closed' when `d`.`status` like '%Closed%' then 'Closed' when `d`.`status` like '%prd ready%' then 'To Validate' when `d`.`status` like '%PRD TO VERIFY%' then 'To Validate' when `d`.`status` like '%Resolved%' then 'To Validate' when `d`.`status` like '%PO To validate%' then 'To Validate' when `d`.`status` like '%To validate%' then 'To Validate' end AS `storyStatus`,`d`.`summary` AS `summary`,ifnull(`pm`.`Domain`,'Un-Defined') AS `domainName`,`pm`.`ProjectReference` AS `ProjectReference`,ifnull(`d`.`resolution`,'Un-Defined') AS `resolution`,ifnull(`d`.`priority`,'Un-Defined') AS `priority`,ifnull(`d`.`assigneeName`,'UnAssigned') AS `assigneeName`,ifnull(`d`.`severity`,'Un-Defined') AS `severity`,ifnull(`d`.`component`,'No Components') AS `component`,ifnull(`d`.`sprintId`,'Un-Defined') AS `sprintId`,ifnull(`d`.`reporter`,'UnAssigned') AS `reporter`,ifnull(`d`.`epicLink`,'Un-Defined') AS `epicLink`,ifnull(`d`.`epicName`,'No Epic Mapped') AS `epicName`,ifnull(`d`.`fixVersions`,'Un-Defined') AS `fixVersions`,`pm`.`ReleaseName` AS `ReleaseName`,ifnull(`d`.`issueType`,'Un-Defined') AS `issueType`,replace(`pm`.`JIRA_KEY`,' ','') AS `JIRA_KEY` from (`iar_byld_1`.`user_story_details` `d` join `iar_byld_1`.`v_project_wise_defect_keys` `pm` on(`d`.`projectKey` = replace(`pm`.`JIRA_KEY`,' ','') and `d`.`fixVersions` = `pm`.`ReleaseName`)) 

/* Base Requirement View - 09-03-2022*/

CREATE OR REPLACE VIEW `v_base_req` AS

select trim(substring_index(replace(`b`.`projectName`,'–','-'),'-',1)) AS `ProjectReference`,trim(substring_index(replace(`b`.`projectName`,'–','-'),'-',locate('-',`b`.`projectName`) + 1)) AS `ProjectName`,`b`.`projectName` AS `project`,`b`.`releaseName` AS `releaseName`,`b`.`id` AS `id`,`b`.`RequirementID` AS `requirementId`,`b`.`requirementTreeIds` AS `requirementTreeIds`,`b`.`reqReleaseTCCountMapTCCount` AS `reqReleaseTCCountMapTCCount`,`b`.`projectId` AS `projectId`,`b`.`actualTestcaseIds` AS `actualTestCaseIds`,`rd`.`reqTestCaseId` AS `reqTestCaseId`,`b`.`epicLink` AS `epicLink`,`rd`.`parentReqId` AS `parentReqId`,`b`.`insertedOn` AS `insertedOn`,`b`.`updatedOn` AS `updatedOn`,`b`.`releaseId` AS `releaseId`,`b`.`RequirementTreeNode` AS `requirementTreeNode`,`b`.`RequirementTreeSubNode` AS `requirementTreeSubnode`,`b`.`RequirementTreeSubNode1` AS `requirementTreeSubnode1`,`b`.`RequirementTreeSubNode2` AS `RequirementTreeSubNode2`,`b`.`RequirementTreeSubNode3` AS `RequirementTreeSubNode3`,`b`.`RequirementTreeSubNode4` AS `RequirementTreeSubNode4`,`b`.`RequirementTreeSubNode5` AS `RequirementTreeSubNode5`,`b`.`RequirementTreeSubNode6` AS `RequirementTreeSubNode6`,`b`.`RequirementTreeSubNode7` AS `RequirementTreeSubNode7`,`b`.`RequirementTreeSubNode8` AS `RequirementTreeSubNode8`,`b`.`RequirementTreeSubNode9` AS `RequirementTreeSubNode9`,`b`.`RequirementTreeSubNode10` AS `RequirementTreeSubNode10`,`b`.`RequirementTreeSubNode11` AS `RequirementTreeSubNode11`,`b`.`RequirementTreeSubNode12` AS `RequirementTreeSubNode12` from (`requirement_details` `b` left join `requirement_wise_test_details` `rd` on(`rd`.`projectId` = `b`.`projectId` and `rd`.`releaseId` = `b`.`releaseId` and `rd`.`RequirementID` = `b`.`RequirementID`)) 

CREATE OR REPLACE VIEW `v_base_req_wise_test` AS

select `br`.`id` AS `id`,`br`.`ProjectReference` AS `projectreference`,`br`.`ProjectName` AS `projectname`,`br`.`project` AS `project`,`br`.`releaseName` AS `releasename`,`br`.`requirementId` AS `requirementid`,`br`.`requirementTreeIds` AS `requirementTreeIds`,`br`.`reqReleaseTCCountMapTCCount` AS `reqReleaseTCCountMapTCCount`,`br`.`projectId` AS `projectid`,`br`.`actualTestCaseIds` AS `actualTestCaseIds`,`br`.`reqTestCaseId` AS `reqTestCaseId`,`br`.`parentReqId` AS `parentReqId`,`br`.`releaseId` AS `releaseid`,`br`.`epicLink` AS `epicLink`,`br`.`requirementTreeNode` AS `requirementTreeNode`,`br`.`requirementTreeSubnode` AS `requirementTreeSubnode`,`br`.`requirementTreeSubnode1` AS `requirementTreeSubnode1`,`br`.`RequirementTreeSubNode2` AS `RequirementTreeSubNode2`,`br`.`RequirementTreeSubNode3` AS `RequirementTreeSubNode3`,`br`.`RequirementTreeSubNode4` AS `RequirementTreeSubNode4`,`br`.`RequirementTreeSubNode5` AS `RequirementTreeSubNode5`,`br`.`RequirementTreeSubNode6` AS `RequirementTreeSubNode6`,`br`.`RequirementTreeSubNode7` AS `RequirementTreeSubNode7`,`br`.`RequirementTreeSubNode8` AS `RequirementTreeSubNode8`,`br`.`RequirementTreeSubNode9` AS `RequirementTreeSubNode9`,`br`.`RequirementTreeSubNode10` AS `RequirementTreeSubNode10`,`br`.`RequirementTreeSubNode11` AS `RequirementTreeSubNode11`,`br`.`RequirementTreeSubNode12` AS `RequirementTreeSubNode12`,`bts`.`tcrequirementid` AS `tcrequirementid`,`bts`.`requirementIds` AS `requirementids`,`bts`.`testCaseId` AS `testcaseid`,`bts`.`testStatus` AS `teststatus` from (`v_base_req` `br` left join `v_base_test_results` `bts` on(`br`.`projectId` = `bts`.`projectId` and `br`.`releaseId` = `bts`.`releaseId` and `br`.`requirementId` = `bts`.`tcrequirementid` and `br`.`reqTestCaseId` = `bts`.`testCaseId`))

CREATE OR REPLACE VIEW `v_req_details_wise_tests_phase` AS

select `rq`.`id` AS `id`,`rq`.`epicLink` AS `epicLink`,`rq`.`projectreference` AS `ProjectReference`,`rq`.`projectname` AS `ProjectName`,`rq`.`releasename` AS `ReleaseName`,`rq`.`requirementid` AS `RequirementID`,`rq`.`actualTestCaseIds` AS `actualTestCaseIds`,`rq`.`reqTestCaseId` AS `reqTestCaseid`,`rq`.`requirementTreeNode` AS `RequirementTreeNode`,`rq`.`requirementTreeSubnode` AS `RequirementTreeSubNode`,`rq`.`requirementTreeSubnode1` AS `RequirementTreeSubNode1`,`rq`.`RequirementTreeSubNode2` AS `RequirementTreeSubNode2`,`rq`.`RequirementTreeSubNode3` AS `RequirementTreeSubNode3`,`rq`.`RequirementTreeSubNode4` AS `RequirementTreeSubNode4`,`rq`.`RequirementTreeSubNode5` AS `RequirementTreeSubNode5`,`rq`.`RequirementTreeSubNode6` AS `RequirementTreeSubNode6`,`rq`.`RequirementTreeSubNode7` AS `RequirementTreeSubNode7`,`rq`.`RequirementTreeSubNode8` AS `RequirementTreeSubNode8`,`rq`.`RequirementTreeSubNode9` AS `RequirementTreeSubNode9`,`rq`.`RequirementTreeSubNode10` AS `RequirementTreeSubNode10`,`rq`.`RequirementTreeSubNode11` AS `RequirementTreeSubNode11`,`rq`.`RequirementTreeSubNode12` AS `RequirementTreeSubNode12`,`rq`.`reqReleaseTCCountMapTCCount` AS `MappedTCCount`,ifnull(`tr`.`testStatus`,'NotCovered') AS `testStatus`,`tr`.`testcaseid` AS `testcaseid`,`tr`.`tcrequirementid` AS `testreqId`,`rq`.`releaseid` AS `reqReleaseId`,`rq`.`projectid` AS `reqProjectId`,`tr`.`releaseid` AS `releaseId`,`tr`.`projectid` AS `projectId`,`rq`.`reqTestCaseId` AS `bts_reqTestID`,`rq`.`testcaseid` AS `bts_testid`,case when (`rq`.`reqTestCaseId` is not null and `rq`.`testcaseid` is null) then 'Not Assigned' when (`rq`.`reqTestCaseId` is null and `rq`.`testcaseid` is null) then 'Not Covered' else `rs`.`testStatus` end AS `reqCoverageStatus`,case when `rq`.`reqReleaseTCCountMapTCCount` is null then 'Not Covered' else 'Covered' end AS `ReQCoverage`,`d`.`tcrequirementid` AS `defReqId`,`d`.`testcaseid` AS `defTestCaseID`,`d`.`defects` AS `defects`,ifnull(replace(`d`.`defects`,',,',','),'') AS `defectids` from (((`v_base_req_wise_test` `rq` left join `v_test_requirement_details_phase` `tr` on(`rq`.`releaseid` = `tr`.`releaseid` and `rq`.`projectid` = `tr`.`projectid` and `rq`.`requirementid` = `tr`.`tcrequirementid` and `rq`.`reqTestCaseId` = `tr`.`testcaseid`)) left join `v_test_requirement_defects_phase` `d` on(`rq`.`requirementid` = `d`.`tcrequirementid`)) left join `v_test_requirement_status_phase` `rs` on(`rq`.`releaseid` = `rs`.`releaseid` and `rq`.`projectid` = `rs`.`projectid` and `rq`.`requirementid` = `rs`.`tcrequirementid`)) 


/* Adding Epic Name in user_story_details - 11/03/2022*/

ALTER TABLE `user_story_details`
	ADD COLUMN `epicName` LONGTEXT NULL DEFAULT NULL AFTER `epicLink`;

/*Update project milesstone view - 16/03/2022*/

CREATE OR REPLACE VIEW `v_project_ccb_details` AS

select `rm`.`ReleaseName` AS `ReleaseName`,`rm`.`ReleaseDate` AS `ReleaseDate`,`pm`.`ProjectReference` AS `ProjectReference`,`pm`.`ProjectName` AS `ProjectName1`,concat(`pm`.`ProjectReference`,' - ',`pm`.`ProjectName`) AS `ProjectName`,`pm`.`Test_Status` AS `test_status`,`pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`,`pm`.`KeyRemarksTesting_Plain` AS `KeyRemarksTesting_Plain`,`pm`.`Domain` AS `Domain`,`pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`,`pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link1`,case when `pm`.`PROJECTREPOSITORY_Link` like 'file://%' then `pm`.`PROJECTREPOSITORY_Link` when `pm`.`PROJECTREPOSITORY_Link` like 'http://%' then `pm`.`PROJECTREPOSITORY_Link` when `pm`.`PROJECTREPOSITORY_Link` like 'https://%' then `pm`.`PROJECTREPOSITORY_Link` else NULL end AS `PROJECTREPOSITORY_Link`,`pm`.`JIRA_Link` AS `JIRA_Link1`,concat('https://jira.pvgroup.be/issues/?jql=project in (',`jr`.`jira_key`,') AND ((issuetype in("Story Defect") and issueFunction in subtasksOf("fixversion in (',`rm`.`ReleaseName`,')")) OR (issuetype = Defect and fixVersion in (',`rm`.`ReleaseName`,') AND created > startOfMonth(-12))) and status != Closed ORDER BY created DESC') AS `JIRA_Link`,concat('https://jira.pvgroup.be/issues/?jql=project in (',`jr`.`jira_key`,') AND ((issuetype in("Story Defect") and issueFunction in subtasksOf("fixversion in (',`rm`.`ReleaseName`,')"))) and status != Closed ORDER BY created DESC') AS `Story_JIRA_Link`,ifnull(`pm`.`ZEPHYR_Link`,'http://zephyr.pvgroup.intranet/flex/html5/login') AS `ZEPHYR_Link`,concat('https://jira.pvgroup.be/issues/?jql=project in (',`jr`.`jira_key`,') AND issuetype in ("Story") AND fixVersion in (',`rm`.`ReleaseName`,')  ORDER BY created DESC') AS `UserStory_Link1`,concat('https://jira.pvgroup.be/issues/?jql=issuetype in ("Story") AND fixVersion in (',`rm`.`ReleaseName`,')  ORDER BY created DESC') AS `UserStory_Link`,`pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`,`pm`.`Test_Coordinator` AS `Test_Coordinator`,`pm`.`Scrum_Master` AS `Scrum_Master`,ifnull(`pm`.`Domain`,'Un-Defined') AS `domainName`,ifnull(`pm`.`CCB_STARTUP_Date`,`rm`.`LastRegistration`) AS `CCB_STARTUP_Date`,ifnull(`pm`.`CCB_Ready4SPRINT_Date`,`rm`.`CCB_Ready4SPRINT`) AS `CCB_Ready4SPRINT_Date`,ifnull(`pm`.`CCB_Ready4UAT_Date`,`rm`.`CCB_Ready4UAT`) AS `CCB_Ready4UAT_Date`,ifnull(`pm`.`CCB_Ready4PROD_Date`,`rm`.`CCB_Ready4PROD`) AS `CCB_Ready4PROD_Date`,ifnull(`pm`.`CCB_Closure_Date`,`rm`.`CCB_Closure`) AS `CCB_Closure_Date`,`pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`,`pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`,convert(`pm`.`CCB_STARTUP_Remarks` using utf8) AS `CCB_STARTUP_Remarks`,convert(`pm`.`Ready2StartTestRemarks` using utf8) AS `Ready2StartTestRemarks`,`pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`,convert(`pm`.`CCBReady4SPRINT_Remarks` using utf8) AS `CCBReady4SPRINT_Remarks`,convert(`pm`.`Ready4SprintTestRemarks` using utf8) AS `Ready4SprintTestRemarks`,`pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`,convert(`pm`.`CCBReady4UAT_Remarks` using utf8) AS `CCBReady4UAT_Remarks`,convert(`pm`.`Ready4UATTestRemarks` using utf8) AS `Ready4UATTestRemarks`,`rm`.`LastRegistration` AS `LastRegistrationDate`,ifnull(`pm`.`UAT_Start_Date`,`rm`.`UAT_StartDate`) AS `UAT_START_DATE`,`rm`.`End_UAT` AS `UAT_END_DATE`,`rm`.`Freeze` AS `FREEZE_DATE`,`pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`,convert(`pm`.`CCBReady4PROD_Remarks` using utf8) AS `CCBReady4PROD_Remarks`,convert(`pm`.`Ready4ProdTestRemarks` using utf8) AS `Ready4ProdTestRemarks`,convert(`pm`.`CCB_CLOSURE_Remarks` using utf8) AS `CCB_CLOSURE_Remarks`,`pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`,`pm`.`Project_Manager` AS `Project_Manager`,`tr`.`UNIT_TEST_PATH` AS `unitTestReportPath`,`tr`.`UAT_TEST_PATH` AS `UAT_TestReportPath`,`tr`.`AUTOMATION_PATH` AS `automationReportPath`,`tr`.`PERFORMANCE_PATH` AS `performanceReportPath`,`tr`.`SECURITY_TEST_PATH` AS `securityReportPath`,`tr`.`IT_TEST_PATH` AS `IT_TestReportPath`,`tr`.`CCB_GATING_PATH` AS `CCB_GATING_PATH`,`tr`.`MASTER_TEST_PLAN_PATH` AS `MASTER_TEST_PLAN_PATH`,`tr`.`BUSSINESS_GO_PATH` AS `BUSSINESS_GO_PATH`,`pm`.`Jira_defect_projects` AS `Jira_defect_projects`,`pm`.`DetailedTestStatus` AS `DetailedTestStatus`,`pm`.`Test_Coordinator_email` AS `Test_Coordinator_email`,`pm`.`Project_Manager_email` AS `Project_Manager_email`,`pm`.`Scrum_Master_email` AS `Scrum_Master_email`,`pm`.`Delivery_Master_Lead_email` AS `Delivery_Master_Lead_email`,substring_index(`pm`.`Jira_defect_projects`,',',1) AS `JIRA_KEY`,concat('https://pvgroupbe.sharepoint.com/sites/ChangeGovernance/Lists/CCB%20Dashboard/CCBMeetingMinutes.aspx?ID=',`pm`.`SP_ID`) AS `SP_ProjectEditLink`,`pm`.`HistoryTestKeyMessage` AS `HistoryTestKeyMessage` from (((`v_release_moments` `rm` left join `project_milestones_status` `pm` on(convert(`rm`.`ReleaseName` using utf8) = replace(trim(substring_index(`pm`.`ReleaseName`,'-',1)),'+',''))) left join `v_test_reports` `tr` on(`pm`.`ReleaseName` = `tr`.`ReleaseName` and `pm`.`ProjectReference` = `tr`.`ProjectReference`)) left join `v_project_wise_defect_keys_grouping` `jr` on(`pm`.`ReleaseName` = `jr`.`ReleaseName` and `pm`.`ProjectReference` = `jr`.`ProjectReference`)) 

/*Update Data table query added - 22/03/2022*/

delete FROM uploadjobs WHERE jobName in ('components','release');

/*Creating releaseCalender Table - 30/03/2022*/

CREATE TABLE `release_calender` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`ReleaseId` INT(11) NULL DEFAULT NULL,
	`ReleaseName` VARCHAR(500) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`insertedOn` TIMESTAMP NULL DEFAULT current_timestamp(),
	`updatedOn` TIMESTAMP NULL DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE,
	UNIQUE INDEX `id` (`id`) USING BTREE,
	UNIQUE INDEX `ReleaseId` (`ReleaseId`) USING BTREE
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

/*Insert Query in uploadsjob - 30/03/2022*/
INSERT INTO `uploadjobs` (`jobName`, `jobDescription`, `jobUrl`, `ManualRuntime`, `LastRunTime`) VALUES
	('release', 'Upload Release', 'http://localhost:3322/api/CCB/uploadRelease?runType=manualRun', '2021-11-10 11:47:59', '2022-03-18 12:31:03');

/* Diary DB Clean up - 31-03-2022*/

DROP TABLE `release_milestones_status`;

DROP TABLE `components`;

DROP VIEW `v_components`;

CREATE OR REPLACE VIEW `v_all_table_counts` AS
 select 'TBL_CUSTOMERS' AS `TBN_NM`,count(0) AS `TBL_CNT` from `customers` union all select 'TBL_DEFECT_DETAILS' AS `TBN_NM`,count(0) AS `TBL_CNT` from `defect_details` union all select 'TBL_DOMAINS' AS `TBN_NM`,count(0) AS `TBL_CNT` from `domains` union all select 'TBL_PROCESS' AS `TBN_NM`,count(0) AS `TBL_CNT` from `process` union all select 'TBL_PROJECT_MILESTONES_STATUS' AS `TBN_NM`,count(0) AS `TBL_CNT` from `project_milestones_status` union all select 'TBL_REQUIREMENT_DETAILS' AS `TBN_NM`,count(0) AS `TBL_CNT` from `requirement_details` union all select 'TBL_REQUIREMENT_WISE_TEST_DETAILS' AS `TBN_NM`,count(0) AS `TBL_CNT` from `requirement_wise_test_details` union all select 'TBL_STORY_DEFECT_DETAILS' AS `TBN_NM`,count(0) AS `TBL_CNT` from `story_defect_details` union all select 'TBL_TEMPCOUNT' AS `TBN_NM`,count(0) AS `TBL_CNT` from `tempcount` union all select 'TBL_TEST_EXECUTION_RESULTS' AS `TBN_NM`,count(0) AS `TBL_CNT` from `test_execution_results` union all select 'TBL_TEST_EXECUTION_WISE_REQ_DETAILS' AS `TBN_NM`,count(0) AS `TBL_CNT` from `test_execution_wise_req_details` union all select 'TBL_TEST_REPORTS' AS `TBN_NM`,count(0) AS `TBL_CNT` from `test_reports` union all select 'TBL_UPLOADJOBS' AS `TBN_NM`,count(0) AS `TBL_CNT` from `uploadjobs` union all select 'TBL_USER_STORY_DETAILS' AS `TBN_NM`,count(0) AS `TBL_CNT` from `user_story_details` 

/*Defects Status Update - 31/05/2022*/
CREATE OR REPLACE VIEW `v_defects_data` AS

select `d`.`defectId` AS `defectId`,`d`.`defectKey` AS `defectKey`,`d`.`projectKey` AS `projectKey`,`d`.`projectName` AS `projectName`,`d`.`projectCategory` AS `projectCategory`,ifnull(`pm`.`Domain`,'Un-Defined') AS `domainName`,`pm`.`ProjectReference` AS `projectReference`,ifnull(`d`.`resolution`,'Un-Defined') AS `resolution`,ifnull(`d`.`defectSevirity`,'Un-Defined') AS `defectSevirity`,ifnull(`d`.`product`,'Un-Defined') AS `product`,case when `d`.`testLevelDetected` like '%system%' then 'System Test' when `d`.`testLevelDetected` like '%sprint%' then 'Sprint Test' when `d`.`testLevelDetected` like '%IT%' then 'Integration Test' when `d`.`testLevelDetected` like '%UAT%' then 'UAT' when `d`.`testLevelDetected` like '%unit%' then 'Unit Test' else 'Un-Defined' end AS `testLevelDetected`,
case when `d`.`testTypes` like '%func%' then 'Functional' 
when `d`.`testTypes` like '%aut%' then 'Automation' 
when `d`.`testTypes` like '%per%' then 'Performance' 
when `d`.`testTypes` like '%sec%' then 'Security Test' 
when `d`.`testTypes` like '%uni%' then 'Unit Test' else 'Un-Defined' end AS `testTypes`,
case when `d`.`status` like '%To Validate%' then 'UAT Ready' 
when `d`.`status` like '%To Verify%' then 'In Test' 
when `d`.`status` like '%To Groom%' then 'In Test' 
when `d`.`status` like '%In Test%' then 'In Test' 
when `d`.`status` like '%UAT on-going%' then 'In Test' 
when `d`.`status` like '%Ready%' then 'UAT Ready' 
when `d`.`status` like '%open%' then 'Open' 
when `d`.`status` like '%sprintable%' then 'In Progress' 
when `d`.`status` like '%progress%' then 'In Progress' 
when `d`.`status` like '%Analysis%' then 'In Progress' 
when `d`.`status` like '%Business Validated%' then 'In Progress' 
when `d`.`status` like '%review%' then 'In Review' 
when `d`.`status` like '%incep%' then 'In Review' 
when `d`.`status` like '%promot%' then 'To Promote' 
when `d`.`status` like 'open' then 'Open' 
when `d`.`status` like 'Backlog' then 'Open' 
when `d`.`status` like 'Next' then 'Open' 
when `d`.`status` like '%resol%' then 'Resolved' 
when `d`.`status` like '%done%' then 'Resolved' 
when `d`.`status` like '%reopened%' then 'Reopened' 
when `d`.`status` like '%closed%' then 'Closed' 
when `d`.`status` like '%prd ready%' then 'Closed' else `d`.`status` end AS `status`,
`d`.`createdOn` AS `createdOn`,
`d`.`summary` AS `summary`,ifnull(`d`.`createdUserName`,'UnAssigned') AS `createdUserName`,
case when `d`.`priority` like '%Major%' then 'Major' 
when `d`.`priority` like '%Highest%' then 'Major'
when `d`.`priority` like '%High%' then 'Major'
when `d`.`priority` like '%Blocker%' then 'Blocker'
when `d`.`priority` like '%Critical%' then 'Critical'
when `d`.`priority` like '%Minor%' then 'Minor'
when `d`.`priority` like '%Low%' then 'Minor'
when `d`.`priority` like '%Lowest%' then 'Minor'
when `d`.`priority` like '%Trivial%' then 'Minor'
when `d`.`priority` like '%Medium%' then 'Medium' 
when `d`.`priority` is null then 'Un-Defined' 
else `d`.`priority` end AS `priority`,
ifnull(`d`.`assigneeName`,'UnAssigned') AS `assigneeName`,ifnull(`d`.`assigneeID`,'UnAssigned') AS `assigneeID`,ifnull(`d`.`severity`,'Un-Defined') AS `severity`,ifnull(`d`.`component`,'No Components') AS `component`,ifnull(`d`.`sprintId`,'Un-Defined') AS `sprintId`,ifnull(`d`.`reporter`,'UnAssigned') AS `reporter`,ifnull(`d`.`progress`,'Un-Defined') AS `progress`,ifnull(`d`.`stereoType`,'Un-Defined') AS `stereoType`,ifnull(`d`.`epicLink`,'Un-Defined') AS `epicLink`,ifnull(`d`.`epicName`,'No Epic Mapped') AS `epicName`,ifnull(`d`.`fixVersions`,'Un-Defined') AS `fixVersions`,`pm`.`ReleaseName` AS `ReleaseName`,ifnull(`d`.`issueType`,'Un-Defined') AS `issueType`,ifnull(`d`.`parentKey`,'Un-Defined') AS `parentKey`,ifnull(`d`.`storyDefectIdFixVersion`,'Un-Defined') AS `storyDefectIdFixVersion`,replace(`pm`.`JIRA_KEY`,' ','') AS `JIRA_KEY` from (`iar_byld_1`.`defect_details` `d` join `iar_byld_1`.`v_project_wise_defect_keys` `pm` on(`d`.`projectKey` = replace(`pm`.`JIRA_KEY`,' ','') and `d`.`fixVersions` = `pm`.`ReleaseName`)) where `d`.`fixVersions` <> 'Un-Defined' 

/*Migrating Test report from old sharepoint to new sharepoint - 21-06-2022*/

ALTER TABLE `test_reports`
	ADD COLUMN `ProjectReference` VARCHAR(700) NULL DEFAULT NULL AFTER `ProjectName`;


CREATE OR REPLACE VIEW `v_test_reports` AS

select `test_reports`.`ReleaseName` AS `ReleaseName`,`test_reports`.`ProjectReference` AS `ProjectReference`,`test_reports`.`ProjectName` AS `ProjectName`,max(case when `test_reports`.`TypeOfReport` = 'Test Status Report' then `test_reports`.`Title` end) AS `IT_TEST`,max(case when `test_reports`.`TypeOfReport` = 'Test Status Report' then `test_reports`.`FileServerRelativeURL` end) AS `IT_TEST_PATH`,max(case when `test_reports`.`TypeOfReport` = 'CCB Gating Test Report' then `test_reports`.`Title` end) AS `CCB_GATING`,max(case when `test_reports`.`TypeOfReport` = 'CCB Gating Test Report' then `test_reports`.`FileServerRelativeURL` end) AS `CCB_GATING_PATH`,max(case when `test_reports`.`TypeOfReport` = 'Test Automation Report' then `test_reports`.`Title` end) AS `AUTOMATION`,max(case when `test_reports`.`TypeOfReport` = 'Test Automation Report' then `test_reports`.`FileServerRelativeURL` end) AS `AUTOMATION_PATH`,max(case when `test_reports`.`TypeOfReport` like '%Performance%' then `test_reports`.`Title` end) AS `PERFORMANCE`,max(case when `test_reports`.`TypeOfReport` like '%Performance%' then `test_reports`.`FileServerRelativeURL` end) AS `PERFORMANCE_PATH`,max(case when `test_reports`.`TypeOfReport` = 'Security Test Report' then `test_reports`.`Title` end) AS `SECURITY_TEST`,max(case when `test_reports`.`TypeOfReport` = 'Security Test Report' then `test_reports`.`FileServerRelativeURL` end) AS `SECURITY_TEST_PATH`,max(case when `test_reports`.`TypeOfReport` = 'Master Test Plan' then `test_reports`.`Title` end) AS `MASTER_TEST_PLAN`,max(case when `test_reports`.`TypeOfReport` = 'Master Test Plan' then `test_reports`.`FileServerRelativeURL` end) AS `MASTER_TEST_PLAN_PATH`,max(case when `test_reports`.`TypeOfReport` like '%Unit%' then `test_reports`.`Title` end) AS `UNIT_TEST`,max(case when `test_reports`.`TypeOfReport` like '%Unit%' then `test_reports`.`FileServerRelativeURL` end) AS `UNIT_TEST_PATH`,max(case when `test_reports`.`TypeOfReport` like '%Acceptance%' then `test_reports`.`Title` end) AS `UAT_TEST`,max(case when `test_reports`.`TypeOfReport` like '%Acceptance%' then `test_reports`.`FileServerRelativeURL` end) AS `UAT_TEST_PATH`,max(case when `test_reports`.`TypeOfReport` like '%Business%' then `test_reports`.`Title` end) AS `BUSSINESS_GO`,max(case when `test_reports`.`TypeOfReport` like '%Business%' then `test_reports`.`FileServerRelativeURL` end) AS `BUSSINESS_GO_PATH` from `test_reports` group by `test_reports`.`ReleaseName`,trim(substring_index(`test_reports`.`ProjectName`,'-',1)) order by `test_reports`.`Modified` desc 

/*Requirement to navigate Edit based on project id to sharepoint - 29/06/2022*/

CREATE OR REPLACE VIEW `v_project_ccb_details` AS

select `rm`.`ReleaseName` AS `ReleaseName`,`rm`.`ReleaseDate` AS `ReleaseDate`,`pm`.`ProjectReference` AS `ProjectReference`,`pm`.`ProjectName` AS `ProjectName1`,concat(`pm`.`ProjectReference`,' - ',`pm`.`ProjectName`) AS `ProjectName`,`pm`.`Test_Status` AS `test_status`,`pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`,`pm`.`KeyRemarksTesting_Plain` AS `KeyRemarksTesting_Plain`,`pm`.`Domain` AS `Domain`,`pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`,`pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link1`,case when `pm`.`PROJECTREPOSITORY_Link` like 'file://%' then `pm`.`PROJECTREPOSITORY_Link` when `pm`.`PROJECTREPOSITORY_Link` like 'http://%' then `pm`.`PROJECTREPOSITORY_Link` when `pm`.`PROJECTREPOSITORY_Link` like 'https://%' then `pm`.`PROJECTREPOSITORY_Link` else NULL end AS `PROJECTREPOSITORY_Link`,`pm`.`JIRA_Link` AS `JIRA_Link1`,concat('https://jira.pvgroup.be/issues/?jql=project in (',`jr`.`jira_key`,') AND ((issuetype in("Story Defect") and issueFunction in subtasksOf("fixversion in (',`rm`.`ReleaseName`,')")) OR (issuetype = Defect and fixVersion in (',`rm`.`ReleaseName`,') AND created > startOfMonth(-12))) and status != Closed ORDER BY created DESC') AS `JIRA_Link`,concat('https://jira.pvgroup.be/issues/?jql=project in (',`jr`.`jira_key`,') AND ((issuetype in("Story Defect") and issueFunction in subtasksOf("fixversion in (',`rm`.`ReleaseName`,')"))) and status != Closed ORDER BY created DESC') AS `Story_JIRA_Link`,ifnull(`pm`.`ZEPHYR_Link`,'http://zephyr.pvgroup.intranet/flex/html5/login') AS `ZEPHYR_Link`,concat('https://jira.pvgroup.be/issues/?jql=project in (',`jr`.`jira_key`,') AND issuetype in ("Story") AND fixVersion in (',`rm`.`ReleaseName`,')  ORDER BY created DESC') AS `UserStory_Link1`,concat('https://jira.pvgroup.be/issues/?jql=issuetype in ("Story") AND fixVersion in (',`rm`.`ReleaseName`,')  ORDER BY created DESC') AS `UserStory_Link`,`pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`,`pm`.`Test_Coordinator` AS `Test_Coordinator`,`pm`.`Scrum_Master` AS `Scrum_Master`,ifnull(`pm`.`Domain`,'Un-Defined') AS `domainName`,ifnull(`pm`.`CCB_STARTUP_Date`,`rm`.`LastRegistration`) AS `CCB_STARTUP_Date`,ifnull(`pm`.`CCB_Ready4SPRINT_Date`,`rm`.`CCB_Ready4SPRINT`) AS `CCB_Ready4SPRINT_Date`,ifnull(`pm`.`CCB_Ready4UAT_Date`,`rm`.`CCB_Ready4UAT`) AS `CCB_Ready4UAT_Date`,ifnull(`pm`.`CCB_Ready4PROD_Date`,`rm`.`CCB_Ready4PROD`) AS `CCB_Ready4PROD_Date`,ifnull(`pm`.`CCB_Closure_Date`,`rm`.`CCB_Closure`) AS `CCB_Closure_Date`,`pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`,`pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`,convert(`pm`.`CCB_STARTUP_Remarks` using utf8) AS `CCB_STARTUP_Remarks`,convert(`pm`.`Ready2StartTestRemarks` using utf8) AS `Ready2StartTestRemarks`,`pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`,convert(`pm`.`CCBReady4SPRINT_Remarks` using utf8) AS `CCBReady4SPRINT_Remarks`,convert(`pm`.`Ready4SprintTestRemarks` using utf8) AS `Ready4SprintTestRemarks`,`pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`,convert(`pm`.`CCBReady4UAT_Remarks` using utf8) AS `CCBReady4UAT_Remarks`,convert(`pm`.`Ready4UATTestRemarks` using utf8) AS `Ready4UATTestRemarks`,`rm`.`LastRegistration` AS `LastRegistrationDate`,ifnull(`pm`.`UAT_Start_Date`,`rm`.`UAT_StartDate`) AS `UAT_START_DATE`,`rm`.`End_UAT` AS `UAT_END_DATE`,`rm`.`Freeze` AS `FREEZE_DATE`,`pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`,convert(`pm`.`CCBReady4PROD_Remarks` using utf8) AS `CCBReady4PROD_Remarks`,convert(`pm`.`Ready4ProdTestRemarks` using utf8) AS `Ready4ProdTestRemarks`,convert(`pm`.`CCB_CLOSURE_Remarks` using utf8) AS `CCB_CLOSURE_Remarks`,`pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`,`pm`.`Project_Manager` AS `Project_Manager`,`tr`.`UNIT_TEST_PATH` AS `unitTestReportPath`,`tr`.`UAT_TEST_PATH` AS `UAT_TestReportPath`,`tr`.`AUTOMATION_PATH` AS `automationReportPath`,`tr`.`PERFORMANCE_PATH` AS `performanceReportPath`,`tr`.`SECURITY_TEST_PATH` AS `securityReportPath`,`tr`.`IT_TEST_PATH` AS `IT_TestReportPath`,`tr`.`CCB_GATING_PATH` AS `CCB_GATING_PATH`,`tr`.`MASTER_TEST_PLAN_PATH` AS `MASTER_TEST_PLAN_PATH`,`tr`.`BUSSINESS_GO_PATH` AS `BUSSINESS_GO_PATH`,`pm`.`Jira_defect_projects` AS `Jira_defect_projects`,`pm`.`DetailedTestStatus` AS `DetailedTestStatus`,`pm`.`Test_Coordinator_email` AS `Test_Coordinator_email`,`pm`.`Project_Manager_email` AS `Project_Manager_email`,`pm`.`Scrum_Master_email` AS `Scrum_Master_email`,`pm`.`Delivery_Master_Lead_email` AS `Delivery_Master_Lead_email`,substring_index(`pm`.`Jira_defect_projects`,',',1) AS `JIRA_KEY`,concat('https://pvgroupbe.sharepoint.com/sites/ChangeGovernance/Lists/CCB%20Dashboard/CCBMeetingMinutes.aspx?ID=',`pm`.`SP_ID`,'&FilterField1=as3s&FilterValue1=',`pm`.`ProjectReference`,'&FilterType1=Text&FilterField2=ReleaseMomentLookup_x003a_Releas&FilterValue2=',`rm`.`ReleaseName`,'&FilterType2=Lookup&FilterField3=LinkTitle&FilterValue3=',`pm`.`ProjectName`,'&FilterType3=Computed') AS `SP_ProjectEditLink`,`pm`.`HistoryTestKeyMessage` AS `HistoryTestKeyMessage` from (((`v_release_moments` `rm` left join `project_milestones_status` `pm` on(convert(`rm`.`ReleaseName` using utf8) = replace(trim(substring_index(`pm`.`ReleaseName`,'-',1)),'+',''))) left join `v_test_reports` `tr` on(`pm`.`ReleaseName` = `tr`.`ReleaseName` and `pm`.`ProjectReference` = `tr`.`ProjectReference`)) left join `v_project_wise_defect_keys_grouping` `jr` on(`pm`.`ReleaseName` = `jr`.`ReleaseName` and `pm`.`ProjectReference` = `jr`.`ProjectReference`)) 

/* Creating Table for comarch jira 28-06-2022*/

CREATE TABLE `comarch_defects` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`defectId` VARCHAR(45) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`defectKey` VARCHAR(45) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`component` VARCHAR(450) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`projectKey` VARCHAR(45) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`projectName` VARCHAR(450) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`mappedProjectName` VARCHAR(450) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`projectCategory` VARCHAR(450) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`fixVersions` VARCHAR(450) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`ReleaseName` VARCHAR(450) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`projectReference` VARCHAR(450) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`resolution` VARCHAR(45) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`summary` LONGTEXT NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`scrumTeamID` VARCHAR(45) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`scrumTeamName` VARCHAR(450) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`testLevelDetected` VARCHAR(450) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`createdUserName` MEDIUMTEXT NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`createdUserID` VARCHAR(45) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`createdOn` DATETIME NULL DEFAULT NULL,
	`priority` VARCHAR(45) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`labels` VARCHAR(300) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`assigneeID` VARCHAR(45) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`assigneeName` MEDIUMTEXT NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`updated` DATETIME NULL DEFAULT NULL,
	`status` VARCHAR(45) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`severity` VARCHAR(45) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`sprintState` VARCHAR(45) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`sprintName` VARCHAR(500) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`sprintStartDate` DATETIME NULL DEFAULT NULL,
	`sprintEndDate` DATETIME NULL DEFAULT NULL,
	`reporter` MEDIUMTEXT NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`progress` VARCHAR(45) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`stereoType` VARCHAR(405) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`epicLink` VARCHAR(450) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`epicName` LONGTEXT NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`module` VARCHAR(450) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`issueType` VARCHAR(450) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`parentKey` VARCHAR(45) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`storyDefectIdFixVersion` VARCHAR(450) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`updatedOn` TIMESTAMP NULL DEFAULT NULL,
	`insertedOn` TIMESTAMP NULL DEFAULT current_timestamp(),
	PRIMARY KEY (`id`) USING BTREE,
	UNIQUE INDEX `defectId_UNIQUE` (`defectId`) USING BTREE
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

/*Comarch Mapping Table - 28-06-2022*/

CREATE TABLE `comarch_mappingtable` (
	`ID` INT(11) NOT NULL AUTO_INCREMENT,
	`Comarch_ID` INT(11) NOT NULL DEFAULT '0',
	`ProjectName` VARCHAR(455) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`mappedProject` VARCHAR(455) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`mappedProjectReference` VARCHAR(455) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`ReleaseName` VARCHAR(455) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`mappedReleaseName` VARCHAR(455) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`insertedOn` TIMESTAMP NULL DEFAULT current_timestamp(),
	`updatedOn` TIMESTAMP NULL DEFAULT NULL,
	PRIMARY KEY (`Comarch_ID`) USING BTREE,
	INDEX `ID` (`ID`) USING BTREE
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

/*Creating view for comarch defects - 28-06-2022 */

CREATE OR REPLACE VIEW `v_comarchjira_defects` AS
select 
`d`.`defectId` AS `defectId`,
`d`.`defectKey` AS `defectKey`,
`d`.`projectKey` AS `projectKey`,
`d`.`projectKey` AS `JIRA_KEY`,
`d`.`projectName` AS `projectName`,
concat(`d`.`projectName`,"_Comarch") as `projectName1`,
`d`.`projectCategory` AS `projectCategory`,
('Life') AS domainName,
`c`.`mappedProject` As mappedProject,
`c`.`mappedProjectReference` As projectReference,
`c`.`mappedReleaseName` As ReleaseName,
ifnull(`d`.`resolution`,'Un-Defined') AS `resolution`,
case when `d`.`testLevelDetected` like '%system%' then 'System Test' 
when `d`.`testLevelDetected` like '%sprint%' then 'Sprint Test' 
when `d`.`testLevelDetected` like '%IT%' then 'Integration Test' 
when `d`.`testLevelDetected` like '%UAT%' then 'UAT' 
when `d`.`testLevelDetected` like '%unit%' then 'Unit Test' else 'Un-Defined' end AS `testLevelDetected`,
case when `d`.`status` like '%CLOSED%' then 'Closed' 
when `d`.`status` like '%OPEN%' then 'Open' 
when `d`.`status` like '%REOPENED%' then 'Reopened' 
when `d`.`status` like '%IN PROGRESS%' then 'In Progress' 
when `d`.`status` like '%RESOLVED%' then 'Resolved' 
when `d`.`status` like '%RELEASED%' then 'Resolved' 
when `d`.`status` like '%VERIFIED%' then 'In Test' 
 else `d`.`status` end AS `status`,

case when `d`.`issueType` like '%External%' then 'Defect' 
when `d`.`issueType` like '%Internal%' then 'Story Defect' else `d`.`issueType` end AS `issueType`,

case when `d`.`priority` like '%Blocker%' then 'Blocker' 
when `d`.`priority` like '%Critical%' then 'Critical' 
when `d`.`priority` like '%Normal%' then 'Medium' 
when `d`.`priority` like '%Minor%' then 'Minor' 
when `d`.`priority` like '%Trivial%' then 'Minor' 
when `d`.`priority` like '%Major%' then 'Major' 
else `d`.`priority` end AS `priority`,
 
`d`.`createdOn` AS `createdOn`,
`d`.`summary` AS `summary`,
ifnull(`d`.`createdUserName`,'UnAssigned') AS `createdUserName`,
ifnull(`d`.`assigneeName`,'UnAssigned') AS `assigneeName`,
ifnull(`d`.`assigneeID`,'UnAssigned') AS `assigneeID`,
ifnull(`d`.`severity`,'Un-Defined') AS `severity`,
ifnull(`d`.`component`,'No Components') AS `component`,
ifnull(`d`.`reporter`,'UnAssigned') AS `reporter`,
ifnull(`d`.`progress`,'Un-Defined') AS `progress`,
ifnull(`d`.`stereoType`,'Un-Defined') AS `stereoType`,
ifnull(`d`.`epicLink`,'Un-Defined') AS `epicLink`,
ifnull(`d`.`epicName`,'No Epic Mapped') AS `epicName`,
ifnull(`d`.`fixVersions`,'Un-Defined') AS `fixVersions`,
ifnull(`d`.`parentKey`,'Un-Defined') AS `parentKey`,
ifnull(`d`.`storyDefectIdFixVersion`,'Un-Defined') AS `storyDefectIdFixVersion` 
FROM comarch_defects as d ,comarch_mappingtable as c 
where `d`.`projectName` = `c`.`ProjectName` and `d`.`fixVersions` = `c`.`ReleaseName` and `d`.`issueType` in ("Internal Bug","External Bug") 


/*Adding Linked Defects in Test cases - Comarch Jira - 28-06-2022*/

ALTER TABLE `comarch_defects`
	ADD COLUMN `linkedIssues` LONGTEXT NULL DEFAULT NULL AFTER `labels`;

/*Adding Linked Defects in Test cases - Comarch Jira - 28-06-2022*/

CREATE OR REPLACE VIEW `v_comarchjira_tests` AS

select 
`d`.`defectId` AS `defectId`,
`d`.`defectKey` AS `defectKey`,
`d`.`projectKey` AS `projectKey`,
`d`.`projectKey` AS `JIRA_KEY`,
`d`.`projectName` AS `ProjectName`,
`d`.`linkedIssues` AS `Defects`,
concat(`d`.`projectName`,"_Comarch") as `projectName1`,
`d`.`projectCategory` AS `projectCategory`,
`c`.`mappedProject` As mappedProject,
`c`.`mappedProjectReference` As ProjectReference,
`c`.`mappedReleaseName` As ReleaseName,
('Life') AS DomainName,
(null) AS `Folder`,
(null) AS `subFolder`,
(null) AS `subFolder1`,
(null) AS `subFolder2`,
(null) AS `subFolder3`,
(null) AS `subFolder4`,
(null) AS `subFolder5`,
(null) AS `subFolder6`,
(null) AS `subFolder7`,
(null) AS `subFolder8`,
(null) AS `subFolder9`,
(null) AS `subFolder10`,
ifnull(`d`.`resolution`,'Un-Defined') AS `resolution`,
case when `d`.`testLevelDetected` like '%system%' then 'System Test' 
when `d`.`testLevelDetected` like '%sprint%' then 'Sprint Test' 
when `d`.`testLevelDetected` like '%IT%' then 'Integration Test' 
when `d`.`testLevelDetected` like '%UAT%' then 'UAT' 
when `d`.`testLevelDetected` like '%unit%' then 'Unit Test' else 'Un-Defined' end AS `testLevelDetected`,
case when `d`.`status` like '%NEW%' then 'Not Executed' 
when `d`.`status` like '%WRITING TC%' then 'Not Executed' 
when `d`.`status` like '%VALIDATING TC%' then 'Not Executed' 
when `d`.`status` like '%TC VERIFIED%' then 'Not Executed' 
when `d`.`status` like '%Testing%' then 'Incomplete' 
when `d`.`status` like '%TESTING BLOCKED%' then 'Fail' 
when `d`.`status` like '%Closed%' then 'Pass' else `d`.`status` end AS `testStatus`,

case when `d`.`issueType` like '%External%' then 'Defect' 
when `d`.`issueType` like '%Internal%' then 'Story Defect' else `d`.`issueType` end AS `issueType`,

case when `d`.`priority` like '%Blocker%' then 'Blocker' 
when `d`.`priority` like '%Critical%' then 'Critical' 
when `d`.`priority` like '%Normal%' then 'Medium' 
when `d`.`priority` like '%Minor%' then 'Minor' 
when `d`.`priority` like '%Trivial%' then 'Minor' 
when `d`.`priority` like '%Major%' then 'Major' 
else `d`.`priority` end AS `priority`,

`d`.`createdOn` AS `createdOn`,
`d`.`summary` AS `summary`,
ifnull(`d`.`createdUserName`,'UnAssigned') AS `createdUserName`,
ifnull(`d`.`assigneeName`,'UnAssigned') AS `assigneeName`,
ifnull(`d`.`assigneeID`,'UnAssigned') AS `assigneeID`,
ifnull(`d`.`severity`,'Un-Defined') AS `severity`,
ifnull(`d`.`component`,'No Components') AS `component`,
ifnull(`d`.`reporter`,'UnAssigned') AS `reporter`,
ifnull(`d`.`progress`,'Un-Defined') AS `progress`,
ifnull(`d`.`stereoType`,'Un-Defined') AS `stereoType`,
ifnull(`d`.`epicLink`,'Un-Defined') AS `epicLink`,
ifnull(`d`.`epicName`,'No Epic Mapped') AS `epicName`,
ifnull(`d`.`fixVersions`,'Un-Defined') AS `fixVersions`,
ifnull(`d`.`parentKey`,'Un-Defined') AS `parentKey`,
ifnull(`d`.`storyDefectIdFixVersion`,'Un-Defined') AS `storyDefectIdFixVersion` 
FROM comarch_defects as d ,comarch_mappingtable as c 
where `d`.`projectName` = `c`.`ProjectName` and `d`.`fixVersions` = `c`.`ReleaseName` and `d`.`issueType` = "Test Scenario" 

/*Adding comarch - Jira service in Admin Page - 28-06-2022*/

INSERT INTO `iar_byld_1`.`uploadjobs` (`jobName`, `jobDescription`, `jobUrl`) VALUES ('comarchJira', 'Upload comarch Defectcs, Story Defects and Test Results', 'http://localhost:3322/api/jira/uploadComarchDefects/upcomingRelease?runType=manualRun');
