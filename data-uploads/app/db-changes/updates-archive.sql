ALTER TABLE `iar_byld_1`.`test_execution_results` CHANGE COLUMN `updatedOn` `updatedOn` TIMESTAMP NULL DEFAULT NULL ;

ALTER TABLE `iar_byld_1`.`test_execution_results`
ADD COLUMN `subfolder3` VARCHAR
(450) NULL AFTER `subfolder2`;

ALTER TABLE `iar_byld_1`.`test_execution_results`
ADD COLUMN `subfolder4` VARCHAR
(450) NULL AFTER `subfolder3`;

ALTER TABLE `iar_byld_1`.`test_execution_results`
ADD COLUMN `subfolder5` VARCHAR
(450) NULL AFTER `subfolder4`;

ALTER TABLE `iar_byld_1`.`test_execution_results`
ADD COLUMN `subfolder6` VARCHAR
(450) NULL AFTER `subfolder5`;

ALTER TABLE `iar_byld_1`.`test_execution_results`
ADD COLUMN `subfolder7` VARCHAR
(450) NULL AFTER `subfolder6`;

ALTER TABLE `iar_byld_1`.`test_execution_results`
ADD COLUMN `subfolder8` VARCHAR
(450) NULL AFTER `subfolder7`;

ALTER TABLE `iar_byld_1`.`test_execution_results`
ADD COLUMN `subfolder9` VARCHAR
(450) NULL AFTER `subfolder8`;

ALTER TABLE `iar_byld_1`.`test_execution_results`
ADD COLUMN `subfolder10` VARCHAR
(450) NULL AFTER `subfolder9`;

ALTER TABLE `iar_byld_1`.`test_execution_results`
DROP INDEX `UNIQ` ,
ADD UNIQUE INDEX `UNIQ`
(`projectName` ASC, `releaseName` ASC, `testCaseId` ASC, `cycle` ASC, `hierarchy` ASC) ;

/******* Defect Fixes ******/

CREATE TABLE tempCount (n int not null primary key);
INSERT INTO tempCount (n)
SELECT a.N + b.N * 10 + 1 n
 FROM 
(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) a
,(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) b
ORDER BY n;

/** drop view v_project_wise_defect_keys;  */
CREATE VIEW `v_project_wise_defect_keys` AS
    SELECT 
        `t`.`ReleaseName` AS `ReleaseName`,
        `t`.`ProjectReference` AS `ProjectReference`,
        `t`.`ProjectName` AS `ProjectName`,
        `t`.`Domain` AS `Domain`,
        `t`.`Jira_defect_projects` AS `Jira_defect_projects`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(`t`.`Jira_defect_projects`, ',', `n`.`n`),
                ',',
                - 1) AS `JIRA_KEY`
    FROM
        (`project_milestones_status` `t`
        JOIN `tempcount` `n`)
    WHERE
        `n`.`n` <= 1 + (OCTET_LENGTH(`t`.`Jira_defect_projects`) - OCTET_LENGTH(REPLACE(`t`.`Jira_defect_projects`,
                    ',',
                    '')))
    ORDER BY SUBSTRING_INDEX(SUBSTRING_INDEX(`t`.`Jira_defect_projects`, ',', `n`.`n`),
            ',',
            - 1)

 
 /** Defects Data View Changes */
 /** Comment- Vew modified to handple spaces in JIRA-DEFECT Key data */
DROP VIEW `v_defects_data`;

CREATE VIEW `v_defects_data` AS
    SELECT 
        `d`.`defectId` AS `defectId`,
        `d`.`defectKey` AS `defectKey`,
        `d`.`projectKey` AS `projectKey`,
        `d`.`projectName` AS `projectName`,
        `d`.`projectCategory` AS `projectCategory`,
        IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`,
        `pm`.`ProjectReference` AS `projectReference`,
        IFNULL(`d`.`resolution`, 'Un-Defined') AS `resolution`,
        IFNULL(`d`.`defectSevirity`, 'Un-Defined') AS `defectSevirity`,
        IFNULL(`d`.`product`, 'Un-Defined') AS `product`,
        CASE
            WHEN `d`.`testLevelDetected` LIKE '%system%' THEN 'System Test'
            WHEN `d`.`testLevelDetected` LIKE '%sprint%' THEN 'Sprint Test'
            WHEN `d`.`testLevelDetected` LIKE '%IT%' THEN 'Integration Test'
            WHEN `d`.`testLevelDetected` LIKE '%UAT%' THEN 'UAT'
            WHEN `d`.`testLevelDetected` LIKE '%unit%' THEN 'Unit Test'
            ELSE 'Un-Defined'
        END AS `testLevelDetected`,
        CASE
            WHEN `d`.`testTypes` LIKE '%func%' THEN 'Functional'
            WHEN `d`.`testTypes` LIKE '%aut%' THEN 'Automation'
            WHEN `d`.`testTypes` LIKE '%per%' THEN 'Performance'
            WHEN `d`.`testTypes` LIKE '%sec%' THEN 'Security Test'
            WHEN `d`.`testTypes` LIKE '%uni%' THEN 'Unit Test'
            ELSE 'Un-Defined'
        END AS `testTypes`,
        CASE
            WHEN `d`.`status` LIKE '%To Verify%' THEN 'In Test'
            WHEN `d`.`status` LIKE '%To Groom%' THEN 'In Test'
            WHEN `d`.`status` LIKE '%In Test%' THEN 'In Test'
            WHEN `d`.`status` LIKE '%UAT on-going%' THEN 'In Test'
            WHEN `d`.`status` LIKE '%Ready%' THEN 'UAT Ready'
            WHEN `d`.`status` LIKE '%open%' THEN 'Open'
            WHEN `d`.`status` LIKE '%sprintable%' THEN 'IN SPRINTABLE'
            WHEN `d`.`status` LIKE '%progress%' THEN 'In Progress'
            WHEN `d`.`status` LIKE '%Analysis%' THEN 'In Progress'
            WHEN `d`.`status` LIKE '%Business Validated%' THEN 'In Progress'
            WHEN `d`.`status` LIKE '%review%' THEN 'In Review'
            WHEN `d`.`status` LIKE '%promot%' THEN 'To Promote'
            WHEN `d`.`status` LIKE 'open' THEN 'Open'
            WHEN `d`.`status` LIKE 'Backlog' THEN 'Open'
            WHEN `d`.`status` LIKE 'Next' THEN 'Open'
            WHEN `d`.`status` LIKE '%resol%' THEN 'Resolved'
            WHEN `d`.`status` LIKE '%done%' THEN 'Resolved'
            WHEN `d`.`status` LIKE '%reopened%' THEN 'Reopened'
            WHEN `d`.`status` LIKE '%closed%' THEN 'Closed'
        END AS `status`,
        IFNULL(`d`.`createdUserName`, 'UnAssigned') AS `createdUserName`,
        IFNULL(`d`.`priority`, 'Un-Defined') AS `priority`,
        IFNULL(`d`.`assigneeName`, 'UnAssigned') AS `assigneeName`,
        IFNULL(`d`.`severity`, 'Un-Defined') AS `severity`,
        IFNULL(`d`.`component`, 'No Components') AS `component`,
        IFNULL(`d`.`sprintId`, 'Un-Defined') AS `sprintId`,
        IFNULL(`d`.`reporter`, 'UnAssigned') AS `reporter`,
        IFNULL(`d`.`progress`, 'Un-Defined') AS `progress`,
        IFNULL(`d`.`stereoType`, 'Un-Defined') AS `stereoType`,
        IFNULL(`d`.`epicLink`, 'Un-Defined') AS `epicLink`,
        IFNULL(`d`.`fixVersions`, 'Un-Defined') AS `fixVersions`,
        `pm`.`ReleaseName` AS `ReleaseName`,
        IFNULL(`d`.`issueType`, 'Un-Defined') AS `issueType`,
        IFNULL(`d`.`parentKey`, 'Un-Defined') AS `parentKey`,
        IFNULL(`d`.`storyDefectIdFixVersion`,
                'Un-Defined') AS `storyDefectIdFixVersion`,
        REPLACE(`pm`.`JIRA_KEY`, ' ', '') AS `JIRA_KEY`
    FROM
        (`defect_details` `d`
        JOIN `v_project_wise_defect_keys` `pm` ON (`d`.`projectKey` = REPLACE(`pm`.`JIRA_KEY`, ' ', '')
            AND `d`.`fixVersions` = `pm`.`ReleaseName`))
    WHERE
        `d`.`status` <> 'Closed'
            AND `d`.`fixVersions` <> 'Un-Defined';


/** Test Result Table alteration for Duplicate Key Constaints*/

ALTER TABLE `iar_byld_1`.`test_execution_results`
ADD COLUMN `cyclePhaseId` INT(20) NULL AFTER `updatedOn`;

ALTER TABLE `iar_byld_1`.`test_execution_results`
ADD COLUMN `tcrCatalogTreeId` INT(20) NULL AFTER `cyclePhaseId`;


ALTER TABLE `iar_byld_1`.`test_execution_results` 
DROP INDEX `UNIQ` ,
ADD UNIQUE INDEX `UNIQ` (`projectName` ASC, `releaseName` ASC, `testCaseId` ASC, `cycle` ASC, `cyclePhaseId` ASC, `tcrCatalogTreeId` ASC) ;


/**  Requirement Details Table Duplicate Alterations **/

ALTER TABLE `iar_byld_1`.`requirement_details` 
DROP INDEX `UNIQ` ,
ADD UNIQUE INDEX `UNIQ` (`projectId` ASC, `releaseIds` ASC, `RequirementID` ASC, `requirementTreeIds` ASC) ;
;

/** Changing the length of ActualTestCaseId's field **/

ALTER TABLE `iar_byld_1`.`requirement_details` 
CHANGE COLUMN `actualTestcaseIds` `actualTestcaseIds` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `updatedOn` `updatedOn` TIMESTAMP NULL DEFAULT NULL ;


/** Field Length alterations 6:40 pm   1 September 2020 */

ALTER TABLE `iar_byld_1`.`requirement_details` 
CHANGE COLUMN `requirementTreePath` `requirementTreePath` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `RequirementTreeNode` `RequirementTreeNode` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `RequirementTreeSubNode` `RequirementTreeSubNode` TEXT NULL DEFAULT NULL ,
CHANGE COLUMN `RequirementTreeSubNode1` `RequirementTreeSubNode1` TEXT NULL DEFAULT NULL ,
CHANGE COLUMN `releaseNames` `releaseNames` VARCHAR(450) NULL DEFAULT NULL ,
CHANGE COLUMN `actualTestcaseIds` `actualTestcaseIds` LONGTEXT NULL DEFAULT NULL ;

/** Requirement Table Schema **/

CREATE TABLE `requirement_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projectName` varchar(450) DEFAULT NULL,
  `releaseName` varchar(200) DEFAULT NULL,
  `requirementTreePath` longtext DEFAULT NULL,
  `RequirementTreeNode` longtext DEFAULT NULL,
  `RequirementTreeSubNode` text DEFAULT NULL,
  `RequirementTreeSubNode1` text DEFAULT NULL,
  `RequirementID` varchar(100) DEFAULT NULL,
  `requirementName` varchar(450) DEFAULT NULL,
  `priority` varchar(45) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `reqCreationDate` date DEFAULT NULL,
  `createdBy` varchar(200) DEFAULT NULL,
  `lastModifiedBy` varchar(200) DEFAULT NULL,
  `lastModifiedOn` datetime DEFAULT NULL,
  `externalId` varchar(100) DEFAULT NULL,
  `requirementType` varchar(50) DEFAULT NULL,
  `ReqStatus` varchar(100) DEFAULT NULL,
  `attachmentCount` varchar(10) DEFAULT NULL,
  `releaseIds` varchar(45) DEFAULT NULL,
  `requirementTreeIds` varchar(50) DEFAULT NULL,
  `reqReleaseTCCountMapId` varchar(100) DEFAULT NULL,
  `reqReleaseTCCountMapReqId` varchar(100) DEFAULT NULL,
  `reqReleaseTCCountMapReleaseId` varchar(100) DEFAULT NULL,
  `reqReleaseTCCountMapTCCount` varchar(100) DEFAULT NULL,
  `projectId` varchar(10) DEFAULT NULL,
  `releaseNames` varchar(450) DEFAULT NULL,
  `actualTestcaseIds` longtext DEFAULT NULL,
  `insertedOn` timestamp NULL DEFAULT current_timestamp(),
  `updatedOn` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ` (`projectId`,`RequirementID`,`requirementTreeIds`,`releaseName`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;



/**** Change character set for TestCaseName - Test Execution Results */

ALTER TABLE iar_byld_1.test_execution_results MODIFY COLUMN testCaseName LONGTEXT  
    CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

/** Changing Charset to UTF8 for RequirementTable **/

ALTER TABLE  `iar_byld_1`.`requirement_details` CONVERT TO CHARACTER SET utf8;

/** Change the Requirement view to support multiple release for same project  02/09/2020 */
/** DROP VIEW v_requirement_coverage  */

CREATE VIEW `v_requirement_coverage` AS
 with `_temp_requirement_coverage` as 
	(select *,row_number() 
			OVER (PARTITION BY `temp_requirement_coverage`.`RequirementID` 
			ORDER BY field(`temp_requirement_coverage`.`testStatus`,'Fail','Pass','InComplete','Blocked','N/A','Not Executed','Not Covered') desc ) 
		AS `reqCount_num` from 
		(select trim(substring_index(`rq`.`ProjectName`,'-',1)) AS `ProjectReference`,
			trim(substr(`rq`.`ProjectName`,(locate('-',`rq`.`ProjectName`) + 1))) AS `ProjectName`,
			`rq`.`ProjectName` AS `project`,
			`rq`.`ReleaseName` AS `ReleaseName`,
			`rq`.`RequirementTreePAth` AS `RequirementTreePath`,
			`rq`.`RequirementTreeNode` AS `RequirementTreeNode`,
			`rq`.`RequirementTreeSubNode` AS `RequirementTreeSubNode`,
			`rq`.`RequirementTreeSubNode1` AS `RequirementTreeSubNode1`,
			`rq`.`RequirementID` AS `RequirementID`,`rq`.`actualTestCaseIds` AS `actualTestCaseIDs`,
			`rq`.`reqReleaseTcCountMapTCCount` AS `MappedTCCount`,
			`rq`.`actualTestIDs` AS `actualTestIds`,
			`tr`.`testCaseId` AS `testcaseid`,
			ifnull(`tr`.`testStatus`,'Not Covered') AS `testStatus`,
			ifnull(`tr`.`testStatus`,'Not Covered') AS `reqCoverageStatus`,
			`tr`.`releaseName` AS `fixVersions`,
			`tr`.`cycle` AS `cycle`,
			`tr`.`defects` AS `defectId`,
			`tr`.`executedOn` AS `executedOn`,
			(case when ((`rq`.`actualTestCaseIds` is null) or (`rq`.`actualTestCaseIds` = '')) then 'Not Covered' 
				else 'Covered' end) AS `ReQCoverage`
			from 
			((with recursive `t` as (select * from `requirement_details`), `n` as 
				(select 1 AS `n` union select (`n`.`n` + 1) AS `n + 1` from (`n` join `t`) 
					where (`n`.`n` <= (length(`t`.`actualTestcaseIds`) - length(replace(`t`.`actualTestcaseIds`,',','')))))
			select distinct substring_index(substring_index(`t`.`actualTestcaseIds`,',',`n`.`n`),',',-(1)) AS `actualTestIDs`,`t`.* 
			from (`n` join `t`)) `rq` left join 
		(with `_test_execution_results` as 
			(select *,row_number() 
				OVER (PARTITION BY `test_execution_results`.`testCaseId`
				ORDER BY `test_execution_results`.`executedOn` desc )  AS `row_num` from `test_execution_results`)
 select `_test_execution_results`.* from `_test_execution_results` where (`_test_execution_results`.`row_num` = 1))
	`tr` on((`rq`.`actualTestIDs` = `tr`.`testCaseId`)))) as `temp_requirement_coverage`) 
select * from `_temp_requirement_coverage` 
	group by
	`_temp_requirement_coverage`.`ProjectName`,
	`_temp_requirement_coverage`.`project`,
	`_temp_requirement_coverage`.`ReleaseName`,
	`_temp_requirement_coverage`.`RequirementID`,
	`_temp_requirement_coverage`.`actualTestCaseIDs`;

/** JIRA_LINK - Modified to display JQL for the project based on  Project key*/

USE `iar_byld_1`;
CREATE VIEW `v_project_ccb_details` AS
    SELECT 
        `rm`.`ReleaseName` AS `ReleaseName`,
        `rm`.`ReleaseDate` AS `ReleaseDate`,
        `pm`.`ProjectReference` AS `ProjectReference`,
        `pm`.`ProjectName` AS `ProjectName`,
        `pm`.`Test_Status` AS `test_status`,
        `pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`,
        `pm`.`Domain` AS `Domain`,
        `pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`,
        `pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link`,
        `pm`.`JIRA_Link` AS `JIRA_Link1`,
        CONCAT('https://jira.pvgroup.be/issues/?jql=project in (',
                `pm`.`Jira_defect_projects`,
                ') AND issuetype in (Defect,"Story Defect") AND status != Closed AND created > startOfMonth(-3) ORDER BY created DESC') AS `JIRA_Link`,
        `pm`.`ZEPHYR_Link` AS `ZEPHYR_Link`,
        `pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`,
        `pm`.`Test_Coordinator` AS `Test_Coordinator`,
        `pm`.`Scrum_Master` AS `Scrum_Master`,
        `pm`.`CCB_STARTUP_Date` AS `CCB_STARTUP_Date`,
        `pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`,
        CONVERT( `pm`.`CCB_STARTUP_Remarks` USING UTF8) AS `CCB_STARTUP_Remarks`,
        CONVERT( `pm`.`Ready2StartTestRemarks` USING UTF8) AS `Ready2StartTestRemarks`,
        `pm`.`CCB_Ready4SPRINT_Date` AS `CCB_Ready4SPRINT_Date`,
        `pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`,
        CONVERT( `pm`.`CCBReady4SPRINT_Remarks` USING UTF8) AS `CCBReady4SPRINT_Remarks`,
        CONVERT( `pm`.`Ready4SprintTestRemarks` USING UTF8) AS `Ready4SprintTestRemarks`,
        `pm`.`CCB_Ready4UAT_Date` AS `CCB_Ready4UAT_Date`,
        `pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`,
        CONVERT( `pm`.`CCBReady4UAT_Remarks` USING UTF8) AS `CCBReady4UAT_Remarks`,
        CONVERT( `pm`.`Ready4UATTestRemarks` USING UTF8) AS `Ready4UATTestRemarks`,
        `rm`.`LastRegistration` AS `LastRegistrationDate`,
        `rm`.`UAT_StartDate` AS `UAT_START_DATE`,
        `rm`.`End_UAT` AS `UAT_END_DATE`,
        `rm`.`Freeze` AS `FREEZE_DATE`,
        `pm`.`CCB_Ready4PROD_Date` AS `CCB_Ready4PROD_Date`,
        `pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`,
        CONVERT( `pm`.`CCBReady4PROD_Remarks` USING UTF8) AS `CCBReady4PROD_Remarks`,
        CONVERT( `pm`.`Ready4ProdTestRemarks` USING UTF8) AS `Ready4ProdTestRemarks`,
        `pm`.`CCB_Closure_Date` AS `CCB_Closure_Date`,
        `pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`,
        CONVERT( `pm`.`CCB_CLOSURE_Remarks` USING UTF8) AS `CCB_CLOSURE_Remarks`,
        `pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`,
        `pm`.`Project_Manager` AS `Project_Manager`,
        `pm`.`unitTestReportPath` AS `unitTestReportPath`,
        `pm`.`automationReportPath` AS `automationReportPath`,
        `pm`.`performanceReportPath` AS `performanceReportPath`,
        `pm`.`securityReportPath` AS `securityReportPath`,
        `pm`.`Jira_defect_projects` AS `Jira_defect_projects`
    FROM
        (`release_milestones_status` `rm`
        LEFT JOIN `project_milestones_status` `pm` ON (CONVERT( `rm`.`ReleaseName` USING UTF8) = `pm`.`ReleaseName`));



/***  Fetch Values from release milestones for date values, if entries are null in Project milestones**/

CREATE VIEW `v_project_ccb_details` AS
    SELECT 
        `rm`.`ReleaseName` AS `ReleaseName`,
        `rm`.`ReleaseDate` AS `ReleaseDate`,
        `pm`.`ProjectReference` AS `ProjectReference`,
        `pm`.`ProjectName` AS `ProjectName`,
        `pm`.`Test_Status` AS `test_status`,
        `pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`,
        `pm`.`Domain` AS `Domain`,
        `pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`,
        `pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link`,
        `pm`.`JIRA_Link` AS `JIRA_Link1`,
        CONCAT('https://jira.pvgroup.be/issues/?jql=project in (',
                `pm`.`Jira_defect_projects`,
                ') AND issuetype in (Defect,"Story Defect") AND status != Closed AND created > startOfMonth(-12) ORDER BY created DESC') AS `JIRA_Link`,
        `pm`.`ZEPHYR_Link` AS `ZEPHYR_Link`,
        `pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`,
        `pm`.`Test_Coordinator` AS `Test_Coordinator`,
        `pm`.`Scrum_Master` AS `Scrum_Master`,
        IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`,
        IFNULL(`pm`.`CCB_STARTUP_Date`,
                `rm`.`LastRegistration`) AS `CCB_STARTUP_Date`,
        IFNULL(`pm`.`CCB_Ready4SPRINT_Date`,
                `rm`.`CCB_Ready4SPRINT`) AS `CCB_Ready4SPRINT_Date`,
        IFNULL(`pm`.`CCB_Ready4UAT_Date`,
                `rm`.`CCB_Ready4UAT`) AS `CCB_Ready4UAT_Date`,
        IFNULL(`pm`.`CCB_Ready4PROD_Date`,
                `rm`.`CCB_Ready4PROD`) AS `CCB_Ready4PROD_Date`,
        IFNULL(`pm`.`CCB_Closure_Date`,
                `rm`.`CCB_Closure`) AS `CCB_Closure_Date`,
        `pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`,
        `pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`,
        CONVERT( `pm`.`CCB_STARTUP_Remarks` USING UTF8) AS `CCB_STARTUP_Remarks`,
        CONVERT( `pm`.`Ready2StartTestRemarks` USING UTF8) AS `Ready2StartTestRemarks`,
        `pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`,
        CONVERT( `pm`.`CCBReady4SPRINT_Remarks` USING UTF8) AS `CCBReady4SPRINT_Remarks`,
        CONVERT( `pm`.`Ready4SprintTestRemarks` USING UTF8) AS `Ready4SprintTestRemarks`,
        `pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`,
        CONVERT( `pm`.`CCBReady4UAT_Remarks` USING UTF8) AS `CCBReady4UAT_Remarks`,
        CONVERT( `pm`.`Ready4UATTestRemarks` USING UTF8) AS `Ready4UATTestRemarks`,
        `rm`.`LastRegistration` AS `LastRegistrationDate`,
        `rm`.`UAT_StartDate` AS `UAT_START_DATE`,
        `rm`.`End_UAT` AS `UAT_END_DATE`,
        `rm`.`Freeze` AS `FREEZE_DATE`,
        `pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`,
        CONVERT( `pm`.`CCBReady4PROD_Remarks` USING UTF8) AS `CCBReady4PROD_Remarks`,
        CONVERT( `pm`.`Ready4ProdTestRemarks` USING UTF8) AS `Ready4ProdTestRemarks`,
        CONVERT( `pm`.`CCB_CLOSURE_Remarks` USING UTF8) AS `CCB_CLOSURE_Remarks`,
        `pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`,
        `pm`.`Project_Manager` AS `Project_Manager`,
        `pm`.`unitTestReportPath` AS `unitTestReportPath`,
        `pm`.`automationReportPath` AS `automationReportPath`,
        `pm`.`performanceReportPath` AS `performanceReportPath`,
        `pm`.`securityReportPath` AS `securityReportPath`,
        `pm`.`Jira_defect_projects` AS `Jira_defect_projects`
    FROM
        (`release_milestones_status` `rm`
        LEFT JOIN `project_milestones_status` `pm` ON (CONVERT( `rm`.`ReleaseName` USING UTF8) = `pm`.`ReleaseName`))



/***  Setting the External reports redirect to "sharePoint URL"**/

CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `v_project_ccb_details` AS
    SELECT 
        `rm`.`ReleaseName` AS `ReleaseName`,
        `rm`.`ReleaseDate` AS `ReleaseDate`,
        `pm`.`ProjectReference` AS `ProjectReference`,
        `pm`.`ProjectName` AS `ProjectName`,
        `pm`.`Test_Status` AS `test_status`,
        `pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`,
        `pm`.`Domain` AS `Domain`,
        `pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`,
        `pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link`,
        `pm`.`JIRA_Link` AS `JIRA_Link1`,
        CONCAT('https://jira.pvgroup.be/issues/?jql=project in (',
                `pm`.`Jira_defect_projects`,
                ') AND issuetype in (Defect,"Story Defect") AND status != Closed AND created > startOfMonth(-12) ORDER BY created DESC') AS `JIRA_Link`,
        `pm`.`ZEPHYR_Link` AS `ZEPHYR_Link`,
        `pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`,
        `pm`.`Test_Coordinator` AS `Test_Coordinator`,
        `pm`.`Scrum_Master` AS `Scrum_Master`,
        IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`,
        IFNULL(`pm`.`CCB_STARTUP_Date`,
                `rm`.`LastRegistration`) AS `CCB_STARTUP_Date`,
        IFNULL(`pm`.`CCB_Ready4SPRINT_Date`,
                `rm`.`CCB_Ready4SPRINT`) AS `CCB_Ready4SPRINT_Date`,
        IFNULL(`pm`.`CCB_Ready4UAT_Date`,
                `rm`.`CCB_Ready4UAT`) AS `CCB_Ready4UAT_Date`,
        IFNULL(`pm`.`CCB_Ready4PROD_Date`,
                `rm`.`CCB_Ready4PROD`) AS `CCB_Ready4PROD_Date`,
        IFNULL(`pm`.`CCB_Closure_Date`,
                `rm`.`CCB_Closure`) AS `CCB_Closure_Date`,
        `pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`,
        `pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`,
        CONVERT( `pm`.`CCB_STARTUP_Remarks` USING UTF8) AS `CCB_STARTUP_Remarks`,
        CONVERT( `pm`.`Ready2StartTestRemarks` USING UTF8) AS `Ready2StartTestRemarks`,
        `pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`,
        CONVERT( `pm`.`CCBReady4SPRINT_Remarks` USING UTF8) AS `CCBReady4SPRINT_Remarks`,
        CONVERT( `pm`.`Ready4SprintTestRemarks` USING UTF8) AS `Ready4SprintTestRemarks`,
        `pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`,
        CONVERT( `pm`.`CCBReady4UAT_Remarks` USING UTF8) AS `CCBReady4UAT_Remarks`,
        CONVERT( `pm`.`Ready4UATTestRemarks` USING UTF8) AS `Ready4UATTestRemarks`,
        `rm`.`LastRegistration` AS `LastRegistrationDate`,
        `rm`.`UAT_StartDate` AS `UAT_START_DATE`,
        `rm`.`End_UAT` AS `UAT_END_DATE`,
        `rm`.`Freeze` AS `FREEZE_DATE`,
        `pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`,
        CONVERT( `pm`.`CCBReady4PROD_Remarks` USING UTF8) AS `CCBReady4PROD_Remarks`,
        CONVERT( `pm`.`Ready4ProdTestRemarks` USING UTF8) AS `Ready4ProdTestRemarks`,
        CONVERT( `pm`.`CCB_CLOSURE_Remarks` USING UTF8) AS `CCB_CLOSURE_Remarks`,
        `pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`,
        `pm`.`Project_Manager` AS `Project_Manager`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `unitTestReportPath`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `automationReportPath`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `performanceReportPath`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `securityReportPath`,
        `pm`.`Jira_defect_projects` AS `Jira_defect_projects`
    FROM
        (`release_milestones_status` `rm`
        LEFT JOIN `project_milestones_status` `pm` ON (CONVERT( `rm`.`ReleaseName` USING UTF8) = `pm`.`ReleaseName`))


/** Setting the default URL values for IT & UAT Test Report Path"*/
CREATE VIEW `v_project_ccb_details` AS
    SELECT 
        `rm`.`ReleaseName` AS `ReleaseName`,
        `rm`.`ReleaseDate` AS `ReleaseDate`,
        `pm`.`ProjectReference` AS `ProjectReference`,
        `pm`.`ProjectName` AS `ProjectName`,
        `pm`.`Test_Status` AS `test_status`,
        `pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`,
        `pm`.`Domain` AS `Domain`,
        `pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`,
        `pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link`,
        `pm`.`JIRA_Link` AS `JIRA_Link1`,
        CONCAT('https://jira.pvgroup.be/issues/?jql=project in (',
                `pm`.`Jira_defect_projects`,
                ') AND issuetype in (Defect,"Story Defect") AND status != Closed AND created > startOfMonth(-12) ORDER BY created DESC') AS `JIRA_Link`,
        `pm`.`ZEPHYR_Link` AS `ZEPHYR_Link`,
        `pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`,
        `pm`.`Test_Coordinator` AS `Test_Coordinator`,
        `pm`.`Scrum_Master` AS `Scrum_Master`,
        IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`,
        IFNULL(`pm`.`CCB_STARTUP_Date`,
                `rm`.`LastRegistration`) AS `CCB_STARTUP_Date`,
        IFNULL(`pm`.`CCB_Ready4SPRINT_Date`,
                `rm`.`CCB_Ready4SPRINT`) AS `CCB_Ready4SPRINT_Date`,
        IFNULL(`pm`.`CCB_Ready4UAT_Date`,
                `rm`.`CCB_Ready4UAT`) AS `CCB_Ready4UAT_Date`,
        IFNULL(`pm`.`CCB_Ready4PROD_Date`,
                `rm`.`CCB_Ready4PROD`) AS `CCB_Ready4PROD_Date`,
        IFNULL(`pm`.`CCB_Closure_Date`,
                `rm`.`CCB_Closure`) AS `CCB_Closure_Date`,
        `pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`,
        `pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`,
        CONVERT( `pm`.`CCB_STARTUP_Remarks` USING UTF8) AS `CCB_STARTUP_Remarks`,
        CONVERT( `pm`.`Ready2StartTestRemarks` USING UTF8) AS `Ready2StartTestRemarks`,
        `pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`,
        CONVERT( `pm`.`CCBReady4SPRINT_Remarks` USING UTF8) AS `CCBReady4SPRINT_Remarks`,
        CONVERT( `pm`.`Ready4SprintTestRemarks` USING UTF8) AS `Ready4SprintTestRemarks`,
        `pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`,
        CONVERT( `pm`.`CCBReady4UAT_Remarks` USING UTF8) AS `CCBReady4UAT_Remarks`,
        CONVERT( `pm`.`Ready4UATTestRemarks` USING UTF8) AS `Ready4UATTestRemarks`,
        `rm`.`LastRegistration` AS `LastRegistrationDate`,
        `rm`.`UAT_StartDate` AS `UAT_START_DATE`,
        `rm`.`End_UAT` AS `UAT_END_DATE`,
        `rm`.`Freeze` AS `FREEZE_DATE`,
        `pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`,
        CONVERT( `pm`.`CCBReady4PROD_Remarks` USING UTF8) AS `CCBReady4PROD_Remarks`,
        CONVERT( `pm`.`Ready4ProdTestRemarks` USING UTF8) AS `Ready4ProdTestRemarks`,
        CONVERT( `pm`.`CCB_CLOSURE_Remarks` USING UTF8) AS `CCB_CLOSURE_Remarks`,
        `pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`,
        `pm`.`Project_Manager` AS `Project_Manager`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `unitTestReportPath`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `automationReportPath`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `performanceReportPath`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `securityReportPath`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `IT_TestReportPath`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `UAT_TestReportPath`,
        `pm`.`Jira_defect_projects` AS `Jira_defect_projects`
    FROM
        (`release_milestones_status` `rm`
        LEFT JOIN `project_milestones_status` `pm` ON (CONVERT( `rm`.`ReleaseName` USING UTF8) = `pm`.`ReleaseName`))

/*** ER_TRUNCATED_WRONG_VALUE_FOR_FIELD:
Incorrect string  value: '\xE2\x80\x8B>AL...' for column `iar_prod_1`.`test_execution_results`.`hierarchy`  */

ALTER TABLE iar_byld_1.test_execution_results CONVERT TO CHARACTER SET utf8;

/** Ater Table to add additional ccb project fields */

ALTER TABLE `iar_byld_1`.`project_milestones_status` 
        CHANGE COLUMN `updatedOn` `updatedOn` TIMESTAMP NULL DEFAULT NULL ;

ALTER TABLE `iar_byld_1`.`project_milestones_status`  
     ADD COLUMN `Scrum_MasterId` VARCHAR(45) NULL AFTER `Project_Name_Dup`;

ALTER TABLE `iar_byld_1`.`project_milestones_status`  
     ADD COLUMN `DEL_STA_HighLevelPlanning` VARCHAR(45) NULL AFTER `Project_Name_Dup`;     

ALTER TABLE `iar_byld_1`.`project_milestones_status`  
     ADD COLUMN `DEL_STA_WarrantyIncluded` VARCHAR(45) NULL AFTER `Project_Name_Dup`;         
     
ALTER TABLE `iar_byld_1`.`project_milestones_status`  
     ADD COLUMN `DEL_STA_HighLevelRequirementsScope` VARCHAR(45) NULL AFTER `Project_Name_Dup`;       
   
ALTER TABLE `iar_byld_1`.`project_milestones_status`  
     ADD COLUMN `DEL_STAStaffingPlanviewBooked` VARCHAR(45) NULL AFTER `Project_Name_Dup`;       
  
ALTER TABLE `iar_byld_1`.`project_milestones_status`  
     ADD COLUMN `DEL_STA_BudgetApproved` VARCHAR(45) NULL AFTER `Project_Name_Dup`;    
     
ALTER TABLE `iar_byld_1`.`project_milestones_status`  
     ADD COLUMN `DEL_STA_PIDStage1_TODO` VARCHAR(45) NULL AFTER `Project_Name_Dup`;    
     
ALTER TABLE `iar_byld_1`.`project_milestones_status`  
     ADD COLUMN `DEL_STA_PIDStage1` VARCHAR(45) NULL AFTER `Project_Name_Dup`;

ALTER TABLE `iar_byld_1`.`project_milestones_status`  
     ADD COLUMN `DEL_SPR_SolutionOutlineArchitecture` VARCHAR(45) NULL AFTER `Project_Name_Dup`;   

ALTER TABLE `iar_byld_1`.`project_milestones_status`  
     ADD COLUMN `DEL_SPR_ComponentFile` VARCHAR(45) NULL AFTER `Project_Name_Dup`;      

ALTER TABLE `iar_byld_1`.`project_milestones_status`  
     ADD COLUMN `DEL_SPR_PlanviewStaffingincUAT` VARCHAR(45) NULL AFTER `Project_Name_Dup`;       

ALTER TABLE `iar_byld_1`.`project_milestones_status`  
     ADD COLUMN `DEL_SPR_DetailledPlanning` VARCHAR(45) NULL AFTER `Project_Name_Dup`;       

ALTER TABLE `iar_byld_1`.`project_milestones_status`  
     ADD COLUMN `CCBDashb` VARCHAR(45) NULL AFTER `Project_Name_Dup`;   

ALTER TABLE `iar_byld_1`.`project_milestones_status`  
     ADD COLUMN `DomainExpert` VARCHAR(45) NULL AFTER `Project_Name_Dup`; 

ALTER TABLE `iar_byld_1`.`project_milestones_status`  
     ADD COLUMN `Product_OwnerID` VARCHAR(45) NULL AFTER `Project_Name_Dup`;     
   

ALTER TABLE `iar_byld_1`.`project_milestones_status`    
   ADD COLUMN `MTP` VARCHAR(45) NULL AFTER `Project_Name_Dup`,
   ADD COLUMN `Delivery_Master_LeadId` VARCHAR(45) NULL AFTER `Project_Name_Dup`,
   ADD COLUMN `Test_CoordinatorId` VARCHAR(45) NULL AFTER `Project_Name_Dup`,
   ADD COLUMN `DetailedTestStatus` VARCHAR(45) NULL AFTER `Project_Name_Dup`,
   ADD COLUMN `SP_ID` VARCHAR(45) NULL AFTER `Project_Name_Dup`,
   ADD COLUMN `AuthorId` VARCHAR(45) NULL AFTER `Project_Name_Dup`,
   ADD COLUMN `EditorId` VARCHAR(45) NULL AFTER `Project_Name_Dup`,
   ADD COLUMN `OdateUIVersionString` VARCHAR(45) NULL AFTER `Project_Name_Dup`,
   ADD COLUMN `GUID` VARCHAR(45) NULL AFTER `Project_Name_Dup`;


ALTER TABLE `iar_byld_1`.`project_milestones_status`  
     ADD COLUMN `Project_ManagerId` VARCHAR(45) NULL AFTER `Project_Name_Dup`;


/** Changing the column Type for DetaieldTestStatus */
ALTER TABLE `iar_byld_1`.`project_milestones_status` 
CHANGE COLUMN `DetailedTestStatus` `DetailedTestStatus` LONGTEXT NULL DEFAULT NULL ;

/** #163  - Epic3 - DetailedTestStatus in projects View **/

CREATE VIEW `v_project_ccb_details` AS
    SELECT 
        `rm`.`ReleaseName` AS `ReleaseName`,
        `rm`.`ReleaseDate` AS `ReleaseDate`,
        `pm`.`ProjectReference` AS `ProjectReference`,
        `pm`.`ProjectName` AS `ProjectName`,
        `pm`.`Test_Status` AS `test_status`,
        `pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`,
        `pm`.`Domain` AS `Domain`,
        `pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`,
        `pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link`,
        `pm`.`JIRA_Link` AS `JIRA_Link1`,
        CONCAT('https://jira.pvgroup.be/issues/?jql=project in (',
                `pm`.`Jira_defect_projects`,
                ') AND issuetype in (Defect,"Story Defect") AND status != Closed AND created > startOfMonth(-12) ORDER BY created DESC') AS `JIRA_Link`,
        `pm`.`ZEPHYR_Link` AS `ZEPHYR_Link`,
        `pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`,
        `pm`.`Test_Coordinator` AS `Test_Coordinator`,
        `pm`.`Scrum_Master` AS `Scrum_Master`,
        IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`,
        IFNULL(`pm`.`CCB_STARTUP_Date`,
                `rm`.`LastRegistration`) AS `CCB_STARTUP_Date`,
        IFNULL(`pm`.`CCB_Ready4SPRINT_Date`,
                `rm`.`CCB_Ready4SPRINT`) AS `CCB_Ready4SPRINT_Date`,
        IFNULL(`pm`.`CCB_Ready4UAT_Date`,
                `rm`.`CCB_Ready4UAT`) AS `CCB_Ready4UAT_Date`,
        IFNULL(`pm`.`CCB_Ready4PROD_Date`,
                `rm`.`CCB_Ready4PROD`) AS `CCB_Ready4PROD_Date`,
        IFNULL(`pm`.`CCB_Closure_Date`,
                `rm`.`CCB_Closure`) AS `CCB_Closure_Date`,
        `pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`,
        `pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`,
        CONVERT( `pm`.`CCB_STARTUP_Remarks` USING UTF8) AS `CCB_STARTUP_Remarks`,
        CONVERT( `pm`.`Ready2StartTestRemarks` USING UTF8) AS `Ready2StartTestRemarks`,
        `pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`,
        CONVERT( `pm`.`CCBReady4SPRINT_Remarks` USING UTF8) AS `CCBReady4SPRINT_Remarks`,
        CONVERT( `pm`.`Ready4SprintTestRemarks` USING UTF8) AS `Ready4SprintTestRemarks`,
        `pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`,
        CONVERT( `pm`.`CCBReady4UAT_Remarks` USING UTF8) AS `CCBReady4UAT_Remarks`,
        CONVERT( `pm`.`Ready4UATTestRemarks` USING UTF8) AS `Ready4UATTestRemarks`,
        `rm`.`LastRegistration` AS `LastRegistrationDate`,
        `rm`.`UAT_StartDate` AS `UAT_START_DATE`,
        `rm`.`End_UAT` AS `UAT_END_DATE`,
        `rm`.`Freeze` AS `FREEZE_DATE`,
        `pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`,
        CONVERT( `pm`.`CCBReady4PROD_Remarks` USING UTF8) AS `CCBReady4PROD_Remarks`,
        CONVERT( `pm`.`Ready4ProdTestRemarks` USING UTF8) AS `Ready4ProdTestRemarks`,
        CONVERT( `pm`.`CCB_CLOSURE_Remarks` USING UTF8) AS `CCB_CLOSURE_Remarks`,
        `pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`,
        `pm`.`Project_Manager` AS `Project_Manager`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `unitTestReportPath`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `automationReportPath`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `performanceReportPath`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `securityReportPath`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `IT_TestReportPath`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `UAT_TestReportPath`,
        `pm`.`Jira_defect_projects` AS `Jira_defect_projects`,
        `pm`.`DetailedTestStatus` AS `DetailedTestStatus`
    FROM
        (`release_milestones_status` `rm`
        LEFT JOIN `project_milestones_status` `pm` ON (CONVERT( `rm`.`ReleaseName` USING UTF8) = `pm`.`ReleaseName`))


/** #Phase 2  https://gitlab.com/expleo-p-v/diary/-/issues/165**/
/** Create New Table - Test Reports  **/

CREATE TABLE `test_reports` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ItemID` int(11) NOT NULL,
  `Title` varchar(450) DEFAULT NULL,
  `FileName` longtext DEFAULT NULL,
  `TypeOfReport` varchar(450) DEFAULT NULL,
  `ReleaseName` varchar(450) DEFAULT NULL,
  `ProjectName` varchar(700) DEFAULT NULL,
  `FileServerRelativeURL` longtext DEFAULT NULL,
  `Modified` datetime DEFAULT NULL,
  `ModifiedBy` varchar(450) DEFAULT NULL,
  `FileURI` longtext DEFAULT NULL,
  `insertedOn` timestamp NULL DEFAULT current_timestamp(),
  `updatedOn` timestamp NULL DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `GUID` varchar(450) DEFAULT NULL,
  PRIMARY KEY (`ID`,`ItemID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


/** #Phase 2  https://gitlab.com/expleo-p-v/diary/-/issues/165**/

CREATE OR REPLACE VIEW `v_project_ccb_details` AS select `rm`.`ReleaseName` AS `ReleaseName`,`rm`.`ReleaseDate` AS `ReleaseDate`,`pm`.`ProjectReference` AS `ProjectReference`,`pm`.`ProjectName` AS `ProjectName`,`pm`.`Test_Status` AS `test_status`,`pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`,`pm`.`Domain` AS `Domain`,`pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`,`pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link`,`pm`.`JIRA_Link` AS `JIRA_Link1`,concat('https://jira.pvgroup.be/issues/?jql=project in (',`pm`.`Jira_defect_projects`,') AND issuetype in (Defect,"Story Defect") AND status != Closed AND created > startOfMonth(-12) ORDER BY created DESC') AS `JIRA_Link`,`pm`.`ZEPHYR_Link` AS `ZEPHYR_Link`,`pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`,`pm`.`Test_Coordinator` AS `Test_Coordinator`,`pm`.`Scrum_Master` AS `Scrum_Master`,ifnull(`pm`.`Domain`,'Un-Defined') AS `domainName`,ifnull(`pm`.`CCB_STARTUP_Date`,`rm`.`LastRegistration`) AS `CCB_STARTUP_Date`,ifnull(`pm`.`CCB_Ready4SPRINT_Date`,`rm`.`CCB_Ready4SPRINT`) AS `CCB_Ready4SPRINT_Date`,ifnull(`pm`.`CCB_Ready4UAT_Date`,`rm`.`CCB_Ready4UAT`) AS `CCB_Ready4UAT_Date`,ifnull(`pm`.`CCB_Ready4PROD_Date`,`rm`.`CCB_Ready4PROD`) AS `CCB_Ready4PROD_Date`,ifnull(`pm`.`CCB_Closure_Date`,`rm`.`CCB_Closure`) AS `CCB_Closure_Date`,`pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`,`pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`,convert(`pm`.`CCB_STARTUP_Remarks` using utf8) AS `CCB_STARTUP_Remarks`,convert(`pm`.`Ready2StartTestRemarks` using utf8) AS `Ready2StartTestRemarks`,`pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`,convert(`pm`.`CCBReady4SPRINT_Remarks` using utf8) AS `CCBReady4SPRINT_Remarks`,convert(`pm`.`Ready4SprintTestRemarks` using utf8) AS `Ready4SprintTestRemarks`,`pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`,convert(`pm`.`CCBReady4UAT_Remarks` using utf8) AS `CCBReady4UAT_Remarks`,convert(`pm`.`Ready4UATTestRemarks` using utf8) AS `Ready4UATTestRemarks`,`rm`.`LastRegistration` AS `LastRegistrationDate`,`rm`.`UAT_StartDate` AS `UAT_START_DATE`,`rm`.`End_UAT` AS `UAT_END_DATE`,`rm`.`Freeze` AS `FREEZE_DATE`,`pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`,convert(`pm`.`CCBReady4PROD_Remarks` using utf8) AS `CCBReady4PROD_Remarks`,convert(`pm`.`Ready4ProdTestRemarks` using utf8) AS `Ready4ProdTestRemarks`,convert(`pm`.`CCB_CLOSURE_Remarks` using utf8) AS `CCB_CLOSURE_Remarks`,`pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`,`pm`.`Project_Manager` AS `Project_Manager`,'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `unitTestReportPath`,'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `UAT_TestReportPath`,`tr`.`AUTOMATION_PATH` AS `automationReportPath`,`tr`.`PERFORMANCE_PATH` AS `performanceReportPath`,`tr`.`SECURITY_TEST_PATH` AS `securityReportPath`,`tr`.`IT_TEST_PATH` AS `IT_TestReportPath`,`tr`.`CCB_GATING_PATH` AS `CCB_GATING_PATH`,`tr`.`MASTER_TEST_PLAN_PATH` AS `MASTER_TEST_PLAN_PATH`,`pm`.`Jira_defect_projects` AS `Jira_defect_projects`,`pm`.`DetailedTestStatus` AS `DetailedTestStatus` from ((`release_milestones_status` `rm` left join `project_milestones_status` `pm` on(convert(`rm`.`ReleaseName` using utf8) = `pm`.`ReleaseName`)) left join `v_test_reports` `tr` on(`pm`.`ReleaseName` = `tr`.`ReleaseName` and `pm`.`ProjectName` = `tr`.`ProjectName`));

CREATE OR REPLACE VIEW `v_test_reports` AS select `test_reports`.`ReleaseName` AS `ReleaseName`,`test_reports`.`ProjectName` AS `PROJECT`,trim(substring_index(`test_reports`.`ProjectName`,'-',1)) AS `ProjectReference`,trim(substr(`test_reports`.`ProjectName`,locate('-',`test_reports`.`ProjectName`) + 1)) AS `ProjectName`,max(case when `test_reports`.`TypeOfReport` = 'Test Status Report' then `test_reports`.`Title` end) AS `IT_TEST`,max(case when `test_reports`.`TypeOfReport` = 'Test Status Report' then `test_reports`.`FileServerRelativeURL` end) AS `IT_TEST_PATH`,max(case when `test_reports`.`TypeOfReport` = 'CCB Gating Test Report' then `test_reports`.`Title` end) AS `CCB_GATING`,max(case when `test_reports`.`TypeOfReport` = 'CCB Gating Test Report' then `test_reports`.`FileServerRelativeURL` end) AS `CCB_GATING_PATH`,max(case when `test_reports`.`TypeOfReport` = 'Test Automation Report' then `test_reports`.`Title` end) AS `AUTOMATION`,max(case when `test_reports`.`TypeOfReport` = 'Test Automation Report' then `test_reports`.`FileServerRelativeURL` end) AS `AUTOMATION_PATH`,max(case when `test_reports`.`TypeOfReport` = 'Performance Test Report' then `test_reports`.`Title` end) AS `PERFORMANCE`,max(case when `test_reports`.`TypeOfReport` = 'Performance Test Report' then `test_reports`.`FileServerRelativeURL` end) AS `PERFORMANCE_PATH`,max(case when `test_reports`.`TypeOfReport` = 'Security Test Report' then `test_reports`.`Title` end) AS `SECURITY_TEST`,max(case when `test_reports`.`TypeOfReport` = 'Security Test Report' then `test_reports`.`FileServerRelativeURL` end) AS `SECURITY_TEST_PATH`,max(case when `test_reports`.`TypeOfReport` = 'Master Test Plan' then `test_reports`.`Title` end) AS `MASTER_TEST_PLAN`,max(case when `test_reports`.`TypeOfReport` = 'Master Test Plan' then `test_reports`.`FileServerRelativeURL` end) AS `MASTER_TEST_PLAN_PATH` from `test_reports` group by `test_reports`.`ReleaseName`,`test_reports`.`ProjectName` order by `test_reports`.`Modified` desc;


/** 166 - Renaming story_details to Defect_story_details **/

ALTER TABLE `iar_byld_1`.`story_details` 
RENAME TO  `iar_byld_1`.`story_defect_details` ;

/** Create table for User Story **/





CREATE TABLE `user_story_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userStoryID` varchar(45) DEFAULT NULL,
  `storyKey` varchar(45) DEFAULT NULL,
  `component` varchar(450) DEFAULT NULL,
  `projectKey` varchar(45) DEFAULT NULL,
  `projectName` varchar(450) DEFAULT NULL,
  `fixVersions` varchar(450) DEFAULT NULL,
  `resolution` varchar(45) DEFAULT NULL,
  `product` varchar(450) DEFAULT NULL,
  `summary` longtext DEFAULT NULL,
  `scrumTeamID` varchar(45) DEFAULT NULL,
  `scrumTeamName` varchar(450) DEFAULT NULL,
  `resolutiondate` datetime DEFAULT NULL,
  `createdUserName` varchar(100) DEFAULT NULL,
  `createdUserID` varchar(45) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `priority` varchar(45) DEFAULT NULL,
  `labels` varchar(300) DEFAULT NULL,
  `assigneeID` varchar(45) DEFAULT NULL,
  `assigneeName` varchar(45) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `severity` varchar(45) DEFAULT NULL,
  `sprintId` varchar(45) DEFAULT NULL,
  `sprintState` varchar(45) DEFAULT NULL,
  `sprintName` varchar(500) DEFAULT NULL,
  `sprintStartDate` datetime DEFAULT NULL,
  `sprintEndDate` datetime DEFAULT NULL,
  `reporter` varchar(450) DEFAULT NULL,
  `epicLink` varchar(450) DEFAULT NULL,
  `module` varchar(450) DEFAULT NULL,
  `issueType` varchar(450) DEFAULT NULL,
  `insertedOn` timestamp NULL DEFAULT current_timestamp(),
  `updatedOn` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userStoryId_UNIQUE` (`userStoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


/** View for User Story  **/

CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `v_user_story` AS
    SELECT 
        `d`.`userStoryID` AS `userStoryID`,
        `d`.`projectKey` AS `projectKey`,
        `d`.`projectName` AS `projectName`,
        IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`,
        `pm`.`ProjectReference` AS `ProjectReference`,
          `d`.`status` AS `storyStatus`,
        IFNULL(`d`.`resolution`, 'Un-Defined') AS `resolution`,
        IFNULL(`d`.`priority`, 'Un-Defined') AS `priority`,
        IFNULL(`d`.`assigneeName`, 'UnAssigned') AS `assigneeName`,
        IFNULL(`d`.`severity`, 'Un-Defined') AS `severity`,
        IFNULL(`d`.`component`, 'No Components') AS `component`,
        IFNULL(`d`.`sprintId`, 'Un-Defined') AS `sprintId`,
        IFNULL(`d`.`reporter`, 'UnAssigned') AS `reporter`,
        IFNULL(`d`.`epicLink`, 'Un-Defined') AS `epicLink`,
        IFNULL(`d`.`fixVersions`, 'Un-Defined') AS `fixVersions`,
        `pm`.`ReleaseName` AS `ReleaseName`,
        IFNULL(`d`.`issueType`, 'Un-Defined') AS `issueType`,
        REPLACE(`pm`.`JIRA_KEY`, ' ', '') AS `JIRA_KEY`
    FROM
        (`user_story_details` `d`
        JOIN `v_project_wise_defect_keys` `pm` ON (`d`.`projectKey` = REPLACE(`pm`.`JIRA_KEY`, ' ', '')
            AND `d`.`fixVersions` = `pm`.`ReleaseName`));


/*** Components Table Changes **/

ALTER TABLE `iar_byld_1`.`components` 
ADD COLUMN `projectId` INT(20) NULL AFTER `projectKey`,
CHANGE COLUMN `ComponentName` `ComponentName` VARCHAR(450) NULL DEFAULT NULL ,
CHANGE COLUMN `Description` `projectKey` VARCHAR(45) NULL DEFAULT NULL ,
ADD COLUMN `insertedOn` timestamp NULL DEFAULT current_timestamp(),
ADD COLUMN `updatedOn` timestamp NULL DEFAULT NULL,
CHANGE COLUMN `ComponentId` `ComponentId` INT(15) NULL ,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`Id`, `ComponentId`),
ADD UNIQUE INDEX `ComponentId_UNIQUE` (`ComponentId` ASC) ;;



/** View for Components  **/

    CREATE    OR REPLACE  VIEW `v_components` AS
        SELECT 
            `c`.`Id` AS `Id`,
            `c`.`ComponentId` AS `ComponentId`,
            `c`.`ComponentName` AS `ComponentName`,
            `c`.`projectKey` AS `projectKey`,
            `c`.`projectId` AS `projectId`,
            `c`.`insertedOn` AS `insertedOn`,
            `c`.`updatedOn` AS `updatedOn`, 
            `pm`.`ProjectReference` AS `ProjectReference`,
            REPLACE(`pm`.`JIRA_KEY`, ' ', '') AS `JIRA_KEY`
        FROM
            (`components` `c`
            JOIN `v_project_wise_defect_keys` `pm` ON (`c`.`projectKey` = REPLACE(`pm`.`JIRA_KEY`, ' ', '')));


/* FOR METEO - Adding Preconditions field for test resuls */

ALTER TABLE `iar_byld_1`.`test_execution_results` 
ADD COLUMN `preConditions` LONGTEXT NULL AFTER `tcrCatalogTreeId`;


/** NULL values for defects **/

CREATE 
   OR REPLACE VIEW `v_defects_data` AS
    SELECT 
        `d`.`defectId` AS `defectId`,
        `d`.`defectKey` AS `defectKey`,
        `d`.`projectKey` AS `projectKey`,
        `d`.`projectName` AS `projectName`,
        `d`.`projectCategory` AS `projectCategory`,
        IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`,
        `pm`.`ProjectReference` AS `projectReference`,
        IFNULL(`d`.`resolution`, 'Un-Defined') AS `resolution`,
        IFNULL(`d`.`defectSevirity`, 'Un-Defined') AS `defectSevirity`,
        IFNULL(`d`.`product`, 'Un-Defined') AS `product`,
        CASE
            WHEN `d`.`testLevelDetected` LIKE '%system%' THEN 'System Test'
            WHEN `d`.`testLevelDetected` LIKE '%sprint%' THEN 'Sprint Test'
            WHEN `d`.`testLevelDetected` LIKE '%IT%' THEN 'Integration Test'
            WHEN `d`.`testLevelDetected` LIKE '%UAT%' THEN 'UAT'
            WHEN `d`.`testLevelDetected` LIKE '%unit%' THEN 'Unit Test'
            ELSE 'Un-Defined'
        END AS `testLevelDetected`,
        CASE
            WHEN `d`.`testTypes` LIKE '%func%' THEN 'Functional'
            WHEN `d`.`testTypes` LIKE '%aut%' THEN 'Automation'
            WHEN `d`.`testTypes` LIKE '%per%' THEN 'Performance'
            WHEN `d`.`testTypes` LIKE '%sec%' THEN 'Security Test'
            WHEN `d`.`testTypes` LIKE '%uni%' THEN 'Unit Test'
            ELSE 'Un-Defined'
        END AS `testTypes`,
        CASE
            WHEN `d`.`status` LIKE '%To Validate%' THEN 'UAT Ready'
            WHEN `d`.`status` LIKE '%To Verify%' THEN 'In Test'
            WHEN `d`.`status` LIKE '%To Groom%' THEN 'In Test'
            WHEN `d`.`status` LIKE '%In Test%' THEN 'In Test'
            WHEN `d`.`status` LIKE '%UAT on-going%' THEN 'In Test'
            WHEN `d`.`status` LIKE '%Ready%' THEN 'UAT Ready'
            WHEN `d`.`status` LIKE '%open%' THEN 'Open'
            WHEN `d`.`status` LIKE '%sprintable%' THEN 'In Progress'
            WHEN `d`.`status` LIKE '%progress%' THEN 'In Progress'
            WHEN `d`.`status` LIKE '%Analysis%' THEN 'In Progress'
            WHEN `d`.`status` LIKE '%Business Validated%' THEN 'In Progress'
            WHEN `d`.`status` LIKE '%review%' THEN 'In Review'
            WHEN `d`.`status` LIKE '%incep%' THEN 'In Review'
            WHEN `d`.`status` LIKE '%promot%' THEN 'To Promote'
            WHEN `d`.`status` LIKE 'open' THEN 'Open'
            WHEN `d`.`status` LIKE 'Backlog' THEN 'Open'
            WHEN `d`.`status` LIKE 'Next' THEN 'Open'
            WHEN `d`.`status` LIKE '%resol%' THEN 'Resolved'
            WHEN `d`.`status` LIKE '%done%' THEN 'Resolved'
            WHEN `d`.`status` LIKE '%reopened%' THEN 'Reopened'
            WHEN `d`.`status` LIKE '%closed%' THEN 'Closed'
            ELSE `d`.`status`
        END AS `status`,
        IFNULL(`d`.`createdUserName`, 'UnAssigned') AS `createdUserName`,
        IFNULL(`d`.`priority`, 'Un-Defined') AS `priority`,
        IFNULL(`d`.`assigneeName`, 'UnAssigned') AS `assigneeName`,
        IFNULL(`d`.`severity`, 'Un-Defined') AS `severity`,
        IFNULL(`d`.`component`, 'No Components') AS `component`,
        IFNULL(`d`.`sprintId`, 'Un-Defined') AS `sprintId`,
        IFNULL(`d`.`reporter`, 'UnAssigned') AS `reporter`,
        IFNULL(`d`.`progress`, 'Un-Defined') AS `progress`,
        IFNULL(`d`.`stereoType`, 'Un-Defined') AS `stereoType`,
        IFNULL(`d`.`epicLink`, 'Un-Defined') AS `epicLink`,
        IFNULL(`d`.`fixVersions`, 'Un-Defined') AS `fixVersions`,
        `pm`.`ReleaseName` AS `ReleaseName`,
        IFNULL(`d`.`issueType`, 'Un-Defined') AS `issueType`,
        IFNULL(`d`.`parentKey`, 'Un-Defined') AS `parentKey`,
        IFNULL(`d`.`storyDefectIdFixVersion`,
                'Un-Defined') AS `storyDefectIdFixVersion`,
        REPLACE(`pm`.`JIRA_KEY`, ' ', '') AS `JIRA_KEY`
    FROM
        (`defect_details` `d`
        JOIN `v_project_wise_defect_keys` `pm` ON (`d`.`projectKey` = REPLACE(`pm`.`JIRA_KEY`, ' ', '')
            AND `d`.`fixVersions` = `pm`.`ReleaseName`))
    WHERE
        `d`.`status` <> 'Closed'
            AND `d`.`fixVersions` <> 'Un-Defined'

            
/** user stories status mapping **/

select `d`.`userStoryID` AS `userStoryID`,`d`.`projectKey` AS `projectKey`,`d`.`projectName` AS `projectName`,
 CASE
            WHEN `d`.`status` LIKE '%Backlog%' THEN 'Backlog'
            WHEN `d`.`status` LIKE '%BA in progress%' THEN 'Backlog'
            WHEN `d`.`status` LIKE '%Open%' THEN 'Backlog'
			
            WHEN `d`.`status` LIKE '%BA to validate%' THEN 'Analysis'
            WHEN `d`.`status` LIKE '%FA in progress%' THEN 'Analysis'
            WHEN `d`.`status` LIKE '%FA to validate%' THEN 'Analysis'
            WHEN `d`.`status` LIKE '%TA in progress%' THEN 'Analysis'
            WHEN `d`.`status` LIKE '%TA to validate%' THEN 'Analysis'
            WHEN `d`.`status` LIKE '%To groom%' THEN 'Analysis'
            WHEN `d`.`status` LIKE '%Sprintable%' THEN 'Analysis'
            WHEN `d`.`status` LIKE '%In Inception%' THEN 'Analysis'
            WHEN `d`.`status` LIKE '%To validate%' THEN 'Analysis'
            WHEN `d`.`status` LIKE '%Ready%' THEN 'Analysis'
			
            WHEN `d`.`status` LIKE '%In progress%' THEN 'Development'
            WHEN `d`.`status` LIKE 'In review' THEN 'Development'
            WHEN `d`.`status` LIKE 'To promote' THEN 'Development'
			
            WHEN `d`.`status` LIKE 'In test' THEN 'Build/Test'
            WHEN `d`.`status` LIKE '%PO To validate%' THEN 'Build/Test'
            WHEN `d`.`status` LIKE '%Resolved%' THEN 'Build/Test'
            WHEN `d`.`status` LIKE '%To Verify%' THEN 'Build/Test'
            WHEN `d`.`status` LIKE '%To review%' THEN 'Build/Test'
			
			WHEN `d`.`status` LIKE 'Ready for uat' THEN 'Closed'
            WHEN `d`.`status` LIKE '%Closed%' THEN 'Closed'
			
			WHEN `d`.`status` LIKE 'Reopened' THEN 'Reopened'			
            
        END AS `storyStatus`
,ifnull(`pm`.`Domain`,'Un-Defined') AS `domainName`,`pm`.`ProjectReference` 
AS `ProjectReference`,ifnull(`d`.`resolution`,'Un-Defined') AS `resolution`,ifnull(`d`.`priority`,'Un-Defined') AS `priority`,ifnull(`d`.`assigneeName`,'UnAssigned') AS `assigneeName`,ifnull(`d`.`severity`,'Un-Defined')
 AS `severity`,ifnull(`d`.`component`,'No Components') AS `component`,ifnull(`d`.`sprintId`,'Un-Defined') AS `sprintId`,ifnull(`d`.`reporter`,'UnAssigned') AS `reporter`,ifnull(`d`.`epicLink`,'Un-Defined') AS `epicLink`,
 ifnull(`d`.`fixVersions`,'Un-Defined') AS `fixVersions`,`pm`.`ReleaseName` AS `ReleaseName`,ifnull(`d`.`issueType`,'Un-Defined') AS `issueType`,replace(`pm`.`JIRA_KEY`,' ','') AS `JIRA_KEY` from 
 (`user_story_details` `d` join `v_project_wise_defect_keys` `pm` on(`d`.`projectKey` = replace(`pm`.`JIRA_KEY`,' ','') and `d`.`fixVersions` = `pm`.`ReleaseName`)) ;



 /* Duplicate records for TestResults & Requirments   21-10-2020 */ 

ALTER TABLE `iar_byld_1`.`requirement_details` 
ADD COLUMN `releaseId` INT(11) NULL AFTER `updatedOn`;


ALTER TABLE `iar_byld_1`.`test_execution_results` 
ADD COLUMN `releaseId` INT(11) NULL AFTER `preConditions`;

ALTER TABLE `iar_byld_1`.`requirement_details` 
DROP INDEX `UNIQ` ,
ADD UNIQUE INDEX `UNIQ` (`projectId` ASC, `releaseId` ASC, `RequirementID` ASC, `requirementTreeIds` ASC) ;


ALTER TABLE `iar_byld_1`.`test_execution_results` 
DROP INDEX `UNIQ` ,
ADD UNIQUE INDEX `UNIQ` (`releaseId` ASC, `projectName` ASC, `testCaseId` ASC, `cyclePhaseId` ASC, `tcrCatalogTreeId` ASC) ;



/** REQUIREMENT COVERAGE - DEFECT LINKING VIEW CHANGES  - #189  21-10-2020 **/

CREATE OR REPLACE VIEW `v_temp_actualtestid_wise_req` AS with recursive t as (select `requirement_details`.`id` AS `id`,`requirement_details`.`projectName` AS `projectName`,`requirement_details`.`releaseName` AS `releaseName`,`requirement_details`.`requirementTreePath` AS `requirementTreePath`,`requirement_details`.`RequirementTreeNode` AS `RequirementTreeNode`,`requirement_details`.`RequirementTreeSubNode` AS `RequirementTreeSubNode`,`requirement_details`.`RequirementTreeSubNode1` AS `RequirementTreeSubNode1`,`requirement_details`.`RequirementID` AS `RequirementID`,`requirement_details`.`requirementName` AS `requirementName`,`requirement_details`.`priority` AS `priority`,`requirement_details`.`createdOn` AS `createdOn`,`requirement_details`.`reqCreationDate` AS `reqCreationDate`,`requirement_details`.`createdBy` AS `createdBy`,`requirement_details`.`lastModifiedBy` AS `lastModifiedBy`,`requirement_details`.`lastModifiedOn` AS `lastModifiedOn`,`requirement_details`.`externalId` AS `externalId`,`requirement_details`.`requirementType` AS `requirementType`,`requirement_details`.`ReqStatus` AS `ReqStatus`,`requirement_details`.`attachmentCount` AS `attachmentCount`,`requirement_details`.`releaseIds` AS `releaseIds`,`requirement_details`.`requirementTreeIds` AS `requirementTreeIds`,`requirement_details`.`reqReleaseTCCountMapId` AS `reqReleaseTCCountMapId`,`requirement_details`.`reqReleaseTCCountMapReqId` AS `reqReleaseTCCountMapReqId`,`requirement_details`.`reqReleaseTCCountMapReleaseId` AS `reqReleaseTCCountMapReleaseId`,`requirement_details`.`reqReleaseTCCountMapTCCount` AS `reqReleaseTCCountMapTCCount`,`requirement_details`.`projectId` AS `projectId`,`requirement_details`.`releaseNames` AS `releaseNames`,`requirement_details`.`actualTestcaseIds` AS `actualTestcaseIds`,`requirement_details`.`insertedOn` AS `insertedOn`,`requirement_details`.`updatedOn` AS `updatedOn` from `requirement_details`), n as (select 1 AS `n` union select `n`.`n` + 1 AS `n + 1` from (`n` join `t`) where `n`.`n` <= octet_length(`t`.`actualTestcaseIds`) - octet_length(replace(`t`.`actualTestcaseIds`,',','')))select distinct substring_index(substring_index(`t`.`actualTestcaseIds`,',',`n`.`n`),',',-1) AS `actualTestIDs`,`t`.`id` AS `id`,`t`.`projectName` AS `projectName`,`t`.`releaseName` AS `releaseName`,`t`.`requirementTreePath` AS `requirementTreePath`,`t`.`RequirementTreeNode` AS `RequirementTreeNode`,`t`.`RequirementTreeSubNode` AS `RequirementTreeSubNode`,`t`.`RequirementTreeSubNode1` AS `RequirementTreeSubNode1`,`t`.`RequirementID` AS `RequirementID`,`t`.`requirementName` AS `requirementName`,`t`.`priority` AS `priority`,`t`.`createdOn` AS `createdOn`,`t`.`reqCreationDate` AS `reqCreationDate`,`t`.`createdBy` AS `createdBy`,`t`.`lastModifiedBy` AS `lastModifiedBy`,`t`.`lastModifiedOn` AS `lastModifiedOn`,`t`.`externalId` AS `externalId`,`t`.`requirementType` AS `requirementType`,`t`.`ReqStatus` AS `ReqStatus`,`t`.`attachmentCount` AS `attachmentCount`,`t`.`releaseIds` AS `releaseIds`,`t`.`requirementTreeIds` AS `requirementTreeIds`,`t`.`reqReleaseTCCountMapId` AS `reqReleaseTCCountMapId`,`t`.`reqReleaseTCCountMapReqId` AS `reqReleaseTCCountMapReqId`,`t`.`reqReleaseTCCountMapReleaseId` AS `reqReleaseTCCountMapReleaseId`,`t`.`reqReleaseTCCountMapTCCount` AS `reqReleaseTCCountMapTCCount`,`t`.`projectId` AS `projectId`,`t`.`releaseNames` AS `releaseNames`,`t`.`actualTestcaseIds` AS `actualTestcaseIds`,`t`.`insertedOn` AS `insertedOn`,`t`.`updatedOn` AS `updatedOn` from (`n` join (select `requirement_details`.`id` AS `id`,`requirement_details`.`projectName` AS `projectName`,`requirement_details`.`releaseName` AS `releaseName`,`requirement_details`.`requirementTreePath` AS `requirementTreePath`,`requirement_details`.`RequirementTreeNode` AS `RequirementTreeNode`,`requirement_details`.`RequirementTreeSubNode` AS `RequirementTreeSubNode`,`requirement_details`.`RequirementTreeSubNode1` AS `RequirementTreeSubNode1`,`requirement_details`.`RequirementID` AS `RequirementID`,`requirement_details`.`requirementName` AS `requirementName`,`requirement_details`.`priority` AS `priority`,`requirement_details`.`createdOn` AS `createdOn`,`requirement_details`.`reqCreationDate` AS `reqCreationDate`,`requirement_details`.`createdBy` AS `createdBy`,`requirement_details`.`lastModifiedBy` AS `lastModifiedBy`,`requirement_details`.`lastModifiedOn` AS `lastModifiedOn`,`requirement_details`.`externalId` AS `externalId`,`requirement_details`.`requirementType` AS `requirementType`,`requirement_details`.`ReqStatus` AS `ReqStatus`,`requirement_details`.`attachmentCount` AS `attachmentCount`,`requirement_details`.`releaseIds` AS `releaseIds`,`requirement_details`.`requirementTreeIds` AS `requirementTreeIds`,`requirement_details`.`reqReleaseTCCountMapId` AS `reqReleaseTCCountMapId`,`requirement_details`.`reqReleaseTCCountMapReqId` AS `reqReleaseTCCountMapReqId`,`requirement_details`.`reqReleaseTCCountMapReleaseId` AS `reqReleaseTCCountMapReleaseId`,`requirement_details`.`reqReleaseTCCountMapTCCount` AS `reqReleaseTCCountMapTCCount`,`requirement_details`.`projectId` AS `projectId`,`requirement_details`.`releaseNames` AS `releaseNames`,`requirement_details`.`actualTestcaseIds` AS `actualTestcaseIds`,`requirement_details`.`insertedOn` AS `insertedOn`,`requirement_details`.`updatedOn` AS `updatedOn` from `requirement_details`) `t`);

CREATE OR REPLACE VIEW `v_temp_req_wise_test_results` AS with _test_execution_results as (select `test_execution_results`.`id` AS `id`,`test_execution_results`.`projectName` AS `projectName`,`test_execution_results`.`releaseName` AS `releaseName`,`test_execution_results`.`hierarchy` AS `hierarchy`,`test_execution_results`.`folder` AS `folder`,`test_execution_results`.`subFolder` AS `subFolder`,`test_execution_results`.`subFolder1` AS `subFolder1`,`test_execution_results`.`subFolder2` AS `subFolder2`,`test_execution_results`.`subfolder3` AS `subfolder3`,`test_execution_results`.`subfolder4` AS `subfolder4`,`test_execution_results`.`subfolder5` AS `subfolder5`,`test_execution_results`.`subfolder6` AS `subfolder6`,`test_execution_results`.`subfolder7` AS `subfolder7`,`test_execution_results`.`subfolder8` AS `subfolder8`,`test_execution_results`.`subfolder9` AS `subfolder9`,`test_execution_results`.`subfolder10` AS `subfolder10`,`test_execution_results`.`createdOn` AS `createdOn`,`test_execution_results`.`requirementId` AS `requirementId`,`test_execution_results`.`reqAltId` AS `reqAltId`,`test_execution_results`.`requirementName` AS `requirementName`,`test_execution_results`.`environment` AS `environment`,`test_execution_results`.`testCaseAltId` AS `testCaseAltId`,`test_execution_results`.`testCaseName` AS `testCaseName`,`test_execution_results`.`testCaseId` AS `testCaseId`,`test_execution_results`.`automation` AS `automation`,`test_execution_results`.`cycle` AS `cycle`,`test_execution_results`.`testStatus` AS `testStatus`,`test_execution_results`.`executedOn` AS `executedOn`,`test_execution_results`.`tags` AS `tags`,`test_execution_results`.`defects` AS `defects`,`test_execution_results`.`executedBy` AS `executedBy`,`test_execution_results`.`createdBy` AS `createdBy`,`test_execution_results`.`insertedOn` AS `insertedOn`,`test_execution_results`.`subFolderLevel` AS `subFolderLevel`,`test_execution_results`.`updatedOn` AS `updatedOn`,`test_execution_results`.`cyclePhaseId` AS `cyclePhaseId`,`test_execution_results`.`tcrCatalogTreeId` AS `tcrCatalogTreeId`,`test_execution_results`.`preConditions` AS `preConditions`,row_number() over ( partition by `test_execution_results`.`testCaseId` order by `test_execution_results`.`executedOn` desc) AS `row_num` from `test_execution_results`)select `_test_execution_results`.`id` AS `id`,`_test_execution_results`.`projectName` AS `projectName`,`_test_execution_results`.`releaseName` AS `releaseName`,`_test_execution_results`.`hierarchy` AS `hierarchy`,`_test_execution_results`.`folder` AS `folder`,`_test_execution_results`.`subFolder` AS `subFolder`,`_test_execution_results`.`subFolder1` AS `subFolder1`,`_test_execution_results`.`subFolder2` AS `subFolder2`,`_test_execution_results`.`subfolder3` AS `subfolder3`,`_test_execution_results`.`subfolder4` AS `subfolder4`,`_test_execution_results`.`subfolder5` AS `subfolder5`,`_test_execution_results`.`subfolder6` AS `subfolder6`,`_test_execution_results`.`subfolder7` AS `subfolder7`,`_test_execution_results`.`subfolder8` AS `subfolder8`,`_test_execution_results`.`subfolder9` AS `subfolder9`,`_test_execution_results`.`subfolder10` AS `subfolder10`,`_test_execution_results`.`createdOn` AS `createdOn`,`_test_execution_results`.`requirementId` AS `requirementId`,`_test_execution_results`.`reqAltId` AS `reqAltId`,`_test_execution_results`.`requirementName` AS `requirementName`,`_test_execution_results`.`environment` AS `environment`,`_test_execution_results`.`testCaseAltId` AS `testCaseAltId`,`_test_execution_results`.`testCaseName` AS `testCaseName`,`_test_execution_results`.`testCaseId` AS `testCaseId`,`_test_execution_results`.`automation` AS `automation`,`_test_execution_results`.`cycle` AS `cycle`,`_test_execution_results`.`testStatus` AS `testStatus`,`_test_execution_results`.`executedOn` AS `executedOn`,`_test_execution_results`.`tags` AS `tags`,`_test_execution_results`.`defects` AS `defects`,`_test_execution_results`.`executedBy` AS `executedBy`,`_test_execution_results`.`createdBy` AS `createdBy`,`_test_execution_results`.`insertedOn` AS `insertedOn`,`_test_execution_results`.`subFolderLevel` AS `subFolderLevel`,`_test_execution_results`.`updatedOn` AS `updatedOn`,`_test_execution_results`.`cyclePhaseId` AS `cyclePhaseId`,`_test_execution_results`.`tcrCatalogTreeId` AS `tcrCatalogTreeId`,`_test_execution_results`.`preConditions` AS `preConditions`,`_test_execution_results`.`row_num` AS `row_num` from `_test_execution_results` where `_test_execution_results`.`row_num` = 1;

CREATE OR REPLACE VIEW `v_temp_req_wise_test_wise_defects` AS SELECT `r`.`RequirementID` AS `requirementID`, `t`.`testCaseId` AS `testcaseid`, GROUP_CONCAT(DISTINCT `t`.`defects` SEPARATOR ',') AS `defectids` FROM (`v_temp_actualtestid_wise_req` `r` JOIN `v_temp_req_wise_test_results` `t`) WHERE `r`.`RequirementID` = `t`.`requirementId` GROUP BY `r`.`RequirementID`;
	
CREATE OR REPLACE VIEW `v_requirement_details` AS select trim(substring_index(`rq`.`projectName`,'-',1)) AS `ProjectReference`,trim(substr(`rq`.`projectName`,locate('-',`rq`.`projectName`) + 1)) AS `ProjectName`,`rq`.`projectName` AS `project`,`rq`.`releaseName` AS `ReleaseName`,`rq`.`requirementTreePath` AS `RequirementTreePath`,`rq`.`RequirementTreeNode` AS `RequirementTreeNode`,`rq`.`RequirementTreeSubNode` AS `RequirementTreeSubNode`,`rq`.`RequirementTreeSubNode1` AS `RequirementTreeSubNode1`,`rq`.`RequirementID` AS `RequirementID`,`rq`.`actualTestcaseIds` AS `actualTestCaseIDs`,`rq`.`reqReleaseTCCountMapTCCount` AS `MappedTCCount`,`rq`.`actualTestIDs` AS `actualTestIds`,`tr`.`testCaseId` AS `testcaseid`,ifnull(`tr`.`testStatus`,'Not Covered') AS `testStatus`,ifnull(`tr`.`testStatus`,'Not Covered') AS `reqCoverageStatus`,`tr`.`releaseName` AS `fixVersions`,`tr`.`cycle` AS `cycle`,`tr`.`defects` AS `defectId`,`tr`.`executedOn` AS `executedOn`,replace(`rd`.`defectids`,',,',',') AS `defectids`,case when (`rq`.`actualTestcaseIds` is null or `rq`.`actualTestcaseIds` = '') then 'Not Covered' else 'Covered' end AS `ReQCoverage` from ((`v_temp_actualtestid_wise_req` `rq` left join `v_temp_req_wise_test_results` `tr` on(`rq`.`RequirementID` = `tr`.`requirementId`)) left join `v_temp_req_wise_test_wise_defects` `rd` on(`tr`.`requirementId` = `rd`.`requirementID`));

/** Release_milestones Bug Fixed #192 **/

ALTER TABLE `release_milestones_status`
	CHANGE COLUMN `TestKeyMessage` `TestKeyMessage` LONGTEXT NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `ReleaseTestStatus`;

/*Test_reports*/

ALTER TABLE `test_reports`
CHANGE COLUMN `Modified` `Modified` VARCHAR(50) NULL DEFAULT NULL AFTER `FileServerRelativeURL`;

ALTER TABLE `test_reports`
CHANGE COLUMN `Created` `Created` VARCHAR(50) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci' AFTER `updatedOn`;

/* Test Results */

ALTER TABLE `test_execution_results`
	CHANGE COLUMN `hierarchy` `hierarchy` VARCHAR(900) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `releaseName`;
ALTER TABLE `test_execution_results`
	CHANGE COLUMN `subFolder1` `subFolder1` VARCHAR(450) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `subFolder`;
ALTER TABLE `test_execution_results`
	CHANGE COLUMN `testCaseName` `testCaseName` LONGTEXT NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `testCaseAltId`;

/*Issue #194 fixed*/

ALTER TABLE `iar_byld_1`.`story_details` 
RENAME TO  `iar_byld_1`.`story_defect_details` ;

ALTER TABLE `defect_details`
	CHANGE COLUMN `fixVersions` `fixVersions` VARCHAR(450) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `projectCategory`;


/* requirement details upload issue - fixed */

ALTER TABLE `requirement_details`
	CHANGE COLUMN `requirementTreePath` `requirementTreePath` LONGTEXT NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci';
ALTER TABLE `requirement_details`
	CHANGE COLUMN `RequirementTreeSubNode` `RequirementTreeSubNode` LONGTEXT NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci';
ALTER TABLE `requirement_details`
	CHANGE COLUMN `requirementName` `requirementName` LONGTEXT NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci';
ALTER TABLE `requirement_details`
	CHANGE COLUMN `RequirementTreeNode` `RequirementTreeNode` LONGTEXT NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci';

/*User Stories*/
ALTER TABLE `user_story_details`
	CHANGE COLUMN `updated` `updated` VARCHAR(50) NULL DEFAULT NULL AFTER `assigneeName`;

/* created date added*/

CREATE OR REPLACE VIEW `v_defects_data` AS
    SELECT 
        `d`.`defectId` AS `defectId`,
        `d`.`defectKey` AS `defectKey`,
        `d`.`projectKey` AS `projectKey`,
        `d`.`projectName` AS `projectName`,
        `d`.`projectCategory` AS `projectCategory`,
        IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`,
        `pm`.`ProjectReference` AS `projectReference`,
        IFNULL(`d`.`resolution`, 'Un-Defined') AS `resolution`,
        IFNULL(`d`.`defectSevirity`, 'Un-Defined') AS `defectSevirity`,
        IFNULL(`d`.`product`, 'Un-Defined') AS `product`,
        CASE
            WHEN `d`.`testLevelDetected` LIKE '%system%' THEN 'System Test'
            WHEN `d`.`testLevelDetected` LIKE '%sprint%' THEN 'Sprint Test'
            WHEN `d`.`testLevelDetected` LIKE '%IT%' THEN 'Integration Test'
            WHEN `d`.`testLevelDetected` LIKE '%UAT%' THEN 'UAT'
            WHEN `d`.`testLevelDetected` LIKE '%unit%' THEN 'Unit Test'
            ELSE 'Un-Defined'
        END AS `testLevelDetected`,
        CASE
            WHEN `d`.`testTypes` LIKE '%func%' THEN 'Functional'
            WHEN `d`.`testTypes` LIKE '%aut%' THEN 'Automation'
            WHEN `d`.`testTypes` LIKE '%per%' THEN 'Performance'
            WHEN `d`.`testTypes` LIKE '%sec%' THEN 'Security Test'
            WHEN `d`.`testTypes` LIKE '%uni%' THEN 'Unit Test'
            ELSE 'Un-Defined'
        END AS `testTypes`,
        CASE
            WHEN `d`.`status` LIKE '%To Verify%' THEN 'In Test'
            WHEN `d`.`status` LIKE '%To Groom%' THEN 'In Test'
            WHEN `d`.`status` LIKE '%In Test%' THEN 'In Test'
            WHEN `d`.`status` LIKE '%UAT on-going%' THEN 'In Test'
            WHEN `d`.`status` LIKE '%Ready%' THEN 'UAT Ready'
            WHEN `d`.`status` LIKE '%open%' THEN 'Open'
            WHEN `d`.`status` LIKE '%sprintable%' THEN 'IN SPRINTABLE'
            WHEN `d`.`status` LIKE '%progress%' THEN 'In Progress'
            WHEN `d`.`status` LIKE '%Analysis%' THEN 'In Progress'
            WHEN `d`.`status` LIKE '%Business Validated%' THEN 'In Progress'
            WHEN `d`.`status` LIKE '%review%' THEN 'In Review'
            WHEN `d`.`status` LIKE '%promot%' THEN 'To Promote'
            WHEN `d`.`status` LIKE 'open' THEN 'Open'
            WHEN `d`.`status` LIKE 'Backlog' THEN 'Open'
            WHEN `d`.`status` LIKE 'Next' THEN 'Open'
            WHEN `d`.`status` LIKE '%resol%' THEN 'Resolved'
            WHEN `d`.`status` LIKE '%done%' THEN 'Resolved'
            WHEN `d`.`status` LIKE '%reopened%' THEN 'Reopened'
            WHEN `d`.`status` LIKE '%closed%' THEN 'Closed'
        END AS `status`,
          `d`.`createdOn` AS `createdOn`,
        IFNULL(`d`.`createdUserName`, 'UnAssigned') AS `createdUserName`,
        IFNULL(`d`.`priority`, 'Un-Defined') AS `priority`,
        IFNULL(`d`.`assigneeName`, 'UnAssigned') AS `assigneeName`,
        IFNULL(`d`.`severity`, 'Un-Defined') AS `severity`,
        IFNULL(`d`.`component`, 'No Components') AS `component`,
        IFNULL(`d`.`sprintId`, 'Un-Defined') AS `sprintId`,
        IFNULL(`d`.`reporter`, 'UnAssigned') AS `reporter`,
        IFNULL(`d`.`progress`, 'Un-Defined') AS `progress`,
        IFNULL(`d`.`stereoType`, 'Un-Defined') AS `stereoType`,
        IFNULL(`d`.`epicLink`, 'Un-Defined') AS `epicLink`,
        IFNULL(`d`.`fixVersions`, 'Un-Defined') AS `fixVersions`,
        `pm`.`ReleaseName` AS `ReleaseName`,
        IFNULL(`d`.`issueType`, 'Un-Defined') AS `issueType`,
        IFNULL(`d`.`parentKey`, 'Un-Defined') AS `parentKey`,
        IFNULL(`d`.`storyDefectIdFixVersion`,
                'Un-Defined') AS `storyDefectIdFixVersion`,
        REPLACE(`pm`.`JIRA_KEY`, ' ', '') AS `JIRA_KEY`
    FROM
        (`defect_details` `d`
        JOIN `v_project_wise_defect_keys` `pm` ON (`d`.`projectKey` = REPLACE(`pm`.`JIRA_KEY`, ' ', '')
            AND `d`.`fixVersions` = `pm`.`ReleaseName`))
    WHERE
        `d`.`status` <> 'Closed'
            AND `d`.`fixVersions` <> 'Un-Defined';

/* Defect Summary field Added*/

ALTER TABLE `defect_details`
	ADD COLUMN `summary` VARCHAR(405) NULL DEFAULT NULL AFTER `origin`;
ALTER TABLE `defect_details`
	CHANGE COLUMN `summary` `summary` LONGTEXT NULL DEFAULT NULL COLLATE 'latin1_swedish_ci' AFTER `origin`;
ALTER TABLE `defect_details`
	CHANGE COLUMN `summary` `summary` LONGTEXT NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci' AFTER `origin`;

/* Defect View Updated*/

CREATE OR REPLACE VIEW `v_defects_data` AS
SELECT 
        `d`.`defectId` AS `defectId`,
        `d`.`defectKey` AS `defectKey`,
        `d`.`projectKey` AS `projectKey`,
        `d`.`projectName` AS `projectName`,
        `d`.`projectCategory` AS `projectCategory`,
        IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`,
        `pm`.`ProjectReference` AS `projectReference`,
        IFNULL(`d`.`resolution`, 'Un-Defined') AS `resolution`,
        IFNULL(`d`.`defectSevirity`, 'Un-Defined') AS `defectSevirity`,
        IFNULL(`d`.`product`, 'Un-Defined') AS `product`,
        CASE
            WHEN `d`.`testLevelDetected` LIKE '%system%' THEN 'System Test'
            WHEN `d`.`testLevelDetected` LIKE '%sprint%' THEN 'Sprint Test'
            WHEN `d`.`testLevelDetected` LIKE '%IT%' THEN 'Integration Test'
            WHEN `d`.`testLevelDetected` LIKE '%UAT%' THEN 'UAT'
            WHEN `d`.`testLevelDetected` LIKE '%unit%' THEN 'Unit Test'
            ELSE 'Un-Defined'
        END AS `testLevelDetected`,
        CASE
            WHEN `d`.`testTypes` LIKE '%func%' THEN 'Functional'
            WHEN `d`.`testTypes` LIKE '%aut%' THEN 'Automation'
            WHEN `d`.`testTypes` LIKE '%per%' THEN 'Performance'
            WHEN `d`.`testTypes` LIKE '%sec%' THEN 'Security Test'
            WHEN `d`.`testTypes` LIKE '%uni%' THEN 'Unit Test'
            ELSE 'Un-Defined'
        END AS `testTypes`,
        CASE
            WHEN `d`.`status` LIKE '%To Validate%' THEN 'UAT Ready'
            WHEN `d`.`status` LIKE '%To Verify%' THEN 'In Test'
            WHEN `d`.`status` LIKE '%To Groom%' THEN 'In Test'
            WHEN `d`.`status` LIKE '%In Test%' THEN 'In Test'
            WHEN `d`.`status` LIKE '%UAT on-going%' THEN 'In Test'
            WHEN `d`.`status` LIKE '%Ready%' THEN 'UAT Ready'
            WHEN `d`.`status` LIKE '%open%' THEN 'Open'
            WHEN `d`.`status` LIKE '%sprintable%' THEN 'In Progress'
            WHEN `d`.`status` LIKE '%progress%' THEN 'In Progress'
            WHEN `d`.`status` LIKE '%Analysis%' THEN 'In Progress'
            WHEN `d`.`status` LIKE '%Business Validated%' THEN 'In Progress'
            WHEN `d`.`status` LIKE '%review%' THEN 'In Review'
            WHEN `d`.`status` LIKE '%incep%' THEN 'In Review'
            WHEN `d`.`status` LIKE '%promot%' THEN 'To Promote'
            WHEN `d`.`status` LIKE 'open' THEN 'Open'
            WHEN `d`.`status` LIKE 'Backlog' THEN 'Open'
            WHEN `d`.`status` LIKE 'Next' THEN 'Open'
            WHEN `d`.`status` LIKE '%resol%' THEN 'Resolved'
            WHEN `d`.`status` LIKE '%done%' THEN 'Resolved'
            WHEN `d`.`status` LIKE '%reopened%' THEN 'Reopened'
            WHEN `d`.`status` LIKE '%closed%' THEN 'Closed'
            ELSE `d`.`status`
        END AS `status`,
        `d`.`createdOn` AS `createdOn`,
        `d`.`summary` AS `summary`,
        IFNULL(`d`.`createdUserName`, 'UnAssigned') AS `createdUserName`,
        IFNULL(`d`.`priority`, 'Un-Defined') AS `priority`,
        IFNULL(`d`.`assigneeName`, 'UnAssigned') AS `assigneeName`,
        IFNULL(`d`.`severity`, 'Un-Defined') AS `severity`,
        IFNULL(`d`.`component`, 'No Components') AS `component`,
        IFNULL(`d`.`sprintId`, 'Un-Defined') AS `sprintId`,
        IFNULL(`d`.`reporter`, 'UnAssigned') AS `reporter`,
        IFNULL(`d`.`progress`, 'Un-Defined') AS `progress`,
        IFNULL(`d`.`stereoType`, 'Un-Defined') AS `stereoType`,
        IFNULL(`d`.`epicLink`, 'Un-Defined') AS `epicLink`,
        IFNULL(`d`.`fixVersions`, 'Un-Defined') AS `fixVersions`,
        `pm`.`ReleaseName` AS `ReleaseName`,
        IFNULL(`d`.`issueType`, 'Un-Defined') AS `issueType`,
        IFNULL(`d`.`parentKey`, 'Un-Defined') AS `parentKey`,
        IFNULL(`d`.`storyDefectIdFixVersion`,
                'Un-Defined') AS `storyDefectIdFixVersion`,
        REPLACE(`pm`.`JIRA_KEY`, ' ', '') AS `JIRA_KEY`
    FROM
        (`defect_details` `d`
        JOIN `v_project_wise_defect_keys` `pm` ON (`d`.`projectKey` = REPLACE(`pm`.`JIRA_KEY`, ' ', '')
            AND `d`.`fixVersions` = `pm`.`ReleaseName`))
    WHERE
        `d`.`status` <> 'Closed'
            AND `d`.`fixVersions` <> 'Un-Defined' ;


/* Emailing list for PDF attachements - 09/11/2020   #200 */

ALTER TABLE `project_milestones_status`
	ADD COLUMN `Test_Coordinator_email` VARCHAR(405) NULL DEFAULT NULL AFTER `Test_Coordinator`;
    
ALTER TABLE `project_milestones_status`
	ADD COLUMN `Project_Manager_email` VARCHAR(405) NULL DEFAULT NULL AFTER `Project_Manager`;
    
ALTER TABLE `project_milestones_status`
	ADD COLUMN `Scrum_Master_email` VARCHAR(405) NULL DEFAULT NULL AFTER `Scrum_Master`;
    
ALTER TABLE `project_milestones_status`
	ADD COLUMN `Delivery_Master_Lead_email` VARCHAR(405) NULL DEFAULT NULL AFTER `Delivery_Master_Lead`;


    /** Emailing list changes for project_ccb_view  #200 */

CREATE 
     OR REPLACE VIEW `v_project_ccb_details` AS
    SELECT 
        `rm`.`ReleaseName` AS `ReleaseName`,
        `rm`.`ReleaseDate` AS `ReleaseDate`,
        `pm`.`ProjectReference` AS `ProjectReference`,
        `pm`.`ProjectName` AS `ProjectName1`,
        CONCAT(`pm`.`ProjectReference`,
                ' - ',
                `pm`.`ProjectName`) AS `ProjectName`,
        `pm`.`Test_Status` AS `test_status`,
        `pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`,
        `pm`.`Domain` AS `Domain`,
        `pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`,
        `pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link`,
        `pm`.`JIRA_Link` AS `JIRA_Link1`,
        CONCAT('https://jira.pvgroup.be/issues/?jql=project in (',
                `pm`.`Jira_defect_projects`,
                ') AND issuetype in (Defect,"Story Defect") AND status != Closed AND created > startOfMonth(-12) ORDER BY created DESC') AS `JIRA_Link`,
        `pm`.`ZEPHYR_Link` AS `ZEPHYR_Link`,
        `pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`,
        `pm`.`Test_Coordinator` AS `Test_Coordinator`,
        `pm`.`Scrum_Master` AS `Scrum_Master`,
        IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`,
        IFNULL(`pm`.`CCB_STARTUP_Date`,
                `rm`.`LastRegistration`) AS `CCB_STARTUP_Date`,
        IFNULL(`pm`.`CCB_Ready4SPRINT_Date`,
                `rm`.`CCB_Ready4SPRINT`) AS `CCB_Ready4SPRINT_Date`,
        IFNULL(`pm`.`CCB_Ready4UAT_Date`,
                `rm`.`CCB_Ready4UAT`) AS `CCB_Ready4UAT_Date`,
        IFNULL(`pm`.`CCB_Ready4PROD_Date`,
                `rm`.`CCB_Ready4PROD`) AS `CCB_Ready4PROD_Date`,
        IFNULL(`pm`.`CCB_Closure_Date`,
                `rm`.`CCB_Closure`) AS `CCB_Closure_Date`,
        `pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`,
        `pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`,
        CONVERT( `pm`.`CCB_STARTUP_Remarks` USING UTF8) AS `CCB_STARTUP_Remarks`,
        CONVERT( `pm`.`Ready2StartTestRemarks` USING UTF8) AS `Ready2StartTestRemarks`,
        `pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`,
        CONVERT( `pm`.`CCBReady4SPRINT_Remarks` USING UTF8) AS `CCBReady4SPRINT_Remarks`,
        CONVERT( `pm`.`Ready4SprintTestRemarks` USING UTF8) AS `Ready4SprintTestRemarks`,
        `pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`,
        CONVERT( `pm`.`CCBReady4UAT_Remarks` USING UTF8) AS `CCBReady4UAT_Remarks`,
        CONVERT( `pm`.`Ready4UATTestRemarks` USING UTF8) AS `Ready4UATTestRemarks`,
        `rm`.`LastRegistration` AS `LastRegistrationDate`,
        `rm`.`UAT_StartDate` AS `UAT_START_DATE`,
        `rm`.`End_UAT` AS `UAT_END_DATE`,
        `rm`.`Freeze` AS `FREEZE_DATE`,
        `pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`,
        CONVERT( `pm`.`CCBReady4PROD_Remarks` USING UTF8) AS `CCBReady4PROD_Remarks`,
        CONVERT( `pm`.`Ready4ProdTestRemarks` USING UTF8) AS `Ready4ProdTestRemarks`,
        CONVERT( `pm`.`CCB_CLOSURE_Remarks` USING UTF8) AS `CCB_CLOSURE_Remarks`,
        `pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`,
        `pm`.`Project_Manager` AS `Project_Manager`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `unitTestReportPath`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `UAT_TestReportPath`,
        `tr`.`AUTOMATION_PATH` AS `automationReportPath`,
        `tr`.`PERFORMANCE_PATH` AS `performanceReportPath`,
        `tr`.`SECURITY_TEST_PATH` AS `securityReportPath`,
        `tr`.`IT_TEST_PATH` AS `IT_TestReportPath`,
        `tr`.`CCB_GATING_PATH` AS `CCB_GATING_PATH`,
        `tr`.`MASTER_TEST_PLAN_PATH` AS `MASTER_TEST_PLAN_PATH`,
        `pm`.`Jira_defect_projects` AS `Jira_defect_projects`,
        `pm`.`DetailedTestStatus` AS `DetailedTestStatus`,
        `pm`.`Test_Coordinator_email`,
        `pm`.`Project_Manager_email`,
        `pm`.`Scrum_Master_email`,
        `pm`.`Delivery_Master_Lead_email`
    FROM
        ((`release_milestones_status` `rm`
        LEFT JOIN `project_milestones_status` `pm` ON (CONVERT( `rm`.`ReleaseName` USING UTF8) = `pm`.`ReleaseName`))
        LEFT JOIN `v_test_reports` `tr` ON (`pm`.`ReleaseName` = `tr`.`ReleaseName`
            AND `pm`.`ProjectName` = `tr`.`ProjectName`));





/** Delete Record **/

CREATE TABLE `project_wise_release_summary` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `releaseId` varchar(45) DEFAULT NULL,
  `mappedRequirementCount` int(11) DEFAULT NULL,
  `unmappedRequirementCount` int(11) DEFAULT NULL,
  `totalRequirementCount` int(11) DEFAULT NULL,
  `totalTestcaseCount` int(11) DEFAULT NULL,
  `totalCycleCount` int(11) DEFAULT NULL,
  `totalDefectCount` int(11) DEFAULT NULL,
  `totalDefectIds` longtext DEFAULT NULL,
  `projectId` int(11) DEFAULT NULL,
  `insertedOn` timestamp NULL DEFAULT current_timestamp(),
  `updatedOn` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UNIQ` (`releaseId`,`projectId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `project_wise_test_results_upload_summary` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `projectId` int(11) DEFAULT NULL,
  `releaseId` int(11) DEFAULT NULL,
  `insertCount` int(11) DEFAULT NULL,
  `updateCount` int(11) DEFAULT NULL,
  `lastRecordIdB4Insertion` int(120) DEFAULT NULL,
  `insertedOn` timestamp NULL DEFAULT current_timestamp(),
  `updatedOn` timestamp NULL DEFAULT NULL,
  `projectName` varchar(450) DEFAULT NULL,
  `releaseName` varchar(450) DEFAULT NULL,
  `totalRecordCount` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UNIQ` (`projectId`,`releaseId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


ALTER TABLE `iar_byld_1`.`test_execution_results` 
ADD COLUMN `projectId` INT NULL DEFAULT NULL AFTER `releaseId`;

ALTER TABLE `iar_byld_1`.`test_execution_results` 
DROP INDEX `UNIQ` ,
ADD UNIQUE INDEX `UNIQ` (`releaseId` ASC, `projectName` ASC, `testCaseId` ASC, `cyclePhaseId` ASC, `tcrCatalogTreeId` ASC) ;


/** #209   https://gitlab.com/expleo-p-v/diary/-/issues/209  - Requirement Coverage Chart Issue  - 20-11-2020*/

CREATE OR REPLACE VIEW `v_requirement_details` AS  SELECT TRIM(SUBSTRING_INDEX(`rq`.`projectName`, '-', 1)) AS `ProjectReference`,        TRIM(SUBSTR(`rq`.`projectName`, LOCATE('-', `rq`.`projectName`) + 1)) AS `ProjectName`, `rq`.`projectName` AS `project`, `rq`.`releaseName` AS `ReleaseName`, `rq`.`requirementTreePath` AS `RequirementTreePath`, `rq`.`RequirementTreeNode` AS `RequirementTreeNode`, `rq`.`RequirementTreeSubNode` AS `RequirementTreeSubNode`, `rq`.`RequirementTreeSubNode1` AS `RequirementTreeSubNode1`, `rq`.`RequirementID` AS `RequirementID`, `rq`.`actualTestcaseIds` AS `actualTestCaseIDs`, `rq`.`reqReleaseTCCountMapTCCount` AS `MappedTCCount`, `rq`.`actualTestIDs` AS `actualTestIds`, `tr`.`testCaseId` AS `testcaseid`, IFNULL(`tr`.`testStatus`, 'Not Covered') AS `testStatus`,  IFNULL(`tr`.`testStatus`, 'Not Covered') AS `reqCoverageStatus`, `tr`.`releaseName` AS `fixVersions`, `tr`.`cycle` AS `cycle`, `tr`.`defects` AS `defectId`, `tr`.`executedOn` AS `executedOn`, REPLACE(`rd`.`defectids`, ',,', ',') AS `defectids`, CASE WHEN (`rq`.`actualTestcaseIds` IS NULL  OR `rq`.`actualTestcaseIds` = '') THEN 'Not Covered' ELSE 'Covered' END AS `ReQCoverage` FROM ((`v_temp_actualtestid_wise_req` `rq` LEFT JOIN `v_temp_req_wise_test_results` `tr` ON (`rq`.`RequirementID` = `tr`.`requirementId` AND `rq`.`actualTestIDs` = `tr`.`testCaseId`)) LEFT JOIN `v_temp_req_wise_test_wise_defects` `rd` ON (`tr`.`requirementId` = `rd`.`requirementID`));


/**** #212 https://gitlab.com/expleo-p-v/diary/-/issues/212   --- 24-11-2020*/

CREATE OR REPLACE VIEW `v_project_ccb_details` AS SELECT  `rm`.`ReleaseName` AS `ReleaseName`,`rm`.`ReleaseDate` AS `ReleaseDate`,`pm`.`ProjectReference` AS `ProjectReference`,`pm`.`ProjectName` AS `ProjectName1`,CONCAT(`pm`.`ProjectReference`,' - ',`pm`.`ProjectName`) AS `ProjectName`,`pm`.`Test_Status` AS `test_status`,`pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`,`pm`.`Domain` AS `Domain`,`pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`,`pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link`,`pm`.`JIRA_Link` AS `JIRA_Link1`,CONCAT('https://jira.pvgroup.be/issues/?jql=project in (',`pm`.`Jira_defect_projects`,') AND issuetype in (Defect,"Story Defect") AND status != Closed AND created > startOfMonth(-12) ORDER BY created DESC') AS `JIRA_Link`,`pm`.`ZEPHYR_Link` AS `ZEPHYR_Link`,`pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`,`pm`.`Test_Coordinator` AS `Test_Coordinator`,`pm`.`Scrum_Master` AS `Scrum_Master`,IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`,IFNULL(`pm`.`CCB_STARTUP_Date`,`rm`.`LastRegistration`) AS `CCB_STARTUP_Date`,IFNULL(`pm`.`CCB_Ready4SPRINT_Date`,`rm`.`CCB_Ready4SPRINT`) AS `CCB_Ready4SPRINT_Date`,IFNULL(`pm`.`CCB_Ready4UAT_Date`,`rm`.`CCB_Ready4UAT`) AS `CCB_Ready4UAT_Date`,IFNULL(`pm`.`CCB_Ready4PROD_Date`,`rm`.`CCB_Ready4PROD`) AS `CCB_Ready4PROD_Date`,IFNULL(`pm`.`CCB_Closure_Date`,`rm`.`CCB_Closure`) AS `CCB_Closure_Date`,`pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`,`pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`,CONVERT( `pm`.`CCB_STARTUP_Remarks` USING UTF8) AS `CCB_STARTUP_Remarks`,CONVERT( `pm`.`Ready2StartTestRemarks` USING UTF8) AS `Ready2StartTestRemarks`,`pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`,CONVERT( `pm`.`CCBReady4SPRINT_Remarks` USING UTF8) AS `CCBReady4SPRINT_Remarks`,CONVERT( `pm`.`Ready4SprintTestRemarks` USING UTF8) AS `Ready4SprintTestRemarks`,`pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`,CONVERT( `pm`.`CCBReady4UAT_Remarks` USING UTF8) AS `CCBReady4UAT_Remarks`,CONVERT( `pm`.`Ready4UATTestRemarks` USING UTF8) AS `Ready4UATTestRemarks`,`rm`.`LastRegistration` AS `LastRegistrationDate`,`rm`.`UAT_StartDate` AS `UAT_START_DATE`,`rm`.`End_UAT` AS `UAT_END_DATE`,`rm`.`Freeze` AS `FREEZE_DATE`,`pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`,CONVERT( `pm`.`CCBReady4PROD_Remarks` USING UTF8) AS `CCBReady4PROD_Remarks`,CONVERT( `pm`.`Ready4ProdTestRemarks` USING UTF8) AS `Ready4ProdTestRemarks`,CONVERT( `pm`.`CCB_CLOSURE_Remarks` USING UTF8) AS `CCB_CLOSURE_Remarks`,`pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`,`pm`.`Project_Manager` AS `Project_Manager`,'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `unitTestReportPath`,'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `UAT_TestReportPath`,`tr`.`AUTOMATION_PATH` AS `automationReportPath`,`tr`.`PERFORMANCE_PATH` AS `performanceReportPath`,`tr`.`SECURITY_TEST_PATH` AS `securityReportPath`,`tr`.`IT_TEST_PATH` AS `IT_TestReportPath`,`tr`.`CCB_GATING_PATH` AS `CCB_GATING_PATH`,`tr`.`MASTER_TEST_PLAN_PATH` AS `MASTER_TEST_PLAN_PATH`,`pm`.`Jira_defect_projects` AS `Jira_defect_projects`,`pm`.`DetailedTestStatus` AS `DetailedTestStatus`,`pm`.`Test_Coordinator_email` AS `Test_Coordinator_email`,`pm`.`Project_Manager_email` AS `Project_Manager_email`,`pm`.`Scrum_Master_email` AS `Scrum_Master_email`,`pm`.`Delivery_Master_Lead_email` AS `Delivery_Master_Lead_email`,SUBSTRING_INDEX(`pm`.`Jira_defect_projects`, ',', 1) AS `JIRA_KEY`FROM((`release_milestones_status` `rm`LEFT JOIN `project_milestones_status` `pm` ON (CONVERT( `rm`.`ReleaseName` USING UTF8) = `pm`.`ReleaseName`))LEFT JOIN `v_test_reports` `tr` ON (`pm`.`ReleaseName` = `tr`.`ReleaseName` AND `pm`.`ProjectName` = `tr`.`ProjectName`));


/**** #211 https://gitlab.com/expleo-p-v/diary/-/issues/211   Handle default values for Zephyr LINK --- 24-11-2020*/

CREATE OR REPLACE VIEW `v_project_ccb_details` AS SELECT  `rm`.`ReleaseName` AS `ReleaseName`,`rm`.`ReleaseDate` AS `ReleaseDate`,`pm`.`ProjectReference` AS `ProjectReference`,`pm`.`ProjectName` AS `ProjectName1`,CONCAT(`pm`.`ProjectReference`,' - ',`pm`.`ProjectName`) AS `ProjectName`,`pm`.`Test_Status` AS `test_status`,`pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`,`pm`.`Domain` AS `Domain`,`pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`,`pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link`,`pm`.`JIRA_Link` AS `JIRA_Link1`,CONCAT('https://jira.pvgroup.be/issues/?jql=project in (',`pm`.`Jira_defect_projects`,') AND issuetype in (Defect,"Story Defect") AND status != Closed AND created > startOfMonth(-12) ORDER BY created DESC') AS `JIRA_Link`,IFNULL(`pm`.`ZEPHYR_Link`,'http://zephyr.pvgroup.intranet/flex/html5/login') AS `ZEPHYR_Link`,`pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`,`pm`.`Test_Coordinator` AS `Test_Coordinator`,`pm`.`Scrum_Master` AS `Scrum_Master`,IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`,IFNULL(`pm`.`CCB_STARTUP_Date`,`rm`.`LastRegistration`) AS `CCB_STARTUP_Date`,IFNULL(`pm`.`CCB_Ready4SPRINT_Date`,`rm`.`CCB_Ready4SPRINT`) AS `CCB_Ready4SPRINT_Date`,IFNULL(`pm`.`CCB_Ready4UAT_Date`,`rm`.`CCB_Ready4UAT`) AS `CCB_Ready4UAT_Date`,IFNULL(`pm`.`CCB_Ready4PROD_Date`,`rm`.`CCB_Ready4PROD`) AS `CCB_Ready4PROD_Date`,IFNULL(`pm`.`CCB_Closure_Date`,`rm`.`CCB_Closure`) AS `CCB_Closure_Date`,`pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`,`pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`,CONVERT( `pm`.`CCB_STARTUP_Remarks` USING UTF8) AS `CCB_STARTUP_Remarks`,CONVERT( `pm`.`Ready2StartTestRemarks` USING UTF8) AS `Ready2StartTestRemarks`,`pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`,CONVERT( `pm`.`CCBReady4SPRINT_Remarks` USING UTF8) AS `CCBReady4SPRINT_Remarks`,CONVERT( `pm`.`Ready4SprintTestRemarks` USING UTF8) AS `Ready4SprintTestRemarks`,`pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`,CONVERT( `pm`.`CCBReady4UAT_Remarks` USING UTF8) AS `CCBReady4UAT_Remarks`,CONVERT( `pm`.`Ready4UATTestRemarks` USING UTF8) AS `Ready4UATTestRemarks`,`rm`.`LastRegistration` AS `LastRegistrationDate`,`rm`.`UAT_StartDate` AS `UAT_START_DATE`,`rm`.`End_UAT` AS `UAT_END_DATE`,`rm`.`Freeze` AS `FREEZE_DATE`,`pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`,CONVERT( `pm`.`CCBReady4PROD_Remarks` USING UTF8) AS `CCBReady4PROD_Remarks`,CONVERT( `pm`.`Ready4ProdTestRemarks` USING UTF8) AS `Ready4ProdTestRemarks`,CONVERT( `pm`.`CCB_CLOSURE_Remarks` USING UTF8) AS `CCB_CLOSURE_Remarks`,`pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`,`pm`.`Project_Manager` AS `Project_Manager`,'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `unitTestReportPath`,'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `UAT_TestReportPath`,`tr`.`AUTOMATION_PATH` AS `automationReportPath`,`tr`.`PERFORMANCE_PATH` AS `performanceReportPath`,`tr`.`SECURITY_TEST_PATH` AS `securityReportPath`,`tr`.`IT_TEST_PATH` AS `IT_TestReportPath`,`tr`.`CCB_GATING_PATH` AS `CCB_GATING_PATH`,`tr`.`MASTER_TEST_PLAN_PATH` AS `MASTER_TEST_PLAN_PATH`,`pm`.`Jira_defect_projects` AS `Jira_defect_projects`,`pm`.`DetailedTestStatus` AS `DetailedTestStatus`,`pm`.`Test_Coordinator_email` AS `Test_Coordinator_email`,`pm`.`Project_Manager_email` AS `Project_Manager_email`,`pm`.`Scrum_Master_email` AS `Scrum_Master_email`,`pm`.`Delivery_Master_Lead_email` AS `Delivery_Master_Lead_email`,SUBSTRING_INDEX(`pm`.`Jira_defect_projects`, ',', 1) AS `JIRA_KEY`FROM((`release_milestones_status` `rm`LEFT JOIN `project_milestones_status` `pm` ON (CONVERT( `rm`.`ReleaseName` USING UTF8) = `pm`.`ReleaseName`))LEFT JOIN `v_test_reports` `tr` ON (`pm`.`ReleaseName` = `tr`.`ReleaseName` AND `pm`.`ProjectName` = `tr`.`ProjectName`));


/* Duplicate/Deleted Records handling for Requirements*/

CREATE TABLE `project_wise_requirements_upload_summary` ( `ID` int(11) NOT NULL AUTO_INCREMENT, `projectId` int(11) DEFAULT NULL,`releaseId` int(11) DEFAULT NULL,`insertCount` int(11) DEFAULT NULL,`updateCount` int(11) DEFAULT NULL,`lastRecordIdB4Insertion` int(120) DEFAULT NULL,`insertedOn` timestamp NULL DEFAULT current_timestamp(),`updatedOn` timestamp NULL DEFAULT NULL,`projectName` varchar(450) DEFAULT NULL,`releaseName` varchar(450) DEFAULT NULL,`totalRecordCount` int(11) DEFAULT NULL, PRIMARY KEY (`ID`),UNIQUE KEY `UNIQ` (`projectId`,`releaseId`)) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


/* https://gitlab.com/expleo-p-v/diary/-/issues/214  
Project Repositlry Link - REdirect with Defautl SITE URL  */

CREATE OR REPLACE VIEW `v_project_ccb_details` AS SELECT `rm`.`ReleaseName` AS `ReleaseName`, `rm`.`ReleaseDate` AS `ReleaseDate`, `pm`.`ProjectReference` AS `ProjectReference`, `pm`.`ProjectName` AS `ProjectName1`,CONCAT(`pm`.`ProjectReference`,' - ',`pm`.`ProjectName`) AS `ProjectName`,`pm`.`Test_Status` AS `test_status`,`pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`, `pm`.`Domain` AS `Domain`,`pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`,`pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link1`,CASE WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'file://%' THEN `pm`.`PROJECTREPOSITORY_Link` WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'http://%' THEN `pm`.`PROJECTREPOSITORY_Link`  WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'https://%' THEN `pm`.`PROJECTREPOSITORY_Link` ELSE 'http://weconnect.pvgroup.intranet/project/general/release/Lists/CCB%20Dashboard/All%20Items%20for%20real.aspx' END AS `PROJECTREPOSITORY_Link`, `pm`.`JIRA_Link` AS `JIRA_Link1`, CONCAT('https://jira.pvgroup.be/issues/?jql=project in (', `pm`.`Jira_defect_projects`, ') AND issuetype in (Defect,"Story Defect") AND status != Closed AND created > startOfMonth(-12) ORDER BY created DESC') AS `JIRA_Link`, IFNULL(`pm`.`ZEPHYR_Link`, 'http://zephyr.pvgroup.intranet/flex/html5/login') AS `ZEPHYR_Link`, `pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`, `pm`.`Test_Coordinator` AS `Test_Coordinator`, `pm`.`Scrum_Master` AS `Scrum_Master`, IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`, IFNULL(`pm`.`CCB_STARTUP_Date`, `rm`.`LastRegistration`) AS `CCB_STARTUP_Date`, IFNULL(`pm`.`CCB_Ready4SPRINT_Date`, `rm`.`CCB_Ready4SPRINT`) AS `CCB_Ready4SPRINT_Date`, IFNULL(`pm`.`CCB_Ready4UAT_Date`, `rm`.`CCB_Ready4UAT`) AS `CCB_Ready4UAT_Date`, IFNULL(`pm`.`CCB_Ready4PROD_Date`, `rm`.`CCB_Ready4PROD`) AS `CCB_Ready4PROD_Date`, IFNULL(`pm`.`CCB_Closure_Date`, `rm`.`CCB_Closure`) AS `CCB_Closure_Date`, `pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`, `pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`, CONVERT( `pm`.`CCB_STARTUP_Remarks` USING UTF8) AS `CCB_STARTUP_Remarks`, CONVERT( `pm`.`Ready2StartTestRemarks` USING UTF8) AS `Ready2StartTestRemarks`, `pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`, CONVERT( `pm`.`CCBReady4SPRINT_Remarks` USING UTF8) AS `CCBReady4SPRINT_Remarks`, CONVERT( `pm`.`Ready4SprintTestRemarks` USING UTF8) AS `Ready4SprintTestRemarks`, `pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`, CONVERT( `pm`.`CCBReady4UAT_Remarks` USING UTF8) AS `CCBReady4UAT_Remarks`, CONVERT( `pm`.`Ready4UATTestRemarks` USING UTF8) AS `Ready4UATTestRemarks`, `rm`.`LastRegistration` AS `LastRegistrationDate`, `rm`.`UAT_StartDate` AS `UAT_START_DATE`, `rm`.`End_UAT` AS `UAT_END_DATE`, `rm`.`Freeze` AS `FREEZE_DATE`, `pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`, CONVERT( `pm`.`CCBReady4PROD_Remarks` USING UTF8) AS `CCBReady4PROD_Remarks`, CONVERT( `pm`.`Ready4ProdTestRemarks` USING UTF8) AS `Ready4ProdTestRemarks`, CONVERT( `pm`.`CCB_CLOSURE_Remarks` USING UTF8) AS `CCB_CLOSURE_Remarks`, `pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`, `pm`.`Project_Manager` AS `Project_Manager`, 'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `unitTestReportPath`, 'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `UAT_TestReportPath`, `tr`.`AUTOMATION_PATH` AS `automationReportPath`, `tr`.`PERFORMANCE_PATH` AS `performanceReportPath`, `tr`.`SECURITY_TEST_PATH` AS `securityReportPath`, `tr`.`IT_TEST_PATH` AS `IT_TestReportPath`, `tr`.`CCB_GATING_PATH` AS `CCB_GATING_PATH`, `tr`.`MASTER_TEST_PLAN_PATH` AS `MASTER_TEST_PLAN_PATH`, `pm`.`Jira_defect_projects` AS `Jira_defect_projects`, `pm`.`DetailedTestStatus` AS `DetailedTestStatus`, `pm`.`Test_Coordinator_email` AS `Test_Coordinator_email`, `pm`.`Project_Manager_email` AS `Project_Manager_email`, `pm`.`Scrum_Master_email` AS `Scrum_Master_email`, `pm`.`Delivery_Master_Lead_email` AS `Delivery_Master_Lead_email`, SUBSTRING_INDEX(`pm`.`Jira_defect_projects`, ',', 1) AS `JIRA_KEY` FROM ((`release_milestones_status` `rm` LEFT JOIN `project_milestones_status` `pm` ON (CONVERT( `rm`.`ReleaseName` USING UTF8) = `pm`.`ReleaseName`)) LEFT JOIN `v_test_reports` `tr` ON (`pm`.`ReleaseName` = `tr`.`ReleaseName` AND `pm`.`ProjectName` = `tr`.`ProjectName`))   ;


/* https://gitlab.com/expleo-p-v/diary/-/issues/214  
Project Repositlry Link - Disable the link for invalid values  */

CREATE OR REPLACE VIEW `v_project_ccb_details` AS SELECT `rm`.`ReleaseName` AS `ReleaseName`, `rm`.`ReleaseDate` AS `ReleaseDate`, `pm`.`ProjectReference` AS `ProjectReference`, `pm`.`ProjectName` AS `ProjectName1`,CONCAT(`pm`.`ProjectReference`,' - ',`pm`.`ProjectName`) AS `ProjectName`,`pm`.`Test_Status` AS `test_status`,`pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`, `pm`.`Domain` AS `Domain`,`pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`,`pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link1`,CASE WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'file://%' THEN `pm`.`PROJECTREPOSITORY_Link` WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'http://%' THEN `pm`.`PROJECTREPOSITORY_Link`  WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'https://%' THEN `pm`.`PROJECTREPOSITORY_Link` ELSE NULL END AS `PROJECTREPOSITORY_Link`, `pm`.`JIRA_Link` AS `JIRA_Link1`, CONCAT('https://jira.pvgroup.be/issues/?jql=project in (', `pm`.`Jira_defect_projects`, ') AND issuetype in (Defect,"Story Defect") AND status != Closed AND created > startOfMonth(-12) ORDER BY created DESC') AS `JIRA_Link`, IFNULL(`pm`.`ZEPHYR_Link`, 'http://zephyr.pvgroup.intranet/flex/html5/login') AS `ZEPHYR_Link`, `pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`, `pm`.`Test_Coordinator` AS `Test_Coordinator`, `pm`.`Scrum_Master` AS `Scrum_Master`, IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`, IFNULL(`pm`.`CCB_STARTUP_Date`, `rm`.`LastRegistration`) AS `CCB_STARTUP_Date`, IFNULL(`pm`.`CCB_Ready4SPRINT_Date`, `rm`.`CCB_Ready4SPRINT`) AS `CCB_Ready4SPRINT_Date`, IFNULL(`pm`.`CCB_Ready4UAT_Date`, `rm`.`CCB_Ready4UAT`) AS `CCB_Ready4UAT_Date`, IFNULL(`pm`.`CCB_Ready4PROD_Date`, `rm`.`CCB_Ready4PROD`) AS `CCB_Ready4PROD_Date`, IFNULL(`pm`.`CCB_Closure_Date`, `rm`.`CCB_Closure`) AS `CCB_Closure_Date`, `pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`, `pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`, CONVERT( `pm`.`CCB_STARTUP_Remarks` USING UTF8) AS `CCB_STARTUP_Remarks`, CONVERT( `pm`.`Ready2StartTestRemarks` USING UTF8) AS `Ready2StartTestRemarks`, `pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`, CONVERT( `pm`.`CCBReady4SPRINT_Remarks` USING UTF8) AS `CCBReady4SPRINT_Remarks`, CONVERT( `pm`.`Ready4SprintTestRemarks` USING UTF8) AS `Ready4SprintTestRemarks`, `pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`, CONVERT( `pm`.`CCBReady4UAT_Remarks` USING UTF8) AS `CCBReady4UAT_Remarks`, CONVERT( `pm`.`Ready4UATTestRemarks` USING UTF8) AS `Ready4UATTestRemarks`, `rm`.`LastRegistration` AS `LastRegistrationDate`, `rm`.`UAT_StartDate` AS `UAT_START_DATE`, `rm`.`End_UAT` AS `UAT_END_DATE`, `rm`.`Freeze` AS `FREEZE_DATE`, `pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`, CONVERT( `pm`.`CCBReady4PROD_Remarks` USING UTF8) AS `CCBReady4PROD_Remarks`, CONVERT( `pm`.`Ready4ProdTestRemarks` USING UTF8) AS `Ready4ProdTestRemarks`, CONVERT( `pm`.`CCB_CLOSURE_Remarks` USING UTF8) AS `CCB_CLOSURE_Remarks`, `pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`, `pm`.`Project_Manager` AS `Project_Manager`, 'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `unitTestReportPath`, 'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `UAT_TestReportPath`, `tr`.`AUTOMATION_PATH` AS `automationReportPath`, `tr`.`PERFORMANCE_PATH` AS `performanceReportPath`, `tr`.`SECURITY_TEST_PATH` AS `securityReportPath`, `tr`.`IT_TEST_PATH` AS `IT_TestReportPath`, `tr`.`CCB_GATING_PATH` AS `CCB_GATING_PATH`, `tr`.`MASTER_TEST_PLAN_PATH` AS `MASTER_TEST_PLAN_PATH`, `pm`.`Jira_defect_projects` AS `Jira_defect_projects`, `pm`.`DetailedTestStatus` AS `DetailedTestStatus`, `pm`.`Test_Coordinator_email` AS `Test_Coordinator_email`, `pm`.`Project_Manager_email` AS `Project_Manager_email`, `pm`.`Scrum_Master_email` AS `Scrum_Master_email`, `pm`.`Delivery_Master_Lead_email` AS `Delivery_Master_Lead_email`, SUBSTRING_INDEX(`pm`.`Jira_defect_projects`, ',', 1) AS `JIRA_KEY` FROM ((`release_milestones_status` `rm` LEFT JOIN `project_milestones_status` `pm` ON (CONVERT( `rm`.`ReleaseName` USING UTF8) = `pm`.`ReleaseName`)) LEFT JOIN `v_test_reports` `tr` ON (`pm`.`ReleaseName` = `tr`.`ReleaseName` AND `pm`.`ProjectName` = `tr`.`ProjectName`))   ;



/**  UAT & UNIT TEST - External Report Integration  **/

CREATE  OR REPLACE VIEW `v_test_reports` AS     SELECT  `test_reports`.`ReleaseName` AS `ReleaseName`, `test_reports`.`ProjectName` AS `PROJECT`, TRIM(SUBSTRING_INDEX(`test_reports`.`ProjectName`, '-', 1)) AS `ProjectReference`, TRIM(SUBSTR(`test_reports`.`ProjectName`, LOCATE('-', `test_reports`.`ProjectName`) + 1)) AS `ProjectName`, MAX(CASE WHEN `test_reports`.`TypeOfReport` = 'Test Status Report' THEN `test_reports`.`Title` END) AS `IT_TEST`, MAX(CASE WHEN `test_reports`.`TypeOfReport` = 'Test Status Report' THEN `test_reports`.`FileServerRelativeURL` END) AS `IT_TEST_PATH`, MAX(CASE  WHEN `test_reports`.`TypeOfReport` = 'CCB Gating Test Report' THEN `test_reports`.`Title` END) AS `CCB_GATING`, MAX(CASE  WHEN `test_reports`.`TypeOfReport` = 'CCB Gating Test Report' THEN `test_reports`.`FileServerRelativeURL` END) AS `CCB_GATING_PATH`,   MAX(CASE WHEN `test_reports`.`TypeOfReport` = 'Test Automation Report' THEN `test_reports`.`Title` END) AS `AUTOMATION`, MAX(CASE WHEN `test_reports`.`TypeOfReport` = 'Test Automation Report' THEN `test_reports`.`FileServerRelativeURL` END) AS `AUTOMATION_PATH`, MAX(CASE WHEN `test_reports`.`TypeOfReport` LIKE '%Performance%' THEN `test_reports`.`Title` END) AS `PERFORMANCE`, MAX(CASE WHEN `test_reports`.`TypeOfReport` LIKE '%Performance%' THEN `test_reports`.`FileServerRelativeURL` END) AS `PERFORMANCE_PATH`, MAX(CASE WHEN `test_reports`.`TypeOfReport` = 'Security Test Report' THEN `test_reports`.`Title` END) AS `SECURITY_TEST`, MAX(CASE WHEN `test_reports`.`TypeOfReport` = 'Security Test Report' THEN `test_reports`.`FileServerRelativeURL` END) AS `SECURITY_TEST_PATH`, MAX(CASE WHEN `test_reports`.`TypeOfReport` = 'Master Test Plan' THEN `test_reports`.`Title`  END) AS `MASTER_TEST_PLAN`, MAX(CASE WHEN `test_reports`.`TypeOfReport` = 'Master Test Plan' THEN `test_reports`.`FileServerRelativeURL` END) AS `MASTER_TEST_PLAN_PATH`, MAX(CASE WHEN `test_reports`.`TypeOfReport` LIKE '%Unit%' THEN `test_reports`.`Title` END) AS `UNIT_TEST`,  MAX(CASE WHEN `test_reports`.`TypeOfReport` LIKE '%Unit%' THEN `test_reports`.`FileServerRelativeURL` END) AS `UNIT_TEST_PATH`, MAX(CASE WHEN `test_reports`.`TypeOfReport` LIKE '%Acceptance%' THEN `test_reports`.`Title` END) AS `UAT_TEST`, MAX(CASE WHEN `test_reports`.`TypeOfReport` LIKE '%Acceptance%' THEN `test_reports`.`FileServerRelativeURL` END) AS `UAT_TEST_PATH` FROM  `test_reports`  GROUP BY `test_reports`.`ReleaseName` , `test_reports`.`ProjectName` ORDER BY `test_reports`.`Modified` DESC ;

CREATE OR REPLACE VIEW `v_project_ccb_details` AS  SELECT  `rm`.`ReleaseName` AS `ReleaseName`, `rm`.`ReleaseDate` AS `ReleaseDate`, `pm`.`ProjectReference` AS `ProjectReference`, `pm`.`ProjectName` AS `ProjectName1`, CONCAT(`pm`.`ProjectReference`, ' - ', `pm`.`ProjectName`) AS `ProjectName`, `pm`.`Test_Status` AS `test_status`, `pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`, `pm`.`Domain` AS `Domain`, `pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`, `pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link1`, CASE WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'file://%' THEN `pm`.`PROJECTREPOSITORY_Link` WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'http://%' THEN `pm`.`PROJECTREPOSITORY_Link` WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'https://%' THEN `pm`.`PROJECTREPOSITORY_Link` ELSE NULL END AS `PROJECTREPOSITORY_Link`, `pm`.`JIRA_Link` AS `JIRA_Link1`, CONCAT('https://jira.pvgroup.be/issues/?jql=project in (', `pm`.`Jira_defect_projects`, ') AND issuetype in (Defect,"Story Defect") AND status != Closed AND created > startOfMonth(-12) ORDER BY created DESC') AS `JIRA_Link`, IFNULL(`pm`.`ZEPHYR_Link`, 'http://zephyr.pvgroup.intranet/flex/html5/login') AS `ZEPHYR_Link`, `pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`, `pm`.`Test_Coordinator` AS `Test_Coordinator`, `pm`.`Scrum_Master` AS `Scrum_Master`, IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`, IFNULL(`pm`.`CCB_STARTUP_Date`, `rm`.`LastRegistration`) AS `CCB_STARTUP_Date`, IFNULL(`pm`.`CCB_Ready4SPRINT_Date`, `rm`.`CCB_Ready4SPRINT`) AS `CCB_Ready4SPRINT_Date`, IFNULL(`pm`.`CCB_Ready4UAT_Date`, `rm`.`CCB_Ready4UAT`) AS `CCB_Ready4UAT_Date`, IFNULL(`pm`.`CCB_Ready4PROD_Date`, `rm`.`CCB_Ready4PROD`) AS `CCB_Ready4PROD_Date`,IFNULL(`pm`.`CCB_Closure_Date`, `rm`.`CCB_Closure`) AS `CCB_Closure_Date`, `pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`, `pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`, CONVERT( `pm`.`CCB_STARTUP_Remarks` USING UTF8) AS `CCB_STARTUP_Remarks`, CONVERT( `pm`.`Ready2StartTestRemarks` USING UTF8) AS `Ready2StartTestRemarks`, `pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`, CONVERT( `pm`.`CCBReady4SPRINT_Remarks` USING UTF8) AS `CCBReady4SPRINT_Remarks`, CONVERT( `pm`.`Ready4SprintTestRemarks` USING UTF8) AS `Ready4SprintTestRemarks`, `pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`, CONVERT( `pm`.`CCBReady4UAT_Remarks` USING UTF8) AS `CCBReady4UAT_Remarks`, CONVERT( `pm`.`Ready4UATTestRemarks` USING UTF8) AS `Ready4UATTestRemarks`, `rm`.`LastRegistration` AS `LastRegistrationDate`, `rm`.`UAT_StartDate` AS `UAT_START_DATE`, `rm`.`End_UAT` AS `UAT_END_DATE`, `rm`.`Freeze` AS `FREEZE_DATE`, `pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`, CONVERT( `pm`.`CCBReady4PROD_Remarks` USING UTF8) AS `CCBReady4PROD_Remarks`, CONVERT( `pm`.`Ready4ProdTestRemarks` USING UTF8) AS `Ready4ProdTestRemarks`, CONVERT( `pm`.`CCB_CLOSURE_Remarks` USING UTF8) AS `CCB_CLOSURE_Remarks`, `pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`, `pm`.`Project_Manager` AS `Project_Manager`, `tr`.`UNIT_TEST_PATH` AS `unitTestReportPath`, `tr`.`UAT_TEST_PATH` AS `UAT_TestReportPath`, `tr`.`AUTOMATION_PATH` AS `automationReportPath`, `tr`.`PERFORMANCE_PATH` AS `performanceReportPath`, `tr`.`SECURITY_TEST_PATH` AS `securityReportPath`, `tr`.`IT_TEST_PATH` AS `IT_TestReportPath`,`tr`.`CCB_GATING_PATH` AS `CCB_GATING_PATH`, `tr`.`MASTER_TEST_PLAN_PATH` AS `MASTER_TEST_PLAN_PATH`, `pm`.`Jira_defect_projects` AS `Jira_defect_projects`, `pm`.`DetailedTestStatus` AS `DetailedTestStatus`,`pm`.`Test_Coordinator_email` AS `Test_Coordinator_email`,`pm`.`Project_Manager_email` AS `Project_Manager_email`, `pm`.`Scrum_Master_email` AS `Scrum_Master_email`, `pm`.`Delivery_Master_Lead_email` AS `Delivery_Master_Lead_email`,  SUBSTRING_INDEX(`pm`.`Jira_defect_projects`, ',', 1) AS `JIRA_KEY` FROM ((`release_milestones_status` `rm` LEFT JOIN `project_milestones_status` `pm` ON (CONVERT( `rm`.`ReleaseName` USING UTF8) = `pm`.`ReleaseName`)) LEFT JOIN `v_test_reports` `tr` ON (`pm`.`ReleaseName` = `tr`.`ReleaseName` AND `pm`.`ProjectName` = `tr`.`ProjectName`));


/** #219 Adding Summary to User Story View  **/

CREATE OR REPLACE VIEW `v_user_story` AS   SELECT  `d`.`userStoryID` AS `userStoryID`,`d`.`projectKey` AS `projectKey`,`d`.`projectName` AS `projectName`,`d`.`status` AS `storyStatus`, `d`.`summary` AS `summary`,IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`, `pm`.`ProjectReference` AS `ProjectReference`,  IFNULL(`d`.`resolution`, 'Un-Defined') AS `resolution`,  IFNULL(`d`.`priority`, 'Un-Defined') AS `priority`,  IFNULL(`d`.`assigneeName`, 'UnAssigned') AS `assigneeName`,  IFNULL(`d`.`severity`, 'Un-Defined') AS `severity`,   IFNULL(`d`.`component`, 'No Components') AS `component`, IFNULL(`d`.`sprintId`, 'Un-Defined') AS `sprintId`, IFNULL(`d`.`reporter`, 'UnAssigned') AS `reporter`,IFNULL(`d`.`epicLink`, 'Un-Defined') AS `epicLink`, IFNULL(`d`.`fixVersions`, 'Un-Defined') AS `fixVersions`,  `pm`.`ReleaseName` AS `ReleaseName`,  IFNULL(`d`.`issueType`, 'Un-Defined') AS `issueType`,  REPLACE(`pm`.`JIRA_KEY`, ' ', '') AS `JIRA_KEY` FROM (`user_story_details` `d` JOIN `v_project_wise_defect_keys` `pm` ON (`d`.`projectKey` = REPLACE(`pm`.`JIRA_KEY`, ' ', '') AND `d`.`fixVersions` = `pm`.`ReleaseName`));

/*Update View*/

ALTER TABLE `test_reports`
ADD COLUMN `summary` LONGTEXT NULL DEFAULT NULL AFTER `FileName`;

CREATE OR REPLACE VIEW `v_user_story` AS   SELECT  `d`.`userStoryID` AS `userStoryID`,`d`.`storyKey` AS `storyKey`,`d`.`projectKey` AS `projectKey`,`d`.`projectName` AS `projectName`,`d`.`status` AS `storyStatus`, `d`.`summary` AS `summary`,IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`, `pm`.`ProjectReference` AS `ProjectReference`,  IFNULL(`d`.`resolution`, 'Un-Defined') AS `resolution`,  IFNULL(`d`.`priority`, 'Un-Defined') AS `priority`,  IFNULL(`d`.`assigneeName`, 'UnAssigned') AS `assigneeName`,  IFNULL(`d`.`severity`, 'Un-Defined') AS `severity`,   IFNULL(`d`.`component`, 'No Components') AS `component`, IFNULL(`d`.`sprintId`, 'Un-Defined') AS `sprintId`, IFNULL(`d`.`reporter`, 'UnAssigned') AS `reporter`,IFNULL(`d`.`epicLink`, 'Un-Defined') AS `epicLink`, IFNULL(`d`.`fixVersions`, 'Un-Defined') AS `fixVersions`,  `pm`.`ReleaseName` AS `ReleaseName`,  IFNULL(`d`.`issueType`, 'Un-Defined') AS `issueType`,  REPLACE(`pm`.`JIRA_KEY`, ' ', '') AS `JIRA_KEY` FROM (`user_story_details` `d` JOIN `v_project_wise_defect_keys` `pm` ON (`d`.`projectKey` = REPLACE(`pm`.`JIRA_KEY`, ' ', '') AND `d`.`fixVersions` = `pm`.`ReleaseName`));

/*User stories updated view*/

CREATE OR REPLACE VIEW `v_user_story` AS   SELECT  `d`.`userStoryID` AS `userStoryID`,`d`.`storyKey` AS `storyKey`,`d`.`projectKey` AS `projectKey`,`d`.`projectName` AS `projectName`,  CASE WHEN `d`.`status` LIKE '%Backlog%' THEN 'Backlog'  WHEN `d`.`status` LIKE '%BA in progress%' THEN 'Backlog'  WHEN `d`.`status` LIKE '%Open%' THEN 'Backlog'  WHEN `d`.`status` LIKE '%BA to validate%' THEN 'Analysis'  WHEN `d`.`status` LIKE '%FA in progress%' THEN 'Analysis'  WHEN `d`.`status` LIKE '%FA to validate%' THEN 'Analysis'  WHEN `d`.`status` LIKE '%TA in progress%' THEN 'Analysis' WHEN `d`.`status` LIKE '%TA to validate%' THEN 'Analysis'  WHEN `d`.`status` LIKE '%To groom%' THEN 'Analysis' WHEN `d`.`status` LIKE '%Sprintable%' THEN 'Analysis' WHEN `d`.`status` LIKE '%In Inception%' THEN 'Analysis' WHEN `d`.`status` LIKE '%To validate%' THEN 'Analysis' WHEN `d`.`status` LIKE '%Ready%' THEN 'Analysis'  WHEN `d`.`status` LIKE '%In progress%' THEN 'Development'  WHEN `d`.`status` LIKE 'In review' THEN 'Development' WHEN `d`.`status` LIKE 'To promote' THEN 'Development' WHEN `d`.`status` LIKE 'In test' THEN 'Build/Test' WHEN `d`.`status` LIKE '%PO To validate%' THEN 'Build/Test'  WHEN `d`.`status` LIKE '%Resolved%' THEN 'Build/Test' WHEN `d`.`status` LIKE '%To Verify%' THEN 'Build/Test' WHEN `d`.`status` LIKE '%To review%' THEN 'Build/Test' WHEN `d`.`status` LIKE 'Ready for uat' THEN 'Closed' WHEN `d`.`status` LIKE '%Closed%' THEN 'Closed' WHEN `d`.`status` LIKE 'Reopened' THEN 'Reopened'	 END AS `storyStatus`, `d`.`summary` AS `summary`,IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`, `pm`.`ProjectReference` AS `ProjectReference`,  IFNULL(`d`.`resolution`, 'Un-Defined') AS `resolution`,  IFNULL(`d`.`priority`, 'Un-Defined') AS `priority`,  IFNULL(`d`.`assigneeName`, 'UnAssigned') AS `assigneeName`,  IFNULL(`d`.`severity`, 'Un-Defined') AS `severity`,   IFNULL(`d`.`component`, 'No Components') AS `component`, IFNULL(`d`.`sprintId`, 'Un-Defined') AS `sprintId`, IFNULL(`d`.`reporter`, 'UnAssigned') AS `reporter`,IFNULL(`d`.`epicLink`, 'Un-Defined') AS `epicLink`, IFNULL(`d`.`fixVersions`, 'Un-Defined') AS `fixVersions`,  `pm`.`ReleaseName` AS `ReleaseName`,  IFNULL(`d`.`issueType`, 'Un-Defined') AS `issueType`,  REPLACE(`pm`.`JIRA_KEY`, ' ', '') AS `JIRA_KEY` FROM (`user_story_details` `d` JOIN `v_project_wise_defect_keys` `pm` ON (`d`.`projectKey` = REPLACE(`pm`.`JIRA_KEY`, ' ', '') AND `d`.`fixVersions` = `pm`.`ReleaseName`));

/**  Project Milestones View updated - To included User Story link in Project Page View  */

CREATE OR REPLACE VIEW `v_project_ccb_details` AS SELECT `rm`.`ReleaseName` AS `ReleaseName`, `rm`.`ReleaseDate` AS `ReleaseDate`, `pm`.`ProjectReference` AS `ProjectReference`, `pm`.`ProjectName` AS `ProjectName1`, CONCAT(`pm`.`ProjectReference`,' - ',`pm`.`ProjectName`) AS `ProjectName`,`pm`.`Test_Status` AS `test_status`, `pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`, `pm`.`Domain` AS `Domain`, `pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`, `pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link1`,  CASE WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'file://%' THEN `pm`.`PROJECTREPOSITORY_Link`  WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'http://%' THEN `pm`.`PROJECTREPOSITORY_Link` WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'https://%' THEN `pm`.`PROJECTREPOSITORY_Link`  ELSE NULL  END AS `PROJECTREPOSITORY_Link`,  `pm`.`JIRA_Link` AS `JIRA_Link1`,  CONCAT('https://jira.pvgroup.be/issues/?jql=project in (',  `pm`.`Jira_defect_projects`, ') AND issuetype in (Defect,"Story Defect") AND status != Closed AND created > startOfMonth(-12) ORDER BY created DESC') AS `JIRA_Link`, IFNULL(`pm`.`ZEPHYR_Link`, 'http://zephyr.pvgroup.intranet/flex/html5/login') AS `ZEPHYR_Link`,  CONCAT('https://jira.pvgroup.be/issues/?jql=project in (',  `pm`.`Jira_defect_projects`, ') AND issuetype in ("Story") AND fixVersion in (',  `rm`.`ReleaseName`,  ')  ORDER BY created DESC') AS `UserStory_Link`,  `pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`,  `pm`.`Test_Coordinator` AS `Test_Coordinator`,  `pm`.`Scrum_Master` AS `Scrum_Master`,  IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`,  IFNULL(`pm`.`CCB_STARTUP_Date`,  `rm`.`LastRegistration`) AS `CCB_STARTUP_Date`, IFNULL(`pm`.`CCB_Ready4SPRINT_Date`, `rm`.`CCB_Ready4SPRINT`) AS `CCB_Ready4SPRINT_Date`, IFNULL(`pm`.`CCB_Ready4UAT_Date`, `rm`.`CCB_Ready4UAT`) AS `CCB_Ready4UAT_Date`, IFNULL(`pm`.`CCB_Ready4PROD_Date`, `rm`.`CCB_Ready4PROD`) AS `CCB_Ready4PROD_Date`, IFNULL(`pm`.`CCB_Closure_Date`, `rm`.`CCB_Closure`) AS `CCB_Closure_Date`, `pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`, `pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`, CONVERT( `pm`.`CCB_STARTUP_Remarks` USING UTF8) AS `CCB_STARTUP_Remarks`, CONVERT( `pm`.`Ready2StartTestRemarks` USING UTF8) AS `Ready2StartTestRemarks`, `pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`, CONVERT( `pm`.`CCBReady4SPRINT_Remarks` USING UTF8) AS `CCBReady4SPRINT_Remarks`, CONVERT( `pm`.`Ready4SprintTestRemarks` USING UTF8) AS `Ready4SprintTestRemarks`, `pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`, CONVERT( `pm`.`CCBReady4UAT_Remarks` USING UTF8) AS `CCBReady4UAT_Remarks`, CONVERT( `pm`.`Ready4UATTestRemarks` USING UTF8) AS `Ready4UATTestRemarks`, `rm`.`LastRegistration` AS `LastRegistrationDate`, `rm`.`UAT_StartDate` AS `UAT_START_DATE`, `rm`.`End_UAT` AS `UAT_END_DATE`, `rm`.`Freeze` AS `FREEZE_DATE`, `pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`, CONVERT( `pm`.`CCBReady4PROD_Remarks` USING UTF8) AS `CCBReady4PROD_Remarks`, CONVERT( `pm`.`Ready4ProdTestRemarks` USING UTF8) AS `Ready4ProdTestRemarks`, CONVERT( `pm`.`CCB_CLOSURE_Remarks` USING UTF8) AS `CCB_CLOSURE_Remarks`, `pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`, `pm`.`Project_Manager` AS `Project_Manager`, `tr`.`UNIT_TEST_PATH` AS `unitTestReportPath`, `tr`.`UAT_TEST_PATH` AS `UAT_TestReportPath`, `tr`.`AUTOMATION_PATH` AS `automationReportPath`, `tr`.`PERFORMANCE_PATH` AS `performanceReportPath`, `tr`.`SECURITY_TEST_PATH` AS `securityReportPath`, `tr`.`IT_TEST_PATH` AS `IT_TestReportPath`, `tr`.`CCB_GATING_PATH` AS `CCB_GATING_PATH`, `tr`.`MASTER_TEST_PLAN_PATH` AS `MASTER_TEST_PLAN_PATH`, `pm`.`Jira_defect_projects` AS `Jira_defect_projects`, `pm`.`DetailedTestStatus` AS `DetailedTestStatus`, `pm`.`Test_Coordinator_email` AS `Test_Coordinator_email`,  `pm`.`Project_Manager_email` AS `Project_Manager_email`, `pm`.`Scrum_Master_email` AS `Scrum_Master_email`, `pm`.`Delivery_Master_Lead_email` AS `Delivery_Master_Lead_email`, SUBSTRING_INDEX(`pm`.`Jira_defect_projects`, ',', 1) AS `JIRA_KEY` FROM ((`release_milestones_status` `rm` LEFT JOIN `project_milestones_status` `pm` ON (CONVERT( `rm`.`ReleaseName` USING UTF8) = `pm`.`ReleaseName`)) LEFT JOIN `v_test_reports` `tr` ON (`pm`.`ReleaseName` = `tr`.`ReleaseName` AND `pm`.`ProjectName` = `tr`.`ProjectName`));

/** EXternalTest Reports - JOIN where clause condition changed to look forproject reference insteas of project name */


CREATE OR REPLACE VIEW `v_test_reports` AS SELECT `test_reports`.`ReleaseName` AS `ReleaseName`, `test_reports`.`ProjectName` AS `PROJECT`, TRIM(SUBSTRING_INDEX(`test_reports`.`ProjectName`, '-', 1)) AS `ProjectReference`, TRIM(SUBSTR(`test_reports`.`ProjectName`, LOCATE('-', `test_reports`.`ProjectName`) + 1)) AS `ProjectName`, MAX(CASE WHEN `test_reports`.`TypeOfReport` = 'Test Status Report' THEN `test_reports`.`Title` END) AS `IT_TEST`, MAX(CASE WHEN `test_reports`.`TypeOfReport` = 'Test Status Report' THEN `test_reports`.`FileServerRelativeURL` END) AS `IT_TEST_PATH`, MAX(CASE WHEN `test_reports`.`TypeOfReport` = 'CCB Gating Test Report' THEN `test_reports`.`Title` END) AS `CCB_GATING`, MAX(CASE WHEN `test_reports`.`TypeOfReport` = 'CCB Gating Test Report' THEN `test_reports`.`FileServerRelativeURL` END) AS `CCB_GATING_PATH`, MAX(CASE WHEN `test_reports`.`TypeOfReport` = 'Test Automation Report' THEN `test_reports`.`Title` END) AS `AUTOMATION`, MAX(CASE WHEN `test_reports`.`TypeOfReport` = 'Test Automation Report' THEN `test_reports`.`FileServerRelativeURL` END) AS `AUTOMATION_PATH`, MAX(CASE WHEN `test_reports`.`TypeOfReport` LIKE '%Performance%' THEN `test_reports`.`Title` END) AS `PERFORMANCE`, MAX(CASE WHEN `test_reports`.`TypeOfReport` LIKE '%Performance%' THEN `test_reports`.`FileServerRelativeURL` END) AS `PERFORMANCE_PATH`, MAX(CASE WHEN `test_reports`.`TypeOfReport` = 'Security Test Report' THEN `test_reports`.`Title` END) AS `SECURITY_TEST`, MAX(CASE WHEN `test_reports`.`TypeOfReport` = 'Security Test Report' THEN `test_reports`.`FileServerRelativeURL` END) AS `SECURITY_TEST_PATH`, MAX(CASE WHEN `test_reports`.`TypeOfReport` = 'Master Test Plan' THEN `test_reports`.`Title` END) AS `MASTER_TEST_PLAN`, MAX(CASE WHEN `test_reports`.`TypeOfReport` = 'Master Test Plan' THEN `test_reports`.`FileServerRelativeURL` END) AS `MASTER_TEST_PLAN_PATH`, MAX(CASE WHEN `test_reports`.`TypeOfReport` LIKE '%Unit%' THEN `test_reports`.`Title` END) AS `UNIT_TEST`, MAX(CASE WHEN `test_reports`.`TypeOfReport` LIKE '%Unit%' THEN `test_reports`.`FileServerRelativeURL` END) AS `UNIT_TEST_PATH`, MAX(CASE WHEN `test_reports`.`TypeOfReport` LIKE '%Acceptance%' THEN `test_reports`.`Title` END) AS `UAT_TEST`, MAX(CASE WHEN `test_reports`.`TypeOfReport` LIKE '%Acceptance%' THEN `test_reports`.`FileServerRelativeURL` END) AS `UAT_TEST_PATH` FROM `test_reports` GROUP BY `test_reports`.`ReleaseName` , TRIM(SUBSTRING_INDEX(`test_reports`.`ProjectName`, '-', 1)) ORDER BY `test_reports`.`Modified` DESC ;

CREATE OR REPLACE VIEW `v_project_ccb_details` AS SELECT `rm`.`ReleaseName` AS `ReleaseName`, `rm`.`ReleaseDate` AS `ReleaseDate`, `pm`.`ProjectReference` AS `ProjectReference`, `pm`.`ProjectName` AS `ProjectName1`, CONCAT(`pm`.`ProjectReference`,' - ',`pm`.`ProjectName`) AS `ProjectName`,`pm`.`Test_Status` AS `test_status`, `pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`, `pm`.`Domain` AS `Domain`, `pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`, `pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link1`,  CASE WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'file://%' THEN `pm`.`PROJECTREPOSITORY_Link`  WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'http://%' THEN `pm`.`PROJECTREPOSITORY_Link` WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'https://%' THEN `pm`.`PROJECTREPOSITORY_Link`  ELSE NULL  END AS `PROJECTREPOSITORY_Link`,  `pm`.`JIRA_Link` AS `JIRA_Link1`,  CONCAT('https://jira.pvgroup.be/issues/?jql=project in (',  `pm`.`Jira_defect_projects`, ') AND issuetype in (Defect,"Story Defect") AND status != Closed AND created > startOfMonth(-12) ORDER BY created DESC') AS `JIRA_Link`, IFNULL(`pm`.`ZEPHYR_Link`, 'http://zephyr.pvgroup.intranet/flex/html5/login') AS `ZEPHYR_Link`,  CONCAT('https://jira.pvgroup.be/issues/?jql=project in (',  `pm`.`Jira_defect_projects`, ') AND issuetype in ("Story") AND fixVersion in (',  `rm`.`ReleaseName`,  ')  ORDER BY created DESC') AS `UserStory_Link`,  `pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`,  `pm`.`Test_Coordinator` AS `Test_Coordinator`,  `pm`.`Scrum_Master` AS `Scrum_Master`,  IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`,  IFNULL(`pm`.`CCB_STARTUP_Date`,  `rm`.`LastRegistration`) AS `CCB_STARTUP_Date`, IFNULL(`pm`.`CCB_Ready4SPRINT_Date`, `rm`.`CCB_Ready4SPRINT`) AS `CCB_Ready4SPRINT_Date`, IFNULL(`pm`.`CCB_Ready4UAT_Date`, `rm`.`CCB_Ready4UAT`) AS `CCB_Ready4UAT_Date`, IFNULL(`pm`.`CCB_Ready4PROD_Date`, `rm`.`CCB_Ready4PROD`) AS `CCB_Ready4PROD_Date`, IFNULL(`pm`.`CCB_Closure_Date`, `rm`.`CCB_Closure`) AS `CCB_Closure_Date`, `pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`, `pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`, CONVERT( `pm`.`CCB_STARTUP_Remarks` USING UTF8) AS `CCB_STARTUP_Remarks`, CONVERT( `pm`.`Ready2StartTestRemarks` USING UTF8) AS `Ready2StartTestRemarks`, `pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`, CONVERT( `pm`.`CCBReady4SPRINT_Remarks` USING UTF8) AS `CCBReady4SPRINT_Remarks`, CONVERT( `pm`.`Ready4SprintTestRemarks` USING UTF8) AS `Ready4SprintTestRemarks`, `pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`, CONVERT( `pm`.`CCBReady4UAT_Remarks` USING UTF8) AS `CCBReady4UAT_Remarks`, CONVERT( `pm`.`Ready4UATTestRemarks` USING UTF8) AS `Ready4UATTestRemarks`, `rm`.`LastRegistration` AS `LastRegistrationDate`, `rm`.`UAT_StartDate` AS `UAT_START_DATE`, `rm`.`End_UAT` AS `UAT_END_DATE`, `rm`.`Freeze` AS `FREEZE_DATE`, `pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`, CONVERT( `pm`.`CCBReady4PROD_Remarks` USING UTF8) AS `CCBReady4PROD_Remarks`, CONVERT( `pm`.`Ready4ProdTestRemarks` USING UTF8) AS `Ready4ProdTestRemarks`, CONVERT( `pm`.`CCB_CLOSURE_Remarks` USING UTF8) AS `CCB_CLOSURE_Remarks`, `pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`, `pm`.`Project_Manager` AS `Project_Manager`, `tr`.`UNIT_TEST_PATH` AS `unitTestReportPath`, `tr`.`UAT_TEST_PATH` AS `UAT_TestReportPath`, `tr`.`AUTOMATION_PATH` AS `automationReportPath`, `tr`.`PERFORMANCE_PATH` AS `performanceReportPath`, `tr`.`SECURITY_TEST_PATH` AS `securityReportPath`, `tr`.`IT_TEST_PATH` AS `IT_TestReportPath`, `tr`.`CCB_GATING_PATH` AS `CCB_GATING_PATH`, `tr`.`MASTER_TEST_PLAN_PATH` AS `MASTER_TEST_PLAN_PATH`, `pm`.`Jira_defect_projects` AS `Jira_defect_projects`, `pm`.`DetailedTestStatus` AS `DetailedTestStatus`, `pm`.`Test_Coordinator_email` AS `Test_Coordinator_email`,  `pm`.`Project_Manager_email` AS `Project_Manager_email`, `pm`.`Scrum_Master_email` AS `Scrum_Master_email`, `pm`.`Delivery_Master_Lead_email` AS `Delivery_Master_Lead_email`, SUBSTRING_INDEX(`pm`.`Jira_defect_projects`, ',', 1) AS `JIRA_KEY` FROM ((`release_milestones_status` `rm` LEFT JOIN `project_milestones_status` `pm` ON (CONVERT( `rm`.`ReleaseName` USING UTF8) = `pm`.`ReleaseName`)) LEFT JOIN `v_test_reports` `tr` ON (`pm`.`ReleaseName` = `tr`.`ReleaseName` AND `pm`.`ProjectReference` = `tr`.`ProjectReference`));

/** Release Milestones Interchange   https://gitlab.com/expleo-p-v/diary/-/issues/225 */

CREATE or REPLACE view v_release_milestones_status as SELECT REPLACE(TRIM(SUBSTRING_INDEX(`pm`.`ReleaseName`, '-', 1)),'+','')  as ReleaseName, `pm`.`ReleaseName` as ReleaseMoment , `rm`.`ReleaseName` AS `RM_ReleaseName`, IFNULL(`rm`.`ReleaseDate` ,'[No Data]') AS `ReleaseDate`, `pm`.`ProjectReference` AS `ProjectReference`, `pm`.`ProjectName` AS `ProjectName1`, CONCAT(`pm`.`ProjectReference`,' - ',`pm`.`ProjectName`) AS `ProjectName` , IFNULL(`rm`.`ReleaseTestStatus` ,'No Data Available') as  ReleaseTestStatus, IFNULL(`rm`.`TestKeyMessage` ,'No Data Available') as TestKeyMessage, IFNULL(`rm`.`Risks_Issues` ,'No Data Available') as Risks_Issues, IFNULL(`rm`.`LastRegistration` ,'No Data Available') as LastRegistration, IFNULL(`rm`.`CCB_Ready4SPRINT` ,'No Data Available') as CCB_Ready4SPRINT, IFNULL(`rm`.`CCB_Ready4UAT` ,'No Data Available') as CCB_Ready4UAT , IFNULL(`rm`.`UAT_StartDate` ,'No Data Available]') as UAT_StartDate, IFNULL(`rm`.`End_UAT` ,'No Data Available') as End_UAT, IFNULL(`rm`.`Freeze` ,'No Data Available') as Freeze, IFNULL(`rm`.`CCB_Ready4PROD` ,'No Data Available')as CCB_Ready4PROD , IFNULL(`rm`.`CCB_Closure` ,'No Data Available')as CCB_Closure , IFNULL(`rm`.`ReleaseDate` ,'No Data Available') as ReleaseDate1 FROM   `project_milestones_status` `pm`  LEFT JOIN   `release_milestones_status` `rm`  ON  `rm`.`ReleaseName` =  REPLACE(TRIM(SUBSTRING_INDEX(`pm`.`ReleaseName`, '-', 1)),'+','')  group by `pm`.`ReleaseName` order by `pm`.`ReleaseName`;

/*** DEFECTS & USER STORY MAPPING FOR prd ready status **/

CREATE  OR REPLACE VIEW `v_defects_data` AS  SELECT `d`.`defectId` AS `defectId`, `d`.`defectKey` AS `defectKey`, `d`.`projectKey` AS `projectKey`,`d`.`projectName` AS `projectName`,`d`.`projectCategory` AS `projectCategory`,IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`,`pm`.`ProjectReference` AS `projectReference`, IFNULL(`d`.`resolution`, 'Un-Defined') AS `resolution`, IFNULL(`d`.`defectSevirity`, 'Un-Defined') AS `defectSevirity`,IFNULL(`d`.`product`, 'Un-Defined') AS `product`,CASE WHEN `d`.`testLevelDetected` LIKE '%system%' THEN 'System Test' WHEN `d`.`testLevelDetected` LIKE '%sprint%' THEN 'Sprint Test'  WHEN `d`.`testLevelDetected` LIKE '%IT%' THEN 'Integration Test'  WHEN `d`.`testLevelDetected` LIKE '%UAT%' THEN 'UAT'  WHEN `d`.`testLevelDetected` LIKE '%unit%' THEN 'Unit Test'  ELSE 'Un-Defined' END AS `testLevelDetected`,CASE  WHEN `d`.`testTypes` LIKE '%func%' THEN 'Functional'  WHEN `d`.`testTypes` LIKE '%aut%' THEN 'Automation'  WHEN `d`.`testTypes` LIKE '%per%' THEN 'Performance'  WHEN `d`.`testTypes` LIKE '%sec%' THEN 'Security Test'  WHEN `d`.`testTypes` LIKE '%uni%' THEN 'Unit Test'  ELSE 'Un-Defined' END AS `testTypes`, CASE  WHEN `d`.`status` LIKE '%To Validate%' THEN 'UAT Ready'  WHEN `d`.`status` LIKE '%To Verify%' THEN 'In Test'  WHEN `d`.`status` LIKE '%To Groom%' THEN 'In Test'  WHEN `d`.`status` LIKE '%In Test%' THEN 'In Test'  WHEN `d`.`status` LIKE '%UAT on-going%' THEN 'In Test'  WHEN `d`.`status` LIKE '%Ready%' THEN 'UAT Ready' WHEN `d`.`status` LIKE '%open%' THEN 'Open'  WHEN `d`.`status` LIKE '%sprintable%' THEN 'In Progress'  WHEN `d`.`status` LIKE '%progress%' THEN 'In Progress'  WHEN `d`.`status` LIKE '%Analysis%' THEN 'In Progress'  WHEN `d`.`status` LIKE '%Business Validated%' THEN 'In Progress'  WHEN `d`.`status` LIKE '%review%' THEN 'In Review'  WHEN `d`.`status` LIKE '%incep%' THEN 'In Review'  WHEN `d`.`status` LIKE '%promot%' THEN 'To Promote'  WHEN `d`.`status` LIKE 'open' THEN 'Open'  WHEN `d`.`status` LIKE 'Backlog' THEN 'Open'  WHEN `d`.`status` LIKE 'Next' THEN 'Open'  WHEN `d`.`status` LIKE '%resol%' THEN 'Resolved'  WHEN `d`.`status` LIKE '%done%' THEN 'Resolved'  WHEN `d`.`status` LIKE '%reopened%' THEN 'Reopened'  WHEN `d`.`status` LIKE '%closed%' THEN 'Closed'  WHEN `d`.`status` LIKE '%prd ready%' THEN 'Closed'  ELSE `d`.`status` END AS `status`, `d`.`createdOn` AS `createdOn`, `d`.`summary` AS `summary`, IFNULL(`d`.`createdUserName`, 'UnAssigned') AS `createdUserName`, IFNULL(`d`.`priority`, 'Un-Defined') AS `priority`, IFNULL(`d`.`assigneeName`, 'UnAssigned') AS `assigneeName`, IFNULL(`d`.`severity`, 'Un-Defined') AS `severity`, IFNULL(`d`.`component`, 'No Components') AS `component`, IFNULL(`d`.`sprintId`, 'Un-Defined') AS `sprintId`, IFNULL(`d`.`reporter`, 'UnAssigned') AS `reporter`, IFNULL(`d`.`progress`, 'Un-Defined') AS `progress`, IFNULL(`d`.`stereoType`, 'Un-Defined') AS `stereoType`, IFNULL(`d`.`epicLink`, 'Un-Defined') AS `epicLink`, IFNULL(`d`.`fixVersions`, 'Un-Defined') AS `fixVersions`, `pm`.`ReleaseName` AS `ReleaseName`, IFNULL(`d`.`issueType`, 'Un-Defined') AS `issueType`, IFNULL(`d`.`parentKey`, 'Un-Defined') AS `parentKey`, IFNULL(`d`.`storyDefectIdFixVersion`, 'Un-Defined') AS `storyDefectIdFixVersion`, REPLACE(`pm`.`JIRA_KEY`, ' ', '') AS `JIRA_KEY`  FROM (`defect_details` `d` JOIN `v_project_wise_defect_keys` `pm` ON (`d`.`projectKey` = REPLACE(`pm`.`JIRA_KEY`, ' ', '')  AND `d`.`fixVersions` = `pm`.`ReleaseName`))  WHERE `d`.`status` <> 'Closed'  AND `d`.`fixVersions` <> 'Un-Defined'; 

CREATE  OR REPLACE VIEW `v_user_story` AS  SELECT  `d`.`userStoryID` AS `userStoryID`, `d`.`storyKey` AS `storyKey`, `d`.`projectKey` AS `projectKey`, `d`.`projectName` AS `projectName`, CASE WHEN `d`.`status` LIKE '%Backlog%' THEN 'Backlog'  WHEN `d`.`status` LIKE '%BA in progress%' THEN 'Backlog'  WHEN `d`.`status` LIKE '%Open%' THEN 'Backlog'  WHEN `d`.`status` LIKE '%BA to validate%' THEN 'Analysis'   WHEN `d`.`status` LIKE '%FA in progress%' THEN 'Analysis'  WHEN `d`.`status` LIKE '%FA to validate%' THEN 'Analysis'   WHEN `d`.`status` LIKE '%TA in progress%' THEN 'Analysis'  WHEN `d`.`status` LIKE '%TA to validate%' THEN 'Analysis'  WHEN `d`.`status` LIKE '%To groom%' THEN 'Analysis'  WHEN `d`.`status` LIKE '%Sprintable%' THEN 'Analysis'  WHEN `d`.`status` LIKE '%In Inception%' THEN 'Analysis'  WHEN `d`.`status` LIKE '%To validate%' THEN 'Analysis'  WHEN `d`.`status` LIKE '%Ready%' THEN 'Analysis'  WHEN `d`.`status` LIKE '%In progress%' THEN 'Development'  WHEN `d`.`status` LIKE 'In review' THEN 'Development'  WHEN `d`.`status` LIKE 'To promote' THEN 'Development'  WHEN `d`.`status` LIKE 'In test' THEN 'Build/Test'  WHEN `d`.`status` LIKE '%PO To validate%' THEN 'Build/Test'  WHEN `d`.`status` LIKE '%Resolved%' THEN 'Build/Test'  WHEN `d`.`status` LIKE '%To Verify%' THEN 'Build/Test'  WHEN `d`.`status` LIKE '%To review%' THEN 'Build/Test'  WHEN `d`.`status` LIKE 'Ready for uat' THEN 'Closed'  WHEN `d`.`status` LIKE '%Closed%' THEN 'Closed'  WHEN `d`.`status` LIKE 'Reopened' THEN 'Reopened'  WHEN `d`.`status` LIKE '%prd ready%' THEN 'Closed' END AS `storyStatus`, `d`.`summary` AS `summary`, IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`, `pm`.`ProjectReference` AS `ProjectReference`, IFNULL(`d`.`resolution`, 'Un-Defined') AS `resolution`, IFNULL(`d`.`priority`, 'Un-Defined') AS `priority`, IFNULL(`d`.`assigneeName`, 'UnAssigned') AS `assigneeName`, IFNULL(`d`.`severity`, 'Un-Defined') AS `severity`, IFNULL(`d`.`component`, 'No Components') AS `component`, IFNULL(`d`.`sprintId`, 'Un-Defined') AS `sprintId`, IFNULL(`d`.`reporter`, 'UnAssigned') AS `reporter`, IFNULL(`d`.`epicLink`, 'Un-Defined') AS `epicLink`, IFNULL(`d`.`fixVersions`, 'Un-Defined') AS `fixVersions`, `pm`.`ReleaseName` AS `ReleaseName`, IFNULL(`d`.`issueType`, 'Un-Defined') AS `issueType`, REPLACE(`pm`.`JIRA_KEY`, ' ', '') AS `JIRA_KEY`  FROM  (`user_story_details` `d` JOIN `v_project_wise_defect_keys` `pm` ON (`d`.`projectKey` = REPLACE(`pm`.`JIRA_KEY`, ' ', '')  AND `d`.`fixVersions` = `pm`.`ReleaseName`));



/** Phase 3 - Test Key Message Changes */

ALTER TABLE `project_milestones_status`  ADD COLUMN `KeyRemarksTesting_Plain` LONGTEXT NULL DEFAULT NULL AFTER `KeyRemarksTesting` ;

CREATE OR REPLACE VIEW `v_project_ccb_details` AS SELECT `rm`.`ReleaseName` AS `ReleaseName`, `rm`.`ReleaseDate` AS `ReleaseDate`, `pm`.`ProjectReference` AS `ProjectReference`, `pm`.`ProjectName` AS `ProjectName1`, CONCAT(`pm`.`ProjectReference`,' - ',`pm`.`ProjectName`) AS `ProjectName`,`pm`.`Test_Status` AS `test_status`, `pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`,  `pm`.`KeyRemarksTesting_Plain` AS `KeyRemarksTesting_Plain`, `pm`.`Domain` AS `Domain`, `pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`, `pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link1`,  CASE WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'file://%' THEN `pm`.`PROJECTREPOSITORY_Link`  WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'http://%' THEN `pm`.`PROJECTREPOSITORY_Link` WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'https://%' THEN `pm`.`PROJECTREPOSITORY_Link`  ELSE NULL  END AS `PROJECTREPOSITORY_Link`,  `pm`.`JIRA_Link` AS `JIRA_Link1`,  CONCAT('https://jira.pvgroup.be/issues/?jql=project in (',  `pm`.`Jira_defect_projects`, ') AND issuetype in (Defect,"Story Defect") AND status != Closed AND created > startOfMonth(-12) ORDER BY created DESC') AS `JIRA_Link`, IFNULL(`pm`.`ZEPHYR_Link`, 'http://zephyr.pvgroup.intranet/flex/html5/login') AS `ZEPHYR_Link`,  CONCAT('https://jira.pvgroup.be/issues/?jql=project in (',  `pm`.`Jira_defect_projects`, ') AND issuetype in ("Story") AND fixVersion in (',  `rm`.`ReleaseName`,  ')  ORDER BY created DESC') AS `UserStory_Link`,  `pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`,  `pm`.`Test_Coordinator` AS `Test_Coordinator`,  `pm`.`Scrum_Master` AS `Scrum_Master`,  IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`,  IFNULL(`pm`.`CCB_STARTUP_Date`,  `rm`.`LastRegistration`) AS `CCB_STARTUP_Date`, IFNULL(`pm`.`CCB_Ready4SPRINT_Date`, `rm`.`CCB_Ready4SPRINT`) AS `CCB_Ready4SPRINT_Date`, IFNULL(`pm`.`CCB_Ready4UAT_Date`, `rm`.`CCB_Ready4UAT`) AS `CCB_Ready4UAT_Date`, IFNULL(`pm`.`CCB_Ready4PROD_Date`, `rm`.`CCB_Ready4PROD`) AS `CCB_Ready4PROD_Date`, IFNULL(`pm`.`CCB_Closure_Date`, `rm`.`CCB_Closure`) AS `CCB_Closure_Date`, `pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`, `pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`, CONVERT( `pm`.`CCB_STARTUP_Remarks` USING UTF8) AS `CCB_STARTUP_Remarks`, CONVERT( `pm`.`Ready2StartTestRemarks` USING UTF8) AS `Ready2StartTestRemarks`, `pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`, CONVERT( `pm`.`CCBReady4SPRINT_Remarks` USING UTF8) AS `CCBReady4SPRINT_Remarks`, CONVERT( `pm`.`Ready4SprintTestRemarks` USING UTF8) AS `Ready4SprintTestRemarks`, `pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`, CONVERT( `pm`.`CCBReady4UAT_Remarks` USING UTF8) AS `CCBReady4UAT_Remarks`, CONVERT( `pm`.`Ready4UATTestRemarks` USING UTF8) AS `Ready4UATTestRemarks`, `rm`.`LastRegistration` AS `LastRegistrationDate`, `rm`.`UAT_StartDate` AS `UAT_START_DATE`, `rm`.`End_UAT` AS `UAT_END_DATE`, `rm`.`Freeze` AS `FREEZE_DATE`, `pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`, CONVERT( `pm`.`CCBReady4PROD_Remarks` USING UTF8) AS `CCBReady4PROD_Remarks`, CONVERT( `pm`.`Ready4ProdTestRemarks` USING UTF8) AS `Ready4ProdTestRemarks`, CONVERT( `pm`.`CCB_CLOSURE_Remarks` USING UTF8) AS `CCB_CLOSURE_Remarks`, `pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`, `pm`.`Project_Manager` AS `Project_Manager`, `tr`.`UNIT_TEST_PATH` AS `unitTestReportPath`, `tr`.`UAT_TEST_PATH` AS `UAT_TestReportPath`, `tr`.`AUTOMATION_PATH` AS `automationReportPath`, `tr`.`PERFORMANCE_PATH` AS `performanceReportPath`, `tr`.`SECURITY_TEST_PATH` AS `securityReportPath`, `tr`.`IT_TEST_PATH` AS `IT_TestReportPath`, `tr`.`CCB_GATING_PATH` AS `CCB_GATING_PATH`, `tr`.`MASTER_TEST_PLAN_PATH` AS `MASTER_TEST_PLAN_PATH`, `pm`.`Jira_defect_projects` AS `Jira_defect_projects`, `pm`.`DetailedTestStatus` AS `DetailedTestStatus`, `pm`.`Test_Coordinator_email` AS `Test_Coordinator_email`,  `pm`.`Project_Manager_email` AS `Project_Manager_email`, `pm`.`Scrum_Master_email` AS `Scrum_Master_email`, `pm`.`Delivery_Master_Lead_email` AS `Delivery_Master_Lead_email`, SUBSTRING_INDEX(`pm`.`Jira_defect_projects`, ',', 1) AS `JIRA_KEY` FROM ((`release_milestones_status` `rm` LEFT JOIN `project_milestones_status` `pm` ON (CONVERT( `rm`.`ReleaseName` USING UTF8) = `pm`.`ReleaseName`)) LEFT JOIN `v_test_reports` `tr` ON (`pm`.`ReleaseName` = `tr`.`ReleaseName` AND `pm`.`ProjectReference` = `tr`.`ProjectReference`));


/** Phase 3 - Test Key Message Edit *  https://gitlab.com/expleo-p-v/diary/-/issues/235   */

CREATE OR REPLACE VIEW `v_project_ccb_details` AS SELECT `rm`.`ReleaseName` AS `ReleaseName`, `rm`.`ReleaseDate` AS `ReleaseDate`, `pm`.`ProjectReference` AS `ProjectReference`, `pm`.`ProjectName` AS `ProjectName1`, CONCAT(`pm`.`ProjectReference`,' - ',`pm`.`ProjectName`) AS `ProjectName`,`pm`.`Test_Status` AS `test_status`, `pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`,`pm`.`Domain` AS `Domain`, `pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`, `pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link1`,  CASE WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'file://%' THEN `pm`.`PROJECTREPOSITORY_Link`  WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'http://%' THEN `pm`.`PROJECTREPOSITORY_Link` WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'https://%' THEN `pm`.`PROJECTREPOSITORY_Link`  ELSE NULL  END AS `PROJECTREPOSITORY_Link`,  `pm`.`JIRA_Link` AS `JIRA_Link1`,  CONCAT('https://jira.pvgroup.be/issues/?jql=project in (',  `pm`.`Jira_defect_projects`, ') AND issuetype in (Defect,"Story Defect") AND status != Closed AND created > startOfMonth(-12) ORDER BY created DESC') AS `JIRA_Link`, IFNULL(`pm`.`ZEPHYR_Link`, 'http://zephyr.pvgroup.intranet/flex/html5/login') AS `ZEPHYR_Link`,  CONCAT('https://jira.pvgroup.be/issues/?jql=project in (',  `pm`.`Jira_defect_projects`, ') AND issuetype in ("Story") AND fixVersion in (',  `rm`.`ReleaseName`,  ')  ORDER BY created DESC') AS `UserStory_Link`,  `pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`,  `pm`.`Test_Coordinator` AS `Test_Coordinator`,  `pm`.`Scrum_Master` AS `Scrum_Master`,  IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`,  IFNULL(`pm`.`CCB_STARTUP_Date`,  `rm`.`LastRegistration`) AS `CCB_STARTUP_Date`, IFNULL(`pm`.`CCB_Ready4SPRINT_Date`, `rm`.`CCB_Ready4SPRINT`) AS `CCB_Ready4SPRINT_Date`, IFNULL(`pm`.`CCB_Ready4UAT_Date`, `rm`.`CCB_Ready4UAT`) AS `CCB_Ready4UAT_Date`, IFNULL(`pm`.`CCB_Ready4PROD_Date`, `rm`.`CCB_Ready4PROD`) AS `CCB_Ready4PROD_Date`, IFNULL(`pm`.`CCB_Closure_Date`, `rm`.`CCB_Closure`) AS `CCB_Closure_Date`, `pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`, `pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`, CONVERT( `pm`.`CCB_STARTUP_Remarks` USING UTF8) AS `CCB_STARTUP_Remarks`, CONVERT( `pm`.`Ready2StartTestRemarks` USING UTF8) AS `Ready2StartTestRemarks`, `pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`, CONVERT( `pm`.`CCBReady4SPRINT_Remarks` USING UTF8) AS `CCBReady4SPRINT_Remarks`, CONVERT( `pm`.`Ready4SprintTestRemarks` USING UTF8) AS `Ready4SprintTestRemarks`, `pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`, CONVERT( `pm`.`CCBReady4UAT_Remarks` USING UTF8) AS `CCBReady4UAT_Remarks`, CONVERT( `pm`.`Ready4UATTestRemarks` USING UTF8) AS `Ready4UATTestRemarks`, `rm`.`LastRegistration` AS `LastRegistrationDate`, `rm`.`UAT_StartDate` AS `UAT_START_DATE`, `rm`.`End_UAT` AS `UAT_END_DATE`, `rm`.`Freeze` AS `FREEZE_DATE`, `pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`, CONVERT( `pm`.`CCBReady4PROD_Remarks` USING UTF8) AS `CCBReady4PROD_Remarks`, CONVERT( `pm`.`Ready4ProdTestRemarks` USING UTF8) AS `Ready4ProdTestRemarks`, CONVERT( `pm`.`CCB_CLOSURE_Remarks` USING UTF8) AS `CCB_CLOSURE_Remarks`, `pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`, `pm`.`Project_Manager` AS `Project_Manager`, `tr`.`UNIT_TEST_PATH` AS `unitTestReportPath`, `tr`.`UAT_TEST_PATH` AS `UAT_TestReportPath`, `tr`.`AUTOMATION_PATH` AS `automationReportPath`, `tr`.`PERFORMANCE_PATH` AS `performanceReportPath`, `tr`.`SECURITY_TEST_PATH` AS `securityReportPath`, `tr`.`IT_TEST_PATH` AS `IT_TestReportPath`, `tr`.`CCB_GATING_PATH` AS `CCB_GATING_PATH`, `tr`.`MASTER_TEST_PLAN_PATH` AS `MASTER_TEST_PLAN_PATH`, `pm`.`Jira_defect_projects` AS `Jira_defect_projects`, `pm`.`DetailedTestStatus` AS `DetailedTestStatus`, `pm`.`Test_Coordinator_email` AS `Test_Coordinator_email`,  `pm`.`Project_Manager_email` AS `Project_Manager_email`, `pm`.`Scrum_Master_email` AS `Scrum_Master_email`, `pm`.`Delivery_Master_Lead_email` AS `Delivery_Master_Lead_email`, SUBSTRING_INDEX(`pm`.`Jira_defect_projects`, ',', 1) AS `JIRA_KEY` , CONCAT('http://weconnect.pvgroup.intranet/project/general/release/Lists/CCB%20Dashboard/EditForm.aspx?ID=', `pm`.`SP_ID`) AS `SP_ProjectEditLink` FROM ((`release_milestones_status` `rm` LEFT JOIN `project_milestones_status` `pm` ON (CONVERT( `rm`.`ReleaseName` USING UTF8) = `pm`.`ReleaseName`)) LEFT JOIN `v_test_reports` `tr` ON (`pm`.`ReleaseName` = `tr`.`ReleaseName` AND `pm`.`ProjectReference` = `tr`.`ProjectReference`));

/** Updated for Key Remark Testing  https://gitlab.com/expleo-p-v/diary/-/issues/235*/
ALTER TABLE `project_milestones_status`
	ADD COLUMN `SP_ProjectEditLink` LONGTEXT NULL DEFAULT NULL AFTER `Open_Risks_Issues`;

CREATE OR REPLACE VIEW `v_project_ccb_details` AS SELECT `rm`.`ReleaseName` AS `ReleaseName`, `rm`.`ReleaseDate` AS `ReleaseDate`, `pm`.`ProjectReference` AS `ProjectReference`, `pm`.`ProjectName` AS `ProjectName1`, CONCAT(`pm`.`ProjectReference`,' - ',`pm`.`ProjectName`) AS `ProjectName`,`pm`.`Test_Status` AS `test_status`, `pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`,`pm`.`KeyRemarksTesting_Plain` AS `KeyRemarksTesting_Plain`, `pm`.`Domain` AS `Domain`, `pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`, `pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link1`,  CASE WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'file://%' THEN `pm`.`PROJECTREPOSITORY_Link`  WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'http://%' THEN `pm`.`PROJECTREPOSITORY_Link` WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'https://%' THEN `pm`.`PROJECTREPOSITORY_Link`  ELSE NULL  END AS `PROJECTREPOSITORY_Link`,  `pm`.`JIRA_Link` AS `JIRA_Link1`,  CONCAT('https://jira.pvgroup.be/issues/?jql=project in (',  `pm`.`Jira_defect_projects`, ') AND issuetype in (Defect,"Story Defect") AND status != Closed AND created > startOfMonth(-12) ORDER BY created DESC') AS `JIRA_Link`, IFNULL(`pm`.`ZEPHYR_Link`, 'http://zephyr.pvgroup.intranet/flex/html5/login') AS `ZEPHYR_Link`,  CONCAT('https://jira.pvgroup.be/issues/?jql=project in (',  `pm`.`Jira_defect_projects`, ') AND issuetype in ("Story") AND fixVersion in (',  `rm`.`ReleaseName`,  ')  ORDER BY created DESC') AS `UserStory_Link`,  `pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`,  `pm`.`Test_Coordinator` AS `Test_Coordinator`,  `pm`.`Scrum_Master` AS `Scrum_Master`,  IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`,  IFNULL(`pm`.`CCB_STARTUP_Date`,  `rm`.`LastRegistration`) AS `CCB_STARTUP_Date`, IFNULL(`pm`.`CCB_Ready4SPRINT_Date`, `rm`.`CCB_Ready4SPRINT`) AS `CCB_Ready4SPRINT_Date`, IFNULL(`pm`.`CCB_Ready4UAT_Date`, `rm`.`CCB_Ready4UAT`) AS `CCB_Ready4UAT_Date`, IFNULL(`pm`.`CCB_Ready4PROD_Date`, `rm`.`CCB_Ready4PROD`) AS `CCB_Ready4PROD_Date`, IFNULL(`pm`.`CCB_Closure_Date`, `rm`.`CCB_Closure`) AS `CCB_Closure_Date`, `pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`, `pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`, CONVERT( `pm`.`CCB_STARTUP_Remarks` USING UTF8) AS `CCB_STARTUP_Remarks`, CONVERT( `pm`.`Ready2StartTestRemarks` USING UTF8) AS `Ready2StartTestRemarks`, `pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`, CONVERT( `pm`.`CCBReady4SPRINT_Remarks` USING UTF8) AS `CCBReady4SPRINT_Remarks`, CONVERT( `pm`.`Ready4SprintTestRemarks` USING UTF8) AS `Ready4SprintTestRemarks`, `pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`, CONVERT( `pm`.`CCBReady4UAT_Remarks` USING UTF8) AS `CCBReady4UAT_Remarks`, CONVERT( `pm`.`Ready4UATTestRemarks` USING UTF8) AS `Ready4UATTestRemarks`, `rm`.`LastRegistration` AS `LastRegistrationDate`, `rm`.`UAT_StartDate` AS `UAT_START_DATE`, `rm`.`End_UAT` AS `UAT_END_DATE`, `rm`.`Freeze` AS `FREEZE_DATE`, `pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`, CONVERT( `pm`.`CCBReady4PROD_Remarks` USING UTF8) AS `CCBReady4PROD_Remarks`, CONVERT( `pm`.`Ready4ProdTestRemarks` USING UTF8) AS `Ready4ProdTestRemarks`, CONVERT( `pm`.`CCB_CLOSURE_Remarks` USING UTF8) AS `CCB_CLOSURE_Remarks`, `pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`, `pm`.`Project_Manager` AS `Project_Manager`, `tr`.`UNIT_TEST_PATH` AS `unitTestReportPath`, `tr`.`UAT_TEST_PATH` AS `UAT_TestReportPath`, `tr`.`AUTOMATION_PATH` AS `automationReportPath`, `tr`.`PERFORMANCE_PATH` AS `performanceReportPath`, `tr`.`SECURITY_TEST_PATH` AS `securityReportPath`, `tr`.`IT_TEST_PATH` AS `IT_TestReportPath`, `tr`.`CCB_GATING_PATH` AS `CCB_GATING_PATH`, `tr`.`MASTER_TEST_PLAN_PATH` AS `MASTER_TEST_PLAN_PATH`, `pm`.`Jira_defect_projects` AS `Jira_defect_projects`, `pm`.`DetailedTestStatus` AS `DetailedTestStatus`, `pm`.`Test_Coordinator_email` AS `Test_Coordinator_email`,  `pm`.`Project_Manager_email` AS `Project_Manager_email`, `pm`.`Scrum_Master_email` AS `Scrum_Master_email`, `pm`.`Delivery_Master_Lead_email` AS `Delivery_Master_Lead_email`, SUBSTRING_INDEX(`pm`.`Jira_defect_projects`, ',', 1) AS `JIRA_KEY` , CONCAT('http://weconnect.pvgroup.intranet/project/general/release/Lists/CCB%20Dashboard/EditForm.aspx?ID=', `pm`.`SP_ID`) AS `SP_ProjectEditLink` FROM ((`release_milestones_status` `rm` LEFT JOIN `project_milestones_status` `pm` ON (CONVERT( `rm`.`ReleaseName` USING UTF8) = `pm`.`ReleaseName`)) LEFT JOIN `v_test_reports` `tr` ON (`pm`.`ReleaseName` = `tr`.`ReleaseName` AND `pm`.`ProjectReference` = `tr`.`ProjectReference`));

/* TEST Key Edit MEsage - Release Milestones  */

ALTER TABLE `release_milestones_status`  ADD COLUMN `SP_ID` INT NULL AFTER `Path`;

CREATE  OR REPLACE VIEW `v_release_milestones_status` AS SELECT REPLACE(TRIM(SUBSTRING_INDEX(`pm`.`ReleaseName`, '-', 1)),'+',  '') AS `ReleaseName`,`pm`.`ReleaseName` AS `ReleaseMoment`, `rm`.`ReleaseName` AS `RM_ReleaseName`, IFNULL(`rm`.`ReleaseDate`, 'No Data Available') AS `ReleaseDate`, `pm`.`ProjectReference` AS `ProjectReference`, `pm`.`ProjectName` AS `ProjectName1`, CONCAT(`pm`.`ProjectReference`, ' - ', `pm`.`ProjectName`) AS `ProjectName`, IFNULL(`rm`.`ReleaseTestStatus`,  'No Data Available') AS `ReleaseTestStatus`, IFNULL(`rm`.`TestKeyMessage`, 'No Data Available') AS `TestKeyMessage`, IFNULL(`rm`.`Risks_Issues`, 'No Data Available') AS `Risks_Issues`, IFNULL(`rm`.`LastRegistration`, 'No Data Available') AS `LastRegistration`, IFNULL(`rm`.`CCB_Ready4SPRINT`, 'No Data Available') AS `CCB_Ready4SPRINT`, IFNULL(`rm`.`CCB_Ready4UAT`, 'No Data Available') AS `CCB_Ready4UAT`, IFNULL(`rm`.`UAT_StartDate`, 'No Data Available]') AS `UAT_StartDate`, IFNULL(`rm`.`End_UAT`, 'No Data Available') AS `End_UAT`, IFNULL(`rm`.`Freeze`, 'No Data Available') AS `Freeze`, IFNULL(`rm`.`CCB_Ready4PROD`, 'No Data Available') AS `CCB_Ready4PROD`, IFNULL(`rm`.`CCB_Closure`, 'No Data Available') AS `CCB_Closure`, IFNULL(`rm`.`ReleaseDate`, 'No Data Available') AS `ReleaseDate1`, CONCAT('http://weconnect.pvgroup.intranet/project/general/release/Lists/Release%20Milestones%20Overview/EditForm.aspx?ID=', `rm`.`SP_ID`) AS `SP_ReleaseEditLink` FROM (`project_milestones_status` `pm` LEFT JOIN `release_milestones_status` `rm` ON (`rm`.`ReleaseName` = REPLACE(TRIM(SUBSTRING_INDEX(`pm`.`ReleaseName`, '-', 1)), '+', ''))) GROUP BY `pm`.`ReleaseName` ORDER BY `pm`.`ReleaseName` ;


/** Adding Priority To Test Case MVP implementation **/
/** https://gitlab.com/expleo-p-v/diary/-/issues/240  */

ALTER TABLE `test_execution_results`  ADD COLUMN `priority` VARCHAR(45) NULL AFTER `projectId`;


CREATE OR REPLACE VIEW `v_test_results` AS  SELECT  `test_execution_results`.`id` AS `id`,  TRIM(SUBSTRING_INDEX(`test_execution_results`.`projectName`,  '-',  1)) AS `ProjectReference`, TRIM(SUBSTR(`test_execution_results`.`projectName`,LOCATE('-',`test_execution_results`.`projectName`) + 1)) AS `ProjectName`,  `project_milestones_status`.`Domain` AS `DomainName`,  `test_execution_results`.`projectName` AS `Project`,  `test_execution_results`.`releaseName` AS `ReleaseName`,  `test_execution_results`.`hierarchy` AS `Hierarchy`,  NULLIF(`test_execution_results`.`folder`, '') AS `Folder`,  NULLIF(`test_execution_results`.`subFolder`, '') AS `subFolder`,  NULLIF(`test_execution_results`.`subFolder1`,   '') AS `subFolder1`,  NULLIF(`test_execution_results`.`subFolder2`,   '') AS `subFolder2`,  `test_execution_results`.`createdOn` AS `CreatedOn`,  `test_execution_results`.`requirementId` AS `RequirementId`,  `test_execution_results`.`reqAltId` AS `ReqAltId`,  `test_execution_results`.`requirementName` AS `RequirementName`,  `test_execution_results`.`environment` AS `Environment`, `test_execution_results`.`testCaseAltId` AS `RestCaseAltId`,  `test_execution_results`.`testCaseName` AS `RestCaseName`,  `test_execution_results`.`testCaseId` AS `TestCaseId`,  `test_execution_results`.`automation` AS `Automation`,  `test_execution_results`.`cycle` AS `Cycle`,  `test_execution_results`.`testStatus` AS `tStatus`,  CASE  WHEN `test_execution_results`.`testStatus` LIKE '%fail%' THEN 'Fail'  WHEN `test_execution_results`.`testStatus` LIKE '%pass%' THEN 'Pass'   WHEN `test_execution_results`.`testStatus` LIKE '%incomp%' THEN 'Incomplete' WHEN `test_execution_results`.`testStatus` LIKE '%blocked%' THEN 'Blocked'   WHEN `test_execution_results`.`testStatus` LIKE '%Not exec%' THEN 'Not Executed' WHEN `test_execution_results`.`testStatus` LIKE '%N/A%' THEN 'N/A'      ELSE 'Not Executed' END AS `testStatus`, `test_execution_results`.`executedOn` AS `ExecutedOn`, `test_execution_results`.`tags` AS `Tags`, `test_execution_results`.`priority` AS `priority`,  `test_execution_results`.`defects` AS `Defects`,  `test_execution_results`.`executedBy` AS `ExecutedBy`,  `test_execution_results`.`createdBy` AS `CreatedBy`,  `test_execution_results`.`insertedOn` AS `insertedOn`,  `test_execution_results`.`subFolderLevel` AS `subFolderLevel`, `test_execution_results`.`updatedOn` AS `updatedOn`     FROM  (`test_execution_results`  LEFT JOIN `project_milestones_status` ON (TRIM(CONVERT( SUBSTRING_INDEX(`test_execution_results`.`projectName`, '-', 1) USING UTF8)) = `project_milestones_status`.`ProjectReference`  AND CONVERT(TRIM(`test_execution_results`.`releaseName`) USING UTF8) = `project_milestones_status`.`ReleaseName`)) ;


/** Release Name to be fetched from CCB Projects - DATA Uploads  **/



CREATE  OR REPLACE VIEW `v_release_milestones_status` AS SELECT REPLACE(TRIM(SUBSTRING_INDEX(`pm`.`ReleaseName`, '-', 1)),'+',  '') AS `ReleaseName`,`pm`.`ReleaseName` AS `ReleaseMoment`, `rm`.`ReleaseName` AS `RM_ReleaseName`, IFNULL(`rm`.`ReleaseDate`, 'No Data Available') AS `ReleaseDate`, `pm`.`ProjectReference` AS `ProjectReference`, `pm`.`ProjectName` AS `ProjectName1`, CONCAT(`pm`.`ProjectReference`, ' - ', `pm`.`ProjectName`) AS `ProjectName`, IFNULL(`rm`.`ReleaseTestStatus`,  'No Data Available') AS `ReleaseTestStatus`, IFNULL(`rm`.`TestKeyMessage`, 'No Data Available') AS `TestKeyMessage`, IFNULL(`rm`.`Risks_Issues`, 'No Data Available') AS `Risks_Issues`, IFNULL(`rm`.`LastRegistration`, 'No Data Available') AS `LastRegistration`, IFNULL(`rm`.`CCB_Ready4SPRINT`, 'No Data Available') AS `CCB_Ready4SPRINT`, IFNULL(`rm`.`CCB_Ready4UAT`, 'No Data Available') AS `CCB_Ready4UAT`, IFNULL(`rm`.`UAT_StartDate`, 'No Data Available]') AS `UAT_StartDate`, IFNULL(`rm`.`End_UAT`, 'No Data Available') AS `End_UAT`, IFNULL(`rm`.`Freeze`, 'No Data Available') AS `Freeze`, IFNULL(`rm`.`CCB_Ready4PROD`, 'No Data Available') AS `CCB_Ready4PROD`, IFNULL(`rm`.`CCB_Closure`, 'No Data Available') AS `CCB_Closure`, IFNULL(`rm`.`ReleaseDate`, 'No Data Available') AS `ReleaseDate1`, `rm`.`ReleaseDate` AS `RM_releaseDate`, CONCAT('http://weconnect.pvgroup.intranet/project/general/release/Lists/Release%20Milestones%20Overview/EditForm.aspx?ID=', `rm`.`SP_ID`) AS `SP_ReleaseEditLink` FROM (`project_milestones_status` `pm` LEFT JOIN `release_milestones_status` `rm` ON (`rm`.`ReleaseName` = REPLACE(TRIM(SUBSTRING_INDEX(`pm`.`ReleaseName`, '-', 1)), '+', ''))) GROUP BY `pm`.`ReleaseName` ORDER BY `pm`.`ReleaseName` ;

create or replace view v_release_moments as  select releasename,releasemoment,rm_releasedate, releasedate as RD, ifNull(rm_releaseDate, (STR_TO_DATE((SUBSTRING_INDEX(ReleaseMoment,'-', 1)),'%Y.%m.%d'))) as releaseDate from  v_release_milestones_status;

/** FETCH CCB DETAILS UAT START DATE CHANGES **/

CREATE OR REPLACE
VIEW `v_project_ccb_details` AS
    SELECT 
        `rm`.`ReleaseName` AS `ReleaseName`,
        `rm`.`ReleaseDate` AS `ReleaseDate`,
        `pm`.`ProjectReference` AS `ProjectReference`,
        `pm`.`ProjectName` AS `ProjectName1`,
        CONCAT(`pm`.`ProjectReference`,
                ' - ',
                `pm`.`ProjectName`) AS `ProjectName`,
        `pm`.`Test_Status` AS `test_status`,
        `pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`,
        `pm`.`KeyRemarksTesting_Plain` AS `KeyRemarksTesting_Plain`,
        `pm`.`Domain` AS `Domain`,
        `pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`,
        `pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link1`,
        CASE
            WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'file://%' THEN `pm`.`PROJECTREPOSITORY_Link`
            WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'http://%' THEN `pm`.`PROJECTREPOSITORY_Link`
            WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'https://%' THEN `pm`.`PROJECTREPOSITORY_Link`
            ELSE NULL
        END AS `PROJECTREPOSITORY_Link`,
        `pm`.`JIRA_Link` AS `JIRA_Link1`,
        CONCAT('https://jira.pvgroup.be/issues/?jql=project in (',
                `pm`.`Jira_defect_projects`,
                ') AND issuetype in (Defect,"Story Defect") AND fixVersion in (',
                `rm`.`ReleaseName`,
                ') AND status != Closed AND created > startOfMonth(-12) ORDER BY created DESC') AS `JIRA_Link`,
        IFNULL(`pm`.`ZEPHYR_Link`,
                'http://zephyr.pvgroup.intranet/flex/html5/login') AS `ZEPHYR_Link`,
        CONCAT('https://jira.pvgroup.be/issues/?jql=project in (',
                `pm`.`Jira_defect_projects`,
                ') AND issuetype in ("Story") AND fixVersion in (',
                `rm`.`ReleaseName`,
                ')  ORDER BY created DESC') AS `UserStory_Link`,
        `pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`,
        `pm`.`Test_Coordinator` AS `Test_Coordinator`,
        `pm`.`Scrum_Master` AS `Scrum_Master`,
        IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`,
        IFNULL(`pm`.`CCB_STARTUP_Date`,
                `rm`.`LastRegistration`) AS `CCB_STARTUP_Date`,
        IFNULL(`pm`.`CCB_Ready4SPRINT_Date`,
                `rm`.`CCB_Ready4SPRINT`) AS `CCB_Ready4SPRINT_Date`,
        IFNULL(`pm`.`CCB_Ready4UAT_Date`,
                `rm`.`CCB_Ready4UAT`) AS `CCB_Ready4UAT_Date`,
        IFNULL(`pm`.`CCB_Ready4PROD_Date`,
                `rm`.`CCB_Ready4PROD`) AS `CCB_Ready4PROD_Date`,
        IFNULL(`pm`.`CCB_Closure_Date`,
                `rm`.`CCB_Closure`) AS `CCB_Closure_Date`,
        `pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`,
        `pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`,
        CONVERT( `pm`.`CCB_STARTUP_Remarks` USING UTF8) AS `CCB_STARTUP_Remarks`,
        CONVERT( `pm`.`Ready2StartTestRemarks` USING UTF8) AS `Ready2StartTestRemarks`,
        `pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`,
        CONVERT( `pm`.`CCBReady4SPRINT_Remarks` USING UTF8) AS `CCBReady4SPRINT_Remarks`,
        CONVERT( `pm`.`Ready4SprintTestRemarks` USING UTF8) AS `Ready4SprintTestRemarks`,
        `pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`,
        CONVERT( `pm`.`CCBReady4UAT_Remarks` USING UTF8) AS `CCBReady4UAT_Remarks`,
        CONVERT( `pm`.`Ready4UATTestRemarks` USING UTF8) AS `Ready4UATTestRemarks`,
        `rm`.`LastRegistration` AS `LastRegistrationDate`,
        IFNULL(`pm`.`UAT_Start_Date`,
                `rm`.`UAT_StartDate`) AS `UAT_START_DATE`,
        `rm`.`End_UAT` AS `UAT_END_DATE`,
        `rm`.`Freeze` AS `FREEZE_DATE`,
        `pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`,
        CONVERT( `pm`.`CCBReady4PROD_Remarks` USING UTF8) AS `CCBReady4PROD_Remarks`,
        CONVERT( `pm`.`Ready4ProdTestRemarks` USING UTF8) AS `Ready4ProdTestRemarks`,
        CONVERT( `pm`.`CCB_CLOSURE_Remarks` USING UTF8) AS `CCB_CLOSURE_Remarks`,
        `pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`,
        `pm`.`Project_Manager` AS `Project_Manager`,
        `tr`.`UNIT_TEST_PATH` AS `unitTestReportPath`,
        `tr`.`UAT_TEST_PATH` AS `UAT_TestReportPath`,
        `tr`.`AUTOMATION_PATH` AS `automationReportPath`,
        `tr`.`PERFORMANCE_PATH` AS `performanceReportPath`,
        `tr`.`SECURITY_TEST_PATH` AS `securityReportPath`,
        `tr`.`IT_TEST_PATH` AS `IT_TestReportPath`,
        `tr`.`CCB_GATING_PATH` AS `CCB_GATING_PATH`,
        `tr`.`MASTER_TEST_PLAN_PATH` AS `MASTER_TEST_PLAN_PATH`,
        `pm`.`Jira_defect_projects` AS `Jira_defect_projects`,
        `pm`.`DetailedTestStatus` AS `DetailedTestStatus`,
        `pm`.`Test_Coordinator_email` AS `Test_Coordinator_email`,
        `pm`.`Project_Manager_email` AS `Project_Manager_email`,
        `pm`.`Scrum_Master_email` AS `Scrum_Master_email`,
        `pm`.`Delivery_Master_Lead_email` AS `Delivery_Master_Lead_email`,
        SUBSTRING_INDEX(`pm`.`Jira_defect_projects`, ',', 1) AS `JIRA_KEY`,
        CONCAT('http://weconnect.pvgroup.intranet/project/general/release/Lists/CCB%20Dashboard/EditForm.aspx?ID=',
                `pm`.`SP_ID`) AS `SP_ProjectEditLink`
    FROM
        ((`release_milestones_status` `rm`
        LEFT JOIN `project_milestones_status` `pm` ON (CONVERT( `rm`.`ReleaseName` USING UTF8) = `pm`.`ReleaseName`))
        LEFT JOIN `v_test_reports` `tr` ON (`pm`.`ReleaseName` = `tr`.`ReleaseName`
            AND `pm`.`ProjectReference` = `tr`.`ProjectReference`))


/** Release view updates to fetch based on project ccb entries  **/

CREATE OR REPLACE view  `v_release_moments` AS  select `rm`.`ReleaseName` AS `releasename`, `rm`.`ReleaseMoment` AS `releasemoment`,`rm`.`RM_releaseDate` AS `rm_releasedate`,  `rm`.`ReleaseDate` AS `RD`, ifnull(`rm`.`RM_releaseDate`,str_to_date(substring_index(`rm`.`ReleaseMoment`,'-',1),'%Y.%m.%d')) AS `releaseDate`, `rm`.`LastRegistration`,  `rm`.`CCB_Ready4SPRINT`,  `rm`.`CCB_Ready4UAT` , `rm`.`CCB_Ready4PROD`, `rm`.`CCB_Closure` , `rm`.`LastRegistration` AS `LastRegistrationDate`,  `rm`.`UAT_StartDate` ,  `rm`.`End_UAT` AS `UAT_END_DATE`,         `rm`.`Freeze` AS `FREEZE_DATE`  from `v_release_milestones_status` rm; 

CREATE OR REPLACE VIEW `v_project_ccb_details` AS    SELECT `rm`.`releasename` AS `ReleaseName`,`rm`.`releaseDate` AS `ReleaseDate`, `pm`.`ProjectReference` AS `ProjectReference`, `pm`.`ProjectName` AS `ProjectName1`,CONCAT(`pm`.`ProjectReference`, ' - ', `pm`.`ProjectName`) AS `ProjectName`, `pm`.`Test_Status` AS `test_status`, `pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`, `pm`.`KeyRemarksTesting_Plain` AS `KeyRemarksTesting_Plain`,`pm`.`Domain` AS `Domain`, `pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`,`pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link1`, CASE     WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'file://%' THEN `pm`.`PROJECTREPOSITORY_Link`    WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'http://%' THEN `pm`.`PROJECTREPOSITORY_Link`     WHEN `pm`.`PROJECTREPOSITORY_Link` LIKE 'https://%' THEN `pm`.`PROJECTREPOSITORY_Link`     ELSE NULL END AS `PROJECTREPOSITORY_Link`,`pm`.`JIRA_Link` AS `JIRA_Link1`,CONCAT('https://jira.pvgroup.be/issues/?jql=project in (',`pm`.`Jira_defect_projects`, ') AND issuetype in (Defect,"Story Defect") AND fixVersion in (',`rm`.`releasename`, ') AND status != Closed AND created > startOfMonth(-12) ORDER BY created DESC') AS `JIRA_Link`, IFNULL(`pm`.`ZEPHYR_Link`, 'http://zephyr.pvgroup.intranet/flex/html5/login') AS `ZEPHYR_Link`, CONCAT('https://jira.pvgroup.be/issues/?jql=project in (',`pm`.`Jira_defect_projects`, ') AND issuetype in ("Story") AND fixVersion in (', `rm`.`releasename`,')  ORDER BY created DESC') AS `UserStory_Link`,`pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`, `pm`.`Test_Coordinator` AS `Test_Coordinator`, `pm`.`Scrum_Master` AS `Scrum_Master`, IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`, IFNULL(`pm`.`CCB_STARTUP_Date`, `rm`.`LastRegistration`) AS `CCB_STARTUP_Date`, IFNULL(`pm`.`CCB_Ready4SPRINT_Date`, `rm`.`CCB_Ready4SPRINT`) AS `CCB_Ready4SPRINT_Date`,IFNULL(`pm`.`CCB_Ready4UAT_Date`, `rm`.`CCB_Ready4UAT`) AS `CCB_Ready4UAT_Date`, IFNULL(`pm`.`CCB_Ready4PROD_Date`, `rm`.`CCB_Ready4PROD`) AS `CCB_Ready4PROD_Date`,IFNULL(`pm`.`CCB_Closure_Date`, `rm`.`CCB_Closure`) AS `CCB_Closure_Date`, `pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`, `pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`, CONVERT( `pm`.`CCB_STARTUP_Remarks` USING UTF8) AS `CCB_STARTUP_Remarks`, CONVERT( `pm`.`Ready2StartTestRemarks` USING UTF8) AS `Ready2StartTestRemarks`, `pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`, CONVERT( `pm`.`CCBReady4SPRINT_Remarks` USING UTF8) AS `CCBReady4SPRINT_Remarks`, CONVERT( `pm`.`Ready4SprintTestRemarks` USING UTF8) AS `Ready4SprintTestRemarks`, `pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`, CONVERT( `pm`.`CCBReady4UAT_Remarks` USING UTF8) AS `CCBReady4UAT_Remarks`, CONVERT( `pm`.`Ready4UATTestRemarks` USING UTF8) AS `Ready4UATTestRemarks`, `rm`.`LastRegistration` AS `LastRegistrationDate`, IFNULL(`pm`.`UAT_Start_Date`, `rm`.`UAT_StartDate`) AS `UAT_START_DATE`, `rm`.`UAT_END_DATE` AS `UAT_END_DATE`, `rm`.`FREEZE_DATE` AS `FREEZE_DATE`, `pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`, CONVERT( `pm`.`CCBReady4PROD_Remarks` USING UTF8) AS `CCBReady4PROD_Remarks`, CONVERT( `pm`.`Ready4ProdTestRemarks` USING UTF8) AS `Ready4ProdTestRemarks`, CONVERT( `pm`.`CCB_CLOSURE_Remarks` USING UTF8) AS `CCB_CLOSURE_Remarks`, `pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`,`pm`.`Project_Manager` AS `Project_Manager`, `tr`.`UNIT_TEST_PATH` AS `unitTestReportPath`, `tr`.`UAT_TEST_PATH` AS `UAT_TestReportPath`, `tr`.`AUTOMATION_PATH` AS `automationReportPath`, `tr`.`PERFORMANCE_PATH` AS `performanceReportPath`, `tr`.`SECURITY_TEST_PATH` AS `securityReportPath`, `tr`.`IT_TEST_PATH` AS `IT_TestReportPath`, `tr`.`CCB_GATING_PATH` AS `CCB_GATING_PATH`, `tr`.`MASTER_TEST_PLAN_PATH` AS `MASTER_TEST_PLAN_PATH`, `pm`.`Jira_defect_projects` AS `Jira_defect_projects`, `pm`.`DetailedTestStatus` AS `DetailedTestStatus`, `pm`.`Test_Coordinator_email` AS `Test_Coordinator_email`, `pm`.`Project_Manager_email` AS `Project_Manager_email`,`pm`.`Scrum_Master_email` AS `Scrum_Master_email`, `pm`.`Delivery_Master_Lead_email` AS `Delivery_Master_Lead_email`, SUBSTRING_INDEX(`pm`.`Jira_defect_projects`, ',', 1) AS `JIRA_KEY`, CONCAT('http://weconnect.pvgroup.intranet/project/general/release/Lists/CCB%20Dashboard/EditForm.aspx?ID=', `pm`.`SP_ID`) AS `SP_ProjectEditLink`     FROM ((`v_release_moments` `rm` LEFT JOIN `project_milestones_status` `pm` ON (CONVERT( `rm`.`releasename` USING UTF8) = REPLACE(TRIM(SUBSTRING_INDEX(`pm`.`ReleaseName`, '-', 1)), '+', ''))) LEFT JOIN `v_test_reports` `tr` ON (`pm`.`ReleaseName` = `tr`.`ReleaseName`    AND `pm`.`ProjectReference` = `tr`.`ProjectReference`)) ; 


CREATE  OR REPLACE VIEW `v_release_milestones_status` AS SELECT REPLACE(TRIM(SUBSTRING_INDEX(`pm`.`ReleaseName`, '-', 1)),'+',  '') AS `ReleaseName`,`pm`.`ReleaseName` AS `ReleaseMoment`, `rm`.`ReleaseName` AS `RM_ReleaseName`, IFNULL(`rm`.`ReleaseDate`, 'No Data') AS `ReleaseDate`, `pm`.`ProjectReference` AS `ProjectReference`, `pm`.`ProjectName` AS `ProjectName1`, CONCAT(`pm`.`ProjectReference`, ' - ', `pm`.`ProjectName`) AS `ProjectName`, IFNULL(`rm`.`ReleaseTestStatus`,  'No Data') AS `ReleaseTestStatus`, IFNULL(`rm`.`TestKeyMessage`, 'No Data') AS `TestKeyMessage`, IFNULL(`rm`.`Risks_Issues`, 'No Data') AS `Risks_Issues`, IFNULL(`rm`.`LastRegistration`, 'No Data ') AS `LastRegistration`, IFNULL(`rm`.`CCB_Ready4SPRINT`, 'No Data') AS `CCB_Ready4SPRINT`, IFNULL(`rm`.`CCB_Ready4UAT`, 'No Data') AS `CCB_Ready4UAT`, IFNULL(`rm`.`UAT_StartDate`, 'No Data ') AS `UAT_StartDate`, IFNULL(`rm`.`End_UAT`, 'No Data') AS `End_UAT`, IFNULL(`rm`.`Freeze`, 'No Data') AS `Freeze`, IFNULL(`rm`.`CCB_Ready4PROD`, 'No Data') AS `CCB_Ready4PROD`, IFNULL(`rm`.`CCB_Closure`, 'No Data') AS `CCB_Closure`, IFNULL(`rm`.`ReleaseDate`, 'No Data') AS `ReleaseDate1`, `rm`.`ReleaseDate` AS `RM_releaseDate`, CONCAT('http://weconnect.pvgroup.intranet/project/general/release/Lists/Release%20Milestones%20Overview/EditForm.aspx?ID=', `rm`.`SP_ID`) AS `SP_ReleaseEditLink` FROM (`project_milestones_status` `pm` LEFT JOIN `release_milestones_status` `rm` ON (`rm`.`ReleaseName` = REPLACE(TRIM(SUBSTRING_INDEX(`pm`.`ReleaseName`, '-', 1)), '+', ''))) GROUP BY `pm`.`ReleaseName` ORDER BY `pm`.`ReleaseName` ;


/** CLOSED DEFECTS IMPLEMENTATION */
CREATE OR REPLACE  VIEW `v_defects_data` AS SELECT  `d`.`defectId` AS `defectId`, `d`.`defectKey` AS `defectKey`, `d`.`projectKey` AS `projectKey`,  `d`.`projectName` AS `projectName`, `d`.`projectCategory` AS `projectCategory`, IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`, `pm`.`ProjectReference` AS `projectReference`, IFNULL(`d`.`resolution`, 'Un-Defined') AS `resolution`, IFNULL(`d`.`defectSevirity`, 'Un-Defined') AS `defectSevirity`, IFNULL(`d`.`product`, 'Un-Defined') AS `product`, CASE WHEN `d`.`testLevelDetected` LIKE '%system%' THEN 'System Test' WHEN `d`.`testLevelDetected` LIKE '%sprint%' THEN 'Sprint Test' WHEN `d`.`testLevelDetected` LIKE '%IT%' THEN 'Integration Test' WHEN `d`.`testLevelDetected` LIKE '%UAT%' THEN 'UAT' WHEN `d`.`testLevelDetected` LIKE '%unit%' THEN 'Unit Test' ELSE 'Un-Defined' END AS `testLevelDetected`, CASE WHEN  `d`.`testTypes` LIKE '%func%' THEN 'Functional' WHEN `d`.`testTypes` LIKE '%aut%' THEN 'Automation' WHEN `d`.`testTypes` LIKE '%per%' THEN 'Performance' WHEN `d`.`testTypes` LIKE '%sec%' THEN 'Security Test' WHEN `d`.`testTypes` LIKE '%uni%' THEN 'Unit Test' ELSE 'Un-Defined' END AS `testTypes`, CASE WHEN `d`.`status` LIKE '%To Validate%' THEN 'UAT Ready' WHEN `d`.`status` LIKE '%To Verify%' THEN 'In Test' WHEN `d`.`status` LIKE '%To Groom%' THEN 'In Test' WHEN `d`.`status` LIKE '%In Test%' THEN 'In Test' WHEN `d`.`status` LIKE '%UAT on-going%' THEN 'In Test' WHEN `d`.`status` LIKE '%Ready%' THEN 'UAT Ready' WHEN `d`.`status` LIKE '%open%' THEN 'Open' WHEN `d`.`status` LIKE '%sprintable%' THEN 'In Progress' WHEN `d`.`status` LIKE '%progress%' THEN 'In Progress' WHEN `d`.`status` LIKE '%Analysis%' THEN 'In Progress' WHEN `d`.`status` LIKE '%Business Validated%' THEN 'In Progress' WHEN `d`.`status` LIKE '%review%' THEN 'In Review' WHEN `d`.`status` LIKE '%incep%' THEN 'In Review' WHEN `d`.`status` LIKE '%promot%' THEN 'To Promote' WHEN `d`.`status` LIKE 'open' THEN 'Open' WHEN `d`.`status` LIKE 'Backlog' THEN 'Open' WHEN `d`.`status` LIKE 'Next' THEN 'Open' WHEN `d`.`status` LIKE '%resol%' THEN 'Resolved' WHEN `d`.`status` LIKE '%done%' THEN 'Resolved' WHEN `d`.`status` LIKE '%reopened%' THEN 'Reopened' WHEN `d`.`status` LIKE '%closed%' THEN 'Closed' WHEN `d`.`status` LIKE '%prd ready%' THEN 'Closed' ELSE `d`.`status` END AS `status`, `d`.`createdOn` AS `createdOn`, `d`.`summary` AS `summary`, IFNULL(`d`.`createdUserName`, 'UnAssigned') AS `createdUserName`, IFNULL(`d`.`priority`, 'Un-Defined') AS `priority`, IFNULL(`d`.`assigneeName`, 'UnAssigned') AS `assigneeName`, IFNULL(`d`.`severity`, 'Un-Defined') AS `severity`, IFNULL(`d`.`component`, 'No Components') AS `component`, IFNULL(`d`.`sprintId`, 'Un-Defined') AS `sprintId`, IFNULL(`d`.`reporter`, 'UnAssigned') AS `reporter`, IFNULL(`d`.`progress`, 'Un-Defined') AS `progress`, IFNULL(`d`.`stereoType`, 'Un-Defined') AS `stereoType`, IFNULL(`d`.`epicLink`, 'Un-Defined') AS `epicLink`,  IFNULL(`d`.`fixVersions`, 'Un-Defined') AS `fixVersions`, `pm`.`ReleaseName` AS `ReleaseName`, IFNULL(`d`.`issueType`, 'Un-Defined') AS `issueType`, IFNULL(`d`.`parentKey`, 'Un-Defined') AS `parentKey`, IFNULL(`d`.`storyDefectIdFixVersion`, 'Un-Defined') AS `storyDefectIdFixVersion`, REPLACE(`pm`.`JIRA_KEY`, ' ', '') AS `JIRA_KEY` FROM (`defect_details` `d` JOIN `v_project_wise_defect_keys` `pm` ON (`d`.`projectKey` = REPLACE(`pm`.`JIRA_KEY`, ' ', '') AND `d`.`fixVersions` = `pm`.`ReleaseName`)) WHERE `d`.`fixVersions` <> 'Un-Defined';


/** Delete LOGIC IMPLEMENTATION **/
ALTER TABLE `defect_details`  ADD COLUMN `updatedOn` TIMESTAMP NULL DEFAULT NULL AFTER `storyDefectIdFixVersion`, ADD COLUMN `insertedOn` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP() AFTER `updatedOn`;

ALTER TABLE `story_defect_details` ADD COLUMN `insertedOn` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP() AFTER `issueType`, ADD COLUMN `updatedOn` TIMESTAMP NULL DEFAULT NULL AFTER `insertedOn`;

/** Component Field Inclusion for Filter support **/

ALTER TABLE `iar_byld_1`.`test_execution_results` 
ADD COLUMN `component` VARCHAR(450) NULL AFTER `requirementId`;

ALTER TABLE `iar_byld_1`.`requirement_details` 
ADD COLUMN `component` VARCHAR(450) NULL AFTER `releaseId`;

CREATE OR REPLACE VIEW `v_test_results` AS  SELECT  `test_execution_results`.`id` AS `id`,  TRIM(SUBSTRING_INDEX(`test_execution_results`.`projectName`,  '-',  1)) AS `ProjectReference`, TRIM(SUBSTR(`test_execution_results`.`projectName`,LOCATE('-',`test_execution_results`.`projectName`) + 1)) AS `ProjectName`,  `project_milestones_status`.`Domain` AS `DomainName`,  `test_execution_results`.`projectName` AS `Project`,  `test_execution_results`.`releaseName` AS `ReleaseName`,  `test_execution_results`.`hierarchy` AS
 `Hierarchy`,  NULLIF(`test_execution_results`.`folder`, '') AS `Folder`,  NULLIF(`test_execution_results`.`subFolder`, '') AS `subFolder`,  NULLIF(`test_execution_results`.`subFolder1`,   '') AS `subFolder1`,  NULLIF(`test_execution_results`.`subFolder2`,   '') AS `subFolder2`,  `test_execution_results`.`createdOn` AS `CreatedOn`,  `test_execution_results`.`requirementId` AS `RequirementId`,  `test_execution_results`.`reqAltId` AS `ReqAltId`,  `test_execution_results`.`requirementName` AS `RequirementName`,  `test_execution_results`.`environment` AS `Environment`, `test_execution_results`.`testCaseAltId` AS `RestCaseAltId`,  `test_execution_results`.`testCaseName` AS `RestCaseName`,  `test_execution_results`.`testCaseId` AS `TestCaseId`,  `test_execution_results`.`automation` AS `Automation`,  `test_execution_results`.`cycle` AS `Cycle`,  `test_execution_results`.`testStatus` AS `tStatus`,  CASE  WHEN `test_execution_results`.`testStatus` LIKE '%fail%' THEN 'Fail'  WHEN `test_execution_results`.`testStatus` LIKE '%pass%' THEN 'Pass'   WHEN `test_execution_results`.`testStatus` LIKE '%incomp%' THEN 'Incomplete' WHEN `test_execution_results`.`testStatus` LIKE '%blocked%' THEN 'Blocked'   WHEN `test_execution_results`.`testStatus` LIKE '%Not exec%' THEN 'Not Executed' WHEN `test_execution_results`.`testStatus` LIKE '%N/A%' THEN 'N/A'      ELSE 'Not Executed' END AS `testStatus`, `test_execution_results`.`executedOn` AS `ExecutedOn`, `test_execution_results`.`tags` AS `Tags`, `test_execution_results`.`priority` AS `priority`,  `test_execution_results`.`defects` AS `Defects`,  `test_execution_results`.`executedBy` AS `ExecutedBy`,  `test_execution_results`.`createdBy` AS `CreatedBy`,  `test_execution_results`.`insertedOn` AS `insertedOn`,  `test_execution_results`.`subFolderLevel` AS `subFolderLevel`, `test_execution_results`.`updatedOn` AS `updatedOn` , `test_execution_results`.`component` AS `component`     FROM  (`test_execution_results`  LEFT JOIN `project_milestones_status` ON (TRIM(CONVERT( SUBSTRING_INDEX(`test_execution_results`.`projectName`, '-', 1) USING UTF8)) = `project_milestones_status`.`ProjectReference`  AND CONVERT(TRIM(`test_execution_results`.`releaseName`) USING UTF8) = `project_milestones_status`.`ReleaseName`)) ;

 /* Subfolders addition for REquirement Details*/
 ALTER TABLE `iar_byld_1`.`requirement_details` 
ADD COLUMN `RequirementTreeSubNode2` VARCHAR(450) NULL AFTER `RequirementTreeSubNode1`,
ADD COLUMN `RequirementTreeSubNode3` VARCHAR(450) NULL AFTER `RequirementTreeSubNode2`,
ADD COLUMN `RequirementTreeSubNode4` VARCHAR(450) NULL AFTER `RequirementTreeSubNode3`,
ADD COLUMN `RequirementTreeSubNode5` VARCHAR(450) NULL AFTER `RequirementTreeSubNode4`,
ADD COLUMN `RequirementTreeSubNode6` VARCHAR(450) NULL AFTER `RequirementTreeSubNode5`,
ADD COLUMN `RequirementTreeSubNode7` VARCHAR(450) NULL AFTER `RequirementTreeSubNode6`,
ADD COLUMN `RequirementTreeSubNode8` VARCHAR(450) NULL AFTER `RequirementTreeSubNode7`,
ADD COLUMN `RequirementTreeSubNode9` VARCHAR(450) NULL AFTER `RequirementTreeSubNode8`,
ADD COLUMN `RequirementTreeSubNode10` VARCHAR(450) NULL AFTER `RequirementTreeSubNode9`,
ADD COLUMN `RequirementTreeSubNode11` VARCHAR(450) NULL AFTER `RequirementTreeSubNode10`,
ADD COLUMN `RequirementTreeSubNode12` VARCHAR(450) NULL AFTER `RequirementTreeSubNode11`;

/** subfolders addition for reqiuirement dettails view */

CREATE OR REPLACE VIEW `v_temp_actualtestid_wise_req` AS with recursive t as (select `id` AS `id`,`projectName` AS `projectName`,`releaseName` AS `releaseName`,`requirementTreePath` AS `requirementTreePath`,`RequirementTreeNode` AS `RequirementTreeNode`,`RequirementTreeSubNode` AS `RequirementTreeSubNode`, `RequirementTreeSubNode1` AS `RequirementTreeSubNode1`, `RequirementTreeSubNode2` AS `RequirementTreeSubNode2`,`RequirementTreeSubNode3` AS `RequirementTreeSubNode3`,`RequirementTreeSubNode4` AS `RequirementTreeSubNode4`, `RequirementTreeSubNode5` AS `RequirementTreeSubNode5`, `RequirementTreeSubNode6` AS `RequirementTreeSubNode6`, `RequirementTreeSubNode7` AS `RequirementTreeSubNode7`, `RequirementTreeSubNode8` AS `RequirementTreeSubNode8`,`RequirementTreeSubNode9` AS `RequirementTreeSubNode9`, `RequirementTreeSubNode10` AS `RequirementTreeSubNode10`, `RequirementTreeSubNode11` AS `RequirementTreeSubNode11`, `RequirementTreeSubNode12` AS `RequirementTreeSubNode12`,`RequirementID` AS `RequirementID`,`requirementName` AS `requirementName`,`priority` AS `priority`,`createdOn` AS `createdOn`,`reqCreationDate` AS `reqCreationDate`,`createdBy` AS `createdBy`,`lastModifiedBy` AS `lastModifiedBy`,`lastModifiedOn` AS `lastModifiedOn`,`externalId` AS `externalId`,`requirementType` AS `requirementType`,`ReqStatus` AS `ReqStatus`,`attachmentCount` AS `attachmentCount`,`releaseIds` AS `releaseIds`,`requirementTreeIds` AS `requirementTreeIds`,`reqReleaseTCCountMapId` AS `reqReleaseTCCountMapId`,`reqReleaseTCCountMapReqId` AS `reqReleaseTCCountMapReqId`,`reqReleaseTCCountMapReleaseId` AS `reqReleaseTCCountMapReleaseId`,`reqReleaseTCCountMapTCCount` AS `reqReleaseTCCountMapTCCount`,`projectId` AS `projectId`,`releaseNames` AS `releaseNames`,`actualTestcaseIds` AS `actualTestcaseIds`,`insertedOn` AS `insertedOn`,`updatedOn` AS `updatedOn` from `iar_byld_1`.`requirement_details`), n as (select 1 AS `n` union select `n`.`n` + 1 AS `n + 1` from (`n` join `t`) where `n`.`n` <= octet_length(`t`.`actualTestcaseIds`) - octet_length(replace(`t`.`actualTestcaseIds`,',','')))select distinct substring_index(substring_index(`t`.`actualTestcaseIds`,',',`n`.`n`),',',-1) AS `actualTestIDs`,`t`.`id` AS `id`,`t`.`projectName` AS `projectName`,`t`.`releaseName` AS `releaseName`,`t`.`requirementTreePath` AS `requirementTreePath`,`t`.`RequirementTreeNode` AS `RequirementTreeNode`,`t`.`RequirementTreeSubNode` AS `RequirementTreeSubNode`,`t`.`RequirementTreeSubNode1` AS `RequirementTreeSubNode1`,t.RequirementTreeSubNode2, `t`.`RequirementTreeSubNode3` , `t`.`RequirementTreeSubNode4` , `t`.`RequirementTreeSubNode5` , `t`.`RequirementTreeSubNode6` ,`t`.`RequirementTreeSubNode7`, `t`.`RequirementTreeSubNode8` , `t`.`RequirementTreeSubNode9` ,`t`.`RequirementTreeSubNode10` , `t`.`RequirementTreeSubNode11`, `t`.`RequirementTreeSubNode12` , `t`.`RequirementID` AS `RequirementID`,`t`.`requirementName` AS `requirementName`,`t`.`priority` AS `priority`,`t`.`createdOn` AS `createdOn`,`t`.`reqCreationDate` AS `reqCreationDate`,`t`.`createdBy` AS `createdBy`,`t`.`lastModifiedBy` AS `lastModifiedBy`,`t`.`lastModifiedOn` AS `lastModifiedOn`,`t`.`externalId` AS `externalId`,`t`.`requirementType` AS `requirementType`,`t`.`ReqStatus` AS `ReqStatus`,`t`.`attachmentCount` AS `attachmentCount`,`t`.`releaseIds` AS `releaseIds`,`t`.`requirementTreeIds` AS `requirementTreeIds`,`t`.`reqReleaseTCCountMapId` AS `reqReleaseTCCountMapId`,`t`.`reqReleaseTCCountMapReqId` AS `reqReleaseTCCountMapReqId`,`t`.`reqReleaseTCCountMapReleaseId` AS `reqReleaseTCCountMapReleaseId`,`t`.`reqReleaseTCCountMapTCCount` AS `reqReleaseTCCountMapTCCount`,`t`.`projectId` AS `projectId`,`t`.`releaseNames` AS `releaseNames`,`t`.`actualTestcaseIds` AS `actualTestcaseIds`,`t`.`insertedOn` AS `insertedOn`,`t`.`updatedOn` AS `updatedOn` from (`n` join (select `id` AS `id`,`projectName` AS `projectName`,`releaseName` AS `releaseName`,`requirementTreePath` AS `requirementTreePath`,`RequirementTreeNode` AS `RequirementTreeNode`,`RequirementTreeSubNode` AS `RequirementTreeSubNode`,`RequirementTreeSubNode1` AS `RequirementTreeSubNode1`,`RequirementTreeSubNode2` AS `RequirementTreeSubNode2`,`RequirementTreeSubNode3` AS `RequirementTreeSubNode3`, `RequirementTreeSubNode4` AS `RequirementTreeSubNode4`, `RequirementTreeSubNode5` AS `RequirementTreeSubNode5`, `RequirementTreeSubNode6` AS `RequirementTreeSubNode6`, `RequirementTreeSubNode7` AS `RequirementTreeSubNode7`, `RequirementTreeSubNode8` AS `RequirementTreeSubNode8`, `RequirementTreeSubNode9` AS `RequirementTreeSubNode9`, `RequirementTreeSubNode10` AS `RequirementTreeSubNode10`, `RequirementTreeSubNode11` AS `RequirementTreeSubNode11`, `RequirementTreeSubNode12` AS `RequirementTreeSubNode12`,`RequirementID` AS `RequirementID`,`requirementName` AS `requirementName`,`priority` AS `priority`,`createdOn` AS `createdOn`,`reqCreationDate` AS `reqCreationDate`,`createdBy` AS `createdBy`,`lastModifiedBy` AS `lastModifiedBy`,`lastModifiedOn` AS `lastModifiedOn`,`externalId` AS `externalId`,`requirementType` AS `requirementType`,`ReqStatus` AS `ReqStatus`,`attachmentCount` AS `attachmentCount`,`releaseIds` AS `releaseIds`,`requirementTreeIds` AS `requirementTreeIds`,`reqReleaseTCCountMapId` AS `reqReleaseTCCountMapId`,`reqReleaseTCCountMapReqId` AS `reqReleaseTCCountMapReqId`,`reqReleaseTCCountMapReleaseId` AS `reqReleaseTCCountMapReleaseId`,`reqReleaseTCCountMapTCCount` AS `reqReleaseTCCountMapTCCount`,`projectId` AS `projectId`,`releaseNames` AS `releaseNames`,`actualTestcaseIds` AS `actualTestcaseIds`,`insertedOn` AS `insertedOn`,`updatedOn` AS `updatedOn` from `iar_byld_1`.`requirement_details`) `t`);

CREATE  OR REPLACE VIEW `v_requirement_details` AS SELECT  TRIM(SUBSTRING_INDEX(`rq`.`projectName`, '-', 1)) AS `ProjectReference`, TRIM(SUBSTR(`rq`.`projectName`, LOCATE('-', `rq`.`projectName`) + 1)) AS `ProjectName`, `rq`.`projectName` AS `project`, `rq`.`releaseName` AS `ReleaseName`, `rq`.`requirementTreePath` AS `RequirementTreePath`, `rq`.`RequirementTreeNode` AS `RequirementTreeNode`, `rq`.`RequirementTreeSubNode` AS `RequirementTreeSubNode`, `rq`.`RequirementTreeSubNode1` AS `RequirementTreeSubNode1`, `rq`.`RequirementTreeSubNode2` AS `RequirementTreeSubNode2`, `rq`.`RequirementTreeSubNode3` AS `RequirementTreeSubNode3`, `rq`.`RequirementTreeSubNode4` AS `RequirementTreeSubNode4`,  `rq`.`RequirementTreeSubNode5` AS `RequirementTreeSubNode5`, `rq`.`RequirementTreeSubNode6` AS `RequirementTreeSubNode6`, `rq`.`RequirementTreeSubNode7` AS `RequirementTreeSubNode7`, `rq`.`RequirementTreeSubNode8` AS `RequirementTreeSubNode8`, `rq`.`RequirementTreeSubNode9` AS `RequirementTreeSubNode9`, `rq`.`RequirementTreeSubNode10` AS `RequirementTreeSubNode10`,     `rq`.`RequirementTreeSubNode11` AS `RequirementTreeSubNode11`, `rq`.`RequirementTreeSubNode12` AS `RequirementTreeSubNode12`,`rq`.`RequirementID` AS `RequirementID`, `rq`.`actualTestcaseIds` AS `actualTestCaseIDs`, `rq`.`reqReleaseTCCountMapTCCount` AS `MappedTCCount`, `rq`.`actualTestIDs` AS `actualTestIds`, `tr`.`testCaseId` AS `testcaseid`, `tr`.`testStatus` AS `tetstatus`, IFNULL(`tr`.`testStatus`, 'Not Executed') AS `testStatus`, IFNULL(`tr`.`testStatus`, 'Not Executed') AS `reqCoverageStatus`, `tr`.`releaseName` AS `fixVersions`, `tr`.`cycle` AS `cycle`, `tr`.`defects` AS `defectId`,  `tr`.`executedOn` AS `executedOn`, REPLACE(`rd`.`defectids`, ',,', ',') AS `defectids`, CASE WHEN (`rq`.`actualTestcaseIds` IS NULL OR `rq`.`actualTestcaseIds` = '')  THEN 'Not Covered' ELSE 'Covered' END AS `ReQCoverage` FROM  ((`iar_byld_1`.`v_temp_actualtestid_wise_req` `rq` LEFT JOIN `iar_byld_1`.`v_temp_req_wise_test_results` `tr` ON (`rq`.`RequirementID` = `tr`.`requirementId` AND `rq`.`actualTestIDs` = `tr`.`testCaseId`)) LEFT JOIN `iar_byld_1`.`v_temp_req_wise_test_wise_defects` `rd` ON (`tr`.`requirementId` = `rd`.`requirementID`));


/** REQUIREMENT Wise TEST DETAILS INCLUSION  REQ Bracnh changes **/

/** Requirement View Optimization  **/

CREATE TABLE `requirement_wise_test_details` ( `id` int(11) NOT NULL AUTO_INCREMENT, `projectName` varchar(450) DEFAULT NULL, `releaseName` varchar(200) DEFAULT NULL,`requirementTreePath` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,   `RequirementTreeNode` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,  `RequirementTreeSubNode` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,   `RequirementTreeSubNode1` text DEFAULT NULL,   `RequirementID` varchar(100) DEFAULT NULL,   `requirementName` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,   `priority` varchar(45) DEFAULT NULL,   `createdOn` datetime DEFAULT NULL,   `reqCreationDate` date DEFAULT NULL,   `createdBy` varchar(200) DEFAULT NULL,   `lastModifiedBy` varchar(200) DEFAULT NULL,   `lastModifiedOn` datetime DEFAULT NULL,   `externalId` varchar(100) DEFAULT NULL,   `requirementType` varchar(50) DEFAULT NULL,   `ReqStatus` varchar(100) DEFAULT NULL,   `attachmentCount` varchar(10) DEFAULT NULL,   `releaseIds` varchar(45) DEFAULT NULL,   `requirementTreeIds` varchar(50) DEFAULT NULL,  `reqReleaseTCCountMapId` varchar(100) DEFAULT NULL,   `reqReleaseTCCountMapReqId` varchar(100) DEFAULT NULL,   `reqReleaseTCCountMapReleaseId` varchar(100) DEFAULT NULL,   `reqReleaseTCCountMapTCCount` varchar(100) DEFAULT NULL,   `projectId` varchar(10) DEFAULT NULL,   `releaseNames` varchar(450) DEFAULT NULL,   `actualTestcaseIds` longtext DEFAULT NULL,   `reqTestCaseId` int(11) DEFAULT NULL,   `insertedOn` timestamp NULL DEFAULT current_timestamp(),   `updatedOn` timestamp NULL DEFAULT NULL,   `releaseId` int(11) DEFAULT NULL,   PRIMARY KEY (`id`),  UNIQUE KEY `UNIQ` (`projectId`,`releaseId`,`RequirementID`,`requirementTreeIds`,`reqTestCaseId`,`actualTestcaseIds`) USING HASH ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE OR REPLACE VIEW `v_test_results` AS select `test_execution_results`.`id` AS `id`,trim(substring_index(`test_execution_results`.`projectName`,'-',1)) AS `ProjectReference`,trim(substr(`test_execution_results`.`projectName`,locate('-',`test_execution_results`.`projectName`) + 1)) AS `ProjectName`,`project_milestones_status`.`Domain` AS `DomainName`,`test_execution_results`.`projectName` AS `Project`,`test_execution_results`.`releaseName` AS `ReleaseName`,`test_execution_results`.`hierarchy` AS `Hierarchy`,nullif(`test_execution_results`.`folder`,'') AS `Folder`,nullif(`test_execution_results`.`subFolder`,'') AS `subFolder`,nullif(`test_execution_results`.`subFolder1`,'') AS `subFolder1`,nullif(`test_execution_results`.`subFolder2`,'') AS `subFolder2`,`test_execution_results`.`createdOn` AS `CreatedOn`,`test_execution_results`.`requirementId` AS `RequirementId`,`test_execution_results`.`reqAltId` AS `ReqAltId`,`test_execution_results`.`requirementName` AS `RequirementName`,`test_execution_results`.`environment` AS `Environment`,`test_execution_results`.`testCaseAltId` AS `RestCaseAltId`,`test_execution_results`.`testCaseName` AS `RestCaseName`,`test_execution_results`.`testCaseId` AS `TestCaseId`,`test_execution_results`.`automation` AS `Automation`,`test_execution_results`.`cycle` AS `Cycle`,`test_execution_results`.`testStatus` AS `tStatus`,case when `test_execution_results`.`testStatus` like '%fail%' then 'Fail' when `test_execution_results`.`testStatus` like '%pass%' then 'Pass' when `test_execution_results`.`testStatus` like '%incomp%' then 'Incomplete' when `test_execution_results`.`testStatus` like '%blocked%' then 'Blocked' when `test_execution_results`.`testStatus` like '%Not exec%' then 'Not Executed' when `test_execution_results`.`testStatus` like '%N/A%' then 'N/A' else 'Not Executed' end AS `testStatus`,`test_execution_results`.`executedOn` AS `ExecutedOn`,`test_execution_results`.`tags` AS `Tags`,`test_execution_results`.`priority` AS `priority`,`test_execution_results`.`defects` AS `Defects`,`test_execution_results`.`executedBy` AS `ExecutedBy`,`test_execution_results`.`createdBy` AS `CreatedBy`,`test_execution_results`.`insertedOn` AS `insertedOn`,`test_execution_results`.`subFolderLevel` AS `subFolderLevel`,`test_execution_results`.`updatedOn` AS `updatedOn` from (`test_execution_results` left join `project_milestones_status` on(trim(convert(substring_index(`test_execution_results`.`projectName`,'-',1) using utf8)) = `project_milestones_status`.`ProjectReference` and convert(trim(`test_execution_results`.`releaseName`) using utf8) = `project_milestones_status`.`ReleaseName`));

CREATE OR REPLACE VIEW `rv_temp_req_wise_test_results` AS with _test_execution_results as (select `test_execution_results`.`id` AS `id`,`test_execution_results`.`projectName` AS `projectName`,`test_execution_results`.`releaseName` AS `releaseName`,`test_execution_results`.`hierarchy` AS `hierarchy`,`test_execution_results`.`folder` AS `folder`,`test_execution_results`.`subFolder` AS `subFolder`,`test_execution_results`.`subFolder1` AS `subFolder1`,`test_execution_results`.`subFolder2` AS `subFolder2`,`test_execution_results`.`subfolder3` AS `subfolder3`,`test_execution_results`.`subfolder4` AS `subfolder4`,`test_execution_results`.`subfolder5` AS `subfolder5`,`test_execution_results`.`subfolder6` AS `subfolder6`,`test_execution_results`.`subfolder7` AS `subfolder7`,`test_execution_results`.`subfolder8` AS `subfolder8`,`test_execution_results`.`subfolder9` AS `subfolder9`,`test_execution_results`.`subfolder10` AS `subfolder10`,`test_execution_results`.`createdOn` AS `createdOn`,`test_execution_results`.`requirementId` AS `requirementId`,`test_execution_results`.`reqAltId` AS `reqAltId`,`test_execution_results`.`requirementName` AS `requirementName`,`test_execution_results`.`environment` AS `environment`,`test_execution_results`.`testCaseAltId` AS `testCaseAltId`,`test_execution_results`.`testCaseName` AS `testCaseName`,`test_execution_results`.`testCaseId` AS `testCaseId`,`test_execution_results`.`automation` AS `automation`,`test_execution_results`.`cycle` AS `cycle`,`test_execution_results`.`testStatus` AS `testStatus`,`test_execution_results`.`executedOn` AS `executedOn`,`test_execution_results`.`tags` AS `tags`,`test_execution_results`.`defects` AS `defects`,`test_execution_results`.`executedBy` AS `executedBy`,`test_execution_results`.`createdBy` AS `createdBy`,`test_execution_results`.`insertedOn` AS `insertedOn`,`test_execution_results`.`subFolderLevel` AS `subFolderLevel`,`test_execution_results`.`updatedOn` AS `updatedOn`,`test_execution_results`.`cyclePhaseId` AS `cyclePhaseId`,`test_execution_results`.`tcrCatalogTreeId` AS `tcrCatalogTreeId`,`test_execution_results`.`preConditions` AS `preConditions`,`test_execution_results`.`releaseId` AS `releaseId`,`test_execution_results`.`projectId` AS `projectId`,`test_execution_results`.`priority` AS `priority`,row_number() over ( partition by `test_execution_results`.`testCaseId` order by `test_execution_results`.`executedOn` desc) AS `row_num` from `test_execution_results`)select `_test_execution_results`.`id` AS `id`,`_test_execution_results`.`projectName` AS `projectName`,`_test_execution_results`.`releaseName` AS `releaseName`,`_test_execution_results`.`hierarchy` AS `hierarchy`,`_test_execution_results`.`folder` AS `folder`,`_test_execution_results`.`subFolder` AS `subFolder`,`_test_execution_results`.`subFolder1` AS `subFolder1`,`_test_execution_results`.`subFolder2` AS `subFolder2`,`_test_execution_results`.`subfolder3` AS `subfolder3`,`_test_execution_results`.`subfolder4` AS `subfolder4`,`_test_execution_results`.`subfolder5` AS `subfolder5`,`_test_execution_results`.`subfolder6` AS `subfolder6`,`_test_execution_results`.`subfolder7` AS `subfolder7`,`_test_execution_results`.`subfolder8` AS `subfolder8`,`_test_execution_results`.`subfolder9` AS `subfolder9`,`_test_execution_results`.`subfolder10` AS `subfolder10`,`_test_execution_results`.`createdOn` AS `createdOn`,`_test_execution_results`.`requirementId` AS `requirementId`,`_test_execution_results`.`reqAltId` AS `reqAltId`,`_test_execution_results`.`requirementName` AS `requirementName`,`_test_execution_results`.`environment` AS `environment`,`_test_execution_results`.`testCaseAltId` AS `testCaseAltId`,`_test_execution_results`.`testCaseName` AS `testCaseName`,`_test_execution_results`.`testCaseId` AS `testCaseId`,`_test_execution_results`.`automation` AS `automation`,`_test_execution_results`.`cycle` AS `cycle`,`_test_execution_results`.`testStatus` AS `testStatus`,`_test_execution_results`.`executedOn` AS `executedOn`,`_test_execution_results`.`tags` AS `tags`,`_test_execution_results`.`defects` AS `defects`,`_test_execution_results`.`executedBy` AS `executedBy`,`_test_execution_results`.`createdBy` AS `createdBy`,`_test_execution_results`.`insertedOn` AS `insertedOn`,`_test_execution_results`.`subFolderLevel` AS `subFolderLevel`,`_test_execution_results`.`updatedOn` AS `updatedOn`,`_test_execution_results`.`cyclePhaseId` AS `cyclePhaseId`,`_test_execution_results`.`tcrCatalogTreeId` AS `tcrCatalogTreeId`,`_test_execution_results`.`preConditions` AS `preConditions`,`_test_execution_results`.`releaseId` AS `releaseId`,`_test_execution_results`.`projectId` AS `projectId`,`_test_execution_results`.`priority` AS `priority`,`_test_execution_results`.`row_num` AS `row_num` from `_test_execution_results` where `_test_execution_results`.`row_num` = 1;

CREATE OR REPLACE VIEW `rv_req_vs_defects` AS select `rv_temp_req_wise_test_results`.`requirementId` AS `requirementID`,`rv_temp_req_wise_test_results`.`testCaseId` AS `testcaseid`,group_concat(distinct `rv_temp_req_wise_test_results`.`defects` separator ',') AS `defects` from `rv_temp_req_wise_test_results` group by `rv_temp_req_wise_test_results`.`requirementId`;

CREATE OR REPLACE VIEW `rv_req_details` AS select trim(substring_index(`rq`.`projectName`,'-',1)) AS `ProjectReference`,trim(substr(`rq`.`projectName`,locate('-',`rq`.`projectName`) + 1)) AS `ProjectName`,`rq`.`projectName` AS `project`,`rq`.`releaseName` AS `releaseName`,`rq`.`RequirementID` AS `requirementId`,`rq`.`actualTestcaseIds` AS `actualTestCaseIds`,`rq`.`reqTestCaseId` AS `reqTestCaseid`,`rq`.`RequirementTreeNode` AS `RequirementTreeNode`,`rq`.`RequirementTreeSubNode` AS `RequirementTreeSubNode`,`rq`.`RequirementTreeSubNode1` AS `RequirementTreeSubNode1`,`rq`.`requirementName` AS `requirementName`,`rq`.`priority` AS `priority`,`rq`.`requirementType` AS `requirementType`,`rq`.`ReqStatus` AS `ReqStatus`,`rq`.`attachmentCount` AS `attachmentCount`,`rq`.`reqReleaseTCCountMapId` AS `reqReleaseTCCountMapId`,`rq`.`reqReleaseTCCountMapReqId` AS `reqReleaseTCCountMapReqId`,`rq`.`reqReleaseTCCountMapReleaseId` AS `reqReleaseTCCountMapReleaseId`,`rq`.`reqReleaseTCCountMapTCCount` AS `reqReleaseTCCountMapTCCount`,`rq`.`releaseNames` AS `releaseNames`,`tr`.`testStatus` AS `tetstatus`,`tr`.`testCaseId` AS `testcaseid`,`tr`.`requirementId` AS `testreqId`,`rq`.`releaseId` AS `reqReleaseId`,`rq`.`projectId` AS `reqProjectId`,`tr`.`releaseId` AS `releaseId`,`tr`.`projectId` AS `projectId`,`rq`.`releaseIds` AS `releaseIDs`,`rq`.`requirementTreeIds` AS `requirementTreeIds`,replace(`rd`.`defects`,',,',',') AS `defectids`,case when (`rq`.`actualTestcaseIds` is null or `rq`.`actualTestcaseIds` = '') then 'Not Covered' else 'Covered' end AS `ReQCoverage`,ifnull(`tr`.`testStatus`,'Not Covered') AS `testStatus`,ifnull(`tr`.`testStatus`,'Not Covered') AS `reqCoverageStatus` from ((`requirement_wise_test_details` `rq` left join `rv_temp_req_wise_test_results` `tr` on(`rq`.`releaseId` = `tr`.`releaseId` and `rq`.`projectId` = `tr`.`projectId` and `rq`.`RequirementID` = `tr`.`requirementId` and `rq`.`reqTestCaseId` = `tr`.`testCaseId`)) left join `rv_req_vs_defects` `rd` on(`rd`.`requirementID` = `rq`.`RequirementID`));


/** Updated ***/


CREATE TABLE `requirement_wise_test_details` (   `id` int(11) NOT NULL AUTO_INCREMENT,  `projectName` varchar(450) DEFAULT NULL,   `releaseName` varchar(200) DEFAULT NULL,  `requirementTreePath` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,  `RequirementTreeNode` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,  `RequirementTreeSubNode` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,  `RequirementTreeSubNode1` text DEFAULT NULL,  `RequirementID` varchar(100) DEFAULT NULL,  `requirementName` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,  `priority` varchar(45) DEFAULT NULL,  `createdOn` datetime DEFAULT NULL,  `reqCreationDate` date DEFAULT NULL,  `createdBy` varchar(200) DEFAULT NULL,  `lastModifiedBy` varchar(200) DEFAULT NULL,  `lastModifiedOn` datetime DEFAULT NULL,  `externalId` varchar(100) DEFAULT NULL,  `requirementType` varchar(50) DEFAULT NULL,  `ReqStatus` varchar(100) DEFAULT NULL,  `attachmentCount` varchar(10) DEFAULT NULL,  `releaseIds` varchar(45) DEFAULT NULL,  `requirementTreeIds` varchar(50) DEFAULT NULL,  `reqReleaseTCCountMapId` varchar(100) DEFAULT NULL,  `reqReleaseTCCountMapReqId` varchar(100) DEFAULT NULL,  `reqReleaseTCCountMapReleaseId` varchar(100) DEFAULT NULL,  `reqReleaseTCCountMapTCCount` varchar(100) DEFAULT NULL,  `projectId` varchar(10) DEFAULT NULL,  `releaseNames` varchar(450) DEFAULT NULL,  `actualTestcaseIds` longtext DEFAULT NULL,  `reqTestCaseId` int(11) DEFAULT NULL,  `insertedOn` timestamp NULL DEFAULT current_timestamp(),  `updatedOn` timestamp NULL DEFAULT NULL,  `releaseId` int(11) DEFAULT NULL,  PRIMARY KEY (`id`),  UNIQUE KEY `REQ_TEST_DETAILS` (`projectId`,`releaseId`,`RequirementID`,`requirementTreeIds`,`reqTestCaseId`) USING HASH) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

ALTER TABLE `iar_byld_1`.`requirement_wise_test_details` 
ADD COLUMN `parentReqId` INT(11) NULL AFTER `projectName`;

CREATE OR REPLACE VIEW `rv_temp_req_wise_test_results` AS with _test_execution_results as (select `test_execution_results`.`id` AS `id`,`test_execution_results`.`projectName` AS `projectName`,`test_execution_results`.`releaseName` AS `releaseName`,`test_execution_results`.`hierarchy` AS `hierarchy`,`test_execution_results`.`folder` AS `folder`,`test_execution_results`.`subFolder` AS `subFolder`,`test_execution_results`.`subFolder1` AS `subFolder1`,`test_execution_results`.`subFolder2` AS `subFolder2`,`test_execution_results`.`subfolder3` AS `subfolder3`,`test_execution_results`.`subfolder4` AS `subfolder4`,`test_execution_results`.`subfolder5` AS `subfolder5`,`test_execution_results`.`subfolder6` AS `subfolder6`,`test_execution_results`.`subfolder7` AS `subfolder7`,`test_execution_results`.`subfolder8` AS `subfolder8`,`test_execution_results`.`subfolder9` AS `subfolder9`,`test_execution_results`.`subfolder10` AS `subfolder10`,`test_execution_results`.`createdOn` AS `createdOn`,`test_execution_results`.`requirementId` AS `requirementId`,`test_execution_results`.`reqAltId` AS `reqAltId`,`test_execution_results`.`requirementName` AS `requirementName`,`test_execution_results`.`environment` AS `environment`,`test_execution_results`.`testCaseAltId` AS `testCaseAltId`,`test_execution_results`.`testCaseName` AS `testCaseName`,`test_execution_results`.`testCaseId` AS `testCaseId`,`test_execution_results`.`automation` AS `automation`,`test_execution_results`.`cycle` AS `cycle`,`test_execution_results`.`testStatus` AS `testStatus`,`test_execution_results`.`executedOn` AS `executedOn`,`test_execution_results`.`tags` AS `tags`,`test_execution_results`.`defects` AS `defects`,`test_execution_results`.`executedBy` AS `executedBy`,`test_execution_results`.`createdBy` AS `createdBy`,`test_execution_results`.`insertedOn` AS `insertedOn`,`test_execution_results`.`subFolderLevel` AS `subFolderLevel`,`test_execution_results`.`updatedOn` AS `updatedOn`,`test_execution_results`.`cyclePhaseId` AS `cyclePhaseId`,`test_execution_results`.`tcrCatalogTreeId` AS `tcrCatalogTreeId`,`test_execution_results`.`preConditions` AS `preConditions`,`test_execution_results`.`releaseId` AS `releaseId`,`test_execution_results`.`projectId` AS `projectId`,`test_execution_results`.`priority` AS `priority`,row_number() over ( partition by `test_execution_results`.`testCaseId` order by `test_execution_results`.`executedOn` desc) AS `row_num` from `test_execution_results`)select `_test_execution_results`.`id` AS `id`,`_test_execution_results`.`projectName` AS `projectName`,`_test_execution_results`.`releaseName` AS `releaseName`,`_test_execution_results`.`hierarchy` AS `hierarchy`,`_test_execution_results`.`folder` AS `folder`,`_test_execution_results`.`subFolder` AS `subFolder`,`_test_execution_results`.`subFolder1` AS `subFolder1`,`_test_execution_results`.`subFolder2` AS `subFolder2`,`_test_execution_results`.`subfolder3` AS `subfolder3`,`_test_execution_results`.`subfolder4` AS `subfolder4`,`_test_execution_results`.`subfolder5` AS `subfolder5`,`_test_execution_results`.`subfolder6` AS `subfolder6`,`_test_execution_results`.`subfolder7` AS `subfolder7`,`_test_execution_results`.`subfolder8` AS `subfolder8`,`_test_execution_results`.`subfolder9` AS `subfolder9`,`_test_execution_results`.`subfolder10` AS `subfolder10`,`_test_execution_results`.`createdOn` AS `createdOn`,`_test_execution_results`.`requirementId` AS `requirementId`,`_test_execution_results`.`reqAltId` AS `reqAltId`,`_test_execution_results`.`requirementName` AS `requirementName`,`_test_execution_results`.`environment` AS `environment`,`_test_execution_results`.`testCaseAltId` AS `testCaseAltId`,`_test_execution_results`.`testCaseName` AS `testCaseName`,`_test_execution_results`.`testCaseId` AS `testCaseId`,`_test_execution_results`.`automation` AS `automation`,`_test_execution_results`.`cycle` AS `cycle`,`_test_execution_results`.`testStatus` AS `testStatus`,`_test_execution_results`.`executedOn` AS `executedOn`,`_test_execution_results`.`tags` AS `tags`,`_test_execution_results`.`defects` AS `defects`,`_test_execution_results`.`executedBy` AS `executedBy`,`_test_execution_results`.`createdBy` AS `createdBy`,`_test_execution_results`.`insertedOn` AS `insertedOn`,`_test_execution_results`.`subFolderLevel` AS `subFolderLevel`,`_test_execution_results`.`updatedOn` AS `updatedOn`,`_test_execution_results`.`cyclePhaseId` AS `cyclePhaseId`,`_test_execution_results`.`tcrCatalogTreeId` AS `tcrCatalogTreeId`,`_test_execution_results`.`preConditions` AS `preConditions`,`_test_execution_results`.`releaseId` AS `releaseId`,`_test_execution_results`.`projectId` AS `projectId`,`_test_execution_results`.`priority` AS `priority`,`_test_execution_results`.`row_num` AS `row_num` from `_test_execution_results` where `_test_execution_results`.`row_num` = 1;

CREATE VIEW `rv_req_vs_defects` AS   SELECT  `rv_temp_req_wise_test_results`.`requirementId` AS `requirementID`, `rv_temp_req_wise_test_results`.`testCaseId` AS `testcaseid`,  GROUP_CONCAT(DISTINCT `rv_temp_req_wise_test_results`.`defects` SEPARATOR ',') AS `defects`  FROM  `rv_temp_req_wise_test_results`     GROUP BY `rv_temp_req_wise_test_results`.`requirementId`;



/** Updated **/

select trim(substring_index(`rq`.`projectName`,'-',1)) AS `ProjectReference`,trim(substr(`rq`.`projectName`,locate('-',`rq`.`projectName`) + 1)) AS `ProjectName`,`rq`.`projectName` AS `project`,`rq`.`releaseName` AS `ReleaseName`,`rq`.`requirementTreePath` AS `RequirementTreePath`,`rq`.`RequirementTreeNode` AS `RequirementTreeNode`,`rq`.`RequirementTreeSubNode` AS `RequirementTreeSubNode`,`rq`.`RequirementTreeSubNode1` AS `RequirementTreeSubNode1`,`rq`.`RequirementID` AS `RequirementID`,`rq`.`actualTestcaseIds` AS `actualTestCaseIDs`,`rq`.`reqReleaseTCCountMapTCCount` AS `MappedTCCount`,`rq`.`actualTestIDs` AS `actualTestIds`,`tr`.`testCaseId` AS `testcaseid`,ifnull(`tr`.`testStatus`,'Not Executed') AS `testStatus`,ifnull(`tr`.`testStatus`,'Not Executed') AS `reqCoverageStatus`,`tr`.`releaseName` AS `fixVersions`,`tr`.`cycle` AS `cycle`,`tr`.`defects` AS `defectId`,`tr`.`executedOn` AS `executedOn`,replace(`rd`.`defectids`,',,',',') AS `defectids`,case when (`rq`.`actualTestcaseIds` is null or `rq`.`actualTestcaseIds` = '') then 'Not Covered' else 'Covered' end AS `ReQCoverage` from ((`iar_byld_1`.`v_temp_actualtestid_wise_req` `rq` left join `iar_byld_1`.`v_temp_req_wise_test_results` `tr` on(`rq`.`RequirementID` = `tr`.`requirementId` and `rq`.`actualTestIDs` = `tr`.`testCaseId`)) left join `iar_byld_1`.`v_temp_req_wise_test_wise_defects` `rd` on(`tr`.`requirementId` = `rd`.`requirementID`)) ;

/** REQUIREMENT VIEW OPTIMIZATION CHANGES & SUBFOLDRS ADDITION  ***/

CREATE OR REPLACE VIEW `base_req` AS  SELECT TRIM(SUBSTRING_INDEX(`b`.`projectName`, '-', 1)) AS `ProjectReference`,TRIM(SUBSTR(`b`.`projectName`,LOCATE('-', `b`.`projectName`) + 1)) AS `ProjectName`, `b`.`projectName` AS `project`,`b`.`releaseName` AS `releaseName`,`b`.`id` AS `id`,`b`.`RequirementID` AS `requirementId`,`b`.`requirementTreeIds` AS `requirementTreeIds`,`b`.`reqReleaseTCCountMapTCCount` AS `reqReleaseTCCountMapTCCount`,`b`.`projectId` AS `projectId`,`b`.`actualTestcaseIds` AS `actualTestCaseIds`,`rd`.`reqTestCaseId` AS `reqTestCaseId`,`rd`.`parentReqId` AS `parentReqId`,`b`.`insertedOn` AS `insertedOn`, `b`.`updatedOn` AS `updatedOn`, `b`.`releaseId` AS `releaseId`,`b`.`RequirementTreeNode` AS `requirementTreeNode`, `b`.`RequirementTreeSubNode` AS `requirementTreeSubnode`, `b`.`RequirementTreeSubNode1` AS `requirementTreeSubnode1`, `b`.`RequirementTreeSubNode2` AS `RequirementTreeSubNode2`,  `b`.`RequirementTreeSubNode3` AS `RequirementTreeSubNode3`, `b`.`RequirementTreeSubNode4` AS `RequirementTreeSubNode4`, `b`.`RequirementTreeSubNode5` AS `RequirementTreeSubNode5`, `b`.`RequirementTreeSubNode6` AS `RequirementTreeSubNode6`, `b`.`RequirementTreeSubNode7` AS `RequirementTreeSubNode7`, `b`.`RequirementTreeSubNode8` AS `RequirementTreeSubNode8`, `b`.`RequirementTreeSubNode9` AS `RequirementTreeSubNode9`, `b`.`RequirementTreeSubNode10` AS `RequirementTreeSubNode10`,`b`.`RequirementTreeSubNode11` AS `RequirementTreeSubNode11`, `b`.`RequirementTreeSubNode12` AS `RequirementTreeSubNode12`  FROM (`requirement_wise_test_details` `rd`  JOIN `requirement_details` `b` ON (`b`.`id` = `rd`.`parentReqId`)) ;

CREATE OR REPLACE VIEW `req_details_wise_tests` AS   SELECT  `rq`.`id` AS `id`, `rq`.`ProjectReference` AS `projectReference`, `rq`.`ProjectName` AS `projectName`, `rq`.`releaseName` AS `releaseName`, `rq`.`requirementId` AS `requirementId`, `rq`.`actualTestCaseIds` AS `actualTestCaseIds`, `rq`.`reqTestCaseId` AS `reqTestCaseid`, `rq`.`requirementTreeNode` AS `requirementTreeNode`, `rq`.`requirementTreeSubnode` AS `requirementTreeSubnode`, `rq`.`requirementTreeSubnode1` AS `requirementTreeSubnode1`, `rq`.`RequirementTreeSubNode2` AS `RequirementTreeSubNode2`, `rq`.`RequirementTreeSubNode3` AS `RequirementTreeSubNode3`, `rq`.`RequirementTreeSubNode4` AS `RequirementTreeSubNode4`, `rq`.`RequirementTreeSubNode5` AS `RequirementTreeSubNode5`, `rq`.`RequirementTreeSubNode6` AS `RequirementTreeSubNode6`, `rq`.`RequirementTreeSubNode7` AS `RequirementTreeSubNode7`, `rq`.`RequirementTreeSubNode8` AS `RequirementTreeSubNode8`, `rq`.`RequirementTreeSubNode9` AS `RequirementTreeSubNode9`, `rq`.`RequirementTreeSubNode10` AS `RequirementTreeSubNode10`, `rq`.`RequirementTreeSubNode11` AS `RequirementTreeSubNode11`,  `rq`.`RequirementTreeSubNode12` AS `RequirementTreeSubNode12`, `tr`.`testStatus` AS `teststatus`,  `tr`.`testStatus` AS `reqCoverageStatus`, `tr`.`testCaseId` AS `testcaseid`, `tr`.`requirementId` AS `testreqId`, `rq`.`releaseId` AS `reqReleaseId`, `rq`.`projectId` AS `reqProjectId`, `tr`.`releaseId` AS `releaseId`,  `tr`.`projectId` AS `projectId`, CASE  WHEN  (`rq`.`actualTestCaseIds` IS NULL  OR `rq`.`actualTestCaseIds` = '')  THEN 'Not Covered' ELSE 'Covered' END AS `ReQCoverage`, `d`.`requirementID` AS `defReqId`,  `d`.`testcaseid` AS `defTestCaseID`, `d`.`defects` AS `defects`, `d`.`defects` AS `defectids` FROM  ((`base_req` `rq`  LEFT JOIN `rv_temp_req_wise_test_results` `tr` ON (`rq`.`releaseId` = `tr`.`releaseId` AND `rq`.`projectId` = `tr`.`projectId` AND `rq`.`requirementId` = `tr`.`requirementId` AND `rq`.`reqTestCaseId` = `tr`.`testCaseId`)) LEFT JOIN `rv_req_vs_defects` `d` ON (`rq`.`requirementId` = `d`.`requirementID`));


 /* Phase 4 - Req-79 - Increase Graph Drill down level to 10 in Test case */
CREATE OR REPLACE VIEW `v_test_results` AS
 SELECT  `test_execution_results`.`id` AS `id`,  TRIM(SUBSTRING_INDEX(`test_execution_results`.`projectName`,  '-',  1)) AS `ProjectReference`, TRIM(SUBSTR(`test_execution_results`.`projectName`,LOCATE('-',`test_execution_results`.`projectName`) + 1)) AS `ProjectName`,  `project_milestones_status`.`Domain` AS `DomainName`,  `test_execution_results`.`projectName` AS `Project`,  `test_execution_results`.`releaseName` AS `ReleaseName`,  `test_execution_results`.`hierarchy` AS
 `Hierarchy`,  NULLIF(`test_execution_results`.`folder`, '') AS `Folder`,  NULLIF(`test_execution_results`.`subFolder`, '') AS `subFolder`,  NULLIF(`test_execution_results`.`subFolder1`,   '') AS `subFolder1`,  NULLIF(`test_execution_results`.`subFolder2`,   '') AS `subFolder2`,  
  NULLIF(`test_execution_results`.`subfolder3`,   '') AS `subFolder3`,
   NULLIF(`test_execution_results`.`subfolder4`,   '') AS `subFolder4`,
    NULLIF(`test_execution_results`.`subfolder5`,   '') AS `subFolder5`,
     NULLIF(`test_execution_results`.`subfolder6`,   '') AS `subFolder6`,
      NULLIF(`test_execution_results`.`subfolder7`,   '') AS `subFolder7`,
       NULLIF(`test_execution_results`.`subfolder8`,   '') AS `subFolder8`,
        NULLIF(`test_execution_results`.`subfolder9`,   '') AS `subFolder9`,
         NULLIF(`test_execution_results`.`subfolder10`,   '') AS `subFolder10`,
 `test_execution_results`.`createdOn` AS `CreatedOn`,  `test_execution_results`.`requirementId` AS `RequirementId`,  `test_execution_results`.`reqAltId` AS `ReqAltId`,  `test_execution_results`.`requirementName` AS `RequirementName`,  `test_execution_results`.`environment` AS `Environment`, `test_execution_results`.`testCaseAltId` AS `RestCaseAltId`,  `test_execution_results`.`testCaseName` AS `RestCaseName`,  `test_execution_results`.`testCaseId` AS `TestCaseId`,  `test_execution_results`.`automation` AS `Automation`,  `test_execution_results`.`cycle` AS `Cycle`,  `test_execution_results`.`testStatus` AS `tStatus`,  CASE  WHEN `test_execution_results`.`testStatus` LIKE '%fail%' THEN 'Fail'  WHEN `test_execution_results`.`testStatus` LIKE '%pass%' THEN 'Pass'   WHEN `test_execution_results`.`testStatus` LIKE '%incomp%' THEN 'Incomplete' WHEN `test_execution_results`.`testStatus` LIKE '%blocked%' THEN 'Blocked'   WHEN `test_execution_results`.`testStatus` LIKE '%Not exec%' THEN 'Not Executed' WHEN `test_execution_results`.`testStatus` LIKE '%N/A%' THEN 'N/A'      ELSE 'Not Executed' END AS `testStatus`, `test_execution_results`.`executedOn` AS `ExecutedOn`, `test_execution_results`.`tags` AS `Tags`, `test_execution_results`.`priority` AS `priority`,  `test_execution_results`.`defects` AS `Defects`,  `test_execution_results`.`executedBy` AS `ExecutedBy`,  `test_execution_results`.`createdBy` AS `CreatedBy`,  `test_execution_results`.`insertedOn` AS `insertedOn`,  `test_execution_results`.`subFolderLevel` AS `subFolderLevel`, `test_execution_results`.`updatedOn` AS `updatedOn` , `test_execution_results`.`component` AS `component`     FROM  (`test_execution_results`  LEFT JOIN `project_milestones_status` ON (TRIM(CONVERT( SUBSTRING_INDEX(`test_execution_results`.`projectName`, '-', 1) USING UTF8)) = `project_milestones_status`.`ProjectReference`  AND CONVERT(TRIM(`test_execution_results`.`releaseName`) USING UTF8) = `project_milestones_status`.`ReleaseName`)) 


/** Requireent View sub folder inclusion  08-03-2021 **/ 

CREATE OR REPLACE  VIEW `req_details_wise_tests` AS  SELECT  `rq`.`id` AS `id`,  `rq`.`ProjectReference` AS `ProjectReference`,  `rq`.`ProjectName` AS `ProjectName`,  `rq`.`releaseName` AS `ReleaseName`,  `rq`.`requirementId` AS `RequirementID`,  `rq`.`actualTestCaseIds` AS `actualTestCaseIds`,  `rq`.`reqTestCaseId` AS `reqTestCaseid`,  `rq`.`requirementTreeNode` AS `RequirementTreeNode`,  `rq`.`requirementTreeSubnode` AS `RequirementTreeSubNode`,  `rq`.`requirementTreeSubnode1` AS `RequirementTreeSubNode1`, `rq`.`RequirementTreeSubNode2` AS `RequirementTreeSubNode2`,  `rq`.`RequirementTreeSubNode3` AS `RequirementTreeSubNode3`,  `rq`.`RequirementTreeSubNode4` AS `RequirementTreeSubNode4`,  `rq`.`RequirementTreeSubNode5` AS `RequirementTreeSubNode5`, `rq`.`RequirementTreeSubNode6` AS `RequirementTreeSubNode6`,  `rq`.`RequirementTreeSubNode7` AS `RequirementTreeSubNode7`,  `rq`.`RequirementTreeSubNode8` AS `RequirementTreeSubNode8`,  `rq`.`RequirementTreeSubNode9` AS `RequirementTreeSubNode9`, `rq`.`RequirementTreeSubNode10` AS `RequirementTreeSubNode10`,  `rq`.`RequirementTreeSubNode11` AS `RequirementTreeSubNode11`,  `rq`.`RequirementTreeSubNode12` AS `RequirementTreeSubNode12`,  `rq`.`reqReleaseTCCountMapTCCount` AS `MappedTCCount`, IFNULL(`tr`.`testStatus`, 'Not Executed') AS `testStatus`,  `tr`.`testCaseId` AS `testcaseid`,  `tr`.`requirementid` AS `testreqId`,  `rq`.`releaseId` AS `reqReleaseId`,  `rq`.`projectId` AS `reqProjectId`, `tr`.`releaseId` AS `releaseId`, `tr`.`projectId` AS `projectId`, IFNULL(`tr`.`testStatus`, 'Not Executed') AS `reqCoverageStatus`, CASE  WHEN   (`rq`.`actualTestCaseIds` IS NULL OR `rq`.`actualTestCaseIds` = '') THEN 'Not Covered'  ELSE 'Covered' END AS `ReQCoverage`, `d`.`requirementID` AS `defReqId`, `d`.`testcaseid` AS `defTestCaseID`,`d`.`defects` AS `defects`, IFNULL(REPLACE(`d`.`defects`, ',,', ','), '') AS `defectids` FROM ((`base_req` `rq` LEFT JOIN `rv_temp_req_wise_test_results` `tr` ON (`rq`.`releaseId` = `tr`.`releaseId`  AND `rq`.`projectId` = `tr`.`projectId` AND `rq`.`requirementId` = `tr`.`requirementid` AND `rq`.`reqTestCaseId` = `tr`.`testCaseId`)) LEFT JOIN `rv_req_vs_defects` `d` ON (`rq`.`requirementId` = `d`.`requirementID`));


/* Add RequirementIds filed in Test Case Execution results  08-03-2021 */

ALTER TABLE `iar_byld_1`.`test_execution_results` ADD COLUMN `requirementIds` VARCHAR(100) NULL AFTER `cyclePhaseId`;

/* Tets Case Fixes  08-03-2021 */

ALTER TABLE `iar_byld_1`.`test_execution_wise_req_details`  DROP FOREIGN KEY `ParentTCRecordId`;
ALTER TABLE `iar_byld_1`.`test_execution_wise_req_details`  DROP INDEX `ParentTCRecordId` ;

/* Req Wise Test View Changes   08-03-2021 **/

CREATE OR REPLACE  VIEW `base_req` AS  SELECT   TRIM(SUBSTRING_INDEX(`b`.`projectName`, '-', 1)) AS `ProjectReference`, TRIM(SUBSTR(`b`.`projectName`,   LOCATE('-', `b`.`projectName`) + 1)) AS `ProjectName`,  `b`.`projectName` AS `project`,  `b`.`releaseName` AS `releaseName`,  `b`.`id` AS `id`,  `b`.`RequirementID` AS `requirementId`,  `b`.`requirementTreeIds` AS `requirementTreeIds`,  `b`.`reqReleaseTCCountMapTCCount` AS `reqReleaseTCCountMapTCCount`,  `b`.`projectId` AS `projectId`,  `b`.`actualTestcaseIds` AS `actualTestCaseIds`,  `rd`.`reqTestCaseId` AS `reqTestCaseId`,  `rd`.`parentReqId` AS `parentReqId`,  `b`.`insertedOn` AS `insertedOn`,  `b`.`updatedOn` AS `updatedOn`,  `b`.`releaseId` AS `releaseId`,  `b`.`RequirementTreeNode` AS `requirementTreeNode`,  `b`.`RequirementTreeSubNode` AS `requirementTreeSubnode`,  `b`.`RequirementTreeSubNode1` AS `requirementTreeSubnode1`,  `b`.`RequirementTreeSubNode2` AS `RequirementTreeSubNode2`,  `b`.`RequirementTreeSubNode3` AS `RequirementTreeSubNode3`,  `b`.`RequirementTreeSubNode4` AS `RequirementTreeSubNode4`,  `b`.`RequirementTreeSubNode5` AS `RequirementTreeSubNode5`,  `b`.`RequirementTreeSubNode6` AS `RequirementTreeSubNode6`,  `b`.`RequirementTreeSubNode7` AS `RequirementTreeSubNode7`,  `b`.`RequirementTreeSubNode8` AS `RequirementTreeSubNode8`,  `b`.`RequirementTreeSubNode9` AS `RequirementTreeSubNode9`,  `b`.`RequirementTreeSubNode10` AS `RequirementTreeSubNode10`, `b`.`RequirementTreeSubNode11` AS `RequirementTreeSubNode11`,  `b`.`RequirementTreeSubNode12` AS `RequirementTreeSubNode12`  FROM  (`requirement_details` `b` JOIN `requirement_wise_test_details` `rd` ON (`rd`.`projectId` = `b`.`projectId`   AND `rd`.`releaseId` = `b`.`releaseId`   AND `rd`.`RequirementID` = `b`.`RequirementID`));

CREATE OR REPLACE VIEW `req_details_wise_tests` AS SELECT  `rq`.`id` AS `id`, `rq`.`ProjectReference` AS `ProjectReference`,`rq`.`ProjectName` AS `ProjectName`, `rq`.`releaseName` AS `ReleaseName`, `rq`.`requirementId` AS `RequirementID`, `rq`.`actualTestCaseIds` AS `actualTestCaseIds`, `rq`.`reqTestCaseId` AS `reqTestCaseid`,`rq`.`requirementTreeNode` AS `RequirementTreeNode`, `rq`.`requirementTreeSubnode` AS `RequirementTreeSubNode`, `rq`.`requirementTreeSubnode1` AS `RequirementTreeSubNode1`, `rq`.`RequirementTreeSubNode2` AS `RequirementTreeSubNode2`,`rq`.`RequirementTreeSubNode3` AS `RequirementTreeSubNode3`, `rq`.`RequirementTreeSubNode4` AS `RequirementTreeSubNode4`, `rq`.`RequirementTreeSubNode5` AS `RequirementTreeSubNode5`,`rq`.`RequirementTreeSubNode6` AS `RequirementTreeSubNode6`,`rq`.`RequirementTreeSubNode7` AS `RequirementTreeSubNode7`, `rq`.`RequirementTreeSubNode8` AS `RequirementTreeSubNode8`, `rq`.`RequirementTreeSubNode9` AS `RequirementTreeSubNode9`, `rq`.`RequirementTreeSubNode10` AS `RequirementTreeSubNode10`, `rq`.`RequirementTreeSubNode11` AS `RequirementTreeSubNode11`, `rq`.`RequirementTreeSubNode12` AS `RequirementTreeSubNode12`, `rq`.`reqReleaseTCCountMapTCCount` AS `MappedTCCount`, IFNULL(`tr`.`testStatus`, 'Not Executed') AS `testStatus`, `tr`.`testCaseId` AS `testcaseid`, `tr`.`requirementid` AS `testreqId`, `rq`.`releaseId` AS `reqReleaseId`, `rq`.`projectId` AS `reqProjectId`, `tr`.`releaseId` AS `releaseId`, `tr`.`projectId` AS `projectId`, IFNULL(`tr`.`testStatus`, 'Not Executed') AS `reqCoverageStatus`, CASE  WHEN (`rq`.`actualTestCaseIds` IS NULL  OR `rq`.`actualTestCaseIds` = '') THEN 'Not Covered'  ELSE 'Covered' END AS `ReQCoverage`, `d`.`requirementID` AS `defReqId`, `d`.`testcaseid` AS `defTestCaseID`, `d`.`defects` AS `defects`,IFNULL(REPLACE(`d`.`defects`, ',,', ','), '') AS `defectids`  FROM ((`base_req` `rq` LEFT JOIN `rv_temp_req_wise_test_results` `tr` ON (`rq`.`releaseId` = `tr`.`releaseId`     AND `rq`.`projectId` = `tr`.`projectId`  AND `rq`.`requirementId` = `tr`.`requirementid`   AND `rq`.`reqTestCaseId` = `tr`.`testCaseId`)) LEFT JOIN `rv_req_vs_defects` `d` ON (`rq`.`requirementId` = `d`.`requirementID`)) ;



/* upload jobs   08-03-2021 **/

DROP TABLE IF EXISTS `uploadjobs`;
CREATE TABLE `uploadjobs` (
  `jobName` varchar(50) NOT NULL,
  `jobDescription` longtext DEFAULT NULL,
  `jobUrl` mediumtext DEFAULT NULL,
  `ManualRuntime` datetime DEFAULT NULL,
  `LastRunTime` datetime DEFAULT NULL,
  PRIMARY KEY (`jobName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
delete from uploadjobs;
INSERT INTO `uploadjobs` VALUES ('components','Upload Components','http://localhost:3322/api/jira/uploadComponents?isManualRun=false','2021-03-04 13:56:39','2021-03-04 13:56:41'),('defects','Upload Defects','http://localhost:3322/api/jira/uploadDefects/upcomingRelease?isManualRun=false','2021-03-04 13:56:43','2021-03-04 13:56:44'),('projects','Upload Projects','http://localhost:3322/api/CCB/uploadProjects?isManualRun=false','2021-03-04 13:56:34','2021-03-04 13:56:35'),('release','Upload Release','http://localhost:3322/api/CCB/uploadRelease?deleteFlag=false?isManualRun=false','2021-03-04 13:56:45','2021-03-04 13:56:46'),('requirements','Upload Requirements','http://localhost:3322/api/uploadRequirements/upcomingRelease?isManualRun=false','2021-03-04 13:56:47','2021-03-04 13:56:47'),('testReports','Upload Test Reports','http://localhost:3322/api/uploadTestReports?isManualRun=false','2021-03-04 13:56:36','2021-03-04 13:56:37'),('testResults','Upload Test Results','http://localhost:3322/api/uploadTests/upcomingRelease?isManualRun=false','2021-03-04 13:56:48','2021-03-04 13:56:50'),('userStory','Upload User Story','http://localhost:3322/api/jira/uploadStories/upcomingRelease?isManualRun=false','2021-03-04 13:56:51','2021-03-04 13:56:51');


ALTER TABLE `iar_byld_1`.`uploadjobs` 
CHANGE COLUMN `jobName` `jobName` VARCHAR(50) NOT NULL ,
ADD PRIMARY KEY (`jobName`);
;


/* Test Executin Results Changes requiremedIds field lenght - 17-03-2021 */

ALTER TABLE `test_execution_results` CHANGE COLUMN `requirementIds` `requirementIds`
 LONGTEXT NULL COLLATE 'utf8_general_ci' AFTER `createdOn`;

/* Creating user table */

CREATE TABLE `iar_byld_1`.`customers` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`email` VARCHAR(255) NOT NULL COLLATE 'utf8_general_ci',
	`name` VARCHAR(255) NOT NULL COLLATE 'utf8_general_ci',
	`active` TINYINT(1) NULL DEFAULT '0',
	`password` LONGTEXT NOT NULL DEFAULT '' COLLATE 'utf8_general_ci',
	`role` LONGTEXT NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	PRIMARY KEY (`id`) USING BTREE
);

INSERT INTO `iar_byld_1`.`customers` (`id`, `email`, `name`, `active`, `password`, `role`) VALUES ('1', 'datauploads', 'Admin', '1', '$2a$10$l.sdWxTyq01wT/us4K4Fu.ZeWBBIcfNllBGBqQJBQ48u4gmeHeEpu', 'admin');