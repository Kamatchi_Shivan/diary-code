/* Data needs to be truncted to avod unique constraints issue */
delete from test_execution_results;

delete from requirement_details;


ALTER TABLE `test_execution_results` CHANGE COLUMN `updatedOn` `updatedOn` TIMESTAMP NULL DEFAULT NULL ;

ALTER TABLE `test_execution_results`
ADD COLUMN `subfolder3` VARCHAR
(450) NULL AFTER `subfolder2`;

ALTER TABLE `test_execution_results`
ADD COLUMN `subfolder4` VARCHAR
(450) NULL AFTER `subfolder3`;

ALTER TABLE `test_execution_results`
ADD COLUMN `subfolder5` VARCHAR
(450) NULL AFTER `subfolder4`;

ALTER TABLE `test_execution_results`
ADD COLUMN `subfolder6` VARCHAR
(450) NULL AFTER `subfolder5`;

ALTER TABLE `test_execution_results`
ADD COLUMN `subfolder7` VARCHAR
(450) NULL AFTER `subfolder6`;

ALTER TABLE `test_execution_results`
ADD COLUMN `subfolder8` VARCHAR
(450) NULL AFTER `subfolder7`;

ALTER TABLE `test_execution_results`
ADD COLUMN `subfolder9` VARCHAR
(450) NULL AFTER `subfolder8`;

ALTER TABLE `test_execution_results`
ADD COLUMN `subfolder10` VARCHAR
(450) NULL AFTER `subfolder9`;

ALTER TABLE `test_execution_results`
DROP INDEX `UNIQ` ,
ADD UNIQUE INDEX `UNIQ`
(`projectName` ASC, `releaseName` ASC, `testCaseId` ASC, `cycle` ASC, `hierarchy` ASC) ;

/******* Defect Fixes ******/

CREATE TABLE tempCount (n int not null primary key);
INSERT INTO tempCount (n)
SELECT a.N + b.N * 10 + 1 n
 FROM 
(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) a
,(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) b
ORDER BY n;

/** drop view v_project_wise_defect_keys;  */
CREATE OR REPLACE VIEW `v_project_wise_defect_keys` AS
    SELECT 
        `t`.`ReleaseName` AS `ReleaseName`,
        `t`.`ProjectReference` AS `ProjectReference`,
        `t`.`ProjectName` AS `ProjectName`,
        `t`.`Domain` AS `Domain`,
        `t`.`Jira_defect_projects` AS `Jira_defect_projects`,
        SUBSTRING_INDEX(SUBSTRING_INDEX(`t`.`Jira_defect_projects`, ',', `n`.`n`),
                ',',
                - 1) AS `JIRA_KEY`
    FROM
        (`project_milestones_status` `t`
        JOIN `tempcount` `n`)
    WHERE
        `n`.`n` <= 1 + (OCTET_LENGTH(`t`.`Jira_defect_projects`) - OCTET_LENGTH(REPLACE(`t`.`Jira_defect_projects`,
                    ',',
                    '')))
    ORDER BY SUBSTRING_INDEX(SUBSTRING_INDEX(`t`.`Jira_defect_projects`, ',', `n`.`n`),
            ',',
            - 1)

 
 /** Defects Data View Changes */
 /** Comment- Vew modified to handple spaces in JIRA-DEFECT Key data */
DROP VIEW `v_defects_data`;

CREATE OR REPLACE VIEW `v_defects_data` AS
    SELECT 
        `d`.`defectId` AS `defectId`,
        `d`.`defectKey` AS `defectKey`,
        `d`.`projectKey` AS `projectKey`,
        `d`.`projectName` AS `projectName`,
        `d`.`projectCategory` AS `projectCategory`,
        IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`,
        `pm`.`ProjectReference` AS `projectReference`,
        IFNULL(`d`.`resolution`, 'Un-Defined') AS `resolution`,
        IFNULL(`d`.`defectSevirity`, 'Un-Defined') AS `defectSevirity`,
        IFNULL(`d`.`product`, 'Un-Defined') AS `product`,
        CASE
            WHEN `d`.`testLevelDetected` LIKE '%system%' THEN 'System Test'
            WHEN `d`.`testLevelDetected` LIKE '%sprint%' THEN 'Sprint Test'
            WHEN `d`.`testLevelDetected` LIKE '%IT%' THEN 'Integration Test'
            WHEN `d`.`testLevelDetected` LIKE '%UAT%' THEN 'UAT'
            WHEN `d`.`testLevelDetected` LIKE '%unit%' THEN 'Unit Test'
            ELSE 'Un-Defined'
        END AS `testLevelDetected`,
        CASE
            WHEN `d`.`testTypes` LIKE '%func%' THEN 'Functional'
            WHEN `d`.`testTypes` LIKE '%aut%' THEN 'Automation'
            WHEN `d`.`testTypes` LIKE '%per%' THEN 'Performance'
            WHEN `d`.`testTypes` LIKE '%sec%' THEN 'Security Test'
            WHEN `d`.`testTypes` LIKE '%uni%' THEN 'Unit Test'
            ELSE 'Un-Defined'
        END AS `testTypes`,
        CASE
            WHEN `d`.`status` LIKE '%To Verify%' THEN 'In Test'
            WHEN `d`.`status` LIKE '%To Groom%' THEN 'In Test'
            WHEN `d`.`status` LIKE '%In Test%' THEN 'In Test'
            WHEN `d`.`status` LIKE '%UAT on-going%' THEN 'In Test'
            WHEN `d`.`status` LIKE '%Ready%' THEN 'UAT Ready'
            WHEN `d`.`status` LIKE '%open%' THEN 'Open'
            WHEN `d`.`status` LIKE '%sprintable%' THEN 'IN SPRINTABLE'
            WHEN `d`.`status` LIKE '%progress%' THEN 'In Progress'
            WHEN `d`.`status` LIKE '%Analysis%' THEN 'In Progress'
            WHEN `d`.`status` LIKE '%Business Validated%' THEN 'In Progress'
            WHEN `d`.`status` LIKE '%review%' THEN 'In Review'
            WHEN `d`.`status` LIKE '%promot%' THEN 'To Promote'
            WHEN `d`.`status` LIKE 'open' THEN 'Open'
            WHEN `d`.`status` LIKE 'Backlog' THEN 'Open'
            WHEN `d`.`status` LIKE 'Next' THEN 'Open'
            WHEN `d`.`status` LIKE '%resol%' THEN 'Resolved'
            WHEN `d`.`status` LIKE '%done%' THEN 'Resolved'
            WHEN `d`.`status` LIKE '%reopened%' THEN 'Reopened'
            WHEN `d`.`status` LIKE '%closed%' THEN 'Closed'
        END AS `status`,
        IFNULL(`d`.`createdUserName`, 'UnAssigned') AS `createdUserName`,
        IFNULL(`d`.`priority`, 'Un-Defined') AS `priority`,
        IFNULL(`d`.`assigneeName`, 'UnAssigned') AS `assigneeName`,
        IFNULL(`d`.`severity`, 'Un-Defined') AS `severity`,
        IFNULL(`d`.`component`, 'No Components') AS `component`,
        IFNULL(`d`.`sprintId`, 'Un-Defined') AS `sprintId`,
        IFNULL(`d`.`reporter`, 'UnAssigned') AS `reporter`,
        IFNULL(`d`.`progress`, 'Un-Defined') AS `progress`,
        IFNULL(`d`.`stereoType`, 'Un-Defined') AS `stereoType`,
        IFNULL(`d`.`epicLink`, 'Un-Defined') AS `epicLink`,
        IFNULL(`d`.`fixVersions`, 'Un-Defined') AS `fixVersions`,
        `pm`.`ReleaseName` AS `ReleaseName`,
        IFNULL(`d`.`issueType`, 'Un-Defined') AS `issueType`,
        IFNULL(`d`.`parentKey`, 'Un-Defined') AS `parentKey`,
        IFNULL(`d`.`storyDefectIdFixVersion`,
                'Un-Defined') AS `storyDefectIdFixVersion`,
        REPLACE(`pm`.`JIRA_KEY`, ' ', '') AS `JIRA_KEY`
    FROM
        (`defect_details` `d`
        JOIN `v_project_wise_defect_keys` `pm` ON (`d`.`projectKey` = REPLACE(`pm`.`JIRA_KEY`, ' ', '')
            AND `d`.`fixVersions` = `pm`.`ReleaseName`))
    WHERE
        `d`.`status` <> 'Closed'
            AND `d`.`fixVersions` <> 'Un-Defined';


/** Test Result Table alteration for Duplicate Key Constaints*/

ALTER TABLE `test_execution_results`
ADD COLUMN `cyclePhaseId` INT(20) NULL AFTER `updatedOn`;

ALTER TABLE `test_execution_results`
ADD COLUMN `tcrCatalogTreeId` INT(20) NULL AFTER `cyclePhaseId`;


ALTER TABLE `test_execution_results` 
DROP INDEX `UNIQ` ,
ADD UNIQUE INDEX `UNIQ` (`projectName` ASC, `releaseName` ASC, `testCaseId` ASC, `cycle` ASC, `cyclePhaseId` ASC, `tcrCatalogTreeId` ASC) ;


/**  Requirement Details Table Duplicate Alterations **/

ALTER TABLE `requirement_details` 
DROP INDEX `UNIQ` ,
ADD UNIQUE INDEX `UNIQ` (`projectId` ASC, `releaseIds` ASC, `RequirementID` ASC, `requirementTreeIds` ASC) ;
;

/** Changing the length of ActualTestCaseId's field **/

ALTER TABLE `requirement_details` 
CHANGE COLUMN `actualTestcaseIds` `actualTestcaseIds` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `updatedOn` `updatedOn` TIMESTAMP NULL DEFAULT NULL ;


/** Field Length alterations 6:40 pm   1 September 2020 */

ALTER TABLE `requirement_details` 
CHANGE COLUMN `requirementTreePath` `requirementTreePath` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `RequirementTreeNode` `RequirementTreeNode` LONGTEXT NULL DEFAULT NULL ,
CHANGE COLUMN `RequirementTreeSubNode` `RequirementTreeSubNode` TEXT NULL DEFAULT NULL ,
CHANGE COLUMN `RequirementTreeSubNode1` `RequirementTreeSubNode1` TEXT NULL DEFAULT NULL ,
CHANGE COLUMN `releaseNames` `releaseNames` VARCHAR(450) NULL DEFAULT NULL ,
CHANGE COLUMN `actualTestcaseIds` `actualTestcaseIds` LONGTEXT NULL DEFAULT NULL ;

/** Requirement Table Schema **/


DROP TABLE `requirement_details`;

CREATE TABLE `requirement_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projectName` varchar(450) DEFAULT NULL,
  `releaseName` varchar(200) DEFAULT NULL,
  `requirementTreePath` longtext DEFAULT NULL,
  `RequirementTreeNode` longtext DEFAULT NULL,
  `RequirementTreeSubNode` text DEFAULT NULL,
  `RequirementTreeSubNode1` text DEFAULT NULL,
  `RequirementID` varchar(100) DEFAULT NULL,
  `requirementName` varchar(450) DEFAULT NULL,
  `priority` varchar(45) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `reqCreationDate` date DEFAULT NULL,
  `createdBy` varchar(200) DEFAULT NULL,
  `lastModifiedBy` varchar(200) DEFAULT NULL,
  `lastModifiedOn` datetime DEFAULT NULL,
  `externalId` varchar(100) DEFAULT NULL,
  `requirementType` varchar(50) DEFAULT NULL,
  `ReqStatus` varchar(100) DEFAULT NULL,
  `attachmentCount` varchar(10) DEFAULT NULL,
  `releaseIds` varchar(45) DEFAULT NULL,
  `requirementTreeIds` varchar(50) DEFAULT NULL,
  `reqReleaseTCCountMapId` varchar(100) DEFAULT NULL,
  `reqReleaseTCCountMapReqId` varchar(100) DEFAULT NULL,
  `reqReleaseTCCountMapReleaseId` varchar(100) DEFAULT NULL,
  `reqReleaseTCCountMapTCCount` varchar(100) DEFAULT NULL,
  `projectId` varchar(10) DEFAULT NULL,
  `releaseNames` varchar(450) DEFAULT NULL,
  `actualTestcaseIds` longtext DEFAULT NULL,
  `insertedOn` timestamp NULL DEFAULT current_timestamp(),
  `updatedOn` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ` (`projectId`,`RequirementID`,`requirementTreeIds`,`releaseName`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;



/**** Change character set for TestCaseName - Test Execution Results */

ALTER TABLE test_execution_results MODIFY COLUMN testCaseName LONGTEXT  
    CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

/** Changing Charset to UTF8 for RequirementTable **/

ALTER TABLE  `requirement_details` CONVERT TO CHARACTER SET utf8;

/** Change the Requirement view to support multiple release for same project  02/09/2020 */
/** DROP VIEW v_requirement_coverage  */

CREATE OR REPLACE VIEW `v_requirement_coverage` AS
 with `_temp_requirement_coverage` as 
	(select *,row_number() 
			OVER (PARTITION BY `temp_requirement_coverage`.`RequirementID` 
			ORDER BY field(`temp_requirement_coverage`.`testStatus`,'Fail','Pass','InComplete','Blocked','N/A','Not Executed','Not Covered') desc ) 
		AS `reqCount_num` from 
		(select trim(substring_index(`rq`.`ProjectName`,'-',1)) AS `ProjectReference`,
			trim(substr(`rq`.`ProjectName`,(locate('-',`rq`.`ProjectName`) + 1))) AS `ProjectName`,
			`rq`.`ProjectName` AS `project`,
			`rq`.`ReleaseName` AS `ReleaseName`,
			`rq`.`RequirementTreePAth` AS `RequirementTreePath`,
			`rq`.`RequirementTreeNode` AS `RequirementTreeNode`,
			`rq`.`RequirementTreeSubNode` AS `RequirementTreeSubNode`,
			`rq`.`RequirementTreeSubNode1` AS `RequirementTreeSubNode1`,
			`rq`.`RequirementID` AS `RequirementID`,`rq`.`actualTestCaseIds` AS `actualTestCaseIDs`,
			`rq`.`reqReleaseTcCountMapTCCount` AS `MappedTCCount`,
			`rq`.`actualTestIDs` AS `actualTestIds`,
			`tr`.`testCaseId` AS `testcaseid`,
			ifnull(`tr`.`testStatus`,'Not Covered') AS `testStatus`,
			ifnull(`tr`.`testStatus`,'Not Covered') AS `reqCoverageStatus`,
			`tr`.`releaseName` AS `fixVersions`,
			`tr`.`cycle` AS `cycle`,
			`tr`.`defects` AS `defectId`,
			`tr`.`executedOn` AS `executedOn`,
			(case when ((`rq`.`actualTestCaseIds` is null) or (`rq`.`actualTestCaseIds` = '')) then 'Not Covered' 
				else 'Covered' end) AS `ReQCoverage`
			from 
			((with recursive `t` as (select * from `requirement_details`), `n` as 
				(select 1 AS `n` union select (`n`.`n` + 1) AS `n + 1` from (`n` join `t`) 
					where (`n`.`n` <= (length(`t`.`actualTestcaseIds`) - length(replace(`t`.`actualTestcaseIds`,',','')))))
			select distinct substring_index(substring_index(`t`.`actualTestcaseIds`,',',`n`.`n`),',',-(1)) AS `actualTestIDs`,`t`.* 
			from (`n` join `t`)) `rq` left join 
		(with `_test_execution_results` as 
			(select *,row_number() 
				OVER (PARTITION BY `test_execution_results`.`testCaseId`
				ORDER BY `test_execution_results`.`executedOn` desc )  AS `row_num` from `test_execution_results`)
 select `_test_execution_results`.* from `_test_execution_results` where (`_test_execution_results`.`row_num` = 1))
	`tr` on((`rq`.`actualTestIDs` = `tr`.`testCaseId`)))) as `temp_requirement_coverage`) 
select * from `_temp_requirement_coverage` 
	group by
	`_temp_requirement_coverage`.`ProjectName`,
	`_temp_requirement_coverage`.`project`,
	`_temp_requirement_coverage`.`ReleaseName`,
	`_temp_requirement_coverage`.`RequirementID`,
	`_temp_requirement_coverage`.`actualTestCaseIDs`;

/** JIRA_LINK - Modified to display JQL for the project based on  Project key*/


CREATE OR REPLACE VIEW `v_project_ccb_details` AS
    SELECT 
        `rm`.`ReleaseName` AS `ReleaseName`,
        `rm`.`ReleaseDate` AS `ReleaseDate`,
        `pm`.`ProjectReference` AS `ProjectReference`,
        `pm`.`ProjectName` AS `ProjectName`,
        `pm`.`Test_Status` AS `test_status`,
        `pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`,
        `pm`.`Domain` AS `Domain`,
        `pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`,
        `pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link`,
        `pm`.`JIRA_Link` AS `JIRA_Link1`,
        CONCAT('https://jira.pvgroup.be/issues/?jql=project in (',
                `pm`.`Jira_defect_projects`,
                ') AND issuetype in (Defect,"Story Defect") AND status != Closed AND created > startOfMonth(-3) ORDER BY created DESC') AS `JIRA_Link`,
        `pm`.`ZEPHYR_Link` AS `ZEPHYR_Link`,
        `pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`,
        `pm`.`Test_Coordinator` AS `Test_Coordinator`,
        `pm`.`Scrum_Master` AS `Scrum_Master`,
        `pm`.`CCB_STARTUP_Date` AS `CCB_STARTUP_Date`,
        `pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`,
        CONVERT( `pm`.`CCB_STARTUP_Remarks` USING UTF8) AS `CCB_STARTUP_Remarks`,
        CONVERT( `pm`.`Ready2StartTestRemarks` USING UTF8) AS `Ready2StartTestRemarks`,
        `pm`.`CCB_Ready4SPRINT_Date` AS `CCB_Ready4SPRINT_Date`,
        `pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`,
        CONVERT( `pm`.`CCBReady4SPRINT_Remarks` USING UTF8) AS `CCBReady4SPRINT_Remarks`,
        CONVERT( `pm`.`Ready4SprintTestRemarks` USING UTF8) AS `Ready4SprintTestRemarks`,
        `pm`.`CCB_Ready4UAT_Date` AS `CCB_Ready4UAT_Date`,
        `pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`,
        CONVERT( `pm`.`CCBReady4UAT_Remarks` USING UTF8) AS `CCBReady4UAT_Remarks`,
        CONVERT( `pm`.`Ready4UATTestRemarks` USING UTF8) AS `Ready4UATTestRemarks`,
        `rm`.`LastRegistration` AS `LastRegistrationDate`,
        `rm`.`UAT_StartDate` AS `UAT_START_DATE`,
        `rm`.`End_UAT` AS `UAT_END_DATE`,
        `rm`.`Freeze` AS `FREEZE_DATE`,
        `pm`.`CCB_Ready4PROD_Date` AS `CCB_Ready4PROD_Date`,
        `pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`,
        CONVERT( `pm`.`CCBReady4PROD_Remarks` USING UTF8) AS `CCBReady4PROD_Remarks`,
        CONVERT( `pm`.`Ready4ProdTestRemarks` USING UTF8) AS `Ready4ProdTestRemarks`,
        `pm`.`CCB_Closure_Date` AS `CCB_Closure_Date`,
        `pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`,
        CONVERT( `pm`.`CCB_CLOSURE_Remarks` USING UTF8) AS `CCB_CLOSURE_Remarks`,
        `pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`,
        `pm`.`Project_Manager` AS `Project_Manager`,
        `pm`.`unitTestReportPath` AS `unitTestReportPath`,
        `pm`.`automationReportPath` AS `automationReportPath`,
        `pm`.`performanceReportPath` AS `performanceReportPath`,
        `pm`.`securityReportPath` AS `securityReportPath`,
        `pm`.`Jira_defect_projects` AS `Jira_defect_projects`
    FROM
        (`release_milestones_status` `rm`
        LEFT JOIN `project_milestones_status` `pm` ON (CONVERT( `rm`.`ReleaseName` USING UTF8) = `pm`.`ReleaseName`));



/***  Fetch Values from release milestones for date values, if entries are null in Project milestones**/

CREATE OR REPLACE VIEW `v_project_ccb_details` AS
    SELECT 
        `rm`.`ReleaseName` AS `ReleaseName`,
        `rm`.`ReleaseDate` AS `ReleaseDate`,
        `pm`.`ProjectReference` AS `ProjectReference`,
        `pm`.`ProjectName` AS `ProjectName`,
        `pm`.`Test_Status` AS `test_status`,
        `pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`,
        `pm`.`Domain` AS `Domain`,
        `pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`,
        `pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link`,
        `pm`.`JIRA_Link` AS `JIRA_Link1`,
        CONCAT('https://jira.pvgroup.be/issues/?jql=project in (',
                `pm`.`Jira_defect_projects`,
                ') AND issuetype in (Defect,"Story Defect") AND status != Closed AND created > startOfMonth(-12) ORDER BY created DESC') AS `JIRA_Link`,
        `pm`.`ZEPHYR_Link` AS `ZEPHYR_Link`,
        `pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`,
        `pm`.`Test_Coordinator` AS `Test_Coordinator`,
        `pm`.`Scrum_Master` AS `Scrum_Master`,
        IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`,
        IFNULL(`pm`.`CCB_STARTUP_Date`,
                `rm`.`LastRegistration`) AS `CCB_STARTUP_Date`,
        IFNULL(`pm`.`CCB_Ready4SPRINT_Date`,
                `rm`.`CCB_Ready4SPRINT`) AS `CCB_Ready4SPRINT_Date`,
        IFNULL(`pm`.`CCB_Ready4UAT_Date`,
                `rm`.`CCB_Ready4UAT`) AS `CCB_Ready4UAT_Date`,
        IFNULL(`pm`.`CCB_Ready4PROD_Date`,
                `rm`.`CCB_Ready4PROD`) AS `CCB_Ready4PROD_Date`,
        IFNULL(`pm`.`CCB_Closure_Date`,
                `rm`.`CCB_Closure`) AS `CCB_Closure_Date`,
        `pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`,
        `pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`,
        CONVERT( `pm`.`CCB_STARTUP_Remarks` USING UTF8) AS `CCB_STARTUP_Remarks`,
        CONVERT( `pm`.`Ready2StartTestRemarks` USING UTF8) AS `Ready2StartTestRemarks`,
        `pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`,
        CONVERT( `pm`.`CCBReady4SPRINT_Remarks` USING UTF8) AS `CCBReady4SPRINT_Remarks`,
        CONVERT( `pm`.`Ready4SprintTestRemarks` USING UTF8) AS `Ready4SprintTestRemarks`,
        `pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`,
        CONVERT( `pm`.`CCBReady4UAT_Remarks` USING UTF8) AS `CCBReady4UAT_Remarks`,
        CONVERT( `pm`.`Ready4UATTestRemarks` USING UTF8) AS `Ready4UATTestRemarks`,
        `rm`.`LastRegistration` AS `LastRegistrationDate`,
        `rm`.`UAT_StartDate` AS `UAT_START_DATE`,
        `rm`.`End_UAT` AS `UAT_END_DATE`,
        `rm`.`Freeze` AS `FREEZE_DATE`,
        `pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`,
        CONVERT( `pm`.`CCBReady4PROD_Remarks` USING UTF8) AS `CCBReady4PROD_Remarks`,
        CONVERT( `pm`.`Ready4ProdTestRemarks` USING UTF8) AS `Ready4ProdTestRemarks`,
        CONVERT( `pm`.`CCB_CLOSURE_Remarks` USING UTF8) AS `CCB_CLOSURE_Remarks`,
        `pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`,
        `pm`.`Project_Manager` AS `Project_Manager`,
        `pm`.`unitTestReportPath` AS `unitTestReportPath`,
        `pm`.`automationReportPath` AS `automationReportPath`,
        `pm`.`performanceReportPath` AS `performanceReportPath`,
        `pm`.`securityReportPath` AS `securityReportPath`,
        `pm`.`Jira_defect_projects` AS `Jira_defect_projects`
    FROM
        (`release_milestones_status` `rm`
        LEFT JOIN `project_milestones_status` `pm` ON (CONVERT( `rm`.`ReleaseName` USING UTF8) = `pm`.`ReleaseName`))



/***  Setting the External reports redirect to "sharePoint URL"**/

CREATE OR REPLACE VIEW `v_project_ccb_details` AS
    SELECT 
        `rm`.`ReleaseName` AS `ReleaseName`,
        `rm`.`ReleaseDate` AS `ReleaseDate`,
        `pm`.`ProjectReference` AS `ProjectReference`,
        `pm`.`ProjectName` AS `ProjectName`,
        `pm`.`Test_Status` AS `test_status`,
        `pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`,
        `pm`.`Domain` AS `Domain`,
        `pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`,
        `pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link`,
        `pm`.`JIRA_Link` AS `JIRA_Link1`,
        CONCAT('https://jira.pvgroup.be/issues/?jql=project in (',
                `pm`.`Jira_defect_projects`,
                ') AND issuetype in (Defect,"Story Defect") AND status != Closed AND created > startOfMonth(-12) ORDER BY created DESC') AS `JIRA_Link`,
        `pm`.`ZEPHYR_Link` AS `ZEPHYR_Link`,
        `pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`,
        `pm`.`Test_Coordinator` AS `Test_Coordinator`,
        `pm`.`Scrum_Master` AS `Scrum_Master`,
        IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`,
        IFNULL(`pm`.`CCB_STARTUP_Date`,
                `rm`.`LastRegistration`) AS `CCB_STARTUP_Date`,
        IFNULL(`pm`.`CCB_Ready4SPRINT_Date`,
                `rm`.`CCB_Ready4SPRINT`) AS `CCB_Ready4SPRINT_Date`,
        IFNULL(`pm`.`CCB_Ready4UAT_Date`,
                `rm`.`CCB_Ready4UAT`) AS `CCB_Ready4UAT_Date`,
        IFNULL(`pm`.`CCB_Ready4PROD_Date`,
                `rm`.`CCB_Ready4PROD`) AS `CCB_Ready4PROD_Date`,
        IFNULL(`pm`.`CCB_Closure_Date`,
                `rm`.`CCB_Closure`) AS `CCB_Closure_Date`,
        `pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`,
        `pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`,
        CONVERT( `pm`.`CCB_STARTUP_Remarks` USING UTF8) AS `CCB_STARTUP_Remarks`,
        CONVERT( `pm`.`Ready2StartTestRemarks` USING UTF8) AS `Ready2StartTestRemarks`,
        `pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`,
        CONVERT( `pm`.`CCBReady4SPRINT_Remarks` USING UTF8) AS `CCBReady4SPRINT_Remarks`,
        CONVERT( `pm`.`Ready4SprintTestRemarks` USING UTF8) AS `Ready4SprintTestRemarks`,
        `pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`,
        CONVERT( `pm`.`CCBReady4UAT_Remarks` USING UTF8) AS `CCBReady4UAT_Remarks`,
        CONVERT( `pm`.`Ready4UATTestRemarks` USING UTF8) AS `Ready4UATTestRemarks`,
        `rm`.`LastRegistration` AS `LastRegistrationDate`,
        `rm`.`UAT_StartDate` AS `UAT_START_DATE`,
        `rm`.`End_UAT` AS `UAT_END_DATE`,
        `rm`.`Freeze` AS `FREEZE_DATE`,
        `pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`,
        CONVERT( `pm`.`CCBReady4PROD_Remarks` USING UTF8) AS `CCBReady4PROD_Remarks`,
        CONVERT( `pm`.`Ready4ProdTestRemarks` USING UTF8) AS `Ready4ProdTestRemarks`,
        CONVERT( `pm`.`CCB_CLOSURE_Remarks` USING UTF8) AS `CCB_CLOSURE_Remarks`,
        `pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`,
        `pm`.`Project_Manager` AS `Project_Manager`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `unitTestReportPath`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `automationReportPath`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `performanceReportPath`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `securityReportPath`,
        `pm`.`Jira_defect_projects` AS `Jira_defect_projects`
    FROM
        (`release_milestones_status` `rm`
        LEFT JOIN `project_milestones_status` `pm` ON (CONVERT( `rm`.`ReleaseName` USING UTF8) = `pm`.`ReleaseName`))


/** Setting the default URL values for IT & UAT Test Report Path"*/
CREATE OR REPLACE VIEW `v_project_ccb_details` AS
    SELECT 
        `rm`.`ReleaseName` AS `ReleaseName`,
        `rm`.`ReleaseDate` AS `ReleaseDate`,
        `pm`.`ProjectReference` AS `ProjectReference`,
        `pm`.`ProjectName` AS `ProjectName`,
        `pm`.`Test_Status` AS `test_status`,
        `pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`,
        `pm`.`Domain` AS `Domain`,
        `pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`,
        `pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link`,
        `pm`.`JIRA_Link` AS `JIRA_Link1`,
        CONCAT('https://jira.pvgroup.be/issues/?jql=project in (',
                `pm`.`Jira_defect_projects`,
                ') AND issuetype in (Defect,"Story Defect") AND status != Closed AND created > startOfMonth(-12) ORDER BY created DESC') AS `JIRA_Link`,
        `pm`.`ZEPHYR_Link` AS `ZEPHYR_Link`,
        `pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`,
        `pm`.`Test_Coordinator` AS `Test_Coordinator`,
        `pm`.`Scrum_Master` AS `Scrum_Master`,
        IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`,
        IFNULL(`pm`.`CCB_STARTUP_Date`,
                `rm`.`LastRegistration`) AS `CCB_STARTUP_Date`,
        IFNULL(`pm`.`CCB_Ready4SPRINT_Date`,
                `rm`.`CCB_Ready4SPRINT`) AS `CCB_Ready4SPRINT_Date`,
        IFNULL(`pm`.`CCB_Ready4UAT_Date`,
                `rm`.`CCB_Ready4UAT`) AS `CCB_Ready4UAT_Date`,
        IFNULL(`pm`.`CCB_Ready4PROD_Date`,
                `rm`.`CCB_Ready4PROD`) AS `CCB_Ready4PROD_Date`,
        IFNULL(`pm`.`CCB_Closure_Date`,
                `rm`.`CCB_Closure`) AS `CCB_Closure_Date`,
        `pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`,
        `pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`,
        CONVERT( `pm`.`CCB_STARTUP_Remarks` USING UTF8) AS `CCB_STARTUP_Remarks`,
        CONVERT( `pm`.`Ready2StartTestRemarks` USING UTF8) AS `Ready2StartTestRemarks`,
        `pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`,
        CONVERT( `pm`.`CCBReady4SPRINT_Remarks` USING UTF8) AS `CCBReady4SPRINT_Remarks`,
        CONVERT( `pm`.`Ready4SprintTestRemarks` USING UTF8) AS `Ready4SprintTestRemarks`,
        `pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`,
        CONVERT( `pm`.`CCBReady4UAT_Remarks` USING UTF8) AS `CCBReady4UAT_Remarks`,
        CONVERT( `pm`.`Ready4UATTestRemarks` USING UTF8) AS `Ready4UATTestRemarks`,
        `rm`.`LastRegistration` AS `LastRegistrationDate`,
        `rm`.`UAT_StartDate` AS `UAT_START_DATE`,
        `rm`.`End_UAT` AS `UAT_END_DATE`,
        `rm`.`Freeze` AS `FREEZE_DATE`,
        `pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`,
        CONVERT( `pm`.`CCBReady4PROD_Remarks` USING UTF8) AS `CCBReady4PROD_Remarks`,
        CONVERT( `pm`.`Ready4ProdTestRemarks` USING UTF8) AS `Ready4ProdTestRemarks`,
        CONVERT( `pm`.`CCB_CLOSURE_Remarks` USING UTF8) AS `CCB_CLOSURE_Remarks`,
        `pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`,
        `pm`.`Project_Manager` AS `Project_Manager`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `unitTestReportPath`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `automationReportPath`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `performanceReportPath`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `securityReportPath`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `IT_TestReportPath`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `UAT_TestReportPath`,
        `pm`.`Jira_defect_projects` AS `Jira_defect_projects`
    FROM
        (`release_milestones_status` `rm`
        LEFT JOIN `project_milestones_status` `pm` ON (CONVERT( `rm`.`ReleaseName` USING UTF8) = `pm`.`ReleaseName`))

/*** ER_TRUNCATED_WRONG_VALUE_FOR_FIELD:
Incorrect string  value: '\xE2\x80\x8B>AL...' for column `iar_prod_1`.`test_execution_results`.`hierarchy`  */

ALTER TABLE test_execution_results CONVERT TO CHARACTER SET utf8;

/** Ater Table to add additional ccb project fields */

ALTER TABLE `project_milestones_status` 
        CHANGE COLUMN `updatedOn` `updatedOn` TIMESTAMP NULL DEFAULT NULL ;

ALTER TABLE `project_milestones_status`  
     ADD COLUMN `Scrum_MasterId` VARCHAR(45) NULL AFTER `Project_Name_Dup`;

ALTER TABLE `project_milestones_status`  
     ADD COLUMN `DEL_STA_HighLevelPlanning` VARCHAR(45) NULL AFTER `Project_Name_Dup`;     

ALTER TABLE `project_milestones_status`  
     ADD COLUMN `DEL_STA_WarrantyIncluded` VARCHAR(45) NULL AFTER `Project_Name_Dup`;         
     
ALTER TABLE `project_milestones_status`  
     ADD COLUMN `DEL_STA_HighLevelRequirementsScope` VARCHAR(45) NULL AFTER `Project_Name_Dup`;       
   
ALTER TABLE `project_milestones_status`  
     ADD COLUMN `DEL_STAStaffingPlanviewBooked` VARCHAR(45) NULL AFTER `Project_Name_Dup`;       
  
ALTER TABLE `project_milestones_status`  
     ADD COLUMN `DEL_STA_BudgetApproved` VARCHAR(45) NULL AFTER `Project_Name_Dup`;    
     
ALTER TABLE `project_milestones_status`  
     ADD COLUMN `DEL_STA_PIDStage1_TODO` VARCHAR(45) NULL AFTER `Project_Name_Dup`;    
     
ALTER TABLE `project_milestones_status`  
     ADD COLUMN `DEL_STA_PIDStage1` VARCHAR(45) NULL AFTER `Project_Name_Dup`;

ALTER TABLE `project_milestones_status`  
     ADD COLUMN `DEL_SPR_SolutionOutlineArchitecture` VARCHAR(45) NULL AFTER `Project_Name_Dup`;   

ALTER TABLE `project_milestones_status`  
     ADD COLUMN `DEL_SPR_ComponentFile` VARCHAR(45) NULL AFTER `Project_Name_Dup`;      

ALTER TABLE `project_milestones_status`  
     ADD COLUMN `DEL_SPR_PlanviewStaffingincUAT` VARCHAR(45) NULL AFTER `Project_Name_Dup`;       

ALTER TABLE `project_milestones_status`  
     ADD COLUMN `DEL_SPR_DetailledPlanning` VARCHAR(45) NULL AFTER `Project_Name_Dup`;       

ALTER TABLE `project_milestones_status`  
     ADD COLUMN `CCBDashb` VARCHAR(45) NULL AFTER `Project_Name_Dup`;   

ALTER TABLE `project_milestones_status`  
     ADD COLUMN `DomainExpert` VARCHAR(45) NULL AFTER `Project_Name_Dup`; 

ALTER TABLE `project_milestones_status`  
     ADD COLUMN `Product_OwnerID` VARCHAR(45) NULL AFTER `Project_Name_Dup`;     
   

ALTER TABLE `project_milestones_status`    
   ADD COLUMN `MTP` VARCHAR(45) NULL AFTER `Project_Name_Dup`,
   ADD COLUMN `Delivery_Master_LeadId` VARCHAR(45) NULL AFTER `Project_Name_Dup`,
   ADD COLUMN `Test_CoordinatorId` VARCHAR(45) NULL AFTER `Project_Name_Dup`,
   ADD COLUMN `DetailedTestStatus` VARCHAR(45) NULL AFTER `Project_Name_Dup`,
   ADD COLUMN `SP_ID` VARCHAR(45) NULL AFTER `Project_Name_Dup`,
   ADD COLUMN `AuthorId` VARCHAR(45) NULL AFTER `Project_Name_Dup`,
   ADD COLUMN `EditorId` VARCHAR(45) NULL AFTER `Project_Name_Dup`,
   ADD COLUMN `OdateUIVersionString` VARCHAR(45) NULL AFTER `Project_Name_Dup`,
   ADD COLUMN `GUID` VARCHAR(45) NULL AFTER `Project_Name_Dup`;


ALTER TABLE `project_milestones_status`  
     ADD COLUMN `Project_ManagerId` VARCHAR(45) NULL AFTER `Project_Name_Dup`;


/** Changing the column Type for DetaieldTestStatus */
ALTER TABLE `project_milestones_status` 
CHANGE COLUMN `DetailedTestStatus` `DetailedTestStatus` LONGTEXT NULL DEFAULT NULL ;

/** #163  - Epic3 - DetailedTestStatus in projects View **/

CREATE OR REPLACE VIEW `v_project_ccb_details` AS
    SELECT 
        `rm`.`ReleaseName` AS `ReleaseName`,
        `rm`.`ReleaseDate` AS `ReleaseDate`,
        `pm`.`ProjectReference` AS `ProjectReference`,
        `pm`.`ProjectName` AS `ProjectName`,
        `pm`.`Test_Status` AS `test_status`,
        `pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`,
        `pm`.`Domain` AS `Domain`,
        `pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`,
        `pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link`,
        `pm`.`JIRA_Link` AS `JIRA_Link1`,
        CONCAT('https://jira.pvgroup.be/issues/?jql=project in (',
                `pm`.`Jira_defect_projects`,
                ') AND issuetype in (Defect,"Story Defect") AND status != Closed AND created > startOfMonth(-12) ORDER BY created DESC') AS `JIRA_Link`,
        `pm`.`ZEPHYR_Link` AS `ZEPHYR_Link`,
        `pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`,
        `pm`.`Test_Coordinator` AS `Test_Coordinator`,
        `pm`.`Scrum_Master` AS `Scrum_Master`,
        IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`,
        IFNULL(`pm`.`CCB_STARTUP_Date`,
                `rm`.`LastRegistration`) AS `CCB_STARTUP_Date`,
        IFNULL(`pm`.`CCB_Ready4SPRINT_Date`,
                `rm`.`CCB_Ready4SPRINT`) AS `CCB_Ready4SPRINT_Date`,
        IFNULL(`pm`.`CCB_Ready4UAT_Date`,
                `rm`.`CCB_Ready4UAT`) AS `CCB_Ready4UAT_Date`,
        IFNULL(`pm`.`CCB_Ready4PROD_Date`,
                `rm`.`CCB_Ready4PROD`) AS `CCB_Ready4PROD_Date`,
        IFNULL(`pm`.`CCB_Closure_Date`,
                `rm`.`CCB_Closure`) AS `CCB_Closure_Date`,
        `pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`,
        `pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`,
        CONVERT( `pm`.`CCB_STARTUP_Remarks` USING UTF8) AS `CCB_STARTUP_Remarks`,
        CONVERT( `pm`.`Ready2StartTestRemarks` USING UTF8) AS `Ready2StartTestRemarks`,
        `pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`,
        CONVERT( `pm`.`CCBReady4SPRINT_Remarks` USING UTF8) AS `CCBReady4SPRINT_Remarks`,
        CONVERT( `pm`.`Ready4SprintTestRemarks` USING UTF8) AS `Ready4SprintTestRemarks`,
        `pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`,
        CONVERT( `pm`.`CCBReady4UAT_Remarks` USING UTF8) AS `CCBReady4UAT_Remarks`,
        CONVERT( `pm`.`Ready4UATTestRemarks` USING UTF8) AS `Ready4UATTestRemarks`,
        `rm`.`LastRegistration` AS `LastRegistrationDate`,
        `rm`.`UAT_StartDate` AS `UAT_START_DATE`,
        `rm`.`End_UAT` AS `UAT_END_DATE`,
        `rm`.`Freeze` AS `FREEZE_DATE`,
        `pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`,
        CONVERT( `pm`.`CCBReady4PROD_Remarks` USING UTF8) AS `CCBReady4PROD_Remarks`,
        CONVERT( `pm`.`Ready4ProdTestRemarks` USING UTF8) AS `Ready4ProdTestRemarks`,
        CONVERT( `pm`.`CCB_CLOSURE_Remarks` USING UTF8) AS `CCB_CLOSURE_Remarks`,
        `pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`,
        `pm`.`Project_Manager` AS `Project_Manager`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `unitTestReportPath`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `automationReportPath`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `performanceReportPath`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `securityReportPath`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `IT_TestReportPath`,
        'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `UAT_TestReportPath`,
        `pm`.`Jira_defect_projects` AS `Jira_defect_projects`,
        `pm`.`DetailedTestStatus` AS `DetailedTestStatus`
    FROM
        (`release_milestones_status` `rm`
        LEFT JOIN `project_milestones_status` `pm` ON (CONVERT( `rm`.`ReleaseName` USING UTF8) = `pm`.`ReleaseName`));;



/** To Validate & TO Inceptoin Status Handling**/


CREATE 
     OR REPLACE VIEW `v_defects_data` AS
    SELECT 
        `d`.`defectId` AS `defectId`,
        `d`.`defectKey` AS `defectKey`,
        `d`.`projectKey` AS `projectKey`,
        `d`.`projectName` AS `projectName`,
        `d`.`projectCategory` AS `projectCategory`,
        IFNULL(`pm`.`Domain`, 'Un-Defined') AS `domainName`,
        `pm`.`ProjectReference` AS `projectReference`,
        IFNULL(`d`.`resolution`, 'Un-Defined') AS `resolution`,
        IFNULL(`d`.`defectSevirity`, 'Un-Defined') AS `defectSevirity`,
        IFNULL(`d`.`product`, 'Un-Defined') AS `product`,
        CASE
            WHEN `d`.`testLevelDetected` LIKE '%system%' THEN 'System Test'
            WHEN `d`.`testLevelDetected` LIKE '%sprint%' THEN 'Sprint Test'
            WHEN `d`.`testLevelDetected` LIKE '%IT%' THEN 'Integration Test'
            WHEN `d`.`testLevelDetected` LIKE '%UAT%' THEN 'UAT'
            WHEN `d`.`testLevelDetected` LIKE '%unit%' THEN 'Unit Test'
            ELSE 'Un-Defined'
        END AS `testLevelDetected`,
        CASE
            WHEN `d`.`testTypes` LIKE '%func%' THEN 'Functional'
            WHEN `d`.`testTypes` LIKE '%aut%' THEN 'Automation'
            WHEN `d`.`testTypes` LIKE '%per%' THEN 'Performance'
            WHEN `d`.`testTypes` LIKE '%sec%' THEN 'Security Test'
            WHEN `d`.`testTypes` LIKE '%uni%' THEN 'Unit Test'
            ELSE 'Un-Defined'
        END AS `testTypes`,
        CASE
            WHEN `d`.`status` LIKE '%To Validate%' THEN 'UAT Ready'
            WHEN `d`.`status` LIKE '%To Verify%' THEN 'In Test'
            WHEN `d`.`status` LIKE '%To Groom%' THEN 'In Test'
            WHEN `d`.`status` LIKE '%In Test%' THEN 'In Test'
            WHEN `d`.`status` LIKE '%UAT on-going%' THEN 'In Test'
            WHEN `d`.`status` LIKE '%Ready%' THEN 'UAT Ready'
            WHEN `d`.`status` LIKE '%open%' THEN 'Open'
            WHEN `d`.`status` LIKE '%sprintable%' THEN 'In Progress'
            WHEN `d`.`status` LIKE '%progress%' THEN 'In Progress'
            WHEN `d`.`status` LIKE '%Analysis%' THEN 'In Progress'
            WHEN `d`.`status` LIKE '%Business Validated%' THEN 'In Progress'
            WHEN `d`.`status` LIKE '%review%' THEN 'In Review'
            WHEN `d`.`status` LIKE '%incep%' THEN 'In Review'
            WHEN `d`.`status` LIKE '%promot%' THEN 'To Promote'
            WHEN `d`.`status` LIKE 'open' THEN 'Open'
            WHEN `d`.`status` LIKE 'Backlog' THEN 'Open'
            WHEN `d`.`status` LIKE 'Next' THEN 'Open'
            WHEN `d`.`status` LIKE '%resol%' THEN 'Resolved'
            WHEN `d`.`status` LIKE '%done%' THEN 'Resolved'
            WHEN `d`.`status` LIKE '%reopened%' THEN 'Reopened'
            WHEN `d`.`status` LIKE '%closed%' THEN 'Closed'
            ELSE `d`.`status`
        END AS `status`,
        IFNULL(`d`.`createdUserName`, 'UnAssigned') AS `createdUserName`,
        IFNULL(`d`.`priority`, 'Un-Defined') AS `priority`,
        IFNULL(`d`.`assigneeName`, 'UnAssigned') AS `assigneeName`,
        IFNULL(`d`.`severity`, 'Un-Defined') AS `severity`,
        IFNULL(`d`.`component`, 'No Components') AS `component`,
        IFNULL(`d`.`sprintId`, 'Un-Defined') AS `sprintId`,
        IFNULL(`d`.`reporter`, 'UnAssigned') AS `reporter`,
        IFNULL(`d`.`progress`, 'Un-Defined') AS `progress`,
        IFNULL(`d`.`stereoType`, 'Un-Defined') AS `stereoType`,
        IFNULL(`d`.`epicLink`, 'Un-Defined') AS `epicLink`,
        IFNULL(`d`.`fixVersions`, 'Un-Defined') AS `fixVersions`,
        `pm`.`ReleaseName` AS `ReleaseName`,
        IFNULL(`d`.`issueType`, 'Un-Defined') AS `issueType`,
        IFNULL(`d`.`parentKey`, 'Un-Defined') AS `parentKey`,
        IFNULL(`d`.`storyDefectIdFixVersion`,
                'Un-Defined') AS `storyDefectIdFixVersion`,
        REPLACE(`pm`.`JIRA_KEY`, ' ', '') AS `JIRA_KEY`
    FROM
        (`defect_details` `d`
        JOIN `v_project_wise_defect_keys` `pm` ON (`d`.`projectKey` = REPLACE(`pm`.`JIRA_KEY`, ' ', '')
            AND `d`.`fixVersions` = `pm`.`ReleaseName`))
    WHERE
        `d`.`status` <> 'Closed'
            AND `d`.`fixVersions` <> 'Un-Defined';


