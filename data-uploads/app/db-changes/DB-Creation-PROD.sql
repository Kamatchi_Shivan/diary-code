-- --------------------------------------------------------
-- Host:                         pvx9mdblr01n1
-- Server version:               10.2.22-MariaDB-log - MariaDB Server
-- Server OS:                    Linux
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for iar_prod_1
DROP DATABASE IF EXISTS `iar_prod_1`;
CREATE DATABASE IF NOT EXISTS `iar_prod_1` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `iar_prod_1`;

-- Dumping structure for table iar_prod_1.components
DROP TABLE IF EXISTS `components`;
CREATE TABLE IF NOT EXISTS `components` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ComponentId` varchar(45) DEFAULT NULL,
  `ComponentName` varchar(45) DEFAULT NULL,
  `Description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table iar_prod_1.defect_details
DROP TABLE IF EXISTS `defect_details`;
CREATE TABLE IF NOT EXISTS `defect_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `defectId` varchar(45) DEFAULT NULL,
  `defectKey` varchar(45) DEFAULT NULL,
  `component` varchar(450) DEFAULT NULL,
  `projectKey` varchar(45) DEFAULT NULL,
  `projectName` varchar(450) DEFAULT NULL,
  `projectCategory` varchar(450) DEFAULT NULL,
  `fixVersions` varchar(450) DEFAULT NULL,
  `resolution` varchar(45) DEFAULT NULL,
  `origin` varchar(405) DEFAULT NULL,
  `defectSevirity` varchar(45) DEFAULT NULL,
  `product` varchar(450) DEFAULT NULL,
  `scrumTeamID` varchar(45) DEFAULT NULL,
  `scrumTeamName` varchar(450) DEFAULT NULL,
  `testTypes` varchar(450) DEFAULT NULL,
  `testLevelDetected` varchar(450) DEFAULT NULL,
  `resolutiondate` datetime DEFAULT NULL,
  `createdUserName` varchar(100) DEFAULT NULL,
  `createdUserID` varchar(45) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `priority` varchar(45) DEFAULT NULL,
  `labels` varchar(300) DEFAULT NULL,
  `stream` varchar(45) DEFAULT NULL,
  `detectedEnvironment` varchar(459) DEFAULT NULL,
  `assigneeID` varchar(45) DEFAULT NULL,
  `assigneeName` varchar(45) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `environment` varchar(450) DEFAULT NULL,
  `severity` varchar(45) DEFAULT NULL,
  `sprintId` varchar(45) DEFAULT NULL,
  `sprintState` varchar(45) DEFAULT NULL,
  `sprintName` varchar(500) DEFAULT NULL,
  `sprintStartDate` datetime DEFAULT NULL,
  `sprintEndDate` datetime DEFAULT NULL,
  `reporter` varchar(450) DEFAULT NULL,
  `progress` varchar(45) DEFAULT NULL,
  `stereoType` varchar(405) DEFAULT NULL,
  `epicLink` varchar(450) DEFAULT NULL,
  `module` varchar(450) DEFAULT NULL,
  `issueType` varchar(450) DEFAULT NULL,
  `parentKey` varchar(45) DEFAULT NULL,
  `storyDefectIdFixVersion` varchar(450) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `defectId_UNIQUE` (`defectId`)
) ENGINE=InnoDB AUTO_INCREMENT=4329190 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table iar_prod_1.domains
DROP TABLE IF EXISTS `domains`;
CREATE TABLE IF NOT EXISTS `domains` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DomainName` varchar(45) DEFAULT NULL,
  `DomainKey` varchar(45) DEFAULT NULL COMMENT 'Additional Field , to check with JIRA mapping',
  `Description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 COMMENT='DomainList Table ';

-- Data exporting was unselected.

-- Dumping structure for table iar_prod_1.project_milestones_status
DROP TABLE IF EXISTS `project_milestones_status`;
CREATE TABLE IF NOT EXISTS `project_milestones_status` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ReleaseName` varchar(45) NOT NULL,
  `ProjectReference` varchar(45) NOT NULL,
  `ProjectName` varchar(200) NOT NULL,
  `Domain` varchar(45) DEFAULT NULL,
  `CCB_Phase` varchar(100) DEFAULT NULL,
  `Test_Status` longtext DEFAULT NULL,
  `KeyRemarksTesting` longtext DEFAULT NULL,
  `Open_Risks_Issues` longtext DEFAULT NULL,
  `Ready2StartTestRemarks` longtext DEFAULT NULL,
  `Ready4SprintTestRemarks` longtext DEFAULT NULL,
  `Ready4UATTestRemarks` longtext DEFAULT NULL,
  `Ready4ProdTestRemarks` longtext DEFAULT NULL,
  `ZEPHYR_Link` longtext DEFAULT NULL,
  `JIRA_Link` longtext DEFAULT NULL,
  `PROJECTREPOSITORY_Link` longtext DEFAULT NULL,
  `CCB_STARTUP_Date` date DEFAULT NULL,
  `CCB_STARTUP_Decision` mediumtext DEFAULT NULL,
  `CCB_Ready4SPRINT_Date` date DEFAULT NULL,
  `CCB_Ready4SPRINT_Decision` varchar(45) DEFAULT NULL,
  `CCB_Ready4UAT_Date` date DEFAULT NULL,
  `CCB_Ready4UAT_Decision` varchar(45) DEFAULT NULL,
  `CCB_Ready4PROD_Date` date DEFAULT NULL,
  `CCB_Ready4PROD_Decision` varchar(45) DEFAULT NULL,
  `CCB_Closure_Date` date DEFAULT NULL,
  `CCB_CLOSURE_Decision` varchar(45) DEFAULT NULL,
  `Test_Coordinator` varchar(90) DEFAULT NULL,
  `Project_Manager` varchar(90) DEFAULT NULL,
  `Scrum_Master` varchar(90) DEFAULT NULL,
  `Delivery_Master_Lead` varchar(90) DEFAULT NULL,
  `ProjectSize_MDs` varchar(45) DEFAULT NULL,
  `ItemType` varchar(45) DEFAULT NULL,
  `Path` longtext DEFAULT NULL,
  `CCB_CLOSURE_Remarks` longblob DEFAULT NULL,
  `unitTestReportPath` longtext DEFAULT NULL,
  `automationReportPath` longtext DEFAULT NULL,
  `performanceReportPath` longtext DEFAULT NULL,
  `securityReportPath` longtext DEFAULT NULL,
  `createdOn` timestamp NOT NULL DEFAULT current_timestamp(),
  `updatedOn` timestamp NULL DEFAULT NULL,
  `Change_Type` varchar(45) DEFAULT NULL,
  `CCB_Date` date DEFAULT NULL,
  `DEL_SPR_Master_Test_Plan` varchar(450) DEFAULT NULL,
  `DEL_SPR_PID_Final` varchar(450) DEFAULT NULL,
  `UAT_Test_Report_Status` varchar(450) DEFAULT NULL,
  `Additional_Info_Dependencies` mediumtext DEFAULT NULL,
  `App_Created_By` varchar(450) DEFAULT NULL,
  `App_Modified_By` varchar(450) DEFAULT NULL,
  `CCBReady4PROD_Actions` longblob DEFAULT NULL,
  `CCBReady4PROD_Remarks` longblob DEFAULT NULL,
  `CCBReady4SPRINT_Actions` longblob DEFAULT NULL,
  `CCBReady4SPRINT_Remarks` longblob DEFAULT NULL,
  `CCBReady4UAT_Actions` longblob DEFAULT NULL,
  `CCBReady4UAT_Remarks` longblob DEFAULT NULL,
  `Content_Type` varchar(405) DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Created_By` varchar(450) DEFAULT NULL,
  `FolderChildCount` varchar(45) DEFAULT NULL,
  `ItemChildCount` varchar(45) DEFAULT NULL,
  `project_milestones_statuscol` varchar(45) DEFAULT NULL,
  `Modified` datetime DEFAULT NULL,
  `Modified_By` varchar(450) DEFAULT NULL,
  `SPRINT_Test_Report_Status` varchar(45) DEFAULT NULL,
  `UAT_Start_Date` date DEFAULT NULL,
  `UAT_Start_Date_Exception` varchar(100) DEFAULT NULL,
  `DEL_STA_BA_Requested` mediumtext DEFAULT NULL,
  `DEL_STA_Budget_Approved` mediumtext DEFAULT NULL,
  `DEL_STA_HighLevel_Planning` mediumtext DEFAULT NULL,
  `DEL_STA_HighLevel_Requirements_Scope` mediumtext DEFAULT NULL,
  `DEL_STA_PARP_Approved` mediumtext DEFAULT NULL,
  `DEL_STA_PID_Stage_1` mediumtext DEFAULT NULL,
  `DEL_STA_Project_BoardSteerco_GO` mediumtext DEFAULT NULL,
  `DEL_STA_SA_Requested` mediumtext DEFAULT NULL,
  `DEL_STA_Staffing_Planview_Booked` mediumtext DEFAULT NULL,
  `DEL_STA_Warranty_Included` mediumtext DEFAULT NULL,
  `DEL_UAT_Business_Value_Summary_Text` mediumtext DEFAULT NULL,
  `DEL_UAT_Implementation_Plan_Draft` mediumtext DEFAULT NULL,
  `DEL_UAT_IT_Test_Report` mediumtext DEFAULT NULL,
  `DEL_UAT_Jira_Stories_Completeness` mediumtext DEFAULT NULL,
  `DEL_UAT_Risk_Matrix_Updated` mediumtext DEFAULT NULL,
  `DEL_UAT_UAT_Test_Case_Ready` blob DEFAULT NULL,
  `Product_Owner_DPMO` varchar(45) DEFAULT NULL,
  `CCB_CLOSURE_Actions` longblob DEFAULT NULL,
  `CCB_CLOSURE_Decision_PreviousState` mediumtext DEFAULT NULL,
  `CCB_OnTime_Registration` mediumtext DEFAULT NULL,
  `CCB_OnTime_UAT_start` mediumtext DEFAULT NULL,
  `CCB_Phase_Dup` mediumtext DEFAULT NULL,
  `CCB_Project_status_change_Notification` mediumtext DEFAULT NULL,
  `CCB_Ready4PRODDecision_PreviousState` mediumtext DEFAULT NULL,
  `CCBReady4SPRINTDecision_PreviousState` mediumtext DEFAULT NULL,
  `CCB_STARTUP_Actions` longblob DEFAULT NULL,
  `CCB_STARTUP_Decision_PreviousState` mediumtext DEFAULT NULL,
  `CCB_STARTUP_Remarks` longblob DEFAULT NULL,
  `Global_Comment` longblob DEFAULT NULL,
  `Implementation_Plan_Status` longblob DEFAULT NULL,
  `Project_Name_Dup` varchar(450) DEFAULT NULL,
  `Project_ManagerId` varchar(45) DEFAULT NULL,
  `GUID` varchar(45) DEFAULT NULL,
  `OdateUIVersionString` varchar(45) DEFAULT NULL,
  `EditorId` varchar(45) DEFAULT NULL,
  `AuthorId` varchar(45) DEFAULT NULL,
  `SP_ID` varchar(45) DEFAULT NULL,
  `DetailedTestStatus` longtext DEFAULT NULL,
  `Test_CoordinatorId` varchar(45) DEFAULT NULL,
  `Delivery_Master_LeadId` varchar(45) DEFAULT NULL,
  `MTP` varchar(45) DEFAULT NULL,
  `Product_OwnerID` varchar(45) DEFAULT NULL,
  `DomainExpert` varchar(45) DEFAULT NULL,
  `CCBDashb` varchar(45) DEFAULT NULL,
  `DEL_SPR_DetailledPlanning` varchar(45) DEFAULT NULL,
  `DEL_SPR_PlanviewStaffingincUAT` varchar(45) DEFAULT NULL,
  `DEL_SPR_ComponentFile` varchar(45) DEFAULT NULL,
  `DEL_SPR_SolutionOutlineArchitecture` varchar(45) DEFAULT NULL,
  `DEL_STA_PIDStage1` varchar(45) DEFAULT NULL,
  `DEL_STA_PIDStage1_TODO` varchar(45) DEFAULT NULL,
  `DEL_STA_BudgetApproved` varchar(45) DEFAULT NULL,
  `DEL_STAStaffingPlanviewBooked` varchar(45) DEFAULT NULL,
  `DEL_STA_HighLevelRequirementsScope` varchar(45) DEFAULT NULL,
  `DEL_STA_WarrantyIncluded` varchar(45) DEFAULT NULL,
  `DEL_STA_HighLevelPlanning` varchar(45) DEFAULT NULL,
  `Scrum_MasterId` varchar(45) DEFAULT NULL,
  `Release_Moment_Dup` varchar(450) DEFAULT NULL,
  `Technical_Release` longtext DEFAULT NULL,
  `WRF_UPDATES` longtext DEFAULT NULL,
  `Jira_defect_projects` varchar(450) DEFAULT NULL,
  PRIMARY KEY (`ID`,`ReleaseName`,`ProjectReference`,`ProjectName`),
  UNIQUE KEY `indx_release_projectReference` (`ReleaseName`,`ProjectReference`,`ProjectName`)
) ENGINE=InnoDB AUTO_INCREMENT=1696 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table iar_prod_1.release_milestones_status
DROP TABLE IF EXISTS `release_milestones_status`;
CREATE TABLE IF NOT EXISTS `release_milestones_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ReleaseName` varchar(45) NOT NULL,
  `ReleaseDate` date DEFAULT NULL,
  `ReleaseTestStatus` longtext DEFAULT NULL,
  `TestKeyMessage` longtext DEFAULT NULL,
  `Risks_Issues` longtext DEFAULT NULL,
  `LastRegistration` date DEFAULT NULL,
  `CCB_Ready4SPRINT` date DEFAULT NULL,
  `CCB_Ready4UAT` date DEFAULT NULL,
  `UAT_StartDate` date DEFAULT NULL,
  `End_UAT` date DEFAULT NULL,
  `Freeze` date DEFAULT NULL,
  `CCB_Ready4PROD` date DEFAULT NULL,
  `CCB_Closure` date DEFAULT NULL,
  `Item_Type` varchar(45) DEFAULT NULL,
  `Path` longtext DEFAULT NULL,
  `insertedOn` timestamp NULL DEFAULT current_timestamp(),
  `updatedOn` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`,`ReleaseName`),
  UNIQUE KEY `ReleaseName_UNIQUE` (`ReleaseName`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=202 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table iar_prod_1.requirement_details
DROP TABLE IF EXISTS `requirement_details`;
CREATE TABLE IF NOT EXISTS `requirement_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projectName` varchar(450) DEFAULT NULL,
  `releaseName` varchar(200) DEFAULT NULL,
  `requirementTreePath` longtext DEFAULT NULL,
  `RequirementTreeNode` longtext DEFAULT NULL,
  `RequirementTreeSubNode` mediumtext DEFAULT NULL,
  `RequirementTreeSubNode1` mediumtext DEFAULT NULL,
  `RequirementID` varchar(100) DEFAULT NULL,
  `requirementName` varchar(450) DEFAULT NULL,
  `priority` varchar(45) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `reqCreationDate` date DEFAULT NULL,
  `createdBy` varchar(200) DEFAULT NULL,
  `lastModifiedBy` varchar(200) DEFAULT NULL,
  `lastModifiedOn` datetime DEFAULT NULL,
  `externalId` varchar(100) DEFAULT NULL,
  `requirementType` varchar(50) DEFAULT NULL,
  `ReqStatus` varchar(100) DEFAULT NULL,
  `attachmentCount` varchar(10) DEFAULT NULL,
  `releaseIds` varchar(45) DEFAULT NULL,
  `requirementTreeIds` varchar(50) DEFAULT NULL,
  `reqReleaseTCCountMapId` varchar(100) DEFAULT NULL,
  `reqReleaseTCCountMapReqId` varchar(100) DEFAULT NULL,
  `reqReleaseTCCountMapReleaseId` varchar(100) DEFAULT NULL,
  `reqReleaseTCCountMapTCCount` varchar(100) DEFAULT NULL,
  `projectId` varchar(10) DEFAULT NULL,
  `releaseNames` varchar(450) DEFAULT NULL,
  `actualTestcaseIds` longtext DEFAULT NULL,
  `insertedOn` timestamp NULL DEFAULT current_timestamp(),
  `updatedOn` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ` (`projectId`,`RequirementID`,`requirementTreeIds`,`releaseName`)
) ENGINE=InnoDB AUTO_INCREMENT=858580 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table iar_prod_1.story_details
DROP TABLE IF EXISTS `story_details`;
CREATE TABLE IF NOT EXISTS `story_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `storyID` varchar(45) DEFAULT NULL,
  `storyKey` varchar(45) DEFAULT NULL,
  `component` varchar(450) DEFAULT NULL,
  `projectKey` varchar(45) DEFAULT NULL,
  `projectName` varchar(450) DEFAULT NULL,
  `fixVersions` varchar(450) DEFAULT NULL,
  `resolution` varchar(45) DEFAULT NULL,
  `defectSevirity` varchar(45) DEFAULT NULL,
  `product` varchar(450) DEFAULT NULL,
  `scrumTeamID` varchar(45) DEFAULT NULL,
  `scrumTeamName` varchar(450) DEFAULT NULL,
  `testTypes` varchar(450) DEFAULT NULL,
  `testLevelDetected` varchar(450) DEFAULT NULL,
  `resolutiondate` datetime DEFAULT NULL,
  `createdUserName` varchar(100) DEFAULT NULL,
  `createdUserID` varchar(45) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `priority` varchar(45) DEFAULT NULL,
  `labels` varchar(300) DEFAULT NULL,
  `assigneeID` varchar(45) DEFAULT NULL,
  `assigneeName` varchar(45) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `severity` varchar(45) DEFAULT NULL,
  `sprintId` varchar(45) DEFAULT NULL,
  `sprintState` varchar(45) DEFAULT NULL,
  `sprintName` varchar(500) DEFAULT NULL,
  `sprintStartDate` datetime DEFAULT NULL,
  `sprintEndDate` datetime DEFAULT NULL,
  `reporter` varchar(450) DEFAULT NULL,
  `epicLink` varchar(450) DEFAULT NULL,
  `module` varchar(450) DEFAULT NULL,
  `issueType` varchar(450) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `storyId_UNIQUE` (`storyID`)
) ENGINE=InnoDB AUTO_INCREMENT=14275702 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table iar_prod_1.tempCount
DROP TABLE IF EXISTS `tempCount`;
CREATE TABLE IF NOT EXISTS `tempCount` (
  `n` int(11) NOT NULL,
  PRIMARY KEY (`n`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table iar_prod_1.test_execution_results
DROP TABLE IF EXISTS `test_execution_results`;
CREATE TABLE IF NOT EXISTS `test_execution_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projectName` varchar(450) DEFAULT NULL,
  `releaseName` varchar(200) DEFAULT NULL,
  `hierarchy` varchar(2200) DEFAULT NULL,
  `folder` varchar(450) DEFAULT NULL,
  `subFolder` varchar(450) DEFAULT NULL,
  `subFolder1` varchar(450) DEFAULT NULL,
  `subFolder2` varchar(450) DEFAULT NULL,
  `subfolder3` varchar(450) DEFAULT NULL,
  `subfolder4` varchar(450) DEFAULT NULL,
  `subfolder5` varchar(450) DEFAULT NULL,
  `subfolder6` varchar(450) DEFAULT NULL,
  `subfolder7` varchar(450) DEFAULT NULL,
  `subfolder8` varchar(450) DEFAULT NULL,
  `subfolder9` varchar(450) DEFAULT NULL,
  `subfolder10` varchar(450) DEFAULT NULL,
  `createdOn` varchar(90) DEFAULT NULL,
  `requirementId` varchar(100) DEFAULT NULL,
  `reqAltId` varchar(450) DEFAULT NULL,
  `requirementName` varchar(450) DEFAULT NULL,
  `environment` varchar(450) DEFAULT NULL,
  `testCaseAltId` varchar(450) DEFAULT NULL,
  `testCaseName` longtext DEFAULT NULL,
  `testCaseId` varchar(100) DEFAULT NULL,
  `automation` varchar(450) DEFAULT NULL,
  `cycle` varchar(120) DEFAULT NULL,
  `testStatus` varchar(450) DEFAULT NULL,
  `executedOn` datetime DEFAULT NULL,
  `tags` varchar(450) DEFAULT NULL,
  `defects` varchar(450) DEFAULT NULL,
  `executedBy` varchar(450) DEFAULT NULL,
  `createdBy` varchar(450) DEFAULT NULL,
  `insertedOn` timestamp NULL DEFAULT current_timestamp(),
  `subFolderLevel` int(11) DEFAULT NULL,
  `updatedOn` timestamp NULL DEFAULT NULL,
  `cyclePhaseId` int(20) DEFAULT NULL,
  `tcrCatalogTreeId` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ` (`projectName`,`releaseName`,`testCaseId`,`cycle`,`cyclePhaseId`,`tcrCatalogTreeId`)
) ENGINE=InnoDB AUTO_INCREMENT=6986212 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for view iar_prod_1.v_defects_data
DROP VIEW IF EXISTS `v_defects_data`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_defects_data` (
	`defectId` VARCHAR(45) NULL COLLATE 'latin1_swedish_ci',
	`defectKey` VARCHAR(45) NULL COLLATE 'latin1_swedish_ci',
	`projectKey` VARCHAR(45) NULL COLLATE 'latin1_swedish_ci',
	`projectName` VARCHAR(450) NULL COLLATE 'latin1_swedish_ci',
	`projectCategory` VARCHAR(450) NULL COLLATE 'latin1_swedish_ci',
	`domainName` VARCHAR(45) NOT NULL COLLATE 'utf8_general_ci',
	`projectReference` VARCHAR(45) NOT NULL COLLATE 'utf8_general_ci',
	`resolution` VARCHAR(45) NOT NULL COLLATE 'latin1_swedish_ci',
	`defectSevirity` VARCHAR(45) NOT NULL COLLATE 'latin1_swedish_ci',
	`product` VARCHAR(450) NOT NULL COLLATE 'latin1_swedish_ci',
	`testLevelDetected` VARCHAR(16) NULL COLLATE 'utf8mb4_general_ci',
	`testTypes` VARCHAR(13) NULL COLLATE 'utf8mb4_general_ci',
	`status` VARCHAR(13) NULL COLLATE 'utf8mb4_general_ci',
	`createdUserName` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`priority` VARCHAR(45) NOT NULL COLLATE 'latin1_swedish_ci',
	`assigneeName` VARCHAR(45) NOT NULL COLLATE 'latin1_swedish_ci',
	`severity` VARCHAR(45) NOT NULL COLLATE 'latin1_swedish_ci',
	`component` VARCHAR(450) NOT NULL COLLATE 'latin1_swedish_ci',
	`sprintId` VARCHAR(45) NOT NULL COLLATE 'latin1_swedish_ci',
	`reporter` VARCHAR(450) NOT NULL COLLATE 'latin1_swedish_ci',
	`progress` VARCHAR(45) NOT NULL COLLATE 'latin1_swedish_ci',
	`stereoType` VARCHAR(405) NOT NULL COLLATE 'latin1_swedish_ci',
	`epicLink` VARCHAR(450) NOT NULL COLLATE 'latin1_swedish_ci',
	`fixVersions` VARCHAR(450) NOT NULL COLLATE 'latin1_swedish_ci',
	`ReleaseName` VARCHAR(45) NOT NULL COLLATE 'utf8_general_ci',
	`issueType` VARCHAR(450) NOT NULL COLLATE 'latin1_swedish_ci',
	`parentKey` VARCHAR(45) NOT NULL COLLATE 'latin1_swedish_ci',
	`storyDefectIdFixVersion` VARCHAR(450) NOT NULL COLLATE 'latin1_swedish_ci',
	`JIRA_KEY` VARCHAR(450) NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view iar_prod_1.v_project_ccb_details
DROP VIEW IF EXISTS `v_project_ccb_details`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_project_ccb_details` (
	`ReleaseName` VARCHAR(45) NOT NULL COLLATE 'utf8_general_ci',
	`ReleaseDate` DATE NULL,
	`ProjectReference` VARCHAR(45) NULL COLLATE 'utf8_general_ci',
	`ProjectName` VARCHAR(200) NULL COLLATE 'utf8_general_ci',
	`test_status` LONGTEXT NULL COLLATE 'utf8_general_ci',
	`KeyRemarksTesting` LONGTEXT NULL COLLATE 'utf8_general_ci',
	`Domain` VARCHAR(45) NULL COLLATE 'utf8_general_ci',
	`ProjectSize_MDs` VARCHAR(45) NULL COLLATE 'utf8_general_ci',
	`PROJECTREPOSITORY_Link` LONGTEXT NULL COLLATE 'utf8_general_ci',
	`JIRA_Link1` LONGTEXT NULL COLLATE 'utf8_general_ci',
	`JIRA_Link` TEXT(65535) NULL COLLATE 'utf8_general_ci',
	`ZEPHYR_Link` LONGTEXT NULL COLLATE 'utf8_general_ci',
	`Delivery_Master_Lead` VARCHAR(90) NULL COLLATE 'utf8_general_ci',
	`Test_Coordinator` VARCHAR(90) NULL COLLATE 'utf8_general_ci',
	`Scrum_Master` VARCHAR(90) NULL COLLATE 'utf8_general_ci',
	`domainName` VARCHAR(45) NOT NULL COLLATE 'utf8_general_ci',
	`CCB_STARTUP_Date` DATE NULL,
	`CCB_Ready4SPRINT_Date` DATE NULL,
	`CCB_Ready4UAT_Date` DATE NULL,
	`CCB_Ready4PROD_Date` DATE NULL,
	`CCB_Closure_Date` DATE NULL,
	`CCB_CLOSURE_Decision` VARCHAR(45) NULL COLLATE 'utf8_general_ci',
	`CCB_STARTUP_Decision` MEDIUMTEXT NULL COLLATE 'utf8_general_ci',
	`CCB_STARTUP_Remarks` LONGTEXT NULL COLLATE 'utf8_general_ci',
	`Ready2StartTestRemarks` LONGTEXT NULL COLLATE 'utf8_general_ci',
	`CCB_Ready4SPRINT_Decision` VARCHAR(45) NULL COLLATE 'utf8_general_ci',
	`CCBReady4SPRINT_Remarks` LONGTEXT NULL COLLATE 'utf8_general_ci',
	`Ready4SprintTestRemarks` LONGTEXT NULL COLLATE 'utf8_general_ci',
	`CCB_Ready4UAT_Decision` VARCHAR(45) NULL COLLATE 'utf8_general_ci',
	`CCBReady4UAT_Remarks` LONGTEXT NULL COLLATE 'utf8_general_ci',
	`Ready4UATTestRemarks` LONGTEXT NULL COLLATE 'utf8_general_ci',
	`LastRegistrationDate` DATE NULL,
	`UAT_START_DATE` DATE NULL,
	`UAT_END_DATE` DATE NULL,
	`FREEZE_DATE` DATE NULL,
	`CCB_Ready4PROD_Decision` VARCHAR(45) NULL COLLATE 'utf8_general_ci',
	`CCBReady4PROD_Remarks` LONGTEXT NULL COLLATE 'utf8_general_ci',
	`Ready4ProdTestRemarks` LONGTEXT NULL COLLATE 'utf8_general_ci',
	`CCB_CLOSURE_Remarks` LONGTEXT NULL COLLATE 'utf8_general_ci',
	`Open_Risks_Issues` LONGTEXT NULL COLLATE 'utf8_general_ci',
	`Project_Manager` VARCHAR(90) NULL COLLATE 'utf8_general_ci',
	`unitTestReportPath` VARCHAR(116) NOT NULL COLLATE 'utf8mb4_general_ci',
	`automationReportPath` VARCHAR(116) NOT NULL COLLATE 'utf8mb4_general_ci',
	`performanceReportPath` VARCHAR(116) NOT NULL COLLATE 'utf8mb4_general_ci',
	`securityReportPath` VARCHAR(116) NOT NULL COLLATE 'utf8mb4_general_ci',
	`IT_TestReportPath` VARCHAR(116) NOT NULL COLLATE 'utf8mb4_general_ci',
	`UAT_TestReportPath` VARCHAR(116) NOT NULL COLLATE 'utf8mb4_general_ci',
	`Jira_defect_projects` VARCHAR(450) NULL COLLATE 'utf8_general_ci',
	`DetailedTestStatus` LONGTEXT NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view iar_prod_1.v_project_wise_defect_keys
DROP VIEW IF EXISTS `v_project_wise_defect_keys`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_project_wise_defect_keys` (
	`ReleaseName` VARCHAR(45) NOT NULL COLLATE 'utf8_general_ci',
	`ProjectReference` VARCHAR(45) NOT NULL COLLATE 'utf8_general_ci',
	`ProjectName` VARCHAR(200) NOT NULL COLLATE 'utf8_general_ci',
	`Domain` VARCHAR(45) NULL COLLATE 'utf8_general_ci',
	`Jira_defect_projects` VARCHAR(450) NULL COLLATE 'utf8_general_ci',
	`JIRA_KEY` VARCHAR(450) NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view iar_prod_1.v_requirement_coverage
DROP VIEW IF EXISTS `v_requirement_coverage`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_requirement_coverage` (
	`ProjectReference` VARCHAR(450) NULL COLLATE 'utf8_general_ci',
	`ProjectName` TEXT(65535) NULL COLLATE 'utf8_general_ci',
	`project` VARCHAR(450) NULL COLLATE 'utf8_general_ci',
	`ReleaseName` VARCHAR(200) NULL COLLATE 'utf8_general_ci',
	`RequirementTreePath` LONGTEXT NULL COLLATE 'utf8_general_ci',
	`RequirementTreeNode` LONGTEXT NULL COLLATE 'utf8_general_ci',
	`RequirementTreeSubNode` MEDIUMTEXT NULL COLLATE 'utf8_general_ci',
	`RequirementTreeSubNode1` MEDIUMTEXT NULL COLLATE 'utf8_general_ci',
	`RequirementID` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`actualTestCaseIDs` LONGTEXT NULL COLLATE 'utf8_general_ci',
	`MappedTCCount` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`actualTestIds` LONGTEXT NULL COLLATE 'utf8_general_ci',
	`testcaseid` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`testStatus` VARCHAR(450) NOT NULL COLLATE 'utf8_general_ci',
	`reqCoverageStatus` VARCHAR(450) NOT NULL COLLATE 'utf8_general_ci',
	`fixVersions` VARCHAR(200) NULL COLLATE 'utf8_general_ci',
	`cycle` VARCHAR(120) NULL COLLATE 'utf8_general_ci',
	`defectId` VARCHAR(450) NULL COLLATE 'utf8_general_ci',
	`executedOn` DATETIME NULL,
	`ReQCoverage` VARCHAR(11) NULL COLLATE 'utf8mb4_general_ci',
	`reqCount_num` BIGINT(21) NOT NULL
) ENGINE=MyISAM;

-- Dumping structure for view iar_prod_1.v_test_results
DROP VIEW IF EXISTS `v_test_results`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `v_test_results` (
	`id` INT(11) NOT NULL,
	`ProjectReference` VARCHAR(450) NULL COLLATE 'utf8_general_ci',
	`ProjectName` TEXT(65535) NULL COLLATE 'utf8_general_ci',
	`DomainName` VARCHAR(45) NULL COLLATE 'utf8_general_ci',
	`Project` VARCHAR(450) NULL COLLATE 'utf8_general_ci',
	`ReleaseName` VARCHAR(200) NULL COLLATE 'utf8_general_ci',
	`Hierarchy` VARCHAR(2200) NULL COLLATE 'utf8_general_ci',
	`Folder` VARCHAR(450) NULL COLLATE 'utf8_general_ci',
	`subFolder` VARCHAR(450) NULL COLLATE 'utf8_general_ci',
	`subFolder1` VARCHAR(450) NULL COLLATE 'utf8_general_ci',
	`subFolder2` VARCHAR(450) NULL COLLATE 'utf8_general_ci',
	`CreatedOn` VARCHAR(90) NULL COLLATE 'utf8_general_ci',
	`RequirementId` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`ReqAltId` VARCHAR(450) NULL COLLATE 'utf8_general_ci',
	`RequirementName` VARCHAR(450) NULL COLLATE 'utf8_general_ci',
	`Environment` VARCHAR(450) NULL COLLATE 'utf8_general_ci',
	`RestCaseAltId` VARCHAR(450) NULL COLLATE 'utf8_general_ci',
	`RestCaseName` LONGTEXT NULL COLLATE 'utf8_general_ci',
	`TestCaseId` VARCHAR(100) NULL COLLATE 'utf8_general_ci',
	`Automation` VARCHAR(450) NULL COLLATE 'utf8_general_ci',
	`Cycle` VARCHAR(120) NULL COLLATE 'utf8_general_ci',
	`tStatus` VARCHAR(450) NULL COLLATE 'utf8_general_ci',
	`testStatus` VARCHAR(12) NULL COLLATE 'utf8mb4_general_ci',
	`ExecutedOn` DATETIME NULL,
	`Tags` VARCHAR(450) NULL COLLATE 'utf8_general_ci',
	`Defects` VARCHAR(450) NULL COLLATE 'utf8_general_ci',
	`ExecutedBy` VARCHAR(450) NULL COLLATE 'utf8_general_ci',
	`CreatedBy` VARCHAR(450) NULL COLLATE 'utf8_general_ci',
	`insertedOn` TIMESTAMP NULL,
	`subFolderLevel` INT(11) NULL,
	`updatedOn` TIMESTAMP NULL
) ENGINE=MyISAM;

-- Dumping structure for view iar_prod_1.v_defects_data
DROP VIEW IF EXISTS `v_defects_data`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_defects_data`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `v_defects_data` AS select `d`.`defectId` AS `defectId`,`d`.`defectKey` AS `defectKey`,`d`.`projectKey` AS `projectKey`,`d`.`projectName` AS `projectName`,`d`.`projectCategory` AS `projectCategory`,ifnull(`pm`.`Domain`,'Un-Defined') AS `domainName`,`pm`.`ProjectReference` AS `projectReference`,ifnull(`d`.`resolution`,'Un-Defined') AS `resolution`,ifnull(`d`.`defectSevirity`,'Un-Defined') AS `defectSevirity`,ifnull(`d`.`product`,'Un-Defined') AS `product`,case when `d`.`testLevelDetected` like '%system%' then 'System Test' when `d`.`testLevelDetected` like '%sprint%' then 'Sprint Test' when `d`.`testLevelDetected` like '%IT%' then 'Integration Test' when `d`.`testLevelDetected` like '%UAT%' then 'UAT' when `d`.`testLevelDetected` like '%unit%' then 'Unit Test' else 'Un-Defined' end AS `testLevelDetected`,case when `d`.`testTypes` like '%func%' then 'Functional' when `d`.`testTypes` like '%aut%' then 'Automation' when `d`.`testTypes` like '%per%' then 'Performance' when `d`.`testTypes` like '%sec%' then 'Security Test' when `d`.`testTypes` like '%uni%' then 'Unit Test' else 'Un-Defined' end AS `testTypes`,case when `d`.`status` like '%To Verify%' then 'In Test' when `d`.`status` like '%To Groom%' then 'In Test' when `d`.`status` like '%In Test%' then 'In Test' when `d`.`status` like '%UAT on-going%' then 'In Test' when `d`.`status` like '%Ready%' then 'UAT Ready' when `d`.`status` like '%open%' then 'Open' when `d`.`status` like '%sprintable%' then 'IN SPRINTABLE' when `d`.`status` like '%progress%' then 'In Progress' when `d`.`status` like '%Analysis%' then 'In Progress' when `d`.`status` like '%Business Validated%' then 'In Progress' when `d`.`status` like '%review%' then 'In Review' when `d`.`status` like '%promot%' then 'To Promote' when `d`.`status` like 'open' then 'Open' when `d`.`status` like 'Backlog' then 'Open' when `d`.`status` like 'Next' then 'Open' when `d`.`status` like '%resol%' then 'Resolved' when `d`.`status` like '%done%' then 'Resolved' when `d`.`status` like '%reopened%' then 'Reopened' when `d`.`status` like '%closed%' then 'Closed' end AS `status`,ifnull(`d`.`createdUserName`,'UnAssigned') AS `createdUserName`,ifnull(`d`.`priority`,'Un-Defined') AS `priority`,ifnull(`d`.`assigneeName`,'UnAssigned') AS `assigneeName`,ifnull(`d`.`severity`,'Un-Defined') AS `severity`,ifnull(`d`.`component`,'No Components') AS `component`,ifnull(`d`.`sprintId`,'Un-Defined') AS `sprintId`,ifnull(`d`.`reporter`,'UnAssigned') AS `reporter`,ifnull(`d`.`progress`,'Un-Defined') AS `progress`,ifnull(`d`.`stereoType`,'Un-Defined') AS `stereoType`,ifnull(`d`.`epicLink`,'Un-Defined') AS `epicLink`,ifnull(`d`.`fixVersions`,'Un-Defined') AS `fixVersions`,`pm`.`ReleaseName` AS `ReleaseName`,ifnull(`d`.`issueType`,'Un-Defined') AS `issueType`,ifnull(`d`.`parentKey`,'Un-Defined') AS `parentKey`,ifnull(`d`.`storyDefectIdFixVersion`,'Un-Defined') AS `storyDefectIdFixVersion`,replace(`pm`.`JIRA_KEY`,' ','') AS `JIRA_KEY` from (`defect_details` `d` join `v_project_wise_defect_keys` `pm` on(`d`.`projectKey` = replace(`pm`.`JIRA_KEY`,' ','') and `d`.`fixVersions` = `pm`.`ReleaseName`)) where `d`.`status` <> 'Closed' and `d`.`fixVersions` <> 'Un-Defined';

-- Dumping structure for view iar_prod_1.v_project_ccb_details
DROP VIEW IF EXISTS `v_project_ccb_details`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_project_ccb_details`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `v_project_ccb_details` AS select `rm`.`ReleaseName` AS `ReleaseName`,`rm`.`ReleaseDate` AS `ReleaseDate`,`pm`.`ProjectReference` AS `ProjectReference`,`pm`.`ProjectName` AS `ProjectName`,`pm`.`Test_Status` AS `test_status`,`pm`.`KeyRemarksTesting` AS `KeyRemarksTesting`,`pm`.`Domain` AS `Domain`,`pm`.`ProjectSize_MDs` AS `ProjectSize_MDs`,`pm`.`PROJECTREPOSITORY_Link` AS `PROJECTREPOSITORY_Link`,`pm`.`JIRA_Link` AS `JIRA_Link1`,concat('https://jira.pvgroup.be/issues/?jql=project in (',`pm`.`Jira_defect_projects`,') AND issuetype in (Defect,"Story Defect") AND status != Closed AND created > startOfMonth(-12) ORDER BY created DESC') AS `JIRA_Link`,`pm`.`ZEPHYR_Link` AS `ZEPHYR_Link`,`pm`.`Delivery_Master_Lead` AS `Delivery_Master_Lead`,`pm`.`Test_Coordinator` AS `Test_Coordinator`,`pm`.`Scrum_Master` AS `Scrum_Master`,ifnull(`pm`.`Domain`,'Un-Defined') AS `domainName`,ifnull(`pm`.`CCB_STARTUP_Date`,`rm`.`LastRegistration`) AS `CCB_STARTUP_Date`,ifnull(`pm`.`CCB_Ready4SPRINT_Date`,`rm`.`CCB_Ready4SPRINT`) AS `CCB_Ready4SPRINT_Date`,ifnull(`pm`.`CCB_Ready4UAT_Date`,`rm`.`CCB_Ready4UAT`) AS `CCB_Ready4UAT_Date`,ifnull(`pm`.`CCB_Ready4PROD_Date`,`rm`.`CCB_Ready4PROD`) AS `CCB_Ready4PROD_Date`,ifnull(`pm`.`CCB_Closure_Date`,`rm`.`CCB_Closure`) AS `CCB_Closure_Date`,`pm`.`CCB_CLOSURE_Decision` AS `CCB_CLOSURE_Decision`,`pm`.`CCB_STARTUP_Decision` AS `CCB_STARTUP_Decision`,convert(`pm`.`CCB_STARTUP_Remarks` using utf8) AS `CCB_STARTUP_Remarks`,convert(`pm`.`Ready2StartTestRemarks` using utf8) AS `Ready2StartTestRemarks`,`pm`.`CCB_Ready4SPRINT_Decision` AS `CCB_Ready4SPRINT_Decision`,convert(`pm`.`CCBReady4SPRINT_Remarks` using utf8) AS `CCBReady4SPRINT_Remarks`,convert(`pm`.`Ready4SprintTestRemarks` using utf8) AS `Ready4SprintTestRemarks`,`pm`.`CCB_Ready4UAT_Decision` AS `CCB_Ready4UAT_Decision`,convert(`pm`.`CCBReady4UAT_Remarks` using utf8) AS `CCBReady4UAT_Remarks`,convert(`pm`.`Ready4UATTestRemarks` using utf8) AS `Ready4UATTestRemarks`,`rm`.`LastRegistration` AS `LastRegistrationDate`,`rm`.`UAT_StartDate` AS `UAT_START_DATE`,`rm`.`End_UAT` AS `UAT_END_DATE`,`rm`.`Freeze` AS `FREEZE_DATE`,`pm`.`CCB_Ready4PROD_Decision` AS `CCB_Ready4PROD_Decision`,convert(`pm`.`CCBReady4PROD_Remarks` using utf8) AS `CCBReady4PROD_Remarks`,convert(`pm`.`Ready4ProdTestRemarks` using utf8) AS `Ready4ProdTestRemarks`,convert(`pm`.`CCB_CLOSURE_Remarks` using utf8) AS `CCB_CLOSURE_Remarks`,`pm`.`Open_Risks_Issues` AS `Open_Risks_Issues`,`pm`.`Project_Manager` AS `Project_Manager`,'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `unitTestReportPath`,'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `automationReportPath`,'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `performanceReportPath`,'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `securityReportPath`,'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `IT_TestReportPath`,'http://weconnect.pvgroup.intranet/project/general/testing/_layouts/15/start.aspx#/Test%20Reports/Forms/AllItems.aspx' AS `UAT_TestReportPath`,`pm`.`Jira_defect_projects` AS `Jira_defect_projects`,`pm`.`DetailedTestStatus` AS `DetailedTestStatus` from (`release_milestones_status` `rm` left join `project_milestones_status` `pm` on(convert(`rm`.`ReleaseName` using utf8) = `pm`.`ReleaseName`));

-- Dumping structure for view iar_prod_1.v_project_wise_defect_keys
DROP VIEW IF EXISTS `v_project_wise_defect_keys`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_project_wise_defect_keys`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `v_project_wise_defect_keys` AS select `t`.`ReleaseName` AS `ReleaseName`,`t`.`ProjectReference` AS `ProjectReference`,`t`.`ProjectName` AS `ProjectName`,`t`.`Domain` AS `Domain`,`t`.`Jira_defect_projects` AS `Jira_defect_projects`,substring_index(substring_index(`t`.`Jira_defect_projects`,',',`n`.`n`),',',-1) AS `JIRA_KEY` from (`project_milestones_status` `t` join `tempCount` `n`) where `n`.`n` <= 1 + (length(`t`.`Jira_defect_projects`) - length(replace(`t`.`Jira_defect_projects`,',',''))) order by substring_index(substring_index(`t`.`Jira_defect_projects`,',',`n`.`n`),',',-1);

-- Dumping structure for view iar_prod_1.v_requirement_coverage
DROP VIEW IF EXISTS `v_requirement_coverage`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_requirement_coverage`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `v_requirement_coverage` AS with _temp_requirement_coverage as (select `temp_requirement_coverage`.`ProjectReference` AS `ProjectReference`,`temp_requirement_coverage`.`ProjectName` AS `ProjectName`,`temp_requirement_coverage`.`project` AS `project`,`temp_requirement_coverage`.`ReleaseName` AS `ReleaseName`,`temp_requirement_coverage`.`RequirementTreePath` AS `RequirementTreePath`,`temp_requirement_coverage`.`RequirementTreeNode` AS `RequirementTreeNode`,`temp_requirement_coverage`.`RequirementTreeSubNode` AS `RequirementTreeSubNode`,`temp_requirement_coverage`.`RequirementTreeSubNode1` AS `RequirementTreeSubNode1`,`temp_requirement_coverage`.`RequirementID` AS `RequirementID`,`temp_requirement_coverage`.`actualTestCaseIDs` AS `actualTestCaseIDs`,`temp_requirement_coverage`.`MappedTCCount` AS `MappedTCCount`,`temp_requirement_coverage`.`actualTestIds` AS `actualTestIds`,`temp_requirement_coverage`.`testcaseid` AS `testcaseid`,`temp_requirement_coverage`.`testStatus` AS `testStatus`,`temp_requirement_coverage`.`reqCoverageStatus` AS `reqCoverageStatus`,`temp_requirement_coverage`.`fixVersions` AS `fixVersions`,`temp_requirement_coverage`.`cycle` AS `cycle`,`temp_requirement_coverage`.`defectId` AS `defectId`,`temp_requirement_coverage`.`executedOn` AS `executedOn`,`temp_requirement_coverage`.`ReQCoverage` AS `ReQCoverage`,row_number() over ( partition by `temp_requirement_coverage`.`RequirementID` order by field(`temp_requirement_coverage`.`testStatus`,'Fail','Pass','InComplete','Blocked','N/A','Not Executed','Not Covered') desc) AS `reqCount_num` from (select trim(substring_index(`rq`.`projectName`,'-',1)) AS `ProjectReference`,trim(substr(`rq`.`projectName`,locate('-',`rq`.`projectName`) + 1)) AS `ProjectName`,`rq`.`projectName` AS `project`,`rq`.`releaseName` AS `ReleaseName`,`rq`.`requirementTreePath` AS `RequirementTreePath`,`rq`.`RequirementTreeNode` AS `RequirementTreeNode`,`rq`.`RequirementTreeSubNode` AS `RequirementTreeSubNode`,`rq`.`RequirementTreeSubNode1` AS `RequirementTreeSubNode1`,`rq`.`RequirementID` AS `RequirementID`,`rq`.`actualTestcaseIds` AS `actualTestCaseIDs`,`rq`.`reqReleaseTCCountMapTCCount` AS `MappedTCCount`,`rq`.`actualTestIDs` AS `actualTestIds`,`tr`.`testCaseId` AS `testcaseid`,ifnull(`tr`.`testStatus`,'Not Covered') AS `testStatus`,ifnull(`tr`.`testStatus`,'Not Covered') AS `reqCoverageStatus`,`tr`.`releaseName` AS `fixVersions`,`tr`.`cycle` AS `cycle`,`tr`.`defects` AS `defectId`,`tr`.`executedOn` AS `executedOn`,case when (`rq`.`actualTestcaseIds` is null or `rq`.`actualTestcaseIds` = '') then 'Not Covered' else 'Covered' end AS `ReQCoverage` from ((with recursive t as (select `iar_prod_1`.`requirement_details`.`id` AS `id`,`iar_prod_1`.`requirement_details`.`projectName` AS `projectName`,`iar_prod_1`.`requirement_details`.`releaseName` AS `releaseName`,`iar_prod_1`.`requirement_details`.`requirementTreePath` AS `requirementTreePath`,`iar_prod_1`.`requirement_details`.`RequirementTreeNode` AS `RequirementTreeNode`,`iar_prod_1`.`requirement_details`.`RequirementTreeSubNode` AS `RequirementTreeSubNode`,`iar_prod_1`.`requirement_details`.`RequirementTreeSubNode1` AS `RequirementTreeSubNode1`,`iar_prod_1`.`requirement_details`.`RequirementID` AS `RequirementID`,`iar_prod_1`.`requirement_details`.`requirementName` AS `requirementName`,`iar_prod_1`.`requirement_details`.`priority` AS `priority`,`iar_prod_1`.`requirement_details`.`createdOn` AS `createdOn`,`iar_prod_1`.`requirement_details`.`reqCreationDate` AS `reqCreationDate`,`iar_prod_1`.`requirement_details`.`createdBy` AS `createdBy`,`iar_prod_1`.`requirement_details`.`lastModifiedBy` AS `lastModifiedBy`,`iar_prod_1`.`requirement_details`.`lastModifiedOn` AS `lastModifiedOn`,`iar_prod_1`.`requirement_details`.`externalId` AS `externalId`,`iar_prod_1`.`requirement_details`.`requirementType` AS `requirementType`,`iar_prod_1`.`requirement_details`.`ReqStatus` AS `ReqStatus`,`iar_prod_1`.`requirement_details`.`attachmentCount` AS `attachmentCount`,`iar_prod_1`.`requirement_details`.`releaseIds` AS `releaseIds`,`iar_prod_1`.`requirement_details`.`requirementTreeIds` AS `requirementTreeIds`,`iar_prod_1`.`requirement_details`.`reqReleaseTCCountMapId` AS `reqReleaseTCCountMapId`,`iar_prod_1`.`requirement_details`.`reqReleaseTCCountMapReqId` AS `reqReleaseTCCountMapReqId`,`iar_prod_1`.`requirement_details`.`reqReleaseTCCountMapReleaseId` AS `reqReleaseTCCountMapReleaseId`,`iar_prod_1`.`requirement_details`.`reqReleaseTCCountMapTCCount` AS `reqReleaseTCCountMapTCCount`,`iar_prod_1`.`requirement_details`.`projectId` AS `projectId`,`iar_prod_1`.`requirement_details`.`releaseNames` AS `releaseNames`,`iar_prod_1`.`requirement_details`.`actualTestcaseIds` AS `actualTestcaseIds`,`iar_prod_1`.`requirement_details`.`insertedOn` AS `insertedOn`,`iar_prod_1`.`requirement_details`.`updatedOn` AS `updatedOn` from `iar_prod_1`.`requirement_details`), n as (select 1 AS `n` union select `n`.`n` + 1 AS `n + 1` from (`n` join `t`) where `n`.`n` <= length(`t`.`actualTestcaseIds`) - length(replace(`t`.`actualTestcaseIds`,',','')))select distinct substring_index(substring_index(`t`.`actualTestcaseIds`,',',`n`.`n`),',',-1) AS `actualTestIDs`,`t`.`id` AS `id`,`t`.`projectName` AS `projectName`,`t`.`releaseName` AS `releaseName`,`t`.`requirementTreePath` AS `requirementTreePath`,`t`.`RequirementTreeNode` AS `RequirementTreeNode`,`t`.`RequirementTreeSubNode` AS `RequirementTreeSubNode`,`t`.`RequirementTreeSubNode1` AS `RequirementTreeSubNode1`,`t`.`RequirementID` AS `RequirementID`,`t`.`requirementName` AS `requirementName`,`t`.`priority` AS `priority`,`t`.`createdOn` AS `createdOn`,`t`.`reqCreationDate` AS `reqCreationDate`,`t`.`createdBy` AS `createdBy`,`t`.`lastModifiedBy` AS `lastModifiedBy`,`t`.`lastModifiedOn` AS `lastModifiedOn`,`t`.`externalId` AS `externalId`,`t`.`requirementType` AS `requirementType`,`t`.`ReqStatus` AS `ReqStatus`,`t`.`attachmentCount` AS `attachmentCount`,`t`.`releaseIds` AS `releaseIds`,`t`.`requirementTreeIds` AS `requirementTreeIds`,`t`.`reqReleaseTCCountMapId` AS `reqReleaseTCCountMapId`,`t`.`reqReleaseTCCountMapReqId` AS `reqReleaseTCCountMapReqId`,`t`.`reqReleaseTCCountMapReleaseId` AS `reqReleaseTCCountMapReleaseId`,`t`.`reqReleaseTCCountMapTCCount` AS `reqReleaseTCCountMapTCCount`,`t`.`projectId` AS `projectId`,`t`.`releaseNames` AS `releaseNames`,`t`.`actualTestcaseIds` AS `actualTestcaseIds`,`t`.`insertedOn` AS `insertedOn`,`t`.`updatedOn` AS `updatedOn` from (`n` join (select `iar_prod_1`.`requirement_details`.`id` AS `id`,`iar_prod_1`.`requirement_details`.`projectName` AS `projectName`,`iar_prod_1`.`requirement_details`.`releaseName` AS `releaseName`,`iar_prod_1`.`requirement_details`.`requirementTreePath` AS `requirementTreePath`,`iar_prod_1`.`requirement_details`.`RequirementTreeNode` AS `RequirementTreeNode`,`iar_prod_1`.`requirement_details`.`RequirementTreeSubNode` AS `RequirementTreeSubNode`,`iar_prod_1`.`requirement_details`.`RequirementTreeSubNode1` AS `RequirementTreeSubNode1`,`iar_prod_1`.`requirement_details`.`RequirementID` AS `RequirementID`,`iar_prod_1`.`requirement_details`.`requirementName` AS `requirementName`,`iar_prod_1`.`requirement_details`.`priority` AS `priority`,`iar_prod_1`.`requirement_details`.`createdOn` AS `createdOn`,`iar_prod_1`.`requirement_details`.`reqCreationDate` AS `reqCreationDate`,`iar_prod_1`.`requirement_details`.`createdBy` AS `createdBy`,`iar_prod_1`.`requirement_details`.`lastModifiedBy` AS `lastModifiedBy`,`iar_prod_1`.`requirement_details`.`lastModifiedOn` AS `lastModifiedOn`,`iar_prod_1`.`requirement_details`.`externalId` AS `externalId`,`iar_prod_1`.`requirement_details`.`requirementType` AS `requirementType`,`iar_prod_1`.`requirement_details`.`ReqStatus` AS `ReqStatus`,`iar_prod_1`.`requirement_details`.`attachmentCount` AS `attachmentCount`,`iar_prod_1`.`requirement_details`.`releaseIds` AS `releaseIds`,`iar_prod_1`.`requirement_details`.`requirementTreeIds` AS `requirementTreeIds`,`iar_prod_1`.`requirement_details`.`reqReleaseTCCountMapId` AS `reqReleaseTCCountMapId`,`iar_prod_1`.`requirement_details`.`reqReleaseTCCountMapReqId` AS `reqReleaseTCCountMapReqId`,`iar_prod_1`.`requirement_details`.`reqReleaseTCCountMapReleaseId` AS `reqReleaseTCCountMapReleaseId`,`iar_prod_1`.`requirement_details`.`reqReleaseTCCountMapTCCount` AS `reqReleaseTCCountMapTCCount`,`iar_prod_1`.`requirement_details`.`projectId` AS `projectId`,`iar_prod_1`.`requirement_details`.`releaseNames` AS `releaseNames`,`iar_prod_1`.`requirement_details`.`actualTestcaseIds` AS `actualTestcaseIds`,`iar_prod_1`.`requirement_details`.`insertedOn` AS `insertedOn`,`iar_prod_1`.`requirement_details`.`updatedOn` AS `updatedOn` from `iar_prod_1`.`requirement_details`) `t`)) `rq` left join (with _test_execution_results as (select `iar_prod_1`.`test_execution_results`.`id` AS `id`,`iar_prod_1`.`test_execution_results`.`projectName` AS `projectName`,`iar_prod_1`.`test_execution_results`.`releaseName` AS `releaseName`,`iar_prod_1`.`test_execution_results`.`hierarchy` AS `hierarchy`,`iar_prod_1`.`test_execution_results`.`folder` AS `folder`,`iar_prod_1`.`test_execution_results`.`subFolder` AS `subFolder`,`iar_prod_1`.`test_execution_results`.`subFolder1` AS `subFolder1`,`iar_prod_1`.`test_execution_results`.`subFolder2` AS `subFolder2`,`iar_prod_1`.`test_execution_results`.`subfolder3` AS `subfolder3`,`iar_prod_1`.`test_execution_results`.`subfolder4` AS `subfolder4`,`iar_prod_1`.`test_execution_results`.`subfolder5` AS `subfolder5`,`iar_prod_1`.`test_execution_results`.`subfolder6` AS `subfolder6`,`iar_prod_1`.`test_execution_results`.`subfolder7` AS `subfolder7`,`iar_prod_1`.`test_execution_results`.`subfolder8` AS `subfolder8`,`iar_prod_1`.`test_execution_results`.`subfolder9` AS `subfolder9`,`iar_prod_1`.`test_execution_results`.`subfolder10` AS `subfolder10`,`iar_prod_1`.`test_execution_results`.`createdOn` AS `createdOn`,`iar_prod_1`.`test_execution_results`.`requirementId` AS `requirementId`,`iar_prod_1`.`test_execution_results`.`reqAltId` AS `reqAltId`,`iar_prod_1`.`test_execution_results`.`requirementName` AS `requirementName`,`iar_prod_1`.`test_execution_results`.`environment` AS `environment`,`iar_prod_1`.`test_execution_results`.`testCaseAltId` AS `testCaseAltId`,`iar_prod_1`.`test_execution_results`.`testCaseName` AS `testCaseName`,`iar_prod_1`.`test_execution_results`.`testCaseId` AS `testCaseId`,`iar_prod_1`.`test_execution_results`.`automation` AS `automation`,`iar_prod_1`.`test_execution_results`.`cycle` AS `cycle`,`iar_prod_1`.`test_execution_results`.`testStatus` AS `testStatus`,`iar_prod_1`.`test_execution_results`.`executedOn` AS `executedOn`,`iar_prod_1`.`test_execution_results`.`tags` AS `tags`,`iar_prod_1`.`test_execution_results`.`defects` AS `defects`,`iar_prod_1`.`test_execution_results`.`executedBy` AS `executedBy`,`iar_prod_1`.`test_execution_results`.`createdBy` AS `createdBy`,`iar_prod_1`.`test_execution_results`.`insertedOn` AS `insertedOn`,`iar_prod_1`.`test_execution_results`.`subFolderLevel` AS `subFolderLevel`,`iar_prod_1`.`test_execution_results`.`updatedOn` AS `updatedOn`,`iar_prod_1`.`test_execution_results`.`cyclePhaseId` AS `cyclePhaseId`,`iar_prod_1`.`test_execution_results`.`tcrCatalogTreeId` AS `tcrCatalogTreeId`,row_number() over ( partition by `iar_prod_1`.`test_execution_results`.`testCaseId` order by `iar_prod_1`.`test_execution_results`.`executedOn` desc) AS `row_num` from `iar_prod_1`.`test_execution_results`)select `_test_execution_results`.`id` AS `id`,`_test_execution_results`.`projectName` AS `projectName`,`_test_execution_results`.`releaseName` AS `releaseName`,`_test_execution_results`.`hierarchy` AS `hierarchy`,`_test_execution_results`.`folder` AS `folder`,`_test_execution_results`.`subFolder` AS `subFolder`,`_test_execution_results`.`subFolder1` AS `subFolder1`,`_test_execution_results`.`subFolder2` AS `subFolder2`,`_test_execution_results`.`subfolder3` AS `subfolder3`,`_test_execution_results`.`subfolder4` AS `subfolder4`,`_test_execution_results`.`subfolder5` AS `subfolder5`,`_test_execution_results`.`subfolder6` AS `subfolder6`,`_test_execution_results`.`subfolder7` AS `subfolder7`,`_test_execution_results`.`subfolder8` AS `subfolder8`,`_test_execution_results`.`subfolder9` AS `subfolder9`,`_test_execution_results`.`subfolder10` AS `subfolder10`,`_test_execution_results`.`createdOn` AS `createdOn`,`_test_execution_results`.`requirementId` AS `requirementId`,`_test_execution_results`.`reqAltId` AS `reqAltId`,`_test_execution_results`.`requirementName` AS `requirementName`,`_test_execution_results`.`environment` AS `environment`,`_test_execution_results`.`testCaseAltId` AS `testCaseAltId`,`_test_execution_results`.`testCaseName` AS `testCaseName`,`_test_execution_results`.`testCaseId` AS `testCaseId`,`_test_execution_results`.`automation` AS `automation`,`_test_execution_results`.`cycle` AS `cycle`,`_test_execution_results`.`testStatus` AS `testStatus`,`_test_execution_results`.`executedOn` AS `executedOn`,`_test_execution_results`.`tags` AS `tags`,`_test_execution_results`.`defects` AS `defects`,`_test_execution_results`.`executedBy` AS `executedBy`,`_test_execution_results`.`createdBy` AS `createdBy`,`_test_execution_results`.`insertedOn` AS `insertedOn`,`_test_execution_results`.`subFolderLevel` AS `subFolderLevel`,`_test_execution_results`.`updatedOn` AS `updatedOn`,`_test_execution_results`.`cyclePhaseId` AS `cyclePhaseId`,`_test_execution_results`.`tcrCatalogTreeId` AS `tcrCatalogTreeId`,`_test_execution_results`.`row_num` AS `row_num` from `_test_execution_results` where `_test_execution_results`.`row_num` = 1) `tr` on(`rq`.`actualTestIDs` = `tr`.`testCaseId`))) `temp_requirement_coverage`)select `_temp_requirement_coverage`.`ProjectReference` AS `ProjectReference`,`_temp_requirement_coverage`.`ProjectName` AS `ProjectName`,`_temp_requirement_coverage`.`project` AS `project`,`_temp_requirement_coverage`.`ReleaseName` AS `ReleaseName`,`_temp_requirement_coverage`.`RequirementTreePath` AS `RequirementTreePath`,`_temp_requirement_coverage`.`RequirementTreeNode` AS `RequirementTreeNode`,`_temp_requirement_coverage`.`RequirementTreeSubNode` AS `RequirementTreeSubNode`,`_temp_requirement_coverage`.`RequirementTreeSubNode1` AS `RequirementTreeSubNode1`,`_temp_requirement_coverage`.`RequirementID` AS `RequirementID`,`_temp_requirement_coverage`.`actualTestCaseIDs` AS `actualTestCaseIDs`,`_temp_requirement_coverage`.`MappedTCCount` AS `MappedTCCount`,`_temp_requirement_coverage`.`actualTestIds` AS `actualTestIds`,`_temp_requirement_coverage`.`testcaseid` AS `testcaseid`,`_temp_requirement_coverage`.`testStatus` AS `testStatus`,`_temp_requirement_coverage`.`reqCoverageStatus` AS `reqCoverageStatus`,`_temp_requirement_coverage`.`fixVersions` AS `fixVersions`,`_temp_requirement_coverage`.`cycle` AS `cycle`,`_temp_requirement_coverage`.`defectId` AS `defectId`,`_temp_requirement_coverage`.`executedOn` AS `executedOn`,`_temp_requirement_coverage`.`ReQCoverage` AS `ReQCoverage`,`_temp_requirement_coverage`.`reqCount_num` AS `reqCount_num` from `_temp_requirement_coverage` group by `_temp_requirement_coverage`.`ProjectName`,`_temp_requirement_coverage`.`project`,`_temp_requirement_coverage`.`ReleaseName`,`_temp_requirement_coverage`.`RequirementID`,`_temp_requirement_coverage`.`actualTestCaseIDs`;

-- Dumping structure for view iar_prod_1.v_test_results
DROP VIEW IF EXISTS `v_test_results`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `v_test_results`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `v_test_results` AS select `test_execution_results`.`id` AS `id`,trim(substring_index(`test_execution_results`.`projectName`,'-',1)) AS `ProjectReference`,trim(substr(`test_execution_results`.`projectName`,locate('-',`test_execution_results`.`projectName`) + 1)) AS `ProjectName`,`project_milestones_status`.`Domain` AS `DomainName`,`test_execution_results`.`projectName` AS `Project`,`test_execution_results`.`releaseName` AS `ReleaseName`,`test_execution_results`.`hierarchy` AS `Hierarchy`,nullif(`test_execution_results`.`folder`,'') AS `Folder`,nullif(`test_execution_results`.`subFolder`,'') AS `subFolder`,nullif(`test_execution_results`.`subFolder1`,'') AS `subFolder1`,nullif(`test_execution_results`.`subFolder2`,'') AS `subFolder2`,`test_execution_results`.`createdOn` AS `CreatedOn`,`test_execution_results`.`requirementId` AS `RequirementId`,`test_execution_results`.`reqAltId` AS `ReqAltId`,`test_execution_results`.`requirementName` AS `RequirementName`,`test_execution_results`.`environment` AS `Environment`,`test_execution_results`.`testCaseAltId` AS `RestCaseAltId`,`test_execution_results`.`testCaseName` AS `RestCaseName`,`test_execution_results`.`testCaseId` AS `TestCaseId`,`test_execution_results`.`automation` AS `Automation`,`test_execution_results`.`cycle` AS `Cycle`,`test_execution_results`.`testStatus` AS `tStatus`,case when `test_execution_results`.`testStatus` like '%fail%' then 'Fail' when `test_execution_results`.`testStatus` like '%pass%' then 'Pass' when `test_execution_results`.`testStatus` like '%incomp%' then 'Incomplete' when `test_execution_results`.`testStatus` like '%blocked%' then 'Blocked' when `test_execution_results`.`testStatus` like '%Not exec%' then 'Not Executed' when `test_execution_results`.`testStatus` like '%N/A%' then 'N/A' else 'Not Executed' end AS `testStatus`,`test_execution_results`.`executedOn` AS `ExecutedOn`,`test_execution_results`.`tags` AS `Tags`,`test_execution_results`.`defects` AS `Defects`,`test_execution_results`.`executedBy` AS `ExecutedBy`,`test_execution_results`.`createdBy` AS `CreatedBy`,`test_execution_results`.`insertedOn` AS `insertedOn`,`test_execution_results`.`subFolderLevel` AS `subFolderLevel`,`test_execution_results`.`updatedOn` AS `updatedOn` from (`test_execution_results` left join `project_milestones_status` on(trim(convert(substring_index(`test_execution_results`.`projectName`,'-',1) using utf8)) = `project_milestones_status`.`ProjectReference` and convert(trim(`test_execution_results`.`releaseName`) using utf8) = `project_milestones_status`.`ReleaseName`));

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
