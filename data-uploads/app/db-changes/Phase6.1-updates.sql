ALTER TABLE `defect_details`
	ADD COLUMN `epicStatus` LONGTEXT NULL AFTER `epicName`;

ALTER TABLE `test_execution_results`
	ADD COLUMN `epicName` VARCHAR(500) NULL AFTER `epicLink`,
	ADD COLUMN `epicStatus` VARCHAR(500) NULL AFTER `epicName`;

  ALTER TABLE `test_execution_results`
	ADD COLUMN `usergroup` INT NULL DEFAULT NULL AFTER `priority`;

ALTER TABLE `requirement_details`
	ADD COLUMN `epicName` VARCHAR(100) NULL AFTER `epicLink`,
	ADD COLUMN `epicStatus` VARCHAR(100) NULL AFTER `epicName`;

  
ALTER TABLE `user_story_details`
	ADD COLUMN `epicStatus` LONGTEXT NULL AFTER `epicName`;

/* Defects status mapping */

select
  `d`.`defectId` AS `defectId`,
  `d`.`defectKey` AS `defectKey`,
  `d`.`projectKey` AS `projectKey`,
  `d`.`projectName` AS `projectName`,
  `d`.`projectCategory` AS `projectCategory`,
  ifnull(`pm`.`Domain`, 'Un-Defined') AS `domainName`,
  `pm`.`ProjectReference` AS `projectReference`,
  ifnull(`d`.`resolution`, 'Un-Defined') AS `resolution`,
  ifnull(`d`.`defectSevirity`, 'Un-Defined') AS `defectSevirity`,
  ifnull(`d`.`product`, 'Un-Defined') AS `product`,case
    when `d`.`testLevelDetected` like '%system%' then 'System Test'
    when `d`.`testLevelDetected` like '%sprint%' then 'Sprint Test'
    when `d`.`testLevelDetected` like '%IT%' then 'Integration Test'
    when `d`.`testLevelDetected` like '%UAT%' then 'UAT'
    when `d`.`testLevelDetected` like '%unit%' then 'Unit Test'
    else 'Un-Defined'
  end AS `testLevelDetected`,case
    when `d`.`testTypes` like '%func%' then 'Functional'
    when `d`.`testTypes` like '%aut%' then 'Automation'
    when `d`.`testTypes` like '%per%' then 'Performance'
    when `d`.`testTypes` like '%sec%' then 'Security Test'
    when `d`.`testTypes` like '%uni%' then 'Unit Test'
    else 'Un-Defined'
  end AS `testTypes`,case
    when `d`.`status` like '%To Validate%' then 'To Validate'
    when `d`.`status` like '%To Verify%' then 'In Test'
    when `d`.`status` like '%To Groom%' then 'Analysis'
    when `d`.`status` like '%In Test%' then 'In Test'
    when `d`.`status` like '%UAT on-going%' then 'In Test'
    when `d`.`status` like '%Ready%' then 'To Validate'
    when `d`.`status` like '%UAT Ready%' then 'To Validate'
    when `d`.`status` like '%open%' then 'Open'
    when `d`.`status` like '%sprintable%' then 'Analysis'
    when `d`.`status` like '%progress%' then 'Development'
    when `d`.`status` like '%Analysis%' then 'Analysis'
    when `d`.`status` like '%Business Validated%' then 'Backlog'
    when `d`.`status` like '%review%' then 'Development'
    when `d`.`status` like '%incep%' then 'Analysis'
    when `d`.`status` like '%promot%' then 'Development'
    when `d`.`status` like 'open' then 'Open'
    when `d`.`status` like 'Backlog' then 'Backlog'
    when `d`.`status` like 'Next' then 'Analysis'
    when `d`.`status` like '%resol%' then 'To Validate'
    when `d`.`status` like '%Resolved%' then 'To Validate'
    when `d`.`status` like '%done%' then 'Closed'
    when `d`.`status` like '%reopened%' then 'Open'
    when `d`.`status` like '%closed%' then 'Closed'
    when `d`.`status` like '%prd ready%' then 'Closed'
    when `d`.`status` like '%In Analysis%' then 'Analysis'
    when `d`.`status` like '%In Progress%' then 'Development'
    when `d`.`status` like '%In Review%' then 'In Test'
    when `d`.`status` like '%PRD To Verify%' then 'Closed'
    when `d`.`status` like '%Closed%' then 'Closed'
    when `d`.`status` like '%Resolved%' then 'To validate'
    when `d`.`status` like '%To Promote%' then 'Development'
    else `d`.`status`
  end AS `status`,
  `d`.`createdOn` AS `createdOn`,
  `d`.`summary` AS `summary`,
  ifnull(`d`.`createdUserName`, 'UnAssigned') AS `createdUserName`,case
    when `d`.`priority` like '%Major%' then 'Major'
    when `d`.`priority` like '%Highest%' then 'Major'
    when `d`.`priority` like '%High%' then 'Major'
    when `d`.`priority` like '%Blocker%' then 'Blocker'
    when `d`.`priority` like '%Critical%' then 'Critical'
    when `d`.`priority` like '%Minor%' then 'Minor'
    when `d`.`priority` like '%Low%' then 'Minor'
    when `d`.`priority` like '%Lowest%' then 'Minor'
    when `d`.`priority` like '%Trivial%' then 'Minor'
    when `d`.`priority` like '%Medium%' then 'Medium'
    when `d`.`priority` is null then 'Un-Defined'
    else `d`.`priority`
  end AS `priority`,
  ifnull(`d`.`assigneeName`, 'UnAssigned') AS `assigneeName`,
  ifnull(`d`.`assigneeID`, 'UnAssigned') AS `assigneeID`,
  ifnull(`d`.`severity`, 'Un-Defined') AS `severity`,
  ifnull(`d`.`component`, 'No Components') AS `component`,
  ifnull(`d`.`sprintId`, 'Un-Defined') AS `sprintId`,
  ifnull(`d`.`reporter`, 'UnAssigned') AS `reporter`,
  ifnull(`d`.`progress`, 'Un-Defined') AS `progress`,
  ifnull(`d`.`stereoType`, 'Un-Defined') AS `stereoType`,
  ifnull(`d`.`epicLink`, 'Un-Defined') AS `epicLink`,
  ifnull(`d`.`epicName`, 'No Epic Mapped') AS `epicName`,
  case
    when `d`.`epicStatus` like '%Closed%' then 'Closed'
    when `d`.`epicStatus` like '%BA In Progress%' then 'Analysis'
	when `d`.`epicStatus` like '%BA To Validate%' then 'Analysis'
    when `d`.`epicStatus` like '%Backlog%' then 'Backlog'
	when `d`.`epicStatus` like '%Open%' then 'Open' 
when `d`.`epicStatus` like '%Done%' then 'Closed'
when `d`.`epicStatus` like '%In Review%' then 'Development'
when `d`.`epicStatus` like '%Resolved%' then 'To Validate'
when `d`.`epicStatus` like '%FA In Progress%' then 'Analysis'
when `d`.`epicStatus` like '%Sprintable%' then 'Analysis'
when `d`.`epicStatus` like '%Business Validated%' then 'Backlog'
when `d`.`epicStatus` like '%Reopened%' then 'Open'
when `d`.`epicStatus` like '%In Progress%' then 'Development'
when `d`.`epicStatus` like '%To Groom%' then 'Analysis'
when `d`.`epicStatus` like '%To Promote%' then 'Development'
when `d`.`epicStatus` like '%In Test%' then 'In Test'
when `d`.`epicStatus` like '%In Analysis%' then 'Analysis'
when `d`.`epicStatus` like '%PO To Validate%' then 'To Validate'
when `d`.`epicStatus` like '%TA In Progress%' then 'Analysis'
when `d`.`epicStatus` like '%PRD Ready%' then 'Closed'
when `d`.`epicStatus` like '%Next%' then 'Analysis'
when `d`.`epicStatus` like '%UAT Ready%' then 'To Validate'
when `d`.`epicStatus` like '%TA To Validate%' then 'Analysis'
when `d`.`epicStatus` like '%UAT on-going%' then 'In Test'
when `d`.`epicStatus` like '%PRD To Verify%' then 'Closed'
when `d`.`epicStatus` is null then 'No Status Mapped'
    else `d`.`epicStatus`
  end AS `epicStatus`,
  ifnull(`d`.`fixVersions`, 'Un-Defined') AS `fixVersions`,
  `pm`.`ReleaseName` AS `ReleaseName`,
  ifnull(`d`.`issueType`, 'Un-Defined') AS `issueType`,
  ifnull(`d`.`parentKey`, 'Un-Defined') AS `parentKey`,
  ifnull(`d`.`storyDefectIdFixVersion`, 'Un-Defined') AS `storyDefectIdFixVersion`,
  replace(`pm`.`JIRA_KEY`, ' ', '') AS `JIRA_KEY`
from
  (
    `iar_byld_1`.`defect_details` `d`
    join `iar_byld_1`.`v_project_wise_defect_keys` `pm` on(
      `d`.`projectKey` = replace(`pm`.`JIRA_KEY`, ' ', '')
      and `d`.`fixVersions` = `pm`.`ReleaseName`
    )
  )
where
  `d`.`fixVersions` <> 'Un-Defined' 

/*User stories Status Mapping */

(select `d`.`userStoryID` AS `userStoryID`,`d`.`storyKey` AS `storyKey`,`d`.`projectKey` AS `projectKey`,`d`.`projectName` AS `projectName`,
case when `d`.`status` like '%Backlog%' then 'Backlog' 
when `d`.`status` like '%BA in progress%' then 'Backlog' 
when `d`.`status` like '%BUSINESS VALIDATED%' then 'Backlog' 
when `d`.`status` like '%PRD Ready%' then 'Closed' 
when `d`.`status` like '%Open%' then 'Open' 
when `d`.`status` like 'Reopened' then 'Open' 
when `d`.`status` like '%BA to validate%' then 'Analysis' 
when `d`.`status` like '%FA in progress%' then 'Analysis' 
when `d`.`status` like '%FA to validate%' then 'Analysis' 
when `d`.`status` like '%TA in progress%' then 'Analysis' 
when `d`.`status` like '%TA to validate%' then 'Analysis' 
when `d`.`status` like '%To groom%' then 'Analysis' 
when `d`.`status` like '%Sprintable%' then 'Analysis' 
when `d`.`status` like '%In Inception%' then 'Analysis' 
when `d`.`status` like '%Ready%' then 'Analysis' 
when `d`.`status` like '%Next%' then 'Analysis' 
when `d`.`status` like '%In progress%' then 'Development'
when `d`.`status` like 'In review' then 'Development' 
when `d`.`status` like 'To promote' then 'Development' 
when `d`.`status` like 'In test' then 'In Test' 
when `d`.`status` like '%To Verify%' then 'In Test' 
when `d`.`status` like '%To review%' then 'In Test' 
when `d`.`status` like '%UAT ON-GOING%' then 'In Test' 
when `d`.`status` like 'Ready for uat' then 'Closed' 
when `d`.`status` like '%Closed%' then 'Closed' 
when `d`.`status` like '%UAT ready%' then 'To Validate' 
when `d`.`status` like '%prd ready%' then 'Closed' 
when `d`.`status` like '%PRD READY%' then 'Closed' 
when `d`.`status` like '%PRD TO VERIFY%' then 'Closed' 
when `d`.`status` like '%Resolved%' then 'To Validate' 
when `d`.`status` like '%PO To validate%' then 'To Validate' 
when `d`.`status` like '%To Do%' then 'Backlog' 
when `d`.`status` like '%To Analyze (FA)%' then 'Backlog' 
when `d`.`status` like '%In Analysis%' then 'Analysis' 
when `d`.`status` like '%Done%' then 'Closed' 
when `d`.`status` like '%Closed%' then 'Closed' 
when `d`.`status` like '%To validate%' then 'To Validate' end AS `storyStatus`,
`d`.`summary` AS `summary`,ifnull(`pm`.`Domain`,'Un-Defined') AS `domainName`,`pm`.`ProjectReference` AS `ProjectReference`,ifnull(`d`.`resolution`,'Un-Defined') AS `resolution`,ifnull(`d`.`priority`,'Un-Defined') AS `priority`,ifnull(`d`.`assigneeName`,'UnAssigned') AS `assigneeName`,ifnull(`d`.`severity`,'Un-Defined') AS `severity`,ifnull(`d`.`component`,'No Components') AS `component`,ifnull(`d`.`sprintId`,'Un-Defined') AS `sprintId`,ifnull(`d`.`reporter`,'UnAssigned') AS `reporter`,ifnull(`d`.`epicLink`,'Un-Defined') AS `epicLink`,ifnull(`d`.`epicName`,'No Epic Mapped') AS `epicName`,
case
    when `d`.`epicStatus` like '%Closed%' then 'Closed'
    when `d`.`epicStatus` like '%BA In Progress%' then 'Analysis'
	when `d`.`epicStatus` like '%BA To Validate%' then 'Analysis'
    when `d`.`epicStatus` like '%Backlog%' then 'Backlog'
	when `d`.`epicStatus` like '%Open%' then 'Open' 
when `d`.`epicStatus` like '%Done%' then 'Closed'
when `d`.`epicStatus` like '%In Review%' then 'Development'
when `d`.`epicStatus` like '%Resolved%' then 'To Validate'
when `d`.`epicStatus` like '%FA In Progress%' then 'Analysis'
when `d`.`epicStatus` like '%Sprintable%' then 'Analysis'
when `d`.`epicStatus` like '%Business Validated%' then 'Backlog'
when `d`.`epicStatus` like '%Reopened%' then 'Open'
when `d`.`epicStatus` like '%In Progress%' then 'Development'
when `d`.`epicStatus` like '%To Groom%' then 'Analysis'
when `d`.`epicStatus` like '%To Promote%' then 'Development'
when `d`.`epicStatus` like '%In Test%' then 'In Test'
when `d`.`epicStatus` like '%In Analysis%' then 'Analysis'
when `d`.`epicStatus` like '%PO To Validate%' then 'To Validate'
when `d`.`epicStatus` like '%TA In Progress%' then 'Analysis'
when `d`.`epicStatus` like '%PRD Ready%' then 'Closed'
when `d`.`epicStatus` like '%Next%' then 'Analysis'
when `d`.`epicStatus` like '%UAT Ready%' then 'To Validate'
when `d`.`epicStatus` like '%TA To Validate%' then 'Analysis'
when `d`.`epicStatus` like '%UAT on-going%' then 'In Test'
when `d`.`epicStatus` like '%PRD To Verify%' then 'Closed'
when `d`.`epicStatus` is null then 'No Status Mapped'
    else `d`.`epicStatus`
  end AS `epicStatus`,
ifnull(`d`.`fixVersions`,'Un-Defined') AS `fixVersions`,`pm`.`ReleaseName` AS `ReleaseName`,ifnull(`d`.`issueType`,'Un-Defined') AS `issueType`,replace(`pm`.`JIRA_KEY`,' ','') AS `JIRA_KEY` from (`iar_byld_1`.`user_story_details` `d` join `iar_byld_1`.`v_project_wise_defect_keys` `pm` on(`d`.`projectKey` = replace(`pm`.`JIRA_KEY`,' ','') and `d`.`fixVersions` = `pm`.`ReleaseName`))) 


/*Adding user group*/

ALTER TABLE `test_execution_results`
	ADD COLUMN `usergroup` INT NULL DEFAULT NULL AFTER `priority`;


   (SELECT `test_execution_results`.`id`
        AS
        `id`,
        Trim(
        Substring_index(
        REPLACE(`test_execution_results`.`projectname`, '–', '-'), '-', 1)) AS
             `ProjectReference`,
        Trim(Substr(`test_execution_results`.`projectname`,
                  Locate('-', `test_execution_results`.`projectname`)
                                                            + 1))
        AS `ProjectName`,
        `project_milestones_status`.`domain`
        AS `DomainName`,
        `test_execution_results`.`projectname`
        AS `Project`,
        `test_execution_results`.`releasename`
        AS `ReleaseName`,
        `test_execution_results`.`hierarchy`
        AS `Hierarchy`,
        Nullif(`test_execution_results`.`folder`, '')
        AS `Folder`,
        Nullif(`test_execution_results`.`subfolder`, '')
        AS `subFolder`,
        Nullif(`test_execution_results`.`subfolder1`, '')
        AS `subFolder1`,
        Nullif(`test_execution_results`.`subfolder2`, '')
        AS `subFolder2`,
        Nullif(`test_execution_results`.`subfolder3`, '')
        AS `subFolder3`,
        Nullif(`test_execution_results`.`subfolder4`, '')
        AS `subFolder4`,
        Nullif(`test_execution_results`.`subfolder5`, '')
        AS `subFolder5`,
        Nullif(`test_execution_results`.`subfolder6`, '')
        AS `subFolder6`,
        Nullif(`test_execution_results`.`subfolder7`, '')
        AS `subFolder7`,
        Nullif(`test_execution_results`.`subfolder8`, '')
        AS `subFolder8`,
        Nullif(`test_execution_results`.`subfolder9`, '')
        AS `subFolder9`,
        Nullif(`test_execution_results`.`subfolder10`, '')
        AS `subFolder10`,
        `test_execution_results`.`createdon`
        AS `CreatedOn`,
        `test_execution_results`.`requirementid`
        AS `RequirementId`,
        `test_execution_results`.`reqaltid`
        AS `ReqAltId`,
        `test_execution_results`.`epiclink`
        AS `epicLink`,
        `test_execution_results`.`requirementname`
        AS `RequirementName`,
        `test_execution_results`.`environment`
        AS `Environment`,
        `test_execution_results`.`testcasealtid`
        AS `RestCaseAltId`,
        `test_execution_results`.`testcasename`
        AS `RestCaseName`,
        `test_execution_results`.`testcaseid`
        AS `TestCaseId`,
        `test_execution_results`.`automation`
        AS `Automation`,
        `test_execution_results`.`cycle`
        AS `Cycle`,
        `test_execution_results`.`teststatus`
        AS `tStatus`,
        CASE
          WHEN `test_execution_results`.`teststatus` LIKE '%fail%' THEN 'Fail'
          WHEN `test_execution_results`.`teststatus` LIKE '%pass%' THEN 'Pass'
          WHEN `test_execution_results`.`teststatus` LIKE '%incomp%' THEN
          'Incomplete'
          WHEN `test_execution_results`.`teststatus` LIKE '%blocked%' THEN
          'Blocked'
          WHEN `test_execution_results`.`teststatus` LIKE '%Not exec%' THEN
          'Not Executed'
          WHEN `test_execution_results`.`teststatus` LIKE '%N/A%' THEN 'N/A'
          ELSE 'Not Executed'
        end
        AS `testStatus`,
        `test_execution_results`.`executedon`
        AS `ExecutedOn`,
        `test_execution_results`.`tags`
        AS `Tags`,
        `test_execution_results`.`usergroup`
        AS `usergroup`,
        `test_execution_results`.`priority`
        AS `priority`,
        `test_execution_results`.`defects`
        AS `Defects`,
        `test_execution_results`.`executedby`
        AS `ExecutedBy`,
        `test_execution_results`.`createdby`
        AS `CreatedBy`,
        `test_execution_results`.`insertedon`
        AS `insertedOn`,
        `test_execution_results`.`subfolderlevel`
        AS `subFolderLevel`,
        `test_execution_results`.`updatedon`
        AS `updatedOn`,
        `test_execution_results`.`component`
        AS `component`
 FROM   (`test_execution_results`
         LEFT JOIN `project_milestones_status`
ON( Trim(
CONVERT(Substring_index(REPLACE(`test_execution_results`.`projectname`, '–', '-'), '-', 1) USING utf8)) = `project_milestones_status`.`projectreference`
AND Trim(Substr(`test_execution_results`.`projectname`,
                  Locate('-', `test_execution_results`.`projectname`)
                                                        + 1)) =
    `project_milestones_status`.`projectname`
AND CONVERT(Trim(`test_execution_results`.`releasename`) USING utf8) =
    `project_milestones_status`.`releasename` ))) 

/*Bussiness Domain Added*/

ALTER TABLE `test_execution_results`
	ADD COLUMN `businessDomain` INT NULL AFTER `usergroup`;

(SELECT `test_execution_results`.`id`
        AS
        `id`,
        Trim(
        Substring_index(
        REPLACE(`test_execution_results`.`projectname`, '–', '-'), '-', 1)) AS
             `ProjectReference`,
        Trim(Substr(`test_execution_results`.`projectname`,
                  Locate('-', `test_execution_results`.`projectname`)
                                                            + 1))
        AS `ProjectName`,
        `project_milestones_status`.`domain`
        AS `DomainName`,
        `test_execution_results`.`projectname`
        AS `Project`,
        `test_execution_results`.`releasename`
        AS `ReleaseName`,
        `test_execution_results`.`hierarchy`
        AS `Hierarchy`,
        Nullif(`test_execution_results`.`folder`, '')
        AS `Folder`,
        Nullif(`test_execution_results`.`subfolder`, '')
        AS `subFolder`,
        Nullif(`test_execution_results`.`subfolder1`, '')
        AS `subFolder1`,
        Nullif(`test_execution_results`.`subfolder2`, '')
        AS `subFolder2`,
        Nullif(`test_execution_results`.`subfolder3`, '')
        AS `subFolder3`,
        Nullif(`test_execution_results`.`subfolder4`, '')
        AS `subFolder4`,
        Nullif(`test_execution_results`.`subfolder5`, '')
        AS `subFolder5`,
        Nullif(`test_execution_results`.`subfolder6`, '')
        AS `subFolder6`,
        Nullif(`test_execution_results`.`subfolder7`, '')
        AS `subFolder7`,
        Nullif(`test_execution_results`.`subfolder8`, '')
        AS `subFolder8`,
        Nullif(`test_execution_results`.`subfolder9`, '')
        AS `subFolder9`,
        Nullif(`test_execution_results`.`subfolder10`, '')
        AS `subFolder10`,
        `test_execution_results`.`createdon`
        AS `CreatedOn`,
        `test_execution_results`.`requirementid`
        AS `RequirementId`,
        `test_execution_results`.`reqaltid`
        AS `ReqAltId`,
        `test_execution_results`.`epiclink`
        AS `epicLink`,
        `test_execution_results`.`requirementname`
        AS `RequirementName`,
        `test_execution_results`.`environment`
        AS `Environment`,
        `test_execution_results`.`testcasealtid`
        AS `RestCaseAltId`,
        `test_execution_results`.`testcasename`
        AS `RestCaseName`,
        `test_execution_results`.`testcaseid`
        AS `TestCaseId`,
        `test_execution_results`.`automation`
        AS `Automation`,
        `test_execution_results`.`cycle`
        AS `Cycle`,
        `test_execution_results`.`teststatus`
        AS `tStatus`,
        CASE
          WHEN `test_execution_results`.`teststatus` LIKE '%fail%' THEN 'Fail'
          WHEN `test_execution_results`.`teststatus` LIKE '%pass%' THEN 'Pass'
          WHEN `test_execution_results`.`teststatus` LIKE '%incomp%' THEN
          'Incomplete'
          WHEN `test_execution_results`.`teststatus` LIKE '%blocked%' THEN
          'Blocked'
          WHEN `test_execution_results`.`teststatus` LIKE '%Not exec%' THEN
          'Not Executed'
          WHEN `test_execution_results`.`teststatus` LIKE '%N/A%' THEN 'N/A'
          ELSE 'Not Executed'
        end
        AS `testStatus`,
        `test_execution_results`.`executedon`
        AS `ExecutedOn`,
        `test_execution_results`.`tags`
        AS `Tags`,
        `test_execution_results`.`usergroup`
        AS `usergroup`,
		 `test_execution_results`.`businessDomain`
        AS `businessDomain`,
        `test_execution_results`.`priority`
        AS `priority`,
        `test_execution_results`.`defects`
        AS `Defects`,
        `test_execution_results`.`executedby`
        AS `ExecutedBy`,
        `test_execution_results`.`createdby`
        AS `CreatedBy`,
        `test_execution_results`.`insertedon`
        AS `insertedOn`,
        `test_execution_results`.`subfolderlevel`
        AS `subFolderLevel`,
        `test_execution_results`.`updatedon`
        AS `updatedOn`,
        `test_execution_results`.`component`
        AS `component`
 FROM   (`test_execution_results`
         LEFT JOIN `project_milestones_status`
ON( Trim(
CONVERT(Substring_index(REPLACE(`test_execution_results`.`projectname`, '–', '-'), '-', 1) USING utf8)) = `project_milestones_status`.`projectreference`
AND Trim(Substr(`test_execution_results`.`projectname`,
                  Locate('-', `test_execution_results`.`projectname`)
                                                        + 1)) =
    `project_milestones_status`.`projectname`
AND CONVERT(Trim(`test_execution_results`.`releasename`) USING utf8) =
    `project_milestones_status`.`releasename` ))) 


(SELECT `d`.`userstoryid`                        AS `userStoryID`,
        `d`.`storykey`                           AS `storyKey`,
        `d`.`projectkey`                         AS `projectKey`,
        `d`.`projectname`                        AS `projectName`,
        CASE
          WHEN `d`.`status` LIKE '%Backlog%' THEN 'Backlog'
          WHEN `d`.`status` LIKE '%BA in progress%' THEN 'Backlog'
          WHEN `d`.`status` LIKE '%BUSINESS VALIDATED%' THEN 'Backlog'
          WHEN `d`.`status` LIKE '%PRD Ready%' THEN 'Closed'
          WHEN `d`.`status` LIKE '%Open%' THEN 'Open'
          WHEN `d`.`status` LIKE 'Reopened' THEN 'Open'
          WHEN `d`.`status` LIKE '%BA to validate%' THEN 'Analysis'
          WHEN `d`.`status` LIKE '%FA in progress%' THEN 'Analysis'
          WHEN `d`.`status` LIKE '%FA to validate%' THEN 'Analysis'
          WHEN `d`.`status` LIKE '%TA in progress%' THEN 'Analysis'
          WHEN `d`.`status` LIKE '%TA to validate%' THEN 'Analysis'
          WHEN `d`.`status` LIKE '%To groom%' THEN 'Analysis'
          WHEN `d`.`status` LIKE '%Sprintable%' THEN 'Analysis'
          WHEN `d`.`status` LIKE '%In Inception%' THEN 'Analysis'
          WHEN `d`.`status` LIKE 'Ready' THEN 'Analysis'
          WHEN `d`.`status` LIKE '%Next%' THEN 'Analysis'
          WHEN `d`.`status` LIKE '%In progress%' THEN 'Development'
          WHEN `d`.`status` LIKE 'In review' THEN 'Development'
          WHEN `d`.`status` LIKE 'To promote' THEN 'Development'
          WHEN `d`.`status` LIKE 'In test' THEN 'In Test'
          WHEN `d`.`status` LIKE '%To Verify%' THEN 'In Test'
          WHEN `d`.`status` LIKE '%To review%' THEN 'In Test'
          WHEN `d`.`status` LIKE '%UAT ON-GOING%' THEN 'In Test'
          WHEN `d`.`status` LIKE 'Ready for uat' THEN 'Closed'
          WHEN `d`.`status` LIKE '%Closed%' THEN 'Closed'
          WHEN `d`.`status` LIKE '%UAT ready%' THEN 'To Validate'
          WHEN `d`.`status` LIKE '%UAT READY%' THEN 'To Validate'
          WHEN `d`.`status` LIKE '%prd ready%' THEN 'Closed'
          WHEN `d`.`status` LIKE '%PRD READY%' THEN 'Closed'
          WHEN `d`.`status` LIKE '%PRD TO VERIFY%' THEN 'Closed'
          WHEN `d`.`status` LIKE '%Resolved%' THEN 'To Validate'
          WHEN `d`.`status` LIKE '%PO To validate%' THEN 'To Validate'
          WHEN `d`.`status` LIKE '%To Do%' THEN 'Backlog'
          WHEN `d`.`status` LIKE '%To Analyze (FA)%' THEN 'Backlog'
          WHEN `d`.`status` LIKE '%In Analysis%' THEN 'Analysis'
          WHEN `d`.`status` LIKE '%Done%' THEN 'Closed'
          WHEN `d`.`status` LIKE '%Closed%' THEN 'Closed'
          WHEN `d`.`status` LIKE '%To validate%' THEN 'To Validate'
        end                                      AS `storyStatus`,
        `d`.`summary`                            AS `summary`,
        Ifnull(`pm`.`domain`, 'Un-Defined')      AS `domainName`,
        `pm`.`projectreference`                  AS `ProjectReference`,
        Ifnull(`d`.`resolution`, 'Un-Defined')   AS `resolution`,
        Ifnull(`d`.`priority`, 'Un-Defined')     AS `priority`,
        Ifnull(`d`.`assigneename`, 'UnAssigned') AS `assigneeName`,
        Ifnull(`d`.`severity`, 'Un-Defined')     AS `severity`,
        Ifnull(`d`.`component`, 'No Components') AS `component`,
        Ifnull(`d`.`sprintid`, 'Un-Defined')     AS `sprintId`,
        Ifnull(`d`.`reporter`, 'UnAssigned')     AS `reporter`,
        Ifnull(`d`.`epiclink`, 'Un-Defined')     AS `epicLink`,
        Ifnull(`d`.`epicname`, 'No Epic Mapped') AS `epicName`,
        CASE
          WHEN `d`.`epicstatus` LIKE '%Closed%' THEN 'Closed'
          WHEN `d`.`epicstatus` LIKE '%BA In Progress%' THEN 'Analysis'
          WHEN `d`.`epicstatus` LIKE '%BA To Validate%' THEN 'Analysis'
          WHEN `d`.`epicstatus` LIKE '%Backlog%' THEN 'Backlog'
          WHEN `d`.`epicstatus` LIKE '%Open%' THEN 'Open'
          WHEN `d`.`epicstatus` LIKE '%Done%' THEN 'Closed'
          WHEN `d`.`epicstatus` LIKE '%In Review%' THEN 'Development'
          WHEN `d`.`epicstatus` LIKE '%Resolved%' THEN 'To Validate'
          WHEN `d`.`epicstatus` LIKE '%FA In Progress%' THEN 'Analysis'
          WHEN `d`.`epicstatus` LIKE '%Sprintable%' THEN 'Analysis'
          WHEN `d`.`epicstatus` LIKE '%Business Validated%' THEN 'Backlog'
          WHEN `d`.`epicstatus` LIKE '%Reopened%' THEN 'Open'
          WHEN `d`.`epicstatus` LIKE '%In Progress%' THEN 'Development'
          WHEN `d`.`epicstatus` LIKE '%To Groom%' THEN 'Analysis'
          WHEN `d`.`epicstatus` LIKE '%To Promote%' THEN 'Development'
          WHEN `d`.`epicstatus` LIKE '%In Test%' THEN 'In Test'
          WHEN `d`.`epicstatus` LIKE '%In Analysis%' THEN 'Analysis'
          WHEN `d`.`epicstatus` LIKE '%PO To Validate%' THEN 'To Validate'
          WHEN `d`.`epicstatus` LIKE '%TA In Progress%' THEN 'Analysis'
          WHEN `d`.`epicstatus` LIKE '%PRD Ready%' THEN 'Closed'
          WHEN `d`.`epicstatus` LIKE '%Next%' THEN 'Analysis'
          WHEN `d`.`epicstatus` LIKE '%UAT Ready%' THEN 'To Validate'
          WHEN `d`.`status` LIKE '%UAT READY%' THEN 'To Validate'
          WHEN `d`.`epicstatus` LIKE '%TA To Validate%' THEN 'Analysis'
          WHEN `d`.`epicstatus` LIKE '%UAT on-going%' THEN 'In Test'
          WHEN `d`.`epicstatus` LIKE '%PRD To Verify%' THEN 'Closed'
          WHEN `d`.`epicstatus` IS NULL THEN 'No Status Mapped'
          ELSE `d`.`epicstatus`
        end                                      AS `epicStatus`,
        Ifnull(`d`.`fixversions`, 'Un-Defined')  AS `fixVersions`,
        `pm`.`releasename`                       AS `ReleaseName`,
        Ifnull(`d`.`issuetype`, 'Un-Defined')    AS `issueType`,
        REPLACE(`pm`.`jira_key`, ' ', '')        AS `JIRA_KEY`
 FROM   (`iar`.`user_story_details` `d`
         JOIN `iar`.`v_project_wise_defect_keys` `pm`
           ON( `d`.`projectkey` = REPLACE(`pm`.`jira_key`, ' ', '')
               AND `d`.`fixversions` = `pm`.`releasename` )));


/*Backlog updated*/

SELECT `d`.`defectid`                                      AS `defectId`,
       `d`.`defectkey`                                     AS `defectKey`,
       `d`.`projectkey`                                    AS `projectKey`,
       `d`.`projectname`                                   AS `projectName`,
       `d`.`projectcategory`                               AS `projectCategory`,
       Ifnull(`pm`.`domain`, 'Un-Defined')                 AS `domainName`,
       `pm`.`projectreference`                             AS `projectReference`
       ,
       Ifnull(`d`.`resolution`, 'Un-Defined')              AS
       `resolution`,
       Ifnull(`d`.`defectsevirity`, 'Un-Defined')          AS `defectSevirity`,
       Ifnull(`d`.`product`, 'Un-Defined')                 AS `product`,
       CASE
         WHEN `d`.`testleveldetected` LIKE '%system%' THEN 'System Test'
         WHEN `d`.`testleveldetected` LIKE '%sprint%' THEN 'Sprint Test'
         WHEN `d`.`testleveldetected` LIKE '%IT%' THEN 'Integration Test'
         WHEN `d`.`testleveldetected` LIKE '%UAT%' THEN 'UAT'
         WHEN `d`.`testleveldetected` LIKE '%unit%' THEN 'Unit Test'
         ELSE 'Un-Defined'
       end                                                 AS
       `testLevelDetected`,
       CASE
         WHEN `d`.`testtypes` LIKE '%func%' THEN 'Functional'
         WHEN `d`.`testtypes` LIKE '%aut%' THEN 'Automation'
         WHEN `d`.`testtypes` LIKE '%per%' THEN 'Performance'
         WHEN `d`.`testtypes` LIKE '%sec%' THEN 'Security Test'
         WHEN `d`.`testtypes` LIKE '%uni%' THEN 'Unit Test'
         ELSE 'Un-Defined'
       end                                                 AS `testTypes`,
       CASE
         WHEN `d`.`status` LIKE '%To Validate%' THEN 'To Validate'
         WHEN `d`.`status` LIKE '%To Verify%' THEN 'In Test'
         WHEN `d`.`status` LIKE '%To Groom%' THEN 'Analysis'
         WHEN `d`.`status` LIKE '%In Test%' THEN 'In Test'
         WHEN `d`.`status` LIKE '%UAT on-going%' THEN 'In Test'
         WHEN `d`.`status` LIKE '%Ready%' THEN 'To Validate'
         WHEN `d`.`status` LIKE '%UAT Ready%' THEN 'To Validate'
         WHEN `d`.`status` LIKE '%open%' THEN 'Open'
         WHEN `d`.`status` LIKE '%sprintable%' THEN 'Analysis'
         WHEN `d`.`status` LIKE '%progress%' THEN 'Development'
         WHEN `d`.`status` LIKE '%Analysis%' THEN 'Analysis'
         WHEN `d`.`status` LIKE '%Business Validated%' THEN 'BackLog'
         WHEN `d`.`status` LIKE '%review%' THEN 'Development'
         WHEN `d`.`status` LIKE '%incep%' THEN 'Analysis'
         WHEN `d`.`status` LIKE '%promot%' THEN 'Development'
         WHEN `d`.`status` LIKE 'open' THEN 'Open'
         WHEN `d`.`status` LIKE 'Backlog' THEN 'BackLog'
         WHEN `d`.`status` LIKE 'Next' THEN 'Analysis'
         WHEN `d`.`status` LIKE '%resol%' THEN 'To Validate'
         WHEN `d`.`status` LIKE '%Resolved%' THEN 'To Validate'
         WHEN `d`.`status` LIKE '%done%' THEN 'Closed'
         WHEN `d`.`status` LIKE '%reopened%' THEN 'Open'
         WHEN `d`.`status` LIKE '%closed%' THEN 'Closed'
         WHEN `d`.`status` LIKE '%prd ready%' THEN 'Closed'
         WHEN `d`.`status` LIKE '%In Analysis%' THEN 'Analysis'
         WHEN `d`.`status` LIKE '%In Progress%' THEN 'Development'
         WHEN `d`.`status` LIKE '%In Review%' THEN 'In Test'
         WHEN `d`.`status` LIKE '%PRD To Verify%' THEN 'Closed'
         WHEN `d`.`status` LIKE '%Closed%' THEN 'Closed'
         WHEN `d`.`status` LIKE '%Resolved%' THEN 'To validate'
         WHEN `d`.`status` LIKE '%To Promote%' THEN 'Development'
         ELSE `d`.`status`
       end                                                 AS `status`,
       `d`.`createdon`                                     AS `createdOn`,
       `d`.`summary`                                       AS `summary`,
       Ifnull(`d`.`createdusername`, 'UnAssigned')         AS `createdUserName`,
       CASE
         WHEN `d`.`priority` LIKE '%Major%' THEN 'Major'
         WHEN `d`.`priority` LIKE '%Highest%' THEN 'Major'
         WHEN `d`.`priority` LIKE '%High%' THEN 'Major'
         WHEN `d`.`priority` LIKE '%Blocker%' THEN 'Blocker'
         WHEN `d`.`priority` LIKE '%Critical%' THEN 'Critical'
         WHEN `d`.`priority` LIKE '%Minor%' THEN 'Minor'
         WHEN `d`.`priority` LIKE '%Low%' THEN 'Minor'
         WHEN `d`.`priority` LIKE '%Lowest%' THEN 'Minor'
         WHEN `d`.`priority` LIKE '%Trivial%' THEN 'Minor'
         WHEN `d`.`priority` LIKE '%Medium%' THEN 'Medium'
         WHEN `d`.`priority` IS NULL THEN 'Un-Defined'
         ELSE `d`.`priority`
       end                                                 AS `priority`,
       Ifnull(`d`.`assigneename`, 'UnAssigned')            AS `assigneeName`,
       Ifnull(`d`.`assigneeid`, 'UnAssigned')              AS `assigneeID`,
       Ifnull(`d`.`severity`, 'Un-Defined')                AS `severity`,
       Ifnull(`d`.`component`, 'No Components')            AS `component`,
       Ifnull(`d`.`sprintid`, 'Un-Defined')                AS `sprintId`,
       Ifnull(`d`.`reporter`, 'UnAssigned')                AS `reporter`,
       Ifnull(`d`.`progress`, 'Un-Defined')                AS `progress`,
       Ifnull(`d`.`stereotype`, 'Un-Defined')              AS `stereoType`,
       Ifnull(`d`.`epiclink`, 'Un-Defined')                AS `epicLink`,
       Ifnull(`d`.`epicname`, 'No Epic Mapped')            AS `epicName`,
       CASE
         WHEN `d`.`epicstatus` LIKE '%Closed%' THEN 'Closed'
         WHEN `d`.`epicstatus` LIKE '%BA In Progress%' THEN 'Analysis'
         WHEN `d`.`epicstatus` LIKE '%BA To Validate%' THEN 'Analysis'
         WHEN `d`.`epicstatus` LIKE '%Backlog%' THEN 'BackLog'
         WHEN `d`.`epicstatus` LIKE '%Open%' THEN 'Open'
         WHEN `d`.`epicstatus` LIKE '%Done%' THEN 'Closed'
         WHEN `d`.`epicstatus` LIKE '%In Review%' THEN 'Development'
         WHEN `d`.`epicstatus` LIKE '%Resolved%' THEN 'To Validate'
         WHEN `d`.`epicstatus` LIKE '%FA In Progress%' THEN 'Analysis'
         WHEN `d`.`epicstatus` LIKE '%Sprintable%' THEN 'Analysis'
         WHEN `d`.`epicstatus` LIKE '%Business Validated%' THEN 'BackLog'
         WHEN `d`.`epicstatus` LIKE '%Reopened%' THEN 'Open'
         WHEN `d`.`epicstatus` LIKE '%In Progress%' THEN 'Development'
         WHEN `d`.`epicstatus` LIKE '%To Groom%' THEN 'Analysis'
         WHEN `d`.`epicstatus` LIKE '%To Promote%' THEN 'Development'
         WHEN `d`.`epicstatus` LIKE '%In Test%' THEN 'In Test'
         WHEN `d`.`epicstatus` LIKE '%In Analysis%' THEN 'Analysis'
         WHEN `d`.`epicstatus` LIKE '%PO To Validate%' THEN 'To Validate'
         WHEN `d`.`epicstatus` LIKE '%TA In Progress%' THEN 'Analysis'
         WHEN `d`.`epicstatus` LIKE '%PRD Ready%' THEN 'Closed'
         WHEN `d`.`epicstatus` LIKE '%Next%' THEN 'Analysis'
         WHEN `d`.`epicstatus` LIKE '%UAT Ready%' THEN 'To Validate'
         WHEN `d`.`epicstatus` LIKE '%TA To Validate%' THEN 'Analysis'
         WHEN `d`.`epicstatus` LIKE '%UAT on-going%' THEN 'In Test'
         WHEN `d`.`epicstatus` LIKE '%PRD To Verify%' THEN 'Closed'
         WHEN `d`.`epicstatus` IS NULL THEN 'No Status Mapped'
         ELSE `d`.`epicstatus`
       end                                                 AS `epicStatus`,
       Ifnull(`d`.`fixversions`, 'Un-Defined')             AS `fixVersions`,
       `pm`.`releasename`                                  AS `ReleaseName`,
       Ifnull(`d`.`issuetype`, 'Un-Defined')               AS `issueType`,
       Ifnull(`d`.`parentkey`, 'Un-Defined')               AS `parentKey`,
       Ifnull(`d`.`storydefectidfixversion`, 'Un-Defined') AS
       `storyDefectIdFixVersion`,
       REPLACE(`pm`.`jira_key`, ' ', '')                   AS `JIRA_KEY`
FROM   (`iar_byld_1`.`defect_details` `d`
        JOIN `iar_byld_1`.`v_project_wise_defect_keys` `pm`
          ON( `d`.`projectkey` = REPLACE(`pm`.`jira_key`, ' ', '')
              AND `d`.`fixversions` = `pm`.`releasename` ))
WHERE  `d`.`fixversions` <> 'Un-Defined' 