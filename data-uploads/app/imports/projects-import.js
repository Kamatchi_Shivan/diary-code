var httpntlm = require('httpntlm');
var spauth = require('node-sp-auth');
var requestprom = require('request-promise');
var moment = require('moment');
const DATA_MODEL = require("../models/dairy-data.model");
const XML = require("../contants/ccb-project-fields");
const USR = require("../contants/sp-users")
const DATA_UTILS = require("../utils/data-utils");
const config = require("../configs/sharepoint")
const CONS = require("../contants/commons")
const API = require("../contants/db-uploads-api");
const sql = require("../models/db");

const { getDateTime, currentTime, getDateTimeFrmNumber } = require("../utils/date-utils");
var usersData = new Map();
/*
const log4js = require("log4js");

log4js.configure({
    appenders: { dataUploads: { type: "file", filename: "data-uploads.log" } },
    categories: { default: { appenders: ["dataUploads"], level: "all" } },
});

const //logger = log4js.get//logger("dataUploads");
*/


module.exports.UploadCCBProjects = function (runType) {
    return new Promise((resolve, reject) => {
        try {
            let lastRecordIdB4Insertion = null;
            var startDate = moment(new Date(), 'YYYY-M-DD HH:mm:ss')
            DATA_MODEL.CCBProjects.getLstRecordCountIdProjectMilestones().then(response => {
                lastRecordIdB4Insertion = response;
                spauth.getAuth(config.authURL, {
                    clientId: config.clientId,
                    clientSecret: config.clientSecret
                }).then(function (options) {
                    // Access Token will be available on the options.headers variable
                    var headers = options.headers;
                    headers['Accept'] = 'application/json;odata=verbose';
                    // Pull the SharePoint list items  
                    requestprom.get({
                        url: config.getUsers_URL,
                        headers: headers,
                        json: true
                    }).then(function (listresponse) {
                        var result = listresponse;
                        for (key in result) {
                            let data = result[key]
                            let entry = data["results"]
                            for (i in entry) {
                                var key = entry[i][USR.Id];
                                var record = {
                                    LoginName: entry[i][USR.LoginName],
                                    Title: entry[i][USR.Title],
                                    Email: entry[i][USR.Email],
                                }
                                usersData.set(key, record);
                            }
                            // console.log("Printing User data >>>>",usersData);

                        }
                        requestprom.get({
                            url: config.projectsCCB_URL,
                            headers: headers,
                            json: true
                        }).then(function (projectResponse) {
                            var result = projectResponse;
                            for (key in result) {
                                let data = result[key];
                                let entry = data["results"];
                                if (entry !== undefined) {
                                    ReleaseLookup().then(activeReleases => {
                                        InsertProjectEntries(entry, activeReleases).then(response => {
                                            console.log(currentTime(), " >> ", response)
                                            DATA_MODEL.CCBProjects.getUnaffectedRecordsProjects(lastRecordIdB4Insertion).then(results => {
                                                console.log(currentTime(), " >> Projects Records Deleted >>  ", results);
                                                //logger.info(CONS.projects, " >> Projects Records Deleted >>  ", results);

                                                DATA_MODEL.DataUploads.updateExecution(runType, API.project).then(updateResponse => {
                                                    DATA_MODEL.DataUploads.getAllData().then(data => {
                                                        return resolve([response, data]);
                                                    });

                                                });

                                            })

                                        })

                                    })

                                }
                            }

                        }).catch(function (err) {
                            console.log(currentTime(), " >>  UploadCCBProjects", err)
                        });

                    }).catch(function (err) {
                        console.log(currentTime(), " >>  UploadCCBProjects", err)
                    });
                });

            }).catch(error => {
                //logger.error(CONS.projects, " >> Error UploadCCBProjects ", error);
                console.log(currentTime(), " >>  UploadCCBProjects", error)
                reject("UploadCCBProjects", error)
            });

        } catch (error) {
            //logger.error(CONS.projects, " >> Error UploadCCBProjects ", error);
        }
    }).catch(error => {
        //logger.error(error)
        return new Error(error);
    })
}


function InsertProjectEntries(entry, activeReleases) {
    return new Promise((resolve, reject) => {

        let insertCounter = 0, updateCounter = 0;
        var startDate = moment(new Date(), 'YYYY-M-DD HH:mm:ss')
        let i = 0;

        let counter = 0;
        for (i in entry) {

            let repoLink = null;
            let content = entry[i];
            if (format(content[XML.PROJECTREPOSITORY_Link])) {
                repoLink = format(content[XML.PROJECTREPOSITORY_Link])["Description"];
            }

            let zephyrLink = null;
            if (format(content[XML.ZEPHYR_Link])) {
                zephyrLink = format(content[XML.ZEPHYR_Link])["Url"];
            }
            var releaseId = format(content[XML.releaseId]);
            let ReleaseName = format(content[XML.ReleaseName]);

            //Apply filter logic get unquie release and update it
            if (releaseId !== null) {
                let ReleaseLookupValue = activeReleases.filter((col) => {
                    return col.ReleaseId === releaseId;
                });
                if (ReleaseLookupValue.length !== 0) {
                    ReleaseName = ReleaseLookupValue[0].ReleaseName;
                }
            }
            if(ReleaseName === null){
                ReleaseName = "No Release Mapped"
                console.log("Release Name and Project Name",ReleaseName,format(content[XML.ProjectName]))
            }
       
            var dataList = {
                Domain: format(content[XML.Domain]),
                PROJECTREPOSITORY_Link: repoLink,
                Change_Type: format(content[XML.Change_Type]),
                Project_Manager: getUserName(content[XML.Project_ManagerId]),
                Project_ManagerId: format(content[XML.Project_ManagerId]),
                Project_Manager_email: getUserEmail(content[XML.Project_ManagerId]),
                Delivery_Master_Lead: getUserName(content[XML.Delivery_Master_LeadId]),
                Delivery_Master_Lead_email: getUserEmail(content[XML.Delivery_Master_LeadId]),
                Delivery_Master_LeadId: format(content[XML.Delivery_Master_LeadId]),
                Test_Coordinator: getUserName(content[XML.Test_CoordinatorId]),
                Test_Coordinator_email: getUserEmail(content[XML.Test_CoordinatorId]),
                Test_CoordinatorId: format(content[XML.Test_CoordinatorId]),
                ProjectSize_MDs: format(content[XML.ProjectSize_MDs]),
                CCB_Date: format(getDateTimeFrmNumber(content[XML.CCB_Date])),
                CCB_Phase: format(content[XML.CCB_Phase]),
                Additional_Info_Dependencies: format(content[XML.Additional_Info_Dependencies]),
                CCB_Ready4SPRINT_Date: format(getDateTimeFrmNumber(content[XML.CCB_Ready4SPRINT_Date])),
                CCB_Ready4SPRINT_Decision: format(content[XML.CCB_Ready4SPRINT_Decision]),
                CCBReady4SPRINT_Remarks: format(content[XML.CCBReady4SPRINT_Remarks]),
                CCBReady4SPRINT_Actions: format(content[XML.CCBReady4SPRINT_Actions]),
                CCB_Ready4UAT_Date: format(getDateTimeFrmNumber(content[XML.CCB_Ready4UAT_Date])),
                CCB_Ready4UAT_Decision: format(content[XML.CCB_Ready4UAT_Decision]),
                CCBReady4UAT_Remarks: format(content[XML.CCBReady4UAT_Remarks]),
                CCBReady4UAT_Actions: format(content[XML.CCBReady4UAT_Actions]),
                CCB_Ready4PROD_Date: format(getDateTimeFrmNumber(content[XML.CCB_Ready4PROD_Date])),
                CCB_Ready4PROD_Decision: format(content[XML.CCB_Ready4PROD_Decision]),
                CCBReady4PROD_Remarks: format(content[XML.CCBReady4PROD_Remarks]),
                CCBReady4PROD_Actions: format(content[XML.CCBReady4PROD_Actions]),
                DEL_SPR_PID_Final: format(content[XML.DEL_SPR_PID_Final]),
                DEL_SPR_Master_Test_Plan: format(content[XML.DEL_SPR_Master_Test_Plan]),
                SPRINT_Test_Report_Status: format(content[XML.SPRINT_Test_Report_Status]),
                UAT_Test_Report_Status: format(content[XML.UAT_Test_Report_Status]),
                Content_Type: format(content[XML.Content_Type]),
                Implementation_Plan_Status: format(content[XML.Implementation_Plan_Status]),
                CCB_Phase: format(content[XML.CCB_Phase]),
                ReleaseName: ReleaseName,
                Project_Name_Dup: format(content[XML.ProjectNameDup]),
                UAT_Start_Date: format(getDateTimeFrmNumber(content[XML.UAT_Start_Date])),
                Scrum_Master: getUserName(content[XML.Scrum_MasterId]),
                Scrum_Master_email: getUserEmail(content[XML.Scrum_MasterId]),
                Scrum_MasterId: format(content[XML.Scrum_MasterId]),
                JIRA_Link: format(content[XML.JIRA_Link]),
                UAT_Start_Date_Exception: format(getDateTime(content[XML.UAT_Start_Date_Exception])),
                CCB_STARTUP_Date: format(getDateTimeFrmNumber(content[XML.CCB_STARTUP_Date])),
                CCB_STARTUP_Remarks: format(content[XML.CCB_STARTUP_Remarks]),
                CCB_STARTUP_Actions: format(content[XML.CCB_STARTUP_Actions]),
                ZEPHYR_Link: zephyrLink,
                WRF_UPDATES: format(content[XML.WRF_UPDATES]),
                Global_Comment: format(content[XML.Global_Comment]),
                Product_OwnerID: format(content[XML.Product_OwnerID]),
                CCB_OnTime_Registration: format(content[XML.CCB_OnTime_Registration]),
                CCB_OnTime_UAT_start: format(content[XML.CCB_OnTime_UAT_start]),
                CCB_Closure_Date: format(getDateTimeFrmNumber(content[XML.CCB_Closure_Date])),
                CCB_CLOSURE_Decision: format(content[XML.CCB_CLOSURE_Decision]),
                CCB_CLOSURE_Remarks: format(content[XML.CCB_CLOSURE_Remarks]),
                CCB_CLOSURE_Actions: format(content[XML.CCB_CLOSURE_Actions]),
                DEL_STA_Project_BoardSteerco_GO: format(content[XML.DEL_STA_Project_BoardSteerco_GO]),
                Ready2StartTestRemarks: format(content[XML.Ready2StartTestRemarks]),
                DEL_STA_HighLevelPlanning: format(content[XML.DEL_STA_HighLevelPlanning]),
                DEL_STA_WarrantyIncluded: format(content[XML.DEL_STA_WarrantyIncluded]),
                DEL_STA_HighLevelRequirementsScope: format(content[XML.DEL_STA_HighLevelRequirementsScope]),
                DEL_STA_PARP_Approved: format(content[XML.DEL_STA_PARP_Approved]),
                DEL_STA_SA_Requested: format(content[XML.DEL_STA_SA_Requested]),
                DEL_STA_BA_Requested: format(content[XML.DEL_STA_BA_Requested]),
                DEL_STAStaffingPlanviewBooked: format(content[XML.DEL_STAStaffingPlanviewBooked]),
                DEL_STA_BudgetApproved: format(content[XML.DEL_STA_BudgetApproved]),
                DEL_STA_PIDStage1_TODO: format(content[XML.DELSTA_PIDStage1_TODO]),
                DEL_STA_PIDStage1: format(content[XML.DEL_STA_PIDStage1]),
                Ready4SprintTestRemarks: format(content[XML.Ready4SprintTestRemarks]),
                Ready4UATTestRemarks: format(content[XML.Ready4UATTestRemarks]),
                Ready4ProdTestRemarks: format(content[XML.Ready4ProdTestRemarks]),
                Technical_Release: format(content[XML.Technical_Release]),
                CCBDashb: format(content[XML.CCBDashb]),
                CCB_STARTUP_Date: format(getDateTimeFrmNumber(content[XML.CCB_STARTUP_Date])),
                Test_Status: format(content[XML.Test_Status]),
                DomainExpert: format(content[XML.DomainExpert]),
                Jira_defect_projects: format(content[XML.Jira_defect_projects]),
                MTP: format(content[XML.MTP]),
                DetailedTestStatus: format(content[XML.DetailedTestStatus]),
                SP_ID: format(content[XML.Id]),
                Open_Risks_Issues: format(content[XML.Open_Risks_Issues]),

            }

            var keyList = {
                ProjectName: format(content[XML.ProjectName]),
                ProjectReference: format(content[XML.ProjectReference]),
                ReleaseName: ReleaseName,

            }

            let insertPost = Object.assign(keyList, dataList);
            let updatePost = Object.assign(dataList);
            ReleaseName = ReleaseName;
            ProjectReference = format(content[XML.ProjectReference]);
            ProjectName = format(content[XML.ProjectName]);
            var testKeyMessage = {
                KeyRemarksTesting_Plain: format(content[XML.KeyRemarksTesting]),
                KeyRemarksTesting: formatKeyMessage(content[XML.KeyRemarksTesting])

            };
            let updateClause = "where ReleaseName='" + ReleaseName + "' AND ProjectReference='" + ProjectReference + "' AND ProjectName='" + ProjectName + "'";
            DATA_MODEL.CCBProjects.insertPrep(insertPost, updatePost, updateClause, ReleaseName, ProjectName, testKeyMessage).then(fullfilled => {
                counter++;
                if (fullfilled.includes("Update")) {
                    updateCounter++;
                } else {
                    insertCounter++;
                }
                if (counter === (entry.length)) {

                    DATA_UTILS.totalTimeTaken(startDate).then(response => {
                        //logger.info(CONS.projects, " >> ", ` Total ${counter} records, inserted >> ${insertCounter} updated >>${updateCounter}`)

                        console.log(currentTime(), " >> ", ` Total ${counter} records, inserted >> ${insertCounter} updated >>${updateCounter}`)
                        return resolve(` Total ${counter} records, inserted >> ${insertCounter} updated >> ${updateCounter} , Total Time Taken ${response}`)
                    });
                }

            })

            // });



        }


    });
}


function format(str) {
    let string = DATA_UTILS.projectformatString(str)
    return string;
}

function formatKeyMessage(str) {
    let string = DATA_UTILS.formatString(str)
    return string;
}

function getUserName(str) {
    let name = null, data = usersData.get(str);
    if (data !== undefined) {
        name = data["Title"]
    }
    return name;
}


function getUserEmail(str) {
    let email = null, data = usersData.get(str);
    if (data !== undefined) {
        email = data["Email"]
    }
    return email;
}


const ReleaseLookup = () => {
    return new Promise((resolve, reject) => {
        let qry = `select  *  from release_calender`;
        sql.query(qry, (err, res) => {
            if (err) {
                reject(err.message)
            } else {
                let releaseName = JSON.parse(JSON.stringify(res));
                return resolve(releaseName);

            }
        });
    }).catch(error => {
        return null;
    });
}
