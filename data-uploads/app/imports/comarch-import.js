var httpntlm = require('httpntlm');
var spauth = require('node-sp-auth');
var requestprom = require('request-promise');
var moment = require('moment');
const DATA_MODEL = require("../models/dairy-data.model");
const XML = require("../contants/ccb-comarch-fields");
const USR = require("../contants/sp-users")
const DATA_UTILS = require("../utils/data-utils");
const config = require("../configs/sharepoint")
const CONS = require("../contants/commons")
const API = require("../contants/db-uploads-api");
const sql = require("../models/db");

const { getDateTime, currentTime, getDateTimeFrmNumber } = require("../utils/date-utils");
var usersData = new Map();


module.exports.UploadCCBMappingTable = function () {
    return new Promise((resolve, reject) => {
        try {
            let lastRecordIdB4Insertion = null;
            var startDate = moment(new Date(), 'YYYY-M-DD HH:mm:ss')
            DATA_MODEL.ComarchMapping.getLstRecordCountIdComarchMappingTable().then(response => {
                lastRecordIdB4Insertion = response;
                spauth.getAuth(config.authURL, {
                    clientId: config.clientId,
                    clientSecret: config.clientSecret
                }).then(function (options) {
                    // Access Token will be available on the options.headers variable
                    var headers = options.headers;
                    headers['Accept'] = 'application/json;odata=verbose';
                    // Pull the SharePoint list items  
                    requestprom.get({
                        url: config.comarch_mapping,
                        headers: headers,
                        json: true
                    }).then(function (projectResponse) {
                        var result = projectResponse;
                        for (key in result) {
                            let data = result[key];
                            let entry = data["results"];
                            if (entry !== undefined) {
                                ReleaseLookup().then(activeReleases => {
                                    ProjectLookup().then(activeProjects => {
                                        InsertProjectEntries(entry, activeReleases, activeProjects).then(response => {
                                            console.log(currentTime(), " >> ", response)
                                            DATA_MODEL.ComarchMapping.getUnaffectedRecordsMappingTable(lastRecordIdB4Insertion).then(results => {
                                                console.log(currentTime(), " >> Projects Records Deleted >>  ", results);
                                                return resolve("Success");
                                            }).catch(function (err) {
                                                console.log(currentTime(), " >>  UploadComarchMappingTable", err)
                                                return new Error(error);
                                            });

                                        }).catch(function (err) {
                                            console.log(currentTime(), " >>  UploadComarchMappingTable", err)
                                            return new Error(error);
                                        });

                                    }).catch(function (err) {
                                        console.log(currentTime(), " >>  UploadComarchMappingTable", err)
                                        return new Error(error);
                                    });


                                }).catch(function (err) {
                                    console.log(currentTime(), " >>  UploadComarchMappingTable", err)
                                    return new Error(error);
                                });

                            }
                        }

                    }).catch(function (err) {
                        console.log(currentTime(), " >>  UploadComarchMappingTable", err)
                        return new Error(error);
                    });
                });
            }).catch(error => {
                console.log(currentTime(), " >>  UploadComarchMappingTable", error)
                reject("UploadComarchMappingTable", error)
            });

        } catch (error) {
            return new Error(error);
        }
    }).catch(error => {
        return new Error(error);
    })
}


function InsertProjectEntries(entry, activeReleases, activeProjects) {
    return new Promise((resolve, reject) => {

        let insertCounter = 0, updateCounter = 0;
        var startDate = moment(new Date(), 'YYYY-M-DD HH:mm:ss')
        let i = 0;

        let counter = 0;
        for (i in entry) {
            let content = entry[i];
            var releaseId = format(content[XML.mappedReleaseName]);
            var projectId = format(content[XML.mappedProject]);
            let ReleaseName = "";
            let projectName = "";
            let projectReference = "";

            //Apply filter logic get unquie release and update it
            if (releaseId !== null) {
                let ReleaseLookupValue = activeReleases.filter((col) => {
                    return col.ReleaseId === releaseId;
                });
                if (ReleaseLookupValue.length !== 0) {
                    ReleaseName = ReleaseLookupValue[0].ReleaseName;
                }
            }

            //Apply filter logic get unquie project and update it
            if (projectId !== null) {
                let ProjectLookupValue = activeProjects.filter((col) => {
                    return col.SP_ID === projectId.toString();
                });
                if (ProjectLookupValue.length !== 0) {
                    projectName = ProjectLookupValue[0].ProjectName;
                    projectReference = ProjectLookupValue[0].ProjectReference;
                }
            }
            let Comarch_ID = format(content[XML.Comarch_ID]);
            var dataList = {
                Comarch_ID: format(content[XML.Comarch_ID]),
                ProjectName: format(content[XML.ProjectName]),
                mappedProject: projectName,
                mappedProjectReference: projectReference,
                ReleaseName: format(content[XML.ReleaseName]),
                mappedReleaseName: ReleaseName,

            }
            let insertPost = Object.assign(dataList);
            let updatePost = Object.assign(dataList);
            DATA_MODEL.ComarchMapping.insertPrep(insertPost, updatePost, Comarch_ID).then(fullfilled => {
                counter++;
                if (fullfilled.includes("Update")) {
                    updateCounter++;
                } else {
                    insertCounter++;
                }
                if (counter === (entry.length)) {

                    DATA_UTILS.totalTimeTaken(startDate).then(response => {
                        console.log(currentTime(), " >> ", ` Total ${counter} records, inserted >> ${insertCounter} updated >>${updateCounter}`)
                        return resolve(` Total ${counter} records, inserted >> ${insertCounter} updated >> ${updateCounter} , Total Time Taken ${response}`)
                    });
                }

            })

        }


    });
}


function format(str) {
    let string = DATA_UTILS.projectformatString(str)
    return string;
}

const ReleaseLookup = () => {
    return new Promise((resolve, reject) => {
        let qry = `select  *  from release_calender`;
        sql.query(qry, (err, res) => {
            if (err) {
                reject(err.message)
            } else {
                let releaseName = JSON.parse(JSON.stringify(res));
                return resolve(releaseName);

            }
        });
    }).catch(error => {
        return null;
    });
}


const ProjectLookup = () => {
    return new Promise((resolve, reject) => {
        let qry = `select  SP_ID,ReleaseName,ProjectReference,ProjectName  from project_milestones_status`;
        sql.query(qry, (err, res) => {
            if (err) {
                reject(err.message)
            } else {
                let releaseName = JSON.parse(JSON.stringify(res));
                return resolve(releaseName);

            }
        });
    }).catch(error => {
        return null;
    });
}