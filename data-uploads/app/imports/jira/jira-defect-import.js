const JIRA = require("../../contants/jira-custom-fields");

const GET = require("../../utils/jira-details")

const sql = require("../../models/db");

const { totalTimeTaken } = require("../../utils/data-utils")
const { currentTime } = require("../../utils/date-utils")
const API = require("../../contants/db-uploads-api");

var projectNameList = [];
var releaseNameList = [];

const fetch = require("node-fetch");

const config = require("../../configs/jira.config")

const baseUrl = config.baseURL;
const epicUrl = config.epicURL;

const base64 = require('base-64');
const DATA_MODELS = require("../../models/dairy-data.model")
const CONS = require("../../contants/commons")
/*
const log4js = require("log4js");


log4js.configure({
    appenders: { dataUploads: { type: "file", filename: "data-uploads.log" } },
    categories: { default: { appenders: ["dataUploads"], level: "all" } },
});

const logger = log4js.get//logger("dataUploads");
*/
async function validateCredentials() {
    let path = baseUrl + "/myself";
    return new Promise((resolve, reject) => {
        fetch(path, {
            method: 'GET',
            credentials: 'same-origin',
            redirect: 'follow',
            headers: {
                'Authorization': 'Basic ' + base64.encode(config.auth.username + ":" + config.auth.password),
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        }).then(response => {
            if (response.ok) {
                return resolve(response.json());
            } else {
                console.log(" erro")
                reject((CONS.jira, " >> ", response.status + "  ", response.statusText, " Invalid Credentials"));
            }
        }).catch(error => {
            return error;
        })
    })
}

async function getRequest(path) {
    return await fetch(path, {
        method: 'GET',
        credentials: 'same-origin',
        redirect: 'follow',
        headers: {
            'Authorization': 'Basic ' + base64.encode(config.auth.username + ":" + config.auth.password),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    }).then(response => {
        if (response.ok) {
            return response.json();
        } else {
            return "error  " + response.status + response.statusText
        }
    }).catch(error => {
        return error
    })
}



async function getRequestWithError(path) {
    return await fetch(path, {
        method: 'GET',
        credentials: 'same-origin',
        redirect: 'follow',
        headers: {
            'Authorization': 'Basic ' + base64.encode(config.auth.username + ":" + config.auth.password),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    }).then(response => {
        return response.json();
    }).catch(error => {
        return error
    })
}

async function getJiraProjectsService(releaseName, count = 0) {
    let path = baseUrl + "project"
    return getRequest(path);
}

async function getJiraEpicSummary(epicLink) {
    // let path = epicUrl + epicLink+"/issue";
    let path = baseUrl + `search?jql= key = ${epicLink}`;
    return getRequest(path);
}

async function getEpicLink(key) {
    let path = baseUrl + `search?jql= key = ${key}`;
    return getRequest(path);
}

async function getTotalDefectsByProjectsService(jqlProjectsNameIn, releaseName, count) {
    let defects = null;
    if (releaseName === "upcomingRelease")
        defects = `search?jql=project in (${jqlProjectsNameIn.toString()}) AND issuetype in (Defect,"Story Defect") AND created > startOfMonth(-12) ORDER BY created DESC `
    else
        defects = `search?jql=project in (${jqlProjectsNameIn.toString()}) AND issuetype in (Defect,"Story Defect")AND fixVersion=${releaseName} `
    console.log(currentTime(), " >> ", defects)
    let path = baseUrl + defects
    let result = await getRequest(path)
    result = (result["total"])
    return result;

}

async function getDefectsByProjectsService(jqlProjectsNameIn, releaseName, startsAt, count = 0) {
    let defects = null;
    if (releaseName === "upcomingRelease")
        defects = `search?jql=project in (${jqlProjectsNameIn.toString()}) AND issuetype in (Defect,"Story Defect") AND created > startOfMonth(-12) ORDER BY created DESC &startAt=${startsAt}`
    else
        defects = `search?jql=project in (${jqlProjectsNameIn.toString()}) AND issuetype in (Defect,"Story Defect") AND fixVersion=${releaseName} AND status != Closed&startAt=${startsAt}`
    let path = baseUrl + defects
    return getRequest(path);
}

async function getTotalStoryDefectsService(jqlStoryKeyIn, releaseName, count = 0) {
    let story = `search?jql=key in (${jqlStoryKeyIn.toString()})`; // AND fixVersion=${releaseName}`
    let path = baseUrl + story
    let result = await getRequest(path)
    result = (result["total"])
    return result;
}

async function getStoryDefectsService(jqlStoryKeyIn, releaseName, startsAt, count = 0) {
    let story = `search?jql=key in (${jqlStoryKeyIn.toString()})&startAt=${startsAt}`
    let path = baseUrl + story
    return getRequest(path);
}

async function getTotalUserStoryByProjectsService(jqlProjectsNameIn, releaseName) {
    let userStories = `search?jql=project in (${jqlProjectsNameIn.toString()}) AND issuetype in ("Story") AND (fixVersion in (${ReleaseFormatter(releaseName)}) OR fixVersion is EMPTY)  ORDER BY created DESC `
    let path = baseUrl + userStories
    let result = await getRequest(path)
    let count = (result["total"])
    console.log("Totoal User stories  >>>>  ", count)
    return count;
}

async function getUserStortByProjectsService(jqlProjectsNameIn, releaseName, startsAt, count = 0) {
    let userStories = `search?jql=project in (${jqlProjectsNameIn.toString()}) AND issuetype in ("Story") AND  (fixVersion in (${ReleaseFormatter(releaseName)}) OR fixVersion is EMPTY ) ORDER BY created DESC&startAt=${startsAt} `
    let path = baseUrl + userStories
    return getRequest(path);
}

async function getComponentByProjectsService(projectKey) {
    let components = `project/${projectKey}/components`
    let path = baseUrl + components
    return getRequest(path)

}

function ReleaseFormatter(ReleaseList){
    let formattedReleaseList = [];
    if(ReleaseList.length !== 0){
        ReleaseList.forEach(release => {
            formattedReleaseList = formattedReleaseList.concat(`'${release}'`)

        });

    }
return formattedReleaseList;
    
}


async function validateReleaseNameService(fixVersion) {
    let releaseNameServ = `search?jql=(fixVersion in ('${fixVersion}'))  ORDER BY created DESC `
    let path = baseUrl + releaseNameServ;
    let result = await getRequest(path)
    let validFixVersion = true;
    if (typeof result === "string") {
        if (result.includes("error")) {
            console.log("Error Details",result,fixVersion)
            validFixVersion = false;
        }
    }
    return validFixVersion;
}

async function getDefectsByIdService(defectKeyList) {
    let defectsByIdServ = `search?jql=(id in (${defectKeyList})) `
    let path = baseUrl + defectsByIdServ;
    return await getRequest(path)
}

async function getUserStoryByIdService(storyKeyList) {
    let userStoryByIdServ = `search?jql=(id in (${storyKeyList})) `
    let path = baseUrl + userStoryByIdServ;
    return await getRequest(path)
}


function validateProjectsNameJQLService(jqlProjectsNameIn) {
    return new Promise((resolve, reject) => {
        let userStories = `search?jql=project in (${jqlProjectsNameIn.toString()})  `
        let path = baseUrl + userStories
        let result = getRequestWithError(path)
        let finalString = jqlProjectsNameIn;
        let arr = [];
        if (result["errorMessages"] !== undefined) {
            let err = [];
            err = result["errorMessages"]
            let c = 0;
            for (i in err) {
                Array.prototype.clean = function (deleteValue) {
                    for (var i = 0; i < this.length; i++) {
                        if (this[i] == deleteValue) {
                            this.splice(i, 1);
                            i--;
                        }
                    }
                    return this;
                };
                c++;
                let val = err[i];
                let key = val.match(/'([^']+)'/)[1];
                finalString = jqlProjectsNameIn.splice(jqlProjectsNameIn.indexOf(key), 1, '')
                if (c === err.length) {
                    let array = jqlProjectsNameIn.clean('');
                    return resolve(array)
                }
            }
        } else {
            return resolve(jqlProjectsNameIn)
        }
    });

}
function insertStoryDefects(story) {
    return new Promise((resolve, reject) => {
        try {
            setTimeout(() => {
                let list = story[JIRA.issues];
                let i = 0;
                for (var key in list) {
                    let results = GET.getStoryDefectDetailsByRow(list, key);
                    if (results[2] != null) {
                        insertOrUpdateStoryDefectIntoDB(results[0], results[1], results[2]).then((fulfilled) => {
                            i++;
                            if (counter()) {
                                return resolve(`Defect Stories Inserted > ${i} `);
                            }
                        })
                    }
                }
                function counter() {
                    if (i == list.length) {
                        return true
                    } else
                        return false;
                }
            }, 1000)
        }
        catch (error) {
            console.log(currentTime(), " >> ", "Error on insertOrUpdateStory >> ", error.message);
            reject(error.message)
        }
    });
}


function insertUserStories(story) {
    return new Promise((resolve, reject) => {
        let list = story[JIRA.issues];
        let i = 0;
        for (var key in list) {
            let results = GET.getUserStoryDetailsByRow(list, key);
            if (results[1] != null) {
                let dataList = null, keyList = null;
                dataList = results[0];
                keyList = results[1]
                let insertPost = Object.assign(dataList, keyList)
                let updatePost = Object.assign(dataList)
                insertOrUpdateUserStoryIntoDB(insertPost, updatePost, keyList).then((fulfilled) => {
                    i++;
                    if (counter()) {
                        return resolve(`User Stories Inserted > ${i} `);
                    }
                })
            }
        }
        function counter() {
            if (i == list.length) {
                return true
            } else
                return false;
        }
    });
}

function insertOrUpdateComponentsIntoDB(insertPost, updatePost, componentId) {
    return new Promise((resolve, reject) => {
        try {
            let insertSet = `INSERT INTO components SET ?  `

            let updateSet = `UPDATE components SET ?, 
                            updatedOn=current_timestamp()  WHERE
                            ComponentId ='${componentId}' ;`
            var insertQry = sql.query(insertSet, insertPost, (err, res) => {
                if (err) {
                    if (err.errno === 1062) {
                        var updateQry = sql.query(updateSet, updatePost, (error, res) => {
                            if (error) {
                                console.log(currentTime(), " >> ", "Error on updation ", error.message)
                                reject(error.message)
                            } else {
                                resolve(`Row Updated > Results Key:  ${componentId} `);
                            }
                        });
                    } else {
                        console.log(currentTime(), " >> ", err.message);
                        reject(err.message)
                    }
                } else {
                    resolve(`Row inserted Result Key ${componentId} `, { id: res.insertId });
                }
            });

        } catch (error) {
            console.log(currentTime(), " >> ", "Error on insertOrUpdateComponentsIntoDB >> ", error.message);
            reject(error.message)
        }
    });
}

function insertOrUpdateStoryDefectIntoDB(dataList, keyList, storyKey) {
    return new Promise((resolve, reject) => {
        try {

            let insertPost = Object.assign(dataList, keyList);
            let updatePost = Object.assign(dataList);

            let insertSet = `INSERT INTO story_defect_details  SET ?`;

            let updateSet = `UPDATE story_defect_details SET ?, 
            updatedOn=current_timestamp()
            WHERE storyID ='${keyList["storyID"]}'  and storyKey ='${keyList["storyKey"]}' ; `;

            setTimeout(() => {
                var insertQry = sql.query(insertSet, insertPost, (err, res) => {
                    if (err) {
                        if (err.errno === 1062) {
                            var updateQry = sql.query(updateSet, updatePost, (error, res) => {
                                if (error) {
                                    console.log(currentTime(), " >> ", "Error on updation ", error.message)
                                    reject(error.message)
                                } else {
                                    resolve("Updated");
                                }
                            });
                        } else {
                            console.log(" insertOrUpdateStoryDefectIntoDB  ", err)
                            reject(err.message)
                        }
                    } else {
                        resolve("inserted");
                    }
                });
            }, 5000)
        } catch (error) {
            console.log(currentTime(), " >> ", "Error on insertOrUpdateStory >> ", error.message);
            reject(error.message)
        }
    });
}


function UpdateUserStoryIntoDB(updatePost, keyList) {
    return new Promise((resolve, reject) => {
        try {
            let updateSet = `UPDATE user_story_details
            SET ?, updatedOn=current_timestamp()  WHERE
               userStoryID ='${keyList["userStoryID"]}'  AND       
               storyKey = '${keyList["storyKey"]}' ;`
            var updateQry = sql.query(updateSet, updatePost, (error, res) => {
                if (error) {
                    console.log(currentTime(), " >> ", "Error on updation ", error.message)
                    reject(error.message)
                } else {
                    resolve(`Row Updated > Results Key:  ${keyList["userStoryID"]} `);
                }
            });


        } catch (error) {
            console.log(currentTime(), " >> ", "Error on insertOrUpdateUserStory >> ", error.message);
            reject(error.message)
        }
    });
}

function insertOrUpdateUserStoryIntoDB(insertPost, updatePost, keyList) {
    return new Promise((resolve, reject) => {
        try {
            let insertSet = `INSERT INTO user_story_details SET ?  `

            let updateSet = `UPDATE user_story_details
            SET ?, updatedOn=current_timestamp()  WHERE
               userStoryID ='${keyList["userStoryID"]}'  AND       
               storyKey = '${keyList["storyKey"]}' ;`
            var insertQry = sql.query(insertSet, insertPost, (err, res) => {
                if (err) {
                    if (err.errno === 1062) {
                        var updateQry = sql.query(updateSet, updatePost, (error, res) => {
                            if (error) {
                                console.log(currentTime(), " >> ", "Error on updation ", error.message)
                                reject(error.message)
                            } else {
                                resolve(`Row Updated > Results Key:  ${keyList["userStoryID"]} `);
                            }
                        });
                    } else {
                        console.log(currentTime(), " >> ", err.message)
                        reject(err.message)
                    }
                } else {
                    resolve(`Row inserted Result Key ${keyList["userStoryID"]} `, { id: res.insertId });
                }
            });

        } catch (error) {
            console.log(currentTime(), " >> ", "Error on insertOrUpdateUserStory >> ", error.message);
            reject(error.message)
        }
    });
}
let defectsList = [];

function insertOrUpdateDefectsIntoDB(dataList, keyList, defectKey) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            let insertSet = `INSERT INTO defect_details SET ?`
            let insertList = Object.assign(dataList, keyList);
            let updateSet = `update defect_details set ?,
            updatedOn=current_timestamp()  WHERE
            defectId= '${keyList["defectId"]}' and 
            defectKey= '${keyList["defectKey"]}';`;

            var insertQry = sql.query(insertSet, insertList, (err, res) => {
                if (err) {
                    if (err.errno === 1062) {
                        var updateQry = sql.query(updateSet, dataList, (error, res) => {
                            if (error) {
                                console.log(currentTime(), " >> ", "Error on updation ", error.message)
                                reject(error.message)
                            } else {
                                defectsList.push(`"${defectKey}"`);
                                resolve("Updated");
                            }
                        });
                    } else {
                        reject(err.message)
                    }
                } else {
                    defectsList.push(`"${defectKey}"`);
                    resolve("inserted");
                }
            });
        }, 1000)
    });
}

function UpdateDefectsIntoDB(dataList, keyList) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            let updateSet = `update defect_details set ?,
            updatedOn=current_timestamp()  WHERE
            defectId= '${keyList["defectId"]}' and 
            defectKey= '${keyList["defectKey"]}';`;

            var updateQry = sql.query(updateSet, dataList, (error, res) => {
                if (error) {
                    console.log(currentTime(), " >> ", "Error on updation ", error.message)
                    reject(error.message)
                } else {
                    resolve("Updated");
                }
            });

        }, 1000)
    });
}

function InsertDefectEntries(defects) {
    return new Promise((resolve, reject) => {
        let list = defects[JIRA.issues];
        let i = 0;
        for (var key in list) {
            let results = GET.getDefectDetailsByRow(list, key);
            if (results[2] != null) {
                insertOrUpdateDefectsIntoDB(results[0], results[1], results[2]).then((fulfilled) => {
                    i++;
                    if (counter()) {
                        return resolve(`Defects Inserted > ${i} `);
                    }
                })
            }
        }
        function counter() {
            if (i == list.length) {
                return true
            } else
                return false;
        }
    });
}

function updateDefctFixVersionsInDB() {
    return new Promise((resolve, reject) => {
        let qry = `update defect_details d inner join story_defect_details s on d.parentKey = s.storyKey set storyDefectIdFixVersion = s.fixVersions ,d.fixVersions = s.fixVersions  where d.fixVersions is null ;`
        setTimeout(() => {
            sql.query(qry, (err, res) => {
                if (err) {
                    console.log(currentTime(), " >> ", "Error on Update FixVersion ", err.message)
                    return reject("Error", err.message)
                }
                else {
                    return resolve("Fix Versions Updated")
                }
            });
        }, 1000)
    });
}

function updateStoryDefectFixVersion() {
    return new Promise((resolve, reject) => {
        let qry = `update defect_details set fixVersions= storyDefectIdFixVersion where fixVersions is null ;`
        setTimeout(() => {
            sql.query(qry, (err, res) => {
                if (err) {
                    console.log(currentTime(), " >> ", "Error on Update FixVersion ", err.message)
                    return reject("Error", err.message)
                }
                else {
                    return resolve("Story Defect Fix Versions Updated")
                }
            });
        }, 1000)
    });
}

function validateReleaseNameList(releaseNameList) {
    return new Promise((resolve, reject) => {
        let updatedReleaseNameList = [];
        let counter = 1;
        if (typeof releaseNameList === "string") {
            validateReleaseNameService(releaseNameList).then(releaseStatus => {
                if (releaseStatus === true) {
                    updatedReleaseNameList.push(releaseNameList)
                    return resolve(updatedReleaseNameList)
                } else {
                    return resolve(updatedReleaseNameList)
                }
            })
        } else {
            for (i in releaseNameList) {
                let key = releaseNameList[i]
                validateReleaseNameService(key).then(releaseStatus => {
                    counter++;
                    if (releaseStatus === true) {
                        updatedReleaseNameList.push(key)
                    }
                    if (counter === releaseNameList.length) {
                        return resolve(updatedReleaseNameList)
                    }
                })

            }

        }

    });
}


function GetProjectsListFromDB(releaseName) {
    return new Promise((resolve, reject) => {
        let qry = `select  Jira_defect_projects  from project_milestones_status where releaseName = '${releaseName}';`;
        if (releaseName === null || releaseName === undefined || releaseName === "upcomingRelease")
            qry = `select ProjectReference,ProjectName,JIRA_Link,Jira_defect_projects FROM v_project_ccb_details WHERE DATE(ReleaseDate) >= (DATE(NOW()) - INTERVAL 9 MONTH);`;
        console.log(currentTime(), " >> ", qry)
        setTimeout(() => {
            sql.query(qry, (err, res) => {
                if (err) {
                    reject(err.message)
                } else {
                    resolve(res);
                }
            });
        }, 1000)
    });
}

function GetJIRAProjectKeyFromDB(releaseName) {
    return new Promise((resolve, reject) => {
        let qry = `select distinct JIRA_KEY from v_project_wise_defect_keys`;
        setTimeout(() => {
            sql.query(qry, (err, res) => {
                if (err) {
                    reject(err.message)
                } else {
                    let arr = [];
                    Object.keys(res).forEach(function (key) {
                        let names = res[key]["JIRA_KEY"]
                        if (names !== null) {
                            arr.push(names.replace(/ /g, ''))
                        }
                    });
                    resolve(arr);
                }
            });
        }, 1000)
    });
}

function GetUpcomingReleaseList(releaseName) {
    return new Promise((resolve, reject) => {
        if (releaseName === null || releaseName === undefined || releaseName === "upcomingRelease") {
            qry = `select  distinct ReleaseName  from v_release_moments  where ReleaseDate>= date_sub(curdate(),INTERVAL 90 DAY);`;
            console.log(currentTime(), " >>  ReleaseName", qry)
            setTimeout(() => {
                sql.query(qry, (err, res) => {
                    if (err) {
                        reject(err.message)
                    } else {
                        let arr = [];
                        Object.keys(res).forEach(function (key) {
                            let names = res[key]["ReleaseName"]
                            if (names !== null) {
                                arr.push(names)
                            }

                        });
                        resolve(arr);
                    }
                });
            }, 1000)
        } else {
            resolve(releaseName)
        }
    });
}


function getParentStoryForDefectsFromDB(defectsList) {
    return new Promise((resolve, reject) => {
        let qry = `select distinct parentKey from defect_details where issueType='Story defect' and parentKey is not null ;`;
        setTimeout(() => {
            sql.query(qry, (err, res) => {
                if (err) {
                    console.log(currentTime(), " >> ", err.message)
                    //logger.error(CONS.defects, " >> ", err.message)
                    reject(err.message)
                } else {
                    resolve(res);
                }
            });
        }, 1000)
    });
}

function getAllParentStoryForDefectsFromDB() {
    return new Promise((resolve, reject) => {
        let qry = `select distinct parentKey from defect_details where issueType='Story defect' and parentKey is not null ;`;
        setTimeout(() => {
            sql.query(qry, (err, res) => {
                if (err) {
                    console.log(currentTime(), " >> ", err.message)
                    //logger.error(CONS.defects, " >> ", err.message)
                    reject(err.message)
                } else {
                    resolve(res);
                }
            });
        }, 1000)
    });
}

const UploadDefects = (defects) => {
    return new Promise((resolve, reject) => {
        try {
            InsertDefectEntries(defects)
                .then((fulfilled) => {
                    return resolve(fulfilled)
                })
                .catch(error => {
                    console.log(currentTime(), " >>  InsertDefectEntries", error);
                });
        } catch (error) {
            console.log(currentTime(), " >>  UploadDefects", error)
            return reject("UploadDefects ", error)
        }
    })
}

const UploadUserStories = (userStories) => {
    return new Promise((resolve, reject) => {
        try {
            insertUserStories(userStories)
                .then((fulfilled) => {
                    if (userStories.length != 0) {
                        return resolve(fulfilled)
                    }
                })
                .catch(error => {
                    console.log(currentTime(), " >>  insertUserStories", error);
                });
        } catch (error) {
             console.log(currentTime(), " >>  Upload User Story", error)
            return reject("UploadUserStorys ", error)
        }

    })
}


 function IterateProjectNamesList(result) {
    return new Promise((resolve, reject) => {
    var set1 = new Set();
    let arr = [];
    Object.keys(result).forEach(function (key) {
        let names = result[key]["Jira_defect_projects"]
        if (names !== null) {
            let arr = names.split(",")
            for (i in arr) {
                set1.add(arr[i])
            }
        }

    });
    arr = Array.from(set1);
    projectNameList = arr;
    let finalProjectList = [];
    let counterReturn = 1;
            for (i in arr) {
            let project = arr[i];
            validateJiraProjectKey(project).then(projectStatus => {
                counterReturn++;
                if (projectStatus === true) {
                    finalProjectList.push(project)
                }
                if (counterReturn === arr.length) {
                    return resolve(finalProjectList);
                }
            }).catch(error => {
                console.log(currentTime(), error);
            });

        }
    })
    // return projectNameList;
}

async function validateJiraProjectKey(projectKey) {
    let path = baseUrl+"project/"+projectKey;
    let result = await getRequest(path)
    let validProjectKey = true;
    if (typeof result === "string") {
        if (result.includes("error")) {
            // console.log("Error Details",result)
            validProjectKey = false;
        }
    }
    return validProjectKey;
}




var totalDefects = 0;

const GetJiraProjectNamesList = (projectNameList, releaseName) => {
    return new Promise((resolve, reject) => {
        try {
            let jqlProjectsNameIn = null;
            jqlProjectsNameIn = Array.from(projectNameList)
            GetTotalDefectsByProjects(jqlProjectsNameIn, releaseName).then(response => {
                return resolve(response);
            })
        } catch (error) {
            console.log(currentTime(), " >> GetJiraProjectNamesList", error)
            return reject("GetJiraProjectNamesList", error)
        }

    })

}

let defects = null;
var totalStories = 0;
const GetTotalDefectsByProjects = (jqlProjectsNameIn, releaseName) => {
    return new Promise((resolve, reject) => {
        try {
            validateProjectsNameJQLService(jqlProjectsNameIn).then(projectNameResponse => {
                getTotalDefectsByProjectsService(projectNameResponse, releaseName)
                    .then((response) => {
                        let total = response;
                        totalDefects = total
                        console.log(currentTime(), " >> ", "Total Defects  ", total)
                        let startsAt = 0;
                        let totCounter = Math.round(total / 50)
                        let counter = 1;
                        console.log('Defects insertion in progress  ',)
                        for (startsAt = 0; startsAt < total; startsAt = startsAt + 50) {
                            GetDefectsByProjects(projectNameResponse, releaseName, startsAt).then(response => {
                            //  process.stdout.write("..");
                                counter++
                                if (counter == totCounter) {
                                    console.log("\r\n") 
                                    console.log(currentTime(), " >> ", "Total Defects Inserted ", total)
                                    return resolve("Total Defects Inserted ", total)
                                }
                            })

                        }
                    })
                    .catch(error => {
                        console.log(currentTime(), " >> ", error);
                    });
            });
        } catch (error) {
            console.log(currentTime(), " >> GetTotalDefectsByProjects", error)
            reject("GetTotalDefectsByProjects", error)
        }
    });
}


const GetComponentListByProjects = (projectKeyList) => {
    return new Promise((resolve, reject) => {
        try {

            validateCredentials().then(response => {
                let counter = 0;
                console.log("Component insertion in progress") 
                for (let i = 0; i < projectKeyList.length; i++) {
                    process.stdout.write(`.`);
                    getComponentByProjectsService(projectKeyList[i]).then(response => {
                        let list = response;
                        for (const key in list) {
                            let element = list[key];
                            let componentId = element["id"];
                            let dataList = {
                                ComponentName: element["name"],
                                projectKey: element["project"],
                                projectId: element["projectId"],
                            }
                            let keyList = {
                                ComponentId: componentId,
                            }
                            let insertPost = Object.assign(dataList, keyList)
                            let updatePost = Object.assign(dataList)
                            
                            insertOrUpdateComponentsIntoDB(insertPost, updatePost, componentId).then((fulfilled) => {
                              
                                totalComponents++;
                                counter++;
                                if (counter === i) {
                                    console.log("\r\n") 
                                    return (resolve("Components Inserted"))
                                }
                            });
                        }
                    }).catch(err => {
                        reject("getComponentByProjectsService", err)
                      });
                }
            }).catch(error => {
                console.log(currentTime(), CONS.jira, error);
                reject("Invalid credntials ");
            })
        } catch (error) {
            console.log(currentTime(), " >> GetComponentListByProjects", error)
            reject("GetComponentListByProjects", error)
        }
    }).catch(error => {
       

    })
}

let userStories = null;
const GetUserStoriesByProjectsRelease = (jqlProjectsNameIn, releaseNameList) => {
    return new Promise((resolve, reject) => {
        try {
            validateProjectsNameJQLService(jqlProjectsNameIn).then(projectNameResponse => {
                getTotalUserStoryByProjectsService(projectNameResponse, releaseNameList)
                    .then((response) => {
                        console.log(currentTime(), " >> ", "Total response Stories  ", response)
                     
                        let total = response;
                        totalUserStories = total
                        console.log(currentTime(), " >> ", "Total User Stories  ", total)
                        let startsAt = 0;
                        let totCounter = Math.round(total / 50)
                        let counter = 1;
                        console.log('User story insertion is progress')
                        for (startsAt = 0; startsAt < total; startsAt = startsAt + 50) {
                            GetUserStoryByProjects(projectNameResponse, releaseNameList, startsAt).then(response => {
                                counter++
                                process.stdout.write(`.`);
                                if (counter == totCounter) {
                                    console.log('\r\n')
                                    console.log(currentTime(), " >> ", "Total UserStories Inserted ", total)
                                    return resolve("Total UserStories Inserted ", total)
                                }
                            })
                        }
                    })
                    .catch(error => {
                        console.log(currentTime(), " >>  getTotalUserStoryByProjectsService ", error);
                    });

            })

        } catch (error) {
            console.log(currentTime(), " >> GetUserStoriesByProjectsRelease", error)
            reject("GetUserStoriesByProjectsRelease", error)
        }
    })
}


const GetDefectsByProjects = (jqlProjectsNameIn, releaseName, startsAt) => {
    return new Promise((resolve, reject) => {
        try {
            getDefectsByProjectsService(jqlProjectsNameIn, releaseName, startsAt)
                .then((response) => {
                    defects = response;
                    return resolve(UploadDefects(defects, releaseName));
                })
                .catch(error => {
                    console.log(error);
                });
        } catch (error) {
            reject("GetDefectsByProjects", error)
        }
    })
}

const GetUserStoryByProjects = (jqlProjectsNameIn, releaseName, startsAt) => {
    return new Promise((resolve, reject) => {
        try {
            getUserStortByProjectsService(jqlProjectsNameIn, releaseName, startsAt)
                .then((response) => {
                    userStories = response;
                    return resolve(UploadUserStories(userStories, releaseName));
                })
                .catch(error => {
                    console.log(error);

                });
        } catch (error) {
            reject("GetUserStoryByProjects", error)
        }
    })
}

let storyList = null;
const GetTotalStoryDefects = (jqlStoryKeyIn, releaseName) => {
    return new Promise((resolve, reject) => {
        try {
            getTotalStoryDefectsService(jqlStoryKeyIn, releaseName)
                .then((response) => {
                    let total = response;
                    totalStories = totalStories + total;
                    let startsAt = 0;
                    for (startsAt = 0; startsAt < total; startsAt = startsAt + 50) {
                        GetStoryDefects(jqlStoryKeyIn, releaseName, startsAt).then(response => {
                            resolve(response)
                        })
                    }
                })
                .catch(error => {
                    console.log(currentTime(), " >> ", error);
                });
        } catch (error) {
            reject("GetTotalStoriesForDefects", error)
        }
    })
}

const UploadStoryDefects = (storyList) => {
    return new Promise((resolve, reject) => {
        try {
            insertStoryDefects(storyList)
                .then((fulfilled) => {
                    return resolve(fulfilled)
                })
                .catch(error => {
                    console.log(currentTime(), " >> ", error);
                });
        } catch (error) {
            reject("UploadStories", error)
        }
    })
}

const UpdateDefctFixVersions = () => {
    return new Promise((resolve, reject) => {
        try {
            let defectsList=null;
            GetParentStoryDefectsForDefects(defectsList).then(response => {
                                                
            updateDefctFixVersionsInDB()
                .then((fulfilled) => {
                    resolve("Updated Fix Versions Resolved")
                })
                .catch(error => {
                    console.log(currentTime(), " >> ", error);
                });
            });
        } catch (error) {
            reject("UpdateDefctFixVersions", error)
        }
    })
}


const GetStoryDefects = (jqlStoryKeyIn, releaseName, startsAt) => {
    return new Promise((resolve, reject) => {
        try {
            getStoryDefectsService(jqlStoryKeyIn, releaseName, startsAt)
                .then((response) => {
                    storyList = response;
                    UploadStoryDefects(storyList).then(response => {
                        return resolve(response)
                    })
                })
                .catch(error => {
                    console.log(currentTime(), " >> ", error);
                });
        } catch (error) {
            return reject("GetStoriesForDefects", error)
        }
    })
}

const GetParentStoryDefectsForDefects = (defectsList) => {
    return new Promise((resolve, reject) => {
        try {
            getParentStoryForDefectsFromDB(defectsList)
                .then((results) => {
                    let parentStoryDefectList = new Set();
                    Object.keys(results).forEach(function (key) {
                        parentStoryDefectList.add(`'${results[key]["parentKey"]}'`);
                    });
                    let jqlStoryKeyIn = Array.from(parentStoryDefectList);
                    if (jqlStoryKeyIn.length !== 0) {
                        GetTotalStoryDefects(jqlStoryKeyIn).then(response => {
                            resolve(response)
                        })
                    }
                    else {
                        console.log(currentTime(), " >> ", "ParentKey Not found for any of the defects")
                        resolve("ParentKey Not found for any of the defects")
                    }
                })
                .catch(error => {
                    console.log(currentTime(), " >> ", error);
                });
        } catch (error) {
            reject("GetParentStoryForDefects", error)
        }
    })
}


const UpdateDefectsBeforeInsertion = (recordsToUpdate) => {
    return new Promise((resolve, reject) => {
        let total = recordsToUpdate.length;
        let totCounter = Math.round(total / 50)
        let count = 0;
        console.log('Defects Updation in progress')
        for (startsAt = 0; startsAt < total; startsAt = startsAt + 50) {
           // process.stdout.write(`.`);
            let defectKey = recordsToUpdate.slice(startsAt, (startsAt + 50));
            getDefectsByIdService(defectKey.toString()).then(response => {
                let list = response[JIRA.issues];
                let i = 0;
               // process.stdout.write(`#`);
                for (var key in list) {
                    let results = GET.getDefectDetailsByRow(list, key);
                    if (results[1] != null) {
                        let dataList = null, keyList = null;
                        dataList = results[0];
                        keyList = results[1]
                        let updatePost = Object.assign(dataList)
                        UpdateDefectsIntoDB(updatePost, keyList).then((fulfilled) => {
                            i++;
                            if (counter()) {
                                console.log('\r\n')
                                return resolve(`Defects Updated > ${total} `);
                            }
                        })
                    }
                    function counter() {
                        if (i == list.length) {
                            return true
                        } else
                            return false;
                    }
                }
            });
        }
    });
};

const CleanUpDefects = () => {
    return new Promise((resolve, reject) => {
        try {
            var startDate = moment(new Date(), 'YYYY-M-DD HH:mm:ss')
            console.log(currentTime(), " >> ", "Started DEFECTS CleanUp >>", new Date(), );
            let lastRecordIdB4Insertion = null;
            validateCredentials().then(response => {
                console.log("Valid Credtentials JIRA   >>>" , response["name"] , response["active"] )
                DeleteDefectsBeforeInsertion().then(data => {
                    let recordsToupdate = data[2];
                   UpdateDefectsBeforeInsertion(recordsToupdate).then(response => {
                        console.log(currentTime(), " >> ", response)
                        resolve("Defects Cleanup Completed");
                   });
                });  
            }).catch(error => {
                console.log(currentTime(), CONS.jira, error);
                reject("Invalid credntials ");
            })
        }catch(error){
            console.log(currentTime()," >> CleanUpDefects " , error)
            reject("CleanUpDefects ", error)
        }
    }).catch(error => {
        reject(error)

    })

}

const UpdateDefectFixVersion =() => {
    return new Promise((resolve,reject)=>{
        try{
            console.log("Defect Fix Versions -> Updations")
            UpdateDefctFixVersions().then(response => {
                // console.log(response)
                updateStoryDefectFixVersion().then(response=>{
                    // console.log(response)
                    UpdateDefectEpicLinkList().then(response=>{
                        // console.log(response)
                        UpdateUserStoryDefectEpicLinkList().then(response=>{
                            // console.log("Response",response)
                            resolve(response)
                        });
                    });
                })
              
            });
        }catch(error){
            console.log(currentTime(), "UpdateDefect Fix Versions ", error)
            reject("UpdatefixVersions", error)

        }
    }).catch(error=>{
        reject(error)
    })
                                                    

}
/*Update epic Link in Test Execution*/

const UpdateEpicLinkinTestExecutionandRequirement =() => {
    return new Promise((resolve,reject)=>{
        try{
                    UpdateTestEpicLinkList().then(response=>{
                        resolve(response)
                        UpdateRequirementEpicLinkList().then(response=>{
                            resolve(response)
                        });
                    });
        }catch(error){
            console.log(currentTime(), "Update Epic Link ", error)
            reject("UpdatefixVersions", error)

        }
    }).catch(error=>{
        reject(error)
    })                                                   

}

const UpdateTestEpicLinkList =() => {
    return new Promise((resolve, reject) => {
        let  qry = `select  distinct reqAltId  from test_execution_results where reqAltId IS NOT NULL;`;
            sql.query(qry, (err, res) => {
                if (err) {
                    reject(err.message)
                } else {
                    let epicData = JSON.parse(JSON.stringify(res));
                    updateEpicLinkinTest(epicData).then(response=>{
                        return resolve(response);
                    });
                                               
                }
            });
    }).catch(error=>{
        reject(error)
    });                                                    
}

const UpdateRequirementEpicLinkList =() => {
    return new Promise((resolve, reject) => {
        let  qry = `select  distinct externalId  from requirement_details where externalId IS NOT NULL;`;
            sql.query(qry, (err, res) => {
                if (err) {
                    reject(err.message)
                } else {
                    let epicData = JSON.parse(JSON.stringify(res));
                    updateEpicLinkinRequirement(epicData).then(response=>{
                        return resolve(response);
                    });
                                               
                }
            });
    }).catch(error=>{
        reject(error)
    });                                                    
}

function processData(data) {
    data = data[JIRA.epicLink]
    if (data === undefined)
        return null;
    else {
        if (typeof data === "string") {
            data = data.replace(/'/g, "''")
        }
        return data;
    }
}

function updateEpicLinkinTest(epicData) {
    return new Promise((resolve, reject) => {
            for(let i= 0; i<epicData.length;i++){  
                let epic = epicData[i];              
                getEpicLink(epic.reqAltId).then(data => { 
                   let status = data.issues[0][JIRA.fields]['status']['name'];  
                    let summary = data.issues[0][JIRA.fields]['summary'];               
                    let epicLink = processData(data.issues[0][JIRA.fields]);
                    let qry = `update test_execution_results set epicLink = '${epicLink}',epicName= '${summary}',epicStatus = '${status}'  where reqAltId =  '${epic.reqAltId}';`
                        sql.query(qry, (err, res) => {
                            if (err) {
                                return reject("Error", err.message)
                            }
                            else {                           
                                if(i === epicData.length-1){

                                    return resolve("Epic Link Updated")
                                }
                            }
                        });
            }) ;  
            }
    }).catch(error=>{
        reject(error)
    });
}

function updateEpicLinkinRequirement(epicData) {
    return new Promise((resolve, reject) => {
            for(let i= 0; i<epicData.length;i++){  
                let epic = epicData[i];              
                getEpicLink(epic.externalId).then(data => {   
                    if(data !== undefined){
                        if(data["issues"] && data["issues"].length !== 0){
                            // console.log(data.issues[0])
                            let epicLink = processData(data.issues[0][JIRA.fields]);
                            let status = data.issues[0][JIRA.fields]['status']['name'];  
                            let summary = data.issues[0][JIRA.fields]['summary'];  
                            let qry = `update requirement_details set epicLink = '${epicLink}',epicName= '${summary}',epicStatus = '${status}'  where externalId =  '${epic.externalId}';`
                                sql.query(qry, (err, res) => {
                                    if (err) {
                                        return reject("Error", err.message)
                                    }
                                    else {                           
                                        if(i === epicData.length-1){
        
                                            return resolve("Epic Link Updated")
                                        }
                                    }
                                });
                        }                 
                        }
            }) ;  
            }
    }).catch(error=>{
        reject(error)
    });
}

const UpdateDefectEpicLinkList =() => {
    return new Promise((resolve, reject) => {
        let  qry = `select  distinct epicLink  from defect_details WHERE epicLink IS NOT NULL;`;
            sql.query(qry, (err, res) => {
                if (err) {
                    reject(err.message)
                } else {
                    let epicData = JSON.parse(JSON.stringify(res));
                    updateEpicSummaryinDB(epicData).then(response=>{
                        return resolve(response);
                    });
                                               
                }
            });
    }).catch(error=>{
        reject(error)
    });                                                    
}

const UpdateUserStoryDefectEpicLinkList =() => {
    return new Promise((resolve, reject) => {
        let  qry = `select  distinct epicLink  from user_story_details WHERE epicLink IS NOT NULL;`;
            sql.query(qry, (err, res) => {
                if (err) {
                    reject(err.message)
                } else {
                    let epicData = JSON.parse(JSON.stringify(res));
                    updateUserStoryEpicSummaryinDB(epicData).then(response=>{
                        return resolve(response);
                    });
                                               
                }
            });
    }).catch(error=>{
        reject(error)
    });                                                    
}

function updateEpicSummaryinDB(epicData) {
    return new Promise((resolve, reject) => {
            for(let i= 0; i<epicData.length;i++){  
                let epic = epicData[i];              
                getJiraEpicSummary(epic.epicLink).then(data => {

                    let status = data.issues[0][JIRA.fields]['status']['name'];  
                    let summary = data.issues[0][JIRA.fields]['summary'];  
                    let qry = `update defect_details set epicName= '${summary}',epicStatus = '${status}' where epicLink =  '${epic.epicLink}';`
                        sql.query(qry, (err, res) => {
                            if (err) {
                                return reject("Error", err.message)
                            }
                            else {                           
                                if(i === epicData.length-1){

                                    return resolve("Epic Summary Updated")
                                }
                            }
                        });
            }) ;  
            }
    }).catch(error=>{
        reject(error)
    });
}

function updateUserStoryEpicSummaryinDB(epicData) {
    return new Promise((resolve, reject) => {
            for(let i= 0; i<epicData.length;i++){  
                let epic = epicData[i];              
                getJiraEpicSummary(epic.epicLink).then(data => {
                        if(data.hasOwnProperty('issues')){
                            let status = data.issues[0][JIRA.fields]['status']['name'];  
                            let summary = data.issues[0][JIRA.fields]['summary'];  
                            // console.log(epic.epicLink,summary,status)
                            let qry = `update user_story_details set epicName= '${summary}',epicStatus = '${status}' where epicLink =  '${epic.epicLink}';`
                                sql.query(qry, (err, res) => {
                                    if (err) {
                                        return reject("Error", err.message)
                                    }
                                    else {                
                                    if(i === epicData.length-1){
                                            return resolve("Epic Summary Updated")
                                        }
                                    }
                                });

                        }                  
            }) ;  
            }
    }).catch(error=>{
        reject(error)
    });
}

function formatKeyMessage(str) {
    if (str === undefined)
    return null;
else {
    let string  = str.replace(/'/g, "''")
    return string;
}
}

const DeleteDefectsBeforeInsertion = () => {
    return new Promise((resolve, reject) => {
        DATA_MODELS.Defects.getAllRecordsDefectDetails().then(response => {
            if (response === null) {
                return resolve("No Records are available to delete")
            }
            else {
                let ids = response[0];
                let defectKey = response[1];
                let total = defectKey.length;
                let totCounter = Math.round(total / 50)
                let count = 0;
                let recordsToDelete = [];
                let recordsToUpdate = [];
                console.log("Validation in progress")
                for (startsAt = 0; startsAt < total; startsAt = startsAt + 50) {
                    let defKey = defectKey.slice(startsAt, (startsAt + 50));
                   // process.stdout.write(`.`);
                    getDefectsByIdService(defKey.toString()).then(response => {
                       // process.stdout.write(`#`);
                        let list = response[JIRA.issues];
                        let i = 0;
                        for (var key in list) {
                            let results = GET.getDefectDataById(list, key);
                                let toBeRemoved = results[1];
                                if (!toBeRemoved) {
                                    i++;
                                    recordsToUpdate.push(results[0]);
                                } else if (toBeRemoved) {
                                    recordsToDelete.push(results[0]);
                                    i++;
                                }
                                if (counter()) {
                                    count++
                                    if (count == totCounter) {
                                        console.log('\r\n')
                                        DATA_MODELS.Defects.deleteDefectsByDefectKey(recordsToDelete).then(deleteResponse => {
                                            console.log(currentTime(), " >> ", "Delete response ", deleteResponse);
                                            return resolve(["Records CleanUp Before Insertion", recordsToDelete.toString(), recordsToUpdate]);
                                        })

                                    }
                                }
                                function counter() {
                                    if (i == list.length) {
                                        return true
                                    } else
                                        return false;
                                }
                           // });
                        }
                    });
                }
            }

        })
    });
};


var moment = require('moment');
const { Console } = require("console");
const { reject } = require("underscore");
const { relativeTimeThreshold } = require("moment");
const { closePath } = require("pdf-lib");
const UploadJiraDefectsForProjects = (runType = false, releaseName) => {
    return new Promise((resolve, reject) => {
        try {
            var startDate = moment(new Date(), 'YYYY-M-DD HH:mm:ss')
            console.log(currentTime(), " >> ", "Started DEFECTS >>", new Date(), "   >> ReleaseName  ", releaseName);
        
            let lastRecordIdB4Insertion = null;

            validateCredentials().then(response => {
                        DATA_MODELS.Defects.getLstRecordCountIdDefectDetails().then(recordId => {
                            lastRecordIdB4Insertion = recordId;
                            console.log("Last Record before insertion ", lastRecordIdB4Insertion)
                            DATA_MODELS.DataUploads.updateExecution(runType, API.defects).then(updateResponse => {
                            GetProjectsListFromDB(releaseName).then((result) => {
                            IterateProjectNamesList(result).then((projectNameList) => {       
                                    if (projectNameList.length > 0) {
                                        GetJiraProjectNamesList(projectNameList, releaseName).then(response => {
                                            let tat = null;
                                            if (defectsList.length != 0) {
                                                GetParentStoryDefectsForDefects(defectsList).then(response => {
                                                    console.log(currentTime(), " >> ", response)
                                                    UpdateDefctFixVersions().then(response => {
                                                        console.log(currentTime(), " >> ", response)
                                                        totalTimeTaken(startDate).then(response => {
                                                            tat = response;
                                                            console.log(currentTime(), " >> ", `Total Records Inserted ${totalDefects} ,  Time Taken >> ${tat} `);
                                                            DATA_MODELS.Defects.getUnaffectedRecordsDefectDetails(lastRecordIdB4Insertion).then(deletionResponse => {
                                                                console.log(currentTime(), " >> ", 'Total record cleaned up from DB ', deletionResponse);
                                                                DATA_MODELS.DataUploads.updateExecution(runType, API.defects).then(updateResponse => {
                                                                    DATA_MODELS.DataUploads.getAllData().then(uploadsData => {
                                                                        return resolve([`Total Defects Inserted ${totalDefects} ,Total Stories Inserted ${totalStories}, Time Taken >> ${tat}`, uploadsData]);

                                                                    });
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            }
                                        });
                                    } else {
                                        console.log(currentTime(), " >> ", "JIRA_PROJECT_DEFECTS are not defined!!!")
                                   
                                    }
                                });

                            }).catch(error => {
                                    console.log(currentTime(), " >> ", error);
                            });
                        });
                        });
                   }).catch(error => {
                console.log(currentTime(), CONS.jira, error);
                reject("Invalid credntials ");
            })
        } catch (error) {
            console.log(currentTime(), " >>  UploadJiraDefectsForProjects", error)
            reject("UploadJiraDefectsForProjects", error)
        }

    }).catch(error => {
        reject(error)

    })

}

const UpdateUserStoriesBeforeInsertion = (recordsToUpdate) => {
    return new Promise((resolve, reject) => {
        let total = recordsToUpdate.length;
        let totCounter = Math.round(total / 50)
        let count = 0;
        for (startsAt = 0; startsAt < total; startsAt = startsAt + 50) {
            let userStoryKey = recordsToUpdate.slice(startsAt, (startsAt + 50));
            getUserStoryByIdService(userStoryKey.toString()).then(response => {
                let list = response[JIRA.issues];
                let i = 0;
                for (var key in list) {
                    let results = GET.getUserStoryDetailsByRow(list, key);
                    if (results[1] != null) {
                        let dataList = null, keyList = null;
                        dataList = results[0];
                        keyList = results[1]
                        let updatePost = Object.assign(dataList)
                        UpdateUserStoryIntoDB(updatePost, keyList).then((fulfilled) => {
                            i++;
                            if (counter()) {
                                return resolve(`User Stories Updated > ${total} `);
                            }
                        })
                    }
                    function counter() {
                        if (i == list.length) {
                            return true
                        } else
                            return false;
                    }
                }
            });
        }
    });
};

const DeleteUserStoriesBeforeInsertion = () => {
    return new Promise((resolve, reject) => {
        DATA_MODELS.UserStory.getAllRecordsUserStory().then(response => {

            if (response === null) {
                return resolve("No Records are available to delete")
            }
            else {
                let ids = response[0];
                let storyKey = response[1]; 
                let map = response[2];
                let storyIdwiseKey=response[3];
                let total = storyKey.length;
                let totCounter = Math.round(total / 50)
                let count = 0;
                let recordsToDelete = [];
                let recordsToUpdate = [];
                console.log("User story validation in progress")
                for (startsAt = 0; startsAt < total; startsAt = startsAt + 50) {
                    let userStoryKey = storyKey.slice(startsAt, (startsAt + 50));
                    getUserStoryByIdService(userStoryKey.toString()).then(response => {
                       // process.stdout.write(`.`);
                        let list = response[JIRA.issues];
                        let i = 0;
                        for (var key in list) {
                            GET.getUserStoryDataById(list, key,map,storyIdwiseKey).then(results => {
                                let toBeRemoved = results[1];
                                if (!toBeRemoved) {
                                    i++;
                                    recordsToUpdate.push(results[0]);
                                } else if (toBeRemoved) {
                                    recordsToDelete.push(results[0]);
                                    i++;
                                }
                                if (counter()) {
                                    count++
                                    if (count == totCounter) {
                                        console.log('\r\n')
                                        DATA_MODELS.UserStory.deleteUserStoryByUserStoryKey(recordsToDelete).then(deleteResponse => {
                                            console.log(currentTime(), " >> ", "Delete response ", deleteResponse);
                                            return resolve(["User Story Records CleanUp Before Insertion", recordsToDelete.toString(), recordsToUpdate])
                                        })
                                    }
                                }
                                function counter() {
                                    if (i == list.length) {
                                        return true
                                    } else
                                        return false;
                                }
                            });
                        }
                    });
                }
            }

        })
    });
};

const CleanUpUserStories = () => {
    return new Promise((resolve, reject) => {
        try {
            var startDate = moment(new Date(), 'YYYY-M-DD HH:mm:ss')
            console.log(currentTime(), " >> ", "Started User Stories Cleanup >>", new Date()  );
            let lastRecordIdB4Insertion = null;
            validateCredentials().then(response => {
                let recordsToUpdate = null;
                DeleteUserStoriesBeforeInsertion().then(data => {
                    recordsToUpdate = data[2];
                   console.log(" After Delete UserStories before insertion ")
                    UpdateUserStoriesBeforeInsertion(recordsToUpdate).then(updateStatus => {
                     console.log(updateStatus)
                       resolve("CleanUp Completed!!") 
                   });
               });
            }).catch(error => {
                console.log(currentTime(), CONS.jira, error);
                reject("Invalid credntials ");
            })
        } catch (error) {
            console.log(currentTime(), " >> UploadUserStoryForProjects", error)
            reject("UploadUserStoryForProjects", error)
        }

    })

}


var totalUserStories = 0;
const UploadJiraUserStories = (runType = API.automatedRun, releaseName) => {
    return new Promise((resolve, reject) => {
        try {
            var startDate = moment(new Date(), 'YYYY-M-DD HH:mm:ss')
            console.log(currentTime(), " >> ", "Started User Stories >>", new Date(), "   >> ReleaseName  ", releaseName);

            let lastRecordIdB4Insertion = null;
            validateCredentials().then(response => {
                let recordsToUpdate = null;
             //   DeleteUserStoriesBeforeInsertion().then(data => {
               //     recordsToUpdate = data[2];
                 //   console.log(" After Delete UserStories before insertion ")
                    // Disabling updates log
                  //  UpdateUserStoriesBeforeInsertion(recordsToUpdate).then(updateStatus => {
                       // console.log(updateStatus)
                        DATA_MODELS.UserStory.getLstRecordCountIdUserStoryDetails().then(recordId => {
                            lastRecordIdB4Insertion = recordId;
                            console.log(" Last Record before insertion")
                            DATA_MODELS.DataUploads.updateExecution(runType, API.userStory).then(updateResponse => {
                                console.log(' Asfter updated')
                            GetUpcomingReleaseList(releaseName).then((releaseData) => {
                                releaseNameList = releaseData;
                                console.log(currentTime(), " >> Release List", releaseNameList)
                                validateReleaseNameList(releaseNameList).then((updatedList => {
                                    if (updatedList.length === 0) {
                                        DATA_MODELS.DataUploads.getAllData().then(uploadsData => {
                                            return resolve([`No User Stories Inserted, Invalid ReleaseName ${releaseName}`, uploadsData])
                                        });
                                    } else {

                                    }
                                    console.log(currentTime(), " >> FixVersions Available in JIRA >>> ", updatedList)
                                    GetProjectsListFromDB(releaseName)
                                        .then((result) => {
                                            IterateProjectNamesList(result).then((projectNameList) => {
                                            if (projectNameList.length > 0) {
                                                GetUserStoriesByProjectsRelease(projectNameList, updatedList).then(response => {
                                                    let tat = null;
                                                    totalTimeTaken(startDate).then(response => {
                                                        tat = response;
                                                        console.log(currentTime(), " >> ", `Total Records Inserted ${totalUserStories} ,  Time Taken >> ${tat} `);
                                                        DATA_MODELS.DataUploads.updateExecution(runType, API.userStory).then(updateResponse => {
                                                            DATA_MODELS.DataUploads.getAllData().then(uploadsData => {
                                                                return resolve([`Total User Stories Inserted ${totalUserStories} Time Taken >> ${tat}`, uploadsData])
                                                            });

                                                        });

                                                        /*  Deletion logic commented since, the records are retrived based on fixVersion // proceeding leads to deletion of old fix version records
                                                        DATA_MODELS.UserStory.getUnaffectedRecordsUserStoryDetails(lastRecordIdB4Insertion).then(deletionResponse => {
                                                            console.log(currentTime(), " >> ", 'Total User Story record cleaned up from DB ', deletionResponse);
                                                            return resolve(`Total User Stories Inserted ${totalUserStories} Time Taken >> ${tat}`)
                                                        }); */
                                                    })
                                                })
                                            } else {
                                                console.log(currentTime(), " >> ", "User Stories are not defined!!!")
                                            }
                                        });

                                        })
                                        .catch(error => {
                                            console.log(currentTime(), " >> ", error);
                                        });
                                }));

                            });
                        });
                        });
                 //   });
              //  });
            }).catch(error => {
                console.log(currentTime(), CONS.jira, error);
               reject("Invalid credntials ");
            })
        } catch (error) {
            //logger.error(CONS.userStory, " >> UploadUserStoryForProjects", error)
         console.log(currentTime(), " >> UploadUserStoryForProjects", error)
            reject("UploadUserStoryForProjects", error)
        }

    })

}

var totalComponents = 0;
const UploadProjectComponents = (runType = API.automatedRun) => {
    return new Promise((resolve, reject) => {
        try {
            validateCredentials().then(response => {
                var projectKeyList = [];
                var startDate = moment(new Date(), 'YYYY-M-DD HH:mm:ss')
                console.log(currentTime(), " >> ", "Started Project Components >>", new Date());
                DATA_MODELS.DataUploads.updateExecution(runType, API.components).then(updateResponse => {
                GetJIRAProjectKeyFromDB().then((result) => {
                   projectKeyList = (result);
                    if (projectKeyList.length > 0) {
                        GetComponentListByProjects(projectKeyList).then(response => {
                            let tat = null;
                            totalTimeTaken(startDate).then(response => {
                                tat = response;
                               console.log(currentTime(), " >> ", `Total Records Inserted ${totalComponents} ,  Time Taken >> ${tat} `);
                                DATA_MODELS.DataUploads.updateExecution(runType, API.components).then(updateResponse => {
                                    DATA_MODELS.DataUploads.getAllData().then(uploadsData => {
                                        return resolve([`Total Components Inserted ${totalComponents} Time Taken >> ${tat}`, uploadsData])
                                    });
                                });

                            })
                        }).catch(error => {
                            console.log("in origin catch ", error)
                        })
                    } else {
                        console.log(currentTime(), " >> ", "Project Keys are not defined!!!")
                    }

                }).catch(error => {
                    console.log(currentTime(), " >> ", error);
                    reject("GetJIRAProjectKeyFromDB", error);
                });
            });
            }).catch(error => {
                console.log(currentTime(), CONS.jira, error);
                reject("Invalid credntials ");
            })

        } catch (error) {
            console.log(currentTime(), " >>  UploadJiraDefectsForProjects", error)
            reject("UploadJiraDefectsForProjects", error)
        }
    }).catch(error => {
        return new Error(error);
    })

}

module.exports.UploadJiraDefectsForProjects = UploadJiraDefectsForProjects;

module.exports.UploadJiraUserStories = UploadJiraUserStories;

module.exports.UploadProjectComponents = UploadProjectComponents;

module.exports.CleanUpDefects=CleanUpDefects;

module.exports.CleanUpUserStories = CleanUpUserStories

module.exports.UpdateDefectFixVersion =UpdateDefectFixVersion;

module.exports.UpdateEpicLinkinTestExecutionandRequirement =UpdateEpicLinkinTestExecutionandRequirement;





