const JIRA = require("../../contants/comarch-jira-custom-fields");

const GET = require("../../utils/comarch-jira-details")

const sql = require("../../models/db");

const { totalTimeTaken } = require("../../utils/data-utils")
const { currentTime } = require("../../utils/date-utils")
const API = require("../../contants/db-uploads-api");
const MappingTable = require("../comarch-import");

const fetch = require("node-fetch");
const https = require("https");

const config = require("../../configs/comarch-jira.config")

const baseUrl = config.baseURL;
const epicUrl = config.epicURL;

const base64 = require('base-64');
const DATA_MODELS = require("../../models/dairy-data.model")
const CONS = require("../../contants/commons");

const httpsAgent = new https.Agent({
    rejectUnauthorized :false,
    });


async function validateCredentials(userData) {
    let path = baseUrl + "/myself";
    return new Promise((resolve, reject) => {
        fetch(path, {
            method: 'GET',
            credentials: 'same-origin',
            redirect: 'follow',
            agent: httpsAgent,
            headers: {
                'Authorization': 'Basic ' + base64.encode(userData.ComarchuserName + ":" + userData.Comarchpassword),
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        }).then(response => {
            // console.log("Authenticated",response.ok)
            if (response.ok) {
                return resolve("Success");
            } else {
                console.log(" erro")
                return resolve(" Invalid Credentials");
                // reject((CONS.jira, " >> ", response.status + "  ", response.statusText, " Invalid Credentials"));
            }
        }).catch(error => {
            return resolve(" Invalid Credentials");
            return error;
        })
    })
}

async function getRequest(path,userData) {
    return await fetch(path, {
        method: 'GET',
        credentials: 'same-origin',
        agent: httpsAgent,
        redirect: 'follow',
        headers: {
            'Authorization': 'Basic ' + base64.encode(userData.ComarchuserName + ":" + userData.Comarchpassword),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    }).then(response => {
        if (response.ok) {
            return response.json();
        } else {
            return "error  " + response.status + response.statusText
        }
    }).catch(error => {
        return error
    })
}


async function getJiraEpicSummary(epicLink,userData) {
    let path = epicUrl + epicLink;
    return getRequest(path,userData);
}


async function getTotalDefectsByProjectsService(releaseName,userData) {
    let defects = null;
    if (releaseName === "upcomingRelease" || releaseName === undefined)
        defects = `search?jql=issuetype in ("Internal Bug","External Bug","Test Scenario") AND created > startOfMonth(-12) ORDER BY created DESC `
    else
        defects = `search?jql=issuetype in ("Internal Bug","External Bug","Test Scenario")AND fixVersion=${releaseName} `
    console.log(currentTime(), " >> ", defects)
    let path = baseUrl + defects
    let result = await getRequest(path,userData)
    result = (result["total"])
    return result;

}

async function getDefectsByProjectsService(releaseName, startsAt,userData) {
    let defects = null;
    if (releaseName === "upcomingRelease" || releaseName === undefined)
        defects = `search?jql=issuetype in ("Internal Bug","External Bug","Test Scenario") AND created > startOfMonth(-12) ORDER BY created DESC &startAt=${startsAt}`
    else
        defects = `search?jql=issuetype in ("Internal Bug","External Bug","Test Scenario") AND fixVersion=${releaseName} AND status != Closed&startAt=${startsAt}`
    let path = baseUrl + defects
    return getRequest(path,userData);
}

async function getTotalStoryDefectsService(jqlStoryKeyIn, releaseName, count = 0) {
    let story = `search?jql=key in (${jqlStoryKeyIn.toString()})`; // AND fixVersion=${releaseName}`
    let path = baseUrl + story
    let result = await getRequest(path)
    result = (result["total"])
    return result;
}

async function getStoryDefectsService(jqlStoryKeyIn, releaseName, startsAt, count = 0) {
    let story = `search?jql=key in (${jqlStoryKeyIn.toString()})&startAt=${startsAt}`
    let path = baseUrl + story
    return getRequest(path);
}

async function getDefectsByIdService(defectKeyList) {
    let defectsByIdServ = `search?jql=(id in (${defectKeyList})) `
    let path = baseUrl + defectsByIdServ;
    return await getRequest(path)
}


function insertStoryDefects(story) {
    return new Promise((resolve, reject) => {
        try {
            setTimeout(() => {
                let list = story[JIRA.issues];
                let i = 0;
                for (var key in list) {
                    let results = GET.getStoryDefectDetailsByRow(list, key);
                    if (results[2] != null) {
                        insertOrUpdateStoryDefectIntoDB(results[0], results[1], results[2]).then((fulfilled) => {
                            i++;
                            if (counter()) {
                                return resolve(`Defect Stories Inserted > ${i} `);
                            }
                        })
                    }
                }
                function counter() {
                    if (i == list.length) {
                        return true
                    } else
                        return false;
                }
            }, 1000)
        }
        catch (error) {
            console.log(currentTime(), " >> ", "Error on insertOrUpdateStory >> ", error.message);
            reject(error.message)
        }
    });
}

function insertOrUpdateStoryDefectIntoDB(dataList, keyList, storyKey) {
    return new Promise((resolve, reject) => {
        try {

            let insertPost = Object.assign(dataList, keyList);
            let updatePost = Object.assign(dataList);

            let insertSet = `INSERT INTO story_defect_details  SET ?`;

            let updateSet = `UPDATE story_defect_details SET ?, 
            updatedOn=current_timestamp()
            WHERE storyID ='${keyList["storyID"]}'  and storyKey ='${keyList["storyKey"]}' ; `;

            setTimeout(() => {
                var insertQry = sql.query(insertSet, insertPost, (err, res) => {
                    if (err) {
                        if (err.errno === 1062) {
                            var updateQry = sql.query(updateSet, updatePost, (error, res) => {
                                if (error) {
                                    console.log(currentTime(), " >> ", "Error on updation ", error.message)
                                    reject(error.message)
                                } else {
                                    resolve("Updated");
                                }
                            });
                        } else {
                            console.log(" insertOrUpdateStoryDefectIntoDB  ", err)
                            reject(err.message)
                        }
                    } else {
                        resolve("inserted");
                    }
                });
            }, 5000)
        } catch (error) {
            console.log(currentTime(), " >> ", "Error on insertOrUpdateStory >> ", error.message);
            reject(error.message)
        }
    });
}


let defectsList = [];

function insertOrUpdateDefectsIntoDB(dataList, keyList, defectKey) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            let insertSet = `INSERT INTO comarch_defects SET ?`
            let insertList = Object.assign(dataList, keyList);
            let updateSet = `update comarch_defects set ?,
            updatedOn=current_timestamp()  WHERE
            defectId= '${keyList["defectId"]}' and 
            defectKey= '${keyList["defectKey"]}';`;

            var insertQry = sql.query(insertSet, insertList, (err, res) => {
                if (err) {
                    if (err.errno === 1062) {
                        var updateQry = sql.query(updateSet, dataList, (error, res) => {
                            if (error) {
                                console.log(currentTime(), " >> ", "Error on updation ", error.message)
                                reject(error.message)
                            } else {
                                defectsList.push(`"${defectKey}"`);
                                resolve("Updated");
                            }
                        });
                    } else {
                        reject(err.message)
                    }
                } else {
                    defectsList.push(`"${defectKey}"`);
                    resolve("inserted");
                }
            });
        }, 1000)
    });
}

function UpdateDefectsIntoDB(dataList, keyList) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            let updateSet = `update defect_details set ?,
            updatedOn=current_timestamp()  WHERE
            defectId= '${keyList["defectId"]}' and 
            defectKey= '${keyList["defectKey"]}';`;

            var updateQry = sql.query(updateSet, dataList, (error, res) => {
                if (error) {
                    console.log(currentTime(), " >> ", "Error on updation ", error.message)
                    reject(error.message)
                } else {
                    resolve("Updated");
                }
            });

        }, 1000)
    });
}

function InsertDefectEntries(defects) {
    return new Promise((resolve, reject) => {
        let list = defects[JIRA.issues];
        let i = 0;
        for (var key in list) {
            let results = GET.getDefectDetailsByRow(list, key);
            if (results[2] != null) {
                insertOrUpdateDefectsIntoDB(results[0], results[1], results[2]).then((fulfilled) => {
                    i++;
                    if (counter()) {
                        return resolve(`Defects Inserted > ${i} `);
                    }
                })
            }
        }
        function counter() {
            if (i == list.length) {
                return true
            } else
                return false;
        }
    });
}


function getParentStoryForDefectsFromDB(defectsList) {
    return new Promise((resolve, reject) => {
        let qry = `select distinct parentKey from comarch_defects where issueType='Story Defect' and parentKey is not null ;`;
        setTimeout(() => {
            sql.query(qry, (err, res) => {
                if (err) {
                    console.log(currentTime(), " >> ", err.message)
                    //logger.error(CONS.defects, " >> ", err.message)
                    reject(err.message)
                } else {
                    resolve(res);
                }
            });
        }, 1000)
    });
}


const UploadDefects = (defects) => {
    return new Promise((resolve, reject) => {
        try {
            InsertDefectEntries(defects)
                .then((fulfilled) => {
                    return resolve(fulfilled)
                })
                .catch(error => {
                    console.log(currentTime(), " >>  InsertDefectEntries", error);
                });
        } catch (error) {
            console.log(currentTime(), " >>  UploadDefects", error)
            return reject("UploadDefects ", error)
        }
    })
}


var totalDefects = 0;

const GetJiraProjectNamesList = (releaseName,userData) => {
    return new Promise((resolve, reject) => {
        try {
            GetTotalDefectsByProjects(releaseName,userData).then(response => {
                return resolve(response);
            })
        } catch (error) {
            console.log(currentTime(), " >> GetJiraProjectNamesList", error)
            return reject("GetJiraProjectNamesList", error)
        }

    })

}

let defects = null;
var totalStories = 0;
const GetTotalDefectsByProjects = (releaseName,userData) => {
    return new Promise((resolve, reject) => {
        try {
                getTotalDefectsByProjectsService(releaseName,userData)
                    .then((response) => {
                        let total = response;
                        totalDefects = total
                        console.log(currentTime(), " >> ", "Total Defects  ", total)
                        let startsAt = 0;
                        let totCounter = Math.round(total / 50)
                        let counter = 1;
                        console.log('Defects insertion in progress  ',)
                        for (startsAt = 0; startsAt < total; startsAt = startsAt + 50) {
                            GetDefectsByProjects(releaseName, startsAt,userData).then(response => {
                            //  process.stdout.write("..");
                                counter++
                                if (counter == totCounter) {
                                    console.log("\r\n") 
                                    console.log(currentTime(), " >> ", "Total Defects Inserted ", total)
                                    return resolve("Total Defects Inserted ", total)
                                }
                            })

                        }
                    })
                    .catch(error => {
                        console.log(currentTime(), " >> ", error);
                    });
        } catch (error) {
            console.log(currentTime(), " >> GetTotalDefectsByProjects", error)
            reject("GetTotalDefectsByProjects", error)
        }
    });
}

let userStories = null;


const GetDefectsByProjects = (releaseName, startsAt,userData) => {
    return new Promise((resolve, reject) => {
        try {
            getDefectsByProjectsService(releaseName, startsAt,userData)
                .then((response) => {
                    defects = response;
                    return resolve(UploadDefects(defects, releaseName));
                })
                .catch(error => {
                    console.log(error);
                });
        } catch (error) {
            reject("GetDefectsByProjects", error)
        }
    })
}


let storyList = null;
const GetTotalStoryDefects = (jqlStoryKeyIn, releaseName) => {
    return new Promise((resolve, reject) => {
        try {
            getTotalStoryDefectsService(jqlStoryKeyIn, releaseName)
                .then((response) => {
                    let total = response;
                    totalStories = totalStories + total;
                    let startsAt = 0;
                    for (startsAt = 0; startsAt < total; startsAt = startsAt + 50) {
                        GetStoryDefects(jqlStoryKeyIn, releaseName, startsAt).then(response => {
                            resolve(response)
                        })
                    }
                })
                .catch(error => {
                    console.log(currentTime(), " >> ", error);
                });
        } catch (error) {
            reject("GetTotalStoriesForDefects", error)
        }
    })
}

const UploadStoryDefects = (storyList) => {
    return new Promise((resolve, reject) => {
        try {
            insertStoryDefects(storyList)
                .then((fulfilled) => {
                    return resolve(fulfilled)
                })
                .catch(error => {
                    console.log(currentTime(), " >> ", error);
                });
        } catch (error) {
            reject("UploadStories", error)
        }
    })
}


const GetStoryDefects = (jqlStoryKeyIn, releaseName, startsAt) => {
    return new Promise((resolve, reject) => {
        try {
            getStoryDefectsService(jqlStoryKeyIn, releaseName, startsAt)
                .then((response) => {
                    storyList = response;
                    UploadStoryDefects(storyList).then(response => {
                        return resolve(response)
                    })
                })
                .catch(error => {
                    console.log(currentTime(), " >> ", error);
                });
        } catch (error) {
            return reject("GetStoriesForDefects", error)
        }
    })
}

const GetParentStoryDefectsForDefects = (defectsList) => {
    return new Promise((resolve, reject) => {
        try {
            getParentStoryForDefectsFromDB(defectsList)
                .then((results) => {
                    let parentStoryDefectList = new Set();
                    Object.keys(results).forEach(function (key) {
                        parentStoryDefectList.add(`'${results[key]["parentKey"]}'`);
                    });
                    let jqlStoryKeyIn = Array.from(parentStoryDefectList);
                    if (jqlStoryKeyIn.length !== 0) {
                        GetTotalStoryDefects(jqlStoryKeyIn).then(response => {
                            resolve(response)
                        })
                    }
                    else {
                        console.log(currentTime(), " >> ", "ParentKey Not found for any of the defects")
                        resolve("ParentKey Not found for any of the defects")
                    }
                })
                .catch(error => {
                    console.log(currentTime(), " >> ", error);
                });
        } catch (error) {
            reject("GetParentStoryForDefects", error)
        }
    })
}


const UpdateDefectsBeforeInsertion = (recordsToUpdate) => {
    return new Promise((resolve, reject) => {
        let total = recordsToUpdate.length;
        let totCounter = Math.round(total / 50)
        let count = 0;
        console.log('Defects Updation in progress')
        for (startsAt = 0; startsAt < total; startsAt = startsAt + 50) {
           // process.stdout.write(`.`);
            let defectKey = recordsToUpdate.slice(startsAt, (startsAt + 50));
            getDefectsByIdService(defectKey.toString()).then(response => {
                let list = response[JIRA.issues];
                let i = 0;
               // process.stdout.write(`#`);
                for (var key in list) {
                    let results = GET.getDefectDetailsByRow(list, key);
                    if (results[1] != null) {
                        let dataList = null, keyList = null;
                        dataList = results[0];
                        keyList = results[1]
                        let updatePost = Object.assign(dataList)
                        UpdateDefectsIntoDB(updatePost, keyList).then((fulfilled) => {
                            i++;
                            if (counter()) {
                                console.log('\r\n')
                                return resolve(`Defects Updated > ${total} `);
                            }
                        })
                    }
                    function counter() {
                        if (i == list.length) {
                            return true
                        } else
                            return false;
                    }
                }
            });
        }
    });
};

const CleanUpDefects = () => {
    return new Promise((resolve, reject) => {
        try {
            var startDate = moment(new Date(), 'YYYY-M-DD HH:mm:ss')
            console.log(currentTime(), " >> ", "Started DEFECTS CleanUp >>", new Date(), );
            let lastRecordIdB4Insertion = null;
            validateCredentials().then(response => {
                console.log("Valid Credtentials JIRA   >>>" , response["name"] , response["active"] )
                DeleteDefectsBeforeInsertion().then(data => {
                    let recordsToupdate = data[2];
                   UpdateDefectsBeforeInsertion(recordsToupdate).then(response => {
                        console.log(currentTime(), " >> ", response)
                        resolve("Defects Cleanup Completed");
                   });
                });  
            }).catch(error => {
                console.log(currentTime(), CONS.jira, error);
                reject("Invalid credntials ");
            })
        }catch(error){
            console.log(currentTime()," >> CleanUpDefects " , error)
            reject("CleanUpDefects ", error)
        }
    }).catch(error => {
        reject(error)

    })

}

const UpdateComarchDefectFixVersion =(userData) => {
    return new Promise((resolve,reject)=>{
        try{
            console.log("Defect Fix Versions -> Updations")
                    UpdateDefectEpicLinkList(userData).then(response=>{
                        resolve(response)
                    });
        }catch(error){
            console.log(currentTime(), "UpdateDefect Fix Versions ", error)
            reject("UpdatefixVersions", error)

        }
    }).catch(error=>{
        reject(error)
    })
                                                    

}



const UpdateDefectEpicLinkList =(userData) => {
    return new Promise((resolve, reject) => {
        let  qry = `select  distinct epicLink  from comarch_defects WHERE epicLink IS NOT NULL;`;
            sql.query(qry, (err, res) => {
                if (err) {
                    reject(err.message)
                } else {
                    let epicData = JSON.parse(JSON.stringify(res));
                    updateEpicSummaryinDB(epicData,userData).then(response=>{
                        return resolve(response);
                    });
                                               
                }
            });
    }).catch(error=>{
        reject(error)
    });                                                    
}


function updateEpicSummaryinDB(epicData,userData) {
    return new Promise((resolve, reject) => {
            for(let i= 0; i<epicData.length;i++){  
                let epic = epicData[i];              
                getJiraEpicSummary(epic.epicLink,userData).then(data => {
                    let summary = formatKeyMessage(data.summary)
                    let qry = `update comarch_defects set epicName= '${summary}' where epicLink =  '${data.key}';`
                        sql.query(qry, (err, res) => {
                            if (err) {
                                return reject("Error", err.message)
                            }
                            else {                           
                                if(i === epicData.length-1){

                                    return resolve("Epic Summary Updated")
                                }
                            }
                        });
            }) ;  
            }
    }).catch(error=>{
        reject(error)
    });
}


function formatKeyMessage(str) {
    if (str === undefined)
    return null;
else {
    let string  = str.replace(/'/g, "''")
    return string;
}
}

const DeleteDefectsBeforeInsertion = () => {
    return new Promise((resolve, reject) => {
        DATA_MODELS.Defects.getAllRecordsDefectDetails().then(response => {
            if (response === null) {
                return resolve("No Records are available to delete")
            }
            else {
                let ids = response[0];
                let defectKey = response[1];
                let total = defectKey.length;
                let totCounter = Math.round(total / 50)
                let count = 0;
                let recordsToDelete = [];
                let recordsToUpdate = [];
                console.log("Validation in progress")
                for (startsAt = 0; startsAt < total; startsAt = startsAt + 50) {
                    let defKey = defectKey.slice(startsAt, (startsAt + 50));
                   // process.stdout.write(`.`);
                    getDefectsByIdService(defKey.toString()).then(response => {
                       // process.stdout.write(`#`);
                        let list = response[JIRA.issues];
                        let i = 0;
                        for (var key in list) {
                            let results = GET.getDefectDataById(list, key);
                                let toBeRemoved = results[1];
                                if (!toBeRemoved) {
                                    i++;
                                    recordsToUpdate.push(results[0]);
                                } else if (toBeRemoved) {
                                    recordsToDelete.push(results[0]);
                                    i++;
                                }
                                if (counter()) {
                                    count++
                                    if (count == totCounter) {
                                        console.log('\r\n')
                                        DATA_MODELS.Defects.deleteDefectsByDefectKey(recordsToDelete).then(deleteResponse => {
                                            console.log(currentTime(), " >> ", "Delete response ", deleteResponse);
                                            return resolve(["Records CleanUp Before Insertion", recordsToDelete.toString(), recordsToUpdate]);
                                        })

                                    }
                                }
                                function counter() {
                                    if (i == list.length) {
                                        return true
                                    } else
                                        return false;
                                }
                           // });
                        }
                    });
                }
            }

        })
    });
};


var moment = require('moment');
const { Console } = require("console");
const { reject } = require("underscore");
const UploadJiraDefectsForProjects = (userData, releaseName) => {
    let runType = false;
    return new Promise((resolve, reject) => {
        try {
            var startDate = moment(new Date(), 'YYYY-M-DD HH:mm:ss')
            console.log(currentTime(), " >> ", "Started DEFECTS >>", new Date());
        
            let lastRecordIdB4Insertion = null;

            validateCredentials(userData).then(response => {
                if(response.includes("Success")){
                                DATA_MODELS.ComarchDefects.getLstRecordCountIdComarchDefectDetails().then(recordId => {
                    lastRecordIdB4Insertion = recordId;
                    console.log("Last Record before insertion ", lastRecordIdB4Insertion)
                    DATA_MODELS.DataUploads.updateExecution(runType, API.comarchDefects).then(updateResponse => {
                
                GetJiraProjectNamesList(releaseName,userData).then(response => {
                    let tat = null;
                    if (defectsList.length != 0) {
                        // GetParentStoryDefectsForDefects(defectsList).then(response => {
                            // UpdateDefctFixVersions().then(response => {
                                totalTimeTaken(startDate).then(response => {
                                    tat = response;
                                    console.log(currentTime(), " >> ", `Total Records Inserted ${totalDefects} ,  Time Taken >> ${tat} `);
                                    DATA_MODELS.ComarchDefects.getUnaffectedRecordsComarchDefectDetails(lastRecordIdB4Insertion).then(deletionResponse => {
                                        console.log(currentTime(), " >> ", 'Total record cleaned up from DB ', deletionResponse);
                                        DATA_MODELS.DataUploads.updateExecution(runType, API.comarchDefects).then(updateResponse => {
                                            MappingTable.UploadCCBMappingTable().then(mappingData => {
                                                UpdateComarchDefectFixVersion(userData).then(epicResponses => {
                                                    DATA_MODELS.DataUploads.getAllData().then(uploadsData => {
                                                       return resolve([`Total Defects Inserted ${totalDefects} ,Total Stories Inserted ${totalStories}, Time Taken >> ${tat}`, uploadsData]);
        
                                                    });
                                                })

                                            })
                                           
                                        });


                                    });

                                });

                            // })

                        // });


                    }
                    
                });
                   }).catch(error => {
                console.log(currentTime(), CONS.jira, error);
                reject("Invalid credntials ");
            })
        });

                }else{
                    console.log("Returning error")
                    return resolve(response)
                }
    
    });
        } catch (error) {
            console.log(currentTime(), " >>  UploadJiraDefectsForProjects", error)
            reject("UploadJiraDefectsForProjects", error)
        }

    }).catch(error => {
        reject(error)

    })

}




module.exports.UploadJiraDefectsForProjects = UploadJiraDefectsForProjects;

module.exports.CleanUpDefects=CleanUpDefects;

module.exports.UpdateComarchDefectFixVersion =UpdateComarchDefectFixVersion;






