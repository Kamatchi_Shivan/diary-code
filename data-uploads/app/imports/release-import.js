var httpntlm = require('httpntlm');
var spauth = require('node-sp-auth');
var requestprom = require('request-promise');
var moment = require('moment');
const DATA_MODEL = require("../models/dairy-data.model");
const DATA_UTILS = require("../utils/data-utils");
const config = require("../configs/sharepoint")
const XML = require("../contants/sp-ccb-release");
const API = require("../contants/db-uploads-api");
const CONS = require("../contants/commons")

const { getDate, getDateTime, currentTime, convertUTCDateToLocalDate, formatDate, getDateTimeFrmNumber } = require("../utils/date-utils")

/*
const log4js = require("log4js");

log4js.configure({
    appenders: { dataUploads: { type: "file", filename: "data-uploads.log" } },
    categories: { default: { appenders: ["dataUploads"], level: "all" } },
});

const //logger = log4js.get//logger("dataUploads");
*/

module.exports.UploadCCBRelease = function (runType) {
    let insertCounter = 0, updateCounter = 0;
    return new Promise((resolve, reject) => {
        try {
            let lastRecordIdB4Insertion = null;
            var startDate = moment(new Date(), 'YYYY-M-DD HH:mm:ss')
            DATA_MODEL.ReleaseMileStones.getLstRecordCountIdReleaseMilestone().then(recordId => {
                lastRecordIdB4Insertion = recordId;
                spauth.getAuth(config.authURL, {
                    clientId: config.clientId,
                    clientSecret: config.clientSecret
                }).then(function (options) {
                    // Access Token will be available on the options.headers variable
                    var headers = options.headers;
                    headers['Accept'] = 'application/json;odata=verbose';
                    requestprom.get({
                        url: config.releaseCCB_URL,
                        headers: headers,
                        json: true
                    }).then(function (listresponse) {
                        var jsonObj = listresponse;
                        for (key in jsonObj) {
                            let data = jsonObj[key]
                            let entry = data["results"]
                            let counter = 0;

                            for (i in entry) {
                                let content = entry[i]
                                var dataList = {
                                    ReleaseName: format(content[XML.Title]),
                                    ReleaseID: format(content[XML.Id])
                                }

                                let insertPost = Object.assign(dataList)

                                let updatePost = Object.assign(dataList)

                                let ReleaseId = format(content[XML.Id]);

                                DATA_MODEL.ReleaseMileStones.insertPrep(
                                    insertPost, updatePost, ReleaseId).then(fullfilled => {
                                        counter++;
                                        if (fullfilled.includes("Update")) {
                                            updateCounter++;
                                        } else {
                                            insertCounter++;
                                        }
                                        if (counter === (entry.length)) {
                                            DATA_UTILS.totalTimeTaken(startDate).then(response => {
                                                //logger.info(CONS.release, " >> ", ` Total ${counter} records, inserted >> ${insertCounter} updated >>${updateCounter}`)

                                                console.log(currentTime(), " >> ", ` Total ${counter} records, inserted >> ${insertCounter} updated >>${updateCounter}`)
                                                DATA_MODEL.ReleaseMileStones.getUnaffectedRecordsRelease(lastRecordIdB4Insertion).then(results => {
                                                    console.log(currentTime(), " >> Release records deleted ", results);
                                                    //logger.info(CONS.release, " >> Release records deleted ", results);

                                                    DATA_MODEL.DataUploads.updateExecution(runType, API.release).then(updateResponse => {
                                                        DATA_MODEL.DataUploads.getAllData().then(data => {
                                                            return resolve([` Total ${counter} records, inserted >> ${insertCounter} updated >> ${updateCounter} , Total Time Taken ${response}`, data]);
                                                        });
                                                    });
                                                });
                                            });

                                        }
                                    });
                            }
                        }
                     })
                           

                });

            }).catch(error => {
                //logger.error(CONS.release, " >> Error UploadCCBRelease ", error);
                console.log(currentTime(), " >>  UploadCCBRelease", error)
                reject("UploadCCBRelease", error)
            });

        } catch (error) {
            console.log(currentTime(), " >>  UploadCCBRelease", error)
            //logger.error(CONS.release, " >> Error UploadCCBRelease ", error);
            reject("UploadCCBRelease", error)
        }
    }).catch(error => {
        //logger.error(error)
        return new Error(error);
    })

}
function dataList(dataMap) {
    return dataList;
}
function keyList(dataMap) {

    return keyList;
}



function format(str) {
    let string = DATA_UTILS.formatString(str)
    return string;
}



