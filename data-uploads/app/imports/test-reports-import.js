var httpntlm = require('httpntlm');
var moment = require('moment');
var spauth = require('node-sp-auth');
var requestprom = require('request-promise');
const DATA_MODEL = require("../models/dairy-data.model");
const DATA_UTILS = require("../utils/data-utils");
const config = require("../configs/sharepoint")
const XML = require("../contants/sp-ccb-test-reports");
const CONS = require("../contants/commons")

const { getDate, getDateTime, currentTime } = require("../utils/date-utils");
const { ItemID } = require('../contants/sp-ccb-test-reports');
const API = require("../contants/db-uploads-api");
/*
const log4js = require("log4js");

log4js.configure({
    appenders: { dataUploads: { type: "file", filename: "data-uploads.log" } },
    categories: { default: { appenders: ["dataUploads"], level: "all" } },
});

const //logger = log4js.get//logger("dataUploads");*/
async function getFileDetails(headers,URL) {
    return new Promise((resolve, reject) => {
        requestprom.get({
            url: URL,
            headers: headers,
            json: true
        }).then(function (listresponse) {
           var jsonObj = listresponse;
                    return resolve(jsonObj);

        });
    });
}


module.exports.UploadCCBTestDeliverables = function (runType) {
    return new Promise((resolve, reject) => {
        try {
            var startDate = moment(new Date(), 'YYYY-M-DD HH:mm:ss')
            spauth.getAuth(config.authURL, {
                clientId: config.clientId,
                clientSecret: config.clientSecret
            }).then(function (options) {
                var headers = options.headers;
                headers['Accept'] = 'application/json;odata=verbose';
                requestprom.get({
                    url: config.testReportsCCB_URL,
                    headers: headers,
                    json: true
                }).then(function (listresponse) {
                
                            var jsonObj = listresponse;
                            for (key in jsonObj) {
                                let data = jsonObj[key]
                                let entry = data["results"]
                                let counter = 0;
                                for (i in entry) {
                                    let content = entry[i];
                                    let ListItemAllFields = content[XML.ListItemAllFields][XML.deferred][XML.FileURI];
                                    getFileDetails(headers,ListItemAllFields).then(response => {
                                        let fData = response["d"];
                                        let relativeURL = config.testingCCB_URL +  content[XML.FileServerRelativeURL];
                                        var dataList = {
                                            Title: format(content[XML.Title]),
                                            ItemID: format(fData[XML.ItemID]),
                                            ReleaseName: format(fData[XML.ReleaseName]),
                                            TypeOfReport: format(fData[XML.TypeOfReport]),
                                            ProjectReference: format(fData[XML.ProjectReference]),
                                            ProjectName: format(fData[XML.ProjectName]),
                                            Created: format(fData[XML.Created]),
                                            Modified: format(fData[XML.Modified]),
                                            GUID: format(fData[XML.GUID]),
                                            FileName: format(content[XML.FileName]),
                                            FileServerRelativeURL: format(relativeURL),
                                        }
                                        //console.log(dataList)
                                        DATA_MODEL.TestReports.insertPrep(
                                            dataList,format(fData[XML.ItemID])).then(fullfilled => {
                                                counter++;
                                                if (counter === (entry.length)) {
                                                    DATA_UTILS.totalTimeTaken(startDate).then(response => {
                                                        console.log(currentTime(), " >> ", ` Total ${counter} records, inserted `)
                                                        //logger.info(CONS.testReports, " >> ", ` Total ${counter} records, inserted `)
                                                        DATA_MODEL.DataUploads.updateExecution(runType, API.testReports).then(updateResponse => {
                                                            DATA_MODEL.DataUploads.getAllData().then(data => {
                                                                return resolve([` Test Reports : Total ${counter} records inserted `, data]);
                                                            });
                                                        });
                                                    });
                                                }
                                            });
                                    })
                                }
                            }
                        });
                });
        } catch (error) {
            console.log(currentTime(), " >>  UploadtestReports", error)
            reject("UploadtestReports", error)
        }
    }).catch(error => {
        //logger.error(error)
        return new Error(error);
    })
}



function format(str) {
    let string = DATA_UTILS.formatString(str)
    return string;
}



