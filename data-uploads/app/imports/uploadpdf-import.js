const fs = require("fs");
var spauth = require('node-sp-auth');
var requestprom = require('request-promise');
var sprequest = require('sp-request');
const config = require("../configs/sharepoint");
const { PDFDocument, rgb, PDFName, PDFString, StandardFonts } = require("pdf-lib");
const credentialOptions = { clientId: config.clientId, clientSecret: config.clientSecret };

let spr = sprequest.create(credentialOptions);

module.exports.UploadPDF = function (data) {
  let filename = data.name + '.pdf';
  return new Promise((resolve, reject) => {
    try {
      pdfMerger(data).then(response => {
        if (response.includes("Success")) {
          // Remove header 
          spauth.getAuth(config.authURL, {
            clientId: config.clientId,
            clientSecret: config.clientSecret
          }).then(function (headeroptions) {
            // Access Token will be available on the options.headers variable
            var headers = headeroptions.headers;
            headers['content-type'] = 'application/json;odata=verbose';
            headers['Accept'] = 'application/json;odata=verbose';
            // Pull the SharePoint list items  
            var options = {
              method: 'POST',
              uri: config.uploadPdf + `add(url='${filename}',overwrite=true)`,
              __metadata: { "type": "SP.User" },
              formData: {
                // Like <input type="text" name="name">
                name: 'Test',
                // Like <input type="file" name="file">
                file: {
                  value: fs.createReadStream(`sharepointUpload/${filename}`),
                  options: {
                    filename: filename,
                    contentType: 'application/pdf'
                  }
                }
              },
              headers: headers,
            };
            requestprom(options)
              .then(function (body) {
                fs.unlinkSync(`sharepointUpload/${filename}`)
                let resp = JSON.parse(body);
                let itemURL = resp.d.ListItemAllFields.__deferred.uri;
                returingItemID(headers, itemURL).then(response => {
                  if (response !== undefined || response !== null) {
                    UpdateMetaDataFields(response, data.metaData).then(resp => {
                      if (resp.includes("Success")) {
                        return resolve("Success");
                      } else {
                        return resolve("Failure");
                      }
                    });
                  } else {
                    return resolve("Failure");
                  }
                });
                // POST succeeded...              

              })
              .catch(function (err) {
                return resolve("Failure");
                // POST failed...
              });
          });

        } else {
          return resolve("Failure");
        }

      });


    } catch (error) {
      //logger.error(CONS.projects, " >> Error UploadCCBProjects ", error);
      return resolve("Failure");
    }
  }).catch(error => {
    //logger.error(error)
    return resolve("Failure");
  })
}


const pdfMerger = (req) => {
  let filename = req.name + '.pdf';
  return new Promise((resolve, reject) => {
    try {
      (async () => {
        const initialPage = await PDFDocument.load(req.object);
        const cover = await PDFDocument.load(req.object1);
        const content = await PDFDocument.load(req.object2);
        const doc = await PDFDocument.create();
        const contentPages = await doc.copyPages(
          initialPage,
          initialPage.getPageIndices()
        );
        for (const page of contentPages) {
          doc.addPage(page);
        }

        const contentPages1 = await doc.copyPages(cover, cover.getPageIndices());
        for (const page of contentPages1) {
          doc.addPage(page);
        }
        const contentPages2 = await doc.copyPages(
          content,
          content.getPageIndices()
        );
        for (const page of contentPages2) {
          doc.addPage(page);
        }
        if (req.externalURL.length !== 0) {
          returingExternalFile(req.externalURL).then(async response => {
            if (response.length !== 0) {
              for (let a = 0; a < response.length; a++) {
                if (response[a].file.includes('pdf')) {
                  const aux = await PDFDocument.load(response[a].data);
                  if (a === 0) {
                    const firstPage = aux.getPage(0);
                    const { width, height } = firstPage.getSize();
                    // firstPage.moveTo(72, 800);
                    firstPage.drawText(`Annex: ${response[a].type}`, {
                      x: width / 2 - (response[a].type.length * 10),
                      y: height -50,
                      size: 12,
                    });
                  }
                  const auxpages = await doc.copyPages(
                    aux,
                    aux.getPageIndices()
                  );
                  for (const page of auxpages) {
                    doc.addPage(page);
                  }
                }
                else if (response[a].file.includes('JPG') || response[a].file.includes('PNG')) {
                  const page = doc.addPage();
                  let img;
                  if (response[a].file.includes('PNG')) {
                    img = await doc.embedPng(response[a].data);
                  } else {
                    img = await doc.embedJpg(response[a].data);
                  }
                  if (a === 0) {
                    page.moveTo(72, 800);
                    page.drawText(`Annex: ${response[a].type}`, {
                      size: 12,
                    });
                  }
                  const jpgDims = img.scale(0.5);
                  page.drawImage(img, {
                    x: page.getWidth() / 2 - jpgDims.width / 2,
                    y: page.getHeight() / 2 - jpgDims.height / 2,
                    width: jpgDims.width,
                    height: jpgDims.height,
                  });
                } else {
                  const helveticaFont = await doc.embedFont(StandardFonts.Helvetica);
                  /* (5) Create a page and it to the PDF document */
                  const page = doc.addPage();
                  /* (4) Create the link annotation object and ref */
                  const linkAnnotation = doc.context.obj({
                    Type: 'Annot',
                    Subtype: 'Link',
                    Rect: [145, page.getHeight() / 2 - 5, 358, page.getHeight() / 2 + 15],
                    Border: [0, 0, 2],
                    C: [0, 0, 1],
                    A: {
                      Type: 'Action',
                      S: 'URI',
                      URI: PDFString.of(response[a].url),
                    },
                  });
                  const linkAnnotationRef = doc.context.register(linkAnnotation);
                  /* (6) Draw some text for the link to be placed over */
                  page.drawText('Click here to view the External Report', {
                    x: 150,
                    y: page.getHeight() / 2,
                    font: helveticaFont,
                    size: 15,
                    color: rgb(0, 0, 1),
                  });
                  /* (7) Add the link to the page */
                  page.node.set(PDFName.of('Annots'), doc.context.obj([linkAnnotationRef]));
                  if (a === 0) {
                    page.moveTo(72, 800);
                    page.drawText(`Annex: ${response[a].type}`, {                      
                      size: 12,
                    });
                  }

                }
              }
            }
            try {
              let base64DataUri = await doc.saveAsBase64({ dataUri: true });
              let base64Image = base64DataUri.split(';base64,').pop();
              fs.writeFile(`sharepointUpload/${filename}`, base64Image, { encoding: 'base64' }, function (err) {
                return resolve("Success")
              });
            } catch {
              return resolve("Failed")
            }
          })
        } else {
          try {
            let base64DataUri = await doc.saveAsBase64({ dataUri: true });
            let base64Image = base64DataUri.split(';base64,').pop();
            fs.writeFile(`sharepointUpload/${filename}`, base64Image, { encoding: 'base64' }, function (err) {
              return resolve("Success")
            });
          } catch {
            return resolve("Failed")
          }
        }

      })();
    } catch {
      return resolve("Failed")
    }
  }).catch(error => {
    //logger.error(error)
    return resolve("Failed")
  })
};


const returingItemID = (headers, URL) => {
  return new Promise((resolve, reject) => {
    requestprom.get({
      url: URL,
      headers: headers,
      json: true
    }).then(function (listresponse) {
      return resolve(listresponse.d.ID);
    });
  }).catch(error => {
    return null;
  });
}

const UpdateMetaDataFields = (ID, metaData) => {
  return new Promise((resolve, reject) => {
    try {
      spr.requestDigest(config.authURL)
        .then(digest => {
          return spr.post(config.metaData + `Items(${ID})`, {
            body: {
              '__metadata': { 'type': 'SP.Data.CCB_x0020_DeliverablesItem' },
              "Releasemoment": metaData.Releasemoment,
              "Projectname": metaData.Projectname,
              "PlanviewID": metaData.PlanviewID,
              "Title": metaData.Releasemoment + "_" + metaData.PlanviewID + "_" + metaData.Projectname
            },
            headers: {
              'X-RequestDigest': digest,
              'X-HTTP-Method': 'MERGE',
              'IF-MATCH': '*',
              "content-type": "application/json;odata=verbose",
            }
          });
        })
        .then(response => {
          if (response.statusCode === 204) {
            return resolve("Success");
          }
        }, err => {
          if (err.statusCode === 404) {
            console.log('List not found!');
          } else {
            console.log(err);
          }
          return resolve("Failure");
        });


    } catch (error) {
      //logger.error(CONS.projects, " >> Error UploadCCBProjects ", error);
      return resolve("Failure");
    }
  }).catch(error => {
    return null;
  });
}

async function returingExternalFile(url) {
  return new Promise((resolve, reject) => {
    if (url.length !== 0) {
      spauth.getAuth(config.authURL, {
        clientId: config.clientId,
        clientSecret: config.clientSecret
      }).then(function (headeroptions) {
        // Access Token will be available on the options.headers variable
        let finalArray = [];
        var headers = headeroptions.headers;
        headers['content-type'] = 'application/json;odata=verbose';
        headers['Accept'] = 'application/json;odata=verbose';
        url.map(async (element) => {
          const urlArr = element.url.split("https://pvgroupbe.sharepoint.com");
          const URLConstruction = config.externalReport + urlArr[1] + "')" + config.endurl;
          let options = {
            method: "GET",
            uri: encodeURI(URLConstruction), //  OpenBinaryStream also works
            headers: headers,
            encoding: null
          };
          await requestprom(options).then(function (dataRetrived) {
            let obj = {
              file: urlArr[1],
              data: Buffer.from(dataRetrived).toString('base64'),
              url: element.url,
              type: element.type
            }
            finalArray = finalArray.concat(obj);
            if (url.length === finalArray.length) {
              return resolve(finalArray);
            }
          });
        }); 

      });

    } else {
      return [];
    }

  }).catch(error => {
    return null;
  });
}

const createPageLinkAnnotation = (page, uri) =>
  page.doc.context.register(
    page.doc.context.obj({
      Type: 'Annot',
      Subtype: 'Link',
      Rect: [0, 30, 40, 230],
      Border: [0, 0, 2],
      C: [0, 0, 1],
      A: {
        Type: 'Action',
        S: 'URI',
        URI: PDFString.of(uri),
      },
    }),
  );