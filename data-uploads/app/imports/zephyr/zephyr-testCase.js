const DATA_MODEL = require("../../models/dairy-data.model");

const ZR = require("../../contants/zephyr-fields");

const GET = require("../../utils/zr-test-details")

const sql = require("../../models/db");

const fetch = require("node-fetch");

const config = require("../../configs/zephyr-config")

const LogFs = require("../../contants/commons")

const baseUrl = config.baseURL;

const base64 = require('base-64');

const { currentTime } = require("../../utils/date-utils");
const { ZEPHYR_TestResults, ZEPHYR_Requirements } = require("../../models/dairy-data.model");
const { versionId } = require("../../contants/zephyr-fields");

async function getRequest(path) {
  return await fetch(path, {
    method: 'GET',
    credentials: 'same-origin',
    redirect: 'follow',
    headers: {
      'Authorization': 'Basic ' + base64.encode(config.username + ":" + config.password),
    },
    //timeout: 5000
  }).then(response => {
    if (response.ok) {
      return response.json();
    } else {
      return "error  " + response.status + response.statusText
    }
  }).catch(error => {
    return error
  })
}

async function getTCExecutionCycle(releaseid) {
  let path = baseUrl + `cycle/release/${releaseid}`;
  return await getRequest(path);
}

/* Get Cycle Phase ID Lists 
http://zephyr.pvgroup.intranet/flex/services/rest/v3/cycle/assignments/852?allexecutions=false */

async function getTCCylePhaseList(releaseid) {
  let path = baseUrl + `cycle/assignments/${releaseid}?allexecutions=true`;
  let response = await getRequest(path);
  return response["cyclePhase"];
}

/* Get Phase List Details 
http://zephyr.pvgroup.intranet/flex/services/rest/v3/assignmenttree/cyclephases

*/
async function getTCCyclePhaseListDetails(phaseIdList) {
  let path = baseUrl + `assignmenttree/cyclephases`;
  let body = { ids: phaseIdList };
  return await fetch(path, {
    method: "PUT",
    credentials: "same-origin",
    redirect: "follow",
    headers: {
      Authorization:
        "Basic " + base64.encode(config.username + ":" + config.password),
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
  }).then((response) => {
    if (response.ok) {
      return response.json();
    } else {
      return "error  " + response.status + response.statusText;
    }
  });
}


async function getRequirementTree(projectId, releaseid,) {
  let query = `requirementtree?projectId=${projectId}&releaseid=${releaseid}`
  let path = baseUrl + encodeURI(query);
  return await getRequest(path);
}

async function getReleaseSummary(releaseId) {
  let path = baseUrl + `summary/release/` + releaseId;
  return await getRequest(path)
}

async function getReleaseName(releaseid) {
  let path = baseUrl + `release/${releaseid}`
  let response = await getRequest(path)
  return response[ZR.name];

}

async function getProjectName(projectId) {
  let path = baseUrl + `project/${projectId}`
  let response = await getRequest(path)
  return response[ZR.name];
}

function getReleaseSummaryData(releaseId) {
  return new Promise((resolve, reject) => {
    getReleaseSummary(releaseId).then(summary => {
      var data = {
        mappedRequirementCount: summary["requirement"]["mappedRequirementCount"],
        unmappedRequirementCount: summary["requirement"]["unmappedRequirementCount"],
        totalRequirementCount: summary["requirement"]["unmappedRequirementCount"],
        totalTestcaseCount: summary["testcase"]["totalTestcaseCount"],
        totalCycleCount: summary["execution"]["totalCycleCount"],
        totalDefectCount: summary["execution"]["totalCycleCount"],
        totalDefectIds: summary["defect"]["totalDefectCount"],
      };

      var key = {
        releaseId: releaseId,
        projectId: summary["projectId"],
      }
      let insertPst = Object.assign(data, key)
      return resolve([data, insertPst, summary["projectId"]])

    })
  });
}

module.exports.insertTestResults = function insertTestResults(tests, releaseName, releaseId, projectName, lastRecordIdTestResults) {
  return new Promise((resolve, reject) => {

    try {
      let list = tests[ZR.results];
      getReleaseSummaryData(releaseId).then(summary => {
        let data = summary[0];
        let insertPst = summary[1];
        let projectId = summary[2]
          getTCExecutionCycle(releaseId).then((testCycle) => {
            getTCCylePhaseList(releaseId).then((cyclePhaseResponse) => {
              let phasesJson = null;
              getTCCyclePhaseListDetails(cyclePhaseResponse).then((phaseListResponse) => {
                phasesJson = phaseListResponse;
                let i = 0;
                let updateCounter = 0, insertCounter = 0;
                for (var key in list) {

                  GET.getTestExecutionDetailsByRow(list[key], releaseName, testCycle, phasesJson, key, projectId).then(results => {
                    if (results != null) {
                      let dataList = null, keyList = null;
                      dataList = results[0];
                      keyList = results[1]
                      let insertPost = Object.assign(dataList, keyList)
                      let updatePost = Object.assign(dataList)
                      insertOrUpdateTestResults(insertPost, updatePost, keyList).then((fulfilled) => {
                        i++;
                        if (fulfilled.includes("Update")) {
                          updateCounter++;
                        } else {
                          insertCounter++;
                        }
                        if (counter(i)) {
                             return resolve(`Inserted > ${insertCounter}  >> Updated > ${updateCounter} `);
                        }
                      })
                    }
                  }).catch(error => {
                    console.log(currentTime(), " >> ", "Error on getTestExecutionDetailsByRow >> ", error.message);
                    reject(error.message)
                  });
                }

                function counter(i) {
                  if (i == list.length) {
                    return true
                  } else
                    return false;
                }
              }).catch(error => {
                console.error(currentTime(), " >> ", "Err on >> getTCCyclePhaseListDetails")
                console.log(currentTime(), " >> ", error);
              });
            }).catch(error => {
              console.error(currentTime(), " >> ", "Err on >> getTCCylePhaseList")
              console.log(currentTime(), " >> ", error);
            });
          }).catch(error => {
            console.error(currentTime(), " >> ", "Err on >> getTCExecutionCycle")
            console.log(currentTime(), " >> ", error);
          });
        });
      

    } catch (error) {
      console.log(currentTime(), " >> ", "Error on insertTestResults >> ", error.message);
      reject(error.message)
    }
  })
}


function insertOrUpdateTestResults(insertPost, updatePost, keyObj) {
  return new Promise((resolve, reject) => {
    try {
      let deleteFlag = "false";

      let insertSet = `INSERT INTO test_execution_results SET ?  `

      let updateSet = `UPDATE test_execution_results
            SET ?, updatedOn=current_timestamp()  WHERE
               projectId ='${keyObj["projectId"]}'  AND 
               releaseId = '${keyObj["releaseId"]}' AND
               testCaseId = '${keyObj["testCaseId"]}' AND
            
               cyclePhaseId = '${keyObj["cyclePhaseId"]}'AND
               tcrCatalogTreeId = '${keyObj["tcrCatalogTreeId"]}' ;`

      var insertQry = sql.query(insertSet, insertPost, (err, res) => {
        // console.log(insertQry.sql)
        if (err) {
          if (err.errno === 1062) {
            var updateQry = sql.query(updateSet, updatePost, (error, result) => {
              if (error) {
                console.log(currentTime(), " >> ", "Error on updation ", error.message)
                reject(error.message)
              } else {
                //console.log(updateQry.sql)


                let rowCount = result.affectedRows;
                if (Number(rowCount) < 1) {
                  console.log(updateQry.sql)
                  console.log(currentTime(), " >> Row updation failed  ", `${result.affectedRows}   ${keyObj}`)
                }
                resolve(`Row Updated > Results Key: ${keyObj} `);
              }
            });
          } else {
            console.log(currentTime(), " >> ", err.message)
            reject(err.message)
          }
        } else {
          ///  console.log(`Row inserted Result Key${keyObj} `, { id: res.insertId });
          resolve(`Row inserted Result Key ${keyObj} `, { id: res.insertId });
        }
      });

    } catch (error) {
      console.log(currentTime(), " >> ", "Error on insertOrUpdateTestResults >> ", error.message);
      reject(error.message)
    }
  });
}

module.exports.insertRequirements = function insertRequirements(req, releaseName, projectName, projectId, releaseId, usersList, lastRecordIdRequirementResults) {
  return new Promise((resolve, reject) => {
    try {
      getReleaseSummaryData(releaseId).then(summary => {
        let data = summary[0];
        let insertPst = summary[1];

        getRequirementTree(projectId, releaseId).then(response => {
          let requirementTreeList = response;
          let list = req[ZR.results];
          let i = 0;
          let updateCounter = 0, insertCounter = 0;
          getProjectName(projectId).then(projectResponse => {
            if (projectName != projectResponse) {
              projectName = projectResponse;
            }
            getReleaseName(releaseId).then(releaseResponse => {
              if (releaseName != releaseResponse) {
                releaseName = releaseResponse;
              }

              for (var key in list) {
                GET.getRequirementsByRow(list[key], releaseName, projectName, requirementTreeList, usersList, releaseId).then(results => {
                  let dataList = null, keyList = null, key = null, insertPost = null, updatePost = null, reqWistTestList = null;
                  if (results != null) {
                    dataList = results[0];
                    keyList = results[1];

                    reqWistTestList = results[2];

                    let insertPost = Object.assign(dataList, keyList)
                    let updatePost = Object.assign(dataList)

                    insertOrUpdateRequirements(insertPost, updatePost, keyList, reqWistTestList).then((fulfilled) => {
                      let insertUpdateStatus = fulfilled;
                      if (insertUpdateStatus.includes("Update")) {
                        updateCounter++;
                      } else {
                        insertCounter++;
                      }
                      insertRequirementWiseTestDetails(keyList, reqWistTestList, keyList).then(fulfilledd => {
                        i++;

                        if (counter(i)) {
                          return resolve(`${releaseName} , Results Inserted > ${insertCounter}   >> updated >> ${updateCounter} `);

                        }
                      })


                    }).catch(error => {
                      console.log(currentTime(), " >> ", "Error on insertOrUpdateRequirements >> ", error);
                    });;
                  }
                }).catch(error => {
                  console.log(currentTime(), " >> ", "Error on getRequirementsByRow >> ", error.message);
                });;
              }
              function counter(i) {
                if (i == list.length) {
                  return true
                } else
                  return false;
              }

            }).catch(error => {
              console.log(" Error on getRequirementTree ", error)
            })
          })
        })
        // });
      });
    } catch (error) {
      console.log(currentTime(), " >> ", "Error on insertRequirements >> ", error);
      reject(error)
    }
  }).catch(error => {
    console.log(currentTime(), " >> ", "Error on insertRequirements >> ", error);
    reject(error)
  });
}

function insertRequirementWiseTestDetails(keyList, reqWiseTestCaseList, recordId) {
  return new Promise((resolve, reject) => {
    let counter = 0;
    for (let i = 0; i < reqWiseTestCaseList.length; i++) {
      let row = reqWiseTestCaseList[i];
      insertActualTestCAseForRequirements(keyList, row, recordId).then(result => {
        counter++;
        if (reqWiseTestCaseList.length === counter) {
          resolve('inserttest    ', keyList)
        }
      }).catch(error => {
        console.log(" INSERT ACTUAL ERRO ", error)

      })
    }
    if (reqWiseTestCaseList.length === 0) {
      resolve("No Test Cases mapped for Requirements");
    }

  })

}


function checkDataRequireWiseTestDetails(reqTestCaseId, parentId) {
  return new Promise((resolve, reject) => {

    if (reqTestCaseId === '') {
      reqTestCaseId = 0;
    }
    let selectQry = `select id from requirement_wise_test_details where
    parentReqId= ${parentId} AND
    reqTestCaseId = ${reqTestCaseId}; `

    sql.getConnection(function (err, connection) {
      connection.query('START TRANSACTION', function (err, rows) {
        var getRecordId = connection.query(selectQry, (error, res) => {
          if (error) {
            console.log(currentTime(), " >> ", "Error on get Record ", error.message)
            reject(error.message)
          } else {
            if (res.length > 0) {
              let recordID = res[0]["id"];
              return resolve(recordID)
            } else {
              return resolve(0)
            }
          }
        });
        connection.query('COMMIT', function (err, rows) {
          connection.release();
        });
      });

    });
  });
}

function insertActualTestCAseForRequirements(keyList, reqWiseTestData, parentId) {
  return new Promise((resolve, reject) => {
    try {

      let reqTestCaseId = reqWiseTestData["reqTestCaseId"];



      let insertPost = null;
      //  keyList["parentReqId"] = parentId

      // checkDataRequireWiseTestDetails(reqTestCaseId, parentId).then(reqTestDetailsResponse => {
      // console.log("TTTT ", parentId, reqTestCaseId, response);
      let insertTestDetailsSet = `INSERT INTO requirement_wise_test_details SET ? `;
      insertPost = { ...keyList, ...reqWiseTestData };
      var insertTetDetailsQry = sql.query(insertTestDetailsSet, insertPost, (error, res) => {
        if (error) {
          if (error.errno === 1062) {
            let updateTestDetailsSet = `UPDATE requirement_wise_test_details
              SET ?, updatedOn = current_timestamp()  WHERE 
              projectId = '${keyList["projectId"]}' AND
              releaseId = '${keyList["releaseId"]}' AND
              requirementTreeIds = '${keyList["requirementTreeIds"]}' AND
              requirementID = '${keyList["requirementID"]}' AND 
              reqTestCaseId='${reqTestCaseId}';     `;
            var updateTestDetailsQry = sql.query(updateTestDetailsSet, reqWiseTestData, (error, res) => {
              //  console.log(updateTestDetailsQry.sql)
              if (error) {
                console.log(currentTime(), " >> ", "Error on UpdatedTestDetail Set Test Details ", error.message)
                reject(error)
              } else {
                resolve(`Updated >>  requirementID = '${keyList["requirementID"]}' AND     reqTestCaseId='${reqTestCaseId}'`)
              }
            });
          } else {
            console.log("Error on insertion ", insertPost["requirementID"], error);
            reject(error)
          }

        } else {
          resolve("REcords inserted TESTDETAILS REQ", insertPost["requirementID"])
        }
      });
      /*
      insertRequirementsRecord(insertPost).then(response => {
        resolve("REcords inserted ", reqTestCaseId, response)
      }).catch(error => {
        console.log("Error on insert REquirements  detals");
        reject(error)
      })
   
      let recordID = reqTestDetailsResponse;
      updateRequirementsRecord(recordID, reqWiseTestData).then(response => {
        // console.log(" RESPONSE   ", updateRequirementsRecord, response)
        resolve(" Updated ", response)
      }).catch(error => {
        console.log("Error on Update REquirements  details  ", error);
        reject(error)
      })*/

      //}
      /*).catch(error => {
        console.log(currentTime(), " >> ", "Error on insertRequirementWiseTestDetails  DB INSERT  >> ", error.message);
        reject(error.message)
      });*/
    } catch (error) {
      console.log(error)
      reject(error)
    }
  });
}



function insertRequirementsRecord(insertPost) {
  return new Promise((resolve, reject) => {
    let insertTestDetailsSet = `INSERT INTO requirement_wise_test_details SET ? `;
    sql.getConnection(function (err, connection) {
      connection.query('START TRANSACTION', function (err, rows) {
        var insertTetDetailsQry = connection.query(insertTestDetailsSet, insertPost, (error, res) => {
          if (error) {
            console.log("insee ", insertTetDetailsQry.sql)
            console.log("Error on insertion ", insertPost["requirementID"], error);
            reject(error)
          } else {
            // console.log("TEST  INSERTED ", reqTestCaseId, keyList["requirementID"])
            resolve("REcords inserted TESTDETAILS REQ", insertPost["requirementID"])
          }
        });

        connection.query('COMMIT', function (err, rows) {
          connection.release();
        });
      });
    });

    /*   var insertTetDetailsQry = sql.query(insertTestDetailsSet, insertPost, (error, res) => {
         if (error) {
           console.log("Error on insertion ", insertPost["requirementID"]  , error);
           reject(error)
         } else {
           // console.log("TEST  INSERTED ", reqTestCaseId, keyList["requirementID"])
           resolve("REcords inserted TESTDETAILS REQ", insertPost["requirementID"])
         }
       });
   */
  });
}

function updateRequirementsRecord(recordID, reqWiseTestData) {
  return new Promise((resolve, reject) => {
    let updateTestDetailsSet = `UPDATE requirement_wise_test_details
    SET ?, updatedOn = current_timestamp()
    WHERE id=${recordID} `;
    sql.getConnection(function (err, connection) {
      connection.query('START TRANSACTION', function (err, rows) {
        var updateTestDetailsQry = connection.query(updateTestDetailsSet, reqWiseTestData, (error, res) => {
          //  console.log(updateTestDetailsQry.sql)
          if (error) {
            console.log(currentTime(), " >> ", "Error on UpdatedTestDetail Set Test Details ", error.message)
            reject(error)
          } else {
            // console.log(updateRequirementsRecord, recordID)
            resolve("updated >>", recordID)
          }
        });
        connection.query('COMMIT', function (err, rows) {
          connection.release();
        });
      });
    });
    /*  var updateTestDetailsQry = sql.query(updateTestDetailsSet, reqWiseTestData, (error, res) => {
          //console.log(updateTestDetailsQry.sql)
          if (error) {
            console.log(currentTime(), " >> ", "Error on UpdatedTestDetail Set Test Details ", error.message)
            reject(error)
          } else {
            //console.log("Record >> updateTestDetails  ", reqTestCaseId, keyList["requirementID"])
            resolve("updated >>", reqWiseTestData["requirementID"], ' >>  ')
          }
        }); */


  });
}

function checkDataRequirements(keyList) {
  return new Promise((resolve, reject) => {
    let selectQry = `select id from requirement_details where
    projectId = '${keyList["projectId"]}' AND
    releaseId = '${keyList["releaseId"]}' AND
    requirementTreeIds = '${keyList["requirementTreeIds"]}' AND
    requirementID = '${keyList["requirementID"]}' ; `

    sql.getConnection(function (err, connection) {
      connection.query('START TRANSACTION', function (err, rows) {
        var getRecordId = connection.query(selectQry, (error, res) => {
          if (error) {
            console.log(currentTime(), " >> ", "Error on get Record ", error.message)
            reject(error.message)
          } else {
            if (Number(keyList["projectId"]) == 170)
              console.log(getRecordId.sql)
            if (res.length > 0) {
              let recordID = res[0]["id"];
              return resolve(recordID)
            } else {
              return resolve(0)
            }
          }
        });
        connection.query('COMMIT', function (err, rows) {
          connection.release();
        });
      });
    });

    /* var getRecordId = sql.query(selectQry, (error, res) => {
       if (error) {
         console.log(currentTime(), " >> ", "Error on get Record ", error.message)
         reject(error.message)
       } else {
         if (res.length > 0) {
           let recordID = res[0]["id"];
           return resolve(recordID)
         } else {
           return resolve(0)
         }
       }
     }); */

  });
}

function insertOrUpdateRequirements(insertPost, updatePost, keyList, reqWistTestList) {
  return new Promise((resolve, reject) => {
    try {

      let insertSet = `INSERT INTO requirement_details SET ? `;

      let updateSet = `UPDATE requirement_details
          SET ?, updatedOn = current_timestamp()
          WHERE 
           projectId = '${keyList["projectId"]}' AND
          releaseId = '${keyList["releaseId"]}' AND
          requirementTreeIds = '${keyList["requirementTreeIds"]}' AND
          requirementID = '${keyList["requirementID"]}' ;`;

      var insertQry = sql.query(insertSet, insertPost, (err, res) => {
        if (err) {
          if (err.errno === 1062) {
            var updateQry = sql.query(updateSet, updatePost, (error, result) => {
              if (error) {
                console.log(updateQry.sql)
                console.log(currentTime(), " >> ", "Error on updation ", error.message)
                reject(error.message)
              } else {
                // console.log(updateQry.sql)


                let rowCount = result.affectedRows;
                if (Number(rowCount) < 1) {
                  console.log(updateQry.sql)
                  console.log(currentTime(), " >> Row updation failedRequirementd  ", `${result.affectedRows}   ${keyObj}`)
                } else {

                }
                resolve(`Row Updated > Results Key: ${keyList} `);
              }
            });
          } else {
            console.log(currentTime(), " >> Err on >>insertOrUpdateRequirements >> ", err.message)
            reject(err.message)
          }

        } else {
          resolve(`Row inserted Result Key ${keyList} `, { id: res.insertId });
        }
      });


    } catch (error) {
      console.log(currentTime(), " >> ", "Error on insertOrUpdateRequirements  DB INSERT  >> ", error.message);
      reject(error.message)
    }
  });
}

module.exports.insertReleaseSummary = function processReleaseSummary(releaseId) {
  return new Promise((resolve, reject) => {
    getReleaseSummary(releaseId).then(summary => {
      var dataList = {
        releaseId: releaseId,
        mappedRequirementCount: summary["requirement"]["mappedRequirementCount"],
        unmappedRequirementCount: summary["requirement"]["unmappedRequirementCount"],
        totalRequirementCount: summary["requirement"]["unmappedRequirementCount"],
        totalTestcaseCount: summary["testcase"]["totalTestcaseCount"],
        totalCycleCount: summary["execution"]["totalCycleCount"],
        totalDefectCount: summary["execution"]["totalCycleCount"],
        totalDefectIds: summary["defect"]["totalDefectCount"],
        projectId: summary["projectId"],
      };

      insertReleaseSummary(dataList, releaseId, summary["projectId"]).then(fulfilled => {
        i++;
        if (counter(i))
          return resolve(`Inserted > ${i} `);
      });
    });
  });
}

module.exports.deleteDuplicateTestResults = function deleteDuplicateTestResults() {
  return new Promise((resolve, reject) => {
    try {
      var fs = require('fs');
      fs.readFile(LogFs.testDeleteLog, 'utf8', function (err, data) {
        if (err) throw err;
        var deleteSet = data.replace("select id ", "delete ");
        
        var deleteQry = sql.query(deleteSet, (err, res) => {
          if (err) {
            if(err.errno===1065){
              console.log("Qyuery empty ")
              resolve(" No Query available ")
            }
            
            else{
              console.log(currentTime(), " >> ", "Error on delete Test Results  >>> ", err.message  , err.errno)
              reject(err.message)
            }
           
          } else {
            fs.writeFile(LogFs.testDeleteLog, '', function () { console.log(currentTime(), " >> ", "Records Deleted ", res.affectedRows) })
            resolve(`Records Deleted >> ${res.affectedRows} `)
          }

        });
      });
    } catch (error) {
      console.log(currentTime(), " >> ", "Error on deleteDuplicateTestResults >> ", error.message);
      reject(error.message)
    }
  });
}


module.exports.deleteDuplicateRequirements = function deleteDuplicateRequirements() {
  return new Promise((resolve, reject) => {
    try {
      var fs = require('fs');
      fs.readFile(LogFs.requirementsDeleteLog, 'utf8', function (err, data) {
        if (err) throw err;
        var deleteSet = data.replace("select id ", "delete ");
         var deleteQry = sql.query(deleteSet, (err, res) => {
          if (err) {
            if(err.errno===1065){
              console.log(" No Qyuery avaiable in log ")
              resolve(" No Query available ")
            }
            else{
              console.log(currentTime(), " >> ", "Error on delete Requirements ", err.message)
              reject(err.message)
            }
       
          } else {
            fs.writeFile(LogFs.requirementsDeleteLog, '', function () { console.log(currentTime(), " >> ", "Records Deleted ", res.affectedRows) })
            resolve(`Records Deleted >> ${res.affectedRows} `)
          }

        });
      });
    } catch (error) {
      console.log(currentTime(), " >> ", "Error on deleteDuplicateRequirements >> ", error.message);
      reject(error.message)
    }
  });
}


module.exports.ChkDeletedRecordsTestResults = function ChkDeletedRecordsTestResults(lastRecordIdB4Insertion, releaseNameList) {
  return new Promise((resolve, reject) => {

    try {
      let insertSCounter = 0, actualSCounter = 0;
      ZEPHYR_TestResults.getUnaffectedRecordsTestResults(lastRecordIdB4Insertion, releaseNameList).then(response => {
        console.log(currentTime(), "  >>  Deleted Records count >> ", response)
        return resolve(`Junk Records deleted  `);
      });
    
    } catch (error) {
      console.log(currentTime(), " >> ", "Error on CheckForDeletedRecords >> ", error.message);
      reject(error.message)
    }
  })
}



module.exports.ChkDeletedRecordsRequirements = function ChkDeletedRecordsRequirements(lastRecordIdB4Insertion, releaseNameList) {
  return new Promise((resolve, reject) => {

    try {
      let insertSCounter = 0, actualSCounter = 0;
      ZEPHYR_Requirements.getUnaffectedRecordsRequirements(lastRecordIdB4Insertion, releaseNameList).then(response => {
        console.log(currentTime(), ' REquirements Deleted Records count  "', response)
        return resolve(`Junk Records deleted  `);
      });
    } catch (error) {
      console.log(currentTime(), " >> ", "Error on CheckForDeletedRecords >> ", error.message);
      reject(error.message)
    }
  })
}
