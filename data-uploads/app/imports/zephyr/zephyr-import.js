const fetch = require("node-fetch");

const config = require("../../configs/zephyr-config")

const baseUrl = config.baseURL;

const base64 = require('base-64');

const { currentTime } = require("../../utils/date-utils")
const CONS = require("../../contants/commons")
/*
const log4js = require("log4js");


log4js.configure({
    appenders: { dataUploads: { type: "file", filename: "data-uploads.log" } },
    categories: { default: { appenders: ["dataUploads"], level: "all" } },
});

const logger = log4js.get//logger("dataUploads");

*/

async function getRequest(path) {
    return await fetch(path, {
        method: 'GET',
        credentials: 'same-origin',
        redirect: 'follow',
        headers: {
            'Authorization': 'Basic ' + base64.encode(config.username + ":" + config.password),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        //timeout: 5000
    }).then(response => {
        if (response.ok) {
            return response.json();
        } else {
            return "error  " + response.status + response.statusText
        }
    }).catch(error => {
        //logger.error(CONS.zephyr, ' >> ERROR ', error)   
        return error
    })
}

const API = require("../../contants/db-uploads-api");

const ZR = require("../../contants/zephyr-fields");

const { totalTimeTaken } = require("../../utils/data-utils")

const DATA_MODELS = require("../../models/dairy-data.model")

const sql = require("../../models/db");

const TestCase = require("./zephyr-testCase");

var moment = require('moment');
const { count, Console } = require("console");
const { project } = require("../../contants/jira-custom-fields");
const { TestResults } = require("../../models/dairy-data.model");

async function getReleaseInfo(count = 0) {
    let path = baseUrl + "release"
    return await getRequest(path);
}

async function getTestExecution(projectNamesIn, releaseidIn, firstreslt, maxResults, count = 0) {
    let query = `advancesearch/?firstresult=${firstreslt}&maxresults=${maxResults}&entitytype=execution&zql=true&order=testcaseId&word=releaseid in (${releaseidIn})`
    let path = baseUrl + encodeURI(query);
    // console.log("Printing path >>>>",path)
     return await getRequest(path);

}

async function getTotalTestExecutionList(projectNamesIn, releaseidIn, count = 0) {
    let query = `advancesearch/?entitytype=execution&zql=true&word=releaseid in (${releaseidIn})`
    let path = baseUrl + encodeURI(query);
    let response = await getRequest(path)
    return response[0]["resultSize"]
}

async function getRequirement(projectNamesIn, releaseidIn, maxResults, count = 0) {
    let query = `advancesearch/?firstresult=0&maxresults=${maxResults}&entitytype=requirement&zql=true&word=releaseid in (${releaseidIn})`
    let path = baseUrl + encodeURI(query);
    return await getRequest(path);
}

async function getTotalRequirementList(projectNamesIn, releaseidIn, count = 0) {
    let query = `advancesearch/?entitytype=requirement&zql=true&word=releaseid in (${releaseidIn})`
    let path = baseUrl + encodeURI(query)
    let response = await getRequest(path)
    return (response[0]["resultSize"]);
}

// http://zephyr.pvgroup.intranet/flex/services/rest/latest/testcase/pathbyrelease?testcaseid=286265&releaseid=794
// GEt Tracable Path

async function getReleaseName(releaseid) {
    let path = baseUrl + `release/${releaseid}`
    let response = await getRequest(path)
    return response[ZR.name];

}

async function getProjectName(projectId) {
    let path = baseUrl + `project/${projectId}`
    let response = await getRequest(path)
    return response[ZR.name];
}

async function getUsersList() {
    let path = baseUrl + `user`;
    return await getRequest(path)
}


function IterateReleaseIdList(result, releaseName) {
    try {
        var releaseVsProjectsMap = new Map;
        let releaseIdList = []
        Object.keys(result).forEach(function (key) {
            if (result[key]["status"] === 0) {
                if (result[key]["name"] === releaseName) {
                    releaseIdList.push(result[key]["id"]);
                    releaseVsProjectsMap.set(result[key]["projectId"], result[key]["id"])
                }
            }
        });
        return [releaseVsProjectsMap, releaseIdList];
    }
    catch (err) {
        console.log(currentTime(), " >> ", IterateReleaseIdList, err)
        return err;
    }
}



function getUpcomingReleaseFromDB() {
    return new Promise((resolve, reject) => {
        try {
            let qry = `select distinct ReleaseName from v_release_moments where ReleaseDate>= date_sub(curdate(),INTERVAL 90 DAY); `;
          
            console.log(currentTime(), " >> ", qry)
            sql.query(qry, (err, res) => {
                if (err) {
                    return reject(err.message)
                } else {
                    let releaseNameList = [];
                    Object.keys(res).forEach(function (key) {
                        let names = res[key]["ReleaseName"]
                        if (names !== null) {
                            releaseNameList.push(names)
                        }
                    });
                    return resolve(releaseNameList);
                }
            });
        } catch (error) {
             return reject("getUpcomingReleaseFromDB", error)
        }

    });
}

function getDistnictReleaseFromDB() {
    return new Promise((resolve, reject) => {
        try {
            let qry = `select distinct ReleaseName from v_release_moments; `;
          
            console.log(currentTime(), " >> ", qry)
            sql.query(qry, (err, res) => {
                if (err) {
                    return reject(err.message)
                } else {
                    let releaseNameList = [];
                    Object.keys(res).forEach(function (key) {
                        let names = res[key]["ReleaseName"]
                        if (names !== null) {
                            releaseNameList.push(names)
                        }
                    });
                    return resolve(releaseNameList);
                }
            });
        } catch (error) {
             return reject("getUpcomingReleaseFromDB", error)
        }

    });
}


function IterateReleaseNamesList(result) {
    return new Promise((resolve, reject) => {
        let releaseNameList = [];
        try {
            Object.keys(result).forEach(function (key) {
                let names = result[key]["ReleaseName"]
                if (names !== null) {
                    releaseNameList.push(names)
                }
            });
            return resolve(releaseNameList);
        } catch (error) {
            return reject(IterateReleaseNamesList, error)
        }
    })
}


function getTestResultsByRelease(releaseName) {
    return new Promise((resolve, reject) => {
        try {
           getReleaseInfo().then((result) => {
                let totalRecords = 0;
                let releaseData = IterateReleaseIdList(result, releaseName);
                let map = releaseData[0], loopSize = map.size, i = 0;
                if (map.size === 0) {
                    console.log(currentTime(), " >> ", `No Records found for ReleaseName ${releaseName} `);
                    return resolve(0);
                }

                for (var entry of map.entries()) {
                    var projectId = entry[0];
                    let releaseId = entry[1];
                    getProjectName(projectId).then(response => {
                        let releaseIdIn = releaseId,
                            projectNamesIn = `"${response}"`;
                        getReleaseName(releaseId).then(relName => {
                            getTotalTestExecutionList(projectNamesIn, releaseIdIn).then(response => {
                                if (response === undefined) {
                                    console.log(currentTime(), " >> ", projectNamesIn, releaseIdIn)
                                    response = 0
                                }
                                let maxSize = response;
                                totalRecords = totalRecords + maxSize
                                console.log(currentTime(), " >> ", "Total TestResults >> ", relName, " >> ", projectNamesIn, " >>", maxSize)
                                if (maxSize > 0) {
                                    let startsAt = 0;
                                    getTestExecution(projectNamesIn, releaseIdIn, startsAt, maxSize).then(response => {
                                        TestCase.insertTestResults(response[0], relName, releaseId, projectNamesIn, lastRecordIdTestResults).then(response => {
                                            console.log(currentTime(), "  >> ", relName, " >>", projectNamesIn, " >> ", response)
                                            i++;
                                            function counter(i) {
                                                if (i === map.size) {
                                                    releaseWiseTestsCount.push(releaseName + " >> " + totalRecords)
                                                    return true;
                                                }
                                            }
                                            if (counter(i)) {
                                                let tat = null;
                                                totalTimeTaken(startTimeTestResults).then(response => {
                                                    tat = response;
                                                   console.log(currentTime(), "  >> ", `Total Records Inserted  ${relName} >>   ${totalRecords} ,  Time Taken >> ${tat} `);
                                                    return resolve(totalRecords)
                                                })
                                            }
                                        }).catch(error => {
                                            console.error(currentTime(), " >> ", "Err on >> TestCase.insertTestResults")
                                            console.log(currentTime(), " >> ", error);
                                        });
                                    }).catch(error => {
                                       console.error(currentTime(), " >> ", "Err on >> getTestExecution ", error)
                                        reject(currentTime(), " >> ", "Err on >> getTestExecution ", error)
                                    });



                                } else {
                                    i++;
                                    if (counter(i)) {
                                        resolve(totalRecords)
                                        return (totalRecords)

                                    }
                                }
                                function counter(i) {

                                    if (i === map.size) {
                                        releaseWiseTestsCount.push(releaseName + " >> " + totalRecords)
                                        return true;
                                    }
                                }
                            }).catch(error => {
                                console.error(currentTime(), " >> ", "Err on >> getTotalTestExecutionList ", error)
                                reject(currentTime(), " >> ", "Err on >> getTotalTestExecutionList ", error)

                            });
                        }).catch(error => {
                           console.error(currentTime(), " >> ", "Err on >> getReleaseName", error)
                            reject((currentTime(), " >> ", "Err on >> getReleaseName", error))
                        })
                        // }
                    }).catch(error => {
                        console.error(currentTime(), " >> ", "Err on >> getProjectName ", error)
                        reject(currentTime(), " >> ", "Err on >> getProjectName ", error)
                    });
                }
            }).catch(error => {
                    console.error(currentTime(), " >> ", "Err on >> getReleaseInfo ", error)
                    reject(currentTime(), " >> ", "Err on >> getReleaseInfo ", error)

                });
        } catch (error) {
            reject(getTestResultsByRelease + error)
        }
    })
}

var releaseWiseTestsCount = [];
var releaseWiseReqCount = [];

var totalRequirementCount = 0;
var totalTestResultsCount = 0;

let reqReleaseWiseProjectMap = new Map();

function getRequirementsByRelease(releaseName, usersList, releaseList) {
    return new Promise((resolve, reject) => {
        try {
            let releaseData = IterateReleaseIdList(releaseList, releaseName);
            let map = releaseData[0], loopSize = map.size, i = 0;
            let totalRecords = 0;
            if (map.size === 0) {
                console.log(currentTime(), " >> ", `No Records found for ReleaseName ${releaseName} `);
                return resolve(0);
            }
            for (var entry of map.entries()) {
                let releaseId = entry[1];
                var projectId = entry[0];
                reqReleaseWiseProjectMap.set(releaseId, projectId)
                getProjectName(entry[0]).then(response => {
                    let releaseIdIn = releaseId,
                        projectNamesIn = `"${response}"`;
                    let projectName = response;
                    getReleaseName(releaseId).then(relName => {
                        getTotalRequirementList(projectNamesIn, releaseIdIn).then(response => {
                            if (response === undefined) {
                                response = 0
                            }
                            console.log(currentTime(), '>> Inital Records ', relName, projectNamesIn, response)
                            let maxSize = response;
                            totalRecords = totalRecords + maxSize
                            if (maxSize > 0) {
                                getRequirement(projectNamesIn, releaseIdIn, maxSize).then(response => {
                                    TestCase.insertRequirements(response[0], releaseName, projectName, reqReleaseWiseProjectMap.get(releaseId), releaseId, usersList, lastRecordIdRequirementResults).then(response => {
                                        console.log(currentTime(), " >> ", projectNamesIn, " >> ", response)
                                        i++;
                                        function counter(i) {
                                            if (i === map.size) {
                                                releaseWiseReqCount.push(releaseName + " >> " + totalRecords)
                                                return true;
                                            }
                                        }
                                        if (counter(i)) {
                                            let tat = null;
                                            totalTimeTaken(startTimeRequirements).then(response => {
                                                tat = response;
                                                console.log(currentTime(), " >> ", `Total Records Inserted ${totalRecords}  >> ReleaseName ${relName} ,  Time Taken >> ${tat} `);
                                                totalRequirementRecords = totalRequirementRecords + totalRecords;
                                                resolve(totalRequirementRecords)
                                            })
                                        }
                                    }).catch(error => {
                                        console.error(currentTime(), " >> ", "Err on >> TestCase.insertRequirements")
                                        console.log(currentTime(), " >> ", error);
                                        reject(error)
                                    });
                                }).catch(error => {
                                    console.error(currentTime(), " >> ", "Err on >> getRequirement")
                                    console.log(error);
                                    reject(error)
                                });

                            } else {
                                console.log(currentTime(), " >> ", projectNamesIn, " No Records Inserted ");

                                i++;
                                function counter(i) {
                                    if (i === map.size) {
                                        releaseWiseTestsCount.push(releaseName + " >> " + totalRecords)
                                        return true;
                                    }
                                }

                                if (counter(i)) {
                                    totalTimeTaken(startTimeRequirements).then(response => {
                                        tat = response;
                                        console.log(currentTime(), " >> ", `Total Records Inserted ${totalRecords} ,  Time Taken >> ${tat} `);
                                        resolve(totalRecords)

                                    });
                                }
                            }
                        }).catch(error => {
                            console.error(currentTime(), " >> ", "Err on >> getTotalRequirementList")
                            console.log(error);
                            reject(error)
                        });
                    }).catch(error => {
                        console.error(currentTime(), " >> ", "Err on >> getReleaseName")
                        console.log(error);
                        reject(error)
                    });
         
                }).catch(error => {
                    console.error(currentTime(), " >> ", "Err on >> getProjectName")
                    console.log(error);
                    return reject(error)
                });
                // }// if by project name
            }

        } catch (error) {
            reject("Err on getRequirementsByRelease", error)
        }
    })
}

let lastRecordIdTestResults = null;

var startTimeTestResults = null;



module.exports.UploadZephyrTestResults = async function (runType, releaseName) {
    startTimeTestResults = moment(new Date(), 'YYYY-M-DD HH:mm:ss')
    //console.log(currentTime(), " ****************** ZEPHYR  TEST RESULTS  UPLOAD STARTED *******************");

    //logger.info(CONS.testResults, " >> ", "Started Test Results >>", new Date(), "   >> ReleaseName  ", releaseName);
    console.log(currentTime(), " >> ", "Started Test Results >>", new Date(), "   >> ReleaseName  ", releaseName);
    return new Promise((resolve, reject) => {
        try {
            DATA_MODELS.ZEPHYR_TestResults.getLstRecordCountIdTestResults().then(response => {
                lastRecordIdTestResults = response;
                if (releaseName !== upcomingRelease) {
                    DATA_MODELS.DataUploads.updateExecution(runType, API.testResults).then(updateResponse => {               
                    getTestResultsByRelease(releaseName).then(response => {
                        try {
                            let data = null;
                            data = response + "     ";
                            TestCase.ChkDeletedRecordsTestResults(lastRecordIdTestResults, releaseName).then(response => {
                                //logger.info(CONS.testResults, " >> After deleted records validation >> ", response);
                                console.log(currentTime(), " >> After deleted records validation >> ", response);
                                DATA_MODELS.ZEPHYR_TestResults.TotalInsertedRecordsTestResults(releaseName).then(totalRcords => {
                                    //   console.log(currentTime(), " ****************** ZEPHYR  TEST RESULTS  UPLOAD COMPLETED *******************");
                                    DATA_MODELS.DataUploads.updateExecution(runType, API.testResults).then(updateResponse => {
                                        DATA_MODELS.DataUploads.getAllData().then(uploadsdata => {
                                            return resolve([data + " " + totalRcords, uploadsdata])
                                        });


                                    });

                                })

                            });

                        } catch (error) {
                            //logger.error(CONS.testResults, " >> getTestResultsByRelease  ", error)
                            return reject(getTestResultsByRelease, response)
                        }
                    });
                });
                } else {
                    getUpcomingReleaseFromDB().then(response => {
                        var releaseNameList = [];
                        releaseNameList = response;
                        let releaseCounter = 0;
                        //logger.info(CONS.testResults,  " >> ", releaseNameList)
                        console.log(currentTime(), " >> ", releaseNameList)
                        for (let i in releaseNameList) {
                            let releaseNameArr = null;
                            releaseNameArr = releaseNameList[i];
                            getTestResultsByRelease(releaseNameArr).then(response => {
                                releaseCounter++
                                try {
                                    if (releaseNameList.length - 1 == i) {
                                        let data = null;
                                        TestCase.ChkDeletedRecordsTestResults(lastRecordIdTestResults, releaseNameList).then(response => {
                                            //logger.info(CONS.testResults,  " >> After deleted records validation >> ", response);
                                            console.log(currentTime(), " >> After deleted records validation >> ", response);
                                            DATA_MODELS.ZEPHYR_TestResults.TotalInsertedRecordsTestResults(releaseNameList).then(totalRcords => {
                                                data = "Total Records Inserted  >> " + totalRcords;
                                                //logger.info(CONS.testResults,  " >> ", data);
                                                console.log(currentTime(), " >> ", data);
                                                //     console.log(currentTime(), "  ****************** ZEPHYR  TEST RESULTS  UPLOAD COMPLETED *******************");

                                                DATA_MODELS.DataUploads.updateExecution(runType, API.testResults).then(updateResponse => {
                                                    DATA_MODELS.DataUploads.getAllData().then(uploadsData => {
                                                        return resolve([data, uploadsData]);
                                                    });


                                                });
                                            })
                                        });

                                    }
                                } catch (error) {
                                    //logger.error(CONS.testResults, " >> ", error)
                                    console.log(currentTime(), " >> ", error)
                                }
                            }).catch(error => {
                                //logger.error(CONS.testResults, " >> ", error)     
                                console.log(error)
                            });
                        }

                    }).catch(error => {
                        //logger.error(CONS.testResults, " >> ", getUpcomingReleaseFromDB, error)
                        console.log(currentTime(), " >> ", getUpcomingReleaseFromDB, error)
                        reject(getUpcomingReleaseFromDB, error)
                    })
                }
            });
        } catch (error) {
            //logger.error(CONS.testResults, " >> ", error) 
            reject("Err on >> ", UploadZephyrTestResults, error);
        }
    }).catch(error => {
        //logger.error(CONS.testResults,  " >> ", "Err on >> UploadZephyrTestResults", error)
        console.error(currentTime(), " >> ", "Err on >> UploadZephyrTestResults", error)
        reject("Err on >> UploadZephyrTestResults", error);
    });
}

const upcomingRelease = "upcomingRelease";

let lastRecordIdRequirementResults = null;
let totalRequirementRecords = 0;
var startTimeRequirements = null;
module.exports.UploadZephyrRequirements = async function (runType, releaseName = null) {
    startTimeRequirements = moment(new Date(), 'YYYY-M-DD HH:mm:ss')
    return new Promise((resolve, reject) => {
        try {
            DATA_MODELS.ZEPHYR_Requirements.getLstRecordCountIdRequirements().then(response => {
                lastRecordIdRequirementResults = response;
                getUsersList().then(usersList => {
                    let data = null;
                    DATA_MODELS.DataUploads.updateExecution(runType, API.requirements).then(updateResponse => {

                    getReleaseInfo().then(releaseList => {
                        if (releaseName !== upcomingRelease) {
                            getRequirementsByRelease(releaseName, usersList, releaseList).then(response => {
                                try {
                                    data = " Total requirements records completed ";
                                    TestCase.ChkDeletedRecordsRequirements(lastRecordIdRequirementResults, releaseName).then(response => {
                                        //logger.info(CONS.requirements ,"  >> After deleted records validation >> ", response);
                                        console.log(currentTime(), " >> After deleted records validation >> ", response);
                                        DATA_MODELS.ZEPHYR_Requirements.TotalInsertedRecordsRequirements(releaseName).then(totalRcords => {
                                            //logger.info(CONS.requirements , " >>  Total Records Inserted  ", totalRcords);
                                            console.log(currentTime(), " >>  Total Records Inserted  ", totalRcords);
                                            console.log(currentTime(), " ******************  REQUIREMENTS UPLOAD COMPLETED *******************");
                                            //logger.info(CONS.requirements , " ******************  REQUIREMENTS UPLOAD COMPLETED *******************");
                                            DATA_MODELS.DataUploads.updateExecution(runType, API.requirements).then(updateResponse => {
                                                DATA_MODELS.DataUploads.getAllData().then(uploadsData => {
                                                    return resolve([` >>  ${data}   >>  ${totalRcords}`, uploadsData]);
                                                }).catch(error => {
                                                    //logger.error(CONS.requirements, "Err on DataUpload Get All Data ", error)
                                                    console.log("Err on DataUpload Get All Data ", error)
                                                    reject("Err on DataUpload Get All Data  ", error)
                                                })

                                            }).catch(error => {
                                                //logger.error(CONS.requirements,"Err on update Execution  ", error)
                                                console.log("Err on update Execution  ", error)
                                                reject("Err on update execution ", error)
                                            })
                                        }).catch(error => {
                                            //logger.error(CONS.requirements,"Err on Total Inserted Requirements  ", error)
                                            console.log("Err on Total Inserted Requirements  ", error)
                                            reject("Err on Total Inserted Requirementd ", error)
                                        })

                                    }).catch(error => {
                                        //logger.error(CONS.requirements,"Err on ChkDeletedRecordsRequirements ", error)
                                        console.log("Err on ChkDeletedRecordsRequirements ", error)
                                        reject("Err ChkDeletedRecordsRequirements ", error)
                                    })

                                } catch (error) {
                                    //logger.error(CONS.requirements,"Err on getRequirementsByRelease ", error)
                                    return reject("Err on getRequirementsByRelease  >>> ", response)
                                }
                            });
                        } else {
                            getUpcomingReleaseFromDB().then(response => {
                                var releaseNameList = [];
                                releaseNameList = response;
                                console.log(releaseNameList)
                                let releaseCounter = 0;
                                for (let i in releaseNameList) {
                                    let releaseNameArr = null;
                                    releaseNameArr = releaseNameList[i]
                                    getRequirementsByRelease(releaseNameArr, usersList, releaseList).then(response => {
                                        try {
                                            releaseCounter++
                                             if (releaseNameList.length === releaseCounter) {
                                                TestCase.ChkDeletedRecordsRequirements(lastRecordIdRequirementResults, releaseNameList).then(response => {
                                                    //logger.info(CONS.requirements, " >> After deleted records validation >> ", response);
                                                    console.log(currentTime(), " >> After deleted records validation >> ", response);
                                                    DATA_MODELS.ZEPHYR_Requirements.TotalInsertedRecordsRequirements(releaseNameList).then(totalRcords => {
                                                        data = "  Total Records inserted >> " + totalRcords;
                                                        //logger.info(CONS.requirements, " >> ", data);
                                                        console.log(currentTime(), " >> ", data);
                                                        DATA_MODELS.DataUploads.updateExecution(runType, API.requirements).then(updateResponse => {
                                                            DATA_MODELS.DataUploads.getAllData().then(uploadsData => {
                                                                return resolve([data, uploadsData]);
                                                            });
                                                        });
                                                    })
                                                });
                                            }
                                        } catch (error) {
                                            //logger.error(CONS.requirements, " >> ", data);
                                            return reject("Err on getRequirementsByRelease ", response)
                                        }
                                    });
                                }
                            }).catch(error => {
                                //logger.error(CONS.requirements,"Err on getUpcomingReleaseFromDB ", error)
                                console.log("Err on getUpcomingReleaseFromDB ", error)
                                reject("Err ongetUpcomingReleaseFromDB ", error)
                            })
                        }
                    }).catch(error => {
                        //logger.error(CONS.requirements, " >> ", "Err on >> getReleaseInfo >> ",error)
                        console.error(currentTime(), " >> ", "Err on >> getReleaseInfo")
                        console.log(currentTime(), " >> ", error);
                        reject(error)
                    });
                });
                }).catch(error => {
                    //logger.error(CONS.requirements, " >> ", "Err on >> getUsersList >> ",error)
                    return reject("Err on getUsersList", response)
                });
            })
        } catch (error) {
            //logger.error(CONS.requirements,"Err on UploadZephyrRequirements ", error)
            console.log("Err on UploadZephyrRequirements ", error)
            reject("Err on UploadZephyrRequirements ", error)
        }
    });
}

const DeleteDuplicteTestResults = () => {
    return new Promise((resolve, reject) => {
        try {
            TestCase.deleteDuplicateTestResults().then(response => {
                return resolve(response)
            })
        } catch (error) {
            //logger.error(CONS.requirements,"Err on DeleteDuplicteTestResults ", error)
            console.log("Err on DeleteDuplicteTestResults ", error)
            reject("Err on DeleteDuplicteTestResults ", error)
        }
    });
}

const DeleteDuplicteRequirements = () => {
    return new Promise((resolve, reject) => {
        try {
            TestCase.deleteDuplicateRequirements().then(response => {
                return resolve(response)
            })
        } catch (error) {
            //logger.error(CONS.requirements,"Err on DeleteDuplicteRequirements ", error)
            console.log("Err on DeleteDuplicteRequirements ", error)
            reject("Err on DeleteDuDeleteDuplicteRequirementsplicteTestResults ", error)
        }
    });
}

module.exports.UploadZephyrTestResultDetails = async function (runType, releaseName) {
    startTimeTestResults = moment(new Date(), 'YYYY-M-DD HH:mm:ss')
    console.log(currentTime(), " >> ", "Started Test Results Details>>", new Date());
    return new Promise((resolve, reject) => {
        try {
            getUpcomingReleaseFromDB().then(releaseList => {
                getDistnictReleaseFromDB().then(disnictreleaseList => {
                DATA_MODELS.ZEPHYR_TestResults.getTestExecutionResults(releaseList).then(testResults => {
                    DATA_MODELS.ZEPHYR_TestResultDetails.truncateData(releaseList,disnictreleaseList).then(truncateStatus => {
                        console.log("Truncate status   ", truncateStatus)
                        let count = 0;
                        setTimeout(() => {
                        for (i in testResults) {
                            let row = testResults[i];
                            let reqids = row['requirementids']
                            if (reqids === null) {
                                count++;
                                if (count === testResults.length) {
                                    console.log(' All reocrds are iinserted')
                                    resolve('All records inserted')
                                }

                            } else {
                                let arr = reqids.split(',');
                                let arrCount = 0;
                                for (i in arr) {
                                    let data = {
                                        parentTcId: row['id'],
                                        projectName: row['projectName'],
                                        projectid: row['projectid'],
                                        releasename: row['releasename'],
                                        requirementids: row['requirementids'],
                                        testcaseid: row['testcaseid'],
                                        cyclephaseid: row['cyclephaseid'],
                                        releaseid: row['releaseid'],
                                        tcRequirementId: arr[i],
                                        requirementId: arr[i],
                                        tcrCatalogTreeId: row['tcrCatalogTreeId']
                                    }
                                    DATA_MODELS.ZEPHYR_TestResultDetails.insertintoTestResultDetails(data).then(insertResponse => {
                                        arrCount++;
                                        if (arrCount === arr.length) {
                                            count++;
                                            if (count === testResults.length) {
                                                console.log(' All reocrds are iinserted')
                                                let tat = null;
                                                totalTimeTaken(startTimeTestResults).then(response => {
                                                    tat = response;
                                                    console.log(currentTime(), "  >> ", ` Test detials Wise Requirements Uploaded , Time Taken >> ${tat} `);
                                                    return resolve(` Test detials Wise Requirements Uploaded , Time Taken >> ${tat} `)
                                                })

                                            }
                                        }
                                    });
                                }
                            }
                        }
                    }, 1000)
                    });

                });
            })
            });
        } catch (error) {
            reject("Err on >> ", UploadZephyrTestResults, error);
        }
    }).catch(error => {
        console.error(currentTime(), " >> ", "Err on >> UploadZephyrTestResults", error)
        reject("Err on >> UploadZephyrTestResults", error);
    });
}

module.exports.DeleteDuplicteTestResults = DeleteDuplicteTestResults;

module.exports.DeleteDuplicteRequirements = DeleteDuplicteRequirements;

