const mysql = require("mysql");
const dbConfig = require("../configs/db.config");

var connection = mysql.createPool({
  host: dbConfig.HOST,
  port: dbConfig.PORT,
  user: dbConfig.USER,
  password: dbConfig.PASSWORD,
  database: dbConfig.DB,
  connectionLimit: 10,
  timezone: 'UTC',
  dateStrings: [
    'DATE',
    'DATETIME'
]
});



module.exports = connection;
