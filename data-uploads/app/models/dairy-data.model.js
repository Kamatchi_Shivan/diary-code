const sql = require("./db");

var HashMap = require('hashmap');

const DATA_UTILS = require("../utils/data-utils");


const dbconfig = require("../configs/db.config")
const LogFs = require("../contants/commons")
const jobsAPI = require('../contants/db-uploads-api')
const { error } = require("console");

const Defects = function (defects) {
};

const ComarchDefects = function (defects) {
};

const CCBProjects = function (ccbProjects) {
}

const TestResults = function (resuts) {
}


const UserStory = function (userStory) {
};

const ReleaseMileStones = function (releaseMileStones) {
};

const ComarchMapping = function (comarchData) {
};

const TestReports = function (releaseMileStones) {
};

const ZEPHYR_TestResults = function (testResults) {
}

const ZEPHYR_Requirements = function (requirements) {
}

const DataUploads = function (dataUploads) {

}
const ZEPHYR_TestResultDetails = function (testResultDetails) {
}



const { currentTime } = require("../utils/date-utils");
const { getColumnValues } = require("../utils/data-utils");
const { id } = require("../contants/jira-custom-fields");

var DefectsInsertOrupdate = async function (qry, updateQry, defectId) {
    return new Promise((resolve, reject) => {
        sql.query(qry, (err, res) => {
            if (err) {
                if (err.errno === 1062) {
                    sql.query(updateQry, (error, res) => {
                        if (error) {
                            console.log(currentTime(), " >> ", "Error on updation ", error.message)
                            return reject(error.message)
                        } else {
                            console.log(currentTime(), " >> ", `Row Updated  > Defect Key : ${defectId} `)
                            resolve("Updated");
                        }
                    });
                } else {
                    return reject(err.message)
                }
            } else {
                console.log(currentTime(), " >> ", `defect inserted Defect Key ${defectKey} `, { id: res.insertId });
                return resolve(inserted);
            }
        });
    });
}

var DefectsUpdateFixVersions = async function () {
    return new Promise((resolve, reject) => {
        let qry = `update  defect_details d inner join story_defect_details s on d.parentKey = s.storyKey set storyDefectIdFixVersion = s.fixVersions ,d.fixVersions = s.fixVersions  where d.fixVersions is null ;`
        sql.query(qry, (err, res) => {
            if (err) {
                console.log(currentTime(), " >> ", "Error on Update FixVersion ", err.message)
                return reject("Error", err.message)
            }
            else {
                console.log(currentTime(), " >> ", `Defect Vesions Updated`);
                return resolve("Updated")
            }
        });
    });
}

TestResults.insert = (qry) => {
    sql.query(qry, (err, res) => {
        if (err) {
            console.log(currentTime(), " >> ", "error: ", err.message);
            console.log(currentTime(), " >> ", "Qry  ", qry)
            return err;
        }
        console.log(currentTime(), " >> ", "Test Results inserted ", { id: res.insertId });
    });
}

async function StoryUpdateOrInsert(insertQry, updateQry, storyKey) {
    return new Promise((resolve, reject) => {
        const rows = sql.query(insertQry, (err, res) => {
            if (err) {
                if (err.errno === 1062) {
                    sql.query(updateQry, (error, res) => {
                        if (error) {
                            console.log(currentTime(), " >> ", "Error on updation ", error.message)
                            reject(err.message);

                        } else {
                            console.log(currentTime(), " >> ", `Row Updated  > StoryID : ${storyKey} `)
                            resolve(`Row Updated  > StoryID : ${storyKey} `);
                        }
                    });
                }
            } else {
                console.log(`Story inserted  ${storyKey}`, { id: res.insertId });
                resolve((`Story inserted  ${storyKey}`, { id: res.insertId }));
            }
        });
        return rows;
    });
}

Defects.getLstRecordCountIdDefectDetails = async () => {
    return new Promise((resolve, reject) => {
        let getAutoIncrementQry = `select ID from defect_details order by id desc LIMIT 1;`
        var getLastRecordQry = sql.query(getAutoIncrementQry, (error, res) => {
            if (error) {
                console.log(currentTime(), " >> ", "Error on get Lat Record ID >> defect_details Table ", error.message)
                reject(error.message)
            } else {
                if (res.length > 0) {
                    let recordID = res[0]["ID"];
                    return resolve(recordID)
                } else {
                    return resolve(0)
                }
            }
        });

    });
}

ComarchDefects.getLstRecordCountIdComarchDefectDetails = async () => {
    return new Promise((resolve, reject) => {
        let getAutoIncrementQry = `select ID from comarch_defects order by id desc LIMIT 1;`
        var getLastRecordQry = sql.query(getAutoIncrementQry, (error, res) => {
            if (error) {
                console.log(currentTime(), " >> ", "Error on get Lat Record ID >> defect_details Table ", error.message)
                reject(error.message)
            } else {
                if (res.length > 0) {
                    let recordID = res[0]["ID"];
                    return resolve(recordID)
                } else {
                    return resolve(0)
                }
            }
        });

    });
}

Defects.deleteDefectsByDefectKey = async (defectKeyList) => {
    return new Promise((resolve, reject) => {
        let defList = ("'" + defectKeyList.join("','") + "'")
        let deleteQry = `delete from defect_details where defectKey in (${defList});`
        var unaffectedRecord = sql.query(deleteQry, (error, res) => {
            console.log(currentTime(), " >> ", unaffectedRecord.sql)
            if (error) {
                console.log(currentTime(), " >> ", "Error on REcord delete list  >> Defect Details ", error.message)
                reject(error.message)
            } else {
                return resolve(res.affectedRows)
            }

        });


    })
}

Defects.getUnaffectedRecordsDefectDetails = async (lastRecordIdB4Insertion) => {
    return new Promise((resolve, reject) => {
        getRecordsToDeleteDefectDetails(lastRecordIdB4Insertion).then(response => {
            let deleteQry = `delete from defect_details where id in (${response});`
            var unaffectedRecord = sql.query(deleteQry, (error, res) => {
                if (error) {
                    console.log(currentTime(), " >> ", "Error on get unaffectedRecord  >> Defect Details ", error.message)
                    reject(error.message)
                } else {
                    return resolve(res.affectedRows)
                }

            });
        })

    })
}

ComarchDefects.getUnaffectedRecordsComarchDefectDetails = async (lastRecordIdB4Insertion) => {
    return new Promise((resolve, reject) => {
        getRecordsToDeleteComarchDefectDetails(lastRecordIdB4Insertion).then(response => {
            let deleteQry = `delete from comarch_defects where id in (${response});`
            var unaffectedRecord = sql.query(deleteQry, (error, res) => {
                if (error) {
                    console.log(currentTime(), " >> ", "Error on get unaffectedRecord  >> Defect Details ", error.message)
                    reject(error.message)
                } else {
                    return resolve(res.affectedRows)
                }

            });
        })

    })
}

async function getRecordsToDeleteComarchDefectDetails(lastRecordIdB4Insertion) {
    return new Promise((resolve, reject) => {
        let selectQry = `select id,defectKey from comarch_defects where
                ID <  ${lastRecordIdB4Insertion} and  
                (updatedon is null  or updatedon 
                 < (NOW() - INTERVAL 5 MINUTE));`
        var deleteRecordSEt = sql.query(selectQry, (error, res) => {
            if (error) {
                console.log(currentTime(), " >> ", "Error on get to delete recordset , defect_detaisl  ", error.message)
                reject(error.message)
            } else {
                let arr = [];
                let defectsKey = [];
                for (var i = 0; i < res.length; i++) {
                    arr.push(res[i].id);
                    defectsKey.push(res[i].defectKey)
                }
                let ids = ("'" + arr.join("','") + "'");
                console.log(currentTime(), " >> ", 'Defects to be removed ', defectsKey.toString())
                return resolve(ids)
            }

        });
    })

}


async function getRecordsToDeleteDefectDetails(lastRecordIdB4Insertion) {
    return new Promise((resolve, reject) => {
        let selectQry = `select id,defectKey from defect_details where
                ID <  ${lastRecordIdB4Insertion} and  
                (updatedon is null  or updatedon 
                 < (NOW() - INTERVAL 5 MINUTE));`
        var deleteRecordSEt = sql.query(selectQry, (error, res) => {
            if (error) {
                console.log(currentTime(), " >> ", "Error on get to delete recordset , defect_detaisl  ", error.message)
                reject(error.message)
            } else {
                let arr = [];
                let defectsKey = [];
                for (var i = 0; i < res.length; i++) {
                    arr.push(res[i].id);
                    defectsKey.push(res[i].defectKey)
                }
                let ids = ("'" + arr.join("','") + "'");
                console.log(currentTime(), " >> ", 'Defects to be removed ', defectsKey.toString())
                return resolve(ids)
            }

        });
    })

}

Defects.getAllRecordsDefectDetails = async () => {
    return new Promise((resolve, reject) => {
        console.log(" in get all records Defects ")

        let selectQry = `select id,defectKey from defect_details order by ID desc;`
        // console.log("yselectQry " , selectQry)
        var deleteRecordSEt = sql.query(selectQry, (error, res) => {
            if (error) {
                console.log(currentTime(), " >> ", "Error on get to delete recordset , defect_detaisl  ", error.message)
                reject(error.message)
            } else {
                let arr = [];
                if (res.length === 0) {
                    console.log(" Results  length")
                    return resolve(null);
                }
                else {
                    let defectsKey = [];
                    for (var i = 0; i < res.length; i++) {
                        arr.push(res[i].id);
                        defectsKey.push(res[i].defectKey)
                    }
                    let ids = ("'" + arr.join("','") + "'");
                    //           console.log(" defects results set with ids" , ids)
                    return resolve([ids, defectsKey])
                }
            }

        });
    })

}

UserStory.getAllRecordsUserStory = async () => {
    return new Promise((resolve, reject) => {
        var storyIdWiseProjectKey = new HashMap();
        var storyIdWiseStoryKey = new HashMap();
        let selectQry = `select id, userStoryID, storyKey , projectKey from user_story_details order by id;`
        var deleteRecordSEt = sql.query(selectQry, (error, res) => {
            if (error) {
                console.log(currentTime(), " >> ", "Error on get to All recordSet , user_story_detaisl  ", error.message)
                reject(error.message)
            } else {
                if (res.length === 0) {
                    return resolve(null);
                } else {
                    let arr = [];
                    let storyKey = [];
                    for (var i = 0; i < res.length; i++) {
                        storyIdWiseProjectKey.set(res[i].userStoryID, res[i].projectKey)
                        storyIdWiseStoryKey.set(res[i].userStoryID, res[i].storyKey)
                        arr.push(res[i].id);
                        storyKey.push(res[i].storyKey)
                    }
                    let ids = ("'" + arr.join("','") + "'");
                    return resolve([ids, storyKey, storyIdWiseProjectKey, storyIdWiseStoryKey])
                }

            }

        });
    });
}

UserStory.deleteUserStoryByUserStoryKey = async (storyKeyList) => {
    return new Promise((resolve, reject) => {
        let keyList = ("'" + storyKeyList.join("','") + "'")
        let deleteQry = `delete from user_story_details where storyKey in (${keyList});`
        var unaffectedRecord = sql.query(deleteQry, (error, res) => {
            console.log(currentTime(), " >> ", unaffectedRecord.sql)
            if (error) {
                console.log(currentTime(), " >> ", "Error on Record delete list  >> User Story Details ", error.message)
                reject(error.message)
            } else {
                return resolve(res.affectedRows)
            }
        });
    })
}

UserStory.getLstRecordCountIdUserStoryDetails = async () => {
    return new Promise((resolve, reject) => {
        let getLastRecordIdQry = `select ID from user_story_details order by id desc LIMIT 1;`
        var getLastRecordQry = sql.query(getLastRecordIdQry, (error, res) => {
            if (error) {
                console.log(currentTime(), " >> ", "Error on get Last Record ID >> user_story_details Table ", error.message)
                reject(error.message)
            } else {
                if (res.length > 0) {
                    let recordID = res[0]["ID"];
                    return resolve(recordID)
                } else {
                    return resolve(0)
                }
            }
        });
    });
}

UserStory.getUnaffectedRecordsUserStoryDetails = async (lastRecordIdB4Insertion) => {
    return new Promise((resolve, reject) => {
        getRecordsToDeleteUserStoryDetails(lastRecordIdB4Insertion).then(response => {
            let deleteQry = `delete from user_story_details where id in (${response});`
            var unaffectedRecord = sql.query(deleteQry, (error, res) => {
                if (error) {
                    console.log(currentTime(), " >> ", "Error on get getUnaffectedRecordsUserStoryDetails  >> Defect Details ", error.message)
                    reject(error.message)
                } else {
                    return resolve(res.affectedRows)
                }
            });
        });
    });
}


async function getRecordsToDeleteUserStoryDetails(lastRecordIdB4Insertion) {
    return new Promise((resolve, reject) => {
        let selectQry = `select id,storyKey from user_story_details where
                ID <  ${lastRecordIdB4Insertion} and  
                (updatedon is null  or updatedon 
                 < (NOW() - INTERVAL 5 MINUTE));`
        var deleteRecordSEt = sql.query(selectQry, (error, res) => {
            if (error) {
                console.log(currentTime(), " >> ", "Error on get to delete recordset , user_story_details  ", error.message)
                reject(error.message)
            } else {
                let arr = [];
                let userStoryKey = [];
                for (var i = 0; i < res.length; i++) {
                    arr.push(res[i].id);
                    userStoryKey.push(res[i].storyKey)
                }
                let ids = ("'" + arr.join("','") + "'");
                console.log(currentTime(), " >> ", 'User Story to be removed ', userStoryKey.toString())
                return resolve(ids)
            }
        });
    });
}


CCBProjects.deleteRecords = async (deleteFlag) => {
    return new Promise((resolve, reject) => {
        console.log(currentTime(), " >> ", "delete Flag", deleteFlag)
        let taskDone = false;
        if (deleteFlag === 'true') {
            deleteSet = `DELETE FROM project_milestones_status;`
            var deleteQry = sql.query(deleteSet, (error, res) => {
                if (error) {
                    console.log(currentTime(), " >> ", "Error on deleting ", error.message)
                    reject(error.message)
                } else {
                    taskDone = true;
                    alterReleaseTable().then(response => {
                        resolve("Delete & Alteration Done")
                    })

                }

            });
            if ((taskDone) === true) {
                resolve("Deletion Completed")
            }

            function alterReleaseTable() {
                return new Promise((resolve, reject) => {
                    var alterQry = 'ALTER TABLE project_milestones_status  AUTO_INCREMENT = 1, CHANGE COLUMN `updatedOn` `updatedOn` TIMESTAMP NULL DEFAULT NULL ;'
                    var alterStmt = sql.query(alterQry, (error, res) => {
                        if (error) {
                            console.log(currentTime(), " >> ", "Error on project_milestones_status Alter ", error.message)
                            reject(error.message)
                        } else {
                            console.log(`Project ID Reset to 1 `)
                            taskDone = true;
                            resolve(true)
                        }
                    });
                });
            }
        } else {
            console.log(currentTime(), " >> ", "Records Deletion Skipped")
            return resolve("Records Deletion Skipped")
        }
    });
}

CCBProjects.insertPrep = async (dataList, updateList, updateClause, releaseName, projectName, testKeyMessage) => {
    return new Promise((resolve, reject) => {
        let fs = require('fs');
        let HistoryTestKeyMessage = DATA_UTILS.replaceQuotes(testKeyMessage["KeyRemarksTesting"]);
        let KeyRemarksTesting_Plain = DATA_UTILS.replaceQuotes(testKeyMessage["KeyRemarksTesting_Plain"]);
        let insertSet = null;
        let updateSet = null;
        if (HistoryTestKeyMessage !== null && HistoryTestKeyMessage != '') {


            insertSet = `INSERT INTO project_milestones_status SET ? , KeyRemarksTesting= '${HistoryTestKeyMessage}' ,
                             KeyRemarksTesting_Plain='${KeyRemarksTesting_Plain}'`;

            updateSet = `UPDATE project_milestones_status SET ?,   
            HistoryTestKeyMessage=  
                CASE WHEN ( HistoryTestKeyMessage is null or historyTestKeyMessage ='') Then 
                    if(STRCMP(KeyRemarksTesting, '${HistoryTestKeyMessage}')=0,
                    '',
                    CONCAT('<br><i>${currentTime()}</i> <br>@$',KeyRemarksTesting,'$@')  )
                WHEN  ( HistoryUpdatedOn!=CURDATE() ) OR (HistoryUpdatedOn=CURDATE())
                    THEN 
                    IF( (STRCMP(SUBSTRING_INDEX(SUBSTRING_INDEX(HistoryTestKeyMessage,'$@',1),'@$',-1),KeyRemarksTesting)) = 0, 
                       HistoryTestKeyMessage,
                       CONCAT('<br><i>${currentTime()}</i> <br>@$',KeyRemarksTesting,'$@',HistoryTestKeyMessage)) 
                END ,updatedOn=current_timestamp() ,HistoryUpdatedOn= if((HistoryTestKeyMessage is  null or HistoryTestKeyMessage=''),null,curdate()) ${updateClause}`

        } else {
            updateList["KeyRemarksTesting"] = ''
            updateList["KeyRemarksTesting_Plain"] = ''
            KeyRemarksTesting = "";
            KeyRemarksTesting_Plain = "";
            insertSet = `INSERT INTO project_milestones_status SET ? , KeyRemarksTesting= '${KeyRemarksTesting}' ,
                    KeyRemarksTesting_Plain='${KeyRemarksTesting_Plain}'`;
            updateSet = `UPDATE project_milestones_status SET ?, KeyRemarksTesting= '${KeyRemarksTesting}' ,
            KeyRemarksTesting_Plain='${KeyRemarksTesting_Plain}' , updatedOn=current_timestamp() ${updateClause};   `

        }

        var insertQry = sql.query(insertSet, dataList, (err, res) => {

            if (err) {
                if (err.errno === 1062) {
                    var updateQry = sql.query(updateSet, updateList, (error, res) => {
                        if (error) {
                            console.log(currentTime(), " >> ", "Error on updation ", error.message)
                            reject(err.message)
                        } else {
                            if (HistoryTestKeyMessage !== null) {
                                let updateTestKeyMessage = `UPDATE project_milestones_status SET  KeyRemarksTesting ='${HistoryTestKeyMessage}' ,
                                KeyRemarksTesting_Plain='${KeyRemarksTesting_Plain}' ,HistoryTestKeyMessage= IF(HistoryTestKeyMessage LIKE '%@$$@%','',HistoryTestKeyMessage), 
                                updatedOn=current_timestamp() ${updateClause}`;
                                var updatTestKeyQry = sql.query(updateTestKeyMessage, (errorRes, response) => {
                                    if (errorRes) {
                                        console.log(currentTime(), ' >> ', 'Error on update Test Key Message ', errorRes)
                                        reject(errorRes)
                                    } else {
                                        resolve(`Row Updated  > relese : ${releaseName}  & Project : ${projectName}`)
                                    }
                                })
                            }
                            else {

                                //console.log(currentTime(), `Row Updated  > relese : ${releaseName}  & Project : ${projectName}`)
                                resolve(`Row Updated  > relese : ${releaseName}  & Project : ${projectName}`)
                            }
                        }
                    });
                }
                else if (err.errno === 1292) {
                    console.log(currentTime(), " >> ", "Error for date values ", err.message)
                    reject(err.message)
                }
                else if (err.errno === 1064) {
                    console.log(currentTime(), " >> ", "Error for 1064 values ", err.message)
                    reject(err.message)
                }
                else {
                    console.log(currentTime(), " >> ", err.errno, err.message)
                    reject(err.message)
                }
            } else {
                console.log(currentTime(), " >> ", "Project CCB entries inserted ", { id: res.insertId });
                resolve("Project CCB entries inserted ", { id: res.insertId });
            }
        });
    });
};

CCBProjects.getLstRecordCountIdProjectMilestones = async (id) => {
    return new Promise((resolve, reject) => {
        let getAutoIncrementQry = `select ID from project_milestones_status order by id desc LIMIT 1;`
        var getLastRecordQry = sql.query(getAutoIncrementQry, (error, res) => {
            if (error) {
                console.log(currentTime(), " >> ", "Error on get Auto Increment ID ", error.message)
                reject(error.message)
            } else {
                if (res.length > 0) {
                    let recordID = res[0]["ID"];
                    return resolve(recordID)
                } else {
                    return resolve(0)
                }
            }
        });

    });
}

CCBProjects.getUnaffectedRecordsProjects = async (lastRecordIdB4Insertion) => {
    return new Promise((resolve, reject) => {
        getRecordsToDeleteProjects(lastRecordIdB4Insertion).then(response => {
            let deleteQry = `delete from project_milestones_status where id in (${response});`
            var unaffectedRecord = sql.query(deleteQry, (error, res) => {
                if (error) {
                    console.log(currentTime(), " >> ", "Error on get unaffectedRecords >> Projects ", error.message)
                    reject(error.message)
                } else {
                    return resolve(res.affectedRows)
                }

            });
        })

    })

}

ComarchMapping.getUnaffectedRecordsMappingTable = async (lastRecordIdB4Insertion) => {
    return new Promise((resolve, reject) => {
        getRecordsToDeleteComarch(lastRecordIdB4Insertion).then(response => {
            let deleteQry = `delete from comarch_mappingtable where ID in (${response});`
            var unaffectedRecord = sql.query(deleteQry, (error, res) => {
                if (error) {
                    console.log(currentTime(), " >> ", "Error on get unaffectedRecords >> Projects ", error.message)
                    reject(error.message)
                } else {
                    return resolve(res.affectedRows)
                }

            });
        })

    })

}

async function getRecordsToDeleteComarch(lastRecordIdB4Insertion) {
    return new Promise((resolve, reject) => {
        let selectQry = `select id from comarch_mappingtable where
                ID <  ${lastRecordIdB4Insertion} and (updatedon is null  or updatedon  < (NOW() - INTERVAL 15 MINUTE));`
        var deleteRecordSEt = sql.query(selectQry, (error, res) => {
            if (error) {
                console.log(currentTime(), " >> ", "Error on get unaffectedRecord >> projects ", error.message)
                reject(error.message)
            } else {
                let arr = [];
                for (var i = 0; i < res.length; i++) {
                    arr.push(res[i].id);
                }
                let ids = ("'" + arr.join("','") + "'");
                return resolve(ids)
            }

        });
    })

}


async function getRecordsToDeleteProjects(lastRecordIdB4Insertion) {
    return new Promise((resolve, reject) => {
        let selectQry = `select id from project_milestones_status where
                ID <  ${lastRecordIdB4Insertion} and (updatedon is null  or updatedon  < (NOW() - INTERVAL 15 MINUTE));`
        var deleteRecordSEt = sql.query(selectQry, (error, res) => {
            if (error) {
                console.log(currentTime(), " >> ", "Error on get unaffectedRecord >> projects ", error.message)
                reject(error.message)
            } else {
                let arr = [];
                for (var i = 0; i < res.length; i++) {
                    arr.push(res[i].id);
                }
                let ids = ("'" + arr.join("','") + "'");
                return resolve(ids)
            }

        });
    })

}


CCBProjects.getUnaffectedRecordsProjects = async (lastRecordIdB4Insertion) => {
    return new Promise((resolve, reject) => {
        getRecordsToDeleteProjects(lastRecordIdB4Insertion).then(response => {
            let deleteQry = `delete from project_milestones_status where id in (${response});`
            var unaffectedRecord = sql.query(deleteQry, (error, res) => {
                if (error) {
                    console.log(currentTime(), " >> ", "Error on get unaffectedRecords >> Projects ", error.message)
                    reject(error.message)
                } else {
                    return resolve(res.affectedRows)
                }

            });
        })

    })

}

ComarchMapping.getLstRecordCountIdComarchMappingTable = async (id) => {
    return new Promise((resolve, reject) => {
        let getAutoIncrementQry = `select ID from comarch_mappingtable order by id desc LIMIT 1;`
        var getLastRecordQry = sql.query(getAutoIncrementQry, (error, res) => {
            if (error) {
                console.log(currentTime(), " >> ", "Error on get Auto Increment ID ", error.message)
                reject(error.message)
            } else {
                if (res.length > 0) {
                    let recordID = res[0]["ID"];
                    return resolve(recordID)
                } else {
                    return resolve(0)
                }
            }
        });

    });
}

ReleaseMileStones.deleteRecords = async (deleteFlag) => {
    return new Promise((resolve, reject) => {
        let taskDone = false;
        console.log(currentTime(), " >> ", "delete Flag", deleteFlag)
        if ((deleteFlag) === 'true') {
            deleteSet = `DELETE FROM release_calender;`
            var deleteQry = sql.query(deleteSet, (error, res) => {
                if (error) {
                    console.log(currentTime(), " >> ", "Error on deleting ", error.message)
                    reject(error.message)
                } else {
                    console.log(`Release Milestone Records Deleted `)
                    taskDone = true;
                    alterReleaseTable().then(response => {
                        resolve("Delete & Alteration Done")
                    })
                }
            });
        }
        else {
            console.log(currentTime(), " >> ", "Records Deletion Skipped")
            return resolve("Records Deletion Skipped")
        }
        function alterReleaseTable() {
            return new Promise((resolve, reject) => {
                var alterQry = 'ALTER TABLE release_calender  AUTO_INCREMENT = 1, CHANGE COLUMN `updatedOn` `updatedOn` TIMESTAMP NULL DEFAULT NULL ;'
                var alterStmt = sql.query(alterQry, (error, res) => {
                    if (error) {
                        console.log(currentTime(), " >> ", "Error on release_calender Alter ", error.message)
                        reject(error.message)
                    } else {
                        console.log(`Release ID Reset to 1 `)
                        taskDone = true;
                        resolve(true)
                    }
                });
            });
        }
    });
}

ReleaseMileStones.insertPrep = async (dataList, updateList, ReleaseId,  lockcount = 0) => {
    return new Promise((resolve, reject) => {
        if (lockcount <= 1) {
            try {
                let insertSet = `INSERT INTO release_calender SET ?  `
                let updateSet = `UPDATE release_calender
                SET ?, updatedOn=current_timestamp() 
                WHERE ReleaseId ='${ReleaseId}'`;
                let updateClause = ` WHERE ReleaseId ='${ReleaseId}'`;

                setTimeout(() => {
                    var insertQry = sql.query(insertSet, dataList, (err, res) => {
                        if (err) {
                            if (err.errno === 1062) {
                                var updateQry = sql.query(updateSet, updateList, (error, res) => {
                                    if (error) {
                                        console.log(currentTime(), " >> ", "Error on updation ", error)
                                        reject(error)
                                    } else {
                                            //  console.log(currentTime(), " >> ", `Row Updated > release :   ${releaseName} `)
                                            resolve(`Row Updated > release :   ${ReleaseId} `)

                                    }
                                });
                            }
                            else if (err.errno === 1292) {
                                console.log(currentTime(), " >> ", "insert 1292 error Error for date values ", err.message)
                                reject(error)
                            }
                            else if (err.errno === 1064) {
                                console.log(currentTime(), " >> ", "insert 1064 error  Error for date values ", err.message)
                                reject(error)
                            }
                            else {
                                console.log(currentTime(), " >> ", err.errno, err.message)
                                reject(error)
                            }
                        } else {
                            //console.log(currentTime(), " >> ", "Release CCB entries inserted ", releaseName, { id: res.insertId });
                            resolve("Release CCB entries inserted ", ReleaseId, { id: res.insertId })
                        }

                    });
                }, 1000
                )
            }
            catch (error) {
                // setTimeout(resolve, 2000)                
                if (error.errno === 1213) {
                    lockcount + 1;
                    insertPrep(insertPost, updatePost, ReleaseId, lockcount)
                }
                else {
                    reject(error)
                }
            }
        } else {
            reject(error);
        }

    });
}

ReleaseMileStones.getLstRecordCountIdReleaseMilestone = async () => {
    return new Promise((resolve, reject) => {
        let getAutoIncrementQry = `select ID from release_calender order by id desc LIMIT 1;`
        var getLastRecordQry = sql.query(getAutoIncrementQry, (error, res) => {
            if (error) {
                console.log(currentTime(), " >> ", "Error on get Lat Record ID >> Release Table ", error.message)
                reject(error.message)
            } else {
                if (res.length > 0) {
                    let recordID = res[0]["ID"];
                    return resolve(recordID)
                } else {
                    return resolve(0)
                }
            }
        });

    });
}

ReleaseMileStones.getUnaffectedRecordsRelease = async (lastRecordIdB4Insertion) => {
    return new Promise((resolve, reject) => {
        getRecordsToDeleteRelease(lastRecordIdB4Insertion).then(response => {
            let deleteQry = `delete from release_calender where id in (${response});`
            var unaffectedRecord = sql.query(deleteQry, (error, res) => {
                if (error) {
                    console.log(currentTime(), " >> ", "Error on get unaffectedRecords >> Release ", error.message)
                    reject(error.message)
                } else {
                    return resolve(res.affectedRows)
                }

            });
        })

    })

}

async function getRecordsToDeleteRelease(lastRecordIdB4Insertion) {
    return new Promise((resolve, reject) => {
        let selectQry = `select id from release_calender where
                ID <  ${lastRecordIdB4Insertion} and (updatedon is null  or updatedon  < (NOW() - INTERVAL 15 MINUTE));`
        var deleteRecordSEt = sql.query(selectQry, (error, res) => {
            if (error) {
                console.log(currentTime(), " >> ", "Error on get unaffectedRecord >> projects ", error.message)
                reject(error.message)
            } else {
                let arr = [];
                for (var i = 0; i < res.length; i++) {
                    arr.push(res[i].id);
                }
                let ids = ("'" + arr.join("','") + "'");
                return resolve(ids)
            }

        });
    })

}

TestReports.deleteRecords = async (deleteFlag) => {
    return new Promise((resolve, reject) => {
        let taskDone = false;

        deleteSet = `DELETE FROM test_reports;`
        var deleteQry = sql.query(deleteSet, (error, res) => {
            if (error) {
                console.log(currentTime(), " >> ", "Error on deleting ", error.message)
                reject(error.message)
            } else {
                console.log(`Test Reports Records Deleted `)
                taskDone = true;
                alterTestReportsTable().then(response => {
                    resolve("Delete & Alteration Done")
                })
            }
        });

        function alterTestReportsTable() {
            return new Promise((resolve, reject) => {
                var alterQry = 'ALTER TABLE test_reports  AUTO_INCREMENT = 1';
                var alterStmt = sql.query(alterQry, (error, res) => {
                    if (error) {
                        console.log(currentTime(), " >> ", "Error on test_reports Alter ", error.message)
                        reject(error.message)
                    } else {
                        console.log(`Test Reports ID Reset to 1 `)
                        taskDone = true;
                        resolve(true)
                    }
                });
            });
        }
    });
}

TestReports.insertPrep = async (dataList, itemId, lockcount = 0) => {
    return new Promise((resolve, reject) => {
        if (lockcount <= 1) {
            try {
                let insertSet = `INSERT INTO test_reports SET ?  `
                let updateSet = `UPDATE test_reports
                SET ?, updatedOn=current_timestamp()  WHERE
                ItemID =${itemId};`
                var insertQry = sql.query(insertSet, dataList, (err, res) => {
                    if (err) {
                        if (err.errno === 1062) {
                            var updateQry = sql.query(updateSet, dataList, (error, res) => {
                                if (error) {
                                    console.log(currentTime(), " >> ", "Error on updation ", error.message)
                                    reject(error.message)
                                } else {
                                    console.log(`Row Updated > Results Key: ${itemId}  `)
                                    resolve(`Row Updated > Results Key: ${itemId}  `);
                                }
                            });
                        } else {
                            reject(err.message)
                        }
                    } else {
                        console.log(`Row Updated > Results Key: ${itemId}  `);
                        resolve(`Row inserted Result Key ${itemId} `, { id: res.insertId });
                    }

                });
            }
            catch (error) {
                if (error.errno === 1213) {
                    lockcount + 1;
                    insertPrep(dataList, lockcount)
                }
                else {
                    reject(error)
                }
            }
        } else {
            reject(error);
        }

    });
}


ZEPHYR_TestResults.getLstRecordCountIdTestResults = async () => {
    return new Promise((resolve, reject) => {
        let getAutoIncrementQry = `select ID from test_execution_results order by id desc LIMIT 1;`
        var getLastRecordQry = sql.query(getAutoIncrementQry, (error, res) => {
            if (error) {
                console.log(currentTime(), " >> ", "Error on get Auto Increment ID ", error.message)
                reject(error.message)
            } else {
                if (res.length > 0) {
                    let recordID = res[0]["ID"];
                    return resolve(recordID)
                } else {
                    return resolve(0)
                }
            }
        });

    });
}


ZEPHYR_TestResults.getActualUploadSummaryTestResults = async () => {
    return new Promise((resolve, reject) => {
        let getActualSummaryQry = `select projectId,releaseId,count(testcaseid) as totalRecordCount from test_execution_results group by projectid,releaseid;`;
        var getRecords = sql.query(getActualSummaryQry, (error, res) => {
            if (error) {
                console.log(currentTime(), " >> ", "Error on get getActualExecutionSummary ", error.message)
                reject(error.message)
            } else {
                return resolve((res))
            }
        });

    });
}


ZEPHYR_TestResults.getUnaffectedRecordsTestResults = async (lastRecordIdB4Insertion, releaseNamesList) => {
    return new Promise((resolve, reject) => {
        let releaseList = null;
        if (typeof releaseNamesList === "string")
            releaseList = "'" + releaseNamesList + "'";
        else
            releaseList = "'" + releaseNamesList.join("','") + "'";

        getRecordsToDeleteTestResults(lastRecordIdB4Insertion, releaseList).then(response => {
            let deleteQry = `select count(*) as count from test_execution_results where id in (${response});`
            var unaffectedRecord = sql.query(deleteQry, (error, res) => {
                if (error) {
                    console.log(currentTime(), " >> ", "Error on get unaffectedRecord ", error.message)
                    reject(error.message)
                } else {
                    if (res.length > 0) {
                        let totalrecords = res[0]['count'];
                        console.log(`Total of ${totalrecords} records to be cleanedup`);
                        resolve(`Total of ${totalrecords} records to be cleanedup`)
                    } else {
                        return resolve("No Junk records found ")
                    }

                }

            });
        })

    })

}

async function getRecordsToDeleteTestResults(lastRecordIdB4Insertion, releaseNamesList) {
    return new Promise((resolve, reject) => {
        let selectQry = `select id from test_execution_results where
                ID <  ${lastRecordIdB4Insertion}  
                 and releaseName in (${releaseNamesList.toString()})
                  and (updatedon is null  or updatedon  < (NOW() - INTERVAL 20 MINUTE))`
        var deleteRecordSEt = sql.query(selectQry, (error, res) => {
            let fs = require('fs');
            fs.writeFile(LogFs.testDeleteLog, `  ${deleteRecordSEt.sql}`, function (err) {
                if (err) return console.log(err);
                console.log('Records to be Deleted Qry available in Test-Delete.log');
            });
            if (error) {
                console.log(currentTime(), " >> ", "Error on get unaffectedRecord ", error.message)
                reject(error.message)
            } else {
                let arr = [];
                for (var i = 0; i < res.length; i++) {
                    arr.push(res[i].id);
                }
                let ids = ("'" + arr.join("','") + "'");
                return resolve(ids)
            }

        });
    })

}



ZEPHYR_TestResults.TotalInsertedRecordsTestResults = async (releaseNamesList) => {
    return new Promise((resolve, reject) => {
        let releaseList = null;
        if (typeof releaseNamesList === "string")
            releaseList = "'" + releaseNamesList + "'";
        else
            releaseList = "'" + releaseNamesList.join("','") + "'";
        let totalRecordCountQry = `select count(*) as total from test_execution_results where releasename in (${releaseList});`
        var totalRecordsSet = sql.query(totalRecordCountQry, (error, res) => {
            if (error) {
                console.log(totalRecordsSet.sql)
                console.log(currentTime(), " >> ", "Error on get unaffectedRecord ", error.message)
                reject(error.message)
            } else {
                return resolve(res[0]["total"]);
            }

        });
    })

}



function getLastUpdatedOnTime() {
    return new Promise((resolve, reject) => {
        let lastUpdatedOnQry = `select distinct updatedon from test_execution_results order by updatedon desc LIMIT 1 ;`
        var getLastUpdatedOnTime = sql.query(lastUpdatedOnQry, (error, res) => {
            if (error) {
                console.log(currentTime(), " >> ", "Error on getLastUpdatedOnTime ", error.message)
                reject(error.message)
            } else {
                return resolve(res[0]["updatedon"])
            }
        });
    });
}


ZEPHYR_Requirements.getLstRecordCountIdRequirements = async () => {
    return new Promise((resolve, reject) => {
        let getAutoIncrementQry = `select ID from requirement_details order by id desc LIMIT 1;`
        var getLastRecordQry = sql.query(getAutoIncrementQry, (error, res) => {
            if (error) {
                console.log(currentTime(), " >> ", "Error on get Last Record ID requirement_details", error.message)
                reject(error.message)
            } else {
                console.log(' Results ', getLastRecordQry.sql, res)
                if (res.length > 0) {
                    let recordID = res[0]["ID"];
                    // recordID = 0;
                    return resolve(recordID)
                } else {
                    return resolve(0)
                }
            }
        });
    });
}




ZEPHYR_Requirements.getUnaffectedRecordsRequirements = async (lastRecordIdB4Insertion, releaseNamesList) => {
    return new Promise((resolve, reject) => {
        let releaseList = null;
        if (typeof releaseNamesList === "string")
            releaseList = "'" + releaseNamesList + "'";
        else
            releaseList = "'" + releaseNamesList.join("','") + "'";
        getRecordsToDeleteRequirements(lastRecordIdB4Insertion, releaseList).then(response => {

            let deleteQry = `select count(*) as count from requirement_details where id in (${response});`
            var unaffectedRecord = sql.query(deleteQry, (error, res) => {
                if (error) {
                    console.log(currentTime(), " >> ", "Error on get unaffectedRecord ", error.message)
                    reject(error.message)
                } else {
                    if (res.length > 0) {
                        let totalrecords = res[0]['count'];
                        console.log(`Total of ${totalrecords} records to be cleanedup`);
                        resolve(`Total of ${totalrecords} records to be cleanedup`)
                    } else {
                        return resolve("No Junk records found ")
                    }
                }

            });
        });
    })
}


async function getRecordsToDeleteRequirementsTestCase(parenetReqIdList) {
    return new Promise((resolve, reject) => {
        let deleteQry = `select * from requirement_wise_test_details where parentReqId in (${parenetReqIdList});`
        var unaffectedRecord = sql.query(deleteQry, (error, res) => {
            console.log(deleteQry)
            if (error) {
                console.log(currentTime(), " >> ", "Error on  getRecordsToDeleteRequirementsTestCase ", error.message)
                reject(error.message)
            } else {
                return resolve(res.affectedRows)
            }

        });
    });
}

//async function getRecordsToDeleteRequirements(projectId, releaseId, lastRecordIdB4Insertion) {
async function getRecordsToDeleteRequirements(lastRecordIdB4Insertion, releaseNamesList) {

    return new Promise((resolve, reject) => {
        let selectQry = `select id from requirement_details where
        ID <  ${lastRecordIdB4Insertion}   
        and releaseName in (${releaseNamesList.toString()}) 
        and (updatedon is null  or updatedon  < (NOW() - INTERVAL 20 MINUTE))`

        var deleteRecordSEt = sql.query(selectQry, (error, res) => {
            let fs = require('fs');
            fs.writeFile(LogFs.requirementsDeleteLog, `${deleteRecordSEt.sql}`, function (err) {
                if (err) return console.log(err);
            });
            if (error) {
                console.log(currentTime(), " >> ", "Error on get unaffectedRecord ", error.message)
                reject(error.message)
            } else {
                let arr = [];
                for (var i = 0; i < res.length; i++) {
                    arr.push(res[i].id);
                }
                let ids = ("'" + arr.join("','") + "'");

                return resolve(ids)
            }

        });
    })

}



ZEPHYR_Requirements.TotalInsertedRecordsRequirements = async (releaseNamesList) => {
    return new Promise((resolve, reject) => {
        let releaseList = null;
        if (typeof releaseNamesList === "string")
            releaseList = "'" + releaseNamesList + "'";
        else
            releaseList = "'" + releaseNamesList.join("','") + "'";
        let totalRecordCountQry = `select count(*) as total from requirement_details where  releaseName in (${releaseList.toString()});`
        console.log(totalRecordCountQry)
        var totalRecordsSet = sql.query(totalRecordCountQry, (error, res) => {
            if (error) {
                console.log(currentTime(), " >> ", "Error on get Total Records ", error.message)
                reject(error.message)
            } else {
                return resolve(res[0]["total"]);
            }

        });
    })

}

DataUploads.updateExecution = async (runType, jobName) => {
    return new Promise((resolve, reject) => {
        let updateSet = null;
        if (runType === jobsAPI.manualRun) {
            updateSet = `UPDATE uploadjobs
            SET  ManualRunTime=current_timestamp() 
            WHERE jobName ='${jobName}'`
         }
         else {
             updateSet = `UPDATE uploadjobs
             SET  lastRunTime=current_timestamp() 
             WHERE jobName ='${jobName}'`

         }
        var updateRecords = sql.query(updateSet, (error, res) => {
            if (error) {
                console.log(currentTime(), " >> ", "Error on get updation ", error.message)
                reject(error.message)
            } else {
                return resolve(`${runType}  > ${jobName}`);
            }

        });
    })
}

const mysql = require("mysql");
const { map } = require("underscore");
const { select } = require("async");

DataUploads.getAllData = async () => {
    return new Promise((resolve, reject) => {
        let selectQry = null;

        selectQry = selectQry = `select  CONCAT(UCASE(MID(jobName,1,1)),MID(jobName,2)) as jobName,jobDescription,jobUrl,ManualRuntime,LastRunTime from uploadjobs 
        order by field(jobName,"release", "projects", "testReports","components","defects","userStory","requirements","testResults");`

        var getRecords = sql.query(selectQry, (error, res) => {
            if (error) {
                console.log(currentTime(), " >> ", "Error on get upload records ", error.message)
                reject(error.message)
            } else {
                return resolve(res);
            }

        });


    })
}


ZEPHYR_TestResults.getTestExecutionResults = async (releaseNamesList) => {
    return new Promise((resolve, reject) => {

        let releaseList = "'" + releaseNamesList.join("','") + "'";
        let getRecordsQry = `select id,projectName,releasename,requirementids,testcaseid,teststatus,cyclephaseid,releaseid,projectid,tcrCatalogTreeId 
                            from test_execution_results where releaseName in (${releaseList.toString()});`
        console.log('get records qry ', getRecordsQry)
        var getRecords = sql.query(getRecordsQry, (error, res) => {
            if (error) {
                console.log(currentTime(), " >> ", "Error on get Auto Test Execution Results", error.message)
                reject(error.message)
            } else {

                return resolve(res)

            }
        });

    });
}


ZEPHYR_TestResultDetails.truncateData = async (releaseNamesList,disnictreleaseNameList) => {
    return new Promise((resolve, reject) => {
        let releaseList = "'" + releaseNamesList.join("','") + "'";
        let disnictreleaseList = "'" + disnictreleaseNameList.join("','") + "'";
        let qry = `delete from  test_execution_wise_req_details where releaseName in (${releaseList.toString()});`
        let distqry = `delete from  test_execution_wise_req_details where releaseName not in (${disnictreleaseList.toString()});`
        console.log(qry)
        var truncateQry = sql.query(qry, (error, res) => {
            if (error) {
                console.log(currentTime(), " >> ", "Error on Truncate test_execution_wise_req_details", error.message)
                reject(error.message)
            } else {
               sql.query(distqry, (error, res) => {
                    if (error) {
                        console.log(currentTime(), " >> ", "Error on Truncate test_execution_wise_req_details", error.message)
                        reject(error.message)
                    } else {
                        return resolve("Records Removed")
                    }
                });
            }
        });
    });
}

ZEPHYR_TestResultDetails.insertintoTestResultDetails = async (dataList) => {
    return new Promise((resolve, reject) => {

        let insertSet = `INSERT INTO test_execution_wise_req_details SET ?  `;

        var insertQry = sql.query(insertSet, dataList, (err, res) => {
            // console.log(insertQry.sql)
            if (err) {
                console.log(currentTime(), " >> ", insertQry.sql)
                console.log(currentTime(), " >> ", err.errno, err.message)
                reject(err.message)
            } else {
                resolve("test_execution_wise_req_details inserted ", { id: res.insertId });
            }
        });

    });
}

ComarchMapping.insertPrep = async (dataList, updateList, Comarch_ID,  lockcount = 0) => {
    return new Promise((resolve, reject) => {
        if (lockcount <= 1) {
            try {
                let insertSet = `INSERT INTO comarch_mappingtable SET ?  `
                let updateSet = `UPDATE comarch_mappingtable
                SET ?, updatedOn=current_timestamp() 
                WHERE Comarch_ID ='${Comarch_ID}'`;
                let updateClause = ` WHERE Comarch_ID ='${Comarch_ID}'`;

                setTimeout(() => {
                    var insertQry = sql.query(insertSet, dataList, (err, res) => {
                        if (err) {
                            if (err.errno === 1062) {
                                var updateQry = sql.query(updateSet, updateList, (error, res) => {
                                    if (error) {
                                        console.log(currentTime(), " >> ", "Error on updation ", error)
                                        reject(error)
                                    } else {
                                            //  console.log(currentTime(), " >> ", `Row Updated > release :   ${releaseName} `)
                                            resolve(`Row Updated > release :   ${Comarch_ID} `)

                                    }
                                });
                            }
                            else if (err.errno === 1292) {
                                console.log(currentTime(), " >> ", "insert 1292 error Error for date values ", err.message)
                                reject(error)
                            }
                            else if (err.errno === 1064) {
                                console.log(currentTime(), " >> ", "insert 1064 error  Error for date values ", err.message)
                                reject(error)
                            }
                            else {
                                console.log(currentTime(), " >> ", err.errno, err.message)
                                reject(error)
                            }
                        } else {
                            //console.log(currentTime(), " >> ", "Release CCB entries inserted ", releaseName, { id: res.insertId });
                            resolve("Release CCB entries inserted ", Comarch_ID, { id: res.insertId })
                        }

                    });
                }, 1000
                )
            }
            catch (error) {
                // setTimeout(resolve, 2000)                
                if (error.errno === 1213) {
                    lockcount + 1;
                    insertPrep(insertPost, updatePost, Comarch_ID, lockcount)
                }
                else {
                    reject(error)
                }
            }
        } else {
            reject(error);
        }

    });
}

module.exports = {
    Defects: Defects, TestResults: TestResults, CCBProjects: CCBProjects, UserStory: UserStory, ComarchDefects:ComarchDefects,
    ReleaseMileStones: ReleaseMileStones, TestReports: TestReports, ZEPHYR_TestResults: ZEPHYR_TestResults,
    ZEPHYR_Requirements: ZEPHYR_Requirements, DataUploads: DataUploads,
    ZEPHYR_TestResultDetails: ZEPHYR_TestResultDetails,
    ComarchMapping: ComarchMapping
}


module.exports.DefectsInsertOrupdate = async function (qry, updateQry, defectKey) {
    await DefectsInsertOrupdate(qry, updateQry, defectKey);
}

module.exports.StoryUpdateOrInsert = async function (qry, updateQry, storyKey) {
    await StoryUpdateOrInsert(qry, updateQry, storyKey);
}

module.exports.DefectsUpdateFixVersions = async function () {
    await DefectsUpdateFixVersions();
}