const fs = require('fs');
const mysql = require('mysql');
var timeout = require('express-timeout-handler');
const bodyParser = require("body-parser");
const express = require('express');

const JiraImports = require("./app/imports/jira/jira-defect-import");

const ComarchJiraImports = require("./app/imports/jira/comarch-jira-import");

const CCBRelease = require("./app/imports/release-import");

const CCBProjects = require("./app/imports/projects-import");

const CCBComarchMappingTable = require("./app/imports/comarch-import");

const SpuploadPDF = require("./app/imports/uploadpdf-import")

const CCBTestReports = require("./app/imports/test-reports-import");

const ZephyrAPI = require("./app/imports/zephyr/zephyr-import");

cors = require("cors");
const app = express();

const v8 = require('v8');

// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({limit: '50mb', extended: true}));        
app.use(bodyParser.text({limit: '50mb', extended: true}));        
app.use(bodyParser.urlencoded({limit:'50mb', extended: true }));  


const { currentTime } = require("./app/utils/date-utils")
var options = {
    timeout: 30000,
    onTimeout: function (req, res) {
        res.status(503).send({
            status: "INPROGRESS",
            message: 'Service Request is running in a background wait for some time and refresh the page for update'
        }
        );
    },

};



const totalHeapSize = v8.getHeapStatistics().total_available_size;
let totalHeapSizeInGB = (totalHeapSize / 1024/1024/1024).toFixed(2);

console.log("Total heap size in bytes",totalHeapSize,"In GB",totalHeapSizeInGB);

app.use(cors());

app.use(timeout.handler(options));

global.__basedir = __dirname;


app.get('/api/CCB/uploadRelease', (req, res) => {
    console.log(currentTime(), "\n");
    console.log(currentTime(), " ******************  RELEASE MILESTONE UPLOAD STARTED *******************");

    CCBRelease.UploadCCBRelease(req.query.runType).then(response => {
        if (isError(response)) {
            console.log(currentTime(), " Error on ", response.message);
            res.send({
                status: "FAILURE",
                message: "Error on get release service",
                error: response.message

            });
        } else {
            res.send({
                status: "SUCCESS",
                message: "Release Milestones imported Successfully",
                userMessage: response[0],
                data: response[1],
            });
        }
        console.log(currentTime(), " ******************  RELEASE MILESTONE UPLOAD COMPLETED *******************\n");

    })

});



app.get('/api/CCB/uploadProjects', (req, res) => {
    console.log(currentTime(), " ******************  PROJECT MILESTONE UPLOAD STARTED *******************");
    CCBProjects.UploadCCBProjects(req.query.runType).then(response => {
        if (isError(response)) {
            console.log(currentTime(), " Error on ", response.message);
            res.send({
                status: "FAILURE",
                message: "Error on get project service",
                error: response.message

            });
        } else {
            res.send({
                status: "SUCCESS",
                message: "Project Milestones imported Successfully",
                userMessage: response[0],
                data: response[1],
            });
        }

        console.log(currentTime(), " ******************  PROJECT MILESTONE UPLOAD COMPLETED *******************\n");
    })

});

function isError(obj) {
    return Object.prototype.toString.call(obj) === "[object Error]";
}

app.post('/api/uploadpdf', (req, res) => {
    console.log(currentTime(), " ******************  PDF UPLOAD STARTED *******************");
    SpuploadPDF.UploadPDF(req.body).then(response => {
        if (response === "Success") {
            res.send({
                status: "SUCCESS",
                messageCode: "MSG200",
                message: "Pdf Merged successfully",
                userMessage: "Pdf Merged successfully !!!.",
              });
              console.log(currentTime(), " ******************  PDF UPLOAD COMPLETED *******************\n");
        } else {
            res.status(500).send({
                status: "Failure",
                messageCode: "MSG500",
                message: "Some error occurred while merging the pdf",
                userMessage: "Please check the parsed data",
              });
              console.log(currentTime(), " ****************** ERROR IN PDF UPLOAD *******************\n");
        }
       
        
    })

});


app.get('/api/uploadTestReports', (req, res) => {
    console.log(currentTime(), " ******************  REPORTS UPLOAD STARTED *******************");
    CCBTestReports.UploadCCBTestDeliverables(req.query.runType).then(response => {
        if (isError(response)) {
            console.log(currentTime(), " Error on ", response.message);
            res.send({
                status: "FAILURE",
                message: "Error on get testReports service",
                error: response.message

            });
        } else {
            res.send({
                status: "SUCCESS",
                message: 'Test Deliverables/External Reports imported Successfully',
                userMessage: response[0],
                data: response[1],
            });
        }
        console.log(currentTime(), " ******************   TEST REPORTS UPLOAD COMPLETED *******************\n");
    })

});



app.get('/api/jira/uploadDefects/:releaseName',
    timeout.set(120000), (req, res) => {
        console.log(currentTime(), "\n");
        console.log(currentTime(), "\n ******************  JIRA DEFECTS UPLOAD STARTED *******************");
        JiraImports.UploadJiraDefectsForProjects(req.query.runType, req.params.releaseName).then(response => {
            if (isError(response)) {
                console.log(currentTime(), " Error on ", response.message);
                res.send({
                    status: "FAILURE",
                    message: "Error on get uploadDefects service",
                    error: response.message

                });
            } else {
                res.send({
                    status: "SUCCESS",
                    message: 'Defects Uploaded Successfully',
                    userMessage: response[0],
                    data: response[1],
                });
            }
            console.log(currentTime(), " ******************  JIRA DEFECTS UPLOAD COMPLETED *******************\n");
        })
    });


app.get('/api/jira/uploadDefects/upcomingRelease',
    timeout.set(120000), (req, res) => {
        console.log(currentTime(), "\n");
        console.log(currentTime(), " ******************  JIRA DEFECTS UPLOAD STARTED *******************");
        JiraImports.UploadJiraDefectsForProjects(req.query.runType).then(response => {
            if (isError(response)) {
                console.log(currentTime(), " Error on ", response.message);
                res.send({
                    status: "FAILURE",
                    message: "Error on get uploadDefects service",
                    error: response.message

                });
            } else {
                res.send({
                    status: "SUCCESS",
                    message: 'Defects Uploaded Successfully',
                    userMessage: response[0],
                    data: response[1],
                });
            }
            console.log(currentTime(), " ******************  JIRA DEFECTS UPLOAD COMPLETED *******************\n");
        })

    });

    app.post('/api/jira/uploadComarchDefects/upcomingRelease',
    timeout.set(120000), (req, res) => {
        console.log(currentTime(), "\n");
        console.log(currentTime(), " ****************** COMARCH JIRA DEFECTS UPLOAD STARTED *******************");
        ComarchJiraImports.UploadJiraDefectsForProjects(req.body).then(response => {
            if (response.includes("Invalid")) {
                console.log(currentTime(), " Error on ", response);
                res.send({
                    status: "FAILURE",
                    message: "Error on get uploadDefects service",
                    error: response.message

                });
            } else {
                res.send({
                    status: "SUCCESS",
                    message: 'Comarch Jira Data Uploaded Successfully',
                    userMessage: response[0],
                    data: response[1],
                });
            }
            console.log(currentTime(), " ******************  COMARCH JIRA DEFECTS UPLOAD COMPLETED *******************\n");
        })

    });


app.get('/api/jira/uploadComponents', (req, res) => {
    console.log(currentTime(), "\n");
    console.log(currentTime(), " ******************  JIRA COMPONENTS UPLOAD STARTED *******************");
    JiraImports.UploadProjectComponents(req.query.runType).then(response => {
        if (isError(response)) {
            console.log(currentTime(), " Error on ", response.message);
            res.send({
                status: "FAILURE",
                message: "Error on get Components service",
                error: response.message
            });
        } else {
            res.send({
                status: "SUCCESS",
                message: 'Components Uploaded Successfully',
                userMessage: response[0],
                data: response[1],
            });
        }
        console.log(currentTime(), " ******************  JIRA COMPONENTS UPLOAD COMPLETED *******************\n");
    })
});


app.get('/api/jira/uploadStories/:releaseName',
    timeout.set(120000), (req, res) => {
        console.log(currentTime(), "\n");
        console.log(currentTime(), " ******************  JIRA USER STORIES UPLOAD STARTED *******************");
        JiraImports.UploadJiraUserStories(req.query.runType, req.params.releaseName).then(response => {
            if (isError(response)) {
                console.log(currentTime(), " Error on ", response.message);
                res.send({
                    status: "FAILURE",
                    message: "Error on get User Stories service",
                    error: response.message
                });
            } else {
                res.send({
                    status: "SUCCESS",
                    message: 'User Stories Uploaded Successfully',
                    userMessage: response[0],
                    data: response[1],
                });
            }
            console.log(currentTime(), " ******************  JIRA USER STORIES UPLOAD COMPLETED *******************\n");
        })

    });


app.get('/api/jira/uploadStories/upcomingRelease',
    timeout.set(120000), (req, res) => {
        console.log(currentTime(), "\n");
        console.log(currentTime(), " ******************  JIRA USER STORIES UPLOAD STARTED *******************");
        JiraImports.UploadJiraUserStories(req.query.runType).then(response => {
            if (isError(response)) {
                console.log(currentTime(), " Error on ", response.message);
                res.send({
                    status: "FAILURE",
                    message: "Error on get User Stories service",
                    error: response.message

                });
            } else {
                res.send({
                    status: "SUCCESS",
                    message: 'User Stories Uploaded Successfully',
                    userMessage: response[0],
                    data: response[1],
                });
            }
            console.log(currentTime(), " ******************  JIRA USER STORIES UPLOAD COMPLETED *******************\n");
        })

    });


app.get('/api/jira/cleanUp/defects', (req, res) => {
    console.log(currentTime(), "\n");
    console.log(currentTime(), " ******************  JIRA DEFECTS CLEANUP STARTED *******************");

    JiraImports.CleanUpDefects().then(response => {
        res.json({
            message: 'Jira Defects Absolete records clean up completed Successfully',
            response
        });
        console.log(currentTime(), " ******************  JIRA DEFECTS CLEANUP COMPLETED *******************\n");
    })
});


app.get('/api/jira/update/defects', (req, res) => {
    console.log(currentTime(), "\n");
    console.log(currentTime(), " ******************  JIRA DEFECTS Version Update STARTED *******************");

    JiraImports.UpdateDefectFixVersion().then(response => {
        res.json({
            message: 'Jira Defects Fix Version completed Successfully',
            response
        });
        console.log(currentTime(), " ******************  JIRA DEFECTS Version Update COMPLETED *******************\n");
    })
});


app.get('/api/jira/cleanUp/stories', (req, res) => {
    console.log(currentTime(), "\n");
    console.log(currentTime(), " ******************  JIRA USER STORIES CLEANUP STARTED *******************");

    JiraImports.CleanUpUserStories().then(response => {
        res.json({
            message: 'Jira User Stories Absolete records clean up completed Successfully',
            response
        });

        console.log(currentTime(), " ******************  JIRA USER STORIES CLEANUP COMPLETED *******************\n");
    })
});



app.get('/api/uploadTests/:releaseName',
    timeout.set(120000), (req, res) => {
        console.log(currentTime(), " ******************  ZEPHYR TEST EXECUTION RESULTS UPLOAD STARTED *******************");
        ZephyrAPI.UploadZephyrTestResults(req.query.runType, req.params.releaseName).then(response => {
            res.json({
                message: 'Test Results uploaded Successfully',
                response
            });
            console.log(currentTime(), " ******************  ZEPHYR TEST EXECUTION RESULTS UPLOAD COMPLETED *******************\n");
        })
    });

app.get('/api/uploadTests/upcomingRelease',
    timeout.set(120000), (req, res) => {
        console.log(currentTime(), "\n");
        console.log(currentTime(), " ******************  ZEPHYR TEST EXECUTION RESULTS UPLOAD STARTED *******************");
        ZephyrAPI.UploadZephyrTestResults(req.query.runType).then(response => {
            res.send({
                status: "SUCCESS",
                message: 'Test Execution Results Uploaded Successfully',
                userMessage: response[0],
                data: response[1],
            });
            console.log(currentTime(), " ******************  ZEPHYR TEST EXECUTION RESULTS UPLOAD COMPLETED *******************");
        })
    });


app.get('/api/uploadTestDetails/upcomingRelease', (req, res) => {
    console.log(currentTime(), "\n");
    console.log(currentTime(), " ******************  ZEPHYR TEST EXECUTION RESULTS Details UPLOAD STARTED *******************");
    ZephyrAPI.UploadZephyrTestResultDetails(req.query.runType).then(response => {
        res.send({
            status: "SUCCESS",
            message: 'Test Execution Result Details Uploaded Successfully',

        });
        console.log(currentTime(), " ******************  ZEPHYR TEST EXECUTION RESULTS Details UPLOAD COMPLETED *******************");
    })
});

app.get('/api/uploadRequirements/:releaseName',
    timeout.set(120000), (req, res) => {
        console.log(currentTime(), "\n");
        console.log(currentTime(), " ****************** ZEPHYR  REQUIREMENTS  UPLOAD STARTED *******************");
        ZephyrAPI.UploadZephyrRequirements(req.query.runType, req.params.releaseName).then(response => {
            res.send({
                status: "SUCCESS",
                message: 'User Requirements Uploaded Successfully',
                userMessage: response[0],
                data: response[1],
            });

            console.log(currentTime(), " ******************  ZEPHYR REQUIREMENTS UPLOAD COMPLETED *******************\n");
        })

    });

app.get('/api/uploadRequirements/upcomingRelease',
    timeout.set(120000), (req, res) => {
        console.log(currentTime(), "\n");
        console.log(currentTime(), " ****************** ZEPHYR  REQUIREMENTS  UPLOAD STARTED *******************");
        ZephyrAPI.UploadZephyrRequirements(req.params.runType).then(response => {
            res.send({
                status: "SUCCESS",
                message: 'User Requirements Successfully',
                userMessage: response[0],
                data: response[1],
            });
            console.log(currentTime(), " ******************  ZEPHYR REQUIREMENTS UPLOAD COMPLETED *******************\n");
        })

    });


app.get('/api/delete/requirements', (req, res) => {
    console.log(currentTime(), " \n");
    console.log(currentTime(), " ******************  ZEPHYR REQUIREMENTS Records Cleanup *******************");
    ZephyrAPI.DeleteDuplicteRequirements(req.params.releaseName).then(response => {
        res.json({
            message : 'Requirements Absolete records clean up completed Successfully',
            response
        });
    })
});

app.get('/api/delete/testResults', (req, res) => {
    console.log(currentTime(), " \n");
    console.log(currentTime(), " ******************  ZEPHYR Test Execution Records Cleanup *******************");
    ZephyrAPI.DeleteDuplicteTestResults(req.params.releaseName).then(response => {
        res.json({
            message: 'Test Results Absolete records clean up completed Successfully',
            response
        });
    })
});


app.get('/api/jira/update/epicLink', (req, res) => {
    console.log(currentTime(), "\n");
    console.log(currentTime(), " ******************  JIRA Epic updation for Test & Requirements STARTED *******************");

    JiraImports.UpdateEpicLinkinTestExecutionandRequirement().then(response => {
        res.json({
            message: 'JIRA Epic updation for Test & Requirements completed Successfully',
            response
        });
        console.log(currentTime(), " ******************  JIRA Epic updation for Test & Requirements COMPLETED *******************\n");
    })
});

app.get('/api/comarchjira/update/epicLink', (req, res) => {
    console.log(currentTime(), "\n");
    console.log(currentTime(), " ******************  Epic updation for Comarch started *******************");

    ComarchJiraImports.UpdateComarchDefectFixVersion().then(response => {
        res.json({
            message: 'Epic updation for Comarch completed Successfully',
            response
        });
        console.log(currentTime(), " ******************  Epic updation for Comarch COMPLETED *******************\n");
    })
});

app.get('/api/CCB/uploadMappingTable', (req, res) => {
    console.log(currentTime(), " ******************  MAPPING TABLE UPLOAD STARTED *******************");
    CCBComarchMappingTable.UploadCCBMappingTable().then(response => {
        if (response.includes('err')) {
            console.log(currentTime(), " Error on ", response.message);
            res.send({
                status: "FAILURE",
                message: "Error on get project service",
                error: response.message

            });
        } else {
            res.send({
                status: "SUCCESS",
                message: "Comarch Mapping Table Updated Successfully",
                userMessage: response,
                data: response,
            });
        }

        console.log(currentTime(), " ******************  MAPPING TABLE UPLOAD COMPLETED *******************\n");
    })

});


function importTestResuts(filePath) {
    console.log("FilePath ", filePath)
    ZephyrTestResults.importTestResuts(filePath)
}


app.listen(3322, () => {
    console.log(`Server is running on port 3322`);
});





