const cron = require("node-cron");

/**
 * cron schedule  * * * * *
 *         1 - minutes
 *         2 - Hours
 *         3 - day
 *         4 - month
 *         5 - year          
 */

//refreshFiles();
var curl = require('curlrequest');

/* Upload Release */
cron.schedule("31 07-19 * * 1-5", function () {
  console.log(getDateTime() + "  >>  Scheduled Action for every 1 hour > Release upload");
  curl.request({ url: 'http://localhost:3322/api/CCB/uploadRelease?runType=automatedRun'}, function (err, stdout, meta) {
    console.log('%s', stdout);
  });
});


/* Upload Projects */
cron.schedule("31 07-19 * * 1-5", function () {
  console.log(getDateTime() + "  >>  Scheduled Action for every 1 hour  > Projects upload");
  curl.request({ url: 'http://localhost:3322/api/CCB/uploadProjects?runType=automatedRun'}, function (err, stdout, meta) {
    console.log('%s', stdout);
  });
});


/*  Upload Test Reports **/
cron.schedule("32 07-19 * * 1-5", function () {
  console.log(getDateTime() + "  >>  Scheduled Action for every 1 hour > Test Reports upload");
  curl.request({ url: 'http://localhost:3322/api/uploadTestReports?runType=automatedRun'}, function (err, stdout, meta) {
    console.log('%s', stdout);
  });
});


// /* Upload Components */
// cron.schedule("33 07-19 * * 1-5", function () {
//   console.log(getDateTime() + "  >>  Scheduled Action for every 1 hour  > Components upload");
//   curl.request({ url: 'http://localhost:3322/api/jira/uploadComponents?runType=automatedRun' }, function (err, stdout, meta) {
//     console.log('%s', stdout);
//   });
// });

/* Upload Defects */
cron.schedule("34 07-19 * * 1-5", function () {
  console.log(getDateTime() + "  >>  Scheduled Action for every 1 hour  > Defects  upload");
  curl.request({ url: 'http://localhost:3322/api/jira/uploadDefects/upcomingRelease?runType=automatedRun'}, function (err, stdout, meta) {
    console.log('%s', stdout);
  });
});


/* Delete Duplicate Records from Defects Table */

cron.schedule("37 07-19 * * 1-5", function () {
  console.log(getDateTime() + "  >>  Scheduled Action for every 1 hour   > Defects Cleanup");
  curl.request({ url: 'http://localhost:3322/api/jira/cleanUp/defects'}, function (err, stdout, meta) {
    console.log('%s ', stdout);
  });
});


/* Update Fix Version  Defects Table */

cron.schedule("37 07-19 * * 1-5", function () {
  console.log(getDateTime() + "  >>  Scheduled Action for every 1 hour   > Defects Update");
  curl.request({ url: 'http://localhost:3322/api/jira/update/defects'}, function (err, stdout, meta) {
    console.log('%s ', stdout);
  });
});

/* Update Epic data in Requirements and Test Results */

cron.schedule("37 07-19 * * 1-5", function () {
  console.log(getDateTime() + "  >>  Scheduled Action for every 1 hour   > Epic Link updation in Requirments and Test Results");
  curl.request({ url: 'http://localhost:3322/api/jira/update/epicLink'}, function (err, stdout, meta) {
    console.log('%s ', stdout);
  });
});

/* Upload User Story */
cron.schedule("39 07-19 * * 1-5", function () {
  console.log(getDateTime() + "  >>  Scheduled Action for every 1 hour  > User Story upload");
  curl.request({ url: 'http://localhost:3322/api/jira/uploadStories/upcomingRelease?runType=automatedRun'}, function (err, stdout, meta) {
    console.log('%s', stdout);
  });
});


/* Delete Duplicate Records from UserStory Table */

cron.schedule("41 07-19 * * 1-5", function () {
  console.log(getDateTime() + "  >>  Scheduled Action for every 1 hour   > User Story Cleanup");
  curl.request({ url: 'http://localhost:3322/api/jira/cleanUp/stories'}, function (err, stdout, meta) {
    console.log('%s ', stdout);
  });
});




/* Upload Requirements */
cron.schedule("43 07-19 * * 1-5", function () {
  console.log(getDateTime() + "  >>  Scheduled Action for every 1 hour > Requirements upload");
  curl.request({ url: 'http://localhost:3322/api/uploadRequirements/upcomingRelease?runType=automatedRun'}, function (err, stdout, meta) {
    console.log('%s', stdout);
  });
});


/* Delete Duplicate Requriements from Requirements Table */

cron.schedule("45 07-19 * * 1-5", function () {
  console.log(getDateTime() + "  >>  Scheduled Action for every 1 hour   > Test Requirements CleanUp");
  curl.request({ url: 'http://localhost:3322/api/delete/requirements'}, function (err, stdout, meta) {
    console.log('%s ', stdout);
  });
});


/* Upload Test Results */
cron.schedule("47 07-19 * * 1-5", function () {
  console.log(getDateTime() + "  >>  Scheduled Action for every 1 hour  > Test Results upload");
  curl.request({ url: 'http://localhost:3322/api/uploadTests/upcomingRelease?runType=automatedRun'}, function (err, stdout, meta) {
    console.log('%s', stdout);
  });
});


/* Load Requriements ID against each tests.  Test Details Vs Requirements */

cron.schedule("58 07-19 * * 1-5", function () {
  console.log(getDateTime() + "  >>  Scheduled Action for every 1 hour   > Test Details Vs Requirements upload");
  curl.request({ url: 'http://localhost:3322/api/uploadTestDetails/upcomingRelease'}, function (err, stdout, meta) {
    console.log('%s ', stdout);
  });
});



/* Delete Duplicate Records from Test Results Table */

cron.schedule("59 07-19 * * 1-5", function () {
  console.log(getDateTime() + "  >>  Scheduled Action for every 1 hour   > Test Results Cleanup");
  curl.request({ url: 'http://localhost:3322/api/delete/testResults'}, function (err, stdout, meta) {
    console.log('%s ', stdout);
  });
});





function getDateTime() {
  let ts = Date.now();

  let date_ob = new Date(ts),

    date = date_ob.getDate(),
    month = date_ob.getMonth() + 1,
    year = date_ob.getFullYear(),
    hr = date_ob.getHours(),
    min = date_ob.getMinutes(),
    sec = date_ob.getSeconds(),
    ms = date_ob.getMilliseconds();
  let dateTime = year + "-" + month + "-" + date + " " + hr + ":" + min + ":" + sec + ":" + ms;

  return dateTime
}

function isLinux() {
  let isLin = process.platform === "linux";
  return isLin
}
